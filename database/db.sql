/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.4.20-MariaDB : Database - wapsi_car
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wapsi_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `wapsi_car`;

/*Table structure for table `agent` */

DROP TABLE IF EXISTS `agent`;

CREATE TABLE `agent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Table structure for table `agent_5sep21` */

DROP TABLE IF EXISTS `agent_5sep21`;

CREATE TABLE `agent_5sep21` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `driver_id` bigint(10) DEFAULT NULL,
  `pickup_location` varchar(500) DEFAULT NULL,
  `pickup_lat` varchar(255) DEFAULT NULL,
  `pickup_long` varchar(255) DEFAULT NULL,
  `dropoff_location` varchar(255) DEFAULT NULL,
  `approx_distance` varchar(255) DEFAULT NULL,
  `route_rate` varchar(255) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `driver_rate` varchar(255) DEFAULT NULL,
  `comission` varchar(255) DEFAULT NULL,
  `instructions` varchar(2500) DEFAULT NULL,
  `status` enum('pending','inprocess','completed','rejectbydriver','rejectedbyclient','cancelbydriver','cancelbyclient','timeout') DEFAULT 'pending' COMMENT '-1 =>cancelled, 3=> new, 2=>assigned, 3=>completed',
  `trip_start` enum('yes','no') DEFAULT 'no',
  `trip_end` enum('yes','no') DEFAULT NULL,
  `trip_start_time` datetime DEFAULT NULL,
  `trip_end_time` datetime DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `driver_last_lat` varchar(255) DEFAULT NULL,
  `driver_last_long` varchar(255) DEFAULT NULL,
  `customer_cnic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `city_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Table structure for table `city_request` */

DROP TABLE IF EXISTS `city_request`;

CREATE TABLE `city_request` (
  `request_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(20) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `status` bigint(20) DEFAULT 0,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `document_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `medium` enum('ios','android') DEFAULT 'ios',
  `notification_type` varchar(255) DEFAULT NULL,
  `booking_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `insertion_datetime` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `sent_status` enum('yes','no') DEFAULT 'yes',
  `read_status` enum('yes','no') DEFAULT 'no',
  `custom_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `otp_code` */

DROP TABLE IF EXISTS `otp_code`;

CREATE TABLE `otp_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(20) DEFAULT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `otp_code` bigint(20) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `payment` decimal(10,2) DEFAULT NULL,
  `transaction_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `difference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `rate_cards` */

DROP TABLE IF EXISTS `rate_cards`;

CREATE TABLE `rate_cards` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `route_id` bigint(10) DEFAULT NULL,
  `driver_rates` decimal(10,2) DEFAULT NULL,
  `commission_rates` decimal(10,2) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `rate_cards__25sep21` */

DROP TABLE IF EXISTS `rate_cards__25sep21`;

CREATE TABLE `rate_cards__25sep21` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `route_id` bigint(10) DEFAULT NULL,
  `driver_rates` decimal(10,2) DEFAULT NULL,
  `commission_rates` decimal(10,2) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;

/*Table structure for table `routes` */

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `route_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `from_city` bigint(10) DEFAULT NULL,
  `to_city` bigint(10) DEFAULT NULL,
  `rates` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '1-> active, 2->disable',
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `session_id` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT -1,
  `username` varchar(420) DEFAULT NULL,
  `ipaddress` varchar(255) DEFAULT '0.0.0.0',
  `creation_time` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `textvalue` text DEFAULT NULL,
  `session_type` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(500) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phoneno` varchar(250) DEFAULT NULL,
  `account_details` varchar(250) DEFAULT NULL,
  `account_name` varchar(250) DEFAULT NULL,
  `facebook_url` varchar(250) DEFAULT NULL,
  `twitter_url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `user_document_info` */

DROP TABLE IF EXISTS `user_document_info`;

CREATE TABLE `user_document_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `document_id` bigint(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `user_documents` */

DROP TABLE IF EXISTS `user_documents`;

CREATE TABLE `user_documents` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `document_id` bigint(10) DEFAULT NULL,
  `user_id` bigint(10) DEFAULT NULL,
  `document_path` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '-1 => deleted, 1=>active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `user_password_history` */

DROP TABLE IF EXISTS `user_password_history`;

CREATE TABLE `user_password_history` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `user_shift_status_logs` */

DROP TABLE IF EXISTS `user_shift_status_logs`;

CREATE TABLE `user_shift_status_logs` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `status` enum('offline','online') DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `user_vehicle` */

DROP TABLE IF EXISTS `user_vehicle`;

CREATE TABLE `user_vehicle` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `make_id` bigint(10) DEFAULT NULL,
  `model_id` bigint(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `vehicle_number` varchar(200) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '-1 => deleted, 1=>active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `cnic_expiry_date` varchar(255) DEFAULT NULL,
  `user_type` int(8) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT current_timestamp(),
  `android_push_id` varchar(1024) DEFAULT NULL,
  `apple_push_id` varchar(1024) DEFAULT NULL,
  `profile_img` varchar(1024) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `num_of_login_attempts` int(2) unsigned DEFAULT NULL,
  `last_login_time` int(11) unsigned DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_new_user` enum('yes','no') DEFAULT 'yes',
  `last_change_password_date` datetime DEFAULT NULL,
  `credit_limit` varchar(255) DEFAULT NULL,
  `account_status` tinyint(2) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `shift_status` enum('offline','online') DEFAULT 'offline',
  `last_shift_update_date` datetime DEFAULT NULL,
  `user_rating` float(10,2) DEFAULT 0.00,
  `driver_booking_status` enum('pending','ontrip') DEFAULT 'pending',
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `last_location_updated_at` datetime DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`user_id`),
  KEY `login_id` (`login_id`),
  KEY `user_type` (`user_type`),
  KEY `first_name` (`full_name`),
  KEY `creation_date` (`creation_date`),
  KEY `user_type_2` (`user_type`),
  KEY `first_name_2` (`full_name`),
  KEY `creation_date_2` (`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `users_25sep21` */

DROP TABLE IF EXISTS `users_25sep21`;

CREATE TABLE `users_25sep21` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `cnic_expiry_date` varchar(255) DEFAULT NULL,
  `user_type` int(8) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT current_timestamp(),
  `android_push_id` varchar(1024) DEFAULT NULL,
  `apple_push_id` varchar(1024) DEFAULT NULL,
  `profile_img` varchar(1024) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `num_of_login_attempts` int(2) unsigned DEFAULT NULL,
  `last_login_time` int(11) unsigned DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_new_user` enum('yes','no') DEFAULT 'yes',
  `last_change_password_date` datetime DEFAULT NULL,
  `credit_limit` varchar(255) DEFAULT NULL,
  `account_status` tinyint(2) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `shift_status` enum('offline','online') DEFAULT 'offline',
  `last_shift_update_date` datetime DEFAULT NULL,
  `user_rating` float(10,2) DEFAULT 0.00,
  `driver_booking_status` enum('pending','ontrip') DEFAULT 'pending',
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `last_location_updated_at` datetime DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`user_id`),
  KEY `login_id` (`login_id`),
  KEY `user_type` (`user_type`),
  KEY `first_name` (`full_name`),
  KEY `creation_date` (`creation_date`),
  KEY `user_type_2` (`user_type`),
  KEY `first_name_2` (`full_name`),
  KEY `creation_date_2` (`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1544 DEFAULT CHARSET=utf8;

/*Table structure for table `vehicle_make` */

DROP TABLE IF EXISTS `vehicle_make`;

CREATE TABLE `vehicle_make` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `status` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Table structure for table `vehicle_model` */

DROP TABLE IF EXISTS `vehicle_model`;

CREATE TABLE `vehicle_model` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `make_id` bigint(10) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `car_type` enum('sedan','hatchback','suv') DEFAULT 'sedan',
  `status` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
