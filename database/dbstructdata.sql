/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.4.20-MariaDB : Database - wapsi_car
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wapsi_car` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `wapsi_car`;

/*Table structure for table `agent` */

DROP TABLE IF EXISTS `agent`;

CREATE TABLE `agent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `agent` */

insert  into `agent`(`id`,`name`,`company_name`,`email`,`mobile`,`city`,`address`,`created_at`,`created_by`,`updated_at`,`updated_by`,`status`) values (15,'WAPSI CAR',NULL,'abbasitransport@gmail.com','03322905423','SUKKUR','AIRPORT ROAD NEAR AL KHAIR HOSPITAL SUKKUR SINDH','2021-09-17 12:50:13',1,'2021-07-28 00:00:00',1,'active'),(27,'Global travellers','','Tahakhwaja@gmail.com','03421128623','Lahore','DHA  Lahore','2021-09-17 00:00:00',1,'2021-09-17 00:00:00',1,'active'),(28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'inactive');

/*Table structure for table `agent_5sep21` */

DROP TABLE IF EXISTS `agent_5sep21`;

CREATE TABLE `agent_5sep21` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `agent_5sep21` */

insert  into `agent_5sep21`(`id`,`name`,`company_name`,`email`,`mobile`,`city`,`address`,`created_at`,`created_by`,`updated_at`,`updated_by`,`status`) values (1,'!@#$%^&*()_+-=',NULL,'','!@#$%^&*()_+-=','!@#$%^&*()_+-=','','2021-04-12 00:00:00',1,NULL,NULL,'inactive'),(2,'Testing Khan aa',NULL,'test@gmail.com aa','03323334999 aa','Karachi aa aa','test aa','2021-04-12 00:00:00',1,'2021-04-13 00:00:00',1,'inactive'),(3,'Ramal Hafeez',NULL,'test@gmail.com','03323334999','Karachi','Test','2021-04-13 00:00:00',1,NULL,NULL,'inactive'),(4,'Testing Khan',NULL,'','03323334999','','','2021-04-13 00:00:00',1,NULL,NULL,'inactive'),(5,'BISMILLAH RENT A CAR',NULL,'testing@gmail.com','03022368212','KARACHI','RABIA CITY','2021-04-16 00:00:00',1,NULL,NULL,'inactive'),(6,'Ramal Hafeez Khan',NULL,'test@gmail.com','099999999999999','Karachi','Test','2021-05-16 00:00:00',1,'2021-06-16 00:00:00',0,'inactive'),(7,'Ramal Hafeez',NULL,'test@gmail.com','099999999999999','Karachi','Test','2021-05-16 00:00:00',1,NULL,NULL,'inactive'),(8,'Ramal Hafeez',NULL,'test@gmail.com','099999999999999','Karachi','hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh','2021-05-16 00:00:00',1,'2021-05-16 00:00:00',1,'inactive'),(9,'dsafdsaf',NULL,'test@gmail.com','111111111111111','dfsg','sadfsa','2021-05-19 00:00:00',1,NULL,NULL,'inactive'),(10,'dsafdsaf',NULL,'test@gmail.com','111111111111111','dfsg','sadfsa','2021-05-19 00:00:00',1,NULL,NULL,'inactive'),(11,'dsafdsaf',NULL,'test@gmail.com','111111111111111','dfsg','sadfsa','2021-05-19 00:00:00',1,NULL,NULL,'inactive'),(12,'Tariq Test','s s f','tkhawaja@gmail.comtest@gmail.com','03002658202','dfsg','sadfsa','2021-05-19 00:00:00',1,'2021-09-01 00:00:00',1,'active'),(15,'WAPSI CAR',NULL,'abbasitransport@gmail.com','03322905423','SUKKUR','AIRPORT ROAD NEAR AL KHAIR HOSPITAL SUKKUR SINDH',NULL,1,'2021-07-28 00:00:00',1,'active'),(16,'BISMILLAH RENT A CAR',NULL,'bismillahrent@gmail.com','03022368212','GHOTKI','KARACHI','2021-06-11 00:00:00',1,NULL,NULL,'active'),(17,'Ali Hashmi',NULL,'ammar.8137@iqra.edu.pk','03352434222','Karachi','hyderi north nazimabad karachi','2021-06-16 00:00:00',1,NULL,NULL,'active'),(18,'usama',NULL,'talha@gmail.com','033353939461','karachi','fsdf','2021-06-17 00:00:00',1,NULL,NULL,'active'),(19,'MURSHID RENT A CAR',NULL,'murshidrentacar@gmail.com','03003125700','KARACHI','KARACHI MOSIMIYAT','2021-06-22 00:00:00',1,NULL,NULL,'inactive'),(20,'MURSHID RENT A CAR',NULL,'murshidrentacar@gmail.com','03003125700','KARACHI','KARACHI MOSIMIYAT','2021-06-22 00:00:00',1,NULL,NULL,'active'),(21,'ABDUL JABBAR ABBASI',NULL,'abduljabbar@gmail.com','03322661333','sukkur','sukkur','2021-06-22 00:00:00',1,NULL,NULL,'active'),(22,'MUNEER AHMED SOLANGI DADU',NULL,'muneerahmed@gmail.comm','03123029742','DADU','DADU','2021-06-30 00:00:00',1,NULL,NULL,'active'),(23,'MUHAMMAD AKRAM BALADI',NULL,'akram@gmail.com','03313124860','Hyderabad','Hyderabad','2021-06-30 00:00:00',1,NULL,NULL,'active'),(24,'BADSHAH RENT A CAR',NULL,'rehmanmuhammad@gmail.com','03148476847','KARACHI','KARACHI KORANGI PNT SOCIETY ','2021-07-08 00:00:00',1,NULL,NULL,'active'),(25,'SINDH RENT A CAR ',NULL,'sindhrentcr@gmail.com','03003108469','SUKKUR ','SUKKUR BYPASS ','2021-07-08 00:00:00',1,NULL,NULL,'active'),(26,'RENT A CAR WARRAH ',NULL,'warah@gmail.com','03333751943','WARRAH ','WARRAH CITY QAMBAR ','2021-07-09 00:00:00',1,NULL,NULL,'active');

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `driver_id` bigint(10) DEFAULT NULL,
  `pickup_location` varchar(500) DEFAULT NULL,
  `pickup_lat` varchar(255) DEFAULT NULL,
  `pickup_long` varchar(255) DEFAULT NULL,
  `dropoff_location` varchar(255) DEFAULT NULL,
  `approx_distance` varchar(255) DEFAULT NULL,
  `route_rate` varchar(255) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `driver_rate` varchar(255) DEFAULT NULL,
  `comission` varchar(255) DEFAULT NULL,
  `instructions` varchar(2500) DEFAULT NULL,
  `status` enum('pending','inprocess','completed','rejectbydriver','rejectedbyclient','cancelbydriver','cancelbyclient','timeout') DEFAULT 'pending' COMMENT '-1 =>cancelled, 3=> new, 2=>assigned, 3=>completed',
  `trip_start` enum('yes','no') DEFAULT 'no',
  `trip_end` enum('yes','no') DEFAULT NULL,
  `trip_start_time` datetime DEFAULT NULL,
  `trip_end_time` datetime DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `driver_last_lat` varchar(255) DEFAULT NULL,
  `driver_last_long` varchar(255) DEFAULT NULL,
  `customer_cnic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `booking` */

insert  into `booking`(`record_id`,`user_id`,`driver_id`,`pickup_location`,`pickup_lat`,`pickup_long`,`dropoff_location`,`approx_distance`,`route_rate`,`price`,`driver_rate`,`comission`,`instructions`,`status`,`trip_start`,`trip_end`,`trip_start_time`,`trip_end_time`,`reason`,`comments`,`created_at`,`updated_at`,`deleted_at`,`driver_last_lat`,`driver_last_long`,`customer_cnic`) values (1,3,2,'','24.94612027554499','67.06186216324569','Mithi','278','','1500.00','1500.00','1350','','completed','yes','yes','2021-09-05 19:13:17','2021-09-05 19:13:19',NULL,NULL,'2021-09-05 17:13:00','2021-09-05 19:13:19',NULL,'24.9461097','67.0617745','44444-4444444-4'),(2,3,2,'','24.94611662758724','67.06185914576054','LARKANA','312','5000','2000.00','2000.00','500','','completed','yes','yes','2021-09-05 19:28:13','2021-09-05 19:28:18',NULL,NULL,'2021-09-05 17:27:49','2021-09-05 19:28:18',NULL,'24.9461133','67.0618465','25455-5555555-5'),(3,4,4,NULL,NULL,NULL,NULL,NULL,'','2500.00','1500.00','300',NULL,'completed','yes','yes','2021-09-05 19:28:13','2021-09-05 19:28:18',NULL,NULL,'2021-09-05 17:27:49','2021-09-05 19:28:18',NULL,NULL,'67.0618465','25455-5555555-6');

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `city_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `city` */

insert  into `city`(`city_id`,`city_name`,`status`) values (12,'Karachi',1),(13,'Hyderabad',1),(14,'LARKANA',1),(15,'Mithi',1),(16,'Islamabad',0),(19,'Sawat',-1),(20,'murre',1),(21,'SUKKUR',1),(22,'Abbotabad',1),(23,'LAHORE',1),(24,'Mansera',0);

/*Table structure for table `city_request` */

DROP TABLE IF EXISTS `city_request`;

CREATE TABLE `city_request` (
  `request_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(20) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `status` bigint(20) DEFAULT 0,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `city_request` */

insert  into `city_request`(`request_id`,`driver_id`,`city`,`status`) values (1,1285,'Mithi',1);

/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `document_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `documents` */

insert  into `documents`(`document_id`,`document_name`,`status`,`comments`) values (1,'CNIC',1,NULL),(2,'Driving License',1,NULL),(3,'Running Page',1,NULL),(4,'Tax Paid Receipt',1,NULL),(5,'Profile Picture',NULL,NULL);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `medium` enum('ios','android') DEFAULT 'ios',
  `notification_type` varchar(255) DEFAULT NULL,
  `booking_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `insertion_datetime` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `sent_status` enum('yes','no') DEFAULT 'yes',
  `read_status` enum('yes','no') DEFAULT 'no',
  `custom_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `notifications` */

insert  into `notifications`(`record_id`,`medium`,`notification_type`,`booking_id`,`user_id`,`insertion_datetime`,`title`,`message`,`sent_status`,`read_status`,`custom_message`) values (1,'android','booking',1,3,'2021-09-05 17:13:00','You have new booking','New booing has been awarded to you please respond','yes','no',NULL),(2,'android','endride',1,3,'2021-09-05 17:13:19','Ride has been finished','Ride has been finished','yes','no',NULL),(3,'android','booking',2,3,'2021-09-05 17:27:49','You have new booking','New booing has been awarded to you please respond','yes','no',NULL),(4,'android','endride',2,3,'2021-09-05 17:28:18','Ride has been finished','Ride has been finished','yes','no',NULL);

/*Table structure for table `otp_code` */

DROP TABLE IF EXISTS `otp_code`;

CREATE TABLE `otp_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(20) DEFAULT NULL,
  `agent_id` bigint(20) DEFAULT NULL,
  `otp_code` bigint(20) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `otp_code` */

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `payment` decimal(10,2) DEFAULT NULL,
  `transaction_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `difference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `payment` */

insert  into `payment`(`id`,`driver_id`,`payment`,`transaction_number`,`description`,`created_at`,`status`,`difference`) values (1,0,'111.00','333333333','','2021-09-14 12:59:22',0,NULL),(2,2,'9000.00','1222','','2021-09-14 13:02:48',1,NULL),(3,2,'900000.00','1212','','2021-09-14 13:12:56',1,NULL),(5,4,'6000.00','2343','ride ','2021-09-17 14:15:14',1,NULL);

/*Table structure for table `rate_cards` */

DROP TABLE IF EXISTS `rate_cards`;

CREATE TABLE `rate_cards` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `route_id` bigint(10) DEFAULT NULL,
  `driver_rates` decimal(10,2) DEFAULT NULL,
  `commission_rates` decimal(10,2) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `rate_cards` */

/*Table structure for table `rate_cards__25sep21` */

DROP TABLE IF EXISTS `rate_cards__25sep21`;

CREATE TABLE `rate_cards__25sep21` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `driver_id` bigint(10) DEFAULT NULL,
  `route_id` bigint(10) DEFAULT NULL,
  `driver_rates` decimal(10,2) DEFAULT NULL,
  `commission_rates` decimal(10,2) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;

/*Data for the table `rate_cards__25sep21` */

insert  into `rate_cards__25sep21`(`id`,`driver_id`,`route_id`,`driver_rates`,`commission_rates`,`status`,`entry_date`) values (6,1343,10,'1000.00','2333.00',1,'2021-05-27 11:03:43'),(29,1285,7,'4500.00','500.00',1,'2021-06-05 00:52:48'),(37,1415,6,'8000.00','1000.00',1,'2021-06-05 16:38:52'),(38,1415,3,'1500.00','500.00',1,'2021-06-05 16:38:52'),(51,1418,15,'9000.00','0.00',1,'2021-06-08 21:28:17'),(52,1418,16,'2500.00','500.00',1,'2021-06-08 21:28:17'),(53,1418,14,'2500.00','4500.00',1,'2021-06-08 21:28:17'),(89,1432,15,'8000.00','1000.00',1,'2021-06-24 09:39:58'),(102,1469,14,'7000.00','0.00',1,'2021-06-30 08:40:53'),(123,1459,15,'8500.00','500.00',1,'2021-07-09 19:17:14'),(124,1459,14,'5000.00','2000.00',1,'2021-07-09 19:17:14'),(142,1491,7,'2000.00','3000.00',1,'2021-07-13 15:00:17'),(143,1491,3,'2000.00','0.00',1,'2021-07-13 15:00:17'),(150,1506,3,'2000.00','0.00',1,'2021-07-14 18:35:20'),(151,1506,6,'1000.00','8000.00',1,'2021-07-14 18:35:20'),(152,1506,7,'5000.00','0.00',1,'2021-07-14 18:35:20'),(166,1516,6,'2000.00','7000.00',1,'2021-07-26 11:42:00'),(171,1514,6,'2000.00','7000.00',1,'2021-07-26 11:54:11'),(172,1514,7,'5000.00','0.00',1,'2021-07-26 11:54:11'),(173,1514,3,'1000.00','1000.00',1,'2021-07-26 11:54:11'),(207,1532,3,'1000.00','1000.00',1,'2021-08-31 14:07:17'),(208,1543,3,'1000.00','1000.00',1,'2021-08-31 14:20:23'),(214,1508,22,'25000.00','7000.00',1,'2021-09-03 06:27:05'),(215,1508,21,'25000.00','3000.00',1,'2021-09-03 06:27:05'),(216,1508,19,'25000.00','5000.00',1,'2021-09-03 06:27:05');

/*Table structure for table `routes` */

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `route_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `from_city` bigint(10) DEFAULT NULL,
  `to_city` bigint(10) DEFAULT NULL,
  `rates` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '1-> active, 2->disable',
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `routes` */

insert  into `routes`(`route_id`,`from_city`,`to_city`,`rates`,`status`) values (1,12,13,'5000',-1),(2,15,14,'900099',1),(3,12,13,'2000',1),(4,14,12,'9000',1),(5,15,12,'10000',1),(6,12,15,'9000',1),(7,12,14,'5000',1),(8,16,15,'453',1),(9,15,14,'0',1),(10,16,19,'3333',1),(11,0,0,'',1),(12,0,0,'',1),(13,0,0,'',1),(14,21,13,'7,000',1),(15,21,12,'9,000',1),(16,21,14,'3,000',-1),(17,21,14,'3,000',1),(18,19,20,'5,000',1),(19,21,19,'30,000',1),(20,13,21,'5,000',1),(21,21,20,'28,000',1),(22,21,22,'32,000',1),(23,21,16,'28,000',1),(24,21,23,'500',1),(25,23,22,'6,000',1);

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `session_id` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT -1,
  `username` varchar(420) DEFAULT NULL,
  `ipaddress` varchar(255) DEFAULT '0.0.0.0',
  `creation_time` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `textvalue` text DEFAULT NULL,
  `session_type` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sessions` */

insert  into `sessions`(`session_id`,`user_id`,`username`,`ipaddress`,`creation_time`,`last_updated`,`textvalue`,`session_type`) values ('43d16865446db4bef5f6699c8be9ae8f',-1,'03007042402','202.47.41.151','2021-09-05 17:11:31','2021-09-05 17:28:42','usertype=4&&&userid=3&&&loginid=tariq_kha%40yahoo.com&&&username=++&&&',1),('b9ecd9a740a6aab3a62c43bd0d88b438',-1,'03002658202','202.47.41.151','2021-09-05 16:59:14','2021-09-05 17:29:11','usertype=2&&&userid=2&&&loginid=03002658202&&&username=++&&&',1),('c1db5a20303f8cbb21015a5c73a07ca4',-1,'admin','127.0.0.1','2021-10-11 02:32:25','2021-10-11 05:32:26','usertype=1&&&userid=1&&&loginid=admin&&&username=++&&&',1);

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(500) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phoneno` varchar(250) DEFAULT NULL,
  `account_details` varchar(250) DEFAULT NULL,
  `account_name` varchar(250) DEFAULT NULL,
  `facebook_url` varchar(250) DEFAULT NULL,
  `twitter_url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`application_name`,`address`,`phoneno`,`account_details`,`account_name`,`facebook_url`,`twitter_url`) values (1,'Wapsi  car','Shahrah e faisal karachi','7777','88888888888888888888','DSFDSFSDF','http://www.facebook.com','http://www.twitter.com/a');

/*Table structure for table `user_document_info` */

DROP TABLE IF EXISTS `user_document_info`;

CREATE TABLE `user_document_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `document_id` bigint(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `user_document_info` */

insert  into `user_document_info`(`id`,`user_id`,`document_id`,`name`,`number`,`expiry_date`,`status`) values (1,2,1,'Tariq Khawaja','42101-1111111-1','2021-09-30',NULL),(2,2,1,'Tariq Khawaja','42101-1111111-1','2021-09-30',NULL),(3,2,2,'Tariq Irshad Khawaja ','668289565659459','2021-09-30',NULL),(4,2,2,'Tariq Irshad Khawaja ','668289565659459','2021-09-30',NULL),(5,2,3,NULL,NULL,NULL,NULL);

/*Table structure for table `user_documents` */

DROP TABLE IF EXISTS `user_documents`;

CREATE TABLE `user_documents` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `document_id` bigint(10) DEFAULT NULL,
  `user_id` bigint(10) DEFAULT NULL,
  `document_path` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '-1 => deleted, 1=>active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `user_documents` */

insert  into `user_documents`(`id`,`document_id`,`user_id`,`document_path`,`created_at`,`status`) values (1,1,2,'http://atswapsi.com/misc/uploads/documents/2/20210905165939IMG-20210905-WA0005.jpg',NULL,1),(2,1,2,'http://atswapsi.com/misc/uploads/documents/2/20210905165939IMG-20210905-WA0005.jpg20210905165939IMG-20210905-WA0005.jpg',NULL,1),(3,2,2,'http://atswapsi.com/misc/uploads/documents/2/20210905170014IMG-20210905-WA0007.jpg',NULL,1),(4,2,2,'http://atswapsi.com/misc/uploads/documents/2/20210905170014IMG-20210905-WA0007.jpg20210905170014IMG-20210905-WA0006.jpg',NULL,1),(5,3,2,'http://atswapsi.com/misc/uploads/documents/2/20210905170020IMG-20210905-WA0004.jpg',NULL,1),(6,4,2,'http://wapsicar.local.pk/misc/uploads/documents/2/20210914113211iamcloud.jpg',NULL,1),(7,4,2,'http://wapsicar.local.pk/misc/uploads/documents/2/20210914113216iamcloud.jpg',NULL,1),(8,4,2,'http://wapsicar.local.pk/misc/uploads/documents/2/20210914113220iamcloud.jpg',NULL,1),(9,4,2,'http://wapsicar.local.pk/misc/uploads/documents/2/20210914113223iamcloud.jpg',NULL,1);

/*Table structure for table `user_password_history` */

DROP TABLE IF EXISTS `user_password_history`;

CREATE TABLE `user_password_history` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_password_history` */

/*Table structure for table `user_shift_status_logs` */

DROP TABLE IF EXISTS `user_shift_status_logs`;

CREATE TABLE `user_shift_status_logs` (
  `record_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `status` enum('offline','online') DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `user_shift_status_logs` */

insert  into `user_shift_status_logs`(`record_id`,`user_id`,`status`,`datetime`) values (1,2,'online','2021-09-05 19:03:37'),(2,2,'online','2021-09-05 19:04:01'),(3,2,'online','2021-09-05 19:26:26'),(4,2,'online','2021-09-05 19:26:42'),(5,2,'online','2021-09-05 19:27:31');

/*Table structure for table `user_vehicle` */

DROP TABLE IF EXISTS `user_vehicle`;

CREATE TABLE `user_vehicle` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) DEFAULT NULL,
  `make_id` bigint(10) DEFAULT NULL,
  `model_id` bigint(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `vehicle_number` varchar(200) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL COMMENT '-1 => deleted, 1=>active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user_vehicle` */

insert  into `user_vehicle`(`id`,`user_id`,`make_id`,`model_id`,`year`,`vehicle_number`,`color`,`status`) values (1,2,2,5,'2020','atr555','red',1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `cnic_expiry_date` varchar(255) DEFAULT NULL,
  `user_type` int(8) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT current_timestamp(),
  `android_push_id` varchar(1024) DEFAULT NULL,
  `apple_push_id` varchar(1024) DEFAULT NULL,
  `profile_img` varchar(1024) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `num_of_login_attempts` int(2) unsigned DEFAULT NULL,
  `last_login_time` int(11) unsigned DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_new_user` enum('yes','no') DEFAULT 'yes',
  `last_change_password_date` datetime DEFAULT NULL,
  `credit_limit` varchar(255) DEFAULT NULL,
  `account_status` tinyint(2) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `shift_status` enum('offline','online') DEFAULT 'offline',
  `last_shift_update_date` datetime DEFAULT NULL,
  `user_rating` float(10,2) DEFAULT 0.00,
  `driver_booking_status` enum('pending','ontrip') DEFAULT 'pending',
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `last_location_updated_at` datetime DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`user_id`),
  KEY `login_id` (`login_id`),
  KEY `user_type` (`user_type`),
  KEY `first_name` (`full_name`),
  KEY `creation_date` (`creation_date`),
  KEY `user_type_2` (`user_type`),
  KEY `first_name_2` (`full_name`),
  KEY `creation_date_2` (`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`user_id`,`login_id`,`password`,`full_name`,`cnic`,`cnic_expiry_date`,`user_type`,`email_address`,`phone_number`,`gender`,`address`,`creation_date`,`android_push_id`,`apple_push_id`,`profile_img`,`status`,`num_of_login_attempts`,`last_login_time`,`comments`,`is_new_user`,`last_change_password_date`,`credit_limit`,`account_status`,`device_id`,`account_no`,`account_name`,`updated_at`,`shift_status`,`last_shift_update_date`,`user_rating`,`driver_booking_status`,`lat`,`long`,`last_location_updated_at`,`agent`,`is_verified`) values (1,'admin','4124bc0a9335c27f086f24ba207a4912','Tariq Khawaja','42101',NULL,1,NULL,'admin',NULL,NULL,'2019-07-13 10:24:36','',NULL,NULL,1,NULL,NULL,NULL,'no','2021-09-07 12:37:19',NULL,NULL,NULL,NULL,NULL,NULL,'offline',NULL,1.00,'pending','','',NULL,NULL,'no'),(2,'03002658202','4124bc0a9335c27f086f24ba207a4912','Tariq Driver','','',2,'tkhawaja@gmail.com','03002658202',NULL,'','2021-09-05 16:59:14','cbANfEbQRi2Y4wzPPNAO5c:APA91bE64GNKIIcOfuCuI8LWNhFlRm5eTbfZUFEk9vNOsdxRd__pfTl-1RiZDCM9HGcX9LLKjXjXfYGSXGJqdHH6Jdw_lwnPsKKcFjdIDTWCC7pSlX33Lf-AvJ9UfQswF0Nl3uueaY9r','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-09-05 22:03:27','online','2021-09-05 19:27:31',0.00,'pending','24.946103','67.061828','2021-09-05 19:29:11','15','yes'),(3,'tariq_kha@yahoo.com','4124bc0a9335c27f086f24ba207a4912','Tariq customerr',NULL,NULL,4,'tariq_kha@yahoo.com','03007042402',NULL,NULL,'2021-09-05 17:11:31','dtg2qYMbT3OBm_Jx3-JqJe:APA91bF-raM9rjiHBoAfjvPIIG4DsCI9tEN0svA-NPCTwZQclpg3gPK3Nmvuv4IczuDo4hsOdte6WbTRukSEHHtel5RlqkVgENawIcDMmA34YUYuEX2N3nc7zpXnTlKZzTvUo3lvqybg','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(4,'taha@yahoo.com','4124bc0a9335c27f086f24ba207a4912','driver',NULL,NULL,2,NULL,NULL,NULL,NULL,'2021-09-17 12:44:44',NULL,NULL,NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'online',NULL,0.00,'pending',NULL,NULL,NULL,'27','yes');

/*Table structure for table `users_25sep21` */

DROP TABLE IF EXISTS `users_25sep21`;

CREATE TABLE `users_25sep21` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  `cnic_expiry_date` varchar(255) DEFAULT NULL,
  `user_type` int(8) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT current_timestamp(),
  `android_push_id` varchar(1024) DEFAULT NULL,
  `apple_push_id` varchar(1024) DEFAULT NULL,
  `profile_img` varchar(1024) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `num_of_login_attempts` int(2) unsigned DEFAULT NULL,
  `last_login_time` int(11) unsigned DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_new_user` enum('yes','no') DEFAULT 'yes',
  `last_change_password_date` datetime DEFAULT NULL,
  `credit_limit` varchar(255) DEFAULT NULL,
  `account_status` tinyint(2) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `shift_status` enum('offline','online') DEFAULT 'offline',
  `last_shift_update_date` datetime DEFAULT NULL,
  `user_rating` float(10,2) DEFAULT 0.00,
  `driver_booking_status` enum('pending','ontrip') DEFAULT 'pending',
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `last_location_updated_at` datetime DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `is_verified` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`user_id`),
  KEY `login_id` (`login_id`),
  KEY `user_type` (`user_type`),
  KEY `first_name` (`full_name`),
  KEY `creation_date` (`creation_date`),
  KEY `user_type_2` (`user_type`),
  KEY `first_name_2` (`full_name`),
  KEY `creation_date_2` (`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1544 DEFAULT CHARSET=utf8;

/*Data for the table `users_25sep21` */

insert  into `users_25sep21`(`user_id`,`login_id`,`password`,`full_name`,`cnic`,`cnic_expiry_date`,`user_type`,`email_address`,`phone_number`,`gender`,`address`,`creation_date`,`android_push_id`,`apple_push_id`,`profile_img`,`status`,`num_of_login_attempts`,`last_login_time`,`comments`,`is_new_user`,`last_change_password_date`,`credit_limit`,`account_status`,`device_id`,`account_no`,`account_name`,`updated_at`,`shift_status`,`last_shift_update_date`,`user_rating`,`driver_booking_status`,`lat`,`long`,`last_location_updated_at`,`agent`,`is_verified`) values (1,'admin','1504c2428ca08ad75f808773dbd13715','Tariq Khawaja','42101',NULL,1,NULL,'admin',NULL,NULL,'2019-07-13 10:24:36','',NULL,NULL,1,NULL,NULL,NULL,'no','2020-06-23 12:37:19',NULL,NULL,NULL,NULL,NULL,NULL,'offline',NULL,1.00,'pending','','',NULL,NULL,'no'),(1417,'030026582021111','4124bc0a9335c27f086f24ba207a4912','tariq',NULL,NULL,2,'','030026582021',NULL,NULL,'2021-06-06 15:18:50','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-08-31 14:03:32',0.00,'pending','24.8657959','67.0811093','2021-08-31 14:03:26','6','yes'),(1418,'03123324458','3c189958921813530a43ced12f182135','ASADULLAH','','',2,'asad@gmail.com','03123324458',NULL,'','2021-06-07 08:56:34','','',NULL,-1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-06-08 11:23:47','online','2021-06-08 21:28:21',0.00,'pending','27.7027937','68.8463836','2021-06-08 21:31:17','7','yes'),(1419,'asad@gmail.com','3c189958921813530a43ced12f182135','ASAD',NULL,NULL,4,'asad@gmail.com','03003151264',NULL,NULL,'2021-06-07 14:18:31','','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1420,'!@#QWE123','4aa9f59b6e7812556c64bb8235433cff','!@#QWE123','!@#QWE123','',4,'!@#QWE123','!@#QWE123',NULL,'!@#QWE123','2021-06-10 11:33:15','','',NULL,-1,NULL,NULL,NULL,'yes',NULL,'!@#QWE123',0,NULL,'!@#QWE123','!@#QWE123','2021-06-17 14:19:32','offline',NULL,0.00,'pending',NULL,NULL,NULL,'16','no'),(1421,'~!@#$%123456qwertyu','b882fb61e97dbc1db1c9fa1777683d81','~!@#$%123456qwertyu','~!@#$%123456qwertyu','2025-11-18',2,'~!@#$%123456qwertyu','~!@#$%123456qwertyu',NULL,'~!@#$%123456qwertyu','2021-06-10 13:25:14','','',NULL,3,NULL,NULL,NULL,'yes',NULL,'~!@#$%123456qwertyu',0,NULL,'~!@#$%123456qwertyu','~!@#$%123456qwertyu','2021-06-17 14:18:18','offline',NULL,0.00,'pending',NULL,NULL,NULL,'7','yes'),(1422,'54515348464','e99a18c428cb38d5f260853678922e03','tty',NULL,NULL,2,'tty@gm.com','54515348464',NULL,NULL,'2021-06-10 13:28:23','','',NULL,-1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1423,'test31@gmail.com','4124bc0a9335c27f086f24ba207a4912','akber 31',NULL,NULL,2,'test31@gmail.com','923212294131',NULL,NULL,'2021-06-17 18:11:52','','456',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1424,'tariqkk@tk.com','4124bc0a9335c27f086f24ba207a4912','tariqdriver',NULL,NULL,2,'tariqkk@tk.com','66666666666',NULL,NULL,'2021-06-17 18:18:02','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'7','yes'),(1425,'aley12@gmail.com','e99a18c428cb38d5f260853678922e03','aley',NULL,NULL,2,'aley12@gmail.com','03122222222',NULL,NULL,'2021-06-17 18:18:18','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1426,'nagori97@gmail.com','e99a18c428cb38d5f260853678922e03','nagori',NULL,NULL,2,'nagori97@gmail.com','03111111111',NULL,NULL,'2021-06-17 18:22:42','dEwGN4M-R1qAnnkL89f_Xs:APA91bEVusnYNQpJ6IOShvQLwovvNz0ujkeAxf9kEvRxA45ejVwMFhdpj98EpdeJKqakohbTotaqmS65uOrpZZh9eSree3EZ3w495uC1mAbjA8qWVAF809BfepDLIlRz0s5G6SQlCOg-','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'online','2021-07-24 17:57:31',0.00,'pending','25.3735987','68.3559702','2021-07-24 17:57:33','15','yes'),(1427,'aassdd@aa@com','4124bc0a9335c27f086f24ba207a4912','aaa','','',2,'aassdd@aa@com','44444444444',NULL,'','2021-06-18 05:53:11','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-06-18 11:15:08','offline',NULL,0.00,'pending',NULL,NULL,NULL,'7','yes'),(1428,'uzairali@gmail.com','e99a18c428cb38d5f260853678922e03','uzair',NULL,NULL,2,'uzairali@gmail.com','03121212121',NULL,NULL,'2021-06-18 06:07:25','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1429,'dhh@gh.com','e99a18c428cb38d5f260853678922e03','dbhd',NULL,NULL,2,'dhh@gh.com','03121212121',NULL,NULL,'2021-06-18 06:10:31','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'7','yes'),(1430,'21212121212','4124bc0a9335c27f086f24ba207a4912','driver101',NULL,NULL,2,'driver101@aa.aa','21212121212',NULL,NULL,'2021-06-18 06:32:32','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-06-18 12:48:50',0.00,'pending','24.8658257','67.0810762','2021-06-18 12:48:52','7','yes'),(1431,'haris@aa.aa','4124bc0a9335c27f086f24ba207a4912','haris',NULL,NULL,4,'haris@aa.aa','99999999999',NULL,NULL,'2021-06-18 06:52:45','','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1432,'ajabbasiq@gmail.com','3c189958921813530a43ced12f182135','',NULL,NULL,4,'ajabbasiq@gmail.com','03322661333',NULL,'sukkur Road','2021-06-18 10:01:19','e3xBdkhAQO2RA5XPv6YBJk:APA91bFVrkTaD_fAUCaJea-xhjBnx-4ZW9JKvy_DUMBWMXAyqvcKPbCrPO_FWDxv3IrjdmE1c1xjvkS2l-G6wyM27LZC4eOzcnece0e4j2CY7gsPpxHgJu8bkOZXtF0LQR5Bzrk4WW-A','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-08-17 20:14:09',0.00,'pending','27.7028415','68.8463859','2021-08-17 20:14:11','21','yes'),(1433,'asadhyder@gmail.com','3c189958921813530a43ced12f182135','asadhyder',NULL,NULL,4,'asadhyder@gmail.com','03123322445',NULL,NULL,'2021-06-18 10:10:16','','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1434,'syedammar.alihashmi@gmail.com','237898d9c54c77a4f37f90cceae248ae','Syed Ammar Ali Hashmi',NULL,NULL,4,'syedammar.alihashmi@gmail.com','03030126915',NULL,NULL,'2021-06-18 10:12:52','','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1435,'ramal.hafeez1996@gmail.com','4124bc0a9335c27f086f24ba207a4912','test ramal',NULL,NULL,4,'ramal.hafeez1996@gmail.com','03323334906',NULL,NULL,'2021-06-18 10:13:41','','',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1436,'test12425655551@gmail.com','4124bc0a9335c27f086f24ba207a4912','akber 31',NULL,NULL,4,'test12425655551@gmail.com','923212294122',NULL,NULL,'2021-06-18 10:15:25','','456',NULL,2,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1437,'test124256555521@gmail.com','4124bc0a9335c27f086f24ba207a4912','akber 31',NULL,NULL,4,'test124256555521@gmail.com','9232122941122',NULL,NULL,'2021-06-18 10:23:19','','456',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1438,'03123322667','3c189958921813530a43ced12f182135','hyderasad',NULL,NULL,2,'hyderasad@gmail.com','03123322667',NULL,NULL,'2021-06-18 10:25:34','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1439,'03023131356','3c189958921813530a43ced12f182135','hydermagsi',NULL,NULL,2,'hydermagsi@gmail.com','03023131356',NULL,NULL,'2021-06-18 10:28:18','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1440,'9232122941312','4124bc0a9335c27f086f24ba207a4912','akber 31',NULL,NULL,2,'test312@gmai2l.com','9232122941312',NULL,NULL,'2021-06-18 10:28:57','','456',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1441,'asd@asd.com','4124bc0a9335c27f086f24ba207a4912','asdqw',NULL,NULL,4,'asd@asd.com','12121212121',NULL,NULL,'2021-06-18 10:31:03','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1442,'03000000000','3c189958921813530a43ced12f182135','AJ123',NULL,NULL,2,'aj123','03000000000',NULL,NULL,'2021-06-18 10:31:22','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1443,'asadullah@gmail.com','3c189958921813530a43ced12f182135','ASADULLAH',NULL,NULL,4,'asadullah@gmail.com','03001234567',NULL,NULL,'2021-06-18 10:34:33','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1444,'03001234567','3c189958921813530a43ced12f182135','asad123',NULL,NULL,2,'asad123@gmail.com','03001234567',NULL,NULL,'2021-06-18 10:36:10','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1445,'03006565651','3c189958921813530a43ced12f182135','hyder0300',NULL,NULL,2,'hyderasad@gmail.com','03006565651',NULL,NULL,'2021-06-18 10:41:48','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1446,'03003151662','3c189958921813530a43ced12f182135','ajabbasi','','',2,'ajabbasiz@gmail.com','03003151662',NULL,'','2021-06-18 10:47:20','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-06-18 16:03:44','offline','2021-06-19 12:30:11',0.00,'pending','27.7085781','68.843875','2021-06-19 12:30:05','15','yes'),(1447,'64748484848','4124bc0a9335c27f086f24ba207a4912','bdhdjdjdjd',NULL,NULL,2,'ggg@aa.asd','64748484848',NULL,NULL,'2021-06-18 10:49:38','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'7','yes'),(1448,'iamabbasi@gmail.com','3c189958921813530a43ced12f182135','imabbasi',NULL,NULL,4,'iamabbasi@gmail.com','03101368152',NULL,NULL,'2021-06-19 07:48:45','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1449,'03101368555','3c189958921813530a43ced12f182135','imabbasi',NULL,NULL,2,'imabbasi gmail.com','03101368555',NULL,NULL,'2021-06-19 07:49:44','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-06-19 09:53:35',0.00,'pending',NULL,NULL,NULL,'15','yes'),(1450,'ghouscarrent@gmail.com','3c189958921813530a43ced12f182135','Ghous Bux',NULL,NULL,4,'ghouscarrent@gmail.com','03123324477',NULL,NULL,'2021-06-19 10:26:14','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1451,'03101368425','6c7e120df929c77ba790d2a76878e388','WADOWAH','','',2,'wadowah@gmail.com','03101368425',NULL,'','2021-06-21 05:34:28','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-06-21 10:37:26','offline','2021-06-21 18:09:35',0.00,'pending','27.7086195','68.8439526','2021-06-21 18:09:29','15','yes'),(1452,'03003125700','def70a38d149edeb8da5c691974e443a','murshid rent a car',NULL,NULL,2,'nh0368729@gmail.com','03003125700',NULL,NULL,'2021-06-21 14:10:32','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1453,'74747483823','4124bc0a9335c27f086f24ba207a4912','test new driver',NULL,NULL,2,'tkhawaja@gmail.com','74747483823',NULL,NULL,'2021-06-22 11:30:44','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'12','yes'),(1454,'03162255464','3c189958921813530a43ced12f182135','farzand',NULL,NULL,2,'farzand@gmail.com','03162255464',NULL,NULL,'2021-06-22 11:47:02','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1455,'03101368100','3c189958921813530a43ced12f182135','Abdul Jabbar',NULL,NULL,2,'abbasi123@gmail.com','03101368100',NULL,NULL,'2021-06-22 12:03:57','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-06-22 18:09:29',0.00,'pending','27.7028135','68.8463454','2021-06-22 18:09:28','21','yes'),(1456,'03322661333','3c189958921813530a43ced12f182135','AJABBASI','','',2,'ajabbasi@gmail.com','03322661333',NULL,'','2021-06-22 17:10:31','','',NULL,-1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-10 13:23:30','offline',NULL,0.00,'pending',NULL,NULL,NULL,'15','no'),(1457,'abdul.basit.abbasi2011@gmail.com','e852a50739fb2d73782ad5aadfe1111a','a basit',NULL,NULL,4,'abdul.basit.abbasi2011@gmail.com','03128318994',NULL,NULL,'2021-06-23 15:27:37','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1458,'03343580830','047a5cb877325049da903c2b69a37d36','Naveed ',NULL,NULL,2,'nsalalani7668@gmail.com','03343580830',NULL,NULL,'2021-06-23 17:00:20','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1459,'03322905423','d2aced8ba4f107ca8026d5b07538b66b','ABBASI','455066906667','2021-06-30',2,'abbasi@gmail.com','03322905423',NULL,'SUKKUR ROAD','2021-06-24 07:42:02','','',NULL,-1,NULL,NULL,NULL,'yes',NULL,'5000',0,NULL,'03002123212','WAPSI','2021-07-10 13:22:10','offline','2021-07-09 19:18:39',0.00,'pending','27.7028204','68.8463905','2021-07-09 19:18:38','21','yes'),(1460,'faysal_mallick@live.com','be9f09589a46884ba8dd38b3c17a01ce','Faisal',NULL,NULL,4,'faysal_mallick@live.com','03203775189',NULL,NULL,'2021-06-24 13:13:23','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1461,'alisardar349@gmail.com','ac479cb3e854897c6416f26c5213f851','sardar ',NULL,NULL,4,'alisardar349@gmail.com','03153507920',NULL,NULL,'2021-06-25 08:47:03','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1462,'09264010852','486dccb55ba1906334f7c1ac8bce362f','John Doe',NULL,NULL,2,'teamguardiandummy1@gmail.com','09264010852',NULL,NULL,'2021-06-25 16:04:01','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1463,'03323164990','d5a92a68f9af953b26eafb935a054ce7','Fayaz Ali ',NULL,NULL,2,'sohoofayazali1973@gmail.com','03323164990',NULL,NULL,'2021-06-26 08:48:58','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1464,'03040387091','3c189958921813530a43ced12f182135','ALI','','',2,'alihyd@gmail.com','03040387091',NULL,'','2021-06-26 09:16:07','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-06-30 14:00:56','offline','2021-07-03 10:53:46',0.00,'pending','27.7085771','68.8438843','2021-07-03 10:53:43','15','yes'),(1465,'03218207584','035ce9d7715bb030d4e8f641c95e2cb9','danish',NULL,NULL,2,'danishsaify295@gmail.com','03218207584',NULL,NULL,'2021-06-26 10:52:47','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1466,'03213780724','d131b15dff44224b40f32484712a3c5a','muhammad akram',NULL,NULL,2,'muhammadakram212@gmail.com','03213780724',NULL,NULL,'2021-06-27 11:38:56','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1467,'03093891437','25f9e794323b453885f5181f1b624d0b','Noor Nabi',NULL,NULL,2,'Noor123456gmail.com','03093891437',NULL,NULL,'2021-06-30 05:50:02','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1468,'03337166817','354a7e1ee6f448a30c41a4ea39f1b4ce','yasir ali ',NULL,NULL,2,'yasir.alihtv@gmail.com','03337166817',NULL,NULL,'2021-06-30 05:52:09','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1469,'03153140624','25d55ad283aa400af464c76d713c07ad','',NULL,NULL,2,'','03153140624',NULL,'sukkur ','2021-06-30 06:14:48','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-06-30 08:41:43',0.00,'pending','27.6983928','68.8373894','2021-06-30 08:41:41','15','yes'),(1470,'03122895445','9a4672d7ef34da0ed678f69d92a2dc20','waqar Ali',NULL,NULL,2,'wa777948@gmail.com','03122895445',NULL,NULL,'2021-06-30 06:48:11','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1471,'03203728474','edb856f2feb6ef62971be75ae79ab4b9','Noor Nabi','','',2,'noor123@gmail.com','03203728474',NULL,'','2021-06-30 07:10:43','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-05 13:32:23','offline','2021-06-30 09:22:33',0.00,'pending','27.94983','68.6355733','2021-06-30 09:22:30','15','yes'),(1472,'03342630955','2a3c9517453b24b1fc0a53e082c4eb55','Nisar ',NULL,NULL,2,'muqadasjan@gmail.com','03342630955',NULL,NULL,'2021-06-30 07:21:23','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1473,'samiabbasi247@gmail.com','6c59220abc8a4fedeed80ab288cf0a44','Abdul Sami ',NULL,NULL,4,'samiabbasi247@gmail.com','03101340340',NULL,NULL,'2021-07-05 17:59:18','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1474,'03003937603','f3f766ac8f113f8b10df4dd305183f7e','Shaharyar khan',NULL,NULL,2,'shaharyarkhan075@gmail.com','03003937603',NULL,NULL,'2021-07-06 18:40:33','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1475,'03122366325','e99a18c428cb38d5f260853678922e03','abdullah',NULL,NULL,2,'ab97@gmail.com','03122366325',NULL,NULL,'2021-07-07 07:21:23','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1476,'ab@uhfsolutions.com','e99a18c428cb38d5f260853678922e03','abdullah',NULL,NULL,4,'ab@uhfsolutions.com','03122366666',NULL,NULL,'2021-07-07 07:34:59','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1477,'15895426656','2a028961df5c3c8ac7530dbb82db3d45','Eu ujffhjdgj',NULL,NULL,2,'utdhgsfjf@gmail.com','15895426656',NULL,NULL,'2021-07-07 07:54:31','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'online','2021-07-08 21:15:00',0.00,'pending','24.907077','67.0718823','2021-07-08 21:15:01','15','yes'),(1478,'03148476847','15aebb8e599d6d1b2dd518ecb60a5f6e','Muhammad Rehman','','',2,'rehmanmuhammad172@gmail.com','03148476847',NULL,'','2021-07-08 07:31:46','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-08 14:25:23','offline',NULL,0.00,'pending',NULL,NULL,NULL,'24','yes'),(1479,'03003108469','3c189958921813530a43ced12f182135','babo',NULL,NULL,2,'babo@gmail.com','03003108469',NULL,NULL,'2021-07-08 08:15:34','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-08-16 13:01:24',0.00,'pending','27.708638','68.8438815','2021-08-16 13:01:21','25','yes'),(1480,'03111111111','e99a18c428cb38d5f260853678922e03','sayani',NULL,NULL,2,'sayani86@uhf.com','03111111111',NULL,NULL,'2021-07-08 19:39:02','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1481,'03022068237','827ccb0eea8a706c4c34a16891f84e7b','Zafar Ali thebo',NULL,NULL,2,'','03022068237',NULL,NULL,'2021-07-09 11:52:26','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1482,'03008081601','926f3dcce72e8d19354053ecd17899bd','Ashique ali chhutto',NULL,NULL,2,'','03008081601',NULL,NULL,'2021-07-09 12:01:40','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1483,'03333759143','4297f44b13955235245b2497399d7a93','Ashique Ali ','','',2,'','03333759143',NULL,'','2021-07-09 12:09:45','fMt7lXFgQ3WOo2ILQ_SE-U:APA91bE8hLCVuJWyCJWw-TOAG8zl-RFg7Ad_5e0cfC-wY8Ed7fSf9Fzm0uXLovDFwnN7_VBbzXVjraa4tLK3CrY8phy51-WDkh9jL-WJIvcfZ2EYUN_9CLDXKNUtDzexjtInubSDP86Z','',NULL,2,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-15 14:48:20','online','2021-07-31 12:29:01',0.00,'pending','27.4412633','67.7799732','2021-08-01 04:20:57','15','yes'),(1484,'03440004661','de1ef62d441d7c4ee99a8085e006d49b','bakht Hussain solangi',NULL,NULL,2,'bakhtaamirali@gmail.com','03440004661',NULL,NULL,'2021-07-09 15:01:03','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1485,'farzand@gmail.com','3c189958921813530a43ced12f182135','farzand',NULL,NULL,4,'farzand@gmail.com','03322661333',NULL,NULL,'2021-07-11 15:32:01','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1486,'meeradnankhan1110@gmail.com','3c189958921813530a43ced12f182135','Adnan Ali',NULL,NULL,4,'meeradnankhan1110@gmail.com','03160258516',NULL,NULL,'2021-07-12 08:30:01','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1487,'tt@tt.tt','4124bc0a9335c27f086f24ba207a4912','tariqbcuatomer',NULL,NULL,4,'tt@tt.tt','03002658212',NULL,NULL,'2021-07-12 14:34:10','','bb',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1488,'ajabbasi@gmail.com','3c189958921813530a43ced12f182135','AJ ABBASI',NULL,NULL,4,'ajabbasi@gmail.com','03322661333',NULL,NULL,'2021-07-13 12:16:16','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1489,'03333333333','4124bc0a9335c27f086f24ba207a4912','Haris','','',2,'abc@gmail.com','03333333333',NULL,'','2021-07-13 12:19:49','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-13 17:56:38','offline','2021-07-14 10:05:05',0.00,'pending','24.8658334','67.0811243','2021-07-14 10:05:03','15','yes'),(1490,'abc@uhf.com','e99a18c428cb38d5f260853678922e03','ABDUL JABBAR ABBASI',NULL,NULL,4,'abc@uhf.com','03222222222',NULL,NULL,'2021-07-13 12:31:38','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1491,'03450900779','d3a228b06402aa01ee2d0a44ddf8917c','','','',2,'haris@gmail.con','03450900779',NULL,'abbasi Transport','2021-07-13 12:40:30','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-13 17:49:28','offline','2021-07-13 15:02:12',0.00,'pending','24.8658277','67.0811189','2021-07-13 15:02:14','21','yes'),(1492,'nagori122@gmail.com','4124bc0a9335c27f086f24ba207a4912','nagori',NULL,NULL,4,'nagori122@gmail.com','03122177241',NULL,NULL,'2021-07-13 12:43:14','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-07-27 10:25:27',0.00,'pending','24.8658316','67.0810862','2021-07-27 10:25:28',NULL,'no'),(1493,'03322661455','3c189958921813530a43ced12f182135','ABDUL JABBAR ABBASI',NULL,NULL,2,'jabbar@gmail.con','03322661455',NULL,NULL,'2021-07-13 13:16:53','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'15','yes'),(1494,'03313924742','3c189958921813530a43ced12f182135','ABDUL GHAFFAR',NULL,NULL,2,'abdulghaffar@gmail.com','03313924742',NULL,NULL,'2021-07-13 13:28:45','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'15','yes'),(1495,'99999999999','4124bc0a9335c27f086f24ba207a4912','tariqtestd1',NULL,NULL,2,'tariqtestd1','99999999999',NULL,NULL,'2021-07-14 07:27:18','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1496,'03555555555','e99a18c428cb38d5f260853678922e03','Talha',NULL,NULL,2,'talha@gmail.com','03555555555',NULL,NULL,'2021-07-14 08:00:57','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1497,'03332297262','e99a18c428cb38d5f260853678922e03','haris',NULL,NULL,2,'ali@gmail.com','03332297262',NULL,NULL,'2021-07-14 08:06:12','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1498,'09007860111','e99a18c428cb38d5f260853678922e03','nagori',NULL,NULL,2,'abc@gmail.com','09007860111',NULL,NULL,'2021-07-14 08:36:32','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1499,'03123324477','3c189958921813530a43ced12f182135','NISAR',NULL,NULL,2,'nisar@gmail.com','03123324477',NULL,NULL,'2021-07-14 09:03:57','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1500,'03313139431','e99a18c428cb38d5f260853678922e03','bsbsh',NULL,NULL,2,'bsb@gmail.com','03313139431',NULL,NULL,'2021-07-14 09:06:59','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1501,'09007860122','e99a18c428cb38d5f260853678922e03','abc123',NULL,NULL,2,'abc123@gmail.com','09007860122',NULL,NULL,'2021-07-14 09:10:01','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1502,'03461510935','e99a18c428cb38d5f260853678922e03','nagori',NULL,NULL,2,'nagori@gmail.com','03461510935',NULL,NULL,'2021-07-14 09:14:32','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1503,'12345678999','e99a18c428cb38d5f260853678922e03','vabsbsvv',NULL,NULL,2,'bsb@gh.com','12345678999',NULL,NULL,'2021-07-14 10:14:58','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1504,'22222222222','4124bc0a9335c27f086f24ba207a4912','vsvsb',NULL,NULL,2,'bsbbsb','22222222222',NULL,NULL,'2021-07-14 10:23:35','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1505,'03003731860','f3f766ac8f113f8b10df4dd305183f7e','SHABIR',NULL,NULL,2,'nh0368729@gmail.com','03003731860',NULL,NULL,'2021-07-14 16:25:49','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1506,'03123164308','153b714b600351b1146ae37892d8b2ee','farman',NULL,NULL,2,'farman@gmail.com','03123164308',NULL,NULL,'2021-07-14 16:30:13','foPFEtLoQk6EmJADh5KIjc:APA91bGJMGjlj3_S7egsYLGyDHq5gTmBimCBkuVUOKxZdC3OuIfDGT_lfdJeuPRlGTwrmt56gO_LERaw6Ykl8rTkQEeZy8ajfhhCUDWWyhyxnlfEqgZgrqIKXHENMn9aolxjspWa_1Wo','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-07-14 18:37:27',0.00,'pending','24.936663','67.1685299','2021-07-14 18:37:27','20','yes'),(1507,'11111111111','4124bc0a9335c27f086f24ba207a4912','gshdhhe',NULL,NULL,2,'hhh@gg.gg','11111111111',NULL,NULL,'2021-07-14 20:24:23','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1508,'03003256123','3c189958921813530a43ced12f182135','AMMAR','','',2,'ammar123@gmail.com','03003256123',NULL,'','2021-07-15 06:22:47','d1fORnFjT66YYuWkmcgtvR:APA91bF9-G4V5QeS65uyEq1o0eu0-fqKteGVJJjj7RYi-7ts88KLKi1nzhBjSoEl8CCnkz7QoC1RG9T7w39dNsDWgw9Ykg2kZR5SUh2guS4frCG8a3Sxm0lVrM6bSpiw-k0KVE5Gp7HP','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-15 14:38:37','offline','2021-09-03 06:35:57',0.00,'pending','27.7028232','68.8463092','2021-09-03 06:35:55','15','yes'),(1509,'ghousbuxcarrent@gmail.com','3c189958921813530a43ced12f182135','Ghous Bux Khoso',NULL,NULL,4,'ghousbuxcarrent@gmail.com','03008316977',NULL,NULL,'2021-07-15 09:52:27','dF57huLOSmmag04Y9JSKJP:APA91bGHIpP7RxaqksjQ4ZyXH9TNL_QiifIZPdXPGSmSY4EbWwvfwi-RzgEY5ZrXlVD-Dq1TN-1EG0rq6EWB-G9Ml0HnoNJTUZHXIuH8xg5NLgkXknOzYm32TKFdZhFU838kpdd5M1dG','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1510,'arj@gmail.com','3c189958921813530a43ced12f182135','ARJ',NULL,NULL,4,'arj@gmail.com','03123324478',NULL,NULL,'2021-07-16 06:20:10','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1511,'muhammadbuxpirzado123@gmail.com','8c249675aea6c3cbd91661bbae767ff1','MUHAMMAD BUX',NULL,NULL,4,'muhammadbuxpirzado123@gmail.com','03366238631',NULL,NULL,'2021-07-20 10:29:28','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1512,'03154313677','5cc160f80fdff0b47267f7ae2014fa75','Waseem Ahmed',NULL,NULL,2,'janiiiwaseem14@gmail.com','03154313677',NULL,NULL,'2021-07-24 12:25:00','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1513,'03003151663','3c189958921813530a43ced12f182135','GHAFFAR',NULL,NULL,2,'ghaffar@gmail.com','03003151663',NULL,NULL,'2021-07-25 16:28:47','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1514,'03205510451','a8cdd4f9ce6b0b3a1f18842fe173e985','','','',2,'desertworm120@gmail.com','03205510451',NULL,'PAF Base Faisal','2021-07-26 08:21:18','','',NULL,1,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-07-26 15:05:13','offline','2021-07-27 10:28:44',0.00,'pending','24.8658042','67.0810918','2021-07-27 10:28:46','15','yes'),(1515,'desertworm120@gmail.com','a8cdd4f9ce6b0b3a1f18842fe173e985','Fawwaz',NULL,NULL,4,'desertworm120@gmail.com','03205510451',NULL,NULL,'2021-07-26 08:38:00','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1516,'m.fawwaz.faisal@gmail.com','a8cdd4f9ce6b0b3a1f18842fe173e985','Fawwaz Faisal',NULL,NULL,4,'m.fawwaz.faisal@gmail.com','03205510450',NULL,NULL,'2021-07-26 08:50:42','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'online','2021-07-26 11:42:08',0.00,'pending','24.844985','66.9909883','2021-07-26 12:33:00',NULL,'no'),(1517,'tkhawaja@gmail.com','4124bc0a9335c27f086f24ba207a4912','Tariq customer',NULL,NULL,4,'tkhawaja@gmail.com','03002658201',NULL,NULL,'2021-07-27 08:20:39','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1518,'desertworm@gmail.com','4124bc0a9335c27f086f24ba207a4912','Mr Moron',NULL,NULL,4,'desertworm@gmail.com','09007896019',NULL,NULL,'2021-07-27 08:24:31','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1519,'03205510450','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,2,'desertworm120@gmail.com','03205510450',NULL,NULL,'2021-07-27 08:30:47','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1520,'03205510455','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,2,'desertworm120@gmail.com','03205510455',NULL,NULL,'2021-07-27 08:36:12','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1521,'wbhjkan@gmail.com','4124bc0a9335c27f086f24ba207a4912','Mr Moron',NULL,NULL,4,'wbhjkan@gmail.com','03205510453',NULL,NULL,'2021-07-27 08:45:04','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1522,'abcv123@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'abcv123@gmail.com','03157017228',NULL,NULL,'2021-07-27 11:11:23','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1523,'hgf@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'hgf@gmail.com','03157017228',NULL,NULL,'2021-07-27 11:27:27','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1524,'asdf@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'asdf@gmail.com','83451245667',NULL,NULL,'2021-07-27 11:44:52','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1525,'ashhhhdf@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'ashhhhdf@gmail.com','83451245667',NULL,NULL,'2021-07-27 11:45:18','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1526,'fffff@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'fffff@gmail.com','03157017228',NULL,NULL,'2021-07-27 12:00:01','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1527,'ffffhf@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'ffffhf@gmail.com','03157017228',NULL,NULL,'2021-07-27 12:01:17','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1528,'11111111112','e99a18c428cb38d5f260853678922e03','allay',NULL,NULL,2,'allay@gmi.com','11111111112',NULL,NULL,'2021-07-27 12:05:52','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'15','yes'),(1529,'kolo@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'kolo@gmail.com','03157017228',NULL,NULL,'2021-07-27 12:47:57','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1530,'lwyrup@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'lwyrup@gmail.com','03157017228',NULL,NULL,'2021-07-27 12:51:31','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1531,'faw@gmail.com','4124bc0a9335c27f086f24ba207a4912','Fawwaz',NULL,NULL,4,'faw@gmail.com','03157017228',NULL,NULL,'2021-07-27 12:55:43','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1532,'tkhawaja123@gmail.com','4124bc0a9335c27f086f24ba207a4912','Tariq ',NULL,NULL,4,'tkhawaja123@gmail.com','030026555558202',NULL,NULL,'2021-07-27 17:01:09','fDMGktknSPK-os8v6dA-XE:APA91bFfxm4Kxd7ex9KMqZNZ3qL9K5n7Kg1KTSMXL7oPuK0afjHBvgusZsFYMdKja-RB4lI_HZX6yL4-M57Z9cgTyQUSEa-A5fhexrfOVa4b3dZWdnaQ_LWfzzxmzZIsWPri5YYqGSiI','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline','2021-08-31 14:15:08',0.00,'pending','24.8657899','67.0811056','2021-08-31 14:15:07','15','yes'),(1533,'atswapsi1@gmail.com','3c189958921813530a43ced12f182135','ABDUL JABBAR',NULL,NULL,4,'atswapsi1@gmail.com','03123324477',NULL,NULL,'2021-07-27 17:05:00','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1534,'ahmed@gmail.com','3c189958921813530a43ced12f182135','AHMED ALI',NULL,NULL,4,'ahmed@gmail.com','03101368100',NULL,NULL,'2021-07-27 17:06:34','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1535,'+1919116886','827ccb0eea8a706c4c34a16891f84e7b','Kishan lal Ranaa','','',2,'kishanlalranaa@gmail.com','+1919116886',NULL,'','2021-08-02 19:49:23','','',NULL,4,NULL,NULL,NULL,'yes',NULL,'',0,NULL,'','','2021-08-06 13:24:54','offline',NULL,0.00,'pending',NULL,NULL,NULL,'Select agent','no'),(1536,'03013888011','4f033a0a2bf2fe0b68800a3079545cd1','Akbar',NULL,NULL,2,'Ali','03013888011',NULL,NULL,'2021-08-07 17:00:31','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1537,'ibetharianatoro@gmail.com@yahoo.com','67b2bcc9eb51853272baef86f8685b77','Deyanira Toro',NULL,NULL,4,'ibetharianatoro@gmail.com@yahoo.com','ibethariana',NULL,NULL,'2021-08-11 22:58:45','cX37Vx_jSpuWdPeLDnM5yI:APA91bFEfIGjks-onUBtEFJbMpf5fh-OgY0y4ktolnAUAr2smJbKSFLSfaO-MlbVF1Uu9-recLsCwRN0etvUyDmzzUAPFdde-lsszYQsx76Ck6agwXhvSbGCHaN1pvgwOrqmIdZJIejk','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1538,'03121231213','3c189958921813530a43ced12f182135','SAJID',NULL,NULL,2,'sajid@gmail.com','03121231213',NULL,NULL,'2021-08-12 11:49:20','','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,'15','yes'),(1539,'03003151478','3c189958921813530a43ced12f182135','Allah Bux',NULL,NULL,2,'allahbuxurfbabusukkur@gmail.com','03003151478',NULL,NULL,'2021-08-16 11:15:39','cv4M4FycRqG2cZZXAQQOVa:APA91bGmrK83GU4keNYQMshbDnpPIeze8j7De7mzDHNBrtA48NxyOoAxgpnoA-IQR1B4GXxgHoHqLuw6sDQIJJMyOwscfnuqh6K-GuJwGBzM3NLbMJgHRZ4JjgvEvSSYM999iRqHDlZH','',NULL,3,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'online','2021-09-05 17:11:31',0.00,'pending','27.6646181','68.8524296','2021-09-05 17:11:32','25','yes'),(1540,'Ibraraliabbasi824@gmail.com','29a1d1561a5ff71bc633e0166f050f39','shahzaib khan',NULL,NULL,4,'Ibraraliabbasi824@gmail.com','03113930519',NULL,NULL,'2021-08-21 16:42:52','ckP16V3MTaWT-uwj1xGLFO:APA91bGNUxYiDJmoZsiD5dbEyod_joyD9l-XE6XOcfG22XrHnwwnmWxYMUSjFoCb89fazA274XzDpFpxOjLnk_mDD_yWbnqcEsCXrvbtFpguNm2BGYX-xz1f1hheNGtP_y4cCG1X30Fb','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1541,'kha@kha.com','4124bc0a9335c27f086f24ba207a4912','khawaja',NULL,NULL,4,'kha@kha.com','03002658201',NULL,NULL,'2021-08-31 09:32:09','','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1542,'wahab.arshad01@gmail.com','e99a18c428cb38d5f260853678922e03','wahab',NULL,NULL,4,'wahab.arshad01@gmail.com','03413542009',NULL,NULL,'2021-08-31 09:41:57','crqDeK6pQ7aBJC3nvKl5Rd:APA91bGcAuaPmxyjeJIx_Cnvv-FEsutNcRBxP2aJIWDboyt7LCSpODsCg4C2VbjJwZKxAtjzOZQpMY-nLjzzVf-33WtNmlksp-FD_vfQ_cpZPs9FRHsF003a1mgowxr2cmcqBhn6d3nt','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'offline',NULL,0.00,'pending',NULL,NULL,NULL,NULL,'no'),(1543,'03002658202','4124bc0a9335c27f086f24ba207a4912','Tariq driver',NULL,NULL,2,'tkhawaja@gmail.com','03002658202',NULL,NULL,'2021-08-31 12:18:08','d0HOX6UwSNaB8macxInx26:APA91bHClyHFatEfefajBDooTzp1TGvNyJuQzZpsFEoA0vVwVbCq7udMDs86UB1PbtiXN4FCTohVcWf1njcau55IyMkHGzsAHIU-naayQOhRU-UTqx9V13f-hEl4RNvMMLn-DcA0H8Ip','',NULL,1,NULL,NULL,NULL,'yes',NULL,NULL,0,NULL,NULL,NULL,NULL,'online','2021-08-31 14:20:27',0.00,'pending','24.8657945','67.0811093','2021-08-31 14:20:24','15','yes');

/*Table structure for table `vehicle_make` */

DROP TABLE IF EXISTS `vehicle_make`;

CREATE TABLE `vehicle_make` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `status` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `vehicle_make` */

insert  into `vehicle_make`(`id`,`name`,`description`,`status`) values (7,'honda','honda city',1),(8,'honda','honda city 1',0),(9,'honda','CLI',1),(10,'TESLA','ELECTRIC ONE',1),(11,'Tesla','tesla',0),(12,'electric 23','tesla 123',0),(14,'baleno','suzuki beleno 2008',1),(15,'corolla ','2020',1);

/*Table structure for table `vehicle_model` */

DROP TABLE IF EXISTS `vehicle_model`;

CREATE TABLE `vehicle_model` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `make_id` bigint(10) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `car_type` enum('sedan','hatchback','suv') DEFAULT 'sedan',
  `status` tinyint(2) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `vehicle_model` */

insert  into `vehicle_model`(`id`,`make_id`,`model_name`,`description`,`car_type`,`status`) values (22,1,'cli','cli x','sedan',0),(23,1,'honda','accord 2009','sedan',0),(24,1,'honda','accord 2010','sedan',0),(25,0,'honda','hybird','sedan',0),(26,39,'SUZUKI','suzuki beleno 2005','sedan',0),(27,1,'accord','accord 2021','sedan',0),(28,39,'cutus','cultus','sedan',0),(29,37,'jimny','Generations: 4 ','sedan',0),(30,37,'Alto','alto','sedan',0),(32,7,'HONDA accord21','honda  cliX','suv',0),(33,11,'electric one','electric 1122','suv',1),(34,8,'accord ','accord 21','suv',1),(35,11,'tesla fire','electric fire','suv',1),(36,8,'honda','accord 2021','sedan',0),(37,11,'electric 23','electric 23','sedan',0),(38,11,'honda','honda 222','sedan',0),(39,8,'test model','hondaa','sedan',0),(40,12,'bhh','jkj','sedan',1),(41,13,'accord 2020','20210','suv',0),(42,14,'model 2010','modle 2010','sedan',0),(43,11,'electric 23','electric 23','suv',0),(44,15,'hybrid','hybrid','sedan',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
