<?
	require_once(MODULE . "/class/bllayer/settings.php");
	require_once(MODULE . "/class/bllayer/user.php");
	require_once(MODULE . "/class/bllayer/documents.php");
	require_once(MODULE . "/class/bllayer/vehicles.php");
	require_once(MODULE . "/class/bllayer/booking.php");
	require_once(MODULE . "/class/bllayer/addstuff.php");
	function login_customer($info)
    {
		
		traceMessage("Getting ID: ".$_REQUEST);
		
		//print_r($info);
		$loginid  =  $info['login_id'];
		$password  =  $info['password'];
		$androidId  =  $info['android_push_id'];
		$appleId  =  $info['apple_push_id'];
		
		$res = Authenticate($loginid,$password);// $blUser->AuhenticateUser($loginid,$loginid);
		
		//print_r_pre($res);
		if($res["code"] != 0)
		{
			
			$userInfo['user_id'] = $res["sid"]; 
			return array("success"=>false,"message"=>"Invalid credentials","data"=>null);
		}
		else{
			$sid = $res['sid'];
			$mysession =  new Session();
			$mysession->updatesession($sid);
			$userId = $res['user_id'];
			$blUser = new BL_User();
			
			
			
			//$userId = $mysession->getvalue("userid"); 
			$blUser = new BL_User();
			$userInfo = $blUser->GetUserInfo($userId);
			$userInfo->rows[0]['password']="";
			if( $userInfo->rows[0]['user_type'] !=USER_TYPE_CUSTOMER)
			{
				return array("success"=>false,"message"=>"Driver cannot login using this app","data"=>null);
			}
			
			
			$userArr = array();
			$userArr['android_push_id']=$androidId;
			$userArr['apple_push_id']= $appleId;
			$blUser->UpdateUserInfo($userArr,$userId);
			
			
			$bldocuments = new BL_Documents(); 
			$retObj = array();
			
			$userInfo->rows[0]['password']="";
			$userInfo->rows[0]['user_id']=$res["sid"];
			
			$retObj['userinfo'] = $userInfo->rows[0];
			
			
		}
		return array("success"=>true,"message"=>"Login Successfully","data"=>$retObj);
		
		
	}
	function logout_customer($info)
    {
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
			
		$userArr = array();
		$userArr['android_push_id']="";
		$userArr['apple_push_id']= "";
		$blUser->UpdateUserInfo($userArr,$userId);
		
		$mysession->closesession($sid);
		return array("success"=>true,"message"=>"Logout Successfully");
		
		
	}
	
	function register_customer($info){
		
		
		
		$name = $info['name'];
		$email = $info['email'];
		$mobile_no = $info['mobile_no'];
		$address = $info['address'];
		$password = $info['password'];
		$c_password = $info['c_password'];
		$androidId  =  $info['android_push_id'];
		$appleId  =  $info['apple_push_id'];
		
		$bluser = New BL_User();
		$bldocuments = new BL_Documents(); 
		$userCount = $bluser->CheckLoginId($info['email']);
		
		if($name == "" )
		return array("success"=>false,"message"=>"Name Cannot be empty","data"=>null);
		
		if($email == "" )
		return array("success"=>false,"message"=>"Email cannot be empty","data"=>null);
		
		if($password  == "" )
		return array("success"=>false,"message"=>"Password cannnot be empty","data"=>null);
		
		if($mobile_no  == "" )
		return array("success"=>false,"message"=>"Mobile cannnot be empty","data"=>null);
		
		$isUserExists = $bluser->CheckLoginId($email);
		
		if($isUserExists > 0)
		return array("success"=>false,"message"=>"Login Id Already Exists","data"=>null);
		
		
		//echo $isUserExists;
		//die();
		//if($password != $c_password)
		//  return array("success"=>false,"message"=>"Password doesnot match","data"=>null);
		
		$arr = array();
		$arr['login_id'] = $email;
		$arr['email_address'] = $email;
		$arr['full_name'] = $name;
		$arr['phone_number'] = $mobile_no;
		$arr['password'] = md5($password);
		$arr['status'] = 2; // 2 pending, 3 incompelete, 4 block, 1 active
		$arr['account_status'] = 0;
		$arr['user_type'] = USER_TYPE_CUSTOMER;
		$arr['android_push_id']=$androidId;
		$arr['apple_push_id']= $appleId;
		
		$userId = $bluser->AddUser($arr);
		
		
		$userInfo = $bluser->GetUserInfo($userId)->rows[0];
		
		//print_r_pre($userInfo);
		if($userId > 0){
			
			
			$res = Authenticate( $email,$password);
			$UserDocuments =  $bldocuments->GetUserDocuments($userId);
			
			
			$userInfo['user_id'] = $res["sid"]; 
			
			
			$userId = $res['user_id']; 
			$blUser = new BL_User();
			$userInfo = $blUser->GetUserInfo($userId);
			$userInfo->rows[0]['password']="";
			$userInfo->rows[0]['user_id']=$res["sid"];
			
			
			$bldocuments = new BL_Documents(); 
			$retObj = array();
			
			$retObj['userinfo'] = $userInfo->rows[0];
			
			
			return array("success"=>true,"message"=>"User Created Successfully","data"=>$retObj);
		}
		
	}
	function car_types(){
		
		
		
		$blvehicle = new BL_Vehicle();
		$info = $blvehicle->GetCarTypes();
		
		return array("success"=>true,"message"=>"car type listing","data"=>$info);
		
		}
	function city_available()
	{
		$sid = $_REQUEST['sid'];
		$fromCity = $_REQUEST['from_city'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		if($fromCity=="")
		{
			return array("success"=>false,"message"=>"Invalid source city","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		
		
		$bluser = New BL_User();
		$info = $bluser->GetAvailableCities($fromCity);
		
		return array("success"=>true,"message"=>"avaialble city listing","data"=>$info->rows);
		
		}
	function city_available_drivers()
	{
		$sid = $_REQUEST['sid'];
		$fromCity = $_REQUEST['from_city'];
		$toCity = $_REQUEST['to_city'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		if($fromCity=="")
		{
			return array("success"=>false,"message"=>"Invalid source city","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		
		
		$bluser = New BL_User();
		$info = $bluser->GetAvailableDriverForCity($fromCity,$toCity);
		
		if($info->count==0)
		{
			return array("success"=>false,"message"=>"no driver available for this route","data"=>array());
		}
		else
			return array("success"=>true,"message"=>"avaialble driver listing","data"=>$info->rows);
		
		}
		function change_password_customer()
		{
		
		$sid = $_REQUEST['sid'];
		$old = md5($_REQUEST['old_password']);
		$new = md5($_REQUEST['new_password']);
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		$existPass = $userInfo->rows[0]['password'];
		
		//echo "$existPass!=$old";
		if($existPass!=$old)
			return array("success"=>false,"message"=>"Old password is incorrect");
		
		
		$userArr = array();
		$userArr['password']=$new;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		return array("success"=>true,"message"=>"password change successfully");
	}
	function update_token_customer(){
		
		$sid = $_REQUEST['sid'];
		$androidId  =  $_REQUEST['android_push_id'];
		$type  =  $_REQUEST['type'];
		$token  =  $_REQUEST['token'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		
		$userArr = array();
		if($type=="android")
		$userArr['android_push_id']=$token;
		else 
		$userArr['apple_push_id']= $token;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		
		//echo $sid;
		
		
		
		
		return array("success"=>true,"message"=>"Token updated");
		
	}
	function create_booking(){
		
		$sid = $_REQUEST['sid'];
		$latitude=  $_REQUEST['lat'];
		$longitude=  $_REQUEST['long'];
		$destCity=  $_REQUEST['dest_city'];
		$driverId=  $_REQUEST['driver_id'];
		
		$destAddressInfo=getLatitudeLongitude($destCity." Pakistan");
		$destLatitude= $destAddressInfo['lat'];
		$destLongitude= $destAddressInfo['long'];
		$instruction= $destAddressInfo['instruction'];
		
		$distance =  distance($latitude, $longitude, $destLatitude, $destLongitude, 'K');
		
		 $distance=round($distance);
   traceMessage($destCity."--$latitude, $longitude, $destLatitude, $destLongitude=====$distance");
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
	   $tripRate = "3000";
	   $driverRate = "2000";
	   
	   $blUser = new BL_User();
	   
	   $driverInfo = $blUser->GetUserInfo($driverId);
	   $driverFCM = $driverInfo->rows[0]['android_push_id'];
	   //$driverFCM = "er_P544ER5Kv7lQhUqhud-:APA91bGvrGWejpMcWnhmQc2BXpchbZ8NjFqhl-8guafwT3kou9uS7fecC9eBakGLrZ2L_R75rFocEGe9Xk1hQshzJYj95OQEsqC0JWJlFOCeUwKfoezfVhjbdpFHSu4ORalYXH_hvmfI";
	   
	   $comm = $tripRate- $driverRate;
	  
		
	   $addArr = array();
	   $addArr['user_id'] = $userId;
	   $addArr['driver_id'] = $driverId;
	   $addArr['pickup_location'] = '';
	   $addArr['pickup_lat'] = $latitude;
	   $addArr['pickup_long'] = $longitude;
	   $addArr['approx_distance'] =$distance;
	   $addArr['dropoff_location'] = $destCity;
	   $addArr['price'] = $tripRate;;
	   $addArr['driver_rate'] = $driverRate;;
	   $addArr['comission'] = $comm;
	   $addArr['instructions'] = $instruction;
	   $addArr['status'] = "pending";
	   $addArr['created_at'] = date("Y-m-d H:i:s");
	   $addArr['updated_at'] = date("Y-m-d H:i:s");
	   
	   //print_r_pre($addArr);
		
	   
		$blBooking = new BL_Booking();
		$bookingId = $blBooking->AddTempBooking($addArr);
		
		$retArr= array();
	
		$retArr["title"]="You have new booking";
		$retArr["body"]="New booing has been awarded to you please respond";
		$retArr["obj_id"]=$bookingId;
		$retArr["not_type"]="booking";
		//$bookingData = $bookingInfo->rows[0];	
		
		$objId = $bookingId;
		$type = "booking";;
		$title = "You have new booking";;
		$body = "New booing has been awarded to you please respond";
		$fcmId = $driverFCM;
		
		$notRet  =SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId);
		
		
		return array("success"=>true,"message"=>"booking created","booking_id"=>$bookingId);
		
	}
	function get_booking_status(){
	
		$sid = $_REQUEST['sid'];
		$bookingId=  $_REQUEST['booking_id'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$bookingTime = $bookingInfo->rows[0]['created_at'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($driverId);
		
		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];
		$currentDate = date('Y-m-d H:i:s');
		
		
		
	
		
		$timeDiff = datetimeDifference($bookingTime , $currentDate , '%i' );
		
		//echo "D:$timeDiff";
		if($timeDiff>MAX_ACCEPT_TIME)
		{
			$status = "timeout";
			$comments = "driver didnt accept the booking in due time";
			$blBooking->RejectBooking($driverId,$status,$bookingId,$reason,$comments);
			$bookingInfo = $blBooking->GetBookingInfo($bookingId);
			$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
			$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
			$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];
			return array("success"=>true,"message"=>"Driver didnt accept the booking, please select another driver","data"=>$bookingInfo->rows);
		}
		$bookingStatus = $bookingInfo->rows[0]['status'];
		
		if($bookingStatus == "rejectbydriver")
		{
			$msg = "driver has rejected the booking, please select another driver";
			return array("success"=>true,"message"=>$msg,"data"=>$bookingInfo->rows);
		}
		
		else if($bookingStatus == "pending")
		{
			$msg = "driver status pending for approval";
			return array("success"=>true,"message"=>$msg,"data"=>$bookingInfo->rows);
		}
		else if($bookingStatus == "inprocess")
		{
			$msg = "driver accept the booking and will come to pick you shortly";
			return array("success"=>true,"message"=>$msg,"data"=>$bookingInfo->rows);
		}
		else if($bookingStatus == "completed")
		{
			$msg = "driver has complete the booking";
			return array("success"=>true,"message"=>$msg,"data"=>$bookingInfo->rows);
		}
		return array("success"=>false,"message"=>"something went wrong","status"=>'');
		
		
		/* ye cases honge nhi
			
		else if($bookingStatus == "rejectedbyclient")
		{
			$msg = "client has rejected the booking, please create another booking";
			return array("success"=>true,"message"=>$msg,"status"=>$bookingStatus);
		}
		else if($bookingStatus == "cancelbydriver")
		{
			$msg = "driver has rejected the booking after acceptance, please choose another driver";
			return array("success"=>true,"message"=>$msg,"status"=>$bookingStatus);
		}
		else if($bookingStatus == "cancelbyclient")
		{
			$msg = "client has rejected the booking after acceptance, please create new booking";
			return array("success"=>true,"message"=>$msg,"status"=>$bookingStatus);
		}	
		*/
	}
	function get_booking_invoice(){
	
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$tripStart = $bookingInfo->rows[0]['trip_start'];
		$tripEnd = $bookingInfo->rows[0]['trip_end'];
		// print_r_pre($bookingInfo);
		
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$info = array();
		$info['total_fare'] = $bookingInfo->rows[0]['price'];
		$info['total_distance'] = $bookingInfo->rows[0]["approx_distance"];
		$info['from_city'] = $bookingInfo->rows[0]['pickup_location'];
		$info['to_city'] = $bookingInfo->rows[0]['dropoff_location'];;
		$info['trip_fare'] = $bookingInfo->rows[0]['price']-$bookingInfo->rows[0]['comission'];;
		$info['ats_commission'] = $bookingInfo->rows[0]['comission'];;
		$info['total'] = $bookingInfo->rows[0]['price'];;
		
		return array("success"=>true,"message"=>"Trip ended","data"=>$info);
	}
	function driver_review(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$rating = $_REQUEST['rating'];
		$comments = $_REQUEST['comments'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
		
		return array("success"=>true,"message"=>"Rating Recorded");
		
	}
	function cancel_customer_rejection_reason(){
		
		$sid = $_REQUEST['sid'];
		
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
		
		$info = array();
		$info[0] = "Driver Not Arrived";
		$info[1] = "car issue";
		$info[2] = "Car is below standard";
		
		return array("success"=>true,"message"=>"Rejection Reasons","data"=>$info); 
		
	}
	function cancel_customer_booking(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$reason = $_REQUEST['reason'];
		$comments = $_REQUEST['comments'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$clientId = $bookingInfo->rows[0]['user_id'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		// print_r_pre($bookingInfo);
		if($currentStatus=="pending" || $currentStatus=="timeout")
		{
			if($clientId!=$userId)
				return array("success"=>false,"message"=>"This is not your booking"); 
		
			$blBooking->CancelBooking($driverId,'cancelbyclient',$bookingId,$reason,$comments);
		
		
			return array("success"=>true,"message"=>"booking has been calcelled"); 
		
		}
		else
			return array("success"=>false,"message"=>"you cannot cancel this booking:$currentStatus");
		
		
	}
	function active_ride_customer(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetCustomerActiveBooking($userId);
		
		$driverId = $bookingInfo->rows[0]['driver_id'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($driverId);
		
		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];
		
		
		return array("success"=>true,"message"=>"Booking Data","data"=>$bookingInfo->rows);
		
	}
	function get_booking_history_customer()
	{
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		
		
		$blUser = new BL_User();
		$ridesInfo = $blUser->GetUserRidesInfo($userId);
		
		$retArr = array();
		for($x=0;$x<$ridesInfo->count;$x++)
		{
			$retArr[$x]['booking_id'] = $ridesInfo->rows[$x]['record_id'];
			$retArr[$x]['driver_name'] = $ridesInfo->rows[$x]['full_name'];
			$retArr[$x]['pickup_location'] = $ridesInfo->rows[$x]['pickup_location'];
			$retArr[$x]['dropoff_location'] = $ridesInfo->rows[$x]['dropoff_location'];
			$retArr[$x]['approx_distance'] = $ridesInfo->rows[$x]['approx_distance'];
			$retArr[$x]['price'] = $ridesInfo->rows[$x]['price'];
			$retArr[$x]['booking_date'] = $ridesInfo->rows[$x]['created_at'];
			$retArr[$x]['rating'] = "3.5";
		}
		
		return array("success"=>true,"message"=>"Booking details","data"=>$retArr);
	}
	function get_customer_profile()
	{
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		unset($userInfo->rows[0]['password']);
		
		return array("success"=>true,"message"=>"Booking details","data"=>$userInfo->rows);
	}
	function update_customer_profile()
	{
		
		$sid = $_REQUEST['sid'];
		$cName = $_REQUEST['customer_name'];
		$address = $_REQUEST['address'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		
		$userArr = array();
		$userArr['full_name']=$cName;
		$userArr['address']= $address;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		$userInfo = $blUser->GetUserInfo($userId);
		unset($userInfo->rows[0]['password']);
		
		return array("success"=>true,"message"=>"User Data Updated","data"=>$userInfo->rows);
	}
	function get_notification_lists()
	{
		
		$sid = $_REQUEST['sid'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$notObject = $blUser->GetNotificationLists($userId);
		
		
		
		
		
		return array("success"=>true,"message"=>"$userId-Notification Data","data"=>$notObject->rows);
	}
	function mark_notification_as_read()
	{
		
		$sid = $_REQUEST['sid'];
		$notId = $_REQUEST['notification_id'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$notObject = $blUser->MarkAsRead($notId,$userId);
		
		
		
		
		
		return array("success"=>true,"message"=>"Notification mark as read");
	}
		
?>