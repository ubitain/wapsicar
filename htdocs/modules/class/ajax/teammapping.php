<?php
	require_once(MODULE . "/class/ajax/json.php");
	require_once(MODULE . "/class/bllayer/teammapping.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/process/user.php");

	function MappingTeam($parentId,$childId)
	{
		$arr = array();
		$arr['parent_id']=$parentId;
		$arr['child_id']=$childId;
		traceMessage("Ajax MappingTeam".print_r_log($arr));
		$blTeamMapping = new BL_TeamMapping();
		$responce = $blTeamMapping->MappingTeam($arr);
		traceMessage(" see response ".$responce);
		return $responce;
	}
	// team mapping view function under goes from here
	function ViewTeamMapping($parentId)
	{
		traceMessage("IN User AJAX".$parentId,$userType);
		$blTeamMapping = new BL_TeamMapping();
		$arr = $blTeamMapping->ViewTeamMapping($parentId,$userType=""); //done
		traceMessage("Result of ViewTeamMapping:  ".print_r_log($arr));
		$str = "";
		for($i=0;$i<$arr->count;$i++)
		{
		$str .= '<option value="'.$arr->rows[$i]["user_id"].'">'.username_style($arr->rows[$i]['first_name'],$arr->rows[$i]['last_name'],$arr->rows[$i]['emp_code']).'</option>';
		}
		return $str;
	}
	function ViewTeamMappingTable($parentId,$userType)
	{
		traceMessage("IN User AJAX".$parentId,$userType);
		$blTeamMapping = new BL_TeamMapping();
		$arr = $blTeamMapping->ViewTeamMapping($parentId,$userType); //done
		traceMessage("Result of ViewTeamMapping:  ".print_r_log($arr));
		$str = "";
		for($i=0;$i<$arr->count;$i++)
		{
		$str .= '<tr style="cursor:pointer" onClick=""><td>'.username_style($arr->rows[$i]['first_name'],$arr->rows[$i]['last_name'],$arr->rows[$i]['designation']).'</td><tr>';
		}
		if ($arr->count <= 0 ) {
			$str .= '<tr style="cursor:pointer" onClick=""><td>No data available</td><tr>';
		}
		return $str;
	}
	/******************************************SE Performance Report************************************************/
	function GetASMByTeamName($team)
	{
		traceMessage("IN User AJAX  ".$team);
		$blTeamMapping = new BL_TeamMapping();


		$arr = $blTeamMapping->GetASMByTeamName($team); //done
		// exit;
		traceMessage(" Result of GetASMByTeamName:  ".print_r_log($arr));
		// exit;

		//traceMessage("******************** for Arr ****");
		$str = "<select class='form-control'  name ='asm' id='asm' onchange='GetASMTeamBySE(this.value)' >";

		$str .= "<option value=''>Select Area Sales Manager </option>";

		for($i=0;$i<$arr->count;$i++)
		{
			//traceMessage("Tayyab");
			//traceMessage("******************** for Arr ****");
		$str .= '<option value="'.$arr->rows[$i]["user_id"].'">'.$arr->rows[$i]["first_name"].' - '.$arr->rows[$i]['city_name'].'</option>';
		}
		$str .= "</select>";
		// traceMessage("aaaa".$str);
		return $str;
	}
	?>
