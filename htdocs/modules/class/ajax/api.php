<?php
	require_once(MODULE . "/class/bllayer/settings.php");
	require_once(MODULE . "/class/bllayer/user.php");
	require_once(MODULE . "/class/bllayer/documents.php");
	require_once(MODULE . "/class/bllayer/vehicles.php");
	require_once(MODULE . "/class/bllayer/booking.php");
	require_once(MODULE . "/class/bllayer/addstuff.php");
	require_once(MODULE . "/class/bllayer/payments.php");
	function login($info)
    {
		
		traceMessage("Getting ID: ".$_REQUEST);

		
		//print_r($info);
		$loginid  =  $info['login_id'];
		$password  =  $info['password'];
		$androidId  =  $info['android_push_id'];
		$appleId  =  $info['apple_push_id'];
		
		$res = Authenticate($loginid,$password);// $blUser->AuhenticateUser($loginid,$loginid);
		
		// print_r_pre($res);
		if($res["code"] != 0)
		{
			 
			$userInfo['user_id'] = $res["sid"]; 
			return array("success"=>false,"message"=>"Invalid credentials","data"=>null);
		}
		else{
			$sid = $res['sid'];
			$mysession =  new Session();
			$mysession->updatesession($sid);
			$userId = $res['user_id'];
			$blUser = new BL_User();
			
			$userArr = array();
			$userArr['android_push_id']=$androidId;
			$userArr['apple_push_id']= $appleId;
			$userArr['shift_status']= 'offline';
			$blUser->UpdateUserInfo($userArr,$userId);
			/*
				
				$userInfo = $blUser->GetUserInfo($user_id)->rows[0];
				$userInfo['sid'] = $sid;
				$userInfo['user_id'] = $sid;
				
				$bldocuments = new BL_Documents(); 
				
				$userDocuments =  $bldocuments->GetUserDocuments($res["user_id"]);
				
				//print_r_pre($userDocuments,"aa");
				$userInfo['user_documents'] = $userDocuments; 
				
			return array("success"=>true,"message"=>"login successfull","data"=>$userInfo);*/
			
			//$userId = $mysession->getvalue("userid"); 
			$blUser = new BL_User();
			$userInfo = $blUser->GetUserInfo($userId);
			$userInfo->rows[0]['password']="";
			
			$bldocuments = new BL_Documents(); 
			$retObj = array();
			
			$userInfo->rows[0]['password']="";
			$userInfo->rows[0]['user_id']=$res["sid"];
			
			$retObj['userinfo'] = $userInfo->rows[0];
			//$retObj['agent_verified'] = $userInfo->rows[0]['is_verified'];
			
			$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
			
			if(count($cnicInfo[0])>0)
			{
				$retObj['cnic']=$cnicInfo[0];
				$retObj['cnic'][0]['files']=$cnicInfo[1];
			}
			
			$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
			
			if(count($licenseInfo[0])>0)
			{
				$retObj['license']=$licenseInfo[0];
				$retObj['license'][0]['files']=$licenseInfo[1];
			}
			
			$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
			
			if(count($runningInfo[0])>0)
			{
				$retObj['running']=$runningInfo[0];
				$retObj['running'][0]['files']=$runningInfo[1];
			}
			
			$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
			
			if(count($taxInfo[0])>0)
			{
				
				$retObj['tax']=$taxInfo[0];
				$retObj['tax'][0]['files']=$taxInfo[1];
			}
			$blVehicle =  new BL_Vehicle();
			$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
			
			if(count($vehicleInfo)>0)
			$retObj['vehicle']=$vehicleInfo[0];
		}
		return array("success"=>true,"message"=>"Login Successfully","data"=>$retObj);
		
		
	}
	function logout($info)
    {
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
			
		$userArr = array();
		$userArr['android_push_id']="";
		$userArr['apple_push_id']= "";
		$userArr['shift_status']= 'offline';
		$blUser->UpdateUserInfo($userArr,$userId);
		
		$mysession->closesession($sid);
		return array("success"=>true,"message"=>"Logout Successfully");
		
		
	}
	
	function register_driver($info){
		
		
		
		$name = $info['name'];
		$email = $info['email'];
		$mobile_no = $info['mobile_no'];
		$address = $info['address'];
		$password = $info['password'];
		$c_password = $info['c_password'];
		$androidId  =  $info['android_push_id'];
		$appleId  =  $info['apple_push_id'];
		
		$bluser = New BL_User();
		$bldocuments = new BL_Documents(); 
		$userCount = $bluser->CheckLoginId($info['mobile_no']);
		
		if($name == "" )
		return array("success"=>false,"message"=>"Name Cannot be empty","data"=>null);
		
		/*if($email == "" )
		return array("success"=>false,"message"=>"Email cannot be empty","data"=>null);
		*/
		if($password  == "" )
		return array("success"=>false,"message"=>"Password cannnot be empty","data"=>null);
		
		if($mobile_no  == "" )
		return array("success"=>false,"message"=>"Mobile cannnot be empty","data"=>null);
		
		$isUserExists = $bluser->CheckLoginId($mobile_no);
		
		if($isUserExists > 0)
		return array("success"=>false,"message"=>"Login Id Already Exists","data"=>null);
		
		
		//echo $isUserExists;
		//die();
		//if($password != $c_password)
		//  return array("success"=>false,"message"=>"Password doesnot match","data"=>null);
		
		$arr = array();
		$arr['login_id'] = $mobile_no;
		$arr['email_address'] = $email;
		$arr['full_name'] = $name;
		$arr['phone_number'] = $mobile_no;
		$arr['password'] = md5($password);
		$arr['status'] = 3; // 2 pending, 3 incompelete, 4 block, 1 active
		$arr['account_status'] = '0';
		$arr['user_type'] = 2;
		$arr['android_push_id']=$androidId;
		$arr['apple_push_id']= $appleId;
		
		$userId = $bluser->AddUser($arr);
		
		
		$userInfo = $bluser->GetUserInfo($userId)->rows[0];
		
		//print_r_pre($userInfo);
		if($userId > 0){
			
			
			$res = Authenticate( $mobile_no,$password);
			$UserDocuments =  $bldocuments->GetUserDocuments($userId);
			
			
			$userInfo['user_documents'] = $UserDocuments; 
			$userInfo['user_id'] = $res["sid"]; 
			
			
			$userId = $res['user_id']; 
			$blUser = new BL_User();
			$userInfo = $blUser->GetUserInfo($userId);
			$userInfo->rows[0]['password']="";
			$userInfo->rows[0]['user_id']=$res["sid"];
			
			
			$bldocuments = new BL_Documents(); 
			$retObj = array();
			
			$retObj['userinfo'] = $userInfo->rows[0];
			
			$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
			
			if(count($cnicInfo[0])>0)
			{
				$retObj['cnic']=$cnicInfo[0];
				$retObj['cnic'][0]['files']=$cnicInfo[1];
			}
			
			$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
			
			if(count($licenseInfo[0])>0)
			{
				$retObj['license']=$licenseInfo[0];
				$retObj['license'][0]['files']=$licenseInfo[1];
			}
			
			$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
			
			if(count($runningInfo[0])>0)
			{
				$retObj['running']=$runningInfo[0];
				$retObj['running'][0]['files']=$runningInfo[1];
			}
			
			$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
			
			if(count($taxInfo[0])>0)
			{
				
				$retObj['tax']=$taxInfo[0];
				$retObj['tax'][0]['files']=$taxInfo[1];
			}
			$blVehicle =  new BL_Vehicle();
			$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
			
			if(count($vehicleInfo)>0)
			$retObj['vehicle']=$vehicleInfo[0];
			
			
			
			
			
			
			
			return array("success"=>true,"message"=>"User Created Successfully","data"=>$retObj);
		}
		//print_r($userInfo);
		
	}
	
	
	function settings(){
		
		
		$blsettings = new BL_Settings();
		$data = $blsettings->GetSiteSettings();
		
		return array("success"=>true,"message"=>"Application Settings","data"=>$data);
	}
	
	
	function update_accounts(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$user_id = $mysession->getvalue("userid");
		
		
		
		
		$info = array();
		$info['account_no'] = $_REQUEST['account_number']; 
		$info['account_name'] = $_REQUEST['account_name']; 
		$info['status'] =2; 
		//$info['user_id'] = $_REQUEST['user_id'];
		
		$blUser = new BL_User();
		$blUser->UpdateUserInfo($info,$user_id);
		
		
		$userId = $mysession->getvalue("userid"); 
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		$userInfo->rows[0]['password']="";
		$userInfo->rows[0]['user_id']=$sid;
		
		
		$bldocuments = new BL_Documents(); 
		$retObj = array();
		
		$retObj['userinfo'] = $userInfo->rows[0];
		
		$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
		
		if(count($cnicInfo[0])>0)
		{
			$retObj['cnic']=$cnicInfo[0];
			$retObj['cnic'][0]['files']=$cnicInfo[1];
		}
		
		$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
		
		if(count($licenseInfo[0])>0)
		{
			$retObj['license']=$licenseInfo[0];
			$retObj['license'][0]['files']=$licenseInfo[1];
		}
		
		$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
		
		if(count($runningInfo[0])>0)
		{
			$retObj['running']=$runningInfo[0];
			$retObj['running'][0]['files']=$runningInfo[1];
		}
		
		$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
		
		if(count($taxInfo[0])>0)
		{
			
			$retObj['tax']=$taxInfo[0];
			$retObj['tax'][0]['files']=$taxInfo[1];
		}
		
		$blVehicle =  new BL_Vehicle();
		$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
		
		if(count($vehicleInfo)>0)
		$retObj['vehicle']=$vehicleInfo[0];
		//print_r_pre($vehicleInfo);
		return array("success"=>true,"message"=>"Updated Successfully","data"=>$retObj);
		
	}
	
	
	function document_listing(){
		
		$bldocuments = new BL_Documents();
		$listing = $bldocuments->GetDocumentListing();
		
		return array("success"=>true,"message"=>"Document listing","data"=>$listing);
		
		
	}
	
	
	function user_documents(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$user_id = $mysession->getvalue("userid");
		$bldocuments = new BL_Documents();
		$listing = $bldocuments->GetUserDocuments($user_id);
		
		return array("success"=>true,"message"=>"Document listing","data"=>$listing);
		
		
	}
	
	function upload_documents(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$user_id = $mysession->getvalue("userid");
		$document_id = $_REQUEST['document_id'];
		//print_r_pre($_FILES);
		//$files = count($_FILES['claim_image']['name']);
		traceMessage("files--------------".print_r_log($_FILES));
		$targetFolder = UPLOAD_DIRECTORY_PATH . "documents/";// . $folderId . "/";
		if (!is_dir($targetFolder)) {
			traceMessage("creating directory--By Date------:$targetFolder");
			mkdir($targetFolder);
		}
		$folderId = $user_id;
		$targetFolder = UPLOAD_DIRECTORY_PATH . "documents/" . $folderId . "/";
		$browsePath   = BROWSE_PATH . "documents/" . $folderId . "/";
		
		if (!is_dir($targetFolder)) {
			traceMessage("creating directory--By Date------:$targetFolder");
			mkdir($targetFolder);
		}
		
		for ($x =0 ; $x<count($_FILES['file']); $x++) {
			
			
			# code...
			$filetime = date("YmdHis");
			$targetFolder = $targetFolder . $filetime.basename($_FILES['file']['name'][$x]);
			$browsePath   = $browsePath . $filetime.basename($_FILES['file']['name'][$x]);
			$fileName     = $_FILES['file']['tmp_name'][$x];
			$name         = $_FILES['file']['name'][$x];
			$size         = $_FILES['file']['size'][$x];
			$type         = $_FILES['file']['type'][$x];
			if ($fileName != "") {
				traceMessage("coming");
				if (move_uploaded_file($fileName, $targetFolder)) {
					//save the filename
					traceMessage("success");
					
					$arr =array();
					$arr['user_id'] = $user_id;
					$arr['document_id'] = $_REQUEST['document_id'];
					$arr['document_path'] = $browsePath;
					$arr['status'] = "1";
					
					
					$blDocuments = new BL_Documents();
					$blDocuments->AddUserDocuments($arr);
					
					
					if($document_id == 1){
						
						$info =array();
						$info['user_id'] = $user_id;
						$info['document_id'] = $document_id;
						$info['name'] = $_REQUEST['name'];
						$info['number'] = $_REQUEST['cnic_number'];
						$info['expiry_date'] = $_REQUEST['expiry_date'];
						
						$blDocuments->AddUserDocumentInfo($info);
						}else if ($document_id == 2){
						traceMessage("222");
						$info =array();
						$info['user_id'] = $user_id;
						$info['document_id'] = $document_id;
						$info['name'] = $_REQUEST['name'];
						$info['number'] = $_REQUEST['license_no'];
						$info['expiry_date'] = $_REQUEST['expiry_date'];
						$blDocuments->AddUserDocumentInfo($info);
						}else if ($document_id == 3){
						
						$info =array();
						$info['user_id'] = $user_id;
						$info['document_id'] = $document_id;
						
						$blDocuments->AddUserDocumentInfo($info);
						}else if ($document_id == 4){
						
						$info =array();
						$info['user_id'] = $user_id;
						$info['document_id'] = $document_id;
						
						$blDocuments->AddUserDocumentInfo($info);
					}
					
					traceMessage("22244");
					//when file uploaded successully
					//$addClaimImageQuery="INSERT INTO `claim_images` (`claim_id`,`claim_image_url`,`status`) VALUES ('$claimId','$browsePath',1)";
					//$result2=mysqli_query($con,$addClaimImageQuery);
					} else {
					traceMessage("no success");
					//return array("success"=>false,"message"=>"unable to upload files","data"=>null);
					// $browsePath = "";
					//when file not uploaded successully
				}
			}
		}
		
		
		$userId = $mysession->getvalue("userid"); 
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		$userInfo->rows[0]['password']="";
		$userInfo->rows[0]['user_id']=$sid;
		
		$bldocuments = new BL_Documents(); 
		$retObj = array();
		
		$retObj['userinfo'] = $userInfo->rows[0];
		
		$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
		
		if(count($cnicInfo[0])>0)
		{
			$retObj['cnic']=$cnicInfo[0];
			$retObj['cnic'][0]['files']=$cnicInfo[1];
		}
		
		$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
		
		if(count($licenseInfo[0])>0)
		{
			$retObj['license']=$licenseInfo[0];
			$retObj['license'][0]['files']=$licenseInfo[1];
		}
		
		$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
		
		if(count($runningInfo[0])>0)
		{
			$retObj['running']=$runningInfo[0];
			$retObj['running'][0]['files']=$runningInfo[1];
		}
		
		$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
		
		if(count($taxInfo[0])>0)
		{
			
			$retObj['tax']=$taxInfo[0];
			$retObj['tax'][0]['files']=$taxInfo[1];
		}
		$blVehicle =  new BL_Vehicle();
		$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
		
		if(count($vehicleInfo)>0)
		$retObj['vehicle']=$vehicleInfo[0];
		
		
		return array("success"=>true,"message"=>"Document listing","data"=>$retObj);
		
		
		
	}
	
	
	function vehicle_make(){
		
		$blvehicle = new BL_Vehicle();
		$info = $blvehicle->getVehicleMake();
		
		return array("success"=>true,"message"=>"Vehicle make listing","data"=>$info);
	}
	
	function vehicle_model(){
		
		$make_id = $_REQUEST['make_id'];
		
		if($make_id  == "" )
		return array("success"=>false,"message"=>"Make Id cannnot be empty","data"=>null);
		
		$blvehicle = new BL_Vehicle();
		$info = $blvehicle->getVehicleModel($make_id);
		
		return array("success"=>true,"message"=>"Vehicle model listing","data"=>$info);
		
	}
	
	
	function add_vehicle_information(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$make_id = $_REQUEST['make_id'];
		$model_id = $_REQUEST['model_id'];
		$year = $_REQUEST['year'];
		$vehicle_number = $_REQUEST['vehicle_number'];
		
		
		$info = array();
		$info['make_id'] = $make_id;
		$info['model_id'] = $model_id;
		$info['year'] = $year;
		$info['vehicle_number'] = $vehicle_number;
		$info['color'] = $_REQUEST['color'];
		$info['status'] = 1;
		$info['user_id'] = $mysession->getvalue("userid");
		
		//print_r_pre($info);
		$blvehicle = new BL_Vehicle();
		$blvehicle->AddVehicleInformation($info);
		
		/*$userinfo = array();
			$userinfo['user_id'] =  $mysession->getvalue("userid");
			$userinfo['status'] = 2 ;
			$bluser = new BL_User();
		$bluser->UpdateUserInfo($userinfo,$userinfo['user_id']);*/
		
		
		$userId = $mysession->getvalue("userid"); 
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		$userInfo->rows[0]['password']="";
		$userInfo->rows[0]['user_id']=$sid;
		
		$bldocuments = new BL_Documents(); 
		$retObj = array();
		
		$retObj['userinfo'] = $userInfo->rows[0];
		
		$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
		
		if(count($cnicInfo[0])>0)
		{
			$retObj['cnic']=$cnicInfo[0];
			$retObj['cnic'][0]['files']=$cnicInfo[1];
		}
		
		$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
		
		if(count($licenseInfo[0])>0)
		{
			$retObj['license']=$licenseInfo[0];
			$retObj['license'][0]['files']=$licenseInfo[1];
		}
		
		$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
		
		if(count($runningInfo[0])>0)
		{
			$retObj['running']=$runningInfo[0];
			$retObj['running'][0]['files']=$runningInfo[1];
		}
		
		$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
		
		if(count($taxInfo[0])>0)
		{
			
			$retObj['tax']=$taxInfo[0];
			$retObj['tax'][0]['files']=$taxInfo[1];
		}
		$blVehicle =  new BL_Vehicle();
		$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
		
		if(count($vehicleInfo)>0)
		$retObj['vehicle']=$vehicleInfo[0];
		
		
		//echo "U:".count($runningInfo[0]);
		return array("success"=>true,"message"=>"Vehicle Information Updated","data"=>$retObj);
		
	}
	
	
	function driver_dashboard(){
		/**/
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		
		$userInfoObj = $blUser->GetUserInfo($userId);
		
		$info = array();
		
		/*
			if($userInfoObj->rows[0]['status']==2)
			$info['account_status'] = "Pending Approval";
			else if($userInfoObj->rows[0]['status']==1)
			$info['account_status'] = "Active";
			else if($userInfoObj->rows[0]['status']==3)
			$info['account_status'] = "InComplete";
			else if($userInfoObj->rows[0]['status']==4)
			$info['account_status'] = "Block";
		*/
		$blBooking = new BL_Booking();
		
		$pendingBooking = $blBooking->GetRiderPendingBooking($userId);
		$ret = $blBooking->GetDriverDashboard($userId);
		
		$info['earning'] = ($ret['total_earnings']!="")?$ret['total_earnings']:"0";
		$info['ats_commission'] = ($ret['total_comission']!="")?$ret['total_comission']:"0";
		$info['ats_dues'] = ($ret['total_comission']!="")?$ret['total_comission']:"0";
		$info['total_rides'] = ($ret['total_rides']!="")?$ret['total_rides']:"0";;
		
		$info['account_status'] = $userInfoObj->rows[0]['status'];
		$info['account_title'] = "ATS Account";
		$info['account_number'] = "032-4547841";
		$info['shift_status'] = $userInfoObj->rows[0]['shift_status'];
		if($pendingBooking->count>0)
		{
			$info['booking_id'] = $pendingBooking->rows[0]['record_id'];
		}
		traceMessage(print_r_log($info));
		return array("success"=>true,"message"=>"Dashboard Data","data"=>$info); 
		
	}
	function get_new_booking_info(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		if($bookingId<0 || trim($bookingId)=="")
		{
			return array("success"=>false,"message"=>"Invalid Booking Id","data"=>null);
		}
		
		if($bookingId=="100")
		{
			return array("success"=>false,"message"=>"This booking is already taken","data"=>null);
		}
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		
		/*
			$info = array();
			$info['from_location'] = "North Nazimabad";
			$info['to_location'] = "Hydrabad";
			$info['lat'] = "24.567108";
			$info['long'] = "67.967632";
			$info['booking_time'] = date("Y-m-d H:i:s");
		*/
		
		
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];
		
		$blUser = new BL_User();
		
		$userInfo = $blUser->GetUserInfo($driverId);
		
		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];
		
		$userInfo = $blUser->GetUserInfo($customerId);
		
		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		
		
		$info = array();
		$info['from_location'] = $bookingInfo->rows[0]['pickup_location'];
		$info['to_location'] = $bookingInfo->rows[0]['dropoff_location'];
		$info['lat'] = $bookingInfo->rows[0]['pickup_lat'];;
		$info['long'] = $bookingInfo->rows[0]['pickup_long'];
		$info['instructions'] = $bookingInfo->rows[0]['instructions'];
		$info['booking_time'] =date("Y-m-d H:i:s", strtotime("+1 hours")); //for now its fixed
		
		return array("success"=>true,"message"=>"Booking Data","data"=>$bookingInfo->rows); 
		
	}
	function UpdateShiftStatus(){
		
		$sid = $_REQUEST['sid'];
		$status = $_REQUEST['online_status'];
		$mysession = new Session();
		
		traceMessage("3333");
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			traceMessage("Invalid Session");	
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		traceMessage("3333444");
		$userId = $mysession->getvalue("userid");
		$blUser = new BL_User();
		traceMessage("3333555---$status---");
		if($status == "offline")
		{
			$blUser->UpdateShiftStatus($userId,$status);
			$msg = "Driver is now offline";
		}
		else if($status == "online")
		{
			$blUser->UpdateShiftStatus($userId,$status);
			$msg = "Driver is now online";
		}
		
		
		return array("success"=>true,"message"=>"$msg"); 
		
	}
	function get_rejection_reason(){
		
		$sid = $_REQUEST['sid'];
		
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
		
		$info = array();
		$info[0] = "not available";
		$info[1] = "car issue";
		$info[2] = "out of area";
		
		return array("success"=>true,"message"=>"Rejection Reasons","data"=>$info); 
		
	}
	function reject_booking(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$reason = $_REQUEST['reason'];
		$comments = $_REQUEST['comments'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		
		if($currentStatus!="pending")
		return array("success"=>false,"message"=>"you cannot reject this booking");
		
		if($driverId!=$userId)
		return array("success"=>false,"message"=>"This is not your booking"); 
		
		$blBooking->RejectBooking($driverId,$status,$bookingId,$reason,$comments);
		
		return array("success"=>true,"message"=>"booking has been rejected"); 
		
	}
	function cancel_rejection_reason(){
		
		$sid = $_REQUEST['sid'];
		
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
		
		$info = array();
		$info[0] = "customer not show";
		$info[1] = "car issue";
		$info[2] = "client issue";
		
		return array("success"=>true,"message"=>"Rejection Reasons","data"=>$info); 
		
	}
	function cancel_booking(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$reason = $_REQUEST['reason'];
		$comments = $_REQUEST['comments'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		// print_r_pre($bookingInfo);
		if($currentStatus=="cancelbyclient" || $currentStatus=="rejectedbyclient")
			return array("success"=>true,"message"=>"booking has been calcelled"); 
		if($currentStatus!="inprocess")
			return array("success"=>false,"message"=>"you cannot cancel this booking");
		
		if($driverId!=$userId)
		return array("success"=>false,"message"=>"This is not your booking"); 
		
		$blBooking->CancelBooking($driverId,'cancelbydriver',$bookingId,$reason,$comments);
		
		
		return array("success"=>true,"message"=>"booking has been calcelled"); 
		
	}
	function accept_booking(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$reason = $_REQUEST['reason'];
		$comments = $_REQUEST['comments'];
		$driverLat = $_REQUEST['driver_last_lat'];
		$driverLong = $_REQUEST['driver_last_long'];
		
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];
		$distance = $bookingInfo->rows[0]['approx_distance'];
		
		// echo "c:$customerId";
		
		if($currentStatus!="pending")
		return array("success"=>false,"message"=>"you cannot accept this booking");
		
		if($driverId!=$userId)
		return array("success"=>false,"message"=>"This is not your booking"); 
		
		$blBooking->AcceptBooking($driverId,$status,$bookingId,$driverLat,$driverLong);
		
		$info = array();
		$info['driver_booking_status'] = "ontrip";
		
		$blUser = new BL_User();
		$blUser->UpdateUserInfo($info,$driverId);
		
		
		/*$userInfo = $blUser->GetUserInfo($customerId);
		//print_r_pre($userInfo,"u");
		
		
		$info = array();
		$info["customer_name"] = $userInfo->rows[0]['full_name'];
		$info["customer_phone"] = $userInfo->rows[0]['phone_number'];;
		$info["cnic"] = $userInfo->rows[0]['cnic'];;
		$info["approx_distance"] = $distance;
		*/
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		//$bookingInfo->rows[0]['customer_cnic']=$userInfo->rows[0]['customer_cnic'];
		
		return array("success"=>true,"message"=>"booking has been accepted","data"=>$bookingInfo->rows); 
		
	}
	
	
	function set_user_status(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$user_id = $mysession->getvalue("userid");
		
		$info = array();
		$info['online_status'] = $_REQUEST['online_status'];
		
		$bluser = new BL_User();
		$bluser->UpdateUserInfo($info,$user_id);
		
		return array("success"=>true,"message"=>"User Online Status Updated","data"=>$info);
		
	}
	function start_trip(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$tripStart = $bookingInfo->rows[0]['trip_start'];
		$tripEnd = $bookingInfo->rows[0]['trip_end'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];

		// print_r_pre($bookingInfo);
		if($currentStatus!="inprocess")
		return array("success"=>false,"message"=>"you cannot start trip this booking","data"=>$bookingInfo->rows);
		if($currentStatus=="cancelbyclient")
			return array("success"=>false,"message"=>"ride has been cancelled by customer","data"=>$bookingInfo->rows);
		
		if($driverId!=$userId)
		return array("success"=>false,"message"=>"This is not your booking","data"=>$bookingInfo->rows); 
		
		if($tripStart=="yes")
		return array("success"=>false,"message"=>"Trip already started","data"=>$bookingInfo->rows); 
		
		$blBooking->StartTrip($driverId,$bookingId);
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		
		return array("success"=>true,"message"=>"trip has been started","data"=>$bookingInfo->rows); 
		
	}
	
	function end_trip(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$tripStart = $bookingInfo->rows[0]['trip_start'];
		$tripEnd = $bookingInfo->rows[0]['trip_end'];
		// print_r_pre($bookingInfo);
		if($currentStatus!="inprocess")
			return array("success"=>false,"message"=>"you cannot start trip this booking");
		
		if($driverId!=$userId)
			return array("success"=>false,"message"=>"This is not your booking"); 
		
		if($tripStart!="yes")
			return array("success"=>false,"message"=>"Trip is not started"); 
		
		if($tripEnd=="yes")
			return array("success"=>false,"message"=>"Trip is aready ended"); 
		
		$blBooking->Endrip($driverId,$bookingId);
		
		
		
		$blStuff = new BL_AddStuff();
		$retDel = $blStuff->DeleteRoutesRate($driverId);
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$customerUserId = $bookingInfo->rows[0]['user_id'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($customerUserId);
		
		$fcmId=$userInfo->rows[0]['android_push_id'];
		
		$objId = $bookingId;
		$type = "endride";;
		$title = "Ride has been finished";;
		$body = "Ride has been finished";
		
		
		$notRet  =SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$customerUserId,"driver");
		
		return array("success"=>true,"message"=>"trip has been finished"); 
		
	}
	function sos(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
		
		$info = array();
		$info['sos_number'] = "923002658202";
		
		return array("success"=>true,"message"=>"SOS Request generated","data"=>$info);
		
	}
	function complete_trip(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];

		$currentStatus = $bookingInfo->rows[0]['status'];
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$tripStart = $bookingInfo->rows[0]['trip_start'];
		$tripEnd = $bookingInfo->rows[0]['trip_end'];
		// print_r_pre($bookingInfo);
		if($currentStatus!="inprocess")
		return array("success"=>false,"message"=>"you cannot end trip this booking","data"=>$bookingInfo->rows);
		
		if($driverId!=$userId)
		return array("success"=>false,"message"=>"This is not your booking","data"=>$bookingInfo->rows); 
		
		if($tripStart!="yes")
		return array("success"=>false,"message"=>"Trip is not started","data"=>$bookingInfo->rows); 
		
		if($tripEnd=="yes")
		return array("success"=>false,"message"=>"Trip is aready ended","data"=>$bookingInfo->rows); 
		
		$blBooking->EndTrip($driverId,$bookingId);
		
		
		$info = array();
		$info['driver_booking_status'] = "pending";
		
		$blUser = new BL_User();
		$blUser->UpdateUserInfo($info,$driverId);
		
		// return array("success"=>true,"message"=>"trip has been started"); 
		
		
		/*from end_trip*/ 
		
		$blStuff = new BL_AddStuff();
		$retDel = $blStuff->DeleteRoutesRate($driverId);
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$customerUserId = $bookingInfo->rows[0]['user_id'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($customerUserId);
		
		$fcmId=$userInfo->rows[0]['android_push_id'];
		
		$objId = $bookingId;
		$type = "endride";;
		$title = "Ride has been finished";;
		$body = "Ride has been finished";
		
		
		$notRet  =SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$customerUserId,"driver");
		/* ********/
		
		
		
		
		
		
		
		
		
		
		
		
		
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		
		
		$info = array();
		$info['total_fare'] = $bookingInfo->rows[0]['total_fare'];
		$info['total_distance'] = $bookingInfo->rows[0]["approx_distance"];
		$info['from_city'] = $bookingInfo->rows[0]['pickup_location'];
		$info['to_city'] = $bookingInfo->rows[0]['dropoff_location'];;
		$info['trip_fare'] = $bookingInfo->rows[0]['total_fare']-$bookingInfo->rows[0]['comission'];;
		$info['ats_commission'] = $bookingInfo->rows[0]['comission'];;
		$info['total'] = $bookingInfo->rows[0]['total_fare'];;
		
		return array("success"=>true,"message"=>"Trip ended","data"=>$bookingInfo->rows);
		
	}
	
	function cash_collected(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
			
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		
		return array("success"=>true,"message"=>"Cash Collected","data"=>$bookingInfo->rows);
		
	}
	function customer_review(){
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$rating = $_REQUEST['rating'];
		$comments = $_REQUEST['comments'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
			
		$driverId = $bookingInfo->rows[0]['driver_id'];
		$customerId = $bookingInfo->rows[0]['user_id'];

		$blUser = new BL_User();

		$userInfo = $blUser->GetUserInfo($driverId);

		$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

		$userInfo = $blUser->GetUserInfo($customerId);

		$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
		$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
		$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		
		
		return array("success"=>true,"message"=>"Rating Recorded","data"=>$bookingInfo->rows);
		
	}
	
	function update_token(){
		
		$sid = $_REQUEST['sid'];
		$androidId  =  $_REQUEST['android_push_id'];
		$type  =  $_REQUEST['type'];
		$token  =  $_REQUEST['token'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		
		$userArr = array();
		if($type=="android")
		$userArr['android_push_id']=$token;
		else 
		$userArr['apple_push_id']= $token;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		
		//echo $sid;
		
		
		
		
		return array("success"=>true,"message"=>"Token updated");
		
	}
	function get_user_info(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
			$blUser = new BL_User();
			$userInfo = $blUser->GetUserInfo($userId);
			$userInfo->rows[0]['password']="";
			
			$bldocuments = new BL_Documents(); 
			$retObj = array();
			
			$userInfo->rows[0]['password']="";
			$userInfo->rows[0]['user_id']=$sid;
			
			$retObj['userinfo'] = $userInfo->rows[0]; 
			
			$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
			
			if(count($cnicInfo[0])>0)
			{
				$retObj['cnic']=$cnicInfo[0];
				$retObj['cnic'][0]['files']=$cnicInfo[1];
			}
			
			$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
			
			if(count($licenseInfo[0])>0)
			{
				$retObj['license']=$licenseInfo[0];
				$retObj['license'][0]['files']=$licenseInfo[1];
			}
			
			$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
			
			if(count($runningInfo[0])>0)
			{
				$retObj['running']=$runningInfo[0];
				$retObj['running'][0]['files']=$runningInfo[1];
			}
			
			$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
			
			if(count($taxInfo[0])>0)
			{
				
				$retObj['tax']=$taxInfo[0];
				$retObj['tax'][0]['files']=$taxInfo[1];
			}
			$blVehicle =  new BL_Vehicle();
			$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
			
			if(count($vehicleInfo)>0)
			$retObj['vehicle']=$vehicleInfo[0];
		
		return array("success"=>true,"message"=>"User Info","data"=>$retObj);
		
	}
	function get_city_routes(){
		
		$sid = $_REQUEST['sid'];
		$city = $_REQUEST['city'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$blStuff = new BL_AddStuff();
		
		$routesArr = $blStuff->GetCityRoutes($city);
		
		if($routesArr->count==0)
		{
			return array("success"=>false,"message"=>"No route exist","data"=>array());
		}
		
		$retObj['routes'] = $routesArr->rows;
		//print_r_pre($cityExisist);
		return array("success"=>true,"message"=>"User Info","data"=>$retObj);
		
	}
	function get_driver_routes(){
		traceMessage('get_driver_routes');
		
		$sid = $_REQUEST['sid'];
		$city = $_REQUEST['city'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>" Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$blStuff = new BL_AddStuff();
		
		$routesArr = $blUser->GetCityRateCard($userId);
		
		if($routesArr->count==0)
		{
			$retObj =array();
			$retObj['routes'] = array();
			return array("success"=>true,"message"=>"No route exist","data"=>$retObj); 
		}	
		
		$retObj['routes'] = $routesArr->rows;
		//print_r_pre($cityExisist);
		return array("success"=>true,"message"=>"User Info","data"=>$retObj);
		
	}
	function add_city_routes(){
		
		$sid = $_REQUEST['sid'];
		$routeStr = file_get_contents('php://input');
		$routeArr = JSON_DECODE($routeStr)->routes;
		
		$mysession = new Session(); 
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session");
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		//print_r_pre($_REQUEST);
		$blUser = new BL_User();
		$blStuff = new BL_AddStuff();
		
		$retDel = $blStuff->DeleteRoutesRate($userId);
		
		for($x=0;$x<count($routeArr);$x++)
		{
			$routeId = $routeArr[$x]->route_id;
			$routeFare = $routeArr[$x]->fare;
			$routesDetail = $blStuff->GetRouteById($routeId);
			$orignalRate = str_replace(",","",$routesDetail->rows[0]['rates']);
			$comm = $orignalRate-$routeFare;
			//print_r_pre($routesDetail,"aa");
			traceMessage("$orignalRate--$routeFare");
			if($comm<0) 
			{
				return array("success"=>false,"message"=>"your rate should be atleast equal to the orignal rate");
			}
			
			if($routeFare%500!=0)
			{
				return array("success"=>false,"message"=>"your rate should in multiple of 500");
			}
			$ret = $blStuff->AddRoutesRate($userId,$routeId,$routeFare,$comm);
		}
		
		//print_r_pre($cityExisist);
		return array("success"=>true,"message"=>"City Route Added");
		
	}
	
	function add_driver_payment()
	{
		$arr =array();
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		$blDocuments = new BL_User();
		$blpayment = new BL_Payments();
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		$transactionNumber=$_REQUEST['transaction_number'];
		$blData=$blDocuments->SelectDriverPayment($transactionNumber);
		$id=$blData->rows[0]['id'];
		$payment=$blData->rows[0]['payment'];
		$transactionNumbers=$blData->rows[0]['transaction_number'];
		$status=$blData->rows[0]['status'];
		traceMessage("dromdb".print_r_log($status));
	
		
		if($payment == $_REQUEST['payment'] && $transactionNumbers == $_REQUEST['transaction_number'])
		{
		$arr['driver_id'] = $userId;
		// $arr['payment'] = $_REQUEST['payment'];
		// $arr['transaction_number'] = $_REQUEST['transaction_number'];
		$arr['description'] = $_REQUEST['description'];
		// $arr['created_at'] = date("Y-m-d H:i:s");
		$arr['status'] = 1;
		
		
		$blDocuments->UpdateDriverPayment($arr,$id);
		
		return array("success"=>true,"message"=>"Payment added successfully");
		}
		else if($_REQUEST['transaction_number'] != $transactionNumbers)
		{
		traceMessage("fromapp".$_REQUEST['transaction_number']);
		traceMessage("dromdb".$transactionNumbers);
		$arr['driver_id'] = $userId;
		$arr['payment'] = $_REQUEST['payment'];
		$arr['transaction_number'] = $_REQUEST['transaction_number'];
		$arr['description'] = $_REQUEST['description'];
		$arr['created_at'] = date("Y-m-d H:i:s");
		$arr['status'] = 2;
		$blDocuments->AddDriverPayment($arr);
		
		return array("success"=>true,"message"=>"Payment added successfully");
		}

		else if($_REQUEST['transaction_number'] == $transactionNumbers && $payment != $_REQUEST['payment'] && $status == '0' ){
			
			 $difference = $payment-$_REQUEST['payment'];
            $transactionInfo = $blpayment->UpdateTransactionStatus($transactionNumbers,"3", $difference);
             $transactionInfo = $blpayment->GetPaymentByTransaction($_REQUEST['transaction_number']);
              traceMessage("transactionInfo".print_r_log($transactionInfo));
             $arr =array();
            $arr['driver_id'] = $userId;
            $arr['payment'] = $_REQUEST['payment'];
            $arr['transaction_number'] = $transactionInfo[0]['transaction_number'];
            $arr['created_at'] = date("Y-m-d H:i:s");
            $arr['status'] = '1';
             traceMessage("1st".print_r_log($arr));
              $blDocuments = new BL_User();
            $blDocuments->AddDriverPayment($arr);
			$blpayment->UpdateTransaction($transactionNumbers , $userId);
            return array("success"=>true,"message"=>"Payment added successfully");
		}
		return array("success"=>true,"message"=>"Already Exists");
		
	}
	function get_driver_payment()
	{
		
		$sid = $_REQUEST['sid'];
		$fromDate = $_REQUEST['from_date'];
		$toDate = $_REQUEST['to_date'];
		$mysession = new Session();
		//print_r_pre($_REQUEST);
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		$blUser = new BL_User();
		$paymentInfo = $blUser->GetPaymentInfo($userId,$fromDate,$toDate);
		
		return array("success"=>true,"message"=>"Payment details","data"=>$paymentInfo->rows);
		
	}
	function delete_driver_payment()
	{
		
		$sid = $_REQUEST['sid'];
		$paymentId = $_REQUEST['payment_id'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
	
		$blUser = new BL_User();
		$paymentInfo = $blUser->GetPaymentInfoById($userId,$paymentId);
		//print_r_pre($paymentInfo);
		if($paymentInfo->rows[0]['status']=="1")
			return array("success"=>false,"message"=>"Payment already approved");
		
		$blUser->DeletePaymentInfo($userId,$paymentId);
		
		return array("success"=>true,"message"=>"Payment deleted successfully");
		
	}
	function get_booking_history()
	{
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		
		
		$blUser = new BL_User();
		$ridesInfo = $blUser->GetDriverRidesInfo($userId);
		
		$retArr = array();
		for($x=0;$x<$ridesInfo->count;$x++)
		{
			$retArr[$x]['booking_id'] = $ridesInfo->rows[$x]['record_id'];
			$retArr[$x]['customer_name'] = $ridesInfo->rows[$x]['full_name'];
			$retArr[$x]['pickup_location'] = $ridesInfo->rows[$x]['pickup_location'];
			$retArr[$x]['dropoff_location'] = $ridesInfo->rows[$x]['dropoff_location'];
			$retArr[$x]['approx_distance'] = $ridesInfo->rows[$x]['approx_distance'];
			$retArr[$x]['price'] = $ridesInfo->rows[$x]['price'];
			$retArr[$x]['booking_date'] = $ridesInfo->rows[$x]['created_at'];
			$retArr[$x]['rating'] = "3.5";
		}
		
		return array("success"=>true,"message"=>"Booking details","data"=>$retArr);
	}
	function change_password()
	{
		
		$sid = $_REQUEST['sid'];
		$old = md5($_REQUEST['old_password']);
		$new = md5($_REQUEST['new_password']);
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		$existPass = $userInfo->rows[0]['password'];
		
		//echo "$existPass!=$old";
		if($existPass!=$old)
			return array("success"=>false,"message"=>"Old password is incorrect");
		
		
		$userArr = array();
		$userArr['password']=$new;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		return array("success"=>true,"message"=>"password change successfully");
	}
	function update_profile()
	{
		
		$sid = $_REQUEST['sid'];
		$fullName = $_REQUEST['full_name'];
		$email = $_REQUEST['email_address'];
		$address = $_REQUEST['address'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$userArr = array();
		$userArr['full_name']=$fullName;
		$userArr['email_address']=$email;
		$userArr['address']=$address;
		
		//print_r_pre($userArr,$userId);
		$blUser->UpdateUserInfo($userArr,$userId);
		
		$document_id = $_REQUEST['document_id'];
		
		//$files = count($_FILES['claim_image']['name']);
		traceMessage("files--------------".print_r_log($_FILES));
		$targetFolder = UPLOAD_DIRECTORY_PATH . "documents/";// . $folderId . "/";
		if (!is_dir($targetFolder)) {
			traceMessage("creating directory--By Date------:$targetFolder");
			mkdir($targetFolder);
		}
		$folderId = $userId;
		$targetFolder = UPLOAD_DIRECTORY_PATH . "documents/" . $folderId . "/";
		$browsePath   = BROWSE_PATH . "documents/" . $folderId . "/";
		
		if (!is_dir($targetFolder)) {
			traceMessage("creating directory--By Date------:$targetFolder");
			mkdir($targetFolder);
		}
		
		for ($x =0 ; $x<count($_FILES['file']); $x++) {
			
			
			# code...
			$filetime = date("YmdHis");
			$targetFolder = $targetFolder . $filetime.basename($_FILES['file']['name'][$x]);
			$browsePath   = $browsePath . $filetime.basename($_FILES['file']['name'][$x]);
			$fileName     = $_FILES['file']['tmp_name'][$x];
			$name         = $_FILES['file']['name'][$x];
			$size         = $_FILES['file']['size'][$x];
			$type         = $_FILES['file']['type'][$x];
			if ($fileName != "") {
				traceMessage("coming");
				if (move_uploaded_file($fileName, $targetFolder)) {
					//save the filename
					traceMessage("success");
					
					$userArr=array();
					$userArr['profile_img']=$browsePath;
					$blUser->UpdateUserInfo($userArr,$userId);
					}
					
					
				}
			}
		
		
		return array("success"=>true,"message"=>"profile successfully");
	}
	function add_city_request()
	{
		
		$sid = $_REQUEST['sid'];
		$fromDate = $_REQUEST['city_name'];
		
		$mysession = new Session();
		//print_r_pre($_REQUEST);
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		/*$blUser = new BL_User();
		$paymentInfo = $blUser->GetPaymentInfo($userId,$fromDate,$toDate);
		*/
		return array("success"=>true,"message"=>"Request Added");
		
	}
	function mark_arrived()
	{
		
		$sid = $_REQUEST['sid'];
		$bookingId = $_REQUEST['booking_id'];
		$mysession = new Session();
		
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		if(!isset($bookingId) || $bookingId=="" || $bookingId<=0)
		{
			return array("success"=>false,"message"=>"Invalid bookingid","data"=>null);
		}
		
		$blBooking = new BL_Booking();
		$bookingInfo = $blBooking->GetBookingInfo($bookingId);
		
		$customerUserId = $bookingInfo->rows[0]['user_id'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($customerUserId);
		
		$fcmId=$userInfo->rows[0]['android_push_id'];
		
		$objId = $bookingId;
		$type = "general";;
		$title = "Driver has arrived";;
		$body = "Driver has arrived";
		
		$notRet  =SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId,"driver");
		
		
		return array("success"=>true,"message"=>"arrived marked");
	}
	function new_function(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		
	}
	
	function get_booking_temp($requestArr)
	{
		/*
			traceMessage("get_booking_temp".$_REQUEST);
			insert into `booking`(`id`,`user_id`,`driver_id`,`pickup_location`,`pickup_lat`,`pickup_long`,`dropoff_location`,`price`,`driver_rate`,`comission`,`instructions`,`status`,`created_at`,`updated_at`,`deleted_at`) values ( NULL,'1','2','Block N North nazimabad','24.914440','67.029831','Hydrabad',NULL,NULL,'3200','2700','500','Come to pick point',NULL,now(),now(),NULL)
		*/
		//print_r_pre($requestArr);
		$sid = $requestArr['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$driverId = $mysession->getvalue("userid");
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($driverId);
		$driverFCM = $userInfo->rows[0]['android_push_id'];
		
		$userId = "1";
		
		$addArr['user_id'] = $userId;
		$addArr['driver_id'] = $driverId;
		$addArr['pickup_location'] = $requestArr["pickup_location"];
		$addArr['pickup_lat'] = $requestArr["pickup_lat"];
		$addArr['pickup_long'] = $requestArr["pickup_long"];
		$addArr['dropoff_location'] = $requestArr["dropoff_location"];
		$addArr['price'] = "3000";
		$addArr['driver_rate'] = "2500";
		$addArr['comission'] = "500";
		$addArr['instructions'] = $requestArr["instruction"];
		$addArr['status'] = "pending";
		$addArr['created_at'] = date("Y-m-d H:i:s");
		$addArr['updated_at'] = date("Y-m-d H:i:s");
		
		//print_r_pre($addArr);
		
		
		$blBooking = new BL_Booking();
		$bookingId = $blBooking->AddTempBooking($addArr);
		
		$retArr= array();
		
		$retArr["title"]="You have new booking";
		$retArr["body"]="New booing has been awarded to you please respond";
		$retArr["obj_id"]=$bookingId;
		$retArr["not_type"]="booking";
		//$bookingData = $bookingInfo->rows[0];	
		
		$objId = $bookingId;
		$type = "booking";;
		$title = "You have new booking";;
		$body = "New booing has been awarded to you please respond";
		$fcmId = $requestArr["fcm_id"];
		$fcmId = $driverFCM;
		
		$notRet  =SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId,"driver");
		return  array("success"=>true,"message"=>"Booking data","data"=>$retArr,"Notification Return"=>$notRet);
		
		
	}
	function active_ride_driver(){
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		//echo $sid;
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$userId = $mysession->getvalue("userid");
		
		$blBooking = new BL_Booking();
		
		$bookingInfo = $blBooking->GetRiderActiveBooking($userId);
		
		if($bookingInfo->count>0)
		{
			$customerUserId = $bookingInfo->rows[0]['user_id'];
			
			$driverId = $bookingInfo->rows[0]['driver_id'];
			$customerId = $bookingInfo->rows[0]['user_id'];

			$blUser = new BL_User();

			$userInfo = $blUser->GetUserInfo($driverId);

			$bookingInfo->rows[0]['driver_rating ']=$userInfo->rows[0]['user_rating'];
			$bookingInfo->rows[0]['driver_phone']=$userInfo->rows[0]['phone_number'];
			$bookingInfo->rows[0]['driver_name']=$userInfo->rows[0]['full_name'];

			$userInfo = $blUser->GetUserInfo($customerId);

			$bookingInfo->rows[0]['customer_rating ']=$userInfo->rows[0]['user_rating'];
			$bookingInfo->rows[0]['customer_phone']=$userInfo->rows[0]['phone_number'];
			$bookingInfo->rows[0]['customer_name']=$userInfo->rows[0]['full_name'];
		}
		return array("success"=>true,"message"=>"Booking Data","data"=>$bookingInfo->rows);
		
	}
	function get_driver_profile()
	{
		
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfo($userId);
		unset($userInfo->rows[0]['password']);
		
		return array("success"=>true,"message"=>"Booking details","data"=>$userInfo->rows);
	}
	function update_driver_profile()
	{
		
		$sid = $_REQUEST['sid'];
		$cName = $_REQUEST['driver_name'];
		$address = $_REQUEST['address'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		
		$userArr = array();
		$userArr['full_name']=$cName;
		$userArr['address']= $address;
		$blUser->UpdateUserInfo($userArr,$userId);
		
		$userInfo = $blUser->GetUserInfo($userId);
		unset($userInfo->rows[0]['password']);
		
		return array("success"=>true,"message"=>"User Data Updated","data"=>$userInfo->rows);
	}
	function get_notification_lists_driver()
	{
		
		$sid = $_REQUEST['sid'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$notObject = $blUser->GetNotificationLists($userId);
		
		
		
		
		
		return array("success"=>true,"message"=>"$userId-Notification Data","data"=>$notObject->rows);
	}
	function mark_notification_as_read_driver()
	{
		
		$sid = $_REQUEST['sid'];
		$notId = $_REQUEST['notification_id'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$notObject = $blUser->MarkAsRead($notId,$userId);
		
		
		
		
		
		return array("success"=>true,"message"=>"Notification mark as read");
	}
	function driver_set_location()
	{
		
		$sid = $_REQUEST['sid'];
		$lat = $_REQUEST['lat'];
		$long = $_REQUEST['long'];
		
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		
		$userId = $mysession->getvalue("userid");
		
		$blUser = new BL_User();
		$notObject = $blUser->DriverSetLocation($userId,$lat,$long);
		
		
		
		
		
		return array("success"=>true,"message"=>"lat long updated");
	}
	function get_availaible_agent()
	{
		
		$blUser = new BL_User();
		$notObject = $blUser->GetAgent();
		return array("success"=>true,"message"=>"Alaible Agent","data"=>$notObject->rows);
		
	}
	function send_approval_code()
	{
		$sid = $_REQUEST['sid'];
		$mysession = new Session();
		
		if(!$mysession->updatesession($sid))
		{
		return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$blUser = new BL_User();
		$addusersInfo = array();
		$randomnum =  rand(1000,9999);
		$userId = $mysession->getvalue("userid");

		$addusersInfo['driver_id']=$userId;
		$addusersInfo['agent_id']=$_REQUEST['agent_id'];
		$addusersInfo['is_verified']='no';
		$addusersInfo['otp_code']=$randomnum;
		
		$agentInfo = $blUser->GetAgentById($_REQUEST['agent_id']);
		$agentMobile = $agentInfo->rows[0]['mobile'];
		
		traceMessage("AgentMobile:$agentMobile");
		SendSMS($agentMobile,$randomnum);
		if($_REQUEST['agent_id']!=15)
		{
			
			$blUser->AddOtpCode($addusersInfo,$randomnum);
		}
		else
		{
			
		}
		
		$userInfo = $blUser->GetUserInfo($userId);
		unset($userInfo->rows[0]['password']);
		$userInfo->rows[0]['user_id'] = $sid; 
		
		$retObj = array();
		$retObj['userinfo'] = $userInfo->rows[0];
		
		
		return array("success"=>true,"message"=>"Otp code add successfully","data"=>$retObj);
	}
	 function vrify_otp()
	{
		$sid = $_REQUEST['sid'];
		$otp_code - $_REQUEST['otp_code'];

		$mysession = new Session();

		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$blUser = new BL_User();
		$userId = $mysession->getvalue("userid");
		$driverId=$userId;
		$agent=$_REQUEST['agent_id'];
		$userOtp=$_REQUEST['otp_code'];
		$blData=$blUser->SelectOtp($driverId,$agent);
		$otp=$blData->rows[0]['otp_code'];
		$otpId = $blData->rows[0]['id'];
		traceMessage("dromdb".print_r_log($status));
		if($agent=="15")
			$otp = "1234";
		
		//$otp = "1234";
		if($userOtp == $otp)
		{
			$arr['is_verified'] = 'yes';
			if($agent=="15")
			{
				//$arr['status'] = '1';
			}
			else
			{
				$arr['status'] = '1';
				$arrr['status'] = '1';
			}
			$blUser->UpdateOtp($arr, $otpId);
			if($agent=="15")
			{
				//$arr['status'] = '1';
			}
			else
			{
				$arr['status'] = '1';
				$arrr['status'] = '1';
			}
			$arrr['agent'] = $agent;	
			$arrr['is_verified'] = 'yes';
			$blUser->UpdateUsers($arrr,$driverId);
			
			$userInfo = $blUser->GetUserInfo($userId);
			unset($userInfo->rows[0]['password']);
			$userInfo->rows[0]['user_id'] = $sid; 
			
			$retObj = array();
			$retObj['userinfo'] = $userInfo->rows[0];
			
			return array("success"=>true,"message"=>"Verified successfully","data"=>$retObj);
			
		}
		else
		{
			 // echo $otp ."arsrgsr".$userOtp;
			return array("success"=>false,"message"=>"otp not matched ");
		}
	}
	function send_otp_generic()
	{
		$phoneNo = $_REQUEST['phone_no'];
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfoByPhone($phoneNo);
		//print_r_pre($userInfo,"aa:$phoneNo");
		$randomnum =  rand(1000,9999);
		
		if($userInfo->count>0)
		{
			$addusersInfo['driver_id']=$userInfo->rows[0]['user_id'];
			$addusersInfo['agent_id']=$userInfo->rows[0]['user_id'];
			$addusersInfo['is_verified']='no';
			$addusersInfo['otp_code']=$randomnum;
			
			SendSMS($phoneNo,$randomnum);
			$blUser->AddOtpCode($addusersInfo,$randomnum);
			return array("success"=>true,"message"=>"Otp code add successfully","data"=>$retObj);
	
	
		}
		else
		{
			return array("success"=>false,"message"=>"Invalid user","data"=>null);
		
		}
			
		
		
	}
	 function verify_otp_generic()
	{
		$phoneNo = $_REQUEST['phone_no'];
		$otpCode = $_REQUEST['otp_code'];

		$mysession = new Session();

		
		
		$blUser = new BL_User();
		$userInfo = $blUser->GetUserInfoByPhone($phoneNo);
		
		
		$userId = $mysession->getvalue("userid");
		$driverId=$userInfo->rows[0]['user_id'];
		$password=$userInfo->rows[0]['password'];
		
		
		$otpData=$blUser->VerifyOtp($driverId,$otpCode);
		
		//echo "O:$otpCode--$phoneNo,$password";
		
		
		
		if($otpData->count>0)
		{
			$blUser->MarkOTPAsDone($driverId,$otpCode);
			
			$res = Authenticate($phoneNo,$password,"yes");// $blUser->AuhenticateUser($loginid,$loginid);
			
			if($res["code"] != 0)
			{
				 
				
				return array("success"=>false,"message"=>"Invalid credentials","data"=>null);
			}
			else
			{
				/*Login work start*/
				$sid = $res['sid'];
					$mysession =  new Session();
					$mysession->updatesession($sid);
					$userId = $res['user_id'];
					$blUser = new BL_User();
					
					$userArr = array();
					$userArr['android_push_id']=$androidId;
					$userArr['apple_push_id']= $appleId;
					$userArr['shift_status']= 'offline';
					$blUser->UpdateUserInfo($userArr,$userId);
					/*
						
						$userInfo = $blUser->GetUserInfo($user_id)->rows[0];
						$userInfo['sid'] = $sid;
						$userInfo['user_id'] = $sid;
						
						$bldocuments = new BL_Documents(); 
						
						$userDocuments =  $bldocuments->GetUserDocuments($res["user_id"]);
						
						//print_r_pre($userDocuments,"aa");
						$userInfo['user_documents'] = $userDocuments; 
						
					return array("success"=>true,"message"=>"login successfull","data"=>$userInfo);*/
					
					//$userId = $mysession->getvalue("userid"); 
					$blUser = new BL_User();
					$userInfo = $blUser->GetUserInfo($userId);
					$userInfo->rows[0]['password']="";
					
					$bldocuments = new BL_Documents(); 
					$retObj = array();
					
					$userInfo->rows[0]['password']="";
					$userInfo->rows[0]['user_id']=$res["sid"];
					
					$retObj['userinfo'] = $userInfo->rows[0];
					//$retObj['agent_verified'] = $userInfo->rows[0]['is_verified'];
					
					$cnicInfo = $bldocuments->GetUserDocumentsByType($userId,"1");
					
					if(count($cnicInfo[0])>0)
					{
						$retObj['cnic']=$cnicInfo[0];
						$retObj['cnic'][0]['files']=$cnicInfo[1];
					}
					
					$licenseInfo = $bldocuments->GetUserDocumentsByType($userId,"2");
					
					if(count($licenseInfo[0])>0)
					{
						$retObj['license']=$licenseInfo[0];
						$retObj['license'][0]['files']=$licenseInfo[1];
					}
					
					$runningInfo = $bldocuments->GetUserDocumentsByType($userId,"3");
					
					if(count($runningInfo[0])>0)
					{
						$retObj['running']=$runningInfo[0];
						$retObj['running'][0]['files']=$runningInfo[1];
					}
					
					$taxInfo = $bldocuments->GetUserDocumentsByType($userId,"4");
					
					if(count($taxInfo[0])>0)
					{
						
						$retObj['tax']=$taxInfo[0];
						$retObj['tax'][0]['files']=$taxInfo[1];
					}
					$blVehicle =  new BL_Vehicle();
					$vehicleInfo = $blVehicle->GetVehicleInfo($userId);
					
					if(count($vehicleInfo)>0)
					$retObj['vehicle']=$vehicleInfo[0];
					
					traceMessage(print_r_log($retObj)); 
					return array("success"=>true,"message"=>"Login Successfully","data"=>$retObj);
		
				/*Login work end*/
			}
		}
		else
		{
			 // echo $otp ."arsrgsr".$userOtp;
			return array("success"=>false,"message"=>"otp not matched ");
		}
	}
	function get_user_agent()
	{
		$sid = $_REQUEST['sid'];
		$mysession = new Session();

		
		if(!$mysession->updatesession($sid))
		{
			return array("success"=>false,"message"=>"Invalid Session","data"=>null);
		}
		$blUser = new BL_User();
		$userId = $mysession->getvalue("userid");
		$agentInfo = $blUser->GetAgentInfoByUser($userId);
		
		return array("success"=>true,"message"=>"user data1","data"=>$agentInfo->rows[0]);
		
	}
	
?>