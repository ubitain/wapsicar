<?php 
// require_once(MODULE . "/class/ajax/json.php");
// require_once(MODULE . "/utility/standard_library.php");
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/addstuff.php");

	function DeleteBranch($recordId)
	{

		traceMessage("DeleteBranch:");
		$agentInfo['status']= 'inactive';
		$blagent = new BL_User();
		$res = $blagent->UpdateAgent($recordId,$agentInfo);
		return $res;
	}
	function CityApproval($recordId,$cityName,$status)
	{

		traceMessage("CityApproval:$recordId,$cityName,$status");
		$cityInfo['status']= $status;
		$blcity = new BL_User();
		$addCity = new BL_AddStuff();
		$res = $blcity->UpdateCityStatus($recordId,$cityInfo);
		if($status == 1){
			$addstuff['city_name'] = $cityName;
			$addstuff['status'] = $status;
			traceMessage('CityApproval'.print_r_log($addstuff));
			$addCity->AddCity($addstuff);
		}
		return $res;
	}
	 function CityDelete($recordId)
	{

		traceMessage("CityDelete:");
		$cityInfo['status']= -1;
		$blcity = new BL_User();
		$res = $blcity->DeleteCityStatus($recordId,$cityInfo);
		return $res;
	}
	?>