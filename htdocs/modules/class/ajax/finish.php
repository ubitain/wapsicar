
<?php
	require_once(MODULE . "/class/ajax/json.php");
    require_once(MODULE . "/class/bllayer/finish.php");
    require_once(MODULE."/class/bllayer/viewclaims.php");


	function ProccedToFinanceDepart($claimid)
	{
		traceMessage("-*-*-*-*-*In Ajax ProccedToFinanceDepart-*-*-*-*",$claimid);
        $claim['claim_status'] = "completed";
        $blproduct = new BL_Finish();
		$updateDescRes = $blproduct->ProccedToFinanceDepart($claim,$claimid);

		$viewClaims=new BL_ViewClaims();
		$getUserId = $viewClaims->FetchUserName($claimid);
	    $UserId = $getUserId->rows[0]['user_id'];
	    $claimNo = $getUserId->rows[0]['claim_no'];

		$getUserDetail = $viewClaims->FetchDealerName($UserId);
	    $claimerName = $getUserDetail->rows[0]['full_name'];
	    $claimerEmail = $getUserDetail->rows[0]['email'];
	    $androidPushId = $getUserDetail->rows[0]['android_push_id'];
	    $applePushId = $getUserDetail->rows[0]['apple_push_id'];
		traceMessage(print_r_log($UserId."HSAODHAOSDIHASDOISHADS".$claimNo));
    $page="inprocess_claims";
		$message = "Dear $claimerName, Your claim ($claimNo) is now proceed to Finanace Department.";
		notifications($UserId,$claimerEmail,$androidPushId,$applePushId,$message,$claimNo,$page);

		return $updateDescRes;
	}


function notifications($userId,$userEmail,$androidPushId,$applePushId,$message,$claimID,$page)
{
    traceMessage("FCM NOTIFICATION FIRST STEP!!!");

    $inserttime = date('Y-m-d H:i:s');
    $notificationtype = NOTIFICATION_GENERAL;
    $data_notify = array();
    $db = array('user_id' => $userId,
              'reference_id' => $claimID,
              'title' => 'Claim Alert!',
              'type' => $notificationtype,
              'message' => $message,
              'creation_time' => $inserttime,
              'email_address' => $userEmail,
              'android_push_id' => $androidPushId,
              'apple_push_id' => $applePushId,
              'page' => $page,
              'status' => 1,
    );
    $msg = array (
          'body'    => $message,
          'title'   => 'Claim Alert!',
          'type'    => $notificationtype,
          'icon'    => 'myicon',
          'sound' => 'mySound'
    );
    $data_notify['id'] = $androidPushId;
    $data_notify['msg'] = $msg;
    $data_notify['db'] = $db;
    traceMessage("FCM NOTIFICATION".print_r_log($data_notify));
    $result = fcm_send_notification($data_notify);
    traceMessage('FCM RETURN;'.print_r_log($result));
    /*
    $temp     = array(
          "header" => array(
              "success" => "0",
              "message" => "Notification Sent!",
              "token" => $token
          ),
          "body" => $obj
      );
      $response = json_encode($temp);
      echo $response;
    */
}

?>