
<?
	require_once(MODULE . "/class/ajax/json.php");
    require_once(MODULE . "/class/bllayer/product.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/process/user.php");

	function DeleteProduct($recordId)
	{
		traceMessage("-*-*-*-*-*In Ajax DeleteProduct-*-*-*-*",$recordId);
        $productInfo['status'] = '-1';
        $blproduct = new BL_Product();
		$updateDescRes = $blproduct->UpdateProduct($productInfo,$recordId);
		return $updateDescRes;
	}


	function checkCodeExist($request,$code){


		traceMessage("-*-*-*-*-*In Ajax check poduct exist-*-*-*-*",$request,$code);


		$data['request'] = $request;
		$data['code'] = $code;

        $blproduct = new BL_Product();
		if($request == "product_code"){
			
			$response =  $blproduct->checkProductCode($data);
			return $response;


		}else{
			
			$response =  $blproduct->checkAtProductCode($data);
			return $response;


		}


	}
?>
