
<?
	require_once(MODULE . "/class/ajax/json.php");
	require_once(MODULE . "/class/bllayer/customer.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/process/user.php");
	require_once(MODULE . "/class/bllayer/user.php");

	function AssignCustomer($assignCustomerInfo)
	{
		traceMessage("-*-*-*-*-*In Ajax AssignCustomer-*-*-*-*");
		$assignCustomerInfo=json_decode($assignCustomerInfo);
		$assignCustomerInfo=(array)$assignCustomerInfo;
		traceMessage("info array is".print_r_log($assignCustomerInfo['customer_id']));
		foreach ($assignCustomerInfo['customer_id'] as $value)
		{
			$assignCustomerSingle['user_id'] = $assignCustomerInfo['user_id'];
			$assignCustomerSingle['customer_id'] = $value;
			traceMessage("info array is ".print_r_log($assignCustomerSingle));
			$blcustomer = new BL_Customer();
			$updateDescRes = $blcustomer->AssignCustomer($assignCustomerSingle);
		}
		return true;
	}
	function UnassignedCustomer($unassignCustomerInfo)
	{
		traceMessage("UnassignedCustomer In Ajax".print_r_log($unassignCustomerInfo));
		$unassignCustomerInfo=json_decode($unassignCustomerInfo);
		$unassignCustomerInfo=(array)$unassignCustomerInfo;
		traceMessage("info array is".print_r_log($unassignCustomerInfo['customer_id']));
		foreach ($unassignCustomerInfo['customer_id'] as $value)
		{
			$unassignCustomerSingle['user_id'] = $unassignCustomerInfo['user_id'];
			$unassignCustomerSingle['customer_id'] = $value;
			traceMessage("info array is ".print_r_log($unassignCustomerSingle));
			$blcustomer = new BL_Customer();
			$updateDescRes = $blcustomer->UnassignedCustomer($unassignCustomerSingle);
		}
		return true;
	}
	function AddNewArea($areaInfo)
	{
		traceMessage("-*-*-*-*-*In Ajax AddNewArea-*-*-*-*");
		$areaInfo=json_decode($areaInfo);
		$areaInfo=(array)$areaInfo;
		traceMessage("info array is ".print_r_log($areaInfo));
		$blcustomer = new BL_Customer();
		$updateDescRes = $blcustomer->AddNewArea($areaInfo);
		return $updateDescRes;
	}
	function DeleteCustomer($recordId)
	{
		$customerInfo['status']= -1;
		$blcustomer = new BL_Customer();
		$res = $blcustomer->UpdateCustomer($customerInfo,$recordId);
		return $res;
	}
	function AjaxCustomerTable()
	{
		$aColumns = array('customer_name', 'company_name', 'email_address', 'customer_type', 'address','','branch_name','account_number','account_balance','customer_id');

		$sIndexColumn = "customer_id";
		$sTable = "customer c left join customer_details cd on c.customer_id=cd.customer_id";
		$blcustomer = new BL_Customer();
		$res = $blcustomer->AjaxCustomerTable($aColumns,$sIndexColumn,$sTable,$_REQUEST);
		echo $res;
	}
	function DeleteDept($recordId)
	{
		traceMessage("-*-*-*-*-*In Ajax DeleteDept-*-*-*-*",$recordId);
        $deptInfo['status'] = '-1';
        $bldept = new BL_Customer();
		$updateDescRes = $bldept->UpdateDepartment($deptInfo,$recordId);
		return $updateDescRes;
	}
	function DeleteReason($recordId)
	{
		traceMessage("*-*-*-*-*- In Ajax Delete Reason -*-*-*-*",$recordId);
        $reasonInfo['status'] = '-1';
        $blcustomer = new BL_Customer();
		$updateDescRes = $blcustomer->UpdateReason($reasonInfo,$recordId);
		return $updateDescRes;
	}
?>
