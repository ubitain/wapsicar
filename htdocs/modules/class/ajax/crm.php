
<?php
	require_once(MODULE . "/class/ajax/json.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/process/user.php");
    // CRM BL Layer
    require_once(MODULE . "/class/bllayer/crm.php");
	// User BL Layer
	require_once(MODULE . "/class/bllayer/user.php");

    //Your functions start from here
    function ResendAll($leadId,$latitude,$longitude)
    {
		$data = array();
		$data['latitude']= $latitude;
		$data['longitude']= $longitude;
		traceMessage("Lead id : $leadId ".print_r_log($data));
		$blcrm = new BL_Crm();
		$bluser = new BL_User();
		$nearbyUsersInfo = $blcrm->NearCustomerUserId();
	    traceMessage("Near Customer UserId : ".print_r_log($nearbyUsersInfo->rows));
	    $distanceRadius = LEAD_DISTANCE_DIFFERENCE;
	    foreach ($nearbyUsersInfo->rows as $key => $value) {
	        $userLat = $value['latitude'];
	        $userLong = $value['longitude'];
	        $distanceDifference=DistanceDifference($userLat, $userLong, $data['latitude'], $data['longitude'], "K");
			traceMessage("User ID is ".$value['user_id']."distance difference is ".$distanceDifference." KM");
	        if($distanceDifference <= $distanceRadius)
	        {
	            // Insert all the user_id in lead_users table
	            $params = array();
	            $params['user_id'] = $value['user_id'];
	            $params['lead_id'] = $leadId;
	            $params['status'] = "PENDING";
	            $params['create_at']= date('Y-m-d H:i:s',strtotime("now"));
	            $params['update_at']= date('Y-m-d H:i:s',strtotime("now"));
	            if (!empty($leadId) && !empty($value['user_id'])) {
	                $assignUsersLeadRecordId = $blcrm->AssignUsersToLeadId($params);
	                // Notification Sent
	                $userdata = $bluser->GetUserInfo($value['user_id']);
	        		$inserttime = date('Y-m-d H:i:s');
	        		$notificationtype = 1;
	        		$data = array();
	        		$db = array('user_id' => $userdata->rows[0]['user_id'],
	                            'reference_id' => $leadId,
	        					'title' => 'New Lead',
	        					'type' => $notificationtype,
	        					'message' => "Please accept the lead",
	        					'creation_time' => $inserttime,
	        					'email_address' => $userdata->rows[0]['email_address'],
	        					'android_push_id' => $userdata->rows[0]['android_push_id'],
	        					'apple_push_id' => $userdata->rows[0]['apple_push_id'],
	        					'status' => 1,
	        		);
	        		$msg = array (
	        		   		'body' 	=> 'Please accept the lead"',
	        				'title'	=> 'New Lead',
	        		   		'type'	=> $notificationtype,
	                        'reference_id' => $leadId,
	        		        'icon'	=> 'myicon',
	        		        'sound' => 'mySound'
	        		);
	        		$data['id'] = $userdata->rows[0]['android_push_id'];
	        		$data['msg'] = $msg;
	        		$data['db'] = $db;
	        		traceMessage("FCM NOTIFICATION".print_r_log($data));
	        		$result = fcm_send_notification($data);
	                traceMessage('FCM RETURN;'.print_r_log($result));
					return $result;
	            }

	        }
	    }
    }

	function Resend($leadId,$userId)
	{
	        if (!empty($leadId) && !empty($userId)) {
	            $params = array();
	            $params['user_id'] = $userId;
	            $params['lead_id'] = $leadId;
	            $params['status'] = "PENDING";
	            $params['create_at']= date('Y-m-d H:i:s',strtotime("now"));
	            $params['update_at']= date('Y-m-d H:i:s',strtotime("now"));
	            $assignUsersLeadRecordId = $blcrm->AssignUsersToLeadId($params);
	            // Notification Sent
	            $userdata = $bluser->GetUserInfo($userId);
	            $inserttime = date('Y-m-d H:i:s');
	            $notificationtype = 1;
	            $data = array();
	            $db = array('user_id' => $userdata->rows[0]['user_id'],
	                        'reference_id' => $leadId,
	                        'title' => 'New Lead',
	                        'type' => $notificationtype,
	                        'message' => "Please accept the lead",
	                        'creation_time' => $inserttime,
	                        'email_address' => $userdata->rows[0]['email_address'],
	                        'android_push_id' => $userdata->rows[0]['android_push_id'],
	                        'apple_push_id' => $userdata->rows[0]['apple_push_id'],
	                        'status' => 1,
	            );
	            $msg = array (
	                    'body' 	=> 'Please accept the lead"',
	                    'title'	=> 'New Lead',
	                    'type'	=> $notificationtype,
	                    'reference_id' => $leadId,
	                    'icon'	=> 'myicon',
	                    'sound' => 'mySound'
	            );
	            $data['id'] = $userdata->rows[0]['android_push_id'];
	            $data['msg'] = $msg;
	            $data['db'] = $db;
	            traceMessage("FCM NOTIFICATION".print_r_log($data));
	            $result = fcm_send_notification($data);
	            traceMessage('FCM RETURN;'.print_r_log($result));
				return $result;
	        }
	}
?>
