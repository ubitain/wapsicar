
<?php
	require_once(MODULE . "/class/ajax/json.php");

	require_once(MODULE . "/class/bllayer/user.php");
	//require_once(MODULE . "/class/bllayer/testadd.php");

	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/process/user.php");


	function deleteduser($userID)
    {

      traceMessage("Getting ID: ".$_REQUEST);
      $userArray['status']= INACTIVE_USER;
      $blUser = new BL_User();
      $res = $blUser->DeleteUser($userArray,$userID);
      return $res;
    }

    function GetSubCategory($categoryid)
	{

		$blUser = new BL_User();
		$cateogory = $blUser->GetSubCategory($categoryid);
      traceMessage("Getting ID: ".print_r_log($cateogory));

		for($i=0;$i<$cateogory->count;$i++) {
		 $value = $cateogory->rows[$i]["sub_category_id"];
			$str .= '<option  value="'.$value.'">'.$cateogory->rows[$i]["sub_category_name"].'</option>';
		}
		return $str;
	}

	function GetRegion($zone)
	{
      	$zoneFull = explode(',',$zone);
		$zoneName = $zoneFull[0];
		$zoneId = $zoneFull[1];

		$blUser = new BL_User();
		$regionreturn = $blUser->GetRegion($zoneName);
      traceMessage("Getting ID: ".print_r_log($regionreturn));

		for($i=0;$i<$regionreturn->count;$i++) {
		 $value = $regionreturn->rows[$i]["region_name"];
		 $valueId = $regionreturn->rows[$i]["region_id"];
			$str .= '<option  value="'.$value.','.$valueId.'">'.$regionreturn->rows[$i]["region_name"].'</option>';
		}
		return $str;
	}
	function PostAjaxScreenMessage($sid)
	{
		$mysession = new session();
		$ret = $mysession->updatesession($sid);
		if(!$ret)
		{
			traceMessage("invalid session");
			return false;
		}
		$message = $mysession->getvalue('message');
		$mysession->deletevalue("message");
		return $message;
	}

	function GetCity($region)
	{
		$regionFull = explode(',',$region);
		$regionName = $regionFull[0];
		$regionId = $regionFull[1];

		$blUser = new BL_User();
		$cityreturn = $blUser->GetCityNames($regionName);
      traceMessage("Getting ID: ".print_r_log($cityreturn));

		for($i=0;$i<$cityreturn->count;$i++) {
		 $value = $cityreturn->rows[$i]["city_name"];
		 $valueId = $cityreturn->rows[$i]["city_id"];
			$str .= '<option  value="'.$value.','.$valueId.'">'.$cityreturn->rows[$i]["city_name"].'</option>';
		}
		return $str;
	}

	function GetArea($city)
	{	
		$cityFull = explode(',',$city);
		$cityName = $cityFull[0];
		$cityId = $cityFull[1];

		$blUser = new BL_User();
		$areareturn = $blUser->GetArea($cityName);
      traceMessage("Getting ID: ".print_r_log($areareturn));

		for($i=0;$i<$areareturn->count;$i++) {
		 $value = $areareturn->rows[$i]["area_name"];
		 $valueId = $areareturn->rows[$i]["area_id"];
			$str .= '<option  value="'.$value.','.$valueId.'">'.$areareturn->rows[$i]["area_name"].'</option>';
		}
		return $str;
	}

	function GetPurchaseFrom($zone,$region,$city,$area)
	{	
		$zoneFull = explode(',',$zone);
		$zoneName = $zoneFull[0];
		$zoneId = $zoneFull[1];

		$regionFull = explode(',',$region);
		$regionName = $regionFull[0];
		$regionId = $regionFull[1];

		$cityFull = explode(',',$city);
		$cityName = $cityFull[0];
		$cityId = $cityFull[1];

		$areaFull = explode(',',$area);
		$areaName = $areaFull[0];
		$areaId = $areaFull[1];

		$blUser = new BL_User();
		$getPurchaseFrom = $blUser->GetPurchaseFrom($zoneId,$regionId,$cityId,$areaId);
      //traceMessage("Getting ID: ".print_r_log($areareturn));

		for($i=0;$i<$getPurchaseFrom->count;$i++) {
		 $value = $getPurchaseFrom->rows[$i]["full_name"];
		 $valueId = $getPurchaseFrom->rows[$i]["user_id"];
			$str .= '<option  value="'.$value.','.$valueId.'">'.$getPurchaseFrom->rows[$i]["full_name"].'</option>';
		}
		return $str;
	}
	function GetAccountof($zone,$region,$city,$area)
	{	
		$zoneFull = explode(',',$zone);
		$zoneName = $zoneFull[0];
		$zoneId = $zoneFull[1];

		$regionFull = explode(',',$region);
		$regionName = $regionFull[0];
		$regionId = $regionFull[1];

		$cityFull = explode(',',$city);
		$cityName = $cityFull[0];
		$cityId = $cityFull[1];

		$areaFull = explode(',',$area);
		$areaName = $areaFull[0];
		$areaId = $areaFull[1];

		$blUser = new BL_User();
		$getPurchaseFrom = $blUser->GetAccountof($zoneId,$regionId,$cityId,$areaId);
      //traceMessage("Getting ID: ".print_r_log($areareturn));

		for($i=0;$i<$getPurchaseFrom->count;$i++) {
		 $value = $getPurchaseFrom->rows[$i]["full_name"];
		 $valueId = $getPurchaseFrom->rows[$i]["user_id"];
			$str .= '<option  value="'.$value.','.$valueId.'">'.$getPurchaseFrom->rows[$i]["full_name"].'</option>';
		}
		return $str;
	}
    /////////////////////////


	function AddUser($companyId,$loginId,$emailAddress,$password,$emailaddress,$userName,$sid,$userType)
	{
		traceMessage("$loginId,$emailAddress,$password,$emailaddress,$userName,$sid,$userType");
		$mysession = new session();
		$ret = $mysession->updatesession($sid);
		/*if(!$ret)
			{
			traceMessage("invalid session");
			return;
		}*/
 
		$blUser = new BL_User();
		$userCount = $blUser->CheckLoginId($loginId); //done
		if($userCount>0)
		{
			traceMessage("User Already Exist...");
			return false;
		}
		traceMessage("ading user:$loginName,$pass,'','',$emailAdd");

		$userId = $blUser->AddUser($companyId,$loginId,$password,$emailAddress,$userName,$userType); //done
		traceMessage("creating session");
		//$mysession->deletevalue("userid");
		$mysession->setvalue("userid",$userId);
		$mysession->setvalue("fullname",$userName);
		$mysession->setvalue("emailaddress",$emailAddress);
		$mysession->setvalue("usertype",$userType);
		//if($userId>0)
		//	SendCustomerWelcomeEmail($email,$pass);
		//traceMessage("ret-userid1:$userId");
		return $userId;

	}
	function CheckNameExist($loginName)
	{
		traceMessage("name:$loginName");

		//$loginName = preg_replace('/[^\da-z]/i', '', $loginName);

		$blUser = new BL_User();
		$userCount = $blUser->CheckLoginId($loginName); //done
		if($userCount>0)
		{
			traceMessage("User Already Exist...");
			return 'false';
		}
		else
		return 'true';
	}

	// function GetAreaByCityId($cityId)
	// {
		// traceMessage("City Id :$cityId");

		//$loginName = preg_replace('/[^\da-z]/i', '', $loginName);

		// $blUser = new BL_User();
		// $areaInfo = $blUser->GetAreaByCityId($cityId);
		// traceMessage(print_r_log($areaInfo));
		// return json_encode($areaInfo);
	// }


	function GetAreaByCityId($cityId,$userId)
	{
		traceMessage("city ID: $cityId  userId : $userId");
		//exit;
	$blUser = new BL_User();
	$areaInfo = $blUser->GetAreaByCityId($cityId,$userId);

	traceMessage("aaaaaaaaa".print_r_log($areaInfo));
	$str = "<select class='form-control'  name ='territory' id='territory'>";
	$str .= '<option value="">Select Area</option>';
	for($i=0;$i<$areaInfo->count;$i++)
	{
		$str .= '<option value="'.$areaInfo->rows[$i]["area_code"].'">'.$areaInfo->rows[$i]["area_name"].'</option>';}$str .= "</select>";
		traceMessage("aaaa".$str);return $str;
	}



	function checkuserName($userId,$oldPassword)
{
		traceMessage(" In sdfsaaction checkuserName ");
		$blUser = new BL_User();
		traceMessage("checkuserName $userId");
		
		$oldPassword = base64_decode($oldPassword);
		$password = md5($oldPassword);
		traceMessage("checkuserName $password");
		$x = $blUser->checkuserName($userId,$password);
		//$count = array();
		//$count = $x->rows[0]['UserCount'];
		//$count .=",";
		//$count .= $x->rows[0]['user_id'];
		traceMessage("status count :".$x);

		return json_encode($x);
		//traceMessage("Driver ID  ".$driverName);
		//exit;

}
	function LinkTerritoryWithArea($terrAreaInfo)
	{
		$terrAreaInfo=json_decode($terrAreaInfo);
		$terrAreaInfo=(array)$terrAreaInfo;
		traceMessage("info array is ".print_r_log($terrAreaInfo));
		$blUser = new BL_User();
		$updateDescRes = $blUser->LinkTerritoryWithArea($terrAreaInfo);
		return true;
	}

	function DeleteUser($recordId)
	{
		//$userinfo['status'] = "-1";
		traceMessage("USAMAAAAA1-".print_r_log($recordId));
		$delete['status']= -1;
		$abc=new Testadd_Bl();
		$abc1=$abc->update($delete,$recordId);
		return true;

		// $blUser = new BL_User();
		// $res = $blUser->UpdateUser($userinfo,$recordId);
		// $userdata = $blUser->GetUserInfo($recordId);
		// $inserttime = date('Y-m-d H:i:s');
		// $notificationtype = 1;
		// $data = array();
		// $db = array('user_id' => $userdata->rows[0]['user_id'],
		// 			'title' => 'Force Logout',
		// 			'type' => $notificationtype,
		// 			'message' => "force logout",
		// 			'creation_time' => $inserttime,
		// 			'email_address' => $userdata->rows[0]['email_address'],
		// 			'android_push_id' => $userdata->rows[0]['android_push_id'],
		// 			'apple_push_id' => $userdata->rows[0]['apple_push_id'],
		// 			'status' => 1,
		// );
		// $msg = array (
		//    		'body' 	=> 'Force Logout',
		// 		'title'	=> 'Delete User',
		//    		'type'	=> $notificationtype,
		//         'icon'	=> 'myicon',
		//         'sound' => 'mySound'
		// );
		// $data['id'] = $userdata->rows[0]['android_push_id'];
		// $data['msg'] = $msg;
		// $data['db'] = $db;
		// traceMessage("FCM NOTIFICATION".print_r_log($data));
		// fcm_send_notification($data);
		/*
		$tablename = user_mapping,attendance,inbox,user_customer;
		*/
		/*
		$tablename = "user_mapping";
		$whereName = "child_id";
		$blUser->DeleteUserReference($tablename,$userinfo,$whereName,$recordId);
		$whereName = "parent_id";
		$blUser->DeleteUserReference($tablename,$userinfo,$whereName,$recordId);
		*/
		$res='asdasdoiasndo';
		return $res;
	}


?>
