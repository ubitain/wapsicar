 <?php

	class DL_AddStuff extends DataAccessBase
	{
		function AddCity($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `city` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateCity($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update city set ".$str." where city_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateVahicle($usereditid,$editusersInfo)
		{
			$str = $this->GetUpdateParams($editusersInfo);
			$updSql="update vehicle_make set ".$str." where id=".$usereditid;
			// echo $updSql;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateVahicleModel($userEditId,$editusersInfo)
		{
			$str = $this->GetUpdateParams($editusersInfo);
			$updSql="update vehicle_model set ".$str." where id=".$userEditId;
			// echo $updSql;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function AddVahicle($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `vehicle_make` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function AddVahicleModel($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `vehicle_model` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function EditVehicle($data)
    		{

           $sql = "SELECT * FROM `vehicle_make` where id=$data ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;


    		}	
    		function EditVehicleModel($data)
    		{

           $sql = "SELECT * FROM `vehicle_model` where id=$data ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;


    		}	
		
		function UpdateRoute($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update routes set ".$str." where route_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}


		function GetCityById($city_id)
		{
			$sql = "SELECT * from city where city_id=$city_id";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		function GetRouteById($city_id)
		{
			$sql = "SELECT * from routes where route_id=$city_id";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		function AddZone($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `zone` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateZone($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update zone set ".$str." where zone_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function AddRegion($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `regions` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateRegion($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update regions set ".$str." where region_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}


		function AddRoute($addstuff)
		{
			$str=$this->GetInsertParams($addstuff);
			$sql="insert into `routes` $str where ";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		// function UpdateRoute($addstuff,$route_id)
		// {
		// 	$str = $this->GetUpdateParams($addstuff);
		// 	$updSql="update routes set ".$str." where route_id=".$route_id;
		// 	traceMessage("sql:$updSql");
		// 	if (!$this->Connect('mysql'))
		// 	{
		// 		traceMessage("Database connection error ".mssql_get_last_message());
		// 		return null;
		// 	}
		// 	if (!$this->ExecuteNonQuery($updSql))
		// 	{
		// 		$this->Dispose();
		// 		return false;
		// 	}
		// 	$this->Dispose();
		// 	return true;
		// }

		function GetRoute()
		{
			$sql = "SELECT r.*, c1.city_name as from_city_name , c2.city_name as to_city_name from routes r, city c1, city c2 where r.status >=0 and c1.city_id = r.to_city and c2.city_id = r.to_city";
			$sql = "SELECT r.*, c1.city_name as from_city_name , c2.city_name as to_city_name 
			from routes r, city c1, city c2 where r.status >=0 and c1.city_id = r.from_city and c2.city_id = r.to_city";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function DeleteRoute($route_id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update routes set status='-1' where route_id=".$route_id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetCityRoutes($city)
		{
		
			$sql = "SELECT r.*, c1.city_name as from_city_name , c2.city_name as to_city_name 
			from routes r, city c1, city c2 where r.status >=0 and c1.city_id = r.from_city and c2.city_id = r.to_city
			and c1.city_name='$city'";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		
		
		function AddRoutesRate($userId,$routeId,$routeFare,$comm)
		{
			$sql = "insert into rate_cards 
					(driver_id, route_id, driver_rates, commission_rates, 
					status,entry_date)
					values
					('$userId', '$routeId', '$routeFare', '$comm', 
					'1',now())";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
		
			return true;

		}
		function DeleteRoutesRate($userId)
		{
			$sqlDel = "delete from rate_cards where driver_id='$userId'";
		
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sqlDel))
			{
				$this->Dispose();
				return null;
			}
			
			$this->Dispose();
			
			return true;

		}
	}

?>