<?php
class Dl_Settings extends DataAccessBase
{
		function DeleteCity($settingArray,$cityID)
		{
			$str = $this->GetUpdateParams($settingArray);
			$updSql="update city set ".$str." where city_id=".$cityID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteVehicle($settingArray,$vehicleID)
		{
			$str = $this->GetUpdateParams($settingArray);
			$updSql="update vehicle_make set ".$str." where id=".$vehicleID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteVehicleModel($settingArray,$vehicleModelID)
		{
			$str = $this->GetUpdateParams($settingArray);
			$updSql="update vehicle_model set ".$str." where id=".$vehicleModelID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteRegion($settingArray,$regionID)
		{
			$str = $this->GetUpdateParams($settingArray);
			$updSql="update regions set ".$str." where region_id=".$regionID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteZone($settingArray,$zoneID)	
		{
			$str = $this->GetUpdateParams($settingArray);
			$updSql="update zone set ".$str." where zone_id=".$zoneID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}

		function GetSiteSettings(){

			$sql = "SELECT * from settings limit 1";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			$this->conn->SetFetchMode(ADODB_FETCH_ASSOC);
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs[0];

		}

		function UpdateSiteSettings($info){

			$str = $this->GetUpdateParams($info);
			$updSql="update settings set ".$str." where id=".$info['id'];
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
}