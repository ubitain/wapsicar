 <?php

	class DL_Finish extends DataAccessBase
	{
		function finish($addstuff,$claimID)
		{
			$str = $this->GetUpdateParams($addstuff);
			$updSql="update claims set ".$str." where claim_id=".$claimID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function ProccedToFinanceDepart($updatearray,$claimId)
		{
			$str = $this->GetUpdateParams($updatearray);
			$updSql="update claims set ".$str." where claim_id=".$claimId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
	}

?>