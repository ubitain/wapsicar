 <?php

	class DL_AcceptRejectQuantity extends DataAccessBase
	{
		function UpdateAcceptRejectQuantity($updatearray,$claimId,$claimIdId)
		{
			$str = $this->GetUpdateParams($updatearray);
			$updSql="update claim_details set ".$str." where claim_id=".$claimId." and claim_detail_id=".$claimIdId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateReceivingWeight($ReceivingWeight,$claimId)
		{
			//$str = $this->GetUpdateParams($updatearray);
			$updSql="update claims set receiving_weight=".$ReceivingWeight." where claim_id=".$claimId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
	}

?>