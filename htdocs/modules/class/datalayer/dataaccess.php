 <?php
require_once(MODULE.'/class/lib/adodb/adodb.inc.php');

global $trans_conn;
global $trans_state;
$trans_state = 0;

class TransactionManager
{
	var $dbHost;
	var $dbName;
	var $dbUser;
	var $dbPwd;

	function TransactionManager()
	{
		$this->dbHost=DESKTOPDBHOSTNAME;
		$this->dbName=DESKTOPDBDATABASE;
		$this->dbUser=DESKTOPDBUSERNAME;
		$this->dbPwd=DESKTOPDBPASSWORD;
		//echo "$dbHost-$dbName-$dbUser-$dbPwd";
	}

	function BeginTrans()
	{
		$database = "mysqlt";
		global $trans_conn;
		global $trans_state;

		//echo "<br>!begin!$trans_state!!";
		if ($trans_state>0)
		{
			$trans_state++;
			//traceMessage("Ignoring Begin Trans, already in trans ...");
			return true;
		}

		//traceMessage("Starting trans ...($trans_state)");
		$trans_conn = NewADOConnection($database);
		$trans_conn->debug = false;

		if (!$trans_conn->Connect($this->dbHost,$this->dbUser,$this->dbPwd,$this->dbName))
		{
			$msg = "Message: Unable to establish connection with '$database', err:".$this->conn->ErrorMsg();
			spongLog($msg,"ERROR");
			$e = new ErrorData($msg,__FILE__,__LINE__,"error");
			ReportError($e);
			return false;
		}
		//$s = "set character set utf8";
		$s = "set names utf8";
		$trans_conn->Execute($s);
		$trans_conn->StartTrans();
		$trans_state++;
		return true;
	}

	function CommitTrans()
	{
		global $trans_conn;
		global $trans_state;

		//echo "<br>!comit!$trans_state!!";
		if ($trans_state>1)
		{
			$trans_state--;
			//traceMessage("Ignoring Commit Trans, already in trans ...");
			return true;
		}

		//traceMessage("Committing trans ...($trans_state)");
		if (!$trans_conn->CompleteTrans())
		{
			$msg = "Message: ".$trans_conn->_errorMsg;
			spongLog($msg,"ERROR");
			$e = new ErrorData("$msg",__FILE__,__LINE__,"error");
			ReportError($e);
			return FALSE;
		}
		$trans_state--;
		$trans_conn->Close();
		//traceMessage("Committing done ($trans_state)");
		return true;
	}

	function FailTrans()
	{
		global $trans_conn;
		global $trans_state;
		$trans_conn->FailTrans();
		$trans_state = 0;
		$trans_conn->Close();
		return true;
	}
}

class DataAccessBase
{
	var $conn;

	function Connect($database,$dbHost="",$dbName="",$dbUser="",$dbPwd="")
	{
		global $trans_state;
		if ($database == "mysql") {
			$database = "mysqli";
		}
		if ($database == "pgsql")
		{
			$dbHost=PGHOST;
			$dbName=PGDB;
			$dbUser=PGUSERID;
			$dbPwd=PGPASSWORD;
		}
		else if ($database == "mysql" || $database == "mysqli")
		{
			if ($dbHost=="" && $dbName=="" && $dbUser=="" && $dbPwd=="")
			{
				$dbHost=DESKTOPDBHOSTNAME;
				$dbName=DESKTOPDBDATABASE;
				$dbUser=DESKTOPDBUSERNAME;
				$dbPwd=DESKTOPDBPASSWORD;
			}
		}
		else
		{
			$e = new ErrorData("Unable to establish connection with '$database',$dbHost",__FILE__,__LINE__,"error");
			ReportError($e);
			return false;
		}

		if ($trans_state > 0)
		{
			traceMessage("Returning connection as Transaction is active");
			return true;
		}

		$this->conn = NewADOConnection($database);
		$this->conn->debug = false;

		//print("CON:$dbHost,$dbUser,$dbPwd,$dbName");
		if (!$this->conn->Connect($dbHost,$dbUser,$dbPwd,$dbName))
		{
			$msg = "Message: Unable to establish connection with '$database', err:".$this->conn->ErrorMsg();
			spongLog($msg,"ERROR");
			$e = new ErrorData($msg,__FILE__,__LINE__,"error");
			ReportError($e);
			return false;
		}
		//$s = "set character set utf8";
		$s = "set names utf8";
		$this->conn->Execute($s);
		return true;
	}

	function ExecuteQuery($query)
	{
		global $trans_conn;
		global $trans_state;

		if ($trans_state > 0)
		{
			$e = new ErrorData("TRANS Query: ".$query,__FILE__,__LINE__,"debug");
			ReportError($e);
			$rs = $trans_conn->Execute($query);
			$con = $trans_conn;
		}
		else
		{
			logMsg("Executed Query: ".$query,"debug");
			
			$this->conn->SetFetchMode(ADODB_FETCH_ASSOC);
			$rs = $this->conn->Execute($query);
			$con = $this->conn;
		}

		if(!is_object($rs))
		{
			$msg = "Message: ".$con->ErrorMsg()."SQL: ".$query;
			spongLog($msg,"ERROR");
			$e = new ErrorData("$msg",__FILE__,__LINE__,"error");
			ReportError($e);
			return null;
		}

		$arr = $rs->GetRows();
		$rs->Close();

		//traceMessage("Fetched:".print_r_log($arr));
		return $arr;

	}

	function ExecuteNonQuery($query)
	{
		global $trans_conn;
		global $trans_state;

		if(ENABLE_QUERY_BACKUP=="1")
			LogQuery($query);
		if ($trans_state > 0)
		{
			$e = new ErrorData("TRANS Query: ".$query,__FILE__,__LINE__,"debug");
			ReportError($e);
			$rs = $trans_conn->Execute($query);
			$con = $trans_conn;
		}
		else
		{
			logmsg("Executed Query: ".$query);
			$rs = $this->conn->Execute($query);
			$con = $this->conn;
		}

		if(!$rs)
		{
			$msg = "Message: ".$con->ErrorMsg()."SQL: ".$query;
			spongLog($msg,"ERROR");
			$e = new ErrorData("$msg",__FILE__,__LINE__,"error");
			ReportError($e);
			return FALSE;
		}

		return TRUE;
	}

	function RowsAffected()
	{
		global $trans_conn;
		global $trans_state;

		if ($trans_state > 0)
			return $trans_conn->Affected_Rows();
		else
			return $this->conn->Affected_Rows();
	}

	function GetInsertId()
	{
		global $trans_conn;
		global $trans_state;

		if ($trans_state > 0)
			$id= $trans_conn->Insert_ID();
		else
			$id= $this->conn->Insert_ID();
		return $id;
	}

	function Dispose()
	{
		global $trans_state;
		if ($trans_state > 0)
		{
			//traceMessage("Returning Close as Transaction is active");
			return true;
		}
		else
			$this->conn->Close();
	}

	function GetUpdateParams($arr)
	{
		$str = "";
		foreach ($arr as $key=>$value)
		{
			// set value = '' if not provided
			$str .= ($value != "")?"$ch $key='$value'":"$ch $key=''";

			// ignore update if value not provided
			//$str .= ($value != "")?"$ch $key='$value'":"";
			$ch = ",";
		}
		return $str;
	}

	function GetInsertParams($arr)
	{
		$str = "";
		foreach ($arr as $key=>$value)
		{
			$keys .= $ch."`$key`";
			$values .= ($value != "")?"$ch'$value' ":"$ch''";
			$ch = ",";
		}

		$str = "( $keys ) values ( $values )";
		return $str;
	}
}

?>
