 <?php
	traceMessage("dl");
	class DL_Product extends DataAccessBase
	{
		public function AddProduct($productInfo)
		{
			$insStr=$this->GetInsertParams($productInfo);
			$insSql="Insert into product $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateProduct($productInfo,$productId)
		{
			$str = $this->GetUpdateParams($productInfo);
			traceMessage("str ".print_r_log($str));
			$updSql="update product set ".$str." where product_id=".$productId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAllProduct($status)
		{
			$query = "SELECT * FROM `product` where STATUS = '$status'";
			traceMessage("all product".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAProduct($id,$status)
		{
			$query = "SELECT * FROM `product` where product_id='$id' and status = '$status'";
			traceMessage("GetAProduct ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function UpdateCompetitorProductData($cProductInfo,$productId,$cProductId)
		{
			traceMessage("productId".$productId);
			traceMessage("cProductId".$cProductId);
			traceMessage("productdata".print_r_log($cProductInfo));
			$sql = "SELECT COUNT(*) AS productcount FROM competitor_products WHERE c_product_id =".$cProductId;
			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			traceMessage("count is ".print_r_log($rs));
			$countProduct=$rs[0]['productcount'];
			traceMessage("count of Product is ".$countProduct);
			if($countProduct > 0)
			{
				$str = $this->GetUpdateParams($cProductInfo);
				traceMessage("str ".print_r_log($str));
				$updSql="update competitor_products set ".$str." where c_product_id=".$cProductId;
				traceMessage("sql:$updSql");
				if (!$this->ExecuteNonQuery($updSql))
				{
					$this->Dispose();
					return false;
				}
				$upPcpSql="Update products_competitor_products set product_id='$productId' where cp_id='$cProductId' ";
				if (!$this->ExecuteNonQuery($upPcpSql))
				{
					$this->Dispose();
					return false;
				}
				$this->Dispose();
				return true;
			}
			else
			{
				$insStr=$this->GetInsertParams($cProductInfo);
				$insSql="Insert into competitor_products $insStr";
				traceMessage("Insert String ".$insSql);
				if (!$this->ExecuteNonQuery($insSql))
				{
					$this->Dispose();
					return false;
				}
				$retId = $this->GetInsertId();
				$insPcpSql="Insert into products_competitor_products (product_id,cp_id) VALUES ('$productId','$retId')";
				if (!$this->ExecuteNonQuery($insPcpSql))
				{
					$this->Dispose();
					return false;
				}
				$this->Dispose();
				return true;
			}
		}


		function GetCompetitorProductsByCompetitorProductId($cProductId)
		{
			if($cProductId==0 || $cProductId=="")
			{
				$query = "SELECT cp.* , product_id FROM products_competitor_products pcp , competitor_products cp WHERE pcp.cp_id=cp.c_product_id";
				traceMessage("all doctors".$query);
			}
			else
			{
				$query= "SELECT * FROM `competitor_products` WHERE c_product_id = '$cProductId' and STATUS = '1'";
				traceMessage("1 product".$query);
			}
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}



		function checkProductCode($data)
		{

			$productCode = $data['code'];
			$query = "SELECT * FROM product WHERE product_code = '$productCode';";
			traceMessage("check product code query".$query);


			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}



		function checkAtProductCode($data)
		{
			$productCode = $data['code'];
			$query = "SELECT * FROM product WHERE at_product_code = '$productCode';";
			traceMessage("check at product code query".$query);

			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GreyProductReport($data)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`date`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT c.*,g.* from grey_product g, customer c where g.`customer_id`=c.`customer_id` and user_id IN ($childId) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// $data = new GenericData();
			$data = array();
			for($i=0; $i < count($rs);  $i++){
				$customer_id = $rs[$i][0];
				$data[$customer_id]['images'][] = $rs[$i]['image_url'];
				$data[$customer_id]['customer_name'] = $rs[$i]['customer_name'];
				$data[$customer_id]['ledger_id'] = $rs[$i]['ledger_id'];
				$data[$customer_id]['grey_product_name'] = $rs[$i]['grey_product_name'];
				// $data->AddRow($rs[$i]);
			}
			return $data;
		}
	}
?>
