 <?php

	class DL_Route extends DataAccessBase
	{
		function AddCity($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `city` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateCity($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update city set ".$str." where city_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}

		function GetCityById($city_id)
		{
			$sql = "SELECT * from city where city_id=$city_id";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		
		function AddRoute($addstuff)
		{
			$str=$this->GetInsertParams($addstuff);
			$sql="insert into `routes` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateRoute($addstuff,$route_id)
		{
			$str = $this->GetUpdateParams($addstuff);
			$updSql="update routes set ".$str." where route_id=".$route_id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}

		function GetRoute()
		{
			$sql = "SELECT r.*, c1.city_name as from_city_name , c2.city_name as to_city_name from routes r, city c1, city c2 where r.status >=0 and c1.city_id = r.to_city and c2.city_id = r.to_city";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function DeleteRoute($route_id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update routes set status='-1' where route_id=".$route_id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetRouteByIds($from_city,$to_city)
		{
			$sql = "SELECT * from routes where to_city='$to_city' and from_city='$from_city' and status = 1";
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
	}

?>