 <?php

	class DL_Documents extends DataAccessBase
	{
		function GetDocumentListing()
		{
			
			$sql = "SELECT * from documents";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}
		function GetUserDocuments($userid)
		{
			$sql = "SELECT documents.document_id, documents.`document_name`, user_documents.document_path FROM documents LEFT JOIN user_documents ON documents.document_id = user_documents.document_id AND user_documents.status = 1 AND user_id = '$userid' WHERE documents.status = 1 ";

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}

		function AddUserDocuments($info)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="insert into user_documents $inst";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}

		function AddUserDocumentInfo($info)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="insert into user_document_info $inst";
			//echo($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		
		function GetUserDocumentsByType($userId,$documentId)
		{
			$sql = "select * from user_document_info where user_id='$userId' and document_id='$documentId' order by id desc limit 1;";
			$sql1 = "select * from user_documents where user_id='$userId' and document_id='$documentId';";

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			if (!is_array($rs1 = $this->ExecuteQuery($sql1)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return array($rs,$rs1);
			
		}

		
	}