 <?php
	traceMessage("dl");
	class DL_TeamMapping extends DataAccessBase
	{
		function MappingTeam($arr)
		{
			$sql = "SELECT count(*) as mapcheck FROM user_mapping WHERE parent_id='".$arr['parent_id']."' AND child_id='".$arr['child_id']."' AND status='1'";
			traceMessage(" check Sql : ".$sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$mapExist = $rs[0]['mapcheck'];

			if($mapExist>0)
			{
					traceMessage("mapExist  ".$mapExist);
					$map="User Already Mapped";
					return $map;
			}
			else
			{
				traceMessage("map not exist".$mapExist);
				traceMessage("in DL MappingTeam".print_r_log($arr));
				$insStr=$this->GetInsertParams($arr);
				$insSql="Insert into user_mapping $insStr";
				traceMessage("Insert String ".$insSql);
				if (!$this->Connect('mysql'))
				{
					traceMessage("Database connection error ".mysql_error());
					return null;
				}
				if (!$this->ExecuteNonQuery($insSql))
				{
					$this->Dispose();
					return false;
				}
				$this->Dispose();
				$map="User Mapped Successfully";
				return $map;
			}

		}
		function GetSeByTeamAndCity($team,$cityName)
		{
			$sql = "SELECT * FROM users WHERE user_type=".USER_TYPE_SE." AND team='$team' OR city_name='$cityName'";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}

		function GetAsmCityAndTeam($asmId)
		{
			$sql = "SELECT * FROM users WHERE user_id='$asmId' AND user_type='2'";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}
		// View team mapping functions
		function ViewTeamMapping($parentId,$userType)
		{
			traceMessage("In dl ViewTeamMapping($parentId)");
			$sqlExtra = "";
			if ($userType !="") {
				$sqlExtra = "user_type=$userType and";
			}
			$sql = "select * from users where $sqlExtra user_id IN (select child_id from user_mapping where parent_id='$parentId' and status='1')";
			traceMessage("ViewTeamMapping    ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}

		/******************************************SE Performance Report************************************************/
		function GetASMByTeamName($team)
		{
			traceMessage("In dl GetASMByTeamName  ($team)");

			// exit;
			$sql = "select * from users where team = '$team' and user_type = '".USER_TYPE_DSR."'and status='1'";
			traceMessage("GetASMByTeamName ".$sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}

		function GetQuarterMonths($currentMonth)
		{
			traceMessage("In dl GetQuarterMonths".$currentMonth);

			// exit;
			// $sql = "SELECT * FROM  quarter_months WHERE  QUARTER IN (SELECT `quarter` FROM `quarter_months` WHERE `month`='$currentMonth')";
			$sql = "SELECT * FROM  quarter_months";
			traceMessage("GetQuarterMonths ".$sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}

		function GetTargetAndSalesAndDiscountMonthByMonth($currentMonth,$currentYear,$seId)
		{
			traceMessage("In dl GetTargetAndSalesAndDiscountMonthByMonth".$currentMonth,$currentYear,$seId);

			// exit;
			$sql = "SELECT ROUND(SUM(NSP*`Budget`)) AS budget, ROUND(SUM(NSP*`MnP_Units`)) AS sales, ROUND(SUM(`SpecialDiscountValue`),2) AS discount FROM FFPerson_Sales fp, users u WHERE fp.`PositionCode`=u.`sse_code` AND `UpdateDate` LIKE '%$currentMonth%' AND `year` LIKE '%$currentYear%' AND u.`user_id`='$seId'";
			traceMessage("GetTargetAndSalesAndDiscountMonthByMonth ".$sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}

		function GetExpenseMonthByMonth($currentMonth,$currentYear,$seId)
		{
			traceMessage("In dl GetExpenseMonthByMonth".$currentMonth,$currentYear,$seId);

			$sql = "SELECT SUM(workTypeAmount+milegeAmount+SundriesAmount) AS expense FROM  `ExpenseAgainstWorking`
					WHERE expenseId IN
					(SELECT expenseId FROM `ExpenseHeader` eh, users u
					WHERE eh.positionCode=u.`sse_code` AND u.`user_id`='$seId' AND`month`='$currentMonth' AND `year`='$currentYear')";

			traceMessage("GetExpenseMonthByMonth ".$sql);

			$policyQuery="SELECT SUM(amount) as policyExpense FROM ExpenseAgainstPolicy WHERE ExpenseId in (SELECT expenseId FROM `ExpenseHeader` eh, users u
					WHERE eh.positionCode=u.`sse_code` AND u.`user_id`='$seId' AND`month`='$currentMonth' AND `year`='$currentYear')";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			if (!is_array($rsPolicy = $this->ExecuteQuery($policyQuery)))
			{
				$this->Dispose();
				return null;
			}

			$this->Dispose();

			$data = new GenericData();

			$rs[0]['expense']+=$rsPolicy[0]['policyExpense'];

			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);

			return $data;
		}
	}
?>
