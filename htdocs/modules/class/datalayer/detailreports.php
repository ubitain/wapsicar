 <?php

	class DL_Reports extends DataAccessBase
	{	
		function GetProductDetail()
    	{
        $query = "SELECT * from items ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    	}
    	function FetchClaimItemCount($itemID)
    	{
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT * from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    	}
        function FetchClaimItemCountDate($itemID,$searchFrom,$searchTo)
        {

        $query = "SELECT * from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
    	function FetchClaimItemAccepted($itemID)
    	{
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT SUM(approved_qty) AS accepted from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    	}
        function FetchClaimItemAcceptedDate($itemID,$searchFrom,$searchTo)
        {
        $query = "SELECT SUM(approved_qty) AS accepted from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
    	function FetchClaimItemRejected($itemID)
    	{
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT SUM(rejected_qty) AS rejected from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    	}
        function FetchClaimItemRejectedDate($itemID,$searchFrom,$searchTo)
        {
        $query = "SELECT SUM(rejected_qty) AS rejected from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
    	function FetchClaimItemOthers($itemID)
    	{
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT SUM(other_qty) AS others from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    	}
        function FetchClaimItemOthersDate($itemID,$searchFrom,$searchTo)
        {
        $query = "SELECT SUM(other_qty) AS others from claim_details where item_id=".$itemID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
		function UpdateRegion($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update regions set ".$str." where region_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
        //++++++++++
        function GetRegionDetail()
        {
        $query = "SELECT * FROM claims c, users u WHERE u.user_id=c.purchased_from";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetUserDetail()
        {
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT * FROM claim_transactions_log where created_date BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetUserDetailDate($searchFrom,$searchTo)
        {
        $query = "SELECT * FROM claim_transactions_log where created_date BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetRegionName()
        {
        $query = "SELECT * FROM regions ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetUserName()
        {
        $query = "SELECT * FROM users where dealer_category_id IN (SELECT dealer_category_id FROM dealer_category WHERE dealer_category_name IN ('Mechanics','SubDealers','Dealers','Distributor'))";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetRegionAccRejOth($claimID)
        {
        $searchTo = date("Y-m-d");
        $searchFrom = date("Y-m-01");
        $query = "SELECT * FROM claim_details where claim_id=".$claimID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
        function GetRegionAccRejOthDate($claimID,$searchFrom,$searchTo)
        {
        $query = "SELECT * FROM claim_details where claim_id=".$claimID." and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
        }
	}

?>