 <?php
	traceMessage("dl");
	class DL_CallTarget extends DataAccessBase
	{
		function AddCallTarget($arr)
		{
			traceMessage("AddCallTarget".print_r_log($arr));
			$insStr=$this->GetInsertParams($arr);
			$insSql="Insert into `visit_target` $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAllCallTarget()
		{
			$sql = "SELECT * FROM `visit_target` vt INNER JOIN `users` u ON vt.`user_id`=u.`user_id` WHERE vt.STATUS = '1'";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetOneCallTarget($recordId)
		{
			$sql = "SELECT * FROM `visit_target` WHERE record_id='$recordId' AND STATUS = '1'";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function EditCallTarget($recordId,$callTargetInfo)
		{
			traceMessage("EditCallTarget".print_r_log($callTargetInfo));
			$str = $this->GetUpdateParams($callTargetInfo);
			traceMessage("str ".print_r_log($str));
			$updSql="update `visit_target` set ".$str." where record_id=".$recordId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteCallTarget($recordId)
    	{
			traceMessage("in dl DeleteCallTarget".$recordId);
			$sql1="UPDATE  visit_target SET status = '-1' WHERE record_id = '$recordId'";
			traceMessage("sql:$sql1");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql1))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetWorkingDays($month)
		{
			$sql = "SELECT COUNT(DISTINCT month_days) as total_days FROM `itinerary` WHERE MONTH(`upload_date`)=$month AND days!='Sun' AND days!='SUNDAY' GROUP BY se_id LIMIT 1";
			traceMessage("working days sql is ".$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
	}
?>
