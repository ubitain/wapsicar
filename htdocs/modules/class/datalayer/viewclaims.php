<?php
class DL_ViewClaims extends DataAccessBase
{
	function ViewClaims()
    {
        $query = "SELECT * from claims   ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsTotal()
    {
        $query = "SELECT * from claims where claim_status!='open'  ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsDates($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where  created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsNo($claimNoSearch)
    {
        $query = "SELECT * from claims where  claim_no='".$claimNoSearch."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ClaimsToday()
    {
        $query = "SELECT claims.claim_id, DATE_FORMAT(claims.created_at, '%Y-%m-%d') FROM claims WHERE DATE(created_at) = CURDATE() and claim_status!='open'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ClaimsMonth()
    {
        $query = "SELECT * FROM claims WHERE MONTH(created_at) = MONTH(CURRENT_DATE())AND YEAR(created_at) = YEAR(CURRENT_DATE()) and claim_status!='open'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ClaimsYear()
    {
        $query = "SELECT * FROM claims WHERE  YEAR(created_at) = YEAR(CURRENT_DATE()) and claim_status!='open'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewTopProductsInClaim()
    {
        $query = "SELECT  item_id as ITEM_ID, SUM(`recieved_qty`) AS TotalQuantity FROM claim_details c GROUP BY item_id ORDER BY SUM(`recieved_qty`) DESC LIMIT 3";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchProductName($itemID)
    {
        $query = "SELECT  item_name as ITEM_NAME,item_code FROM items WHERE item_id=".$itemID;
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewPendingClaimsDates($searchFrom,$searchTo)
    {
        // $query = "SELECT * from claims where claim_status='approved'and forwarded_status='1' and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC ";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewSendTolLeopardClaimsDates($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where claim_status='approved'and forwarded_status='0' and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewPendingClaims()
    {
        // $query = "SELECT * from claims where claim_status='approved' and forwarded_status='1' ORDER BY claim_id DESC ";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewSendTolLeopardClaims()
    {
        $query = "SELECT * from claims where claim_status='approved' and forwarded_status='0' ORDER BY claim_id DESC ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewPendingClaimNo($claimNoSearch)
    {
        // $query = "SELECT * from claims where claim_status='approved' and forwarded_status='1' and claim_no='".$claimNoSearch."' ";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewSendTolLeopardClaimNo($claimNoSearch)
    {
        $query = "SELECT * from claims where claim_status='approved' and forwarded_status='0' and claim_no='".$claimNoSearch."' ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInProcessClaimsDate($searchFrom,$searchTo)
    {
        // $query = "SELECT * from claims where claim_status='inprocess' and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewInProcessClaims()
    {
        // $query = "SELECT * from claims where claim_status='inprocess' ORDER BY claim_id DESC";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewInProcessClaimNo($claimNoSearch)
    {
        // $query = "SELECT * from claims where claim_status='inprocess' and claim_no='".$claimNoSearch."' ";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewInProcessResolveClaimsDate($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where claim_status='completed' and created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)  ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInProcessResolveClaims()
    {
        $query = "SELECT * from claims where claim_status='completed'  ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInProcessResolveClaimsDatee($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where claim_status='completed' AND created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE)  ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInProcessResolveClaimNoo($claimNoSearch)
    {
        $query = "SELECT * from claims where claim_status='completed' and claim_no='".$claimNoSearch."'  ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInProcessResolveClaimNo($claimNoSearch)
    {
        $query = "SELECT * from claims where claim_status='completed'  and claim_no='".$claimNoSearch."' ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewIncompleteClaims()
    {
        // $query = "SELECT * from claims ORDER BY claim_id DESC";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewInCompleteClaimsDates($searchFrom,$searchTo)
    {
        // $query = "SELECT * from claims where created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewInCompleteClaimsDatess($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where created_at BETWEEN CAST('".$searchFrom."' AS DATE) AND CAST('".$searchTo."' AS DATE) ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInCompleteClaimsNo($claimNoSearch)
    {
        // $query = "SELECT * from claims where claim_no='".$claimNoSearch."'";
      
        //     if (!$this->Connect('mysql')) {
        //     traceMessage("Database connection error ".mssql_get_last_message());
        //     return null;
        // }
        // // Executing Query in Sql
        // if (!is_array($result = $this->ExecuteQuery($query))) {
        //     $this->Dispose();
        //     return null;
        // }
        // $this->Dispose();
        // $data = new GenericData();
        // for($i=0; $i < count($result);  $i++)
        // $data->AddRow($result[$i]);
        // return $data;
    }
    function ViewCompletedClaims()
    {
        $query = "SELECT * from claims where claim_status='closed' ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewCompletedClaimsDate($searchFrom,$searchTo)
    {
        $query = "SELECT * from claims where claim_status='closed' and created_at BETWEEN '".$searchFrom."' AND '".$searchTo."'  ORDER BY claim_id DESC";
        
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewCompletedClaimsClaimNo($claimNoSearch)
    {
        $query = "SELECT * from claims where claim_status='closed' and claim_no='".$claimNoSearch."' ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewInCompletedClaims()
    {
        $query = "SELECT * from claims where claim_status='open' ORDER BY claim_id DESC";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsDetail($claimId)
    {
        $query = "SELECT * from claims where claim_id='".$claimId."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsProductImages($claimId)
    {
        $query = "SELECT * from claim_images where claim_id='".$claimId."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsProductDetail($claimId)
    {
        $query = "SELECT * from claim_details where claim_id='".$claimId."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsPurchaseHierarchyDetail($claimId)
    {
        $query = "SELECT * from claim_transactions where claim_id='".$claimId."' and assigned_to='Purchase'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchPurchaseName($userId)
    {
        $query = "SELECT * FROM users u,dealer_category d WHERE d.dealer_category_id=u.dealer_category_id AND u.user_id='".$userId."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchUserNameTrans($claimID)
    {
        $query = "SELECT user_id FROM claim_transactions WHERE  claim_id=".$claimID."";
    //traceMessage(print_r_log($query."HSAODHAOSDIHASDOISHADS11111111111111".$claimID));
        
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchUserName($claimID)
    {
        $query = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimID."";
    traceMessage(print_r_log($query."HSAODHAOSDIHASDOISHADS11111111111111".$claimID));
        
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchDealerName($dealerID)
    {
        $query = "SELECT * from users u,dealer_category t where u.user_id='".$dealerID."' and t.dealer_category_id=u.dealer_category_id";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchClaimClaimerAccount($claimNo)
    {
        $query = "SELECT * from claims where claim_no='".$claimNo."' ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchClaimTransactionsPurchase($claimID)
    {
        $query = "SELECT * from claim_transactions where claim_id=".$claimID." and assigned_to='purchase'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchClaimTransactions($claimID)
    {
        $query = "SELECT * from claim_transactions where claim_id=".$claimID." and assigned_to='account'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchItemName($claimItemId)
    {
        $query = "SELECT * from items where item_id='".$claimItemId."'";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function FetchVoucherDetail($claimID)
    {
        $query = "SELECT * from claim_finalsettlement where claim_id='".$claimID."' and status=1";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewClaimsGrandTotal($claimId)
    {
        $query = "SELECT * from claim_amount where claim_id='".$claimId."'";
        //echo $query;
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
}

?>