 <?php
	traceMessage("dl");
	class DL_Dashboard extends DataAccessBase
	{
		function GetCurrentMonthDistanceByUserId($userId,$data)
    	{
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`location_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT ROUND(SUM(`distance`),2) AS totalDistance FROM user_tracking
			WHERE user_id=$userId ".$sqlExtra;
			traceMessage("filterStr2 ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetCurrentMonthTimeSpentByUserId($userId,$data)
    	{
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`location_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT location_time as toTime FROM user_tracking
			WHERE user_id=$userId".$sqlExtra." order by location_time asc limit 1";
			traceMessage("GetCurrentMonthTimeSpentByUserId  ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$toTime=$rs[0]['toTime'];
			$sql2 = "SELECT location_time as fromTime FROM user_tracking
			WHERE user_id=$userId AND location_time
			BETWEEN  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() order by record_id desc limit 1";
			if (!is_array($rs2 = $this->ExecuteQuery($sql2)))
			{
				$this->Dispose();
				return null;
			}
			$fromTime=$rs2[0]['fromTime'];
			$data = array($toTime,$fromTime);
			// print_r_pre($data);
			return $data;
		}

		function GetSalesOrderReports($data = array()){
			// echo "<pre>";
			// print_r($data);
			// exit();

			$sql = "SELECT SUM(rate), sales_order.`status` FROM sales_product
					INNER JOIN sales_order ON sales_order.sales_order_id = sales_product.`sales_order_id`
					GROUP BY sales_order.`status`";
			traceMessage("sales order report query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}

		function GetVisitPlans($data = array()){



			$date = date("Y-m-d");
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra = " AND date(`plan_date`) between '$fromDate' AND '$toDate'";
			}else
			{
				$fromDate = date("Y-m-01");
				$toDate = date("Y-m-t", strtotime($fromDate));
				$sqlExtra= " AND date(`plan_date`) between '$fromDate' AND '$toDate'";
			}

			$sql = "SELECT users.first_name,users.last_name,date(visit_plan.plan_date) as planedDate,
					 customer.customer_name ,visit_plan.* FROM `visit_plan`
					INNER JOIN users ON users.user_id = visit_plan.user_id
					INNER JOIN customer ON visit_plan.customer_id = customer.customer_id
					WHERE visit_plan.`user_id` IN ($childId) $sqlExtra";

			// echo $sql."<br>";

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}


		function GetCompletedVisits($visitsData = array(),$data = array()){


			$userIds = implode(', ', $visitsData['user_id']);
			$planDates = implode(', ', $visitsData['plan_date']);

			if($data['visit_type'] == "sales"){

				$sql = "SELECT user_id,visits.visit_id,visit_survey.visit_id AS survey_visit_ids, date(visited_time) as visited_time
						FROM `visits`
						INNER JOIN visit_survey ON visit_survey.visit_id = visits.visit_id
						WHERE visits.status='1' and user_id IN ($userIds) AND date(visited_time) IN ($planDates) GROUP BY visits.visit_id;";
				// echo $sql;

			}else{

				$sql = "SELECT user_id,visits.visit_id,visit_recovery.visit_id AS recovery_visit_ids,  date(visited_time) as visited_time
						FROM `visits`
						INNER JOIN visit_recovery ON visit_recovery.visit_id = visit_recovery.visit_id
						WHERE visits.status='1' and user_id IN ($userIds) AND date(visited_time) IN ($planDates) GROUP BY visits.visit_id;";
			}

			// echo $sql."<br>";

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}


		function GetOtherCompletedVisits($visitsData = array(), $data = array()){

			// echo "<pre>";
			// print_r([$data]);
			// exit();


			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}else
			{
				$fromDate = date("Y-m-01");
				$toDate = date("Y-m-t", strtotime($fromDate));
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}

			$userIds = implode(', ', $visitsData['user_id']);
			$planDates = implode(', ', $visitsData['plan_date']);

			$sql = "SELECT user_id,visit_id, date(visited_time) as visited_time FROM `visits` WHERE  visits.status='1' and user_id IN ($userIds) AND date(visited_time) NOT IN($planDates) $sqlExtra ;";

			// echo $sql."<br>";

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}



		function GetRecoveryCompletedVisits($data = array()){

			// $userIds = implode(', ', $visitsData['user_id']);
			// $planDates = implode(', ', $visitsData['plan_date']);


			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra = " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}else
			{
				$fromDate = date("Y-m-01");
				$toDate = date("Y-m-t", strtotime($fromDate));
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}

			$sql = "SELECT users.user_id,users.first_name,users.last_name,customer.ledger_id,customer.customer_name,visits.visit_id,visit_recovery.visit_id AS recovery_visit_ids,visit_recovery.amount,
				DATE(visited_time) AS visited_time FROM `visits`
				INNER JOIN users ON users.`user_id` = visits.`user_id`
				INNER JOIN customer ON customer.`customer_id` = visits.`customer_id`
				INNER JOIN visit_recovery ON visit_recovery.visit_id =  visits.visit_id
				 WHERE  visits.status='1' and visits.user_id IN ($childId) $sqlExtra GROUP BY visits.visit_id;";


			// echo $sql."<br>";

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}


		function GetPlannedVisitsByIds($data = array()){


			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit();

			$date = date("Y-m-d");
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`plan_date`) between '$fromDate' AND '$toDate'";
			}else
			{
				$fromDate = date("Y-m-01");
				$toDate = date("Y-m-t", strtotime($fromDate));
				$sqlExtra= " AND date(`plan_date`) between '$fromDate' AND '$toDate'";
			}

			$sql = "SELECT users.first_name,users.last_name, date(visit_plan.plan_date) as planedDate,
					visit_plan.*,customer.customer_name,customer.ledger_id
					FROM `visit_plan`
					INNER JOIN users ON users.user_id = visit_plan.user_id
					INNER JOIN customer ON visit_plan.customer_id = customer.customer_id
					WHERE visit_plan.`user_id` IN ($childId) $sqlExtra";

			// echo $sql;

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}

		function GetRemainigVisits($visitsData){

			$userIds = implode(', ', $visitsData['user_id']);
			$planDates = implode(', ', $visitsData['plan_date']);

			$sql = "SELECT user_id,visit_id,customer_id, date(visited_time) as visited_time FROM `visits` WHERE  visits.status='1' and user_id IN ($userIds) AND date(visited_time) IN ($planDates);";

			// echo $sql;

			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;


		}
		function GetCalls($visitType,$childIds,$fromDate,$toDate)
		{
			$sql = "select count(*) as ccount from visits where
					visit_type='$visitType'
					and visited_time between '$fromDate' and '$toDate'
					and user_id in ($childIds)";
			//echo $sql."<br>";
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();

			return $rs[0]["ccount"];
		}



	}
?>
