 <?php
traceMessage("dl");
class DL_Reports extends DataAccessBase
{
	function GetThisMonthItnerary($date,$seId)
	{
			$sql = "SELECT i.* FROM `itinerary` i , users u WHERE i.se_id=u.user_id and month(upload_date)=month('$date') and se_id='$seId' and i.status=1";
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetLocationBySaleExecutive($startDate,$endDate,$seId)
	{
			$sql = "SELECT
  sda.`visit_date`,c.`city_name`,TIME_FORMAT(sda.`visit_time`,'%r') AS visit_time
FROM
  sales_doctor_assignment sda,
  users u,
  doctors d,
  areas a,
  cities c
WHERE sda.`user_id` = u.`user_id`
  AND sda.`doctor_id` = d.`doctor_id`
  AND d.`area_code` = a.`area_code`
  AND c.`city_id` = a.`city_id`
  AND sda.`user_id`='$seId' AND sda.`status`='1' AND sda.`visit_date` BETWEEN '$startDate' AND 'endDate'
  GROUP BY sda.`visit_date`";
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetExpenseBySaleExecutive($month,$year,$seId)
	{
			$sql = "SELECT ew.`WorkType` FROM ExpenseHeader eh, ExpenseAgainstWorking ew, users u WHERE u.`sse_code` = eh.`PositionCode` AND eh.`ExpenseId`=ew.`ExpenseId` AND u.`user_id` = '$seId' AND eh.`Month` = '$month' AND eh.`Year` = '$year' ";
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetDoctorVisitByDate($seId,$date)
	{
			$sql = "SELECT * FROM sales_doctor_assignment WHERE user_id = '$seId' AND visit_date = '$date'";
			 traceMessage('datesql--'.$sql);

			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetDoctorCityByDoctorId($docId)
	{
			$sql = "SELECT * FROM doctors d,areas a,cities c WHERE d.area_code = a.area_id AND a.city_id = c.city_id AND doctor_id = '$docId'";
			 traceMessage('docsql--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetAllBricksOfFieldForce($team,$code)
	{
		$sql = "SELECT DISTINCT BrickName
				FROM   FieldForce
				INNER JOIN BrickDetails ON FieldForce.Code=BrickDetails.PositionCode
				WHERE
				FieldForce.GroupName='$team'
				AND FieldForce.Code='$code'";

			 traceMessage('docsql--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetBrickSalesOfFieldForce($team,$sseCode,$month,$year)
	{
			$month1 = date('m',strtotime($month));
			$date=cal_days_in_month(CAL_GREGORIAN,$month1,$year);

			$sql = "SELECT BrickName,ROUND(SUM((mnp_sales.TradeUnit * BrickShare)/100 * NSP)) as sales
					FROM   FieldForce
					INNER JOIN BrickDetails ON FieldForce.Code=BrickDetails.PositionCode
					INNER JOIN PRODUCTDETAILS ON FieldForce.GroupName=PRODUCTDETAILS.GroupName
					INNER JOIN Product_Mapping ON PRODUCTDETAILS.PRODUCTNAME=Product_Mapping.ProductName
					INNER JOIN mnp_sales ON (Product_Mapping.ProductCode=mnp_sales.ProductCode) AND (BrickDetails.BrickCode=mnp_sales.BrickCode)
					WHERE  mnp_sales.UpdateDate = '$month $date, $year'
					AND FieldForce.GroupName='$team'
					AND  NOT (Product_Mapping.ProductName LIKE 'ALERID SYRUP' OR Product_Mapping.ProductName LIKE 'LEVORIZ 5 MG' OR Product_Mapping.ProductName LIKE 'SNAPCEF 400 MG CAPSULES')
					AND FieldForce.Code='$sseCode'
					GROUP BY BrickName";

			 traceMessage('bricksalessql--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetMonthlyExpenseByBrick($brick,$sseCode,$month,$year)
	{
		$sql = "SELECT SUM(MilegeAmount) as mileageSum,SUM(WorkTypeAmount) as worktypeSum,SUM(SundriesAmount) as sundriesSum
				FROM ExpenseAgainstWorking ew , ExpenseHeader eh
				WHERE ew.`ExpenseId`=eh.`ExpenseId` AND ew.`To`='$brick' AND
				eh.PositionCode='$sseCode' AND eh.`month`='$month' AND eh.`year`='$year'";

			 traceMessage('bricksalessql--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetMileage($to,$from)
	{
		$sql = "select mileage from mileage_rate where `to`='$to' and `from`='$from'";

			 traceMessage('GetMileageSQL--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetStationNameByBrick($brick)
	{
			$sql = "SELECT * FROM mileage_rate WHERE to_mnp ='$brick' GROUP BY to_mnp";

			 traceMessage('GetStationNameByBrickSQL--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
	function GetSummaryExpense($sseCode,$month,$year)
	{



		$sql = "SELECT SUM(ew.`WorkTypeAmount`) AS localExpense
				FROM `ExpenseHeader` e,`ExpenseAgainstWorking` ew
				WHERE ew.`ExpenseId` = e.`ExpenseId` AND ew.`WorkType`='Local' AND e.`PositionCode` = '$sseCode' AND e.`Month`='$month' AND e.`Year`='$year';";

			 traceMessage('GetSummaryExpenselocal--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}

			$sql1 = "SELECT SUM(ew.MilegeAmount) AS mileageSum,SUM(ew.WorkTypeAmount) AS worktypeSum, SUM(ew.SundriesAmount) AS sundriesSum
				FROM `ExpenseHeader` e,`ExpenseAgainstWorking` ew
				WHERE ew.`ExpenseId` = e.`ExpenseId` AND ew.`WorkType`!='Local' AND e.`PositionCode` = '$sseCode' AND e.`Month`='$month' AND e.`Year`='$year';";

			 traceMessage('GetSummaryExpenseEx--'.$sql1);
			if (!is_array($rs1 = $this->ExecuteQuery($sql1)))
			{
				$this->Dispose();
				return null;
			}

			traceMessage(print_r_log($rs));
			$data[0]['localExpense']= $rs[0]['localExpense'];
			$data[0]['mileageSum']= $rs1[0]['mileageSum'];
			$data[0]['worktypeSum']= $rs1[0]['worktypeSum'];
			$data[0]['sundriesSum']= $rs1[0]['sundriesSum'];

			return $data;
	}


	function GetSalesOrder($data = array(),$userType){


		$date = date("Y-m-d");
		$childId = $data['child_id'];

		$sqlExtra = "";
		if ($data['from_date'] && $data['to_date'] ){
			$fromDate = $data['from_date'];
			$toDate = $data['to_date'];
			$sqlExtra= " AND date(`date`) between '$fromDate' AND '$toDate'";
		}else
		{
			$fromDate = date("Y-m-01");
			$toDate = date("Y-m-t", strtotime($fromDate));
			$sqlExtra= " AND date(`date`) between '$fromDate' AND '$toDate'";
		}

		$getByUsers = "";
		if($userType != 2){
			$getByUsers = " AND users.user_id IN ($childId)";
		}

		$statusQuery = "";
		if($data['searchbyStatus'] == "all"){

			$statusQuery = " AND (sales_order.status = 'open' OR sales_order.status = 'in_transit' OR sales_order.status = 'delivered') ";

		}else if($data['searchbyStatus'] == "open"){

			$statusQuery = " AND sales_order.status = 'open'";


		}else if($data['searchbyStatus'] == "in_transit"){

			$statusQuery = " AND sales_order.status = 'in_transit'";

		}else if($data['searchbyStatus'] == "delivered"){

			$statusQuery = " AND sales_order.status = 'delivered'";

		}




		$sql = "SELECT sales_order.sales_order_id,sales_order.invoice_id,sales_order.customer_name, date(sales_order.date) as order_date,sales_order.status, customer.ledger_id,
			(SELECT sum(rate) from sales_product ss WHERE ss.sales_order_id = sales_order.sales_order_id) total_amount,
			customer.customer_id, user_customer.user_id,users.first_name as dsr_name FROM sales_order
			INNER JOIN customer ON customer.ledger_id = sales_order.ledger_id
			INNER JOIN user_customer ON user_customer.customer_id = customer.customer_id
			INNER JOIN users ON user_customer.user_id = users.user_id WHERE 1=1 $getByUsers $statusQuery $sqlExtra ";

		// echo $sql;
		traceMessage("query ".$sql);

		if (!$this->Connect('mysql'))
		{
			return null;
		}
		if (!is_array($rs = $this->ExecuteQuery($sql)))
		{
			$this->Dispose();
			return null;
		}
		$this->Dispose();
		$data = new GenericData();
		for($i=0; $i < count($rs);  $i++)
		$data->AddRow($rs[$i]);
		return $data;

	}

	function GetRecoveryVisitsLogs($data){

		// echo $data['child_id'];

		$extraQuery = "";
		if($data['child_id'] != ""){
			$childIds = $data['child_id'];
			$extraQuery = "WHERE user_id IN ($childIds)";
		}



		$sql = "SELECT *,date(visit_recovery_log.create_at) AS log_date FROM visit_recovery_log $extraQuery;";

			 traceMessage('GetRecoveryVisitsLogs--'.$sql);
			 // echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
	}
}
