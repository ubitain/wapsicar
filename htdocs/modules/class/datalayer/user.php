<?php
	traceMessage("dl");
	class DL_User extends DataAccessBase
	{
		
		
		function DeletedDriver()
		{
			
			$sql = "SELECT * FROM `users` where status='-1' and user_type =".USER_TYPE_DRIVER ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function SelectDriverPayment($transactionNumber)
		{
			
			$sql = "SELECT * FROM `payment` where status IN (0,1,2,3) AND transaction_number='$transactionNumber'" ;
			traceMessage("sql".$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function SelectOtp($driverId,$agent)
		{
			
			$sql = "SELECT * FROM `otp_code` where agent_id =$agent  AND driver_id='$driverId' order by id desc limit 1" ;
			// echo $sql;
			traceMessage("sql".$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
    	function UpdateOtp($arr,$otp)
		{
			$str = $this->GetUpdateParams($arr);
		    $updSql="update otp_code set ".$str." where id=".$otp;
			
			// echo $updSql;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateUsers($arrr,$driverId)
		{
			
			$str = $this->GetUpdateParams($arrr);
			$updSql="update users set ".$str." where user_id=".$driverId;
			
			// echo $updSql;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeletedCustomer()
		{
			$sql = "SELECT * FROM `users` where status='-1' and user_type =".USER_TYPE_CUSTOMER;
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function PaymentPending()
		{
			
			$sql = "SELECT * FROM `payment` where status='2' ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function PaymentApprove()
		{
			
			$sql = "SELECT * FROM `payment` where status='1' ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function DriverWithPaymentApprove()
		{
			tracemessage("driver with payment approve message");
			$sql = "SELECT * FROM payment p INNER JOIN users u 
		    WHERE p.`driver_id` = u.`user_id` 
			AND u.`user_type` = '2'  AND p.`driver_id` != 0" ;

			//echo $sql;
			TraceMessage("in dl ".print_r_log($sql));
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function CustomerWithBookingPayment()
		{
			tracemessage("customer with booking payment in DL");
			$sql = "SELECT b.`user_id`,b.`record_id`record_id,b.`dropoff_location`,b.`approx_distance`,
			b.`route_rate`,b.`price`,b.`driver_rate`, b.`comission`, b.`status`,b.`customer_cnic`,u.`user_type`,u.`full_name` 
		  FROM
			booking b 
			INNER JOIN users u 
		  WHERE b.`user_id` = u.`user_id` 
			AND u.`user_type` = '4'
			GROUP BY  
                 b.`customer_cnic`";
			TraceMessage("in dl ".print_r_log($sql));
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function PaymentUnlink()
		{
			
			$sql = "SELECT * FROM `payment` where status='0' ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function PaymentConflict()
		{
			
			$sql = "SELECT * FROM `payment` where status='3' ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		
		function GetDealerCatergory()
		{
			$sql = "SELECT dealer_category_id, dealer_category_name from dealer_category";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		
		function GetSubDealerCatergory() 
		{
			$sql = "SELECT sub_category_id, sub_category_name from dealer_sub_category";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetGroups()
		{
			$sql = "SELECT `group_id`,`group_name` from `groups`";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
        function VehicleModelInformation()
		{
			$query = "SELECT * from vehicle_model ";
			
            if (!$this->Connect('mysql')) {
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			// Executing Query in Sql
			if (!is_array($result = $this->ExecuteQuery($query))) {
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($result);  $i++)
			$data->AddRow($result[$i]);
			return $data;
		}
		function GetCity()
		{
			$sql = "SELECT * from city where status >= 0 order by city_id desc";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
        function GetVehicle()
		{
			$sql = "SELECT * from vehicle_make where status in(0,1)  order by id desc";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function GetVehicleModel()
		{
			$sql = "SELECT v2.id,v1.name,v2.model_name,v2.description,v2.car_type  from vehicle_model v2 inner join vehicle_make v1 on v1.id=v2.make_id where v2.status in(0,1)";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function GetRegions()
		{
			$sql = "SELECT * from regions where status=1";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function GetRoute()
		{
			
			$sql = "SELECT r.*, c1.city_name as from_city_name , c2.city_name as to_city_name 
			from routes r, city c1, city c2 where r.status >=0 and c1.city_id = r.from_city and c2.city_id = r.to_city";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		
		function GetZones()
		{
			$sql = "SELECT * from zone where status=1";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function GetDeliveryDetails()
		{
			$sql = "SELECT * from crown_delivery_point ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function GetTown()
		{
			$sql = "SELECT * from town where status=1";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetPurchaseFromList()
		{
			$sql = "SELECT * FROM users WHERE dealer_category_id IN (SELECT dealer_category_id FROM dealer_category WHERE dealer_category_name IN ('MECHANICS','SUB_DEALERS','DEALERS'))";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		function VehicleInformation()
		{
			$sql = "SELECT * FROM vehicle_make ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetAllUsers()
		{
			$sql = "SELECT * FROM users WHERE status = 1 ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function AddUserInfo($userinfo)
		{
			
			traceMessage("add Customer Info in Action".print_r_log($userinfo));
			
			$inst=$this->GetInsertParams($userinfo);
			$insSql="Insert into users $inst";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		
		function UpdateUserInfo($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			$updSql="update users set ".$str." where user_id=".$id;
			
			//echo $updSql;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		function DeleteUser($userArray,$userID)
		{
			$str = $this->GetUpdateParams($userArray);
			$updSql="update users set ".$str." where user_id=".$userID;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		function GetSubCategory($categoryid)
		{
			
			$sql = "SELECT * from dealer_sub_category where dealer_category_id=".$categoryid;
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetRegion($zone)
		{
			
			$sql = "SELECT * from regions where zone_id=(SELECT zone_id from zone where zone_name='".$zone."')";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetCityNames($region)
		{
			
			$sql = "SELECT * from city where region_id=(SELECT region_id from regions where region_name='".$region."')";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetArea($city)
		{
			
			$sql = "SELECT * from area where city_id=(SELECT city_id from city where city_name='".$city."')";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetPurchaseFrom($zoneId,$regionId,$cityId,$areaId)
		{
			
			$sql = "SELECT * from users where zone_id='".$zoneId."' and region_id='".$regionId."' and city_id='".$cityId."' and area_id='".$areaId."'";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		function GetAccountof($zoneId,$regionId,$cityId,$areaId)
		{
			
			$sql = "SELECT * from users where zone_id='".$zoneId."' and region_id='".$regionId."' and city_id='".$cityId."' and area_id='".$areaId."' and account_of=1";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////
        function AuhenticateUser($userId,$password,$enc)
		{
			if($enc=="no")
				$password = md5($password);
			$userType = USER_TYPE_ADMIN;
			$sql = "select *,u.`email_address` as login_id from users u where u.`login_id`='$userId' and u.`password`='$password' ";
			$sql = "select *,u.`phone_number` as login_id from users u where u.`phone_number`='$userId' and u.`password`='$password' ";
			
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("err conn::");
				// traceMessage("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				traceMessage("err rs::");
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			if(count($rs)>0)
			{
				$authInfo["user_id"] = $rs[0]["user_id"];
				$authInfo["user_type"] = $rs[0]["user_type"];
				$authInfo["num_of_login_attempts"] = $rs[0]["num_of_login_attempts"];
				$authInfo["last_login_time"] = $rs[0]["last_login_time"];
				return $authInfo;
			}
			else
			return false;
		}
		function GetAllUserInfo()
		{
			$sql = "SELECT u.*, v.first_name as pfname, v.last_name as plname, v.emp_code as pempcode
			from users u Left JOIN user_mapping um on um.child_id=u.user_id
			LEFT JOIN users v on um.parent_id=v.user_id
			where u.user_type <> '1' and u.status=1
			order by u.user_type DESC";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllAdminUserInfo()
		{
			$sql = "SELECT u.* from users u where u.`user_type` IN (".USER_TYPE_FINANCE.",".USER_TYPE_ADMIN_FINANCE.",".USER_TYPE_COMPLAIN.") and u.`status`='1' order by u.`user_type` DESC";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllUserByUserType($type,$status)
		{
			
			$sql = "SELECT * FROM `users` u left join agent a on a.id=u.agent WHERE  u.user_type='$type' and u.status='$status' ORDER BY u.`user_id` DESC";
			if($status == "all")
			$sql = "SELECT * FROM `users` u left join agent a on  a.id=u.agent  WHERE u.user_type='$type' && u.status<> -1  ORDER BY u.`user_id` DESC";
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAsmInfo($customerType,$status)
		{
			if($customerType!="" && $status!=""){
				
				$sql = "SELECT * FROM `users` WHERE user_type='$customerType' AND status='$status' ORDER BY `user_id` DESC";
				
			}
			else {
				$sql = "SELECT * FROM `users` WHERE user_type='$customerType' ORDER BY `user_id` DESC";
			}
			
			traceMessage("In dl ".$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetMedRepInfoByUserId($userId,$medRepId)
		{
			traceMessage("In dl ".$medRepId);
			if($medRepId==0 || $medRepId=="")
			{
				$sql = "SELECT u.* , t.territory_code FROM `asm_users` au , `users` u , territory t WHERE au.`asm_id`='$userId' and au.`med_rep_id`=u.`user_id` and u.territory_id=t.territory_id";
			}
			else
			{
				$sql = "SELECT u.* , t.territory_code FROM `asm_users` au , `users` u , territory t WHERE au.`asm_id`='$userId' and u.user_id='$medRepId' and au.`med_rep_id`=u.`user_id` and u.territory_id=t.territory_id";
			}
			// echo $sql;
			traceMessage("query  GetMedRepInfoByUserId ".$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetUserInfo($userId)
		{
			
			$sql = "SELECT  u.* from users u  where  u.`user_id` ='$userId' ";
            // echo $Sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		
		function GetUserInfoByUserType($userType)
		{
			$sql = "SELECT  u.* from users u  where  u.`user_type` ='$userType' ";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function AddUser($arr)
    	{
			
			
			$str=$this->GetInsertParams($arr);
			$sql="insert into `users` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			
			$this->Dispose();
			return $retId;
			
		}
		function AddAsm($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `users` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateUser($arr,$id)
    	{
			$str=$this->GetUpdateParams($arr);
			$sql="update `users` set ".$str." where user_id=".$id;
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql')){
				return false;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function CheckLoginId($loginName)
		{
			$loginName = mysql_escape_string($loginName);
			$sql = "select count(*) as ccount  from users where login_id='$loginName'";
			traceMessage($sql);
			//echo "sql:$sql";
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			return $rs[0]["ccount"];
		}
		function AddEvent($info,$infoDoctor,$infoTime)
		{
			traceMessage("infoDocTime".print_r_log($infoDoctor));
			traceMessage("infoDoc".print_r_log($infoTime));
			traceMessage("infoDoc".count($infoDoctor));
			for($i=0;$i<count($infoDoctor);$i++)
			{
				$userId = mysql_escape_string($info["med_rep"]);
				$doctorId = mysql_escape_string($infoDoctor[$i]);
				$visitDate = mysql_escape_string($info["date"]);
				$visitTime = mysql_escape_string($infoTime[$i]);
				$sql="INSERT INTO `sales_doctor_assignment`
				(user_id, doctor_id, visit_date, visit_time)
				values
				('$userId', '$doctorId', '$visitDate','$visitTime')";
				traceMessage("sql:$sql");
				if (!$this->Connect('mysql'))
				return false;
				if (!$this->ExecuteNonQuery($sql))
				{
					$this->Dispose();
					return false;
				}
				$this->Dispose();
			}
		}
		function GetEventByUserId($userId,$territoryId)
		{
			$sql = "SELECT sds.`distance_difference`,u.`first_name`, u.`last_name`,u.`color_code`, sds.`visited_latitude`, sds.`visited_longitude`, sds.`visited_time`, sds.`doctor_id`, sds.`user_id`,sds.`status`, sds.`visit_date`,sds.`visit_time`, d.`doctor_name`
			FROM `sales_doctor_assignment` sds, `doctors` d, `users` u
			WHERE d.`doctor_id`=sds.`doctor_id` AND u.`user_id`=sds.`user_id` AND u.`territory_id` IN ($territoryId)";
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetUserMapping($userId)
		{
			$sql = "SELECT * from users u INNER JOIN user_mapping um ON u.`user_id` = um.`child_id` where um.`parent_id`='$userId';";
			// echo $sql;
			traceMessage('Hello'.$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllVisitByType($data)
		{
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "select count(visit_id) as visit_count,visit_type,date(visited_time)as visit_date from visits v where status = '1' and user_id in ($childId) $sqlExtra group by visit_type,date(visited_time) order by date(visited_time) desc $sqlExtra1";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllVisitByIds($data)
		{
			$visit_status = "";
			$visit_status = $data['visit_status'];
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			
			$sql = "SELECT * FROM visits v LEFT JOIN visit_survey vs ON v.`visit_id`=vs.`visit_id`,customer c,users u WHERE v.customer_id = c.customer_id AND v.user_id = u.user_id
			AND v.user_id IN ($childId) $sqlExtra group by vs.visit_id";
			// echo $sql;
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetVisitDetailsByIds($visitId)
		{
			$sql = "SELECT * FROM visits v LEFT JOIN visit_survey vs ON v.`visit_id`=vs.`visit_id`,customer c,users u WHERE v.customer_id = c.customer_id AND v.user_id = u.user_id
			and v.`visit_id`='$visitId' and v.status='1'";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllVisitByCatergory($data)
		{
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT * FROM visits v INNER JOIN customer c ON v.customer_id = c.customer_id
			INNER JOIN users u ON v.user_id = u.user_id INNER JOIN visit_products vp ON v.visit_id =vp.visit_id INNER JOIN product p ON p.product_id=vp.product_id
			where v.status = '1' and v.user_id in ($childId) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllVisitByCustomer($data)
		{
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT *,count(v.visit_id) as total_visits FROM visits v INNER JOIN customer c ON v.customer_id = c.customer_id
			INNER JOIN users u ON v.user_id = u.user_id
			where v.status='1' and v.user_id in ($childId) $sqlExtra group by v.customer_id";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetActualVSTargetVisit($data)
		{
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "select v.visited_time,vt.*,count(*) as actual_visit,u.first_name as fname,
			u.last_name as lname,u.emp_code as ecode
			from visits v, visit_target vt,users u
			where v.user_id in($childId) and vt.user_id=u.user_id
			and v.user_id = vt.user_id
			and v.visit_type !=''
			and month(visited_time) = vt.month
			and year(visited_time) = vt.year
			$sqlExtra
			and v.visit_type = vt.visit_type
			group by v.visit_type,v.user_id order by v.user_id";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAccountSummary($data)
		{
			$childId = $data['child_id'];
			
			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra = " AND date(acc.`create_at`) between '$fromDate' AND '$toDate'";
				$sqlExtra1 = " AND date(vr.`create_at`) between '$fromDate' AND '$toDate'";
				$sqlExtra2= " AND date(i.`create_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "select c.*,acc.* from account_summary acc, customer c
			where acc.customer_id=c.customer_id and c.customer_id in (select customer_id from user_customer where user_id in ($childId)) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$query = "select sum(vr.amount) as collect_amount from visit_recovery vr, visits v, users u where v.status='1' and vr.visit_id=v.visit_id and v.user_id = u.user_id and v.user_id IN ($childId) $sqlExtra1";
			if (!is_array($rs1 = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$query1 = "SELECT SUM(i.`quantity`) as total_quantity from inventory i,product p, customer c where i.`product_id`=p.`product_id` and i.`customer_id`=c.`customer_id` and c.`customer_id` IN (select customer_id from user_customer where user_id IN ($childId)) $sqlExtra2";
			if (!is_array($rs2 = $this->ExecuteQuery($query1)))
			{
				$this->Dispose();
				return null;
			}
			$acctSummary = array();
			$totalOutStanding = 0;
			for($i=0; $i < count($rs);  $i++){
				$ageingArr = array('A','B','C','D','E');
                foreach ($ageingArr as $key => $value) {
                    if ($rs[$i]['ageing_class'] <= $value) {
						$totalOutStanding += $rs[$i][$value];
					}
				}
                $acctSummary['overdue']  = $totalOutStanding;
                $acctSummary['total']  +=  $rs[$i]['total'];
			}
			for($i=0; $i < count($rs1);  $i++)
			$acctSummary['collect_amount'] = $rs1[$i]['collect_amount'];
			for($i=0; $i < count($rs2);  $i++)
			$acctSummary['total_quantity'] += $rs2[$i]['total_quantity'];
			$this->Dispose();
			// $data = new GenericData();
			return $acctSummary;
		}
		function GetUserDataByUserId($userId)
		{
			$sql = "select * from users u where  u.user_id ='$userId' and status=1";
			traceMessage("GetUserDataByUserId  ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetTerritoryByTerritoryId($territoryIds)
		{
			$sql = "SELECT * FROM territory WHERE territory_id IN ($territoryIds)";
			traceMessage("query ".$sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function MonthlyMedRepReportInfo($data)
		{
			traceMessage ("In DL MonthlyMedRepReportInfo");
			$searchDate=date("Y-m-d",strtotime($data['search_date']));
			$childId = $data['child_id'];
			$sql="SELECT * FROM visits v INNER JOIN customer c ON v.customer_id = c.customer_id
			INNER JOIN users u ON v.user_id = u.user_id
			where v.status='1' and v.user_id in ($childId) AND YEAR(visited_time)=YEAR('$searchDate')
			AND MONTH(visited_time)=MONTH('$searchDate')";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function MedicalRepVisitInfo($userId,$searchDate)
		{
			traceMessage ("In DL MedicalRepVisitInfo");
		    $sql="SELECT *,COUNT(v.visit_id) AS visit_count , DATE(v.`visited_time`) AS date_of_visit
			FROM visits v INNER JOIN customer c ON v.customer_id = c.customer_id
			INNER JOIN users u ON v.user_id = u.user_id
			where v.status='1' and v.user_id = '$userId' AND YEAR(visited_time)=YEAR('$searchDate')
			AND MONTH(visited_time)=MONTH('$searchDate')
			GROUP BY DATE(v.`visited_time`);";
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function MonthlyAttendanceReportInfo($data)
		{
			traceMessage ("In DL MonthlyAttendanceReportInfo");
			$searchDate=date("Y-m-d",strtotime($data['search_date']));
			$childId = $data['child_id'];
			$sql="SELECT * FROM attendance a INNER JOIN users u ON a.user_id = u.user_id
			where a.user_id in ($childId) AND DATE(`time`)='$searchDate' order by `time` ASC";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function MonthlyAttendanceUserInfo($userId,$searchDate)
		{
			$searchDate=date("Y-m-d",strtotime($searchDate));
			traceMessage ("In DL MonthlyAttendanceUserInfo");
		    $sql="SELECT MIN(a.`time`) AS first_checkin , MAX(a.`time`) AS last_checkout
			FROM attendance a INNER JOIN users u ON a.`user_id` = u.`user_id`
			where a.`user_id` = '$userId' AND DATE(`time`)='$searchDate'
			GROUP BY a.`type`,DATE(a.`time`);";
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllCities($status)
		{
			$sql = "SELECT * FROM city WHERE status='$status'";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllRegions($status)
		{
			$sql = "SELECT * FROM region WHERE status='$status'";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllDepartment($status)
		{
			$sql = "SELECT * FROM department WHERE status='$status'";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function checkuserName($userId,$oldPassword)
		{
			$query="select count(*) as status from users where user_id='$userId' and password ='$oldPassword'";
			
			
			
			traceMessage("user info  ".$query);
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data=$rs[0]['status'];
			// $data = new GenericData();
			// for($i=0; $i < count($rs);  $i++)
			// $data->AddRow($rs[$i]);
			return $data;
		}
		
		function UpdatePassword($userId,$password)
		{
			$updSql="update users set password ='$password' where user_id ='$userId' ";
			traceMessage("sql:  $updSql");
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		function AddNewPassword($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `user_password_history` $str";
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		
		function GetLastThreePassword($userId,$limit)
		{
			$sql = "select password from user_password_history where user_id='$userId' ORDER BY record_id DESC LIMIT $limit";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		
		function GetAllAreas($status)
		{
			$sql = "select * from area where status='$status'";
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		
		function changeVisitType($visit_id,$status)
		{
			// 2 Follow-up
			// 3 Completed
			$statusCode = "";
			
			if($status == "Yes"){
				$statusCode = 3;
				}else {
				$statusCode = 2;
			}
			$updSql="UPDATE visit_recovery set status_id ='$statusCode' where visit_id ='$visit_id' ;";
			// echo $updSql;
			traceMessage("sql:  $updSql");
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		function InsertNotification($data)
    	{
			$data = $data['db'];
			$str=$this->GetInsertParams($data);
			$sql="insert into `notifications` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		
		function GetAllVisitCustomerStatus()
		{
			$sql = "SELECT * from `visit_customer_status`";
			traceMessage("GetAllVisitCustomerStatus Query: ".$sql);
			if (!$this->Connect('mysqli')) {
				return null;
			}
			if (!is_array($result = $this->ExecuteQuery($sql))) {
				$this->Dispose();
				return null;
			}
			$error = new ErrorData($data->RECORD_NOT_FOUND, __FILE__, __LINE__, 'warning');
			if (CheckCondition(count($result) == 0, $e)) {
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($result); $i++) {
				$data->AddRow($result[$i]);
			}
			return $data;
		}
		function SessionDetailById($loginId,$sid)
		{
			$sql = "SELECT * FROM sessions WHERE username='$loginId'";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function DeleteUserSession($loginId,$sid)
		{
			$sqlExtra = "";
			if(!empty($sid)){
				$sqlExtra = "AND `session_id`!='$sid'";
			}
			$delSql="DELETE FROM sessions WHERE username='$loginId' $sqlExtra";
			traceMessage("Delete Query".$delSql);
			if (!$this->Connect('mysql')) {
				return null;
			}
			if (!$this->ExecuteNonQuery($delSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		
		function GetDriverRidesInfo($userid){
			
			$sql = "SELECT * FROM booking b, users u WHERE b.user_id =u.user_id and driver_id='$userid'";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetUserRidesInfo($userid){
			
			$sql = "SELECT * FROM booking b, users u WHERE b.driver_id =u.user_id and b.user_id='$userid'";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetPaymentInfo($userId){
			
			if(isset($fromDate) && $fromDate!="")
			{
				$filter  = " and created_at between '$fromDate' and '$toDate' order by id desc";
			}
			else
			$filter  = " order by id desc limit 30;";
			$sql = "SELECT * FROM payment WHERE driver_id='$userId' and status in ('1','2') $filter";
			traceMessage($sql);
			
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetCityRateCard($userid){
			
			// $sql = "SELECT * FROM rate_cards WHERE driver_id='$userid'";
			traceMessage(' in dl GetCityRateCardGetCityRateCard');
			$sql = "SELECT 
			r.*, c1.city_name as from_city_name , c2.city_name as to_city_name,rc.driver_rates 
			FROM rate_cards rc,routes r , city c1, city c2 
			WHERE 
			rc.route_id=r.route_id
			and c1.city_id = r.from_city and c2.city_id = r.to_city
			and driver_id='$userid'  and rc.entry_date >= NOW() - INTERVAL 1 DAY";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		
		function AddDriverPayment($info){
			//$data = $data['db'];
			$str=$this->GetInsertParams($info);
			$sql="insert into `payment` $str" ;
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		
		function UpdateShiftStatus($userId,$shiftStatus)
		{
			//$data = $data['db'];
			$sql="update users set shift_status='$shiftStatus',last_shift_update_date=now() where user_id='$userId'";
			
			$sql1="insert into user_shift_status_logs 
			(user_id, status, datetime)
			values
			('$userId', '$shiftStatus', now())";
			traceMessage("sql:$sql");
			traceMessage("sql:$sql1");
			
			if (!$this->Connect('mysql'))
			return false;
			
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			if (!$this->ExecuteNonQuery($sql1))
			{
				$this->Dispose();
				return false;
			}
			
			$this->Dispose();
			return true;
		}
		function DeletePaymentInfo($userId,$paymentId)
		{
			$sql="update payment set status='-1' where id='$paymentId' and driver_id='$userId' ";
			
			
			traceMessage("sql:$sql");
			traceMessage("sql:$sql1");
			
			if (!$this->Connect('mysql'))
			return false;
			
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			
			
			$this->Dispose();
			return true;
		}
		function GetPaymentInfoById($userId,$paymentId)
		{
			$sql="select * from payment where id='$paymentId' and driver_id='$userId' ";
			
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		
		function GetAvailableCities($fromCity)
		{
			$sql="SELECT 
			c2.city_name as to_city_name 
			FROM rate_cards rc,routes r , city c1, city c2 
			WHERE 
			rc.route_id=r.route_id
			and c1.city_id = r.from_city and c2.city_id = r.to_city
			and c1.city_name='$fromCity' group by c2.city_name";
			
			
			$sql = "SELECT 
					c2.city_name as to_city_name 
					FROM city c2 
					where status=1";
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetAvailableDriverForCity($fromCity,$toCity)
		{
			$sql="SELECT 
			r.*,u.full_name,u.user_id as driver_id,u.user_rating,
			c1.city_name as from_city_name , c2.city_name as to_city_name,rc.driver_rates,model_name,name as make_name, uv.vehicle_number,uv.color,uv.year
			FROM rate_cards rc,routes r , city c1, city c2 , users u, user_vehicle uv,vehicle_model vm,vehicle_make vmake
			WHERE 
			rc.route_id=r.route_id
			and u.user_id=rc.driver_id
			and u.user_id=uv.user_id
			and u.shift_status='online'
			and u.status='1'
			and uv.model_id=vm.id
			and uv.make_id=vmake.id
			and c1.city_id = r.from_city and c2.city_id = r.to_city
			and  c2.city_name='$toCity'
			and  c1.city_name='$fromCity' 
			and rc.entry_date >= NOW() - INTERVAL 1 DAY and driver_booking_status='pending';
			";
			
			$sql="SELECT 
			r.*,u.full_name,u.user_id as driver_id,u.user_rating,
			c1.city_name as from_city_name , c2.city_name as to_city_name,rc.driver_rates,model_name,name as make_name, uv.vehicle_number,uv.color,uv.year
			FROM rate_cards rc,routes r , city c1, city c2 , users u, user_vehicle uv,vehicle_model vm,vehicle_make vmake
			WHERE 
			rc.route_id=r.route_id
			and u.user_id=rc.driver_id
			and u.user_id=uv.user_id
			and u.shift_status='online'
			and u.status='1'
			and uv.model_id=vm.id
			and uv.make_id=vmake.id
			and c1.city_id = r.from_city and c2.city_id = r.to_city
			and  c2.city_name='$toCity'
			and  c1.city_name='$fromCity' 
			and u.last_location_updated_at> now() - INTERVAL 5 minute
			order by driver_rates asc
			-- and rc.entry_date >= NOW() - INTERVAL 1 DAY 
			-- and driver_booking_status='pending';
			";
			traceMessage($sql);		
			//	echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetNotificationLists($userId)
		{
			//$sql="select * from notifications where user_id='$userId' and notification_type not in ('booking','endride')";
			$sql="select * from notifications where  notification_type not in ('booking','endride')";
			traceMessage($sql);		
			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function MarkAsRead($notId,$userId)
		{
			
			$updateSql="update notifications set read_status='yes' where user_id='$userId' and record_id=".$notId;
			
			//echo $updSql;
			traceMessage("sql:$updateSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updateSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
		function AddNotification($notArr)
		{
			
			$inst=$this->GetInsertParams($notArr);
			$insSql="Insert into notifications $inst";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		function DriverSetLocation($userId,$lat,$long)
		{
			
			$inst=$this->GetInsertParams($notArr);
			$insSql="update users set `lat`='$lat',`long`='$long',`last_location_updated_at`=now() where user_id='$userId'";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			
			return true;
		}
		
		function GetDriverCityRate($driverId,$destCity)
		{
			
			$sql="select *  from rate_cards rc, routes r, city c
			where 
			rc.route_id=r.route_id
			and r.to_city=c.city_id
			and driver_id='$driverId'
			and c.city_name='$destCity'";
			traceMessage($sql);
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			// Create generic data variable
			$data = new GenericData();
			for ($i=0; $i < count($rs); $i++) {
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function AddAgent($data)
		{
			traceMessage("Insert Agent".print_r_log($data));
			$insStr=$this->GetInsertParams($data);
			$insSql="Insert into `agent` $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function AddOtpCode($data,$randomnum)
		{
			traceMessage("AddOtpCode".print_r_log($data));
			$insStr=$this->GetInsertParams($data,$randomnum);
			$insSql="Insert into `otp_code` $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAgent()
		{
			
			$sql = "SELECT * FROM `agent` where status='active'  ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetBooking()
		{
			
			$sql = "SELECT * FROM `booking` where DATE(booking.`created_at`) BETWEEN '2021-01-01' AND '2021-09-05' ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetCityRequest()
		{
			
			$sql = "SELECT full_name,city,request_id FROM users INNER JOIN city_request ON user_id=driver_id WHERE city_request.`status`=0";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAgentDriver($data)
		{
			$agentId = $data['id'];
			$sql = "SELECT full_name,driver_id,transaction_number,payment,p.created_at FROM users u INNER JOIN payment p ON u.`user_id` = p.`driver_id` WHERE u.`agent`='$agentId' AND p.`status` = '1'";
			
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
			
		}
		function ReportAgent($fromDate,$toDate)
		{
			traceMessage("ReportAgent in Dl".print_r_log($fromDate));
			traceMessage("ReportAgent in Dl".print_r_log($toDate));
			$sqlDate = "";
			if ($fromDate !='1970-01-01' && $toDate !='1970-01-01'){
				$sqlDate	= "AND DATE(booking.`created_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT 
			COUNT(record_id),
			SUM(price) AS price,
			SUM(comission) * 0.50 AS commission,
			driver_id,
			agent.name,
			full_name,
			booking.`created_at` AS created_at,
			agent.id AS agent_id 
		  FROM
			agent 
			INNER JOIN users 
			  ON id = agent 
			INNER JOIN booking 
			  ON users.`user_id` = driver_id 
		  WHERE booking.`status` = 'completed' 
			 $sqlDate
		  GROUP BY agent";
		  traceMessage("ReportAgent in Dl".print_r_log($sql));
		 
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}
		function EditAgent($data)
		{
			
			$sql = "SELECT * FROM `agent` where id=$data ";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			
			
		}	
		function UpdateAgent($usereditid,$editusersInfo)
		{
			$str = $this->GetUpdateParams($editusersInfo);
			traceMessage("Editusers ".print_r_log($str));
			$updSql="update agent set ".$str." where id='$usereditid'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateCityStatus($usereditid,$editusersInfo)
		{
			$str = $this->GetUpdateParams($editusersInfo);
			traceMessage("Editusers ".print_r_log($str));
			$updSql="update city_request set ".$str." where request_id='$usereditid'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteCityStatus($usereditid,$editusersInfo)
		{
			$str = $this->GetUpdateParams($editusersInfo);
			traceMessage("Editusers ".print_r_log($str));
			$updSql="update city_request set ".$str." where request_id='$usereditid'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function CheckMobile($mobile)
		{
			$sql = "SELECT * FROM `agent` WHERE mobile='$mobile'";
			traceMessage('if3'.$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAgentById($agentId)
		{
			$sql = "SELECT * FROM `agent` WHERE id='$agentId'";
			traceMessage('if3'.$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
			return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
			}
			
			function VerifyOtp($driverId,$otp)
			{
			
			
				$sql = "SELECT * FROM `otp_code` where driver_id='$driverId' and otp_code='$otp' and is_verified='no' order by id desc limit 1" ;
				traceMessage($sql);
				if (!$this->Connect('mysql'))
				{
					echo ("Database connection error ".mysql_error());
					return null;
				}
				if (!is_array($rs = $this->ExecuteQuery($sql)))
				{
					$this->Dispose();
					return null;
				}
				$this->Dispose();
				$data = new GenericData();
				for($i=0; $i < count($rs);  $i++)
				$data->AddRow($rs[$i]);
				return $data;
			}
			function MarkOTPAsDone($driverId,$otpCode)
			{
			
			
				$sql = "update `otp_code` set is_verified='yes' where driver_id='$driverId' and otp_code='$otpCode' and is_verified='no'" ;
				traceMessage($sql);
				if (!$this->Connect('mysql'))
				{
					echo ("Database connection error ".mysql_error());
					return null;
				}
				if (!$this->ExecuteNonQuery($sql))
				{
					$this->Dispose();
					return null;
				}
				$this->Dispose();
			
				return true;
			}
			function GetUserInfoByPhone($phoneNo)
			{

				$sql = "SELECT  u.* from users u  where  u.`phone_number` ='$phoneNo' and status='1' ";
				// echo $Sql;
				if (!$this->Connect('mysql'))
				{
					return null;
				}
				if (!is_array($rs = $this->ExecuteQuery($sql)))
				{
					$this->Dispose();
					return null;
				}
				$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
				if(CheckCondition(count($rs)==0,$e))
				{
					$this->Dispose();
					return null;
				}
				$this->Dispose();
				$data = new GenericData();
				for($i=0; $i < count($rs);  $i++)
				$data->AddRow($rs[$i]);
				return $data;
			}
			function GetAgentInfoByUser($userId)
			{

				$sql = "select * from agent where id in 
						(
						select agent from users where user_id=$userId
						);";
				// echo $Sql;
				if (!$this->Connect('mysql'))
				{
					return null;
				}
				if (!is_array($rs = $this->ExecuteQuery($sql)))
				{
					$this->Dispose();
					return null;
				}
				$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
				if(CheckCondition(count($rs)==0,$e))
				{
					$this->Dispose();
					return null;
				}
				$this->Dispose();
				$data = new GenericData();
				for($i=0; $i < count($rs);  $i++)
				$data->AddRow($rs[$i]);
				return $data;
			}
			
	}
