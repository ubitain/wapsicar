 <?php

	class DL_Vehicle extends DataAccessBase
	{
		function getVehicleMake()
		{
			
			$sql = "SELECT * from vehicle_make";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}
		function getVehicleModel($make_id)
		{
			$sql = "SELECT * from vehicle_model  ";

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}

		function AddVehicleInformation($info)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="insert into user_vehicle $inst";
			//echo($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		
		function GetVehicleInfo($userId)
		{
			$sql = "select * from user_vehicle where user_id='$userId' ";

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}
		function GetCarTypes()
		{
			$sql = "select distinct car_type from vehicle_model ";

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}

		
	}