 <?php

	class DL_Payments extends DataAccessBase
	{
		function GetDocumentListing()
		{
			
			$sql = "SELECT * from documents";
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}
		function GetPaymentByTransaction($transaction_number)
		{
			$sql = "Select * from payment where transaction_number = '$transaction_number'";
			 traceMessage("GetPaymentByTransaction".$sql);

			//echo $sql;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			return $rs;
			
		}

		function UpdateTransactionStatus($transaction_number,$status,$difference)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="update payment set status='$status',difference='$difference' where transaction_number='$transaction_number'";
			traceMessage('UpdateTransactionStatus'.$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		function UpdateTransaction($transactionNumbers , $userId)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="update payment set driver_id='$userId' where transaction_number='$transactionNumbers'";
			traceMessage('UpdateTransactionStatus'.$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		
		function AddUserDocuments($info)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="insert into user_documents $inst";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}

		function AddUserDocumentInfo($info)
		{
			$inst=$this->GetInsertParams($info);
			$insSql="insert into user_document_info $inst";
			//echo($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
		}
		function CheckTransaction($transactionId)
		{
				$sql = "SELECT * FROM `payment` WHERE transaction_number='$transactionId' AND status IN (1,3,0)";
				traceMessage('if3'.$sql);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}

		
	}