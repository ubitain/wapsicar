 <?php

	class DL_UpdateDelivery extends DataAccessBase
	{
		function UpdateDelivery($updatearray)
		{
			$str = $this->GetUpdateParams($updatearray);
			$updSql="update crown_delivery_point set ".$str." ";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
	}

?>