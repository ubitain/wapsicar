 <?php

	class DL_AddVoucher extends DataAccessBase
	{
		
		function AddGrandTotal($addarray)
    	{
			$str=$this->GetInsertParams($addarray);
			$sql="insert into `claim_amount` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function AddVoucher($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `claim_finalsettlement` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateGrandTotal($updateArrayGrandTotal,$claimId)
		{
			$str = $this->GetUpdateParams($updateArrayGrandTotal);
			traceMessage("str ".print_r_log($str));
			$updSql="update claim_amount set ".$str." where claim_id='".$claimId."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function AddCal($addarray,$claimId,$claimDetailId)
		{
			$str = $this->GetUpdateParams($addarray);
			traceMessage("str ".print_r_log($str));
			$updSql="update claim_details set ".$str." where claim_id='".$claimId."' and claim_detail_id='".$claimDetailId."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function DeleteVoucher($addarray,$voucherNumber)
		{
			$str = $this->GetUpdateParams($addarray);
			traceMessage("str ".print_r_log($str));
			$updSql="update claim_finalsettlement set ".$str." where claim_finalsettlement_id='".$voucherNumber."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		
	}

?>