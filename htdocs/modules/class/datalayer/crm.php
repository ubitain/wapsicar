 <?php
/**
 * CRM Data Layer Class
 */
class DL_Crm extends DataAccessBase
{
    function AddLead($params)
    {
        traceMessage('Add Lead Data Layer'.print_r_log($params));
        // Preparing insert params from Array params
        $insertParams = $this->GetInsertParams($params);
        // Building the insert query
        $query = "INSERT into crm_lead $insertParams";
        traceMessage("Insert Query of Add Lead ".$query);
        // Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing NonQuery in Sql
        if (!$this->ExecuteNonQuery($query)) {
            $this->Dispose();
            return false;
        }
        $insertId = $this->GetInsertId();
        $this->Dispose();
        return $insertId;
    }

    function GetCustomerIdByPhoneNumber($params)
    {
        traceMessage("GetCustomerIdByPhoneNumber ".$params);
        $query = "SELECT * from customer WHERE status='1' AND contact_number=".$params;
        traceMessage("GetCustomerIdByPhoneNumber ".$query);
        // Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }

    function NearCustomerUserId($params)
    {
        $startDateTime = $params['start_date_time'];
        $endDateTime = $params['end_date_time'];

        $query = "SELECT user_id, latitude, longitude from user_tracking WHERE location_time BETWEEN '$startDateTime' AND '$endDateTime' GROUP BY user_id";
        traceMessage('hell'.$query);
        // Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }

    function AssignUsersToLeadId($params)
    {
        traceMessage('AssignUsersToLeadId Data Layer'.print_r_log($params));
        // Preparing insert params from Array params
        $insertParams = $this->GetInsertParams($params);
        // Building the insert query
        $query = "INSERT into lead_users $insertParams";
        traceMessage("Insert Query of AssignUsersToLeadId ".$query);
        // Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing NonQuery in Sql
        if (!$this->ExecuteNonQuery($query)) {
            $this->Dispose();
            return false;
        }
        $insertId = $this->GetInsertId();
        $this->Dispose();
        return $insertId;
    }

    function GetUserIdByCustomerId($params)
    {
        $customerId = $params['customer_id'];
        $query = "SELECT user_id FROM user_customer WHERE customer_id='$customerId' AND status='1'";
        traceMessage("GetUserIdByCustomerId".$query);
        //Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return false;
        }
        $this->Dispose();
        $data = new GenericData();
        for ($i=0; $i < count($result); $i++)
        $data->AddRow($result[$i]);
        return $data;
    }

    function GetLeadIdByNic($params)
    {
        $nic = $params['nic'];
        $query = "SELECT lead_id FROM crm_lead WHERE nic='$nic'";
        traceMessage("Get lead id by nic ".$query);
        //Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return false;
        }
        $this->Dispose();
        $data = new GenericData();
        for ($i=0; $i < count($result); $i++)
        $data->AddRow($result[$i]);
        return $data;
    }

    function GetLeads($params)
    {
        $extra = "";
        $status = $params['status'];
        if (!empty($status)) {
            $extra = "AND status = '$status'";
        }
        $query = "SELECT * from crm_lead WHERE 1=1 $extra";
        traceMessage("Get Leads query".$status); 
        //Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error");
            return null;
        }

        //Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return false;
        }

        $this->Dispose();
        $data = new GenericData();
        for ($i=0; $i < count($result); $i++) {
            $data->AddRow($result[$i]);
        }
        return $data;
    }
    function GetLeadsRelatedUser($params)
    {
        $leadId = $params['lead_id'];
        $query = "SELECT u.`first_name` AS user_name, u.`emp_code`,n.`creation_time` AS sent_date, lu.`status` FROM lead_users lu INNER JOIN users u ON lu.`user_id`=u.`user_id` INNER JOIN notifications n ON lu.`lead_id`=n.`reference_id` WHERE lu.`lead_id`='$leadId'";
        traceMessage("Get Leads related user query".$query);

        //Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error");
            return null;
        }

        //Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return false;
        }

        $this->Dispose();
        $data = new GenericData();
        for ($i=0; $i < count($result); $i++) {
            $data->AddRow($result[$i]);
        }
        return $data;
    }

    function GetLeadDetail($params)
    {
        $leadId = $params['lead_id'];
        $query = "SELECT * FROM crm_lead WHERE lead_id='$leadId'";
        traceMessage("Get lead id by lead_id ".$query);
        //Checking the Sql Connection
        if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return false;
        }
        $this->Dispose();
        $data = new GenericData();
        for ($i=0; $i < count($result); $i++){
            $data->AddRow(array_filter($result[$i]));
        }
        return $data;
    }
}

?>
