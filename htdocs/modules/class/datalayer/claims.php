 <?php

	class DL_AddPurchaseFromAccountOff extends DataAccessBase
	{
		function ProccedToLeopard($claimNo)
		{
			// $str = $this->GetUpdateParams($claimNo);
			// traceMessage("str ".print_r_log($str));
			for ($i=0; $i < count($claimNo); $i++) { 
				
			$updSql="UPDATE claims SET forwarded_status='1' where claim_no='".$claimNo[$i]."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}
		function AddPurchaseFromAccountOff($addstuff,$claimID)
		{
			$str = $this->GetUpdateParams($addstuff);
			traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claims SET ".$str." where claim_id='".$claimID."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}
		function AddPurchaseFromhis($arr)
    	{
    		traceMessage("LETS SEE IF ITS COMING!!!");
			$str=$this->GetInsertParams($arr);
			$sql="insert into `claim_transactions` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function AddAccountOffhis($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `claim_transactions` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function AddPurchaseFromhislog($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `claim_transactions_log` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function AddAccountOffhislog($arr)
    	{
			$str=$this->GetInsertParams($arr);
			$sql="insert into `claim_transactions_log` $str";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function GetMaxTransLog($claimID)
	    {
	        $query = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTrans($claimID)
	    {
	        $query = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransUser($claimTransctionsId)
	    {
	        $query = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransAccount($claimID)
	    {
	        $query = "SELECT MAX(transaction_id) as claimTransctionsAccountId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransAccountLog($claimID)
	    {
	        $query = "SELECT MAX(claim_transactions_log_id) as claimTransctionsAccountIdLog  FROM claim_transactions_log WHERE assigned_to='account' AND claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransAccountStatus($claimID)
	    {
	        $query = "SELECT status as claimTransctionsAccountStatus  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransAccountClaim($claimID)
	    {
	        $query = "SELECT account_of FROM claims WHERE  claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
	    function GetMaxTransPurchaseClaim($claimID)
	    {
	        $query = "SELECT purchased_from FROM claims WHERE  claim_id=".$claimID."";
	      
	            if (!$this->Connect('mysql')) {
	            traceMessage("Database connection error ".mssql_get_last_message());
	            return null;
	        }
	        // Executing Query in Sql
	        if (!is_array($result = $this->ExecuteQuery($query))) {
	            $this->Dispose();
	            return null;
	        }
	        $this->Dispose();
	        $data = new GenericData();
	        for($i=0; $i < count($result);  $i++)
	        $data->AddRow($result[$i]);
	        return $data;
	    }
		function UpdatePurchaseFrom($data1,$claimTransctionsId)
		{
			$str = $this->GetUpdateParams($data1);
			traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claim_transactions SET ".$str." where transaction_id='".$claimTransctionsId."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}	
		function UpdatePurchaseFromLog($data1,$claimTransctionsLogId)
		{
			$str = $this->GetUpdateParams($data1);
			traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claim_transactions_log SET ".$str." where claim_transactions_log_id='".$claimTransctionsLogId."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}
		function DeletePurchaseFrom($claimID)
    	{
			$sql="DELETE FROM claim_transactions where claim_id='".$claimID."' and assigned_to='purchase'";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdateAccountOffFromLog($dataChangeStatus,$claimTransctionsAccountIdLog)
		{
			$str = $this->GetUpdateParams($dataChangeStatus);
			traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claim_transactions_log SET ".$str." where claim_transactions_log_id='".$claimTransctionsAccountIdLog."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}
		function DeleteAccountOff($claimID)
    	{
			$sql="DELETE FROM claim_transactions where claim_id='".$claimID."' and assigned_to='account'";
			traceMessage("sql:$sql");
			if (!$this->Connect('mysql'))
			return false;
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId;
		}
		function UpdatePurchaseFromClaim($updatePurchaseFromInClaim,$claimID)
		{
			$str = $this->GetUpdateParams($updatePurchaseFromInClaim);
			traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claims SET ".$str." where claim_id='".$claimID."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}
		function UpdateStatusFromClaim($claimID)
		{
			// $str = $this->GetUpdateParams($updatePurchaseFromInClaim);
			// traceMessage("str ".print_r_log($str));
			$updSql="UPDATE claims SET claim_status='approved' where claim_id='".$claimID."'";
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;


			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				traceMessage('Query not executed');
				$this->Dispose();
				return false;
			}
			traceMessage("the code is coming here!!".print_r_log($this->conn->ErrorMsg()));
			$this->Dispose();
			return true;
		}		
	}
?>