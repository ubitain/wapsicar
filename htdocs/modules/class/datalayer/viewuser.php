<?php
class DL_ViewUser extends DataAccessBase
{
	function ViewUser()
    {
        $query = "SELECT * from users where status='active' and user_type!=16";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewUserInformation($userId)
    {
        $query = "SELECT * from users where user_id=$userId";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
    function ViewAgntInformation()
    {
        $query = "SELECT distinct name,id from agent ";
      
            if (!$this->Connect('mysql')) {
            traceMessage("Database connection error ".mssql_get_last_message());
            return null;
        }
        // Executing Query in Sql
        if (!is_array($result = $this->ExecuteQuery($query))) {
            $this->Dispose();
            return null;
        }
        $this->Dispose();
        $data = new GenericData();
        for($i=0; $i < count($result);  $i++)
        $data->AddRow($result[$i]);
        return $data;
    }
}

?>
