 <?php
	traceMessage("dl");
	class DL_Customer extends DataAccessBase
	{
		function AddCustomer($arr)
		{
			traceMessage("Add Customer".print_r_log($arr));
			$insStr=$this->GetInsertParams($arr);
			$insSql="Insert into customer $insStr";
			traceMessage("Insert Customer Query : ".$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$insertId = $this->GetInsertId();
			$insSql = "Insert into customer_details (`customer_id`) VALUES ('$insertId')";
			traceMessage("Insert Customer Detail Query : ".$insSql);
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return $insertId;
		}
		function UpdateCustomer($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			traceMessage("UpdateCustomer ".print_r_log($str));
			$updSql="update customer set ".$str." where customer_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}

		function GetAllCustomer($status)
		{
			$query="select c.*,cd.branch_name,cd.account_number,cd.account_balance from customer c left join customer_details cd on c.customer_id=cd.customer_id where c.status='$status' order by c.customer_id desc limit 5000";
			traceMessage("GetAllCustomerQuery ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}


		function GetAllComplaints($data,$status)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(complain.`create_at`) between '$fromDate' AND '$toDate'";
			}

			// echo "<pre>";
			// print_r($data);

			$statusQuery = "";
			if($data['complain_status'] == "open"){

				$statusQuery = " AND complain.complain_status = 'open' ";

			}else if($data['complain_status'] == "close"){

				$statusQuery = " AND complain.complain_status = 'close' ";

			}

			$query="SELECT users.`user_id`,users.`first_name`,users.`last_name`,customer.`customer_id`,customer.`customer_name`,customer.`ledger_id`,customer.`region_id`,
				complain.`reason_id`,complain.`link`,complain.`remarks`,complain.`complain_status`,
				reason.`description`,reason.`department_id`,department.`department_id`,department.`name`,
				DATE(complain.`create_at`) AS complaint_date
				FROM complain
				INNER JOIN users ON users.`user_id` = complain.`user_id`
				INNER JOIN customer ON customer.`customer_id` = complain.`customer_id`
				INNER JOIN reason ON reason.`reason_id` = complain.`reason_id`
				INNER JOIN department ON department.`department_id` = reason.`department_id`
				WHERE complain.`user_id` IN ($childId)
				$sqlExtra $statusQuery;";

			 // echo $query;

			traceMessage("GetAllCustomerQuery ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}


		function GetACustomer($id,$status)
		{
			$query="select c.*,cd.branch_name,cd.account_number,cd.account_balance from customer c left join customer_details cd on c.customer_id=cd.customer_id where  c.`customer_id`='$id' and c.`status`='$status' order by c.customer_id asc limit 100";
			traceMessage("GetACustomerQuery ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllUnAssignCustomer()
		{
			$query="SELECT c.*,uc.* FROM customer c LEFT JOIN user_customer uc  ON uc.`customer_id` = c.`customer_id` WHERE uc.`customer_id` IS NULL order by customer_name asc";
			traceMessage("GetUnAssignCustomer ".$query);

			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllAssignCustomer()
		{
			$query="SELECT c.*,uc.* FROM customer c LEFT JOIN user_customer uc  ON uc.`customer_id` = c.`customer_id` WHERE uc.`customer_id` IS NOT NULL";
			traceMessage("GetAssignCustomer ".$query);

			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAssignCustomerByUserId($userId)
		{
			$query="SELECT c.*,uc.* FROM customer c LEFT JOIN user_customer uc ON uc.customer_id = c.customer_id WHERE uc.user_id ='$userId'";
			traceMessage("GetAssignCustomer ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAllUserAssignedByCustomerId($customerId)
		{
			$query="SELECT u.* FROM users u LEFT JOIN user_customer uc ON uc.`user_id` = u.`user_id` WHERE uc.`customer_id` ='$customerId'";
			traceMessage("GetAssignCustomer ".$query);

			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function AssignCustomer($assignCustomerInfo)
		{
			traceMessage("***********In DL AssignCustomer ".print_r_log($assignCustomerInfo));
			$insStr=$this->GetInsertParams($assignCustomerInfo);
			traceMessage("Insert String ".$insStr);
			$insSql="Insert into `user_customer` $insStr";
			traceMessage($insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UnassignedCustomer($unassignCustomerInfo)
		{
			traceMessage ("In DL UnassignedCustomer".print_r_log($unassignCustomerInfo));
			$userId = $unassignCustomerInfo['user_id'];
			$customerId = $unassignCustomerInfo['customer_id'];
			$sql = "DELETE  FROM user_customer WHERE user_id='$userId' AND customer_id='$customerId'";
			traceMessage("UnassignedCustomer Query Is \n $sql");
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function AddNewArea($areaInfo)
		{
			traceMessage("In DL areaInfo ".print_r_log($areaInfo));
			$insStr=$this->GetInsertParams($areaInfo);
			traceMessage("Insert String ".$insStr);
			$insSql="Insert into areas $insStr";
			traceMessage("-------".$insSql."**********");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
				$retId = $this->GetInsertId();
				traceMessage("retidddd::" . $retId);
			// $this->Dispose();
			return $retId;
		}
		function AjaxCustomerTable($aColumns,$sIndexColumn,$sTable,$request)
		{
			traceMessage("DL AjaxCustomerTable".print_r_log($request));
			/*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $request['iDisplayStart'] ) && $request['iDisplayLength'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $request['iDisplayStart'] ).", ".
					intval( $request['iDisplayLength'] );
			}
			/*
			 * Ordering
			 */
			$sOrder = "";
			if ( isset( $request['iSortCol_0'] ) )
			{
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $request['iSortingCols'] ) ; $i++ )
				{
					if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == "true" )
					{
						$sOrder .= "`".$aColumns[ intval( $request['iSortCol_'.$i] ) ]."` ".
							($request['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
					}
				}

				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
			/*
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			$sWhere = "";
			if ( isset($request['sSearch']) && $request['sSearch'] != "" )
			{
				$sWhere = "WHERE status='1' and (";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $request['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ')';
			}
			/* Individual column filtering */
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( isset($request['bSearchable_'.$i]) && $request['bSearchable_'.$i] == "true" && $request['sSearch_'.$i] != '' )
				{
					if ( $sWhere == "" )
					{
						$sWhere = "WHERE ";
					}
					else
					{
						$sWhere .= " AND ";
					}
					$sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($request['sSearch_'.$i])."%' ";
				}
			}
			if ($sWhere == '') {
				$sWhere = "WHERE status='1'";
			}
			traceMessage("Query Request".print_r_log($request['sSearch_0']));
			/*
			 * SQL queries
			 * Get data to display
			 */
			$sQuery = "
				SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
				FROM   $sTable
				$sWhere
				$sOrder
				$sLimit
				";
			traceMessage("Ajax Query Here".$sQuery);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rResult = $this->ExecuteQuery($sQuery)))
			{
				$this->Dispose();
				return null;
			}
			/* Data set length after filtering */
			$sQuery = "
				SELECT FOUND_ROWS()
			";
			if (!is_array($rResultFilterTotal = $this->ExecuteQuery($sQuery)))
			{
				$this->Dispose();
				return null;
			}
			$iFilteredTotal = $rResultFilterTotal[0][0];

			/* Total data set length */
			$sQuery = "
				SELECT COUNT(`".$sIndexColumn."`)
				FROM   $sTable
			";
			if (!is_array($rResultTotal = $this->ExecuteQuery($sQuery)))
			{
				$this->Dispose();
				return null;
			}
			$iTotal = $rResultTotal[0][0];

			/*
			 * Output
			 */
			$output = array(
				"sEcho" => intval($request['sEcho']),
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
			for ($i=0; $i < count($rResult); $i++) {
				$row = array();
				for ( $j=0 ; $j<count($aColumns) ; $j++ )
				{
					if ($aColumns[$j] == "customer_id") {
						$row[] =
						'<a class="btn btn-sm btn-info" onClick="					LoadAjaxScreen(\'editcustomer\'+\'&customer_id=\'+'.$rResult[$i][$aColumns[$j]].')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
						&nbsp;&nbsp;
						<a class="btn btn-sm btn-danger" onClick="DeleteCustomer(\''.$rResult[$i][$aColumns[$j]].'\')"><i class="fa fa fa-trash-o" aria-hidden="true"></i></a>';
					}
					else
					if ( $aColumns[$j] == "customer_name" || $aColumns[$j] == "company_name" || $aColumns[$j] == "email_address" || $aColumns[$j] == "customer_type" || $aColumns[$j] == "address")
					{
						/* Special output formatting for 'version' column */
						$row[] = ($rResult[$i][$aColumns[$j]]=="") ? '-' : $rResult[$i][$aColumns[$j]];
					}
					else
					if ( $aColumns[$i] != ' ' )
					{
						/* General output */
						$row[] = $rResult[$i][$aColumns[$j]];
					}
				}
				$output['aaData'][] = $row;
			}
			// print_r_pre($rResult);
			return json_encode( $output );
		}
		function InboxCustomerList($regionId)
		{
			$sqlExtra = "";
			if(!empty($regionId))
				$sqlExtra = "AND c.region_id='$regionId'";

			$query = "SELECT * from inbox i, customer c where c.customer_id=i.customer_id $sqlExtra group by i.customer_id";
			traceMessage("InboxCustomerList Query : ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function InboxCustomerThread($customerId)
		{
			$query = "select * from inbox i, customer c, users u where u.user_id=i.user_id and c.customer_id=i.customer_id and i.customer_id='$customerId'";
			traceMessage("InboxCustomerList Query : ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function InboxCustomerThreadImages($inboxId)
		{
			$query = "select * from inbox_image where inbox_id='$inboxId'";
			traceMessage("InboxCustomerList Query : ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function AddThreadMessage($arr)
 		{
 			traceMessage("Add Thread Message".print_r_log($arr));
 			$insStr=$this->GetInsertParams($arr);
 			$insSql="Insert into inbox $insStr";
 			traceMessage("Insert String ".$insSql);
 			if (!$this->Connect('mysql'))
 			{
 				traceMessage("Database connection error ".mssql_get_last_message());
 				return null;
 			}
 			if (!$this->ExecuteNonQuery($insSql))
 			{
 				$this->Dispose();
 				return false;
 			}
			$inboxId = $this->GetInsertId();
 			$this->Dispose();
 			return $inboxId;
 		}
		function AddThreadFile($arr)
 		{
 			traceMessage("Add Thread Message".print_r_log($arr));
 			$insStr=$this->GetInsertParams($arr);
 			$insSql="Insert into inbox_image $insStr";
 			traceMessage("Insert String ".$insSql);
 			if (!$this->Connect('mysql'))
 			{
 				traceMessage("Database connection error ".mssql_get_last_message());
 				return null;
 			}
 			if (!$this->ExecuteNonQuery($insSql))
 			{
 				$this->Dispose();
 				return false;
 			}
 			$this->Dispose();
 			return true;
 		}
		function GetAllVisitByIds($data)
		{
			$customerId = $data['customer_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT * FROM visits v LEFT JOIN visit_survey vs ON v.`visit_id`=vs.`visit_id`,customer c,users u WHERE v.customer_id = c.customer_id AND v.user_id = u.user_id
			AND v.status='1' and v.customer_id = '$customerId' $sqlExtra group by vs.visit_id";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetTotalOutStanding($data)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(acc.`create_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "select c.*,acc.* from account_summary acc, customer c
					where acc.customer_id=c.customer_id and c.customer_id in (select customer_id from user_customer where user_id in ($childId)) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetTotalOverDue($data)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(acc.`create_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "select c.*,acc.* from account_summary acc, customer c
					where acc.customer_id=c.customer_id and c.customer_id in (select customer_id from user_customer where user_id in ($childId)) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++){
				$ageingArr = array('A','B','C','D','E');
				$totalOverDue = 0;
				foreach ($ageingArr as $key => $value) {
					if ($rs[$i]['ageing_class'] <= $value) {
						// echo "Customer Ageing : ".$rs[$i]['ageing_class']."Ageing : ".$value."</br>";
						$totalOverDue += $rs[$i][$value];
					}
				}
				$rs[$i]['overdue'] = (string) $totalOverDue;
				$data->AddRow($rs[$i]);
			}
			return $data;
		}
		function GetTotalCollectedAmount($data)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(vr.`create_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT c.customer_name,c.ledger_id,SUM(vr.amount) as collect_amount from visit_recovery vr, visits v, users u,customer c where vr.visit_id=v.visit_id and v.user_id=u.user_id and v.customer_id=c.customer_id and v.user_id IN ($childId) $sqlExtra group by v.customer_id";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
				$data->AddRow($rs[$i]);
			return $data;
		}
		function GetTotalInventory($data)
		{
			$childId = $data['child_id'];

			$sqlExtra = "";
			if ($data['from_date'] && $data['to_date'] ){
				$fromDate = $data['from_date'];
				$toDate = $data['to_date'];
				$sqlExtra= " AND date(i.`create_at`) between '$fromDate' AND '$toDate'";
			}
			$sql = "SELECT i.`quantity`,p.product_name,c.ledger_id,c.customer_name,u.first_name from inventory i,product p, customer c, user_customer uc,users u where i.`product_id`=p.`product_id` and i.`customer_id`=c.`customer_id` and c.`customer_id`=uc.customer_id and u.user_id=uc.user_id and uc.user_id IN ($childId) $sqlExtra";
			traceMessage($sql);
			// echo $sql;
			if (!$this->Connect('mysql'))
			{
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$e = new ErrorData($data->RECORD_NOT_FOUND,__FILE__,__LINE__,'warning');
			if(CheckCondition(count($rs)==0,$e))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
				$data->AddRow($rs[$i]);
			return $data;
		}
		function AddCustomerContact($arr)
		{
			traceMessage("Add Customer Contact".print_r_log($arr));
			$insStr=$this->GetInsertParams($arr);
			$insSql="Insert into customer_contact_person $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateCustomerContact($arr,$id)
		{
			$str = $this->GetUpdateParams($arr);
			traceMessage("UpdateCustomerContact ".print_r_log($str));
			$updSql="update customer_contact_person set ".$str." where contact_person_id=".$id;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAllCustomerContact($id,$status)
		{
			$query="SELECT * FROM customer_contact_person c WHERE c.`customer_id`='$id' and c.`status`='$status'";
			traceMessage("GetACustomerContact Query ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetACustomerContact($id,$status)
		{
			$query="SELECT * FROM customer_contact_person c WHERE c.`contact_person_id`='$id' and c.`status`='$status'";
			traceMessage("GetACustomerContact Query ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		public function AddDepartment($departmentInfo)
		{
			$insStr=$this->GetInsertParams($departmentInfo);
			$insSql="Insert into department $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateDepartment($departmentInfo,$departmentId)
		{
			$str = $this->GetUpdateParams($departmentInfo);
			traceMessage("str ".print_r_log($str));
			$updSql="update department set ".$str." where department_id=".$departmentId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAllDepartment($status)
		{
			$query = "SELECT * FROM `department` where STATUS = '$status'";
			traceMessage("all department".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetADepartment($id,$status)
		{
			$query = "SELECT * FROM `department` where department_id='$id' and status = '$status'";
			traceMessage("GetADepartment ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		/* Reason CRUD*/
		public function AddReason($reasonInfo)
		{
			$insStr=$this->GetInsertParams($reasonInfo);
			$insSql="Insert into reason $insStr";
			traceMessage("Insert String ".$insSql);
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}
			if (!$this->ExecuteNonQuery($insSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function UpdateReason($reasonInfo,$reasonId)
		{
			$str = $this->GetUpdateParams($reasonInfo);
			traceMessage("str ".print_r_log($str));
			$updSql="update reason set ".$str." where reason_id=".$reasonId;
			traceMessage("sql:$updSql");
			if (!$this->Connect('mysql'))
			{
				traceMessage("Database connection error ".mssql_get_last_message());
				return null;
			}

			if (!$this->ExecuteNonQuery($updSql))
			{
				$this->Dispose();
				return false;
			}
			$this->Dispose();
			return true;
		}
		function GetAllReason($status)
		{
			$query = "SELECT * FROM `reason` r INNER JOIN `department` d ON r.`department_id`=d.`department_id` where r.`STATUS` = '$status'";
			traceMessage("all reason".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
		function GetAReason($id,$status)
		{
			$query = "SELECT * FROM `reason` where reason_id='$id' and status = '$status'";
			traceMessage("GetAReason ".$query);
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($query)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;
		}
	}
?>
