<?php
	class DL_Booking extends DataAccessBase
	{
		function GetBookingInfo($bookingId)
		{
			//$sql = "select * from booking where record_id ='$bookingId' and `status`='pending'";
			$sql = "select * from booking where record_id ='$bookingId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		function AddTempBooking($addArr)
		{
			$inst=$this->GetInsertParams($addArr);
			$sql="Insert into booking $inst";
			traceMessage($sql);
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			$retId = $this->GetInsertId();
			$this->Dispose();
			return $retId ;

		}
		function RejectBooking($driverId,$status,$bookingId,$reason,$comments)
		{
			$comments = mysql_escape_string($comments);	
			$sql="update booking set status='rejectbydriver',reason='$reason',comments='$comments',updated_at=now() where record_id='$bookingId' and driver_id='$driverId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			
			$this->Dispose();
			return true ;

		}
	
		function AcceptBooking($driverId,$status,$bookingId,$driverLat,$driverLong)
		{
			$comments = mysql_escape_string($comments);	
			$sql="update booking set status='inprocess',updated_at=now(),driver_last_lat='$driverLat',driver_last_long='$driverLong'
			where record_id='$bookingId' and driver_id='$driverId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			
			$this->Dispose();
			return true ;

		}
		function CancelBooking($driverId,$status,$bookingId,$reason,$comments)
		{
			$comments = mysql_escape_string($comments);	
			$sql="update booking set status='$status',reason='$reason',comments='$comments',updated_at=now() where record_id='$bookingId' and driver_id='$driverId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			
			$this->Dispose();
			return true ;

		}
		
		function StartTrip($driverId,$bookingId)
		{
			$sql="update booking set trip_start='yes',trip_start_time=now(),updated_at=now() where record_id='$bookingId' and driver_id='$driverId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			
			$this->Dispose();
			return true ;

		}
		function EndTrip($driverId,$bookingId)
		{
			$sql="update booking set status='completed',trip_end='yes',trip_end_time=now(),updated_at=now() where record_id='$bookingId' and driver_id='$driverId'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!$this->ExecuteNonQuery($sql))
			{
				$this->Dispose();
				return null;
			}
			
			
			$this->Dispose();
			return true ;

		}
		function GetDriverDashboard($userId)
		{
			$sql = "select sum(price) as total_earnings from booking where driver_id='$userId' and trip_end='yes'";
			$sql1 = "select sum(comission) as total_comission from booking where driver_id='$userId' and trip_end='yes'";
			$sql2 = "select count(*) as total_rides from booking where driver_id='$userId' and trip_end='yes'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			if (!is_array($rs1 = $this->ExecuteQuery($sql1)))
			{
				$this->Dispose();
				return null;
			}
			if (!is_array($rs2 = $this->ExecuteQuery($sql2)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			$ret['total_earnings'] =  $rs[0]['total_earnings'];
			$ret['total_comission'] =  $rs1[0]['total_comission'];
			$ret['ats_dues'] =  $rs1[0]['total_comission'];
			$ret['total_rides'] =  $rs2[0]['total_rides'];
			
			return $ret;

		}
		function  GetCustomerDashboard($userId)
		{
			$sql = "select sum(price) as total_earnings from booking where user_id='$userId' and trip_end='yes'";
			// $sql1 = "select sum(comission) as total_comission from booking where user_id='$userId' and trip_end='yes'";
			$sql2 = "select count(*) as total_rides from booking where user_id='$userId' and trip_end='yes'";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			// if (!is_array($rs1 = $this->ExecuteQuery($sql1)))
			// {
			// 	$this->Dispose();
			// 	return null;
			// }
			if (!is_array($rs2 = $this->ExecuteQuery($sql2)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			
			$ret['total_earnings'] =  $rs[0]['total_earnings'];
			// $ret['total_comission'] =  $rs1[0]['total_comission'];
			// $ret['ats_dues'] =  $rs1[0]['total_comission'];
			$ret['total_rides'] =  $rs2[0]['total_rides'];
			
			return $ret;

		}
		function GetCustomerActiveBooking($userId)
		{
			//$sql = "select * from booking where record_id ='$bookingId' and `status`='pending'";
			$sql = "select * from booking where user_id=$userId
					and status in ('inprocess') order by record_id desc limit 1";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		function GetRiderActiveBooking($userId)
		{
			//$sql = "select * from booking where record_id ='$bookingId' and `status`='pending'";
			$sql = "select * from booking where driver_id=$userId
					and status in ('inprocess') order by record_id desc limit 1";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
		function GetRiderPendingBooking($userId)
		{
			//$sql = "select * from booking where record_id ='$bookingId' and `status`='pending'";
			$sql = "select * from booking where driver_id=$userId
					and status in ('pending') order by record_id desc limit 1";
			
			//echo $sql ;
			if (!$this->Connect('mysql'))
			{
				echo ("Database connection error ".mysql_error());
				return null;
			}
			if (!is_array($rs = $this->ExecuteQuery($sql)))
			{
				$this->Dispose();
				return null;
			}
			$this->Dispose();
			$data = new GenericData();
			for($i=0; $i < count($rs);  $i++)
			$data->AddRow($rs[$i]);
			return $data;

		}
}
