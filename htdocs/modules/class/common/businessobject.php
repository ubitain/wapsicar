<?php
/**
 * Generic Data
 *
 */
class GenericData
{
	var $count = 0;
	var $objectCount = 0;
	var $totalCount = 0; // Count of all entries without limit
	var $rows = array();
	var $objects = array();

	// Defining Generic Errors
	var $CONNECTION_ERROR = "Unable to connect to the database";
	var $RECORD_NOT_FOUND = "No record found";


	function AddObject($o)
	{
		$this->objects[] = $o;
		$this->objectCount++;
	}

	function AddRow($row)
	{
		$this->rows[] = $row;
		$this->count++;
	}

//	function LoadObject($rs)
//	{
//		if(count($rs)==0)
//		{
//			return $data;
//		}
//
//		for ($i=0; $i<count($rs); $i++)
//		{
//			$temp = array();
//			$arr = $rs[$i];
//			$temp[$data->COMPANYID] = $arr['company_id'];
//			$temp[$data->CUSTOMERID] = $arr['customerid'];
//			$temp[$data->COMPANYNAME] = $arr['companyname'];
//			$temp[$data->ACCOUNTNO] = $arr['account_number'];
//
//			$data->AddRow($temp);
//		}
//		return $data;
//	}
}

class CustomerData extends GenericData
{
	// Defining error messages
	var $INVALID_PASSWORD = "Incorrect password provided";
	var $PASSWORD_NOT_CHANGED = "Cannot change password";
	var $REQUIRE_CUSTOMER_ID = "Customer_Id required!";
	var $REQUIRE_BUSINESS_NAME = "Business name is necessary";
	var $REQUIRE_CONTACT_NAME = "Contact name is necessary";
	var $REQUIRE_DEPOSIT_LEFT = "Amount is required";
	var $REQUIRE_PASSWORD = "Password should be of minimum 2 characters";
	var $REQUIRE_EMAIL = "Email address is necessary";
	var $RECORD_NOT_FOUND = "Unable to find the record";
	var $CUST_UPDATED = "Customer information updated";
	var $PASS_CHANGED = "Customer password changed";
	var $AUTH_FAILED = "Invalid Customer Id or Password";	
}

class ConferenceData extends GenericData
{
	// Defining error messages
	var $SEAT_COUNT_MISSING = "Please specify number of seats for conference";
	var $PIN_CODE_MISSING ="Please specify pin code for conference";
	var $START_TIME_MISSING = "Please specify start time for conference";
	var $END_TIME_MISSING = "Please specify end time for conference";
	var $TYPE_MISSING ="Please specify the type of conference";
	var $DESC_MISSING ="Description is missing!";
	var $CONFERENCE_DELETED	="Conference deleted successfully!";
	var $SEAT_NOT_FREE	="Seats are not available at this time, Please select different time span!";


	/*
	var $PASSWORD_NOT_CHANGED = "Cannot change password";
	var $REQUIRE_CUSTOMER_ID = "Customer_Id required!";
	var $REQUIRE_BUSINESS_NAME = "Business name is necessary";
	var $REQUIRE_CONTACT_NAME = "Contact name is necessary";
	var $REQUIRE_DEPOSIT_LEFT = "Amount is required";
	var $REQUIRE_PASSWORD = "Password should be of minimum 2 characters";
	var $REQUIRE_EMAIL = "Email address is necessary";
	var $RECORD_NOT_FOUND = "Unable to find the record";
	var $CUST_UPDATED = "Customer information updated";
	var $PASS_CHANGED = "Customer password changed";
	var $AUTH_FAILED = "Invalid Customer Id or Password";	
	*/
}

/**
 * Branch / Department Class For Error Reporting
 *
 */
class BranchData extends GenericData
{
	// Defining error messages
	var $UNPAID = "Branch is un paid";
	var $INACTIVE = "Barnch is in cative";	
	var $SELECT_COMPANY_FIRST = "Please select the Company first.";
	var $BRANCH_ALREADY_EXIST = "This department name already exists. Kindly rename the department using a different name.";
	var $COMPANY_AMOUNT_GREATER = "The amount is lesser than the amount in Global wallet.";
	//var $UNABLE_DOWNLOAD_MAIL = "";
}

class MailerData extends GenericData
{
	// Defining error messages
	var $INVITE_SENT = "Invitation has been sent";	
}

class UserData extends GenericData
{
	// Defining error messages
	var $REQUIRED_FIRSTNAME = "First name is necessary";
	var $REQUIRED_USERNAME = "User Id is necessary";
	var $ALREADY_EXIST_USERNAME = "User Id already exist, Please try another";
	var $REQUIRE_PASSWORD = "Password should be of minimum 2 characters";
	
	var $REQUIRED_PREV_PASSWORD = "Enter old password";
	var $REQUIRED_NEW_PASSWORD = "Enter new password";
	var $INVALID_PASSWORD = "Invalid username or password";
	
	var $REQUIRE_EMAIL = "Email address is necessary";
	var $USER_UPDATED = "User information updated";
	var $PASS_CHANGED = "User password changed";
	
	var $RECORD_NOT_FOUND = "Unable to find the record";
	var $USER_DELETED = "User deleted successfully";
	var $USER_ADDED = "User added successfully";
	
	var $PR_ACCOUNT_EXIST = "Primary Login Account Already exist.";
	var $PR_ACCOUNT_EXIST_USER = "There is already a primary account configured for this user.";
	var $PR_ACCOUNT_OCCUPIED = "Primary Account already occupied";
	var $PR_ACCOUNT_NOT_ALLOWED ="This id is not allowed";
	var $USR_INFO_REST = "User information is restricted";
}

class SubDomainData extends GenericData
{
	// Defining data columns
	var $DOMAINNAME = "domainName";
	var $ADMIN = "username";
	var $HOSTNAME = "hostname";
	var $CUSTOMERID = "customerid";
	var $HOSTING = "hosting";
	var $PASSWORD = "password";
	var $MONTHS = "months";
	var $DESCRIPTION = "description";
	var $OLDCUSTOMER = "oldCustomer";

	// Defining error messages
	var $ORDERNO_NOT_FOUND = "Order number not found";
}

class EmailAccountData extends GenericData
{
	var $REQUIRED_ACCOUNT_NAME = "Account name is necessary";
	var $REQUIRED_DISPLAY_NAME = "Display name is necessary";
	var $REQUIRED_INCOMING_SERVER = "Incoming server address is necessary";
	var $REQUIRED_INCOMING_USER = "Incoming server username is necessary";
	var $REQUIRED_INCOMING_PASSWORD = "Incoming server password is necessary";
	var $ACCOUNT_EXIST = "Email account already exist";
	var $ACCOUNT_ADDED = "Email account added succesfully";
	var $ACCOUNT_DELETED = "Email account deleted succesfully";
	var $ACCOUNT_DELETED_FAIL = "Unable to delete email account";
	var $ACCOUNT_ADDITION_FAIL = "Unable to add account";
	var $PRI_ACCOUNT_ADDITION_FAIL = "Unable to add primary login";
	var $PRI_ACCOUNT_DELETION_FAIL = "Unable to delete primary login";
	var $INVALID_ACCOUNT = "Account is invalid.";	
}

class TaskData extends GenericData 
{
	var $REQUIRED_TASK_TITLE = "Task title is necessary";
	var $REQUIRED_TASK_MESSAGE = "Task Message is necessary";
	var $REQUIRED_TASK_USER = "Task must be assigned to atleast one user";
	var $NO_TASK_FOUND = "No Task is associated wityh this user";
}
class CompanyData extends GenericData
{
	var $COMPANYNAME_EXIST = "Company name already exist, Please try another";
}

class GroupData extends GenericData
{
	var $GROUP_ADDED = "Group added successfully";
	var $REQUIRED_GROUP_NAME = "Group name is necessary";
}

class MailOptionData extends GenericData
{
	var $UPDATE_SUCCESS = "Mail package updated successfully";
	var $UPDATE_FAILURE = "Cannot Update Mail Packages";
	var $ADD_FAILURE = "Cannot Add Mail Packages";
}

/**
 * Fax Option Data For Error Reporting
 *
 */
class FaxOptionData extends GenericData
{
	var $FWD_NO_MISSING = "Forwarding number is missing"; 
	var $EMAIL_MISSING = "Email Address is missing"; 
	var $UPDATE_SUCCESS = "Fax package updated successfully";
	var $UPDATE_FAILURE = "Cannot Update Fax Packages";
	var $PACKAGE_NOT_ALLOCATED = "Package is as yet unallocated. Please make sure that your payment is updated.";
}

/**
 * Phone Option Data For Error Reporting
 *
 */
class PhoneOptionData extends GenericData
{
	var $VM_FILE_SIZE_EXCEED = "File exceeds size limit: "; 
	var $EMAIL_REQUIRED = "Email address is necessary"; 
	var $UPDATE_SUCCESS = "Phone package updated successfully";
	var $UPDATE_FAILURE = "Cannot update phone packages";
	var $CONTACT_PERSON_REQUIRED = "Contact person is necessary";
	var $AM_REQUIRED = "Announcement message is necessary";
	var $VM_REQUIRED = "Voice message is necessary";
	var $FWDNO_REQUIRED = "Forwarding number is necessary";
	var $OFF_FWDNO_REQUIRED = "Off-forwarding number is necessary";
	var $INTROTEXT_REQUIRED = "Introductory message is necessary";
	var $CELLNO_REQUIRED = "Cell number is necessary";
	var $PACKAGE_NOT_ALLOCATED = "Package is as yet unallocated. Please make sure that your payment is updated.";
}

class ContactData extends GenericData
{
	var $UPDATE_SUCCESS = "Contact updated successfully";
	var $DELETE_SUCCESS  = "Contact deleted successfuly";
}

class CustomerWalletData extends GenericData
{
	// Defining error messages
	var $CUSTOMER_NOT_FOUND = "Customer name is missing";
	var $ORDERID_NOT_FOUND = "Order Id is missing";
	var $COMPANYNAME_NOT_FOUND = "Company name is missing";
	var $AMOUNT_NOT_FOUND = "Amount is missing";
	var $RECORDID_NOT_FOUND = "Record Id is missing";
	var $SERVICETYPE_NOT_FOUND = "Service Type is missing";
	var $LIMIT_EXCEEDS = "Your credit limit in wallet exceed";
	var $COMPANY_AMOUNT_GREATER = "The amount is lesser than the amount in Global wallet.";
}

class EmailData extends GenericData
{
	// Defining error messages
	var $EMAIL_NOT_SENT = "Email message not sent";
	var $EMAIL_SIZE_EXEEDS = "Attachment size greater than 10MB is not allowed.";
	var $SENDER_NOT_ADDED = "Unable to add sender into spam list.";	

}
 
class FilterData extends GenericData
{
	var $FILTER_NAME_MISSING = "Filter Name is missing";
	var $FILTER_NULL = "Filter is empty";
	var	$RECORD_NOT_FOUND = "No record found";
	var	$INVALID_DEST = "Invalid destination please check your filters";

}

class MailRecordData extends GenericData
{
	// Defining error messages
	/*
	var $COMPANYNAME_MISSING = "Company name is missing";
	var $SENDER_NAME_MISSING = "Sender Name is missing";
	var $MAIL_TO_MISSING = "Mail To field is missing";
	var $STATUS_MISSING = "Status is missing";*/
	var	$RECORD_NOT_FOUND = "No mail record found";
}

class ProjectData extends GenericData
{
	// Defining error messages
	var $PROJECT_RIGHTS_ADDED = "Project rights added seccessfully";
	var $PROJECT_RIGHTS_UPDATED = "Project rights updated seccessfully";
	var $PROJECT_RIGHTS_NOT_ADDED = "Unable to assign Project Rights not added";
	var $PROJECT_RIGHTS_NOT_UPDATED = "Unable to update Project Rights not added";
}

/**
 * Voip Option class for error reporting
 *
 */
class VoipOption extends GenericData {
	
	// Defining error messages
	var $UNABLE_UPDATE_SECRET = "Unable to update PIN code. Please try again.";
	var $UNABLE_ASSIGN_EXTENSION = "Unable to assign Extension. Please try again.";
	var $UNABLE_UPDATE_EXTENSION = "Unable to update Extension. Please try again.";
	var $EXTENSION_ALREADY_ASSIGN = "Extension is already in use. Kindly assign a different extension.";
	var $REQUIRED_INFO_MISSING = "Kindly fill in the required information.";
	var $OLD_PASS_NOTMATCH = "Your old PIN code doesn't match";
	var $PASS_CHANGED = "Your PIN code has been changed";
}

class SessionData extends GenericData 
{
	var $SESSION_EXPIRE = "Your session has expired. Kindly login using your login id and password.";
	var $INVALID_IP = "You are unable to continue, Your IP Address is not authenticated.";	
	var $INVALID_SESSIONID = "Invalid Session";
}

class LoginData extends GenericData {
	
	
	var $INVALID_UID_PWD = "Invalid user id or password. Kindly log in using your correct id and/or password.";	
}

class RenewalData extends GenericData {
	
	var $RENEWLINK_EXPIRED = "Renew link has expired. Kindly contact support to receive assistance.";
}

class CompanyWalletData{
	
	var $LIMIT_EXCEEDS = "Your credit limit in wallet exceed";
}


/*
class FileData extends GenericData
{
	// Defining data columns
	var $NAME = "name";
	var $SIZE = "size";
	var $DESC = "desc";
	var $TYPE = "type";
	var $MDATE = "mdate";
	var $EXT = "extension";

	// Defining error messages
	var $INVALID_PATH = "Invalid path";
	var $DIR_NOT_DELETED = "Folder cannot be deleted";
	var $DIR_NOT_EXIST = "Project does not exist";
	var $FILE_NOT_DELETED = "File cannot be deleted";
	var $FILE_NOT_EXIST = "File not exist";
	var $FILE_NOT_MOVED = "File not moved";
	var $CREATE_DIR_FAILED = "Cannot create folder";
	var $INFO_MISSING = "Insufficient information provided";
	var $UPLOAD_FAILED = "Uploading file failed";
	var $DIR_ALREADY_EXIST = "Project already exist";
	var $FILE_ALREADY_EXIST = "File already exist";
}

class DirData extends GenericData
{
	// Defining data columns
	var $NAME = "name";
	var $SIZE = "size";
	var $DESC = "desc";
	var $TYPE = "type";
	var $EXT = "extension";

	var $INVALID_PATH = "Invalid path";
	var $DIR_NOT_EXIST = "Folder does not exist";
}

class PhoneMessageData extends GenericData
{
	// Defining data columns
	var $SNO = "sno";
	var $SUBJECT = "subject";
	var $TOADD = "toadd";
	var $FROMADD = "fromadd";
	var $RCVDATE ="date" ;
	var $STATUS = "status";

	var $CONTACTPERSON = "contactperson";
	var $MESSAGE = "message";
	var $EMAIL = "email";
	var $CONTACTNO = "contactno";
	var $CALLFROM = "callfrom";
	var $CALLERCOMP = "callercomp";

	var $PROJECTID = "projectid";
	var $PROJECTPATH = "projectpath";
	var $STATUS = "status";

	// Defining error messages
	var	$RECORD_NOT_FOUND = "No phone message(s) found";
	var	$DID_NOT_FOUND = "Phone number not found";
	var	$PHONEID_NOT_FOUND = "Phone Id not found";
}

class FaxMessageData extends GenericData
{
	var $mapping = array(
		"sno"=>"record_id"
	);

	// Defining data columns
	var $SNO = "sno";
	var $PHONENO = "phone_no";
	var $SUBJECT = "subject";
	var $SIZE = "pages";
	var $CALLERID = "callerid";
	var $RCVDATE ="date" ;
	var $COMPANY = "company";
	var $STATUS = "status";
	var $PROJECTID = "projectid";
	var $PROJECTPATH = "projectpath";

	// Defining error messages
	var	$DID_NOT_FOUND = "Fax number not found";
	var	$RECORD_NOT_FOUND = "No fax message(s) found";
	var	$FAXID_NOT_FOUND = "Fax Id not found";
}

class VoiceMessageData extends GenericData
{
	// Defining data columns
	var $UNIQUEID = "unique_id";
	var $DIDNO = "didno";
	var $CALLERID = "caller_id";
	var $STATUS = "status";
	var $DATE ="origdate" ;
	var $DURATION ="duration" ;
	var $PROJECTID = "projectid";
	var $PROJECTPATH = "projectpath";

	// Defining error messages
	var	$DID_NOT_FOUND = "Voice number not found";
	var	$RECORD_NOT_FOUND = "No voice message(s) found";
	var $INFO_MISSING = "DID, Project ID, or Project Path is missing";
	var $VOICEID_NOT_FOUND = "Voice Id not found";
}

class EmailData extends GenericData
{
	// Defining data columns
	var $ID = "id";
	var $HEADER = "header";
	var $RECV = "recievedon";
	var $SUBJECT = "subject";
	var $BODY ="body" ;
	var $SENDER ="sender" ;

	// Defining error messages
	var	$MAIL_NOT_FOUND = "MailId not found";
	var	$FOLDER_NOT_FOUND = "Folder is null";
}

class ExternalEmailData extends GenericData
{
	// Defining data columns
	var $USERID = "userid";
	var $EMAILADDRESS = "emailaddress";
	var $EMAILPASSWORD = "emailpassword";
	var	$POPSERVER = "server";
	var	$SMTPSERVER = "smtpserver";
	var $AUTH = "smtpauth";
	var	$SMTPUSER = "smtpuser";
	var	$SMTPPASSWORD = "smtppassword";

	// Defining error messages
	var $INVALID_USER_ID = "Invalid Username";
	var $RECORD_NOT_FOUND = "No records found with this code";
	var $INVALID_EMAIL_ADDRESS = "Invalid Email address";
	var	$INVALID_EMAIL_PASSWORD = "Invalid Email Password";
	var	$INVALID_POPSERVER = "Pop server name is invalid";
	var	$INVALID_SMTPSERVER = "SMTP server name is invalid";
	var	$SMTP_AUTHENTICATION_FAILED = "Fail to autheticate to SMTP server";
	var $INVALID_SMTP_USER = "Invalid SMTP username";
	var $INVALID_SMTP_PASSWORD = "Invalid SMTP password";
	var $QUERY_FAILED = "Fail to enter a record";
}

class AttachmentData extends GenericData
{
	// Defining data columns
	var $ID = "id";
	var $EMAILID = "emailid";
	var $FILENAME = "filename";
	var $FILETYPE = "filetype";
	var $CONTENT ="content" ;

	// Defining error messages
	var	$ATTACHMENT_NOT_FOUND = "Attachment not found.";
	var $ATTACHMENT_NOT_DISPLAYED="Unable to display attachment";
}

class ProjectContentData extends GenericData
{
	// Defining data columns
	var $NAME = "name";
	var $TYPE = "type";
	var $SIZE = "size";

	// Defining error messages
	var $PID_MISSING = "Invalid Project ID";
}

class UserRightsData extends GenericData
{
	// Defining data columns
	var $PROJECTID = "projectid";
	var $PROJECTNAME = "projectname";
	var $RIGHTS = "rights";

	// Defining error messages
	var $INVALID_PATH = "Invalid path";
	var $DIR_NOT_EXIST = "Directory does not exist";
	var $DATA_MISSING = "Username or Project ID is missing";
	var $USER_RIGHTS_NOT_FOUND = "User Rights not found";

}


class ProjectData extends GenericData
{
	var $PROJECTID = "projectid";
	var $PROJECTNAME = "projectname";
	var $COMPANYNAME = "companyname";
	var $DESCRIPTION = "description";
	var $DOMAIN = "domainname";
	var $MANAGEMENT = "management";

	// Defining error messages
	var $PROJECT_NOT_FOUND = "Project does not exist";
	var	$DEST_PROJECTID_NOT_FOUND = "Destination project id not found";
	var	$SRC_PROJECTID_NOT_FOUND = "Source project id not found";
	var	$SRC_PROJECTPATH_NOT_FOUND = "Source project path not found";
	var	$DEST_PROJECTPATH_NOT_FOUND = "Destination path not found";
	var	$SRC_ACCESS_DENIED = "Insufficient access rights on source project";
	var	$DEST_ACCESS_DENIED = "Insufficient access rights on destination project";
	var	$USERNAME_NOT_FOUND = "User name not found";
	var	$ALREADY_EXIST = "Project with this name already exist";
	var	$DOMAIN_NOT_FOUND = "Domain name is null";
	var	$PROJECTNAME_NOT_FOUND = "Project name is null";
	var	$ACCOUNTID_NOT_FOUND = "Account id is null";
}

class CardData extends GenericData
{
	// Defining data columns
	var $ID = "id";
	var $NAME = "name";

	var $RECORD_NOT_FOUND = "No credit card found";
}
*/
class ProductData extends GenericData
{
	// Defining data columns
	var $ID = "productid";
	var $NAME = "name";
	var $CODE = "code";
	var $DURATION = "duration";
	var $RENEWABLE = "renewable";
	var $UPGRADABLE = "upgradable";
	var $RENEWCODE = "renewcode";
	var $SERIALNO = "serialno";
	var $PRICE = "price";
	var $OLDCODE = "oldcode";
	var $RENEWPRICE = "renewprice";

	// Defining error messages
	var $INVALID_CODE = "Invalid code";
	var $RECORD_NOT_FOUND = "No records found with this code";
}

class OrderData extends GenericData
{
	// Defining data columns
	var $INVOICEINFO = "invoiceinfo";
	var $ORDERNO = "orderno";
	var $CUSTOMERID = "customerid";
	var $CUSTOMERUID = "customeruid";
	var $DATE = "date";
	var $ENTITYNAME = "entityname";
	var $ENTITYID = "entityid";
	var $PRODUCTS_OBJECT = "productsObject";
	var $PACKAGE_OBJECT = "packagesObject";
	var $USE_CXL = "usecxl";
	var $CXL_TEST_MODE = "cxltestmode";
	var $ORDER_TYPE = "ordertype";
	var $REFERRER = "referrer";
	var $COMMENTS = "comment";

	// Defining error messages
	var $ORDERNO_NOT_FOUND = "Order number not found";
	var $ORDER_PLACED = "Order placed";

}

class InvoiceData extends GenericData
{
	// Defining data columns
	var $PAYBYCHECK = "paybycheck";
	var $CARD_TYPE = "cardtype";
	var $CARD_HOLDER_NAME = "cardholdername";
	var $CARDNO = "cardno";
	var $CARD_ADDRESS = "cardaddress";
	var $START_DATE = "startdate";
	var $END_DATE = "enddate";
	var $CARD_EXPIRY = "cardexpiry";
	var $ISSUE_NUMBER = "issuenumber";
	var $SECURITY_CODE = "securitycode";
	var $REFERRER = "referrer";

	// Defining error messages
	var $ORDERNO_NOT_FOUND = "Order number not found";

}
class TransactionData extends GenericData
{
	// Defining data columns
	var $TRANSACTIONID = "transactionid";
	var	$COMPANYNAME ="companyname";
	var $AMOUNT = "amount";
	var $DATE = "tdate";
	var $COMMENTS = "comments";
	var $SERVICETYPE = "servicetype";
	var $RECORDID = "record_id";
	var $TRANSACTIONTYPE="transactiontype";

	// Defining error messages
	var $COMPANYNAME_NOT_FOUND = "Company name is missing";
	var $RECORD_NOT_FOUND = "No customer found";
	var $RECORDID_NOT_FOUND = "Recordid is missing";
}

class DiaryData extends GenericData
{
	// Defining error messages
	var $REQUIRED_DIARY_NAME= "Diary name is missing";
	var $RECORD_NOT_FOUND = "No customer found";
}
/*
class DomainData extends GenericData
{
	// Defining data columns
	var $NAME = "name";
	var $COMPANYNAME = "companyname";
	var $ISCODE = "isocode";
	var $EMAIL = "email";
	var $PASSWORD = "password";
	var $DOMAIN = "domainname";
	var $REFERRER = "refererid";
	var $CUSTOMERID = "customerid";
	var $COMPANYNUMBER = "companynumber";
	var $HOSTING = "hosting";
	var $MEMBERINFO = "memberinfo";

	// Defining error messages
	var $ORDERNO_NOT_FOUND = "Order number not found";
}



class DomainMapData extends GenericData
{
	// Defining data columns
	var $DOMAIN = "domainname";
	var $COMPANYID = "companyid";
	var $COMPANYNAME = "companyname";
	var $ACCOUNTNO = "accountnumber";

	// Defining error messages
	var $CUSTID_NOT_FOUND = "Customer Id not found";
	var $COMPID_NOT_FOUND = "Company Id not found";
	var $DOMAIN_EXIST = "Domain already occupied";
}

class CompanyDepositData extends GenericData
{
	// Defining data columns
	var $COMPANYNAME = "companyname";
	var $AMOUNT = "amount";
	var $DATE = "depositdate";
	var $COMMENTS = "comments";
	var $DEPOSITID = "depositid";
	var $ORDERID = "orderid";
	var $STATUS = "status";

	// Defining error messages
	var $COMPANYNAME_NOT_FOUND = "Company name is missing";
	var $ORDERID_NOT_FOUND = "Order Id is missing";
	var $AMOUNT_NOT_FOUND = "Amount is missing";
	var $LIMIT_EXCEEDS = "Your credit limit in wallet exceed";
	var $RECORD_NOT_FOUND = "No company found";
	var $RECORDID_NOT_FOUND = "Record Id is missing";
	var $RECORD_NOT_FOUND = "No company found";
	var $SERVICETYPE_NOT_FOUND = "Service Type is missing";
}

class TransactionData extends GenericData
{
	// Defining data columns
	var $TRANSACTIONID = "transactionid";
	var	$COMPANYNAME ="companyname";
	var $AMOUNT = "amount";
	var $DATE = "tdate";
	var $COMMENTS = "comments";
	var $SERVICETYPE = "servicetype";
	var $RECORDID = "record_id";
	var $TRANSACTIONTYPE="transactiontype";

	// Defining error messages
	var $COMPANYNAME_NOT_FOUND = "Company name is missing";
	var $RECORD_NOT_FOUND = "No customer found";
	var $RECORDID_NOT_FOUND = "Recordid is missing";
}
class BunchManagerData extends GenericData
{
	//Defining data columns
	var $BUNCHID = "bunchid";
	var $COMPANYNAME = "companyname";
	var $CREATIONDATE = "creationdate";
	var $COST = "cost";
	var $STATUS = "status";

	//Defining error messages
	var $INVALID_ID = "Invalid bunch id, please enter valid bunch id";
	var $INVALID_STATUS = "Invalid status";
	var $INVALID_COMPANY_NAME = "Invalid company name";
	var $INVALID_DURATION = "Invalid duration: Duration must be greater than 1 day";
	var $RECORD_UPDATE_FAILED = "Failed to update record";
	var $CREATE_BUNCH_FAILED = "Failed to create new bunch";
	var $RECORD_NOT_FOUND = "Record not found";
}

class PhoneRecordData extends GenericData
{
	// Defining data columns

	var $RECORDID = "record_id";
	var $COMPANYNAME = "comp_name";
	var $PHONE_NO="phone_no";
	var	$PHONE_ID ="phone_id";
	var	$CALLER_ID ="caller_id";
	var	$CALL_DATE ="call_date";
	var $DURATION = "duration";
	var $BILLSEC = "bill_sec";
	var $DISPOSITION = "disposition";
	var $PARENT_FILE="parent_file";
	var $STATUS="vmail_status";
	var $LAST_APP="last_app";
	var $RATE = "rate";
	var $SENT_TO="sent_to";
	var $LAST_DATA="last_data";
	var	$SRC ="src";
	var	$DST ="dst";
	var	$DIALEDNUM ="dailed_num";

	// Defining error messages
	var $COMPANYNAME_MISSING = "Company name is missing";
	var $RECORDID_MISSING = "Record Id is missing";
	var	$RECORD_NOT_FOUND = "No phone record found";
	var	$DID_MISSING = "Fax number is missing.";

}

class FaxRecordData extends GenericData
{
	// Defining data columns

	var $RECORDID = "record_id";
	var $COMPANYNAME = "comp_name";
	var	$CALL_DATE ="recv_date";
	var	$CALLER_ID ="caller_id";
	var	$SIZE ="pages";

	// Defining error messages
	var $COMPANYNAME_MISSING = "Company name is missing";
	var $RECORDID_MISSING = "Record Id is missing";
	var	$RECORD_NOT_FOUND = "No phone record found";
	var	$DID_MISSING = "Fax number is missing.";

}

class MailGridData extends GenericData
{
	// Defining data columns
	var $COMPANYNAME = "companyname";
	var $BUSINESSNAME = "businessname";
	var	$CUSTOMERID ="customerid";
	var	$CUSTOMERUID ="customeruid";
	var	$AMOUNT ="amount";
	var	$NEW_MAILS="new_mail";
	var	$SENT_MAILS ="sent_mail";
	var	$PENDING_COLLECT="pending_collect";
	var	$PENDING_DISPATCH ="pending_dispatch";
	var $COMP_STATUS ="companystatus";

	// Defining error messages
	var $COMPANYNAME_MISSING = "Company name is missing";
	var	$RECORD_NOT_FOUND = "No mail record found";

}

class UserRecordData extends GenericData
{
	//Defining data columns
	var $USER_ID = "user_id";
	var $COMP_ID = "comp_id";
	var $USER_NAME = "user_name";
	var $USER_TYPE = "user_type";
	var $FIRST_NAME = "first_name";
	var $MIDDLE_NAME = "middle_name";
	var $LAST_NAME = "last_name";
	var $EMAIL = "email";

	//Defining error messages
	var $USER_ID_MISSING = "";
	var $COMP_ID_MISSING = "";
	var $USER_NAME_INVALID = "";
	var $USER_TYPE_MISSING = "";
	var $FIRST_NAME_MISSING = "";
	var $LAST_NAME_MISSING = "";
}

class UserProfileData extends GenericData
{
	// Defining data columns
	var $USERID = "userid";
	var	$MSGPAGESIZE ="msgpagesize";
	var	$AUTOPREVIEW ="autopreview";

	// Defining error messages
	var $USERID_MISSING = "Userid is missing";
	var	$RECORD_NOT_FOUND = "No record found";
}



class WalletStatusData extends GenericData
{
	// Defining data columns
	var $STATUS_ID = "id";
	var $COMPANY_ID = "company_id";
	var $COMPANY_NAME = "company_name";
	var	$NOTIFY_COUNT ="notify_count";
	var	$THRESHOLD_AMOUNT ="threshold_amount";
	var	$CUTOFF_AMOUNT ="cutoff_amount";
	var	$IS_DEFAULT ="is_default";
	var	$WALLET_STATUS ="wallet_status";

		// Defining error messages
	var $STATUS_ID_NOT_FOUND = "status id not found";
	var $COMPANY_ID_NOT_FOUND = "company id is invalid or it doesnot exist";
	var $COMPANY_NAME_NOT_FOUND = "company name doesnot exist";
	var	$NOTIFY_COUNT_INVALID ="notify count is invalid";
	var	$THRESHOLD_AMOUNT_NOT_FOUND ="threshold amount not found";
	var	$CUTOFF_AMOUNT_NOT_FOUND ="cutoff amount not found";
	var	$IS_DEFAULT_INVALID ="not set to default";
	var $RECORD_NOT_FOUND = "no record found";
}

class WalletLogData extends GenericData
{
	// Defining data columns
	var $ID = "id";
	var $COMPANY_ID = "company_id";
	var	$MAIL_TYPE ="mail_type";
	var	$LOG_TIME ="log_time";
	var	$EMAIL ="email";
		// Defining error messages

	var $COMPANY_ID_NOT_FOUND = "company id is invalid or it doesnot exist";
	var	$MAIL_TYPE_NOTFOUND ="not set to default";
	var $RECORD_NOT_FOUND = "no record found";
}

class ContactData extends GenericData 
{
	// Defining error messages for Contacts
	var	$RECORD_NOT_FOUND = "No Contact(s) found";
	var $EMPTY_CONTACT_ID = "Contact Id is empty";
	var $CONTACT_ADD_FAIL = "Can't Add Record";
	var $UPDATE_FAIL = "Can't Update Record";
}
*/
class ErrorData
{
	var $desc;
	var $code;
	var $type;
	var $line;
	var $func;
	var $filename;	
	var $classname;

	/*
	Type 
	1. notice
	2. warning
	3. error
	4. critical
	5. silent
	*/
	
	function ErrorData($desc,$filename="",$line="",$type="notice")
	{
		//$this->desc = $desc;
		//$this->line = $line;
		//$this->filename = $filename;
		//$this->type = $type;
		//$this->code = $type;
		
		$this->desc = $desc;
		$this->code = -1;
		$this->type = $type;

		$debug_array = debug_backtrace();
		//traceMessage(print_r_log($debug_array));
		
		$last = 1;//count($debug_array)-1;
		
		if (strtolower($debug_array[$last]["function"]) != "logmsg")
			$last--;
		
		$filename = $debug_array[$last]["file"];
		$this->line = $debug_array[$last]["line"];
		$this->func = $debug_array[$last+1]["function"];
		$this->classname = $debug_array[$last+1]["class"];

		if (strstr(strtolower($desc),"in:"))
		{
			$desc = $desc."\r\n".print_r_log($debug_array[$last+1]["args"]);
			$serializeDesc = serialize($debug_array[$last+1]["args"]);
			$this->desc = $desc.":SerializeData:".$serializeDesc;
			
		}
		
		$fileArr = explode("/",$filename);
		$filename = "";
		$length = count($fileArr);
		
		if($fileArr[$length-3])
			$filename .= "/".$fileArr[$length-3];
		if($fileArr[$length-2])			
			$filename .= "/".$fileArr[$length-2];
		if($fileArr[$length-1])			
			$filename .= "/".$fileArr[$length-1];
		
		$this->filename = $filename;		
	}
}

define ("ERR_DATA_OBJECT_NULL","Data object is null");



?>