 <?php
require_once(MODULE."/class/datalayer/crm.php");
/**
 * CRM Business Layer Class
 */
class BL_Crm
{
    function AddLead($data)
    {
        traceMessage('Add Lead'.print_r_log($data));
        $dlObj = new DL_Crm();
        return $dlObj->AddLead($data);
    }

    function GetCustomerIdByPhoneNumber($data)
    {
        $dlObj = new DL_Crm();
        return $dlObj->GetCustomerIdByPhoneNumber($data);
    }

    function NearCustomerUserId()
    {
        $dlObj = new DL_Crm();
        $data['start_date_time'] = date('Y-m-d H:i:s',strtotime("-30 minutes"));
        $data['end_date_time'] = date('Y-m-d H:i:s',strtotime("now"));
        return $dlObj->NearCustomerUserId($data);
    }

    function AssignUsersToLeadId($data)
    {
        $dlObj = new DL_Crm();
        return $dlObj->AssignUsersToLeadId($data);
    }

    function GetUserIdByCustomerId($data)
    {
        $dlObj = new DL_Crm();
        return $dlObj->GetUserIdByCustomerId($data);
    }

    function GetLeadIdByNic($data)
    {
        $dlObj = new DL_Crm();
        return $dlObj->GetLeadIdByNic($data);
    }

    function GetLeads($data)
    {
        traceMessage("Get leads data : ".print_r_log($data));
        $dlObj = new DL_Crm();
        return $dlObj->GetLeads($data);
    }

    function GetLeadsRelatedUser($data)
    {
        traceMessage("Get leads related users".print_r_log($data));
        $dlObj = new DL_Crm();
        return $dlObj->GetLeadsRelatedUser($data);
    }

    function GetLeadDetail($data)
    {
        $dlObj = new DL_Crm();
        return $dlObj->GetLeadDetail($data);
    }
}

?>
