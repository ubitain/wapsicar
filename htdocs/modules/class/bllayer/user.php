 <?php
	require_once(MODULE."/class/datalayer/user.php");
	class BL_User
	{
		function GetDealerCatergory()
		{
			$dlObj = new DL_User();
			return $dlObj->GetDealerCatergory();
		}

		function GetSubDealerCatergory()
		{
			$dlObj = new DL_User();
			return $dlObj->GetSubDealerCatergory();
		}

		function GetGroups()
		{
			$dlObj = new DL_User();
			return $dlObj->GetGroups();
		}
        function VehicleModelInformation()
    {
        $viewuser= new DL_User();
        return $viewuser->VehicleModelInformation();
    }
		function GetCity()
		{
			$dlObj = new DL_User();
			return $dlObj->GetCity();
		}
		function GetVehicle()
		{
			$dlObj = new DL_User();
			return $dlObj->GetVehicle();
		}
        function GetVehicleModel()
		{
			$dlObj = new DL_User();
			return $dlObj->GetVehicleModel();
		}

		function GetRegions()
		{
			$dlObj = new DL_User();
			return $dlObj->GetRegions();
		}

		function GetZones()
		{
			$dlObj = new DL_User();
			return $dlObj->GetZones();
		}
        function GetRoute()
		{
			$dlObj = new DL_User();
			return $dlObj->GetRoute();
		}
		function GetDeliveryDetails()
		{
			$dlObj = new DL_User();
			return $dlObj->GetDeliveryDetails();
		}
		function GetTown()
		{
			$dlObj = new DL_User();
			return $dlObj->GetTown();
		}
		
		
		function GetPurchaseFromList()
		{
			$dlObj = new DL_User();
			return $dlObj->GetPurchaseFromList();
		}
        function VehicleInformation()
    {
        $viewuser= new DL_User();
        return $viewuser->VehicleInformation();
    }
		function GetAllUsers()
		{
			$dlObj = new DL_User();
			return $dlObj->GetAllUsers();
		}

		function AddUserInfo($userinfo)
		{
			$addUser=new DL_User();
			$addUser->AddUserInfo($userinfo);
		}

		function UpdateUserInfo($userinfo,$userID)
		{
			$addUser=new DL_User();
			return $addUser->UpdateUserInfo($userinfo,$userID);
		}

		function DeleteUser($userArray,$userID)
		{
			$deleteUser=new DL_User();
			$deleteUser->DeleteUser($userArray,$userID);
		}

		function GetSubCategory($categoryid)
		{
			$category=new DL_User();
			return $category->GetSubCategory($categoryid);

		}
		function GetRegion($zone)
		{
			$region=new DL_User();
			return $region->GetRegion($zone);

		}
		function GetCityNames($region)
		{
			$city=new DL_User();
			return $city->GetCityNames($region);

		}
		function GetArea($city)
		{
			$area=new DL_User();
			return $area->GetArea($city);

		}
		function GetPurchaseFrom($zoneId,$regionId,$cityId,$areaId)
		{
			$area=new DL_User();
			return $area->GetPurchaseFrom($zoneId,$regionId,$cityId,$areaId);

		}
		function GetAccountof($zoneId,$regionId,$cityId,$areaId)
		{
			$area=new DL_User();
			return $area->GetAccountof($zoneId,$regionId,$cityId,$areaId);

		}
		function DeletedDriver()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->DeletedDriver();
	    }
     	function DeletedCustomer()
	    {
	    	traceMessage('92199'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->DeletedCustomer();
	    }
	    function PaymentPending()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->PaymentPending();
	    }
	    function PaymentApprove()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->PaymentApprove();
	    }
		function DriverWithPaymentApprove()
	    {
	    	traceMessage('78686'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->DriverWithPaymentApprove();
	    }
		function CustomerWithBookingPayment(){
			tracemessage('customer booking payment in BL'.print_r_log($data));
			$dlObj = new DL_User();
			return $dlObj->CustomerWithBookingPayment();
		}
	    function PaymentUnlink()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->PaymentUnlink();
	    }
	    function PaymentConflict()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->PaymentConflict();
	    }

		function AuhenticateUser($userId="",$password="",$enc="no")
		{

			$e1 = new ErrorData(__FUNCTION__ . " userid is not provided",__FILE__,__LINE__,'notice');
			if(CheckCondition(!isset($userId)||$userId=='', $e1))
			return false;
			$dlObj = new DL_User();
			return $dlObj->AuhenticateUser($userId,$password,$enc);
		}
		function GetAllUserInfo()
		{
			traceMessage("in BL GetAllUserInfo");
			$dlObj = new DL_user();
			return $dlObj->GetAllUserInfo();
		}
		function GetAllAdminUserInfo()
		{
			traceMessage("in BL GetAllAdminUserInfo");
			$dlObj = new DL_user();
			return $dlObj->GetAllAdminUserInfo();
		}
		function GetAllUserByUserType($type,$status=1)
		{
			if (empty($type)) {
				return false;
			}
			traceMessage("in BL GetAllUserByUserType($type,$status)");
			$dlObj = new DL_user();
			return $dlObj->GetAllUserByUserType($type,$status);
		}
		function GetAsmInfo($customerType,$status)
		{
			traceMessage("in BL GetAsmInfo");
			$dlObj = new DL_user();
			return $dlObj->GetAsmInfo($customerType,$status);
		}
		function GetMedRepInfoByUserId($userId,$medRepId)
		{
			traceMessage("in BL GetMedRepInfo ".$medRepId);
			$dlObj = new DL_user();
			return $dlObj->GetMedRepInfoByUserId($userId,$medRepId);
		}
		function GetUserInfo($userId)
		{
			$e1 = new ErrorData(__FUNCTION__ . " userid is not provided",__FILE__,__LINE__,'notice');
			if(CheckCondition(!isset($userId)||$userId==''||$userId==0, $e1))
			return false;
			$dlObj = new DL_User();
			return $dlObj->GetUserInfo($userId);
		}
		function GetUserInfoByPhone($phoneNo)
		{
			$dlObj = new DL_User();
			return $dlObj->GetUserInfoByPhone($phoneNo);
		}
		function GetUserInfoByUserType($userType)
		{
			//echo "sss";
			$dlObj = new DL_User();
			return $dlObj->GetUserInfoByUserType($userType);
		}
		function AddUser($arr)
		{
			$dlObj = new DL_User();
			return $dlObj->AddUser($arr);
		}
		function AddAsm($arr)
		{
			traceMessage("datafrombl:".print_r_log($arr));
			$dlObj = new DL_User();
			return $dlObj->AddAsm($arr);
		}
		function UpdateUser($arr,$id)
		{
			traceMessage("data from bl: $id".print_r_log($arr));
			$dlObj = new DL_User();
			return $dlObj->UpdateUser($arr,$id);
		}
		function DeleteUserReference($table,$arr,$whereName,$id)
		{
			traceMessage("DeleteUserReference table name $table from bl: $whereName=$id".print_r_log($arr));
			$dlObj = new DL_User();
			return $dlObj->DeleteUserReference($table,$arr,$whereName,$id);
		}
		function CheckLoginId($loginName)
		{
			$e1 = new ErrorData(__FUNCTION__ . " loginname is not provided",__FILE__,__LINE__,'notice');
			if(CheckCondition(!isset($loginName)||$loginName=='', $e1))
			return false;
			$dlObj = new DL_User();
			return $dlObj->CheckLoginId($loginName);
		}
		function AddEvent($info,$infoDoctor,$infoTime)
		{	traceMessage("In Bl".print_r_log($arr));
			$dlObj = new DL_User();
			return $dlObj->AddEvent($info,$infoDoctor,$infoTime);
		}
		function GetUserMapping($userId)
		{
			traceMessage("In Bl GetUserMapping".$userId);
			$dlObj = new DL_User();
			return $dlObj->GetUserMapping($userId);
		}
		function GetAllVisitByType($data)
		{

			$dlObj = new DL_User();
			return $dlObj->GetAllVisitByType($data);
		}
		function GetAllVisitByIds($data)
		{

			$dlObj = new DL_User();
			return $dlObj->GetAllVisitByIds($data);
		}
		function GetVisitDetailsByIds($visitId)
		{

			$dlObj = new DL_User();
			return $dlObj->GetVisitDetailsByIds($visitId);
		}
		function GetAllVisitByCatergory($data)
		{

			$dlObj = new DL_User();
			return $dlObj->GetAllVisitByCatergory($data);
		}
		function GetAllVisitByCustomer($data)
		{

			$dlObj = new DL_User();
			return $dlObj->GetAllVisitByCustomer($data);
		}
		function GetUserDataByUserId($userId)
		{
			$dlObj = new DL_User();
			return $dlObj->GetUserDataByUserId($userId);
		}
		function GetTerritoryByTerritoryId($territoryIds)
		{
			$dlObj = new DL_User();
			return $dlObj->GetTerritoryByTerritoryId($territoryIds);
		}
		function MonthlyMedRepReportInfo($data)
		{
			traceMessage("In BL MonthlyMedRepReportInfo");
			$dlObj = new DL_User();
			return $dlObj->MonthlyMedRepReportInfo($data);
		}
		function MedicalRepVisitInfo($userId,$searchDate)
		{
			traceMessage("In BL MedicalRepVisitInfo");
			$searchDate=date("Y-m-d",strtotime($searchDate));
			$dlObj = new DL_User();
			return $dlObj->MedicalRepVisitInfo($userId,$searchDate);
		}
		function MonthlyAttendanceReportInfo($data)
		{
			traceMessage("In BL MonthlyAttendanceReportInfo");
			$dlObj = new DL_User();
			return $dlObj->MonthlyAttendanceReportInfo($data);
		}
		function MonthlyAttendanceUserInfo($userId,$searchDate)
		{
			traceMessage("In BL MonthlyAttendanceUserInfo");
			$dlObj = new DL_User();
			return $dlObj->MonthlyAttendanceUserInfo($userId,$searchDate);
		}
		function GetAllCities($status=1)
		{
			traceMessage("in BL GetAllCities");
			$dlObj = new DL_user();
			return $dlObj->GetAllCities($status);
		}
		function GetAllRegions($status=1)
		{
			traceMessage("in BL GetAllRegions");
			$dlObj = new DL_user();
			return $dlObj->GetAllRegions($status);
		}
		function GetAllDepartment($status=1)
		{
			traceMessage("in BL GetAllDepartment");
			$dlObj = new DL_user();
			return $dlObj->GetAllDepartment($status);
		}
		function checkuserName($userId,$oldPassword)
		{
			traceMessage("GetCitiesByAsmId bl");
			$dlObj = new DL_user();
			return $dlObj->checkuserName($userId,$oldPassword);
		}
		function UpdatePassword($userId,$password)
		{
			traceMessage("UpdatePassword bl");
			$dlObj = new DL_user();
			return $dlObj->UpdatePassword($userId,$password);
		}
		function AddNewPassword($arr)
		{
			traceMessage("data from bl:".print_r_log($arr));
			$dlObj = new DL_User();
			return $dlObj->AddNewPassword($arr);
		}
		function GetLastThreePassword($userId,$limit=3)
		{
			traceMessage("GetLastThreePassword BL");
			$dlObj = new DL_User;
			return $dlObj->GetLastThreePassword($userId,$limit);
		}
		function GetAllAreas($status=1)
		{
			traceMessage("in BL GetAllAreas");
			$dlObj = new DL_user();
			return $dlObj->GetAllAreas($status);
		}

		function GetActualVSTargetVisit($data)
		{
			traceMessage("GetActualVSTargetVisit BL".print_r_log($data));
			$dlObj = new DL_User;
			return $dlObj->GetActualVSTargetVisit($data);
		}
		function GetAccountSummary($data)
		{
			traceMessage("GetAccountSummary BL".print_r_log($data));
			$dlObj = new DL_User;
			return $dlObj->GetAccountSummary($data);
		}
		function changeVisitType($visitId,$status)
		{
			traceMessage("changeVisitType BL".print_r_log($visitId,$status));
			$dlObj = new DL_User;
			return $dlObj->changeVisitType($visitId,$status);
		}
		function InsertNotification($data)
		{
			traceMessage("InsertNotification BL".print_r_log($data));
			$dlObj = new DL_User;
			return $dlObj->InsertNotification($data);
		}
		function GetAllVisitCustomerStatus()
		{
			traceMessage("GetAllVisitCustomerStatus BL");
			$dlObj = new DL_User;
			return $dlObj->GetAllVisitCustomerStatus();
		}

		function SessionDetailById($loginId,$sid="")
		{
			traceMessage("SessionDetailById BL");
			$dlObj = new DL_User;
			return $dlObj->SessionDetailById($loginId,$sid);
		}
		function DeleteUserSession($loginId,$sid="")
		{
			traceMessage("DeleteUserSession BL");
			$dlObj = new DL_User;
			return $dlObj->DeleteUserSession($loginId,$sid);
		}

		function GetDriverRidesInfo($userid){

			$dlObj = new DL_User();
			return $dlObj->GetDriverRidesInfo($userid);
		} 
		function GetUserRidesInfo($userid){

			$dlObj = new DL_User();
			return $dlObj->GetUserRidesInfo($userid);
		} 
		function GetPaymentInfo($userId){

			$dlObj = new DL_User();
			return $dlObj->GetPaymentInfo($userId);
		}

		function AddDriverPayment($info){

			$dlObj = new DL_User();
			return $dlObj->AddDriverPayment($info);
		}
		
		function UpdateDriverPayment($arr,$id){

			$dlObj = new DL_User();
			return $dlObj->UpdateDriverPayment($arr,$id);
		}
		function UpdateOtp($arr,$otp){

			$dlObj = new DL_User();
			return $dlObj->UpdateOtp($arr,$otp);
		}
		function UpdateUsers($arrr,$driverId){

			$dlObj = new DL_User();
			return $dlObj->UpdateUsers($arrr,$driverId);
		}
			function SelectDriverPayment($transactionNumber){

			$dlObj = new DL_User();
			return $dlObj->SelectDriverPayment($transactionNumber);
		}
		function SelectOtp($driverId,$agent){

			$dlObj = new DL_User();
			return $dlObj->SelectOtp($driverId,$agent);
		}

		function GetCityRateCard($userid){

			$dlObj = new DL_User();
			return $dlObj->GetCityRateCard($userid);
		}
		function UpdateShiftStatus($userId,$shiftStatus){

			$dlObj = new DL_User();
			return $dlObj->UpdateShiftStatus($userId,$shiftStatus);
		}
		function DeletePaymentInfo($userId,$paymentId){

			$dlObj = new DL_User();
			return $dlObj->DeletePaymentInfo($userId,$paymentId);
		}
		function GetPaymentInfoById($userId,$paymentId){

			$dlObj = new DL_User();
			return $dlObj->GetPaymentInfoById($userId,$paymentId);
		}
		function GetAvailableCities($fromCity)
		{
			$dlObj = new DL_User();
			return $dlObj->GetAvailableCities($fromCity);
		}
		function GetAvailableDriverForCity($fromCity,$toCity)
		{
			$dlObj = new DL_User();
			return $dlObj->GetAvailableDriverForCity($fromCity,$toCity);
		}
		function GetNotificationLists($userId)
		{
			$dlObj = new DL_User();
			return $dlObj->GetNotificationLists($userId);
		}
		function MarkAsRead($notId,$userId)
		{
			$dlObj = new DL_User();
			return $dlObj->MarkAsRead($notId,$userId);
		}

		function AddNotification($notArr)
		{
			$dlObj = new DL_User();
			return $dlObj->AddNotification($notArr);
		}
		function DriverSetLocation($userId,$lat,$long)
		{
			$dlObj = new DL_User();
			return $dlObj->DriverSetLocation($userId,$lat,$long);
		}
	
		function GetDriverCityRate($driverId,$destCity)
		{
			$dlObj = new DL_User();
			return $dlObj->GetDriverCityRate($driverId,$destCity);
		}
		function AddAgent($data)
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_user();
	        return $dlObj->AddAgent($data);
	    }
	    function AddOtpCode($data,$randomnum)
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_user();
	        return $dlObj->AddOtpCode($data,$randomnum);
	    }
	    function ReportAgent($fromDate,$toDate)
	    {
	    	traceMessage('92198');
	        $dlObj = new DL_user();
	        return $dlObj->ReportAgent($fromDate,$toDate);
	    }
	    function GetAgent()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->GetAgent();
	    }
	    function GetBooking()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->GetBooking();
	    }
	    function GetCityRequest()
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->GetCityRequest();
	    }
	    function GetAgentDriver($data)
	    {
	    	traceMessage('92198'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->GetAgentDriver($data);
	    }
	    function EditAgent($data)
	    {
	    	traceMessage('92190'.print_r_log($data));
	        $dlObj = new DL_User();
	        return $dlObj->EditAgent($data);
	    }
	    function UpdateAgent($usereditid,$EditusersInfo)
	    {
	        traceMessage('Editagent'.print_r_log($usereditidid));
	        $dlObj = new DL_User();
	        return $dlObj->UpdateAgent($usereditid,$EditusersInfo);
	    }
	    function DeleteCityStatus($usereditid,$EditusersInfo)
	    {
	        traceMessage('Editagent'.print_r_log($usereditidid));
	        $dlObj = new DL_User();
	        return $dlObj->DeleteCityStatus($usereditid,$EditusersInfo);
	    }
	    function UpdateCityStatus($usereditid,$EditusersInfo)
	    {
	        traceMessage('Updatecitystatus'.print_r_log($usereditidid));
	        $dlObj = new DL_User();
	        return $dlObj->UpdateCityStatus($usereditid,$EditusersInfo);
	    }
	    function CheckMobile($mobile)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_User();
			return $dlObj->CheckMobile($mobile);
			
		}
		function GetAgentById($agentId)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_User();
			return $dlObj->GetAgentById($agentId);
			
		}
		function VerifyOtp($driverId,$otp)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_User();
			return $dlObj->VerifyOtp($driverId,$otp);
			
		}
		function MarkOTPAsDone($driverId,$otpCode)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_User();
			return $dlObj->MarkOTPAsDone($driverId,$otpCode);
			
		}
		function GetAgentInfoByUser($userId)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_User();
			return $dlObj->GetAgentInfoByUser($userId);
			
		}
		
	}
?>