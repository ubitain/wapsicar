 <?
require_once(MODULE."/class/datalayer/dashboard.php");
require_once(MODULE."/class/datalayer/user.php");


class BL_Dashboard
{
	function GetCurrentMonthDistanceByUserId($userId,$data)
    {
    	$e1 = new ErrorData(__FUNCTION__ . " userid is not provided",__FILE__,__LINE__,'notice');
		if(CheckCondition(!isset($userId)||$userId=='', $e1))
			return false;

        $dlObj = new DL_Dashboard();
        return $dlObj->GetCurrentMonthDistanceByUserId($userId,$data);
    }
	function GetCurrentMonthTimeSpentByUserId($userId,$data)
    {
    	$e1 = new ErrorData(__FUNCTION__ . " userid is not provided",__FILE__,__LINE__,'notice');
		if(CheckCondition(!isset($userId)||$userId=='', $e1))
			return false;

        $dlObj = new DL_Dashboard();
        return $dlObj->GetCurrentMonthTimeSpentByUserId($userId,$data);
    }
    function GetSalesOrderReports($data){
		$dlObj = new DL_Dashboard();
        $result =  $dlObj->GetSalesOrderReports($data);
        return $result->rows;
	}
	function GetVisitPlans($filterData = array()){


		$dlObj = new DL_Dashboard();

		if($filterData['visit_type'] == "sales"){


			//getting total visit plan
			$visitPlans = $dlObj->GetVisitPlans($filterData);
			$visitData = array();
			if($visitPlans->count > 0){
				foreach ($visitPlans->rows as $key => $value) {

					$visitData['user_id'][] = $value['user_id'];
					$visitData['plan_date'][] = "'".$value['planedDate']."'";

				}

				// print_r($visitData);
				// exit();
				$total_PlanedVisits = array_count_values($visitData['user_id']);

				$completedVisits = $dlObj->GetCompletedVisits($visitData,$filterData);

				// print_r($filterData);
				$otherCompletedVisits = $dlObj->GetOtherCompletedVisits($visitData,$filterData);

				// print_r($completedVisits,$otherCompletedVisits);exit();


			}else{
				$completedVisits = new stdClass();
				$total_PlanedVisits = array();
			}

			// print_r([$total_PlanedVisits,$completedVisits,$otherCompletedVisits]);exit();

			$visitsMeta = array();


			foreach ($visitPlans->rows as $visitkey => $visitValue) {

				foreach ($total_PlanedVisits as $user_ids => $userCount) {
					if($user_ids == $visitValue['user_id']){
						$visitPlans->rows[$visitkey]['userCount'] = $userCount;
					}
				}

				foreach ($completedVisits->rows as $completedKey => $comppledtedValue) {

					if($visitValue['user_id'] == $comppledtedValue['user_id']){


						// if($visitValue['planedDate'] == $comppledtedValue['visited_time']){


							$visitsMeta[$visitkey]['completedVisits'][] = 1;

							$visitsMeta[$visitkey]['user_id'][] = $comppledtedValue['user_id'];

							$visitsMeta[$visitkey]['visitIds'][] = $comppledtedValue['visit_id'];

						// }

					}

				}

				// print_r($otherCompletedVisits->rows);exit();

				foreach ($otherCompletedVisits->rows as $otherKey => $otherValue) {

					if($visitValue['user_id'] == $otherValue['user_id']){

						// if($visitValue['planedDate'] == $otherValue['visited_time']){

							$visitsMeta[$visitkey]['otherCompletedVisites']['count'][] = 1;
							$visitsMeta[$visitkey]['otherCompletedVisites']['visit_ids'][] = $otherValue['visit_id'];

						// }

					}
				}

			}
			$visitPlans->rows = unique_multidim_array($visitPlans->rows,'user_id');

			$returnArray = array('visitData' => $visitPlans,'visitMeta' => $visitsMeta);

			// print_r($returnArray);exit();

			return $returnArray;


		}else{

			// echo "<pre>";

			$recoveryVisits = $dlObj->GetRecoveryCompletedVisits($filterData);
			$visitData = $recoveryVisits->rows;

			$ledger_ids = array();
			$recoveryMeta = array();
			foreach ($visitData as $rowkey => $rowvalue) {

				$ledger_ids[] = $rowvalue['ledger_id'];

				// print_r($ledger_ids);

				if(in_array($rowvalue['ledger_id'], $ledger_ids)){

				  $visitData[$rowkey]['customerCount'][] = 1;
				  $recoveryMeta[$rowvalue['ledger_id']]['totalVisits'] += 1;
				  $recoveryMeta[$rowvalue['ledger_id']]['totalAmount'] += $rowvalue['amount'];

				  // echo "Good <br>";

				}

			}


			$visitData = unique_multidim_array($visitData,'ledger_id');

			// print_r([$visitData,$visitsCounts]);exit();
			$returnArray = array('visitData' => $visitData , 'recoveryMeta' => $recoveryMeta);

			return $returnArray;

		}



	}


	function GetPlannedVisitsByIds($data){


		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit();

		if($data['request'] == "" ){

			$dlObj = new DL_Dashboard();

			return $dlObj->GetPlannedVisitsByIds($data);

		}else if($data['request'] == "remaining"){

			$dlObj = new DL_Dashboard();

			$visitPlans = $dlObj->GetVisitPlans($data);

			$incompleteVisits = new stdClass();
			if($visitPlans->count > 0){
				foreach ($visitPlans->rows as $key => $value) {

					$visitData['user_id'][] = $value['user_id'];
					$visitData['plan_date'][] = "'".$value['planedDate']."'";

				}

				$completedVisits = $dlObj->GetRemainigVisits($visitData);

				$incompleteVisists = array();
				$checkInc = 0;


				foreach ($visitPlans->rows as $vkey => $vvalue) {
					$checkInc = 0;

					foreach ($completedVisits->rows as $ikey => $ivalue) {


						if($ivalue['customer_id'] == $vvalue['customer_id'] && $ivalue['planedDate'] == $vvalue['visited_time']){
							$checkInc = 1;
						}

					}

					if($checkInc == 0){

						$incompleteVisits->rows[] = $vvalue;


					}


				}

				return $incompleteVisits;


			}

			return $incompleteVisits->rows = array();


		}
	}

	function GetCalls($visitType,$childIds,$fromDate,$toDate)
	{
		$dlObj =  new DL_Dashboard();
		return $dlObj->GetCalls($visitType,$childIds,$fromDate,$toDate);
	}

}
?>
