
<?php
	require_once(MODULE."/class/datalayer/addstuff.php");
	class BL_AddStuff
	{
		function AddCity($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddCity($addstuff);
		}
		function UpdateCity($addstuff,$cityid)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateCity($addstuff,$cityid);
		}
		function UpdateVahicle($usereditid,$editusersInfo)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateVahicle($usereditid,$editusersInfo);
		}
		function UpdateVahicleModel($userEditId,$editusersInfo)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateVahicleModel($userEditId,$editusersInfo);
		}
		// function UpdateRoute($addstuff,$cityid)
		// {
		// 	$dlObj = new DL_AddStuff();
		// 	return $dlObj->UpdateRoute($addstuff,$cityid);
		// }
        function AddVahicle($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddVahicle($addstuff);
		}
		function AddVahicleModel($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddVahicleModel($addstuff);
		}
		function GetCityById($city_id){

			$dlObj = new DL_AddStuff();
			return $dlObj->GetCityById($city_id);
		}
		// function GetRouteById($city_id){

		// 	$dlObj = new DL_AddStuff();
		// 	return $dlObj->GetRouteById($city_id);
		// }
		function EditVehicle($data)
	    {
	    	traceMessage('92190'.print_r_log($data));
	        $dlObj = new DL_AddStuff();
	        return $dlObj->EditVehicle($data);
	    }
	    function EditVehicleModel($data)
	    {
	    	traceMessage('92190'.print_r_log($data));
	        $dlObj = new DL_AddStuff();
	        return $dlObj->EditVehicleModel($data);
	    }
		function AddZone($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddZone($addstuff);
		}
		function UpdateZone($addstuff,$zoneid)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateZone($addstuff,$zoneid);
		}
		function AddRegion($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddRegion($addstuff);
		}
		function UpdateRegion($addstuff,$zoneid)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateRegion($addstuff,$zoneid);
		}

		function AddRoute($addstuff)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddRoute($addstuff);
		}
		function UpdateRoute($addstuff,$route_id)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->UpdateRoute($addstuff,$route_id);
		}

		function GetRoute()
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->GetRoute();
		}
		function DeleteRoute($route_id)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->DeleteRoute($route_id);
		}
		function GetCityRoutes($city)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->GetCityRoutes($city);
		}
		function GetRouteById($routeId)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->GetRouteById($routeId);
		}
		function AddRoutesRate($userId,$routeId,$routeFare,$comm)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->AddRoutesRate($userId,$routeId,$routeFare,$comm);
		}
		function DeleteRoutesRate($userId)
		{
			$dlObj = new DL_AddStuff();
			return $dlObj->DeleteRoutesRate($userId);
		}
	}


?>