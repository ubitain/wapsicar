<?php
	require_once(MODULE."/class/datalayer/route.php");
	class BL_Route
	{
		function AddCity($addstuff)
		{
			$dlObj = new DL_Route();
			return $dlObj->AddCity($addstuff);
		}
		function UpdateCity($addstuff,$cityid)
		{
			$dlObj = new DL_Route();
			return $dlObj->UpdateCity($addstuff,$cityid);
		}

		function GetCityById($city_id){

			$dlObj = new DL_Route();
			return $dlObj->GetCityById($city_id);
		}		

		function AddRoute($addstuff)
		{
			$dlObj = new DL_Route();
			return $dlObj->AddRoute($addstuff);
		}
		function UpdateRoute($addstuff,$route_id)
		{
			$dlObj = new DL_Route();
			return $dlObj->UpdateRoute($addstuff,$route_id);
		}

		function GetRoute()
		{
			$dlObj = new DL_Route();
			return $dlObj->GetRoute();
		}
		function DeleteRoute($route_id)
		{
			$dlObj = new DL_Route();
			return $dlObj->DeleteRoute($route_id);
		}
		
		// function GetRouteByIds($from_city,$to_city)
		// {
		// 	$dlObj = new DL_Route();
		// 	return $dlObj->GetRouteByIds($from_city,$to_city);
		// }


	}


?>