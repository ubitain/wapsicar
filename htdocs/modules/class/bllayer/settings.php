 <?php
	require_once(MODULE."/class/datalayer/settings.php");
	class BL_Settings
	{
		function DeleteCity($settingArray,$cityID)		
		{
			$deleteCity=new Dl_Settings();
			$deleteCity->DeleteCity($settingArray,$cityID);
		}
		function DeleteVehicle($settingArray,$vehicleID)		
		{
			$deleteCity=new Dl_Settings();
			$deleteCity->DeleteVehicle($settingArray,$vehicleID);
		}
		function DeleteVehicleModel($settingArray,$vehicleModelID)		
		{
			$deleteCity=new Dl_Settings();
			$deleteCity->DeleteVehicleModel($settingArray,$vehicleModelID);
		}
		function DeleteRegion($settingArray,$regionID)		
		{
			$deleteRegion =new Dl_Settings();
			$deleteRegion->DeleteRegion($settingArray,$regionID);
		}
		function DeleteZone($settingArray,$zoneID)	
		{
			$deleteZone =new Dl_Settings();
			$deleteZone->DeleteZone($settingArray,$zoneID);
		}

		function GetSiteSettings(){

			$dlSettings =new Dl_Settings();
			return $dlSettings->GetSiteSettings();
		}

		function UpdateSiteSettings($info){
			$dlSettings =new Dl_Settings();
			return $dlSettings->UpdateSiteSettings($info);
		}

	}