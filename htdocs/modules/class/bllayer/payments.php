 <?php
	require_once(MODULE."/class/datalayer/payments.php");
	class BL_Payments
	{
		function GetDocumentListing()
		{
			$dlObj = new DL_Payments();
			return $dlObj->GetDocumentListing();

		}
		function GetPaymentByTransaction($transaction_number)
		{
			$dlObj = new DL_Payments();
			return $dlObj->GetPaymentByTransaction($transaction_number);
		}

		function UpdateTransactionStatus($transaction_number,$status,$difference)
		{
			$dlObj = new DL_Payments();
			return $dlObj->UpdateTransactionStatus($transaction_number,$status,$difference);
			
		}
		function UpdateTransaction($transactionNumbers , $userId)
		{
			$dlObj = new DL_Payments();
			return $dlObj->UpdateTransaction($transactionNumbers , $userId);
			
		}
		// function InserAdmin ($amount,$status)
		// {
		// 	$dlObj = new DL_Payments();
		// 	return $dlObj->InserAdmin($amount,$status);
			
		// }

		
		function AddUserDocuments($info)
		{
			$dlObj = new DL_Payments();
			return $dlObj->AddUserDocuments($info);
			
		}
		function AddUserDocumentInfo($info)
		{
			$dlObj = new DL_Payments();
			return $dlObj->AddUserDocumentInfo($info);
			
		}
		function CheckTransaction($transactionId)
		{
			traceMessage("IN BL:");
			$dlObj = new DL_Payments();
			return $dlObj->CheckTransaction($transactionId);
			
		}
	}
	