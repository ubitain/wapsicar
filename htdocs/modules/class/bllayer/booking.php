<?php
	require_once(MODULE."/class/datalayer/booking.php");
	class BL_Booking
	{
		function GetBookingInfo($bookingId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetBookingInfo($bookingId);
		}
		function AddTempBooking($addArr)
		{
			$dlObj = new DL_Booking();
			return $dlObj->AddTempBooking($addArr);
		}
		function RejectBooking($driverId,$status,$bookingId,$reason,$comments)
		{
			$dlObj = new DL_Booking();
			return $dlObj->RejectBooking($driverId,$status,$bookingId,$reason,$comments);
		}
		function AcceptBooking($driverId,$status,$bookingId,$driverLat,$driverLong)
		{
			$dlObj = new DL_Booking();
			return $dlObj->AcceptBooking($driverId,$status,$bookingId,$driverLat,$driverLong);
		}
		function CancelBooking($driverId,$status,$bookingId,$reason,$comments)
		{
			$dlObj = new DL_Booking();
			return $dlObj->CancelBooking($driverId,$status,$bookingId,$reason,$comments);
		}
		function StartTrip($driverId,$bookingId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->StartTrip($driverId,$bookingId);
		}
		function EndTrip($driverId,$bookingId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->EndTrip($driverId,$bookingId);
		}
		function GetDriverDashboard($userId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetDriverDashboard($userId);
		}
		function GetCustomerDashboard($userId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetCustomerDashboard($userId);
		}
		function GetCustomerActiveBooking($userId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetCustomerActiveBooking($userId);
		}
		function GetRiderActiveBooking($userId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetRiderActiveBooking($userId);
		}
		function GetRiderPendingBooking($userId)
		{
			$dlObj = new DL_Booking();
			return $dlObj->GetRiderPendingBooking($userId);
		}
	}