 <?php
require_once(MODULE."/class/datalayer/reports.php");
class BL_Reports
{
	function GetThisMonthItnerary($date,$seId)
	{
		traceMessage("GetThisMonthItnerary".$seId.$date);
		$dlObj = new DL_Reports();
		return $dlObj->GetThisMonthItnerary($date,$seId);
	}
	function GetLocationBySaleExecutive($startDate,$endDate,$seId)
	{
		traceMessage("GetLocationBySaleExecutive".$startDate.$endDate.$seId);
		$dlObj = new DL_Reports();
		return $dlObj->GetLocationBySaleExecutive($startDate,$endDate,$seId);
	}
	function GetExpenseBySaleExecutive($month,$year,$seId)
	{
		traceMessage("GetExpenseBySaleExecutive".$startDate.$endDate.$seId);
		$dlObj = new DL_Reports();
		return $dlObj->GetExpenseBySaleExecutive($month,$year,$seId);
	}
	function GetDoctorVisitByDate($seId,$date)
	{
		traceMessage("GetDoctorVisitByDate($seId,$date)");
		$dlObj = new DL_Reports();
		return $dlObj->GetDoctorVisitByDate($seId,$date);
	}
	function GetDoctorCityByDoctorId($docId)
	{
		traceMessage("GetDoctorCityByDoctorId($docId)");
		$dlObj = new DL_Reports();
		return $dlObj->GetDoctorCityByDoctorId($docId);
	}
	function GetAllBricksOfFieldForce($team,$code)
	{	
		traceMessage("GetAllBricksOfFieldForce($team,$code)");
		$dlObj = new DL_Reports();
		return $dlObj->GetAllBricksOfFieldForce($team,$code);
	}
	function GetBrickSalesOfFieldForce($team,$sseCode,$month,$year)
	{	
		traceMessage("GetBrickSalesOfFieldForce($team,$sseCode,$month,$year)");
		$dlObj = new DL_Reports();
		return $dlObj->GetBrickSalesOfFieldForce($team,$sseCode,$month,$year);
	}
	function GetMonthlyExpenseByBrick($brick,$sseCode,$month,$year)
	{
		traceMessage("GetMonthlyExpenseByBrick($brick,$sseCode,$month,$year)");
		$dlObj = new DL_Reports();
		return $dlObj->GetMonthlyExpenseByBrick($brick,$sseCode,$month,$year);
	}
	function GetMileage($to,$from)
	{
		traceMessage("GetMileage($to,$from)");
		$dlObj = new DL_Reports();
		return $dlObj->GetMileage($to,$from);
	}
	function GetStationNameByBrick($brick)
	{
		traceMessage("GetStationNameByBrick($brick)");
		$dlObj = new DL_Reports();
		return $dlObj->GetStationNameByBrick($brick);
	}
	function GetSummaryExpense($sseCode,$month,$year)
	{
		traceMessage("GetSummaryExpense($sseCode,$month,$year)");
		$dlObj = new DL_Reports();
		return $dlObj->GetSummaryExpense($sseCode,$month,$year);
	}

	function GetSalesOrder($filterData = array(),$userType){

		traceMessage("GetSalesOrder($filterData)");
		$dlObj = new DL_Reports();
		return $dlObj->GetSalesOrder($filterData,$userType);

	}
	function GetRecoveryVisitsLogs($data){

		traceMessage("GetRecoveryVisitsLogs($data)");
		$dlObj = new DL_Reports();
		return $dlObj->GetRecoveryVisitsLogs($data);

	}
	
}