 <?php
require_once(MODULE."/class/datalayer/customer.php");
class BL_Customer
{
	function AddCustomer($arr)
	{
		traceMessage("Add Customer".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->AddCustomer($arr);
	}
	function UpdateCustomer($arr,$id)
	{
		traceMessage("Update Customer=$id".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->UpdateCustomer($arr,$id);
	}
	function GetAllCustomer($status=1)
	{
		traceMessage("GetAllCustomer".$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllCustomer($status);
	}
	function GetACustomer($id,$status=1)
	{
		traceMessage("GetACustomer".$id,$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetACustomer($id,$status);
	}
	function GetAllUnAssignCustomer()
	{
		traceMessage("GetUnAssignCustomer");
		$dlObj = new DL_Customer();
		return $dlObj->GetAllUnAssignCustomer();
	}
	function GetAllAssignCustomer()
	{
		traceMessage("GetAllAssignCustomer");
		$dlObj = new DL_Customer();
		return $dlObj->GetAllAssignCustomer();
	}
	function AssignCustomer($assignCustomerInfo)
	{
		if (empty($assignCustomerInfo)) {
			return false;
		}
		traceMessage("--------------------In BL AssignCustomer----------------------");
		$dlObj = new DL_Customer();
		return $dlObj->AssignCustomer($assignCustomerInfo);
	}
	function UnassignedCustomer($unassignCustomerInfo)
	{
		traceMessage("UnassignedCustomer bl".print_r_log($unassignCustomerInfo));
		$dlObj = new DL_Customer();
		return $dlObj->UnassignedCustomer($unassignCustomerInfo);
	}
	function GetAssignCustomerByUserId($userId)
	{
		traceMessage("GetAssignCustomerByUserId bl".$userId);
		$dlObj = new DL_Customer();
		return $dlObj->GetAssignCustomerByUserId($userId);
	}
	function GetAllUserAssignedByCustomerId($customerId)
	{
		traceMessage("GetAllUserAssignedByCustomerId bl".$customerId);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllUserAssignedByCustomerId($customerId);
	}
	function AddNewArea($areaInfo)
	{
		traceMessage("In BL AddNewArea");
		$dlObj = new DL_Customer();
		return $dlObj->AddNewArea($areaInfo);
	}
	function AjaxCustomerTable($aColumns,$sIndexColumn,$sTable,$request="")
	{
		traceMessage("AjaxCustomerTable".print_r_log($request));
		$dlObj = new DL_Customer();
		return $dlObj->AjaxCustomerTable($aColumns,$sIndexColumn,$sTable,$request);
	}
	function InboxCustomerList($regionId)
	{
		traceMessage("InboxCustomerList");
		$dlObj = new DL_Customer();
		return $dlObj->InboxCustomerList($regionId);
	}
	function InboxCustomerThread($customerId)
	{
		traceMessage("InboxCustomerThread");
		$dlObj = new DL_Customer();
		return $dlObj->InboxCustomerThread($customerId);
	}
	function InboxCustomerThreadImages($inboxId)
	{
		traceMessage("InboxCustomerThreadImages");
		$dlObj = new DL_Customer();
		return $dlObj->InboxCustomerThreadImages($inboxId);
	}
	function AddThreadMessage($arr)
	{
		traceMessage("Add Thread Message".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->AddThreadMessage($arr);
	}
	function AddThreadFile($arr)
	{
		traceMessage("Add Thread File".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->AddThreadFile($arr);
	}
	function GetAllVisitByIds($data)
	{
		traceMessage("GetAllVisitByIds".print_r_log($data));
		$dlObj = new DL_Customer();
		return $dlObj->GetAllVisitByIds($data);
	}
	// Reporting
	function GetTotalOutStanding($data)
	{
		traceMessage("GetTotalOutStanding".print_r_log($data));
		$dlObj = new DL_Customer();
		return $dlObj->GetTotalOutStanding($data);
	}
	function GetTotalOverDue($data)
	{
		traceMessage("GetTotalOverDue".print_r_log($data));
		$dlObj = new DL_Customer();
		return $dlObj->GetTotalOverDue($data);
	}
	function GetTotalCollectedAmount($data)
	{
		traceMessage("GetTotalCollectedAmount".print_r_log($data));
		$dlObj = new DL_Customer();
		return $dlObj->GetTotalCollectedAmount($data);
	}
	function GetTotalInventory($data)
	{
		traceMessage("GetTotalInventory".print_r_log($data));
		$dlObj = new DL_Customer();
		return $dlObj->GetTotalInventory($data);
	}
	// Reporting End Here
	function AddCustomerContact($arr)
	{
		traceMessage("Add Customer Contact".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->AddCustomerContact($arr);
	}
	function UpdateCustomerContact($arr,$id)
	{
		traceMessage("Update Customer Contact =$id".print_r_log($arr));
		$dlObj = new DL_Customer();
		return $dlObj->UpdateCustomerContact($arr,$id);
	}
	function GetACustomerContact($id,$status=1)
	{
		traceMessage("GetACustomerContact".$id,$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetACustomerContact($id,$status);
	}
	function GetAllCustomerContact($id,$status=1)
	{
		traceMessage("GetAllCustomerContact".$id,$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllCustomerContact($id,$status);
	}

	function GetAllComplaints($data,$status=1)
	{
		traceMessage("GetAllComlaints".$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllComplaints($data,$status);
	}

	function AddDepartment($deptInfo)
	{
		traceMessage("dept data".print_r_log($deptInfo));
		$dlObj = new DL_Customer();
		return $dlObj->AddDepartment($deptInfo);
	}
    function UpdateDepartment($deptInfo,$deptId)
	{
		traceMessage("dept data".print_r_log($deptInfo));
		$dlObj = new DL_Customer();
		return $dlObj->UpdateDepartment($deptInfo,$deptId);
	}
    function GetAllDepartment($status=1)
	{
		traceMessage("dept status ".$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllDepartment($status);
	}
    function GetADepartment($id,$status=1)
	{
		traceMessage("deptId".$id,$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetADepartment($id,$status);
	}
	/* Reason CRUD*/
	function AddReason($reasonInfo)
	{
		traceMessage("dept data".print_r_log($reasonInfo));
		$dlObj = new DL_Customer();
		return $dlObj->AddReason($reasonInfo);
	}
    function UpdateReason($reasonInfo,$reasonId)
	{
		traceMessage("dept data".print_r_log($reasonInfo));
		$dlObj = new DL_Customer();
		return $dlObj->UpdateReason($reasonInfo,$reasonId);
	}
    function GetAllReason($status=1)
	{
		traceMessage("dept status ".$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAllReason($status);
	}
    function GetAReason($id,$status=1)
	{
		traceMessage("deptId".$id,$status);
		$dlObj = new DL_Customer();
		return $dlObj->GetAReason($id,$status);
	}
}
?>
