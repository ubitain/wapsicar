 <?php
require_once(MODULE."/class/datalayer/calltarget.php");
class BL_CallTarget
{
 
	function AddCallTarget($arr)
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->AddCallTarget($arr);
	}
	function GetAllCallTarget()
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->GetAllCallTarget();
	}
	function GetOneCallTarget($recordId)
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->GetOneCallTarget($recordId);
	}
	function EditCallTarget($recordId,$callTargetInfo)
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->EditCallTarget($recordId,$callTargetInfo);
	}
	function DeleteCallTarget($recordId)
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->DeleteCallTarget($recordId);
	}
	function GetWorkingDays($month)
	{
		$dlObj = new DL_CallTarget();
		return $dlObj->GetWorkingDays($month);
	}
}
?>
