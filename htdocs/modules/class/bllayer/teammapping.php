  <?php
	require_once(MODULE."/class/datalayer/teammapping.php");
	class BL_TeamMapping
	{
		/* Team mapping function*/
		function MappingTeam($arr)
		{
			traceMessage("in BL MappingTeam".print_r_log($arr));
			$dlObj = new DL_TeamMapping();
			return $dlObj->MappingTeam($arr);
		}
		// team view of mapping functions
		function ViewTeamMapping($parentId,$userType)
		{
			traceMessage("in BL ViewTeamMapping".$parentId,$userType);
			$dlObj = new DL_TeamMapping();
			return $dlObj->ViewTeamMapping($parentId,$userType);
		}
	}
?>
