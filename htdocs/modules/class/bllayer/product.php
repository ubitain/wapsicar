 <?php
require_once(MODULE."/class/datalayer/product.php");
class BL_Product
{
    function AddProduct($productInfo)
	{
		traceMessage("productdata".print_r_log($productInfo));
		$dlObj = new DL_Product();
		return $dlObj->AddProduct($productInfo);
	}
    function UpdateProduct($productInfo,$productId)
	{
		traceMessage("productdata".print_r_log($productInfo));
		$dlObj = new DL_Product();
		return $dlObj->UpdateProduct($productInfo,$productId);
	}
    function GetAllProduct($status=1)
	{
		traceMessage("productId11".$status);
		$dlObj = new DL_Product();
		return $dlObj->GetAllProduct($status);
	}
    function GetAProduct($id,$status=1)
	{
		traceMessage("productId".$id,$status);
		$dlObj = new DL_Product();
		return $dlObj->GetAProduct($id,$status);
	}
	function UpdateCompetitorProductData($cProductInfo,$productId,$cProductId)
	{
		traceMessage("cproductdata".print_r_log($cProductInfo));
		$dlObj = new DL_Product();
		return $dlObj->UpdateCompetitorProductData($cProductInfo,$productId,$cProductId);
	}

	function GetCompetitorProductsByCompetitorProductId($cProductId)
	{
		traceMessage("cproductId11".$cProductId);
		$dlObj = new DL_Product();
		return $dlObj->GetCompetitorProductsByCompetitorProductId($cProductId);
	}

	function checkProductCode($data){

		traceMessage("check product code bl layer product".$data);
		$dlObj = new DL_Product();
		$result = $dlObj->checkProductCode($data);

		if($result->count > 0){

			return 'false';

		}
		return 'true';

	}

	function checkAtProductCode($data){

		traceMessage("check product at code bl layer product".$data);
		$dlObj = new DL_Product();
		$result = $dlObj->checkAtProductCode($data);

		if($result->count > 0){

			return 'false';

		}

		return 'true';

	}
    function GreyProductReport($data)
	{
		traceMessage("GreyProductReport BL".print_r_log($data));
		$dlObj = new DL_Product();
		return $dlObj->GreyProductReport($data);
	}

}
?>
