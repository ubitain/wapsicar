 <?php
	require_once(MODULE."/class/datalayer/documents.php");
	class BL_Documents
	{
		function GetDocumentListing()
		{
			$dlObj = new DL_Documents();
			return $dlObj->GetDocumentListing();

		}
		function GetUserDocuments($userid)
		{
			$dlObj = new DL_Documents();
			return $dlObj->GetUserDocuments($userid);
			
		}

		function AddUserDocuments($info)
		{
			$dlObj = new DL_Documents();
			return $dlObj->AddUserDocuments($info);
			
		}
		function AddUserDocumentInfo($info)
		{
			$dlObj = new DL_Documents();
			return $dlObj->AddUserDocumentInfo($info);
			
		}
		function GetUserDocumentsByType($userId,$documentId)
		{
			$dlObj = new DL_Documents();
			return $dlObj->GetUserDocumentsByType($userId,$documentId);
			
		}
	}