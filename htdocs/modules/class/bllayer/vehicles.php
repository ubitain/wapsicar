 <?php
	require_once(MODULE."/class/datalayer/vehicles.php");
	class BL_Vehicle
	{
		function getVehicleMake()
		{
			$dlObj = new DL_Vehicle();
			return $dlObj->getVehicleMake();

		}
		function getVehicleModel($make_id)
		{
			$dlObj = new DL_Vehicle();
			return $dlObj->getVehicleModel($make_id);
			
		}

		function AddVehicleInformation($info)
		{
			$dlObj = new DL_Vehicle();
			return $dlObj->AddVehicleInformation($info);
			
		}
		function GetVehicleInfo($userId)
		{
			$dlObj = new DL_Vehicle();
			return $dlObj->GetVehicleInfo($userId);
			
		}

		function GetCarTypes()
		{
			$dlObj = new DL_Vehicle();
			return $dlObj->GetCarTypes();
			
		}

		
	}