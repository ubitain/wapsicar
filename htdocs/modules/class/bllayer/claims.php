 <?php
	require_once(MODULE."/class/datalayer/claims.php");
	class BL_AddPurchaseFromAccountOff
	{
		function AddPurchaseFromAccountOff($addstuff,$claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->AddPurchaseFromAccountOff($addstuff,$claimID);
		}
		function AddPurchaseFromhis($add)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->AddPurchaseFromhis($add);
		}
		function AddAccountOffhis($add)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->AddAccountOffhis($add);
		}
		function AddPurchaseFromhislog($add)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->AddPurchaseFromhislog($add);
		}
		function AddAccountOffhislog($add)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->AddAccountOffhislog($add);
		}
		function GetMaxTransLog($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransLog($claimID);
		}
		function GetMaxTrans($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTrans($claimID);
		}
		function GetMaxTransUser($claimTransctionsId)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransUser($claimTransctionsId);
		}
		function GetMaxTransAccount($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransAccount($claimID);
		}
		function GetMaxTransAccountLog($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransAccountLog($claimID);
		}
		function GetMaxTransAccountStatus($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransAccountStatus($claimID);
		}
		function GetMaxTransAccountClaim($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransAccountClaim($claimID);
		}
		function GetMaxTransPurchaseClaim($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->GetMaxTransPurchaseClaim($claimID);
		}
		function UpdatePurchaseFrom($data1,$claimTransctionsId)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->UpdatePurchaseFrom($data1,$claimTransctionsId);
		}
		function UpdatePurchaseFromLog($data1,$claimTransctionsLogId)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->UpdatePurchaseFromLog($data1,$claimTransctionsLogId);
		}
		function DeletePurchaseFrom($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->DeletePurchaseFrom($claimID);
		}
		function UpdateAccountOffFromLog($dataChangeStatus,$claimTransctionsAccountIdLog)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->UpdateAccountOffFromLog($dataChangeStatus,$claimTransctionsAccountIdLog);
		}
		function DeleteAccountOff($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->DeleteAccountOff($claimID);
		}
		function UpdatePurchaseFromClaim($updatePurchaseFromInClaim,$claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->UpdatePurchaseFromClaim($updatePurchaseFromInClaim,$claimID);
		}
		function UpdateStatusFromClaim($claimID)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->UpdateStatusFromClaim($claimID);
		}
		function ProccedToLeopard($claimNo)
		{
			$dlObj = new DL_AddPurchaseFromAccountOff();
			return $dlObj->ProccedToLeopard($claimNo);
		}
		
	}


?>