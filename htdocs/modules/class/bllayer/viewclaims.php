 <?php
require_once(MODULE."/class/datalayer/viewclaims.php");

class BL_ViewClaims
{
	function ViewClaims()
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewClaims();
    }function ViewClaimsTotal()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewClaimsTotal();
    }
    function ViewClaimsDates($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewClaimsDates($searchFrom,$searchTo);
    }
    
    function ViewClaimsNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewClaimsNo($claimNoSearch);
    }
    function ClaimsToday()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ClaimsToday();
    }
    function claimsMonth()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->claimsMonth();
    }
    function ClaimsYear()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ClaimsYear();
    }
    function ViewTopProductsInClaim()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewTopProductsInClaim();
    }
    function FetchProductName($itemID)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchProductName($itemID);
    }
    function ViewPendingClaimsDates($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewPendingClaimsDates($searchFrom,$searchTo);
    }
    function ViewSendTolLeopardClaimsDates($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewSendTolLeopardClaimsDates($searchFrom,$searchTo);
    }
    function ViewPendingClaims()
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewPendingClaims();
    }
    function ViewSendTolLeopardClaims()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewSendTolLeopardClaims();
    }
    function ViewPendingClaimNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewPendingClaimNo($claimNoSearch);
    }
    function ViewSendTolLeopardClaimNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewSendTolLeopardClaimNo($claimNoSearch);
    }
    function ViewInProcessClaimsDate($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessClaimsDate($searchFrom,$searchTo);
    }
    function ViewInProcessClaims()
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewInProcessClaims();
    }
    function ViewInProcessClaimNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessClaimNo($claimNoSearch);
    }
    function ViewInProcessResolveClaims()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessResolveClaims();
    }
    function ViewInProcessResolveClaimsDatee($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessResolveClaimsDatee($searchFrom,$searchTo);
    }
    function ViewInProcessResolveClaimNoo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessResolveClaimNoo($claimNoSearch);
    }
    function ViewInProcessResolveClaimsDate($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessResolveClaimsDate($searchFrom,$searchTo);
    }
    function ViewInProcessResolveClaimNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInProcessResolveClaimNo($claimNoSearch);
    }
    function ViewIncompleteClaims()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewIncompleteClaims();
    }
    function ViewInCompleteClaimsDates($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInCompleteClaimsDates($searchFrom,$searchTo);
    }
    function ViewInCompleteClaimsNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInCompleteClaimsNo($claimNoSearch);
    }
    function ViewInCompleteClaimsDatess($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInCompleteClaimsDatess($searchFrom,$searchTo);
    }
    
    function ViewCompletedClaims()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewCompletedClaims();
    }
    function ViewCompletedClaimsDate($searchFrom,$searchTo)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewCompletedClaimsDate($searchFrom,$searchTo);
    }
    function ViewCompletedClaimsClaimNo($claimNoSearch)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewCompletedClaimsClaimNo($claimNoSearch);
    }
    function ViewInCompletedClaims()
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewInCompletedClaims();
    }
    function ViewClaimsDetail($claimId)
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewClaimsDetail($claimId);
    }
    function ViewClaimsProductImages($claimId)
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewClaimsProductImages($claimId);
    }
    function ViewClaimsProductDetail($claimId)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewClaimsProductDetail($claimId);
    }
    function ViewClaimsPurchaseHierarchyDetail($claimId)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->ViewClaimsPurchaseHierarchyDetail($claimId);
    }
    function FetchPurchaseName($userId)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchPurchaseName($userId);
    }
    function FetchDealerName($dealerID)
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->FetchDealerName($dealerID);
    }
    function FetchUserNameTrans($claimID)
    {

        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchUserNameTrans($claimID);
    }
    function FetchUserName($claimID)
    {

        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchUserName($claimID);
    }
    function FetchItemName($claimItemId)
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->FetchItemName($claimItemId);
    }
    function FetchVoucherDetail($claimID)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchVoucherDetail($claimID);
    }
    function ViewClaimsGrandTotal($claimId)
    {
    	$viewClaims= new DL_ViewClaims();
    	return $viewClaims->ViewClaimsGrandTotal($claimId);
    }
    function FetchClaimTransactions($claimID)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchClaimTransactions($claimID);
    }
    function FetchClaimTransactionsPurchase($claimID)
    {
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchClaimTransactionsPurchase($claimID);
    }
    function FetchClaimClaimerAccount($claimNo)
    {
        traceMessage(print_r_log("BL LAYER!!"));
        $viewClaims= new DL_ViewClaims();
        return $viewClaims->FetchClaimClaimerAccount($claimNo);
    }
    
    // function FetchClaimTransactions($claimID)
    // {
    //     $viewClaims= new DL_ViewClaims();
    //     return $viewClaims->FetchClaimTransactions($claimID);
    // }

    
}

?>