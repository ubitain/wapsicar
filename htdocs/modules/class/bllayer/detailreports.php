 <?php
	require_once(MODULE."/class/datalayer/detailreports.php");
	class BL_Reports
	{
		function GetProductDetail()
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetProductDetail();
		}
		function FetchClaimItemCount($itemID)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemCount($itemID);
		}
		function FetchClaimItemCountDate($itemID,$searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemCountDate($itemID,$searchFrom,$searchTo);
		}
		function FetchClaimItemAccepted($itemID)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemAccepted($itemID);
		}
		function FetchClaimItemAcceptedDate($itemID,$searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemAcceptedDate($itemID,$searchFrom,$searchTo);
		}
		function FetchClaimItemRejected($itemID)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemRejected($itemID);
		}
		function FetchClaimItemRejectedDate($itemID,$searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemRejectedDate($itemID,$searchFrom,$searchTo);
		}
		function FetchClaimItemOthers($itemID)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemOthers($itemID);
		}
		function FetchClaimItemOthersDate($itemID,$searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->FetchClaimItemOthersDate($itemID,$searchFrom,$searchTo);
		}
		//+++++++++
		function GetRegionDetail()
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetRegionDetail();
		}
		function GetRegionName()
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetRegionName();
		}
		function GetRegionAccRejOth($claimID)
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetRegionAccRejOth($claimID);
		}
		function GetRegionAccRejOthDate($claimID,$searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetRegionAccRejOthDate($claimID,$searchFrom,$searchTo);
		}
		function GetUserName()
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetUserName();
		}
		function GetUserDetail()
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetUserDetail();
		}
		function GetUserDetailDate($searchFrom,$searchTo)
		{
			$dlObj = new DL_Reports();
			return $dlObj->GetUserDetailDate($searchFrom,$searchTo);
		}
	}


?>