
<style>
* {
  box-sizing: border-box;
}

.image1 {

  transition: transform .2s;
  width: 100px;
  height: 100px;
  margin: 0 auto;
}


.image1:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}

</style>


<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Claim Detail
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>


            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5>Claimer Information</h5>
                                            </div>
                                </div>
                            <div style="border:  1px solid lightgrey; padding-top: 10px;" >
                                <div class="form-row" style="padding-left: 3px; padding-right: 3px; ">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Voucher No</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="voc-05-az" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Voucher Type</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Claim" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Date</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="10-Jul-2019" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Pending
" >
                                            </div>
                                </div>
                                <div class="form-row" style="padding-left: 3px; padding-right: 3px; ">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Vendor Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Brielle Williamson" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Vendor Type</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Mechanic" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Phone Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="0329484948" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Address</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Gulshan" >
                                            </div>
                                </div>
                                <div class="form-row" style="padding-left: 3px; padding-right: 3px; ">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchased From</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Humza" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">On Account Of</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="3153463545434" >
                                            </div>
                                </div>
                            </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <h5>Claim Product Information</h5>
                                    </div>
                                </div>
                                <div style="border:  1px solid lightgrey;">
                                 <?php 
                                for ($i=0; $i < 2; $i++) { 
                                $count=5003+$i;
                                if ($i==0) {
                                    $productName="Nuts";
                                    $Quantity=5;
                                    $status="Broken";
                                    $desc="None";
                                }
                                elseif ($i==1) {
                                    $productName="Clutch";
                                    $Quantity=15;
                                    $status="Partially Broken";
                                    $desc="Bad quality";
                                }
                                ?>
                                <div style=" padding-top: 15px; ">
                                <div class="form-row" style="padding-left: 3px; padding-right: 3px; ">
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">Product ID</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $count ?>" >
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">Product Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $productName ?>" >
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">Quantity</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $Quantity ?>" >
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $status ?>" >
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label class="form-label" for="validationTooltip01">Description</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $desc ?>" >
                                            </div>
                                </div>
                                <div class="form-row" style="padding-left: 3px; padding-right: 3px; ">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Accept Quantity</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""   value="4" disabled="true">
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Reject Quantity</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="0"  disabled="true">
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Other Vendor Quantity</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""   value="1"  disabled="true">
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Remarks</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""   value="Our Fault"  disabled="true">
                                            </div>
                                </div>
                                </div>
                                <div style="height: 10px;"></div>
                                <?php
                                }
                                ?>
                                </div>

                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <h5>Assigned Voucher</h5>
                        </div>
                    </div>

                    <div  style="border:  1px solid lightgrey; padding-top: 15px; ">
                    <div class="form-row" id="delete0" style="padding-left: 3px; padding-right: 3px;">
                    <div class="col-md-5 mb-3">
                        <label class="form-label" >Voucher ID</label>
                        <input type="text" class="form-control" id="Quality0" placeholder="" value="44-2asd-43" disabled="true">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label">Description</label>
                        <input type="text" class="form-control" id="Description0" placeholder="" value="50000rs Only" disabled="true">
                    </div>
                    </div>
                    </div>


                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <h5>Images</h5>
                        </div>
                    </div>

                    <div class="form-row" align="center"> 
                        <div class="col-md-3  mb-3" >
                            <img src="\img\parts\nuts.jpg" class="image1">
                        </div>
                        <div class="col-md-3  mb-3">
                            <img src="\img\parts\nuts2.jpg"  class="image1">
                        </div>
                        <div class="col-md-3  mb-3" >
                            <img src="\img\parts\clutch.jpg" class="image1">
                        </div>
                        <div class="col-md-3  mb-3">
                            <img src="\img\parts\clutch2.jpg"  class="image1">
                        </div>
                    </div>  

                    

                    <div style="margin-top: 20px;"></div>
                    <div class="form-row">
                        <div class="col-md-10  mb-3">
                        </div>
                        <div class="col-md-2  mb-3">
                            
                            <button type="button" onClick="LoadAjaxScreen('completedclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>