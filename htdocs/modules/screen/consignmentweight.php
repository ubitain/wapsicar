
<style>
* {
  box-sizing: border-box;
}

.image1 {

  transition: transform .2s;
  width: 100px;
  height: 100px;
  margin: 0 auto;
}

.image1:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}

</style>

<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    $claimId=$_GET['id'];
    $page=$_GET['page'];

   // $userId = $mysession->getValue("userid");
    $viewClaims=new BL_ViewClaims();
    $fetchData=$viewClaims->ViewClaimsDetail($claimId);
    $count =  $fetchData->count;

    for ($i=0; $i <1; $i++) { 
        
          //for
            $claimID = $fetchData->rows[$i]['claim_id'];
            $claimNo = $fetchData->rows[$i]['claim_no'];
            $dealerID = $fetchData->rows[$i]['user_id'];
            
            $fetchData1=$viewClaims->FetchDealerName($dealerID);
            $claimName = $fetchData1->rows[0]['full_name'];
            $claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
            $claimDealerCatergory=strtolower($claimDealerCatergory);
            $claimMobNumber = $fetchData1->rows[0]['mobile_no'];
            $claimDealerAddress = $fetchData1->rows[0]['address'];

            $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
            $accountOf = $fetchData->rows[$i]['account_of'];
            $lcsNo = $fetchData->rows[$i]['lcs_no'];
            $cartonWeight = $fetchData->rows[$i]['carton_weight'];
            $remarks = $fetchData->rows[$i]['remarks'];
            $claimStatus = $fetchData->rows[$i]['claim_status'];
            $Date = $fetchData->rows[$i]['created_at'];  
            // ye sirf print krwnay kay liye tha jab last time form banaya tha ar reject hogya tha, sir said saari values na dekhao!
    }

    $fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
    $count2 =  $fetchData2->count;


?>

<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Assign Consignment Number/ Weight
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

<form class="form-horizontal" name='addconsignweight' id='addconsignweight' method='post'>
<input type='hidden' name='ACTION'  value='ADDCONSIGNWEIGHT'>
<input type='hidden' name='claimID' value="<?php echo $claimID ?>" >
            <div class="row" style="margin-top: -30px;"> 
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5></h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claim Number</label>
                                                <input type="text" class="form-control" id="" name="" placeholder="" disabled="true"  value="<?php echo $claimNo ?>" >
                                                
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Dealer Name</label>
                                                <input type="text" class="form-control" id="claimName" name="claimName" placeholder="" disabled="true"  value="<?php echo ucfirst($claimName) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Carton Weight (Kg)</label>
                                                <input type="text" class="form-control" name="cartonWeight" id="cartonWeight" placeholder=""   value="<?php echo $cartonWeight ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            
                                            <div class="col-md-12 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Consignment Number</label>
                                                <input type="text" class="form-control" id="lcsNo" name="lcsNo" placeholder=""   value="<?php echo $lcsNo ?>" >
                                            </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-8 mb-3">
                                    </div>
                                    <div class="col-md-4 mb-3" >
                                        <button type="button" style="margin-left: 25px;" onclick="AddConsignWeight(<?php echo $page ?>)" data-dismiss="modal"   class="btn btn-med btn-warning">
                                            <span class="fal fa fa-arrow-right mr-1"></span>
                                            Save
                                        </button>
                                    </div>
                                </div>
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>
</main>

<!-- <nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav> -->
<script type="text/javascript">
    
$("#cartonWeight").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#cartonWeight").val().length >10 ) {
            return false;
       }
});

$("#lcsNo").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#lcsNo").val().length >25 ) {
            return false;
       }
});


function AddConsignWeight(page) {
    var x = document.getElementById("cartonWeight").value;
    var y = document.getElementById("lcsNo").value;
    var z = page;
        if (x!="" && y!="") {
            if(confirm("Are you sure you want to save?"))
            {
                if (z==3) {
                    var callfunc={onSuccess:NextPage};
                }
                else if (z==5) {
                    var callfunc={onSuccess:NextPage2};
                }
                else
                {
                    var callfunc={onSuccess:NextPage1};
                }
                    
              
            form= document.getElementById('addconsignweight');   
            PostAjaxScreen("addconsignweight",form,callfunc);
            }
        }
        else
        {
            alert('Fill the fields!');
            OpenModalBox('consignmentweight&id=<?php echo $claimID ?>','screen','');
        }
        
}

function NextPage(){
      LoadAjaxScreen("claimdetails&id=<?php echo $claimID ?>&page=3");
}
function NextPage1(){
      LoadAjaxScreen("pendingclaims");
}
function NextPage2(){
      LoadAjaxScreen("claimdetails&id=<?php echo $claimID ?>&page=5");
}
</script>


<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>