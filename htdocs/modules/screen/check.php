<?php
    require_once(MODULE."/class/bllayer/viewuser.php");
    
   // $userId = $mysession->getValue("userid");
    $viewUser=new BL_ViewUser();
    $fetchData=$viewUser->ViewUser();
    $count =  $fetchData->count;


?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
View Users
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 50px;">
 <!-- datatable start -->
                <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>CNIC</th>
                            <th>User Type</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Joining Date</th>
                            <th>Password</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?

                            for ($i=0; $i <$count; $i++) { 
                                //for
                                $userName = $fetchData->rows[$i]['full_name'];
                                $gender = $fetchData->rows[$i]['reg_code'];
                                $cnic = $fetchData->rows[$i]['sap_code'];
                                $user_type_name = $fetchData->rows[$i]['mobile_no'];
                                $password = $fetchData->rows[$i]['email'];
                                $phone_number = $fetchData->rows[$i]['address'];
                                $address = $fetchData->rows[$i]['market'];
                                $city_name = $fetchData->rows[$i]['area'];
                                $country_name = $fetchData->rows[$i]['town'];
                                $joiningdate = $fetchData->rows[$i]['joiningdate'];                            ?>

                                <tr>
                                <td ><?=$userName?></td>
                                <td ><?=$gender?></td>
                                <td ><?=$cnic?></td>
                                <td ><?=$user_type_name?></td>
                                
                                <td ><?=$phone_number?></td>
                                <td ><?=$address?></td>
                                <td ><?=$city_name?></td>
                                <td ><?=$country_name?></td>
                                <td ><?=$joiningdate?></td>
                                <td ><?=$password?></td>
                                <td align="center"><button type="button" class="btn btn-sm btn-warning" onClick="LoadAjaxScreen()">
                                <span class="fal  fa-edit mr-1"></span>
                                </button></td>
                               <!--  <td ><a class="btn btn-sm btn-primary" onClick="deleted('<?php echo $fetchData->rows[$i]['user_id'] ?>')">Delete</a></td> -->
                                </tr>
                            <?
                                }
                            ?>
                       
                    </tbody>
                </table>

</div>
</div>




</div>
</div>
</div>


<div class="modal fade" id="default-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fal fa-times"></i></span>
            </button>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</div>
</div> 
</main>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>