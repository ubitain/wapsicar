<?php 
require_once(MODULE."/class/bllayer/user.php");
$user = new BL_User();

$delivery = $user->GetDeliveryDetails();
$count =  $delivery->count;

$contactPersonOne = $delivery->rows[0]['contact_person_one'];
$contactPersonOneNo = $delivery->rows[0]['contact_person_one_phone'];
$contactPersonTwo = $delivery->rows[0]['contact_person_two'];
$contactPersonTwoNo = $delivery->rows[0]['contact_person_two_phone'];
$address = $delivery->rows[0]['address'];
$leopard_email = $delivery->rows[0]['leopard_email'];
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Settings
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='updatesetting' id='updatesetting' method='post'>
<input type='hidden' name='ACTION' value='UPDATEDELIVERY'>

<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <h5>Delivery Point</h5>
                    </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label class="form-label" for="validationTooltip01">Contact Person</label>
                <input type="text" class="form-control" id="contact1" name="contact1" placeholder=""    value="<?php echo $contactPersonOne ?>" >
            </div>
            <div class="col-md-6 mb-3">
                <label class="form-label" for="validationTooltip01">Contact No</label>
                <input type="text" class="form-control" id="contactNo1" name="contactNo1" placeholder=""    value="<?php echo $contactPersonOneNo ?>" >
            </div>               
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label class="form-label" for="validationTooltip01">Substitute Contact Person</label>
                <input type="text" class="form-control" id="contact2" name="contact2" placeholder=""    value="<?php echo $contactPersonTwo ?>" >
            </div>
            <div class="col-md-6 mb-3">
                <label class="form-label" for="validationTooltip01">Substitute Contact No</label>
                <input type="text" class="form-control" id="contactNo2" name="contactNo2" placeholder=""    value="<?php echo $contactPersonTwoNo ?>" >
            </div>               
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label class="form-label" for="validationTooltip01">Address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder=""    value="<?php echo $address ?>" >
            </div>
            <div class="col-md-6 mb-3" >
                <label class="form-label" for="validationTooltip01">Leopard Email</label>
                <input type="text" class="form-control" id="leopardemail" name="leopardemail" placeholder=""    value="<?php echo $leopard_email ?>" >
            </div>               
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                
            </div>
            <div class="col-md-6 mb-3" style="margin-top: 25px;" align="right">
                <button type="button" onclick='UpdateDeliveryDetails()' class="btn btn-med btn-warning">
                     <span class="fal fa fa-arrow-right mr-1"></span>
                    Update
                </button>
                <button type="button" onClick="LoadAjaxScreen('dashboard_superuser')" class="btn btn-med btn-dark">
                    <span class="fal fa fa-arrow-left mr-1"></span>
                    Back
                </button>
            </div>               
        </div>
      

<div style="margin-top: 20px;"></div>

</div>
</div>
</div>
</div>
</div>
</form>
</main>


<script type="text/javascript">


function UpdateDeliveryDetails() {
    if(confirm("Are you sure you want to save?"))
    {
            var callfunc={onSuccess:NextPage1};
            form= document.getElementById('updatesetting');   
            PostAjaxScreen("updatesetting",form,callfunc);
    } 
}

function NextPage1(){
      LoadAjaxScreen("setting_superuser");
}

$("#contact1").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#contact1").val().length >35 ) {
            return false;
       }



});

$("#contactNo1").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#contactNo1").val().length >12 ) {
            return false;
       }



});

$("#contact2").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#contact2").val().length >35 ) {
            return false;
       }



});

$("#contactNo2").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#contactNo2").val().length >12 ) {
            return false;
       }



});


</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>