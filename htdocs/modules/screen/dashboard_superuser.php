<?php
 
    require_once(MODULE."/class/bllayer/viewclaims.php");
    
    $viewClaims=new BL_ViewClaims();
    $blCountData=$viewClaims->ViewClaims();
    $count =  $blCountData->count;

    $claimsToday=$viewClaims->ClaimsToday();
    $countToday =  $claimsToday->count;

    $claimsMonth=$viewClaims->ClaimsMonth();
    $countMonth =  $claimsMonth->count;


    $claimsYear=$viewClaims->ClaimsYear();
    $countYear =  $claimsYear->count;

    $blCountData=$viewClaims->ViewPendingClaims();
    $countPending =  $blCountData->count;

    $blCountData=$viewClaims->ViewInProcessClaims();
    $countInProcess =  $blCountData->count;

    $blCountData=$viewClaims->ViewCompletedClaims();
    $countCompleted =  $blCountData->count;

    $blCountData=$viewClaims->ViewInCompletedClaims();
    $countInComplete =  $blCountData->count;

    $blCountData=$viewClaims->ViewInProcessResolveClaims();
    $countCompletedResolve =  $blCountData->count;

    $blCountData=$viewClaims->ViewTopProductsInClaim();
    $viewTopProductsInClaim =  $blCountData->count;

   

    
?>


<main id="js-page-content" role="main" class="page-content">

                        <ol class="breadcrumb page-breadcrumb">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-chart-area'></i> Analytics <span class='fw-300'>Dashboard</span>
                            </h1>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
                        </ol>
                        <!--   <div class="subheader">
                     </div> -->

                     <div class="row" style="margin-top: -25px;">
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $count; ?>
                                            <small class="m-0 l-h-n">Total Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countToday; ?>
                                            <small class="m-0 l-h-n">Reported Today</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countMonth; ?>
                                            <small class="m-0 l-h-n">Reported This Month</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countYear; ?>
                                            <small class="m-0 l-h-n">Reported This Year</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                    </div>


<?php

    $viewClaims=new BL_ViewClaims();
    $blCountData=$viewClaims->ViewCompletedClaims();
    $count =  $blCountData->count;

?>

    <div style="height: 750px;"></div>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>


<script>
$(document).ready(function()
{
$('#tableabc').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>