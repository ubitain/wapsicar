<?php
 require_once MODULE."/class/bllayer/user.php";
 require_once MODULE."/class/bllayer/viewuser.php"; 
 $blUser = new BL_User();
 $blViewUser = new BL_ViewUser();
    $fetchData = $blUser->GetBooking();
    // print_r_pre($fetchData);
    
?>

<link rel="stylesheet" media="screen, print" href="css/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.css">
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
View Booking
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addcityform' id='addcityform' method='post'>
<div id="panel-10" class="panel" style="margin-top: -30px;">
        

        <div>

             
            
            <div class="form-row" style="margin-top: 30px;">
                    <div class="col-md-1 mb-3">
                      
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label">Booking Report</label>
                          <div class="col-md-12">
                          <input type="text" class="form-control" id="datepicker-1" placeholder="Select date" value="01/01/2021 - 01/15/2021">
                          </div>
                          <div class="col-md-4">
                          <button class="btn btn-primary"  style="background: #ff8000" onclick="search()">Search </button>
                          </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr align="center">
                                    <th>S.No</th>
                                    <th>Driver Name</th>
                                    <th>Booking Date</th>
                                    <th>Routes</th>
                                    <th>Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for ($i=0; $i <$fetchData->count; $i++) { 
                                    $driver_id = $fetchData->rows[$i]['driver_id'];
                                    $userInfo = $blViewUser->ViewUserInformation($driver_id );
                                    $driverName = $userInfo->rows[0]['full_name'];
                                    $created_at = $fetchData->rows[$i]['created_at'];
                                    $pickup_location = $fetchData->rows[$i]['pickup_location'];
                                    $dropoff_location = $fetchData->rows[$i]['dropoff_location'];
                                    $location_name = $fetchData->rows[$i]['location_name'];

                                ?>
                                <tr>
                                    <td align="center"><?php echo $i+1 ?></td>
                                    <td align="center"><?php echo $driverName?></td>
                                    <td align="center"><?php echo $created_at ?></td>
                                    <td align="center"><?php echo $pickup_location.'-'.$dropoff_location ?></td>
                                    <td align="center"><?php echo $location_name?></td>
                                    
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-1 mb-3">
                        
                    </div> -->
                   
            </div>
            

          
        </div>

</div>
<div style="height: 150px;"></div>
  </form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteRoutes(cityID)
{
  var r = confirm("Are you sure you want to delete this routes");
  if(r)
  {
  o =new Array(cityID);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=settings&function=deleteroute&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
  }
  else
  {
    return;
  }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("addroutes");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  LoadAjaxScreen("addroutes");
}

  
    
function AddRoutesCheck(){
    var a=document.getElementById('from_city').value;
    var b=document.getElementById('to_city').value;

    if(a=='0')
  {
    alert("Please insert city");
    return false;
  }
  if(b=='0')
  {
    alert("Please insert city");
    return false;
  }

  if(a == b)
  {
    alert("Both Are Same ");
    return false;
  }
  return true;
   

     
}

function AddRoutes(){
 var check=AddRoutesCheck();
if(check==true){
  if(confirm("Are you sure you want to add this Route?"))
    {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('addcityform');   
        PostAjaxScreen("addcityform",form,callfunc);
    } 
}

}

function NextPage(){
      //alert(response.responseText);
      // arr = JSON.decode(response.responseText);
      // if(arr.code != 0)
      //   {
      //     alert(arr.message);
      //     return false;
      //   }

    alert('Successfullu Added')  //console.log(arr);
      LoadAjaxScreen("addroutes");
}


function UpdateCity() {
    if(confirm("Are you sure you want to update this City?"))
    {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('addcityform');   
        PostAjaxScreen("addcityform",form,callfunc);
    }
}

function NextPage1(response){
      LoadAjaxScreen("addroutes");
}

</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script src="js/dependency/moment/moment.js"></script>
<script src="js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>



<script type="text/javascript">
$(document).ready(function() {
    var table = $('#dt-basic-example').DataTable();
     
    $('#dt-basic-example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
       // document.getElementById("CityUpdate").value = data[1];
       // document.getElementById("CityID").value = data[0];
       // document.getElementById("CityUpdate").disabled = false;
    } );
} );
   
$(document).ready(function()
    {
      $('#datepicker-1').daterangepicker(
       {
         opens: 'right'
        }, function(start, end, label)
         {
         console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
       });
  });

  function search(){
      date  =  document.getElementById('datepicker-1').value;
      console.log(date);
              date1 = date.split("-");
              console.log(date1);
            fromDate = date1.[0];
            toDate =date1.[1];
            console.log(fromDate);
            console.log(toDate);
  }

</script>
