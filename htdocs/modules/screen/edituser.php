<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/viewuser.php");

$userId=$_GET['id'];


$viewUser=new BL_ViewUser();
$fetchData=$viewUser->ViewUserInformation($userId);

$vieewUser=new BL_ViewUser();
$fetchDataa=$vieewUser->ViewAgntInformation();

$countViewUser =  $fetchData->count;
for ($i=0; $i <$countViewUser; $i++) { 
$userId = $fetchData->rows[$i]['user_id'];
$userDealerCatergoryId = $fetchData->rows[$i]['dealer_category_id'];
$userSubCategoryId = $fetchData->rows[$i]['sub_category_id'];
$userGroupId = $fetchData->rows[$i]['group_id'];
$password = $fetchData->rows[$i]['password'];
$fullName = $fetchData->rows[$i]['full_name'];
$regCode = $fetchData->rows[$i]['reg_code'];
$cnic = $fetchData->rows[$i]['cnic'];
$cnic_expiry_date = $fetchData->rows[$i]['cnic_expiry_date'];
$mobNumber = $fetchData->rows[$i]['phone_number'];
$email = $fetchData->rows[$i]['email_address'];
$agent = $fetchData->rows[$i]['agent'];
$loginId = $fetchData->rows[$i]['login_id'];
$userStatus = $fetchData->rows[$i]['status'];

$address = $fetchData->rows[$i]['address'];

$account_name = $fetchData->rows[$i]['account_name'];       
$account_number = $fetchData->rows[$i]['account_no'];       
$credit_limit = $fetchData->rows[$i]['credit_limit'];       
//$account_number = $fetchData->rows[$i]['account_number'];       
$created_at = $fetchData->rows[$i]['

d_at'];       
$Date=substr($created_at,0,11);
$Date=date_create($Date);                     
$Date = date_format($Date,"d-M-Y");                    
}
//echo "U:$userStatus";
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title" onclick="LoadAjaxScreen('edituser&id=<?=$userId?>')">
Edit User
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='adduserform1' id='adduserform1' method='post'>
<input type='hidden' name='ACTION' value='UPDATEUSER'>
<input type='hidden' class="form-control" id="userID" name="userID" placeholder="" value="<?php echo $userId ?>" >
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <h5>User Detail</h5>
                    </div>
        </div>
        <div class="form-row">
                 
                     <?php 
                        for ($i=0; $i <$count; $i++) { 
                            $dealerCatergoryId = $dealerCatergory->rows[$i]['dealer_category_id'];
                            $dealerCatergoryName = $dealerCatergory->rows[$i]['dealer_category_name'];
                            $dealerCatergoryName = strtolower($dealerCatergoryName);
                            $dealerCatergoryName = ucfirst($dealerCatergoryName);

                        if ($userDealerCatergoryId==$dealerCatergoryId) {
                       
                            $finaldealerCatergoryName=$dealerCatergoryName;
                        
                        }
                        }
                    ?>
                   
                   
                    
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Name?</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder=""   value="<?php echo $fullName ?>" >
                    </div>
                    <!-- <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Gender</label>
                        <select id="gender" name="gender" class="form-control">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        </select>
                    </div> -->
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">CNIC?</label>
                        <input type="text" class="form-control" id="cnic" name="cnic" placeholder="" d   value="<?php echo $cnic ?>" >
                    </div>
                        <?php 
                        for ($i=0; $i <$count2; $i++) { 
                            $groupId = $groups->rows[$i]['group_id'];
                            $groupName = $groups->rows[$i]['group_name'];
                        if ($userGroupId==$groupId) {
                        
                            $finalgroupName = $groupName;
                        
                        }
                        }
                        ?>
                   
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">CNIC Expiry Date?</label>
                        <input type="date" class="form-control" id="cnic_expiry_date" name="cnic_expiry_date" placeholder="" d   value="<?php echo $cnic_expiry_date ?>" >
                    </div>

                    
        </div>
        <div class="form-row">

                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Mobile Number?</label>
                        <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder=""   value="<?php echo $mobNumber ?>" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Email?</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="" d   value="<?php echo $email ?>" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Address?</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder=""   value="<?php echo $address ?>" >
                    </div>
                   
                   
                   

                    
        </div>
       
        <div class="form-row">

             <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Account Name?</label>
                        <input type="text" class="form-control" id="account_name" name="account_name" placeholder=""   value="<?php echo $account_name ?>" >
                    </div>
                   
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Account Number?</label>
                        <input type="text" class="form-control" id="account_number" name="account_number" placeholder=""    value="<?php echo $account_number ?>" >
                    </div>

                     <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Credit Limit?</label>
                        <input type="text" class="form-control" id="credit_limit" name="credit_limit" placeholder=""    value="<?php  echo $credit_limit ?>" >
                    </div>
        </div>

        <div class="form-row">
                   
                  
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Login ID?</label>
                        <input type="text" class="form-control" id="loginid" name="loginid" placeholder="" d   value="<?php echo $loginId ?>" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Password?</label>
                        <input type="text" class="form-control" id="password" name="password" placeholder="" d   value="<?php  ?>" >
                    </div>
            <div class="col-md-3 mb-3">
                <label class="form-label" for="">Account Status?</label>
                <select id="status" name="status" class="form-control">
                
                   <option value="1" <?=($userStatus=="1")?"SELECTED":""?>>Active</option>
                   <option value="4" <?=($userStatus=="4")?"SELECTED":""?>>Block</option>
                   <option value="3" <?=($userStatus=="3")?"SELECTED":""?>>Incomplete</option>
                   <option value="2" <?=($userStatus=="2")?"SELECTED":""?>>Pending</option>
                   
                  
                
                </select>
            </div> 
        </div>
        <div class="form-row">
            <div class="col-md-3 ">
                <label class="form-label" for="">Agent?</label>
                <select id="agent" name="agent" class="form-control" >
                    <option selected="selected">Select agent</option>
                    
                    <?php
                    for($i=0; $i<$fetchDataa->count;$i++){
                     $selected = $agent == $fetchDataa->rows[$i]['id'] ? 'selected' : '';
                        ?>
                        <option <?=$selected?> value="<?=$fetchDataa->rows[$i]['id']?>"  ><?=$fetchDataa->rows[$i]['name']?></option>
                    <?php }?>
                </select>
                </div>
        <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Joining Date</label>
                        <input type="text" class="form-control" id="joiningdate" name="joiningdate" placeholder="" d   value="<?php echo $Date ?>" disabled>
                    </div>
                
            </div>
      

<div style="margin-top: 20px;"></div>
<div class="form-row">
<div class="col-md-12 mb-12" style="margin-left: 800px">
    


     <button type="button" onClick="LoadAjaxScreen('viewusers&type=all')" class="btn btn-med btn-dark" style="background: #ff8000;color: black" >
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
    


    <button type="button" onclick='UpdateUser()' class="btn btn-med btn-warning" style="background: #ff8000">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Update
    </button>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>
<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
function viewsubcategory(id)
{
//alert(id);

if (id==1 || id==2 || id==9) {
    document.getElementById("accountoffon").disabled = true;
    document.getElementById("group").disabled = true;
    document.getElementById("zone").disabled = true;
    document.getElementById("region").disabled = true;
    document.getElementById("city").disabled = true;
    document.getElementById("area").disabled = true;
    document.getElementById("town").disabled = true;
    document.getElementById("market").disabled = true;
}
else{
    document.getElementById("accountoffon").disabled = false;
    document.getElementById("group").disabled = false;
    document.getElementById("zone").disabled = false;
    document.getElementById("region").disabled = false;
    document.getElementById("city").disabled = false;
    document.getElementById("area").disabled = false;
    document.getElementById("town").disabled = false;
    document.getElementById("market").disabled = false;
}


if(id == '')
{
alert("No ID found!!");


}
else
{
o =new Array(id);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetSubCategory&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetSubCategoryCallBack});

}
}
function GetSubCategoryCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('subdealer').options.length = 0;
document.getElementById('subdealer').innerHTML += "<option value=''>Select Sub Dealer Catergory</option>";
document.getElementById('subdealer').innerHTML += obj;
}
///
function zonetoregion(zone)
{
// alert(zone);
if(zone == '')
{
alert("No ID found!!");
}
else
{
o =new Array(zone);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetRegion&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetRegionCallBack});

}
}
function GetRegionCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('region').options.length = 0;
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('region').innerHTML += obj;
}
///
function regiontocity(region)
{
// alert(region);
if(region == '')
{
alert("No ID found!!");
}
else
{
o =new Array(region);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetCity&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetCityCallBack});

}
}
function GetCityCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('city').options.length = 0;
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('city').innerHTML += obj;
}
///
function citytoarea(city)
{
//  alert(city);
if(city == '')
{
alert("No ID found!!");
}
else
{
o =new Array(city);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetArea&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAreaCallBack});

}
}
function GetAreaCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('area').options.length = 0;
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('area').innerHTML += obj;
}
///
function  CheckError()
{
console.log('error');
}

///

function UpdateUser() {
	if(confirm("Are you sure you want to update this user?"))
	{
		var callfunc={onSuccess:NextPage};
		form= document.getElementById('adduserform1');   
		PostAjaxScreen("adduserform1",form,callfunc);
	}
}

function NextPage(){
LoadAjaxScreen("viewusers&type=all");
}
</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
