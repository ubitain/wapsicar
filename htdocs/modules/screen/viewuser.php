<?php
    require_once(MODULE."/class/bllayer/viewuser.php");
    
   // $userId = $mysession->getValue("userid");
    $viewUser=new BL_ViewUser();
    $fetchData=$viewUser->ViewUser();
    $count =  $fetchData->count;

?>

<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
View Users
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
 <!-- datatable start -->
                <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr align="center">
                            <th>Reg Code</th>
                            <th>Sap Code</th>
                            <th>Login ID</th>
                            <th>Name</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Town</th>
                            <th>Zone</th>
                            <th>City</th>
                            <th>Joining Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?

                            for ($i=0; $i <$count; $i++) { 
                                //for
                                $userId = $fetchData->rows[$i]['user_id'];
                                $fullName = $fetchData->rows[$i]['full_name'];
                                $fullName = ucwords($fullName);
                                $regCode = $fetchData->rows[$i]['reg_code'];
                                $sapCode = $fetchData->rows[$i]['sap_code'];
                                $mobNumber = $fetchData->rows[$i]['mobile_no'];
                                $loginId = $fetchData->rows[$i]['login_id'];
                                $email = $fetchData->rows[$i]['email'];
                                $address = $fetchData->rows[$i]['address'];
                                $town = $fetchData->rows[$i]['town'];
                                $zone = $fetchData->rows[$i]['zone'];                          
                                $city = $fetchData->rows[$i]['city'];                          
                                $created_at = $fetchData->rows[$i]['created_at'];                          
                                $created_at=substr($created_at,0,10);

                        ?>

                                <tr>
                                <td ><?=$regCode?></td>
                                <td ><?=$sapCode?></td>
                                <td ><?=$loginId?></td>
                                <td ><?=$fullName?></td>
                                <td ><?=$mobNumber?></td>
                                <td ><?=$email?></td>
                                <td ><?=$address?></td>
                                <td ><?=$town?></td>
                                <td ><?=$zone?></td>
                                <td ><?=$city?></td>
                                <td ><?=$created_at?></td>
                                <td align="center" nowrap><button type="button" onClick="LoadAjaxScreen('edituser&id=<?php echo $userId ?>')" class="btn btn-sm btn-warning" onClick="LoadAjaxScreen()">
                                <span class="fal  fa-edit mr-1"></span>
                                </button>
                                <button type="button" onclick="DeleteUser('<?php echo $userId ?>')" class="btn btn-sm btn-dark">
                                <span class="fal  fa-times mr-1"></span>
                                </button></td>
                                
                                </tr>
                            <?
                                }
                            ?>
                       
                    </tbody>
                </table>
</div>
</div>

</div>
</div>
</div>


</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteUser(userID)
{
    var r = confirm("Are you sure you want to delete this user?");
    if(r==true)
    {
    o =new Array(userID);
    o = JSON.encode(o);
    var pars = 'param='+o;
    var url = "/index.php?object=user&function=deleteduser&isajaxcall=1&returnType=string";
    var myAjax = new Ajax.Request( url,
    { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
    }
    else
    {
      return;
    }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("viewuser");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  LoadAjaxScreen("viewuser");
}
</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>