 <main id="js-page-content" role="main" class="page-content">

                        <ol class="breadcrumb page-breadcrumb">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-chart-area'></i> Static <span class='fw-300'>Report</span>
                            </h1>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
                        </ol>
                        <!--   <div class="subheader">
                     </div> -->

                   
                     <div class="row" style="margin-top: -25px;">
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            1506
                                            <small class="m-0 l-h-n">Total Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            45
                                            <small class="m-0 l-h-n">Reported Today</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            205
                                            <small class="m-0 l-h-n">Reported This Month</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            405
                                            <small class="m-0 l-h-n">Reported This Year</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <h5>WRT Status</h5>
                        </div>
                    </div>
                     <div class="row" style="">
                        <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            156
                                            <small class="m-0 l-h-n">Pending Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            300
                                            <small class="m-0 l-h-n">In Process Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            405
                                            <small class="m-0 l-h-n">Completed Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            53
                                            <small class="m-0 l-h-n">None</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            
                    </div>


<div class="row" >
    <div class="col-lg-6">
        <div id="panel-4" class="panel">
            <div class="panel-container show" style="">
                    <div class="form-row" style="margin-left: 5px; margin-right: 5px;">
                        <div class="col-md-12 mb-3">
                            <h5>Top 3 Claims wrt Vendors</h5>
                            <div style="margin-top:20px;"></div>
                            <table class="table table-bordered table-hover m-0">
                            <thead>
                                <tr>
                                    
                                    <th>#</th>
                                    <th>Vendor ID</th>
                                    <th>Name</th>
                                    <th>No of Complains</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>az-35</td>
                                    <td>Otto</td>
                                    <td>22</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>ab-21</td>
                                    <td>Thornton</td>
                                    <td>12</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>bk-43</td>
                                    <td>Ronaldo</td>
                                    <td>9</td>
                                </tr>
                            </tbody>
                        </table>


                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div id="panel-4" class="panel">
            <div class="panel-container show" style="">
                    <div class="form-row" style="margin-left: 5px; margin-right: 5px;">
                        <div class="col-md-12 mb-3">
                            <h5>Top 3 Claims wrt Product</h5>
                            <div style="margin-top:20px;"></div>
                            <table class="table table-bordered table-hover m-0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product ID</th>
                                    <th>Name</th>
                                    <th>No of Complains</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>nuts-sl-2</td>
                                    <td>Nuts</td>
                                    <td>645</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>clu-63</td>
                                    <td>Clutch</td>
                                    <td>331</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>br-61</td>
                                    <td>Brake</td>
                                    <td>160</td>
                                </tr>
                            </tbody>
                        </table>

                        </div>
                    </div>
            </div>
        </div>
    </div>


</div>




</main>
