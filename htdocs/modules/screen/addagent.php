 <?php 
require_once(MODULE."/class/bllayer/user.php");
// require_once(MODULE."/class/bllayer/viewuser.php");
// require_once(MODULE."/class/bllayer/addstuff.php");

$userId=$mysession->getvalue('userid');
/*
$fcmId="dSTfHIW5QRqp6jy1FnsoBj:APA91bEq6jvjjVY2ulScjoeJ9AB_glHqou2HZW2cudxQqnyq_kQCv739TYW6JwU57sYu1xhIC1R7ewduVqgJ_ceDRwvcsXVMnXLK-x_Pgq8moFN7mLQTd_W7s8myLORcIrY7OHitjghX";
$body="test body";
$title="test title";
$objId="1";
$type="general";
SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId,"customer");
*/
// $blUser=new BL_User();
// $driverInfo = $blUser->GetUserInfoByUserType(USER_TYPE_DRIVER);

// $routes = new BL_AddStuff();
// $routesInfo = $routes->GetRoute();

//print_r_pre($driverInfo,USER_TYPE_DRIVER);
//echo "U:$userStatus";
?>
<main id="js-page-content" role="main" class="page-content">

<h1 class="subheader-title">
Add Agent
</h1>
 
<form class="needs-validation" name='form1' id='form1' method='post' novalidate>
<input type='hidden'  name='ACTION' id='ADDAGENT' value='ADDAGENT'>
<input type='hidden'  name='userId' id='userId' value='<?=$userId?>'>
<input type='hidden'  name='mobilecheck' id='mobilecheck' value=''>
<div class="panel">
<div class="panel-container show">
    <div class="panel-content" >
        <div class="row">
              
          <div class="col-md-6 mb-4">
              <label class="form-label" for="">Name</label>
              <input type="text" id="" name="name" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)"maxlength="25" placeholder="Name">
              
			  <label class="form-label" for="">Company Name</label>
              <input type="text" id="" name="company_name" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)"maxlength="25" placeholder="Company Name">
              
              <label class="form-label" for="" style="margin-top: 6px">Email</label>
              <input  type="email" id="email" name="email" class="form-control" maxlength="25" placeholder="Email">

              <div align="left">
              <label class="form-label" for="" style="margin-top: 6px">City</label>
              <input type="text" id="" name="city" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" maxlength="10" placeholder="City">
            </div>
          </div>
          <div class="col-md-6 mb-4" style="padding-right: 10px;">
              <label class="form-label" for="" style="margin-top: ">Mobile Number</label>
              <span style="color: red"> *</span>
              <input type="text" id="mobile" name="mobile" class="form-control"onchange="CheckMobile(this.value)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'maxlength="15" placeholder="Mobile Number " >
              
              <label class="form-label" for="" style="margin-top: 6px">Address</label>
              <input type="text" id="" name="address" class="form-control"maxlength="60" placeholder="Address">
          </div>
          <button type="button" onclick="AddAgent()" class="btn btn-med btn-warning" style="margin-left: 800px; background: #ff8000;">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Add Agent
    </button>               
        </div>
      </div>
    </div>
  </div>
  </form>
</main>
  <script type="text/javascript">
    
  function Check(){
   if(document.getElementById('mobile').value=='')
  {
    alert("Please insert mobile number");
    return false;
  }
  return true;
}
function AddAgent(){
  var x=Check();

  email=document.getElementById('email').value;

        if (email == "") {
            alert("Email must be filled");
          return false;
        }
        var emailregex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (emailregex.test(email) == false){
            alert("Wrong Email! (Hint:abc@gmail.com)");
            return false;
        }

        mobilecheck = document.getElementById('mobilecheck').value;
        if (mobilecheck == "1") {
			alert('Mobile Number already exist');
           return false;
        }
		
		

  if(x==true){



    if(confirm("Are you sure you want to Add this user?")){
    var obj = {onSuccess:AddusersInfoCallBack};
          form = document.getElementById('form1');
          PostAjaxScreen("form1",form,obj)
        }
  }
    function AddusersInfoCallBack(){
     alert('Successfully added');
     LoadAjaxScreen("viewagent");
    }
  }
</script>
<script type="text/javascript">
function CheckMobile(mobile){

  // alert(empId);
  o =new Array(mobile);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=agent&function=CheckMobile&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: CheckMobileCallBack});

}
function CheckMobileCallBack(res){
var ret = JSON.decode(res.responseText);
console.log(ret);
if(ret == 1)
{
    alert(" Mobile Number already exist");
    $('#mobile').val('');
    document.getElementById('mobilecheck').value = "1";
    return false;
}
else{
    document.getElementById('mobilecheck').value = "";
}
// else
// {
//      alert("Not Exit Or  NOT Approved");
// }
}
</script>
        
     <script type="text/javascript">
       
     </script>    
