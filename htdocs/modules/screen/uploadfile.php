<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
<i class='subheader-icon fal fa-chart-area'></i> Test Upload
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div id="panel-5" class="panel">
<div class="panel-container show">
<div class="panel-content p-0">
<form class="needs-validation" novalidate>
<div class="panel-content">
    <div class="form-row">
        <div class="col-md-4 mb-3">
           
        </div>
        <div class="col-md-4 mb-3">
            
        </div>
        <div class="col-md-4 mb-3">
            
        </div>
    </div>
    
    <div id="dynamic" style="border:  1px solid lightgrey; padding-top: 15px; ">
        <div class="form-row" id="delete0" style="padding-left: 3px; padding-right: 3px;">
        <div class="col-md-4 mb-3">
            <label class="form-label" >Product Name <span class="text-danger">*</span> </label>
            <input type="text" class="form-control" id="Name0" placeholder="" value="" >
        </div>
        <div class="col-md-3 mb-3">
            <label class="form-label" >Product Quality <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="Quality0" placeholder="" value="" >
        </div>
        <div class="col-md-4 mb-3">
            <label class="form-label" >Picture <span class="text-danger">*</span></label>
            <input type="file" class="form-control" id="Picture0" placeholder="" value="" >
        </div>
        <div class="col-md-1 mb-3">
            
        </div>
    </div>
    </div>
    <div class="form-row">
        <div class="col-md-9 mb-3">
           
        </div>
        <div class="col-md-3 mb-3" style="margin-top: 10px;  padding-left: 80px;" >
        <button type="button" onclick="GetMoreRowsForClaim()"  class="btn btn btn-warning"><span class="fal fa-plus"></span> Add Row</button>
        </div>
    </div>
</div>
</form>
</div>
</div>
</div>
</main>


<script type="text/javascript">
    var count = 0;
    function GetMoreRowsForClaim()
    {   
        count++;
        var data = '<div class="form-row" id="delete'+count+'" style="padding-left: 4px; padding-right: 3px;">';
        data = data +'<div class="col-md-4 mb-3">';
        data = data +'<label class="form-label" >Product Name <span class="text-danger">*</span> </label>';
        data = data +'<input type="text" class="form-control" id="Name'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-3 mb-3">';
        data = data +'<label class="form-label" >Product Quality <span class="text-danger">*</span></label>';
        data = data +'<input type="text" class="form-control" id="Quality'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-4 mb-3">';
        data = data +'<label class="form-label" >Picture <span class="text-danger">*</span></label>';
        data = data +'<input type="file" class="form-control" id="Picture'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-1 mb-3">';
        data = data +'<label class="form-label" >Action</label>';
        data = data +'<button type="button" class="btn btn-danger"  id="del'+count+'" onclick="DelRowForClaim('+count+')"><i class="fa fa-times"></i> </button>';
        data = data +'</div>';
        data = data +'</div>';


        $("#dynamic").append(data);
    }



    function DelRowForClaim(count)
    {
  
    if(confirm("Are you sure you want to Delete?"))
        {
     var el = count;
     $('#delete'+el).remove();
    }
    }
</script>