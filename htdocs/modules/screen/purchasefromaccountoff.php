
<style>
* {
box-sizing: border-box;
}

.image1 {

transition: transform .2s;
width: 100px;
height: 100px;
margin: 0 auto;
}

.image1:hover {
-ms-transform: scale(1.5); /* IE 9 */
-webkit-transform: scale(1.5); /* Safari 3-8 */
transform: scale(1.5); 
}

</style>

<?php
require_once(MODULE."/class/bllayer/viewclaims.php");
require_once(MODULE."/class/bllayer/user.php");
$claimId=$_GET['id'];
$user = new BL_User();
$purchaseFromList = $user->GetPurchaseFromList();
$countPurchaseFromList =  $purchaseFromList->count;

$accountOffList = $user->GetAccountOffList();
$countAccountOffList =  $accountOffList->count;
// $userId = $mysession->getValue("userid");
$viewClaims=new BL_ViewClaims();
$fetchData=$viewClaims->ViewClaimsDetail($claimId);
$count =  $fetchData->count;

for ($i=0; $i <1; $i++) { 

//for
$claimID = $fetchData->rows[$i]['claim_id'];
$claimNo = $fetchData->rows[$i]['claim_no'];
$dealerID = $fetchData->rows[$i]['user_id'];

$fetchData1=$viewClaims->FetchDealerName($dealerID);
$claimName = $fetchData1->rows[0]['full_name'];
$claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
$claimDealerCatergory=strtolower($claimDealerCatergory);
$claimMobNumber = $fetchData1->rows[0]['mobile_no'];
$claimDealerAddress = $fetchData1->rows[0]['address'];

$purchasedFrom = $fetchData->rows[$i]['purchased_from'];
$accountOf = $fetchData->rows[$i]['account_of'];
$lcsNo = $fetchData->rows[$i]['lcs_no'];
$cartonWeight = $fetchData->rows[$i]['carton_weight'];
$remarks = $fetchData->rows[$i]['remarks'];
$claimStatus = $fetchData->rows[$i]['claim_status'];
$Date = $fetchData->rows[$i]['created_at'];  
// ye sirf print krwnay kay liye tha jab last time form banaya tha ar reject hogya tha, sir said saari values na dekhao!
}

$fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
$count2 =  $fetchData2->count;

$statusError=$_GET['statusError'];
if ($statusError==1) {
?>
<script type="text/javascript">
document.getElementById("Account").style.borderColor = "red";
</script>
<?php
}



$fetchDataPurch = $viewClaims->FetchClaimTransactionsPurchase($claimID);
$countTransactionPurchase =  $fetchDataPurch->count;


for ($j=0; $j <$countTransactionPurchase; $j++) { 
$purchasedFrom = $fetchDataPurch->rows[$j]['user_id'];
$fetchData13=$viewClaims->FetchDealerName($purchasedFrom);
$purchasedFrom1 = $fetchData13->rows[0]['full_name'];
$purchasedFromDesig = $fetchData13->rows[0]['dealer_category_name'];
$purchasedFromDesig=strtolower($purchasedFromDesig);
$purchasedFromDesig=ucfirst($purchasedFromDesig);
}

$fetchData1= $viewClaims->FetchClaimTransactions($claimID);
$countTransaction =  $fetchData1->count;

for ($j=0; $j <$countTransaction; $j++) { 
$accountOfId = $fetchData1->rows[$j]['user_id'];
$fetchDataAccountOfId=$viewClaims->FetchDealerName($accountOfId);
$accountOfName = $fetchDataAccountOfId->rows[0]['full_name'];
$accountOfDesig = $fetchDataAccountOfId->rows[0]['dealer_category_name'];
$accountOfDesig=strtolower($accountOfDesig);
$accountOfDesig=ucfirst($accountOfDesig);
}

$zones = $user->GetZones();
$count5 =  $zones->count;

?>

<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
In Assign Purchase From/ Account Of
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addpurchaseaccount' id='addpurchaseaccount' method='post'>
<input type='hidden' name='ACTION'  value='ADDPURCHASEACCOUNT'>
<input type='hidden' name='claimID' value="<?php echo $claimID ?>" >
<div class="row" style="margin-top: -30px;"> 
<div class="col-xl-12">
    <div id="panel-1" style="" class="panel">
        
        <div class="panel-container show">
            <div class="panel-content" >
                <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <h5></h5>
                            </div>
                </div>
                <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="validationTooltip01">Claim Number</label>
                                <input type="text" class="form-control" id="" name="" placeholder="" disabled="true"  value="<?php echo $claimNo ?>" >
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="validationTooltip01">Claimer Name</label>
                                <input type="text" class="form-control" id="claimName" name="claimName" placeholder="" disabled="true"  value="<?php echo ucfirst($claimName) ?>" >
                            </div>
                </div>
                <div class="form-row">
                    <?php 
                    if ($countTransactionPurchase<=0) 
                    {
                        ?>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Zone</label>
                                <select id="zone" name="zone" class="form-control" onchange='zonetoregion(this.value)' >
                                    <option value="">Select Zone</option>
                                 <?php 
                                for ($i=0; $i <$count5; $i++) { 
                                    $zonesName = $zones->rows[$i]['zone_name'];
                                    $zoneId = $zones->rows[$i]['zone_id'];
                                    $zonesName = strtolower($zonesName);
                                    $zoneId = strtolower($zoneId);
                                    $zonesName = ucfirst($zonesName);
                                    $zoneId = ucfirst($zoneId);
                                ?>
                                    <option value="<?php echo $zonesName.",".$zoneId ?>"><?php echo $zonesName ?></option>
                                <?php 
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Region</label>
                                <select id="region" name="region" class="form-control" onchange='regiontocity(this.value)'>
                                     <option value="">Select Region</option>
                              <!--   <?php 
                                for ($i=0; $i <$count4; $i++) { 
                                    $regionsName = $regions->rows[$i]['region_name'];
                                    $regionsName = strtolower($regionsName);
                                    $regionsName = ucfirst($regionsName);
                                ?>
                                    <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div> 
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="validationTooltip01">City</label>
                                <select id="city" name="city" class="form-control" onchange='citytoarea(this.value)'>
                                <option value="">Select City</option>
                                <!-- <?php 
                                for ($i=0; $i <$count3; $i++) { 
                                    $cityName = $city->rows[$i]['city_name'];
                                ?>
                                    <option value="<?php echo $cityName ?>"><?php echo $cityName ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Area</label>
                                <select id="area" name="area" onchange='areatopurchasefrom(this.value)' class="form-control">
                                <option value="">Select Area</option>

                                </select>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="validationTooltip01">Purchase From</label>
                                 <select id="Purchaseselect" name="Purchaseselect" class="form-control" style="">
                                    <option value="">Select Purchase From</option>
                                 <!-- <?php 
                                for ($i=0; $i <$countPurchaseFromList; $i++) { 
                                    $purchasedFrom = $purchaseFromList->rows[$i]['user_id'];
                                    $purchasedFrom1 = $purchaseFromList->rows[$i]['full_name'];
                                    $purchasedFrom1 = strtolower($purchasedFrom1);
                                    $purchasedFrom1 = ucfirst($purchasedFrom1);
                                ?>
                                    <option value="<?php echo $purchasedFrom1.",".$purchasedFrom ?>"><?php echo $purchasedFrom1 ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div>
                        <?php
                    }
                    else{
                    ?>
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="validationTooltip01">Purchase From</label>
                                 <input type="text" class="form-control" id="Purchase" name="Purchase" placeholder="" disabled="true"  value="<?php echo ucfirst($purchasedFrom1).' - '. ucfirst($purchasedFromDesig) ?>" >
                            </div>
                    <?php 
                    } 
                    ?>
                </div>
                <div class="form-row">
                            <?php 
                            if ($countTransaction<=0) 
                            {
                            ?>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Zone</label>
                                <select id="zone" name="zone" class="form-control" onchange='zonetoregion(this.value)' >
                                    <option value="">Select Zone</option>
                                 <?php 
                                for ($i=0; $i <$count5; $i++) { 
                                    $zonesName = $zones->rows[$i]['zone_name'];
                                    $zoneId = $zones->rows[$i]['zone_id'];
                                    $zonesName = strtolower($zonesName);
                                    $zoneId = strtolower($zoneId);
                                    $zonesName = ucfirst($zonesName);
                                    $zoneId = ucfirst($zoneId);
                                ?>
                                    <option value="<?php echo $zonesName.",".$zoneId ?>"><?php echo $zonesName ?></option>
                                <?php 
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Region</label>
                                <select id="region" name="region" class="form-control" onchange='regiontocity(this.value)'>
                                     <option value="">Select Region</option>
                              <!--   <?php 
                                for ($i=0; $i <$count4; $i++) { 
                                    $regionsName = $regions->rows[$i]['region_name'];
                                    $regionsName = strtolower($regionsName);
                                    $regionsName = ucfirst($regionsName);
                                ?>
                                    <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div> 
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="validationTooltip01">City</label>
                                <select id="city" name="city" class="form-control" onchange='citytoarea(this.value)'>
                                <option value="">Select City</option>
                                <!-- <?php 
                                for ($i=0; $i <$count3; $i++) { 
                                    $cityName = $city->rows[$i]['city_name'];
                                ?>
                                    <option value="<?php echo $cityName ?>"><?php echo $cityName ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="">Area</label>
                                <select id="area" name="area" onchange='areatoaccountoff(this.value)' class="form-control">
                                <option value="">Select Area</option>

                                </select>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="validationTooltip01">Account Of</label>
                                 <select id="Account" name="Account" class="form-control" style="">
                                    <option value="">Select Account Of</option>
                                <!--  <?php 
                                for ($i=0; $i <$countAccountOffList; $i++) { 
                                    $accountOffId = $accountOffList->rows[$i]['user_id'];
                                    $accountOffName = $accountOffList->rows[$i]['full_name'];
                                    $accountOffName = strtolower($accountOffName);
                                    $accountOffName = ucfirst($accountOffName);
                                ?>
                                    <option value="<?php echo $accountOffName.",".$accountOffId ?>"><?php echo $accountOffName ?></option>
                                <?php 
                                }
                                ?> -->
                                </select>
                            </div>
                            <?php 
                            }
                            else{
                                ?>
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="validationTooltip01">Account Of</label>
                                 <input type="text" class="form-control" id="Accounttext" name="Accounttext" placeholder="" disabled="true"  value="<?php echo ucfirst($accountOfName).' - '. ucfirst($accountOfDesig) ?>" >
                            </div>
                                <?php 
                            }
                            ?>
                </div>
                <div class="form-row">
                    <div class="col-md-8 mb-3">
                    </div>
                    <div class="col-md-4 mb-3" >
                        <button type="button" style="margin-left: 25px;" onclick="AddPurchaseAccount()" data-dismiss="modal"   class="btn btn-med btn-warning">
                            <span class="fal fa fa-arrow-right mr-1"></span>
                            Save
                        </button>
                    </div>
                </div>
              
        </div>
    </div>
</div>
</div>
</div>
</form>
</main>

<!-- <nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav> -->
<script type="text/javascript">





function AddPurchaseAccount() {
// var x = document.getElementById("Account").value;
// var y = document.getElementById("Purchase").value;

// if (x!="" && y!="") {
if(confirm("Are you sure you want to save?"))
{
var callfunc={onSuccess:NextPage1};
form= document.getElementById('addpurchaseaccount');   
PostAjaxScreen("addpurchaseaccount",form,callfunc);
}
// }
// else
// {
//     alert('No Account Of Found!');
//    OpenModalBox('purchasefromaccountoff&id=<?php echo $claimID ?>&statusError=1','screen','');
// }

}

function NextPage1(){
LoadAjaxScreen("incompleteclaims");
}


function zonetoregion(zone)
{
// alert(zone);
if(zone == '')
{
alert("No ID found!!");
}
else
{
o =new Array(zone);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetRegion&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetRegionCallBack});

}
}
function GetRegionCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('region').options.length = 0;
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('region').innerHTML += obj;
}
///
function regiontocity(region)
{
// alert(region);
if(region == '')
{
alert("No ID found!!");
}
else
{
o =new Array(region);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetCity&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetCityCallBack});

}
}
function GetCityCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('city').options.length = 0;
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('city').innerHTML += obj;
}
///
function citytoarea(city)
{
//  alert(city);
if(city == '')
{
alert("No ID found!!");
}
else
{
o =new Array(city);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetArea&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAreaCallBack});

}
}
function GetAreaCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('area').options.length = 0;
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('area').innerHTML += obj;
}
//
function areatopurchasefrom(area)
{
//  alert(city);
if(area == '')
{
alert("No ID found!!");
}
else
{

var zone = document.getElementById("zone").value;
var region = document.getElementById("region").value;
var city = document.getElementById("city").value;
//var area = document.getElementById("area").value;

o = new Array(zone,region,city,area);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetPurchaseFrom&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetPurchaseFromCallBack});

}
}
function GetPurchaseFromCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('Purchaseselect').options.length = 0;
document.getElementById('Purchaseselect').innerHTML += "<option value=''>Select Purchase From</option>";
document.getElementById('Purchaseselect').innerHTML += obj;
}
//
function areatoaccountoff(area)
{
//  alert(city);
if(area == '')
{
alert("No ID found!!");
}
else
{

var zone = document.getElementById("zone").value;
var region = document.getElementById("region").value;
var city = document.getElementById("city").value;
//var area = document.getElementById("area").value;

o = new Array(zone,region,city,area);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetAccountof&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAccountofCallBack});

}
}
function GetAccountofCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('Account').options.length = 0;
document.getElementById('Account').innerHTML += "<option value=''>Select Account Of</option>";
document.getElementById('Account').innerHTML += obj;
}
</script>


<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>