
<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    $claimId=$_GET['id'];
   // $userId = $mysession->getValue("userid");
    $viewClaims=new BL_ViewClaims();
    $fetchData=$viewClaims->ViewClaimsDetail($claimId);
    $count =  $fetchData->count;

    for ($i=0; $i <1; $i++) { 
          //for
            $claimID = $fetchData->rows[$i]['claim_id'];
            $dealerID = $fetchData->rows[$i]['user_id'];
         
            
            $fetchData1=$viewClaims->FetchDealerName($dealerID);
            $claimName = $fetchData1->rows[0]['full_name'];
            $claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
            $claimDealerCatergory=strtolower($claimDealerCatergory);
            $claimMobNumber = $fetchData1->rows[0]['mobile_no'];
            $claimDealerAddress = $fetchData1->rows[0]['address'];

            $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
            $fetchData1=$viewClaims->FetchDealerName($purchasedFrom);
            $purchasedFrom = $fetchData1->rows[0]['full_name'];
            $purchasedFromDesig = $fetchData1->rows[0]['dealer_category_name'];

            $accountOf = $fetchData->rows[$i]['account_of'];
            $fetchData1=$viewClaims->FetchDealerName($accountOf);
            $accountOf = $fetchData1->rows[0]['full_name'];
            $accountOfDesig = $fetchData1->rows[0]['dealer_category_name'];

            $lcsNo = $fetchData->rows[$i]['lcs_no'];
            $cartonWeight = $fetchData->rows[$i]['carton_weight'];
            $remarks = $fetchData->rows[$i]['remarks'];
            $claimStatus = $fetchData->rows[$i]['claim_status'];
            $Date = $fetchData->rows[$i]['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");

            $purchasedFromDesig=strtolower($purchasedFromDesig);
            $purchasedFromDesig=ucfirst($purchasedFromDesig);
            $accountOfDesig=strtolower($accountOfDesig);
            $accountOfDesig=ucfirst($accountOfDesig);

    }

    $fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
    $count5 =  $fetchData2->count;

    $fetchData4=$viewClaims->FetchVoucherDetail($claimId);
    $countVouchers =  $fetchData4->count;

    $fetchData6=$viewClaims->ViewClaimsGrandTotal($claimId);
    $count6 =  $fetchData6->count;
    $grandTotal = $fetchData6->rows[0]['grand_total'];
    $discount = $fetchData6->rows[0]['discount'];
    //echo $grandTotal."sadardiqwoeqwoie".$discount ;
?>

<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Final Settlement
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>
<form class="form-horizontal" name='addvoucher' id='addvoucher' method='post'>
<input type='hidden' name='ACTION' value='ADDVOUCHER'>
<input type="text" class="form-control" id="count2" name="count2" placeholder="" hidden="true"  value="0" >
<input type="text" class="form-control" id="count5" name="count5" placeholder="" hidden="true"  value="<?php echo $count5 ?>" >
<input type="text" class="form-control" id="voucherortotal" name="voucherortotal" placeholder="" hidden="true"   >
            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                    <div class="panel-container show">
                        <div class="panel-content" >
                            

                            <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">ClaimID</label>
                                                <input type="text" class="form-control" name="" id="" placeholder="" disabled="true"  value="<?php echo $claimID ?>" >
                                                <input type="text" class="form-control" name="aclaimID" id="aclaimID" placeholder="" hidden="true"  value="<?php echo $claimID ?>" >
                                            </div>

                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Date</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $Date ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Remarks</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $remarks ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimStatus ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Dealer Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimName ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Dealer Type</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimDealerCatergory ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Phone Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimMobNumber ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Address</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimDealerAddress ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchased From</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $purchasedFrom." "."$purchasedFromDesig".")" ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">On Account Of</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $accountOf ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Carton Weight</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $cartonWeight ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Consigment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $lcsNo ?>" >
                                            </div>

                                </div>

                   
                    <div class="form-row" >
                        <div class="col-md-12 mb-3">
                            <h5 style="font-weight: bold;">Claim Item Information</h5>
                        </div>
                    </div>
                            <div class="form-row" style="">
                            <div class="col-md-12 mb-3">
                                <div style="margin-top:0px;"></div>
                                <table class="table table-bordered table-hover m-0"   >
                                <thead>
                                    <tr style="background-color: #dfe1e6;">
                                        <th style="text-align: center;">Serial No</th>
                                        <th style="text-align: center;">Item Code</th>
                                        <th style="text-align: center;">Item Name</th>
                                        <th style="text-align: center;">Recieved Quantity</th>
                                        <th style="text-align: center;">Approved Quantity</th>
                                        <th style="text-align: center;">Price Per Item</th>
                                        <th style="text-align: center;">Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?

                                for ($i=0; $i <$count5; $i++) { 
                                      //for
                                        $claimItemId = $fetchData2->rows[$i]['item_id'];
                                        $recievedQty = $fetchData2->rows[$i]['recieved_qty'];
                                        $claimDetailId = $fetchData2->rows[$i]['claim_detail_id'];
                                        $approvedQty = $fetchData2->rows[$i]['approved_qty'];
                                        $totalPrice = $fetchData2->rows[$i]['totalprice'];
                                        $pricePerItem = $fetchData2->rows[$i]['priceperitem'];
                                        $fetchData3=$viewClaims->FetchItemName($claimItemId);
                                        $claimItemName = $fetchData3->rows[0]['item_name'];
                                        $claimItemCode = $fetchData3->rows[0]['item_code'];

                                        $remarks = $fetchData2->rows[$i]['remarks'];

                                ?>
                                <tr>
                                <th scope="row" style="text-align: center;"><?php echo $i+1; ?></th>
                                <td>
                                <input type="text" class="form-control" id="" placeholder="" disabled="true"  value="<?php echo $claimItemCode; ?>" >
                                <input type="text" class="form-control" id="claimDetailId<?php echo $i ?>" name="claimDetailId<?php echo $i ?>" placeholder="" hidden="true"  value="<?php echo $claimDetailId; ?>" >
                                </td>
                                <td>
                                <input type="text" class="form-control"  placeholder="" disabled="true"  value="<?php echo $claimItemName ?>" >
                                </td>
                                <td >
                                <input type="text" style="text-align: center;" class="form-control"  id="recievedQty<?php echo $i ?>" placeholder="" disabled="true"  value="<?php echo $recievedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="Approved<?php echo $i ?>" disabled="true" name="Approved<?php echo $i ?>" placeholder=""  value="<?php echo $approvedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="pricePerItem<?php echo $i ?>" onchange="countValues(<?php echo $i ?>)" name="pricePerItem<?php echo $i ?>" placeholder=""   value="<?php echo $pricePerItem ?>">

                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="totalPrice<?php echo $i ?>" readonly name="totalPrice<?php echo $i ?>" placeholder=""  value="<?php echo $totalPrice ?>" >
                                <!-- <input type="text" style="text-align: center;" class="form-control" id="totalPrice<?php echo $i ?>" name="totalPrice<?php echo $i ?>" hidden placeholder=""  value="<?php echo $totalPrice ?>" > -->
                                </td> 
                                <tr>        
                                <?php } ?>
                                <tr>
                                <th scope="row" style="text-align: center;"></th>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td >
                                </td>
                                <td>
                                </td>
                                <td align="center">
                            <h6 style="font-weight: bold; margin-top: 15px;">Discount</h6>
                                </td>
                                <td>
                                    <input type="text" style="text-align: center; " class="form-control" id="discount" name="discount"  placeholder="" onchange="calculate()" value="<?php echo $discount ?>">
                                </td> 
                                <tr>
                                   <tr>
                                <th scope="row" style="text-align: center;"></th>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td >
                                </td>
                                <td>
                                </td>
                                <td align="center">
                            <h5 style="font-weight: bold; margin-top: 15px;">Total Amount</h5>
                                </td>
                                <td>

                                    <input type="text" style="text-align: center; " class="form-control" id="grandSum" name="grandSum"   placeholder="" readonly="" value="<?php echo $grandTotal ?>">
                                </td> 
                                <tr>    
                                </tbody>
                                </table>

                                </div>
                            </div>





                            <!-- <div class="form-row" >
                                    <div class="col-md-10">
                                    </div>
                                    <div class="col-md-2" align="">
                                        <label class="form-label" style="margin-left: 20px;" for="validationTooltip01">Phone Number</label>
                                        <input type="text" style="text-align: center; width: 85%; margin-left: 20px;" class="form-control" id="discount" name="discount"  placeholder=""  value="">
                                    </div>
                            </div>
 -->
                    <!-- <div style="margin-top: 20px;"></div>
 -->                
                    <div class="form-row" style="margin-top: 0px;">
                        <div class="col-md-2 ">
                        </div>
                        <div class="col-md-10  " align="right" >
                            <button type="button" onclick="AddCalculation()" style="width: 100px;" class="btn btn btn-dark"><span class="fal fa-save"></span> Save</button>
                            <button type="button" onclick="calculate()" style="" class="btn btn-med btn-warning">
                                <span class="fal fa  fa-link mr-1"></span>
                                Calculate
                            </button>
                        </div>
                    </div>
                    <div class="form-row" >
                        <div class="col-md-12 mb-3">
                            <h5 style="font-weight: bold;">Assigned Voucher</h5>
                        </div>
                    </div>
                    <div  style="border:  1px solid lightgrey; padding-top: 15px; ">
                    <?php 
                    if ($countVouchers<=0) {
                        ?>
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                
                            </div>
                            <div class="col-md-6 mb-3">
                                <h4>No Record Found!</h4>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-row" id="delete0" style="padding-left: 3px; padding-right: 15px;">
 <input type="text" class="form-control" id="vouchernumber" name="vouchernumber" placeholder="" value="" hidden="true">
                     <?
                    
                    for ($i=0; $i <$countVouchers; $i++) { 
                        
                            $voucherId = $fetchData4->rows[$i]['voucher_id'];
                            $description = $fetchData4->rows[$i]['description'];
                            $claimFinalsettlementId = $fetchData4->rows[$i]['claim_finalsettlement_id'];

                    ?>    
                    <div class="col-md-5 mb-3">
                        <label class="form-label" >Voucher Number</label>
                        <input type="text" class="form-control" id="" placeholder="" value="<?php echo $voucherId ?>" disabled="true">
                       
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label">Description</label>
                        <input type="text" class="form-control" id="" placeholder="" value="<?php echo $description ?>" disabled="true">
                    </div>
                   <div class="col-md-1 mb-3" style="padding-left: 3px; padding-right: 15px;">
                        <label class="form-label" >Action</label>
                        <button type="button" onclick="Deletevoucher('<?php echo $claimFinalsettlementId ?>')" class="btn btn-danger"  id=""><i class="fa fa-times"></i> </button>
                    </div>
                    <?php 
                    } 
                    ?>
                    </div>
                    </div> 
                    <div style="margin-top: 20px;"></div>

                    <div class="form-row" >
                        <div class="col-md-12 mb-3">
                            <h5 style="font-weight: bold;">Add More Voucher</h5>
                        </div>
                    </div>
                    <div id="dynamic" style="border:  1px solid lightgrey; padding-top: 15px; margin-top: 0px;">
                    <div class="form-row" id="delete0" style="padding-left: 3px; padding-right: 15px;">
                    <div class="col-md-5 mb-3">
                        <label class="form-label" >Voucher Number</label>
                        <input type="text" class="form-control" id="Quality0" name="Quality0" placeholder="" value="" >
                    </div>
                    <div class="col-md-6 mb-3">
                        <label class="form-label" >Description</label>
                        <input type="text" class="form-control" id="Description0" name="Description0" placeholder="" value="" >
                    </div>
                    <div class="col-md-1 mb-3" style="padding-left: 3px; padding-right: 15px;">

                    </div>
                    </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-9 mb-3" >
                           
                        </div>
                        <div class="col-md-3 mb-3" style="margin-top: 10px;  " align="right">
                        <button type="button" onclick="AddVoucher()" style="width: 100px;" class="btn btn btn-dark"><span class="fal fa-save"></span> Save</button>
                        <button type="button" onclick="GetMoreRowsForClaim()"   class="btn btn btn-warning"><span class="fal fa-plus"></span> Add Row</button>
                        </div>
                    </div>

</form>

<form class="form-horizontal" name='finish' id='finish' method='post'>
<input type='hidden' name='ACTION' value='FINISH'>
<input type="text" class="form-control" name="aclaimID" id="aclaimID" placeholder="" hidden="true"  value="<?php echo $claimID ?>" >

                    <div class="form-row" style="margin-top: 30px;">
                        <div class="col-md-8  mb-3">
                        </div>
                        <div class="col-md-4  mb-3" align="right">
                            <button type="button" onclick="finishfun()" style="width: 100px;" class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Finish
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('inprocessclaim')" style="width: 100px;" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
</form>
                    </div>
                        </div>
                    </div>
            

</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">

function AddCalculation()
{
    document.getElementById("voucherortotal").value="calculate";
    // var count = document.getElementById("count5").value;
    grandTotal=document.getElementById('grandSum').value;
    if (grandTotal=="") {
        calculate();
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addvoucher');   
        PostAjaxScreen("addvoucher",form,callfunc);
    }
    else{
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addvoucher');   
        PostAjaxScreen("addvoucher",form,callfunc);
    }
    //     for (var i = 0; i <= count; i++) {
    //         var pricePerItem = document.getElementById("pricePerItem"+i).value;
    //         var totalPrice = document.getElementById("totalPrice"+i).value;
    //         //alert(quantity+description);
    //         if (pricePerItem!="" || totalPrice!="") {
                
        //     }
        // }
        
}


function AddVoucher() {
    document.getElementById("voucherortotal").value="voucher";
    var count = document.getElementById("count2").value;

        for (var i = 0; i <= count; i++) {
            var quantity = document.getElementById("Quality"+i).value;
            var description = document.getElementById("Description"+i).value;
            //alert(quantity+description);
            if (quantity=="" || description=="") {
                alert("Null value!!");
                return false;
            }
        }
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addvoucher');   
        PostAjaxScreen("addvoucher",form,callfunc);
}
function finishfun() {
    if(confirm("Are you sure you want to finish this claim?"))
    {
        var callfunc={onSuccess:NextPage2};
        form= document.getElementById('finish');   
        PostAjaxScreen("finish",form,callfunc);
    }
}
function NextPage2(){
      LoadAjaxScreen("completedclaims");
}
</script>

<script type="text/javascript">
function Deletevoucher(id) {
        if(confirm("Are you sure you want to Delete?"))
        {
        document.getElementById('vouchernumber').value=id;
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addvoucher');   
        PostAjaxScreen("addvoucher",form,callfunc);
        }
}
function NextPage1(){
      LoadAjaxScreen("finalsettlement&id=<?php echo $claimID ?>");
}

    var id = document.getElementById('count5').value;
  //  alert(id);
for (var i = 0; i < id; i++) {
$("#pricePerItem"+i).keypress(function(e){
var keyCode = e.keyCode || e.which;
var regex = /^[0-9 ]+$/;
var isValid = regex.test(String.fromCharCode(keyCode));
if (!isValid) {
   return false
   }
if( $("#pricePerItem"+i).val().length >10 ) {
        return false;
   }
});

// $("#totalPrice"+i).keypress(function(e){
// var keyCode = e.keyCode || e.which;
// var regex = /^[ ]+$/;
// var isValid = regex.test(String.fromCharCode(keyCode));
// if (!isValid) {
//    return false
//    }
// });  

}

// $("#grandSum").keypress(function(e){
// var keyCode = e.keyCode || e.which;
// var regex = /^[ ]+$/;
// var isValid = regex.test(String.fromCharCode(keyCode));
// if (!isValid) {
//    return false
//    }
// });  

function calculate()
{
    var id = document.getElementById('count5').value;
  //  alert(id);
    var totalSum = 0;
    for (var i = 0; i < id; i++) {
        totalSum = totalSum+ + document.getElementById("totalPrice"+i).value;
    }
    var discount = document.getElementById('discount').value;
    var grandTotal = totalSum-discount;
    document.getElementById('grandSum').value=grandTotal;
}
function countValues(letters)
{
    var w=letters;
    //var c = $("#pricePerItem"+letters).val().length;
   // alert(c);
    // if( $("#pricePerItem"+w).val().length >9 ) {
    //     var num = document.getElementById('pricePerItem'+w).value;
    //     var res = num.substring(0, 9);
    //     document.getElementById('pricePerItem'+w).value=res;   
    // }
    // else{
       // alert(w);
        var pricePerItem = document.getElementById('pricePerItem'+w).value;
       // alert(pricePerItem);
        var Approved = document.getElementById('Approved'+w).value;
       // alert(Approved);
        var total = pricePerItem * Approved;
       // alert(total);

        document.getElementById("totalPrice"+w).value=total;
        document.getElementById("grandSum").value="";
   // }
}

$("#Quality0").keypress(function(e){
var keyCode = e.keyCode || e.which;
var regex = /^[A-Za-z0-9-  ]+$/;
var isValid = regex.test(String.fromCharCode(keyCode));
if (!isValid) {
   return false
   }
if( $("#Quality0").val().length >25 ) {
        return false;
   }
});  

$("#Description0").keypress(function(e){
var keyCode = e.keyCode || e.which;
var regex = /^[A-Za-z0-9-  ]+$/;
var isValid = regex.test(String.fromCharCode(keyCode));
if (!isValid) {
   return false
   }
if( $("#Description0").val().length >25 ) {
        return false;
   }
});  


</script>

<script type="text/javascript">
    var count = 0;
    function GetMoreRowsForClaim()
    {   
        for (var i = 0; i <=count; i++) {
            var voucherQuality = document.getElementById('Quality'+i).value;
            var voucherDescription = document.getElementById('Description'+i).value;
            
            if (voucherQuality=="" || voucherDescription=="") {
                alert('First Fill Value Before Adding Another Voucher!');
                return false;
            }
        }
        count++;
        var data = '<div class="form-row" id="delete'+count+'" style="padding-left: 3px; padding-right: 3px;">';
        data = data +'<div class="col-md-5 mb-3">';
        data = data +'<label class="form-label" >Voucher Number</label>';
        data = data +'<input type="text" class="form-control" id="Quality'+count+'" name="Quality'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-6 mb-3">';
        data = data +'<label class="form-label" >Description</label>';
        data = data +'<input type="text" class="form-control" id="Description'+count+'" name="Description'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-1 mb-3" style="padding-left: 3px; padding-right: 15px;">';
        data = data +'<label class="form-label" >Action</label>';
        data = data +'<button type="button" class="btn btn-danger"  id="del'+count+'" onclick="DelRowForClaim('+count+')"><i class="fa fa-times"></i> </button>';
        data = data +'</div>';
        data = data +'</div>';


        $("#dynamic").append(data);

        document.getElementById("count2").value=count;

        $("#Quality"+count).keypress(function(e){
            var keyCode = e.keyCode || e.which;
            var regex = /^[A-Za-z0-9-  ]+$/;
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
               return false
               }
            if( $("#Quality"+count).val().length >25 ) {
                    return false;
               }
            });  

            $("#Description"+count).keypress(function(e){
            var keyCode = e.keyCode || e.which;
            var regex = /^[A-Za-z0-9-  ]+$/;
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
               return false
               }
            if( $("#Description"+count).val().length >35 ) {
                    return false;
               }
        });  
    }



    function DelRowForClaim(count)
    {
  
    if(confirm("Are you sure you want to Delete?"))
        {
     var el = count;
     $('#delete'+el).remove();
    }
    }
</script>



<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>

