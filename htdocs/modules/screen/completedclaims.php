

<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    
   // $userId = $mysession->getValue("userid");

    $claimNoSearch=$_GET['claimno'];
    $searchFrom=$_GET['from'];
    $searchTo=$_GET['to'];

    if ($claimNoSearch!="") {
        $viewClaims=new BL_ViewClaims();
        $fetchData=$viewClaims->ViewCompletedClaimsClaimNo($claimNoSearch);
        $count =  $fetchData->count;
    }
    else if($searchFrom!="" && $searchTo!="")
    {

        $searchFrom = date('Y-m-d',strtotime($searchFrom));
        $searchTo = date('Y-m-d',strtotime($searchTo));
        $searchTo = str_replace('-', '/', $searchTo);
        $searchTo = date('Y-m-d',strtotime($searchTo . "+1 days"));
        $viewClaims=new BL_ViewClaims();
        $fetchData=$viewClaims->ViewCompletedClaimsDate($searchFrom,$searchTo);
        $count =  $fetchData->count;
    }
    else
    {
        $viewClaims=new BL_ViewClaims();
        $fetchData=$viewClaims->ViewCompletedClaims();
        $count =  $fetchData->count;
    }

    

?>



<main id="js-page-content" role="main" class="page-content">
<form class="form-horizontal" name='searchdateorclaim' id='searchdateorclaim' method='post'>
<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Closed Claims
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
    <div class="form-row">   
        <!-- <div class="col-md-2 mb-3" >
            <label class="form-label"> From Date</label>
            <div class="input-group" style=" ">
                
                <input type="date" class="form-control "  onchange="selectdate()" placeholder="" id="fromdate">
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-exclamation"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2 mb-3" >
            <label class="form-label"> To Date</label>
            <div class="input-group" style=" ">
                
                <input type="date" class="form-control " placeholder="" id="todate">
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-exclamation"></i>
                    </span>
                </div>
            </div>
        </div> -->
        <div class="col-md-4 mb-3" >
            <label class="form-label">Select Dates</label>
            <div class="input-daterange input-group" id="datepicker-5">
                <input type="text" class="form-control" readonly="" name="fromdate" placeholder="From Date" id="fromdate">
                <div class="input-group-append input-group-prepend">
                    <span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
                </div>
                <input type="text" class="form-control" readonly="" name="todate"  placeholder="To Date" id="todate">
            </div>
        </div>
       <!--  <div class="col-md-3 mb-3" >
            <label class="form-label"> From Date</label>
            <div class="input-group">
                <input type="text" class="form-control " placeholder="To Date" id="fromdate">
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-plus"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3" >
            <label class="form-label"> To Date</label>
            <div class="input-group">
                <input type="text" class="form-control " placeholder="From Date" id="todate">
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-plus"></i>
                    </span>
                </div>
            </div>
        </div> -->
        <div class="col-md-2 mb-3" >
            <label class="form-label"> Claim Number</label>
            <div class="input-group" style=" ">            
                <input type="text" class="form-control " placeholder="" id="claimno">
            </div>
        </div>
        <div class="col-md-3  mb-3" style="margin-top: 23px;">
            <button type="button" onclick="DateOrClaimNoSearch()" class="btn btn-med btn-warning">
                <span class="fal fa fa-search mr-1"></span>
                Search
            </button>
        </div>
        
    </div>
    <hr>
 <!-- datatable start -->
                <table   class="table table-bordered table-hover table-striped w-100" id="dt-basic-example6">
                    <thead style="background-color: lightgrey;">
                        <tr style="text-align: center;">
                            <th>Serial No</th>
                            <th>Claim Number</th>
                            <th>Raised By</th>
                            <th>Raised By Mobile</th>
                            <th>Raised By Address</th>
                            <th>Raised By Market</th>

                            <th>Purchased From</th>
                            <th>Account Of</th>
                            <th>Claim Type</th>
                            <th>Claim Status</th>
                            <th>Date</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?

                        for ($i=0; $i <$count; $i++) { 
                              //for
                                  $claimID = $fetchData->rows[$i]['claim_id'];
                                $claimNo = $fetchData->rows[$i]['claim_no'];
                                $dealerID = $fetchData->rows[$i]['user_id'];
                                
                                $fetchData1=$viewClaims->FetchDealerName($dealerID);
                                $dealerCategoryName = $fetchData1->rows[0]['dealer_category_name'];
                                $claimName = $fetchData1->rows[0]['full_name'];
                                $mobileNo = $fetchData1->rows[0]['mobile_no'];
                                $address = $fetchData1->rows[0]['address'];
                                $market = $fetchData1->rows[0]['market'];

                                $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
                                $fetchData1=$viewClaims->FetchDealerName($purchasedFrom);
                                $purchasedFrom = $fetchData1->rows[0]['full_name'];
                                $purchasedFromDesig = $fetchData1->rows[0]['dealer_category_name'];

                                $accountOf = $fetchData->rows[$i]['account_of'];
                                $fetchData1=$viewClaims->FetchDealerName($accountOf);
                                $accountOf = $fetchData1->rows[0]['full_name'];
                                $accountOfDesig = $fetchData1->rows[0]['dealer_category_name'];
                                
                                $remarks = $fetchData->rows[$i]['claim_type'];
                                $claimStatus = $fetchData->rows[$i]['claim_status'];
                                $Date = $fetchData->rows[$i]['created_at'];                      
                                //$Date=substr($Date,0,11);
                                //$Date=date_create($Date);                     
                                //$Date = date_format($Date,"d-M-Y");
                                $Date = date('d-M-Y',strtotime($Date));
                                $dealerCategoryName=strtolower($dealerCategoryName);
                                $dealerCategoryName=ucfirst($dealerCategoryName);
                                $purchasedFromDesig=strtolower($purchasedFromDesig);
                                $purchasedFromDesig=ucfirst($purchasedFromDesig);
                                $accountOfDesig=strtolower($accountOfDesig);
                                $accountOfDesig=ucfirst($accountOfDesig);
                        ?>
                        

                        <tr>
                            <td style="text-align: center;"><?=$i+1?></td>
                            <td style="text-align: center;"><?=$claimNo?></td>
                            <td><?=ucfirst($claimName)." (".$dealerCategoryName.")"?></td>
                            <td style="text-align: center;"><?=$mobileNo?></td>
                            <td style="text-align: center;"><?=$address?></td>
                            <td style="text-align: center;"><?=$market?></td>
                            <td><?=ucfirst($purchasedFrom)." (".$purchasedFromDesig.")"?></td>
                            <td><?=ucfirst($accountOf)." (".$accountOfDesig.")"?></td>
                            <td style="text-align: center;"><?=ucfirst($remarks)?></td>
                            <td style="text-align: center;"><?php 
                            if ($claimStatus=="closed") {
                              ?>
                              <div style="background-color:#3cabf0; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Closed
                              </div>
                              <?php
                            }
                            ?>
                            </td>
                            <td style="text-align: center;"><?=$Date?></td>
                            <td align="center">
                            <button type="button" onClick="LoadAjaxScreen('claimdetails&id=<?php echo $claimID ?>&page=4')" class="btn btn-sm btn-dark">
                            <span class="fal  fa-eye mr-1"></span>
                            </button>
                            </td>
                        </tr>

                        <?php 
                        }
                        ?>
                       <!--  <tr>
                            <td>voc-13-ad</td>
                            <td>15-Mar-2019</td>
                            <td>Garrett Winters</td>
                            <td></td>
                            <td></td>
                            <td><div style="background-color:red; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    None
                                </div></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-dark">
                            <span class="fal  fa-eye mr-1"></span>
                            </button></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-warning" disabled="true" >
                            <span class="fal  fa-edit mr-1"></span>
                            </button></td>
                        </tr>
                        <tr>
                            <td>voc-05-az</td>
                            <td>10-Jul-2019</td>
                            <td>Brielle Williamson</td>
                            <td>Humza</td>
                            <td>3153463545434</td>
                            
                            <td><div style="background-color:lightgrey; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Pending
                                </div></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-dark" onClick="LoadAjaxScreen('claimdetails')">
                            <span class="fal fa-eye mr-1"></span>
                            </button></td>
                            <td align="center"><button type="button" id="modal-btn" class="btn btn-sm btn-warning">
                            <span class="fal  fa-edit mr-1"></span>
                            </button></td>
                        </tr> -->
                       
                    </tbody>
                </table>

</div>
</div>


</div>
</div>


</form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">

// var runDatePicker = function()
// {
//     $('#todate').datepicker(
//     {
//         // todayHighlight: true,
//         // orientation: "bottom left",
//         // templates: controls
//     });
//     $('#fromdate').datepicker(
//     {
//         // todayHighlight: true,
//         // orientation: "bottom left",
//         // templates: controls
//     });
// }

// $(document).ready(function()
// {
//     runDatePicker();
// });


// function SetMinToDate(){
//     var x = document.getElementById("joiningdateFrom").value;
//     document.getElementById("joiningdateTo").min = x;
//     document.getElementById("joiningdateTo").max = x+29;
// }


function selectdate()
{
   
    var fromdate = document.getElementById("fromdate").value;
    document.getElementById("todate").min=fromdate;

}

function DateOrClaimNoSearch() {
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    var claimNo = document.getElementById("claimno").value;

    if (claimNo!="") {
        var callfunc={onSuccess:NextPageClaim};
        form= document.getElementById('searchdateorclaim');   
        PostAjaxScreen("searchdateorclaim",form,callfunc);
    }
    else if(fromdate!="" && todate!=""){
        if (fromdate!="" && todate!="") {
                var callfunc={onSuccess:NextPageDate};
                form= document.getElementById('searchdateorclaim');   
                PostAjaxScreen("searchdateorclaim",form,callfunc);
        }
        else
        {
            alert('Please Select The Dates!');
        }
    }
    else
    {
        alert('Please Select Date or Claim Number!');
    }
}

function NextPageDate(){
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    LoadAjaxScreen("completedclaims&from="+fromdate+"&to="+todate);
}

function NextPageClaim(){
    var claimNo = document.getElementById("claimno").value;
    LoadAjaxScreen("completedclaims&claimno="+claimNo);
}
</script>

<!-- <script type="text/javascript">
var runfromdate = function()
{
    $('#fromdate').fromdate(
    {
        orientation: "bottom left",
        todayHighlight: true,
        templates: controls
    });

    $('#todate').fromdate(
    {
        orientation: "bottom left",
        todayHighlight: true,
        templates: controls
    });
}

$(document).ready(function()
{
    runfromdate();
});
</script> -->


<script src="js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>
    // Class definition

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    // var runDatePicker = function()
    // {

    //     $('#fromdate').datepicker(
    //     {
    //         orientation: "bottom right",
    //         todayHighlight: true,
    //         templates: controls
    //     });

    //     $('#todate').datepicker(
    //     {
    //         orientation: "bottom right",
    //         todayHighlight: true,
    //         templates: controls
    //     });
        
    // }

    // $(document).ready(function()
    // {
    //     runDatePicker();
    // });

$('#datepicker-5').datepicker(
{
    todayHighlight: true,
    format: 'dd-M-yyyy',
    endDate: new Date(),
    startDate: '01-jan-2000',
    templates: controls
});

$("#claimno").keypress(function(e){
var keyCode = e.keyCode || e.which;
var regex = /^[A-Za-z0-9-  ]+$/;
var isValid = regex.test(String.fromCharCode(keyCode));
if (!isValid) {
   return false
   }
if( $("#claimno").val().length >25 ) {
        return false;
   }
});

//     $("#fromdate").datepicker({
//           prevText: '<<',
//           nextText: '>>',
//           changeMonth: true,
//           changeYear: true,
//           format: 'dd-M-yyyy',
//           // maxDate: new Date(),
//         onchange: function (selected) {
//             //console.log(selected);
//         }
//     });

//     $("#fromdate").datepicker().on('changeDate', function (selected) {
//         var dt = new Date(selected.dates);
//         dt.setDate(dt.getDate() - 1);
//       $("#todate").datepicker("option", 'endDate', dt.toISOString());
//       console.log(dt.toISOString());
// });

//     $("#todate").datepicker({
//           prevText: '<<',
//           nextText: '>>',
//           changeMonth: true,
//           changeYear: true,
//           format: 'dd-M-yyyy',
//          // maxDate: new Date(),
//         onclick: function (selected) {
//             console.log(selected);
        
//             // var dt = new Date(selected);
//             // dt.setDate(dt.getDate() - 1);
//             // $("#fromdate").datepicker("option", "maxDate", dt);
//         }
//     });

//      $("#todate").datepicker().on('changeDate', function (ev) {
    
// });


</script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>

<script>
 $(document).ready(function()
            {
                // Setup - add a text input to each footer cell
                $('#dt-basic-example6 thead tr').clone(true).appendTo('#dt-basic-example6 thead');
                $('#dt-basic-example6 thead tr:eq(1) th').each(function(i)
                {
                    var title = $(this).text();
                    $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');

                    $('input', this).on('keyup change', function()
                    {
                        if (table.column(i).search() !== this.value)
                        {
                            table
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                });

                var table = $('#dt-basic-example6').DataTable(
                {
                    responsive: true,
                    orderCellsTop: true,
                    lengthChange: false,
                  //  fixedHeader: true,

                    dom:
                        /*  --- Layout Structure 
                            --- Options
                            l   -   length changing input control
                            f   -   filtering input
                            t   -   The table!
                            i   -   Table information summary
                            p   -   pagination control
                            r   -   processing display element
                            B   -   buttons
                            R   -   ColReorder
                            S   -   Select

                            --- Markup
                            < and >             - div element
                            <"class" and >      - div with a class
                            <"#id" and >        - div with an ID
                            <"#id.class" and >  - div with an ID and a class

                            --- Further reading
                            https://datatables.net/reference/option/dom
                            --------------------------------------
                         */
                        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-12 d-flex align-items-center justify-content-end'lB>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    buttons: [
                        /*{
                            extend:    'colvis',
                            text:      'Column Visibility',
                            titleAttr: 'Col visibility',
                            className: 'mr-sm-3'
                        },*/
                        {
                            extend: 'pdfHtml5',
                            text: 'PDF',
                            titleAttr: 'Generate PDF',
                            className: 'btn-outline-danger btn-sm mr-1'
                        },
                        {
                            extend: 'excelHtml5',
                            text: 'Excel',
                            titleAttr: 'Generate Excel',
                            className: 'btn-outline-success btn-sm mr-1'
                        },
                        {
                            extend: 'csvHtml5',
                            text: 'CSV',
                            titleAttr: 'Generate CSV',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'copyHtml5',
                            text: 'Copy',
                            titleAttr: 'Copy to clipboard',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            titleAttr: 'Print Table',
                            className: 'btn-outline-primary btn-sm'
                        }
                    ]
                });

            });
        </script>


