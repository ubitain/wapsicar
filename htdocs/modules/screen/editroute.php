<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/addstuff.php");



$blStuff = new BL_AddStuff();
$blUser = new BL_User();
$route_id = $_REQUEST['route_id'];
$cityInfo = $blUser->GetCity();
$routeInfo = $blStuff->GetRouteById($route_id);
$id=$routeInfo->rows[0]['route_id'] ;
    $fromcity=$routeInfo->rows[0]['from_city'] ;
  $tocity=$routeInfo->rows[0]['to_city'];
  $rates=$routeInfo->rows[0]['rates'];
  

?>
<main id="js-page-content" role="main" class="page-content">
<h1>Edit Route</h1>
<form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
<input type='hidden' name='ACTION' value='EDITROUTE'>
<input type='hidden' name='routeid' value='<?=$route_id?>'>


<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
      

          <div class="form-row">
            <div class="col-md-12 mb-12">

                        
                        <label class="form-label" >From City <span style="color: red"> * </span></label>
                        <select id="from_city" name="from_city" class="form-control"  >
                                      <option value="0" >Select City</option>
                                  
                                     <?php
                              for($i=0; $i<$cityInfo->count;$i++){
                               $selected = $fromcity == $cityInfo->rows[$i]['city_id'] ? 'selected' : '';
                                  ?>
                                <option <?=$selected?> value="<?=$cityInfo->rows[$i]['city_id']?>"  ><?=$cityInfo->rows[$i]['city_name']?></option> 
                    <?php }?>
                       </select>
                    </div>
          </div>
         <div class="form-row">
            <div class="col-md-12 mb-12">
                <label class="form-label" >To City <span style="color: red"> * </span></label>
                        <select id="to_city" name="to_city" class="form-control"  >
                                      <option value="0" >Select City</option>
                                  
                                     <?php
                              for($i=0; $i<$cityInfo->count;$i++){
                               $selected = $tocity == $cityInfo->rows[$i]['city_id'] ? 'selected' : '';
                                  ?>
                                <option <?=$selected?> value="<?=$cityInfo->rows[$i]['city_id']?>"  ><?=$cityInfo->rows[$i]['city_name']?></option> 
                    <?php }?>
                                    </select>
                    </div>
          </div>
           <div class="form-row">
            <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">Rates <span style="color: red"> * </span></label>
                        <input type="text" class="form-control" id="rates" name="rates" multiple="true" placeholder="Rates"    value="<?=$rates?>"onkeypress='return event.charCode >= 48 && event.charCode <= 57'maxlength="15"pattern="^[\d,]+$" >
                    </div>
          </div>
        

<div style="margin-top: 20px;"></div>
<div class="form-row">

<div class="col-md-12  mb-12" style="margin-left: 170px">

    <button type="button" onclick='BackRoute()' class="btn btn-med btn-dark">
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
    <button type="button" onclick='EditCity()' class="btn btn-med btn-warning" style="background: #ff8000">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Update
    </button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>



<script type="text/javascript">


function EditCity() {

var check=AddRoutesCheck();
if(check==true){
   if(confirm("Are you sure you want to update this Route?"))
    {
    var callfunc={onSuccess:NextPage};
    form= document.getElementById('adduserform');   
    PostFileAjaxScreen("adduserform",form,callfunc);
    }
}

}
function ValidateLeadData()
{
    name=document.getElementById('name').value;
    var illegalCharacters = name.match(/[^a-zA-Z- ]/g);
    if (illegalCharacters) {
        $("#name").focus();
        alert('Wrong Name!!');
        return false;
    }
    email=document.getElementById('email').value;
    var emailregex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (emailregex.test(email) == false){
        alert("Wrong Email! (Hint:abc@gmail.com)");
        return false;
    }
}

function NextPage(){
 $('#myModal').modal('toggle');
LoadAjaxScreen("addroutes");
}
function BackRoute(){
 $('#myModal').modal('toggle');
LoadAjaxScreen("addroutes");
} 

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;
var fromdate = today;
document.getElementById("joiningdate").max = fromdate;
//////////////////////////////


$("#name").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#name").val().length >35 ) {
            return false;
       }



});

$("#cnic").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#cnic").val().length >12 ) {
            return false;
       }



});

$("#email").keypress(function(e){

      if( $("#email").val().length >50 ) {
            return false;
        }

  });

$("#phonenumber").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#phonenumber").val().length >10 ) {
            return false;
       }
});

$("#registration").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#registration").val().length >35 ) {
            return false;
       }
});

$("#sapcode").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#sapcode").val().length >35 ) {
            return false;
       }
});

function AddRoutesCheck(){
    var a=document.getElementById('from_city').value;
    var b=document.getElementById('to_city').value;
    rates=document.getElementById('rates').value;

        

    if(a=='0')
  {
    alert("Please select From city");
    return false;
  }
  if(b=='0')
  {
    alert("Please select To city");
    return false;
  }

  if(a == b)
  {
    alert("Both Are Same ");
    return false;
  }
  if (rates == ""|| rates == 0 ) {
            alert("Rates must be filled");
          return false;
        }
  return true;
   

     
}

</script>
