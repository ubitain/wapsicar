<?php
 require_once MODULE."/class/bllayer/user.php";
 $blUser = new BL_User();
 $agentId = $_REQUEST['agent_id'];
 $data = array('id'=>$agentId);
//  print_r($data);
 
    $fetchData = $blUser->GetAgentDriver($data);
    // print_r_pre($fetchData);
    // die();
    
?>

<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Payment And Drivers Of Agent
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
 <!-- datatable start -->
               <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr align="center">
                         
                           
                            <th>Driver Name</th>
                            <th>Driver Id </th>
                            <th>Transaction Number </th>
                            <th>Payment</th>
                            <th>Payment Date</th>
                            
                    
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            for ($i=0; $i <$fetchData->count; $i++) { 
                                
                                $name = $fetchData->rows[$i]['full_name']=='' ? 'Unknow':$fetchData->rows[$i]['full_name'];
                                $userId = $fetchData->rows[$i]['driver_id'];
                                $transactionNumber = $fetchData->rows[$i]['transaction_number'];
                                $payment =  $fetchData->rows[$i]['payment']; 
                                $created_at = $fetchData->rows[$i]['created_at'];                          
                                $created_at=substr($created_at,0,10);                 
                                //echo "here...";
                        ?>
                                <tr>
                                <td style="text-align: center;"><?=$name?></td>
                                <td style="text-align: center;"><?=$userId ?></td>
                                <td style="text-align: center;"><?=$transactionNumber ?></td>
                                <td style="text-align: center;"><?=$payment?></td>
                                <td style="text-align: center;"><?=$created_at?></td>
                                </tr>
                            <?php
                                }
                            ?>
                       
                    </tbody>
                </table>
</div>
</div>

</div>
</div>
</div>


</main>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>
<script type="text/javascript">
    function Delete(recordId)
{
  alert(recordId);
  var r = confirm("Are you sure you want to delete?");
  if(r==true)
  {
  o = new Array(recordId);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=delete&function=DeleteBranch&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteBranchCallBack});
  }
  else
  {
    return;
  }
}
function DeleteBranchCallBack(response)
{
    alert('Delete Sucessfully');
  LoadAjaxScreen("viewagent");
}
function ReportError(res)
{

}
</script>