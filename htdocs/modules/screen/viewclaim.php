<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
View Claim
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<div class="row">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">
    
    <div class="panel-container show">
        <div class="panel-content" style="margin-top: 50px;">
             <!-- datatable start -->
                            <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                <thead>
                                    <tr>
                                        <th>Claimer Name</th>
                                        <th>Claimer Type</th>
                                        <th>City</th>
                                        <th>View Details</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>
                                        <button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button>
                                        </td>
                                        <td>
                                            <div style="background-color:lightblue; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div>
                                        </td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Garrett Winters</td>
                                        <td>Mechanic</td>
                                        <td>Tokyo</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" >
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>

                                        <td>Brielle Williamson</td>
                                        <td>Distributor</td>
                                        <td>New York</td>
                                        <td><button type="button" class="btn btn-sm btn-dark" onClick="LoadAjaxScreen('claimdetails')">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" onClick="LoadAjaxScreen('actionassignmentweight')">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Unity Butler</td>
                                        <td>Marketing Designer</td>
                                        <td>San Francisco</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" >
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Hope Fuentes</td>
                                        <td>Secretary</td>
                                        <td>San Francisco</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Vivian Harrell</td>
                                        <td>Financial Controller</td>
                                        <td>San Francisco</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" >
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Timothy Mooney</td>
                                        <td>Office Manager</td>
                                        <td>London</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Jackson Bradshaw</td>
                                        <td>Director</td>
                                        <td>New York</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" >
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Olivia Liang</td>
                                        <td>Retailer</td>
                                        <td>Singapore</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightgrey; text-align: center;
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Pending
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" >
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Bruno Nash</td>
                                        <td>Dealer</td>
                                        <td>London</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>Thor Walton</td>
                                        <td>Developer</td>
                                        <td>New York</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Finn Camacho</td>
                                        <td>Retailer</td>
                                        <td>San Francisco</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Serge Baldwin</td>
                                        <td>Data Coordinator</td>
                                        <td>Singapore</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Zenaida Frank</td>
                                        <td>Dealer</td>
                                        <td>New York</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                    <tr>
                                        <td>Donna Snider</td>
                                        <td>Distributor</td>
                                        <td>New York</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">
                                        <span class="fal  fa-bars mr-1"></span>
                                        Details
                                        </button></td>
                                        <td><div style="background-color:lightblue; text-align: center; 
                                             padding-top: 7px; padding-bottom: 7px;
                                            ">
                                                Completed
                                            </div></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" disabled="true">
                                        <span class="fal  fa-edit mr-1"></span>
                                        Action
                                        </button></td>
                                    </tr>
                                </tbody>
                            </table>
 
        </div>
    </div>




</div>
</div>
</div>


 <div class="modal fade" id="default-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fal fa-times"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
</div> 
</main>


<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
    responsive: true,
    dom:
        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    buttons: [
        {
            
        },
        {
            
        },
        {
           
        },
        {
           
        },
        {
            
        },
        {
            
        },
        {
            
        }

    ],
    select: false
});
});
</script>