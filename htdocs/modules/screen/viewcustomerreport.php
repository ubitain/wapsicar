<?php
 require_once MODULE."/class/bllayer/user.php";
 require_once MODULE."/class/bllayer/viewuser.php"; 
 $blUser = new BL_User();
 $blViewUser = new BL_ViewUser();
 $fetchData = $blUser->CustomerWithBookingPayment();
//   print_r_pre($fetchData);
    
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
 Customer Booking Payment
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
 <!-- datatable start -->
               <table id="customer-payment" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr align="center">
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Customer CNIC</th>
                            <th>Price</th>
                            <th>Distance</th>
                            <th>Status</th>
                    
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            for ($i=0; $i <$fetchData->count; $i++) { 
                                //for

                                //print_r_pre($fetchData->rows[$i]);
                                $id = $fetchData->rows[$i]['id'];
                                $userId = $fetchData->rows[$i]['user_id'];
							    $customerName = $fetchData->rows[$i]['full_name'];
								$cnic = $fetchData->rows[$i]['customer_cnic'];
                                $price = $fetchData->rows[$i]['price'];
                                $distance = $fetchData->rows[$i]['approx_distance'];
                                $status =  $fetchData->rows[$i]['status'];                  
                                
                            
                        ?>

                                <tr>
                                 <td align="center"><?php echo $i+1 ?></td>
                                <td align="center"><?=$customerName?></td>
                                <td align="center"><?=$cnic?></td>
                                <td align="center"><?=$price?></td>
                                <td align="center"><?=$distance?></td>
                                <td align="center"><?=$status?></td>
                               </tr>
                            <?php
                                }
                            ?>
                       
                    </tbody>
                </table>
</div>
</div>

</div>
</div>
</div>


</main>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('customer-payment').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>