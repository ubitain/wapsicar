<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Action Against Claim
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>


            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h3>Consignment Number/Weight</h3>
                                            </div>
                                    </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claim Voucher ID</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="voc-05-az" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claimer Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Brielle Williamson" >
                                            </div>
                                    </div>
                    <div class="frame-wrap w-100">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Assignment Number
                                        <span class="ml-auto">
                                            <span class="collapsed-reveal">
                                                <i class="fal fa-minus-circle text-danger"></i>
                                            </span>
                                            <span class="collapsed-hidden">
                                                <i class="fal fa-plus-circle text-success"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Consignment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="A-5312-3" >
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6  mb-3">
                                                <button type="button" class="btn btn-med btn-warning">
                                                    <span class="fal fa-check mr-1"></span>
                                                    Save
                                                </button>
                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <a href="javascript:void(0);" class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Weight
                                        <span class="ml-auto">
                                            <span class="collapsed-reveal">
                                                <i class="fal fa-minus-circle text-danger"></i>
                                            </span>
                                            <span class="collapsed-hidden">
                                                <i class="fal fa-plus-circle text-success"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Weight (KG)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="55" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Weight (KG)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="50" >
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6  mb-3">
                                                <button type="button" class="btn btn-med btn-warning">
                                                    <span class="fal fa-check mr-1"></span>
                                                    Save
                                                </button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-8  mb-3">
                        </div>
                        <div class="col-md-4  mb-3">
                            <button type="button" onClick="LoadAjaxScreen('inprocessclaim')" class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Proceed
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('pendingclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>