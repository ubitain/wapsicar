<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Action Against Claim
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>


            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" style="margin-bottom: -30px;">
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <h3 style="font-weight: bold;">Step 1</h3>
                                    </div>
                                    <div class="col-md-2 mb-3" >
                                       <h3 style="margin-left: 15px;">Step 2</h3>
                                    </div>
                                    <div class="col-md-1 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <h3 style="margin-left: -15px;">Step 3</h3>
                                    </div>                                    <div class="col-md-2 mb-3">
                                        <h3>Step 4</h3>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                         <img src="\img\steps\step1yellow.png" height=" 50px" width="50px" style="margin-top: -33px; margin-left: 140px;" onClick="LoadAjaxScreen('actionassignmentweight')" >
                                    </div>
                                    <div class="col-md-1 mb-3">
                                          <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 90px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                       <img src="\img\steps\step2.png" height=" 40px" width="75px" style="margin-top: -24px; margin-left: 80px;" onClick="LoadAjaxScreen('acceptrejectparts')">
                                    </div>
                                    <div class="col-md-1 mb-3">
                                         <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 50px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <img src="\img\steps\step3.png" height=" 55px" width="55px" style="margin-top: -35px; margin-left: 50px;" onClick="LoadAjaxScreen('export')">
                                    </div>
                                    <div class="col-md-1 mb-3">
                                         <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 10px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <img src="\img\steps\step4.png" height=" 50px" width="50px" style="margin-top: -30px; margin-left: 10px;" onClick="LoadAjaxScreen('finalsettlement')">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                       <p style="margin-left: -10px; font-weight: bold;">Consignment Number/Weight</p>

                                    </div>
                                    <div class="col-md-2 mb-3" >
                                        <p style="margin-left: 10px;">Accept/Reject</p>
                                    </div>
                                    <div class="col-md-1 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <p style="margin-left: -5px;">Export</p>
                                        
                                    </div>                                    <div class="col-md-2 mb-3">
                                        <p style="margin-left: -10px;">Final Settlement</p>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h3>Consignment Number/Weight</h3>
                                            </div>
                                    </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claim Voucher ID</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="1102" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claimer Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="Brielle Williamson" >
                                            </div>
                                    </div>
                    <div class="frame-wrap w-100">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <a href="javascript:void(0);" class="card-title" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Assignment Number
                                        <span class="ml-auto">
                                            <span class="collapsed-reveal">
                                                <i class="fal fa-minus-circle text-danger"></i>
                                            </span>
                                            <span class="collapsed-hidden">
                                                <i class="fal fa-plus-circle text-success"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Consignment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="A-5312-3" >
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6  mb-3">
                                                <button type="button" class="btn btn-med btn-warning">
                                                    <span class="fal fa-check mr-1"></span>
                                                    Save
                                                </button>
                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <a href="javascript:void(0);" class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Weight
                                        <span class="ml-auto">
                                            <span class="collapsed-reveal">
                                                <i class="fal fa-minus-circle text-danger"></i>
                                            </span>
                                            <span class="collapsed-hidden">
                                                <i class="fal fa-plus-circle text-success"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Weight (KG)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="55" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Weight (KG)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder=""  value="50" >
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6  mb-3">
                                                <button type="button" class="btn btn-med btn-warning">
                                                    <span class="fal fa-check mr-1"></span>
                                                    Save
                                                </button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-8  mb-3">
                        </div>
                        <div class="col-md-4  mb-3">
                            <button type="button" onClick="LoadAjaxScreen('acceptrejectparts')" class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Proceed
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('viewclaim')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

</main>
<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>