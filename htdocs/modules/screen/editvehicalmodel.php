<?php
// echo("taha");
require_once(MODULE."/class/bllayer/vehicles.php");
$blAddvehicle = new BL_Vehicle();
// $vehId = $_REQUEST['id'];

// echo('parentId : '.$parentId);
// echo '<br>';
// echo('childId : '.$childId);

    $icon ='<i class="fas fa-floppy-o" style="margin-right: 4px"></i>';
    $title = 'Update';
    $buttonName = 'Update';

$getRecord = $blAddvehicle->GetDetailRecord($parentId,$childId);
// echo ($vehId);
// print_r_pre($getRecord);
//die();
?>

<div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <form class="needs-validation" name='vehicalmodelforms' id='vehicalmodelforms' method='post' novalidate>
                          <input type='hidden' name='ACTION' value='EDITVEHICALMODEL'>
                           <input type='hidden' name='childId' value='<?php echo $childId;?>'>


                  <div class="form-row">
                
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Vehicle Name<span class="text-danger">*</span></label>
                      <input readonly class="form-control" value="<?=$getRecord->rows[0]['name']?>" placeholder="Enter Vehicle Name" type="text" id='name' name='name' maxlength="20">
                      <div class="invalid-feedback">
                          Please Enter a Vehicle Model Name.
                      </div>
                    </div> 
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Description<span class="text-danger">*</span></label>
                      <input readonly class="form-control" value="<?=$getRecord->rows[0]['description']?>" placeholder="Enter Vehicle Description" type="text" id='description' name='description' maxlength="20"  required>
                      <div class="invalid-feedback">
                          Please Enter a Description.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Model Name<span class="text-danger">*</span></label>
                      <input class="form-control" value="<?=$getRecord->rows[0]['model_name']?>" placeholder="Enter Vehicle Model" type="text" id='model_name' name='model_name' maxlength="20"  required>
                      <div class="invalid-feedback">
                          Please Enter a Model Name.
                      </div>
                    </div> 
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Description<span class="text-danger">*</span></label>
                      <input class="form-control" value="<?=$getRecord->rows[0]['des']?>" placeholder="Enter Vehicle Model Description" type="text" id='description' name='description' maxlength="20"  required>
                      <div class="invalid-feedback">
                          Please Enter a Model Description.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Car Type<span class="text-danger">*</span></label>
                      <!-- <input class="form-control" value="<?=$getRecord->rows[0]['car_type']?>" placeholder="Enter Vehicle Model Car Type" type="text" id='car_type' name='car_type' maxlength="20"  required> -->
                      <select class="form-control" name="car_type" id="car_type">
                      <option value="">Select Type</option>
                      <?php if($getRecord->rows[0]['car_type']=="hatchback"){ ?>
                                        <option selected value="hatchback">hatchback</option>
                                        <option value="sedan">sedan</option>
                                        <option value="suv">suv</option>
                                        <? } 
                                        elseif($getRecord->rows[0]['car_type'] == "sedan") { ?>
                                          <option  value="hatchback">hatchback</option>
                                        <option selected value="sedan">sedan</option>
                                        <option value="suv">suv</option>
                                          <? } elseif($getRecord->rows[0]['car_type'] == "suv") {?>
                                            <option  value="hatchback">hatchback</option>
                                        <option  value="sedan">sedan</option>
                                        <option selected value="suv">suv</option>
                                            <? }?>
                                    </select>
                      <div class="invalid-feedback">
                          Please Enter a Car Type.
                      </div>
                    </div>
                  </div>

                  <div class="form-row">

                    <div class="col-md-6 p-4" style="text-align: center;"> 
                      <button style="width:130px; background: #ff8000" id="js-add-btn" class="btn btn-primary btn-md " type="button" onclick="Edit('<?=$getVehicle->rows[$i]['id']?>')"><?=$icon?><?=$buttonName?></button>
                      <button style="width:130px;background: #ff8000" class="btn btn-primary btn-md " type="button" onclick='LoadAjaxScreen("viewvehiclemodel")'><i class="fas fa-times-circle" style="margin-right: 4px"></i>Cancel</button>

                    </div> 
                  </div>
                  
                </div>
        </form>
      </div>
    </div>
  </div>

  <script>
   function Edit()
  {
    var r = confirm("Are you sure you want to add?");
    if(r==true)
      {
        
          var obj = {onSuccess:VehicleInfoCallBack};
          form = document.getElementById('vehicalmodelforms');
          PostAjaxScreen("vehicalmodelforms",form,obj);
         
        
     }
  }

  function VehicleInfoCallBack()
  {
    HideBlockDiv();
    LoadAjaxScreen("viewvehiclemodel");
  }
  </script>