<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Action Against Claim
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                  
                        
                        <div class="panel-container show">
                            <div class="panel-content" style="margin-bottom: -30px;">
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <h3 style="">Step 1</h3>
                                    </div>
                                    <div class="col-md-2 mb-3" >
                                       <h3 style="margin-left: 15px;">Step 2</h3>
                                    </div>
                                    <div class="col-md-1 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <h3 style="margin-left: -15px; font-weight: bold;">Step 3</h3>
                                    </div>                                    <div class="col-md-2 mb-3">
                                        <h3>Step 4</h3>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                         <img src="\img\steps\step1green.png" height=" 50px" width="50px" style="margin-top: -33px; margin-left: 140px;" onClick="LoadAjaxScreen('actionassignmentweight')">
                                    </div>
                                    <div class="col-md-1 mb-3">
                                          <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 90px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                       <img src="\img\steps\step2green.png" height=" 40px" width="75px" style="margin-top: -24px; margin-left: 80px;" onClick="LoadAjaxScreen('acceptrejectparts')">
                                    </div>
                                    <div class="col-md-1 mb-3">
                                         <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 50px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <img src="\img\steps\step3yellow.png" height=" 55px" width="55px" style="margin-top: -35px; margin-left: 50px;" onClick="LoadAjaxScreen('export')">
                                    </div>
                                    <div class="col-md-1 mb-3">
                                         <img src="\img\steps\arrows.png" height=" 30px" width="30px" style="margin-top: -15px; margin-left: 10px;">
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <img src="\img\steps\step4.png" height=" 50px" width="50px" style="margin-top: -30px; margin-left: 10px;" onClick="LoadAjaxScreen('finalsettlement')">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                       <p style="margin-left: -10px; ">Consignment Number/Weight</p>

                                    </div>
                                    <div class="col-md-2 mb-3" >
                                        <p style="margin-left: 10px;">Accept/Reject</p>
                                    </div>
                                    <div class="col-md-1 mb-3">
                                        
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        <p style="margin-left: -5px; font-weight: bold;">Export</p>
                                        
                                    </div>                                    <div class="col-md-2 mb-3">
                                        <p style="margin-left: -10px;">Final Settlement</p>
                                    </div>
                                    <div class="col-md-2 mb-3">
                                        
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                    <div class="panel-container show">
                        <div class="panel-content" >
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <h3>Export</h3>
                                </div>
                            </div>

                    <div class="form-row">
                        <div class="col-md-1  mb-1">
                        </div>
                        <div class="col-md-2  mb-1">
                            <h4>Word</h4>
                            <img src="\img\export\word.png" height=" 70px" width="70px" style="">
                        </div>
                        <div class="col-md-1  mb-1">
                        </div>
                        <div class="col-md-2  mb-1">
                            <h4>Excel</h4>
                            <img src="\img\export\excel.png" height=" 70px" width="70px" style="">
                        </div>
                        <div class="col-md-1  mb-1">
                        </div>
                        <div class="col-md-2  mb-1">
                            <h4>Powerpoint</h4>
                            <img src="\img\export\powerpoint.png" height=" 70px" width="70px" style=" ">
                        </div>
                        <div class="col-md-1  mb-1">
                        </div>
                        <div class="col-md-2  mb-1">
                            <h4>Access</h4>
                            <img src="\img\export\access.png" height=" 70px" width="70px" style=" ">
                        </div>
                    </div>           

                    <div class="form-row" style="margin-top: 30px;">
                        <div class="col-md-8  mb-3">
                        </div>
                        <div class="col-md-4  mb-3">
                            <button type="button" onClick="LoadAjaxScreen('finalsettlement')" class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Proceed
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('acceptrejectparts')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>

</main>
<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>