<?php
 require_once MODULE."/class/bllayer/user.php";
 $blUser = new BL_User();
 $fetchData = $blUser->DeletedCustomer();
    
?>

<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Deletd Customer
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
 <!-- datatable start -->
               <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr align="center">
                            <th>S.No</th>
                            <th>Login ID</th>
                            <th>Name</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Status</th>
                    
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            for ($i=0; $i <$fetchData->count; $i++) { 
                                //for

                                //print_r_pre($fetchData->rows[$i]);
                                $userId = $fetchData->rows[$i]['user_id'];
                                $fullName = $fetchData->rows[$i]['full_name'];
                                $fullName = ucwords($fullName);
                              
                                $mobNumber = $fetchData->rows[$i]['phone_number'];
                                $loginId = $fetchData->rows[$i]['login_id'];
                                $email = $fetchData->rows[$i]['email_address'];
                                $address = $fetchData->rows[$i]['address'];
                                $statusLabel =  $fetchData->rows[$i]['status'];                  
                                $created_at = $fetchData->rows[$i]['creation_date'];                          
                                $created_at=substr($created_at,0,10);
                                //echo "here...";
                        ?>

                                <tr>
                               <td><?php echo $i+1 ?></td>
                                <td ><?=$loginId?></td>
                                <td ><?=$fullName?></td>
                                <td ><?=$mobNumber?></td>
                                <td ><?=$email?></td>
                                <td ><?=$address?></td>
                            
                                <td ><?=$statusLabel?></td>
                                

                               
                                </tr>
                            <?php
                                }
                            ?>
                       
                    </tbody>
                </table>
</div>
</div>

</div>
</div>
</div>


</main>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>