<?php
  require_once(MODULE."/class/bllayer/vehicles.php");
  $blvehicle = new BL_Vehicle();
  $data=$blvehicle->GetMakeModel();
  // print_r_pre($data);
    $icon ='<i class="fas fa-floppy-o" style="margin-right: 4px"></i>';
    $title = 'Add';
    $buttonName = 'Add';
?>

 <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
        <form class="needs-validation" name='vehicalmodelforms' id='vehicalmodelforms' method='post' novalidate>
                          <input type='hidden' name='ACTION' value='ADDVEHICALMODEL'>
                  <div class="form-row">
                  <div class="col-md-6 mb-3">
                     <label class="form-label" for="">Vehical Make</label>
                     <select id="id" name="id" class="form-control">
                      <option value="">Select Vehical</option>
                       <?php for ($i=0; $i < $data->count; $i++) { 
                         
                        ?>
                        <option value="<?=$data->rows[$i]['id']?>"><?=$data->rows[$i]['name']?></option>
                      <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label"> Name<span class="text-danger">*</span></label>
                      <input class="form-control" placeholder="Enter vehicle model Name" type="text" name='name' maxlength="20">
                      <div class="invalid-feedback">
                          Please Enter a vehicle model Name.
                      </div>
                    </div> 
                    <div class="col-md-6 mb-3"> 
                      <label class="form-label">Description<span class="text-danger">*</span></label>
                      <input class="form-control" placeholder="Enter vehicle model Description" type="" name='description' maxlength="20"  required>
                      <div class="invalid-feedback">text
                          Please Enter a vehicle Description.
                      </div>
                    </div> 
                    
                    
                  </div>

                  <div class="form-row">
                    
                    <div class="col-md-6 p-4" style="text-align: center;"> 
                      <button style="width:130px; background: #ff8000" id="js-add-btn" class="btn btn-primary btn-md "style="background: #ff8000" type="button" onclick='AddVehicleModel()'><?=$icon?><?=$buttonName?></button>
                      <button style="width:130px; background: #ff8000" class="btn btn-primary btn-md " type="button" onclick='LoadAjaxScreen("viewvehicle")'><i class="fas fa-times-circle" style="margin-right: 4px"></i>Cancel</button>

                    </div> 
                  </div>
                  
                </div>
        </form>
      </div>
    </div>
  </div>
</div>

  <script>
   function AddVehicleModel()
  {
    var r = confirm("Are you sure you want to add?");
    if(r==true)
      {
        
          var obj = {onSuccess:VehicleModelInfoCallBack};
          form = document.getElementById('vehicalmodelforms');
          PostAjaxScreen("vehicalmodelforms",form,obj);
         
        
     }
  }

  function VehicleModelInfoCallBack()
  {
    
    LoadAjaxScreen("viewvehicle");
  }
  </script>