<?php
require_once(MODULE."/class/bllayer/user.php");
$user = new BL_User();


    $zone = $user->GetZones();
    $count3 =  $zone->count;



?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
<i class='subheader-icon fal fa-chart-area'></i>Add Zone
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addzoneform' id='addzoneform' method='post'>
<input type='hidden' name='ACTION' value='ADDZONE'>
<div id="panel-10" class="panel" style="margin-top: -30px;">
    

        <div>
            
            <div class="form-row" style="margin-top: 30px;">
                    <div class="col-md-1 mb-3">
                        
                    </div>
                    <div class="col-md-10 mb-3">
                        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr align="center">
                                    <th>#</th>
                                    <th>Zone Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for ($i=0; $i <$count3; $i++) { 
                                    $zoneName = $zone->rows[$i]['zone_name'];
                                    $zoneName=strtolower($zoneName);
                                    $zoneName=ucfirst($zoneName);
                                    $zoneID = $zone->rows[$i]['zone_id'];
                                ?>
                                <tr>
                                    <td align="center"><?php echo $i+1 ?></td>
                                    <td><input type="text" name="" value="<?php echo $zoneName ?>"></td>
                                    <td align="center" nowrap>
                                    <button type="button" onclick="DeleteZone('<?php echo $zoneID ?>')" class="btn btn-sm btn-dark">
                                    <span class="fal  fa-times mr-1"></span>
                                    </button></td>
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-1 mb-3">
                        
                    </div> -->
                   <!--  <div class="col-md-5 mb-3">
                            <div class="form-row" style="margin-top: 10px;">
                                <div class="col-md-8 mb-3">
                                    <label>Zone Name</label>
                                    <input type="text" class="form-control" id="zone" name="zone" placeholder="" d   value="" >
                                </div>
                                <div class="col-md-4 mb-3" style="margin-top: 23px;">
                                        <button type="button" style="width: 100px;" onclick="AddZone()" class="btn btn-med btn-warning">
                                            <span class="fal fa fa-plus mr-1"></span>
                                            Add
                                        </button>
                                </div>
                            </div>
                            <div class="form-row" style="margin-top: 10px;">
                                <div class="col-md-8 mb-3">
                                    <label>Zone Name</label>
                                    <input type="text" class="form-control" id="ZoneUpdate" name="ZoneUpdate" placeholder="" disabled="true"   value="" >
                                </div>
                                <div class="col-md-4 mb-3" style="margin-top: 23px;">
                                        <button type="button" style="width: 100px;" onclick="UpdateZone()" class="btn btn-med btn-warning">
                                            <span class="fal fa fa-chart-area mr-1"></span>
                                            Update
                                        </button>
                                </div>
                            </div>
                            <div class="form-row" style="margin-top: 10px;">
                                <div class="col-md-8 mb-3">
                                    <input type="text" class="form-control" id="ZoneID" name="ZoneID" placeholder="" d   value="" hidden="true">
                                </div>
                            </div>
                    </div> -->
            </div>
          
        </div>
</div>
<div style="height: 150px;"></div>
  </form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteZone(ZoneID)
{
  var r = confirm("Are you sure you want to delete this city?");
  if(r==true)
  {
  o =new Array(ZoneID);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=settings&function=deletezone&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
  }
  else
  {
    return;
  }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("addzone");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  LoadAjaxScreen("addzone");
}
</script>

<script type="text/javascript">
    
function AddZone() {
    if(confirm("Are you sure you want to add this Zone?"))
    {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('addzoneform');   
        PostAjaxScreen("addzoneform",form,callfunc);
    }
}

function NextPage(){
      LoadAjaxScreen("addzone");
}

function UpdateZone() {
    if(confirm("Are you sure you want to update this Zone?"))
    {
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addzoneform');   
        PostAjaxScreen("addzoneform",form,callfunc);
    }
}

function NextPage1(){
      LoadAjaxScreen("addzone");
}

</script>


<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var table = $('#dt-basic-example').DataTable();
     
    $('#dt-basic-example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        document.getElementById("ZoneUpdate").value = data[1];
        document.getElementById("ZoneID").value = data[0];
        document.getElementById("ZoneUpdate").disabled = false;
    } );
} );
    
</script>