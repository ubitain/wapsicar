<?php
require_once MODULE."/class/bllayer/user.php";
 $blUser = new BL_User();
    $fetchData = $blUser->GetCityRequest();
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
<i class='subheader-icon fal fa- chart-area'></i>Approve City Request
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addcityform' id='addcityform' method='post'>
<input type='hidden' name='ACTION' value='ADDROUTE'>
<div id="panel-10" class="panel" style="margin-top: -30px;">
        

        <div>
            <div class="form-row" style="margin-top: 30px;">
                    <div class="col-md-1 mb-3">
                        
                    </div>
                    <div class="col-md-10 mb-3">
                        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr align="center">
                                    <th>S.No</th>
                                    <th>Driver Name</th>
                                    <th> City</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                            for ($i=0; $i <$fetchData->count; $i++) { 
                                $id =$fetchData->rows[$i]['request_id'];
                                $name = $fetchData->rows[$i]['full_name'];
                                $city =  $fetchData->rows[$i]['city'];
                                ?>
                                <tr> 
                                    <td><?php echo $i+1 ?></td>
                                    <td align="center"><?=$name?></td>
                                    <td align="center"><?=$city?></td>
                                    <td align="center" nowrap>
                                <button type="button" onclick="CityApprovalStatus('<?=$id?>','<?=$city?>',1)" class="btn btn-sm btn-warning" style="background: #ff8000">
                                  <span class="fa fa-check-circle " aria-hidden="true"></span>

                                 </button>
                                <button type="button" onclick="CityDeleteStatus('<?=$id?>')" class="btn btn-sm btn-dark" style="background: #ff8000">
                                <span class="fa fa-trash-o mr-1" style="color: black"></span>
                                </button></td>
                                </tr>
                                <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-1 mb-3">
                        
                    </div> -->
                   
            </div>
            

          
        </div>

</div>
<div style="height: 150px;"></div>
  </form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>







<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
$(document).ready(function()
{
    $('#dt-basic-example').dataTable(
    {
    responsive: true,
    dom:
    "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    buttons: [],
    select: false
    });


    
    
});


function CityApprovalStatus(recordId,cityId,status)
{
  var r = confirm("Are you sure you want to Approve ?");
  if(r==true)
  {
  o = new Array(recordId,cityId,status);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=delete&function=CityApproval&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: CityApprovalCallBack});
  }
  else
  {
    return;
  }
}
function CityApprovalCallBack(response)
{
    alert('Approve Sucessfully');
  LoadAjaxScreen("approve");
}
function ReportError(res)
{

}
function CityDeleteStatus(recordId)
{
  alert(recordId);
  var r = confirm("Are you sure you want to Delete ?");
  if(r==true)
  {
  o = new Array(recordId);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=delete&function=CityDelete&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: CityDeleteCallBack});
  }
  else
  {
    return;
  }
}
function CityDeleteCallBack(response)
{
    alert('Delete Sucessfully');
  LoadAjaxScreen("approve");
}
function ReportError(res)
{

}

</script>