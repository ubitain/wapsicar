
<style>
* {
  box-sizing: border-box;
}

.image1 {

  transition: transform .2s;
  width: 100px;
  height: 100px;
  margin: 0 auto;
}

.image1:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}

</style>

<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    require_once(MODULE."/class/bllayer/user.php");
    $claimId=$_GET['id'];
    $user = new BL_User();
    $purchaseFromList = $user->GetPurchaseFromList();
    $countPurchaseFromList =  $purchaseFromList->count;

    $accountOffList = $user->GetAccountOffList();
    $countAccountOffList =  $accountOffList->count;
   // $userId = $mysession->getValue("userid");
    $viewClaims=new BL_ViewClaims();
    $fetchData=$viewClaims->ViewClaimsDetail($claimId);
    $count =  $fetchData->count;

    for ($i=0; $i <1; $i++) { 
        
          //for
            $claimID = $fetchData->rows[$i]['claim_id'];
            $claimNo = $fetchData->rows[$i]['claim_no'];
            $dealerID = $fetchData->rows[$i]['user_id'];
            
            $fetchData1=$viewClaims->FetchDealerName($dealerID);
            $claimName = $fetchData1->rows[0]['full_name'];
            $claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
            $claimDealerCatergory=strtolower($claimDealerCatergory);
            $claimMobNumber = $fetchData1->rows[0]['mobile_no'];
            $claimDealerAddress = $fetchData1->rows[0]['address'];

            $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
            $accountOf = $fetchData->rows[$i]['account_of'];
            $lcsNo = $fetchData->rows[$i]['lcs_no'];
            $cartonWeight = $fetchData->rows[$i]['carton_weight'];
            $remarks = $fetchData->rows[$i]['remarks'];
            $claimStatus = $fetchData->rows[$i]['claim_status'];
            $Date = $fetchData->rows[$i]['created_at'];  
            // ye sirf print krwnay kay liye tha jab last time form banaya tha ar reject hogya tha, sir said saari values na dekhao!
    }




    $fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
    $count2 =  $fetchData2->count;

    $statusError=$_GET['statusError'];
    if ($statusError==1) {
        ?>
        <script type="text/javascript">
            document.getElementById("PurchaseFromStatusByForm").style.borderColor = "red";
        </script>
        <?php
    }
    if ($statusError==2) {
        ?>
        <script type="text/javascript">
            document.getElementById("PurchaseFromStatusByFormAccount").style.borderColor = "red";
        </script>
        <?php
    }
    if ($statusError==5) {
        ?>
        <script type="text/javascript">
            document.getElementById("PurchaseFromStatusByFormAccount").style.borderColor = "red";
            document.getElementById("PurchaseFromStatusByForm").style.borderColor = "red";
        </script>
        <?php
    }

$fetchData1= $viewClaims->FetchClaimTransactions($claimID);
$countTransaction =  $fetchData1->count;

for ($j=0; $j <$countTransaction; $j++) { 
    $accountOff = $fetchData1->rows[$j]['user_id'];
    $accountOffStatus = $fetchData1->rows[$j]['status'];
    $fetchDataAcc=$viewClaims->FetchDealerName($accountOff);
    $accountOffName = $fetchDataAcc->rows[0]['full_name'];
    $accountOffNameDes = $fetchDataAcc->rows[0]['dealer_category_name'];
    $accountOffNameDes=strtolower($accountOffNameDes);
    $accountOffNameDes=ucfirst($accountOffNameDes);
}


$fetchDataPurch = $viewClaims->FetchClaimTransactionsPurchase($claimID);
$countTransactionPurchase =  $fetchDataPurch->count;


for ($j=0; $j <$countTransactionPurchase; $j++) { 
    $purchasedFrom = $fetchDataPurch->rows[$j]['user_id'];
    $purchasedFromStatus = $fetchDataPurch->rows[$j]['status'];
    $fetchData13 = $viewClaims->FetchDealerName($purchasedFrom);
    $purchasedFrom1 = $fetchData13->rows[0]['full_name'];
    $purchasedFromDesig = $fetchData13->rows[0]['dealer_category_name'];
    $purchasedFromDesig = strtolower($purchasedFromDesig);
    $purchasedFromDesig = ucfirst($purchasedFromDesig);
}


$zones = $user->GetZones();
$count5 =  $zones->count;

?>

<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Change Status (Purchase From/ Account Of)
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

<form class="form-horizontal" name='updatepurchaseaccount' id='updatepurchaseaccount' method='post'>
<input type='hidden' name='ACTION'  value='ADDPURCHASEACCOUNTCLAIMDEPARTMENT'>
<input type='hidden' name='claimID' value="<?php echo $claimID ?>" >
<input type='hidden' name='purchasedFromHidden' value="<?php echo $purchasedFrom ?>" >
<input type='hidden' name='ClaimerIdHidden' value="<?php echo $dealerID ?>" >
<input type='hidden' name='accountOffHidden' value="<?php echo $accountOff ?>" >
<input type='hidden' name='claimNo' value="<?php echo $claimNo ?>" >
            <div class="row" style="margin-top: -30px;"> 
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5></h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claim Number</label>
                                                <input type="text" class="form-control" id="" name="" placeholder="" disabled="true"  value="<?php echo $claimNo ?>" >
                                                
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Claimer Name</label>
                                                <input type="text" class="form-control" id="claimName" name="claimName" placeholder="" disabled="true"  value="<?php echo ucfirst($claimName) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <?php 
                                            if ($purchasedFromStatus=="accept") {
                                                ?>

                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchase From</label>
                                                 <input type="text" class="form-control" id="Purchase" name="Purchase" placeholder="" disabled="true"  value="<?php echo ucfirst($purchasedFrom1).' - '. ucfirst($purchasedFromDesig) ?>" >
                                                <input type="text" class="form-control" id="PurchaseFromStatusByForm" name="PurchaseFromStatusByForm" placeholder="" hidden="true"  value="" >
                                            </div>

                                                <?php 
                                            }
                                            else if ($purchasedFromStatus=="pending") {
                                                ?>

                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchase From</label>
                                                 <input type="text" class="form-control" id="Purchase" name="Purchase" placeholder="" disabled="true"  value="<?php echo ucfirst($purchasedFrom1).' - '. ucfirst($purchasedFromDesig) ?>" >
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                 <select id="PurchaseFromStatusByForm" name="PurchaseFromStatusByForm" class="form-control" style="" onchange="StatusChangeFuncCall()">
                                                    <option value="">Select Status</option>
                                                    <option value="accept">Accept</option>
                                                    <option value="rejected">Reject</option>
                                                    <option value="change_purchased_from">Change Purchase From</option>
                                                </select>
                                            </div>    
                                                <?php 
                                            }
                                            ?>
                                </div>
                                <div class="form-row" id="hiddendiv" hidden>
                                            

                                            <div class="col-md-3 mb-3">
                                            <label class="form-label" for="">Zone</label>
                                            <select id="zone" name="zone" class="form-control" onchange='zonetoregion(this.value)' >
                                                <option value="">Select Zone</option>
                                             <?php 
                                            for ($i=0; $i <$count5; $i++) { 
                                                $zonesName = $zones->rows[$i]['zone_name'];
                                                $zoneId = $zones->rows[$i]['zone_id'];
                                                $zonesName = strtolower($zonesName);
                                                $zoneId = strtolower($zoneId);
                                                $zonesName = ucfirst($zonesName);
                                                $zoneId = ucfirst($zoneId);
                                            ?>
                                                <option value="<?php echo $zonesName.",".$zoneId ?>"><?php echo $zonesName ?></option>
                                            <?php 
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label class="form-label" for="">Region</label>
                                            <select id="region" name="region" class="form-control" onchange='regiontocity(this.value)'>
                                                 <option value="">Select Region</option>
                                          <!--   <?php 
                                            for ($i=0; $i <$count4; $i++) { 
                                                $regionsName = $regions->rows[$i]['region_name'];
                                                $regionsName = strtolower($regionsName);
                                                $regionsName = ucfirst($regionsName);
                                            ?>
                                                <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                                            <?php 
                                            }
                                            ?> -->
                                            </select>
                                        </div> 
                                        <div class="col-md-3 mb-3">
                                            <label class="form-label" for="validationTooltip01">City</label>
                                            <select id="city" name="city" class="form-control" onchange='citytoarea(this.value)'>
                                            <option value="">Select City</option>
                                            <!-- <?php 
                                            for ($i=0; $i <$count3; $i++) { 
                                                $cityName = $city->rows[$i]['city_name'];
                                            ?>
                                                <option value="<?php echo $cityName ?>"><?php echo $cityName ?></option>
                                            <?php 
                                            }
                                            ?> -->
                                            </select>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label class="form-label" for="">Area</label>
                                            <select id="area" name="area" onchange='areatopurchasefrom(this.value)' class="form-control">
                                            <option value="">Select Area</option>

                                            </select>
                                        </div>



                                            <div class="col-md-12 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchase From</label>
                                                 <select id="PurchaseSelect" name="PurchaseSelect" class="form-control" style="">
                                                    <option value="">Select Purchase From</option>
                                                 <!-- <?php 
                                                for ($i=0; $i <$countPurchaseFromList; $i++) { 
                                                    $purchaseFromId = $purchaseFromList->rows[$i]['user_id'];
                                                    $purchaseFromName = $purchaseFromList->rows[$i]['full_name'];
                                                    $purchaseFromName = strtolower($purchaseFromName);
                                                    $purchaseFromName = ucfirst($purchaseFromName);
                                                    if ($purchasedFrom==$purchaseFromId) {
                                                        # code...
                                                    }
                                                    else{

                                                ?>
                                                    <option value="<?php echo $purchaseFromName.",".$purchaseFromId ?>"><?php echo $purchaseFromName ?></option>
                                                <?php 
                                                }
                                                }
                                                ?> -->
                                                </select>
                                            </div>

                                </div>
                                <div class="form-row">
                                            <?php 
                                            if ($accountOffStatus=="accept") {
                                                ?>

                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Account Of</label>
                                                 <input type="text" class="form-control" id="Account" name="Account"  placeholder="" disabled="true"  value="<?php echo ucfirst($accountOffName).' - '. ucfirst($accountOffNameDes) ?>" >
                                                 <input type="text" class="form-control" id="PurchaseFromStatusByFormAccount" name="PurchaseFromStatusByFormAccount"  placeholder="" hidden="true"  value="" >
                                            </div>

                                            <?php 
                                            }
                                            else if ($accountOffStatus=="pending") {
                                            ?>
                                            <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Account Of</label>
                                                 <input type="text" class="form-control" id="Account" name="Account"  placeholder="" disabled="true"  value="<?php echo ucfirst($accountOffName).' - '. ucfirst($accountOffNameDes) ?>" >
                                            </div>
                                             <div class="col-md-6 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                 <select id="PurchaseFromStatusByFormAccount" name="PurchaseFromStatusByFormAccount" class="form-control" style="">
                                                    <option value="">Select Status</option>
                                                    <option value="accept">Accept</option>
                                                    <option value="rejected">Reject</option>
                                                </select>
                                            </div>  
                                        <?php } ?>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-8 mb-3">
                                    </div>
                                    <div class="col-md-4 mb-3" >
                                        <button type="button" style="margin-left: 25px;" onclick="AddPurchaseAccount()" data-dismiss="modal"   class="btn btn-med btn-warning">
                                            <span class="fal fa fa-arrow-right mr-1"></span>
                                            Save
                                        </button>
                                    </div>
                                </div>
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>
</main>


<!-- <nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav> -->

<script type="text/javascript">


function zonetoregion(zone)
{
// alert(zone);
if(zone == '')
{
alert("No ID found!!");
document.getElementById('region').innerHTML = "";
document.getElementById('city').innerHTML = "";
document.getElementById('area').innerHTML = "";
document.getElementById('PurchaseSelect').innerHTML = "";
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('PurchaseSelect').innerHTML += "<option value=''>Select Area</option>";
}
else
{
o =new Array(zone);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetRegion&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetRegionCallBack});

}
}
function GetRegionCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('region').options.length = 0;
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('region').innerHTML += obj;
}
///
function regiontocity(region)
{
// alert(region);
if(region == '')
{
alert("No ID found!!");

document.getElementById('city').innerHTML = "";
document.getElementById('area').innerHTML = "";
document.getElementById('PurchaseSelect').innerHTML = "";

document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('PurchaseSelect').innerHTML += "<option value=''>Select Area</option>";

}
else
{
o =new Array(region);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetCity&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetCityCallBack});

}
}
function GetCityCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('city').options.length = 0;
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('city').innerHTML += obj;
}
///
function citytoarea(city)
{
//  alert(city);
if(city == '')
{
alert("No ID found!!");

document.getElementById('area').innerHTML = "";
document.getElementById('PurchaseSelect').innerHTML = "";


document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('PurchaseSelect').innerHTML += "<option value=''>Select Area</option>";

}
else
{
o =new Array(city);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetArea&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAreaCallBack});

}
}
function GetAreaCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('area').options.length = 0;
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('area').innerHTML += obj;
}

function areatopurchasefrom(area)
{
//  alert(city);
if(area == '')
{
alert("No ID found!!");
document.getElementById('PurchaseSelect').innerHTML = "";
document.getElementById('PurchaseSelect').innerHTML += "<option value=''>Select Area</option>";

}
else
{

var zone = document.getElementById("zone").value;
var region = document.getElementById("region").value;
var city = document.getElementById("city").value;
//var area = document.getElementById("area").value;

o = new Array(zone,region,city,area);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetPurchaseFrom&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetPurchaseFromCallBack});

}
}
function GetPurchaseFromCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('PurchaseSelect').options.length = 0;
document.getElementById('PurchaseSelect').innerHTML += "<option value=''>Select Purchase From</option>";
document.getElementById('PurchaseSelect').innerHTML += obj;
}


function StatusChangeFuncCall()
{
    var x = document.getElementById("PurchaseFromStatusByForm").value;
    if (x=="change_purchased_from") {
        document.getElementById("hiddendiv").hidden=false;
    }
    else{
        document.getElementById("hiddendiv").hidden=true;
    }

}

function AddPurchaseAccount() {
    var x = document.getElementById("PurchaseFromStatusByForm").value;
    var y = document.getElementById("PurchaseFromStatusByFormAccount").value;
    var z = document.getElementById("PurchaseSelect").value;
    var status=0;
    var accountandpurchase=0;

    if (y=="" && x=="") {
        alert('Purchase From Or Account Off Required!');
        OpenModalBox('purchaseaccountacceptreject&id=<?php echo $claimID ?>&statusError=5','screen','');
        status=1;
    }
    else{

        if (y!="") {
            if(confirm("Are you sure you want to save?"))
            {
            var callfunc={onSuccess:NextPage1};
            form= document.getElementById('updatepurchaseaccount');   
            PostAjaxScreen("updatepurchaseaccount",form,callfunc);
            }
            status=1;
            accountandpurchase=1;
        }

        if (x!="") {
            if (x!="change_purchased_from") {
                    if (accountandpurchase==0) {
                    if(confirm("Are you sure you want to save?"))
                    {
                    var callfunc={onSuccess:NextPage1};
                    //alert('asdasdsad');
                    form= document.getElementById('updatepurchaseaccount');   
                    debugger;
                    PostAjaxScreen("updatepurchaseaccount",form,callfunc);
                    debugger;
                    status=1;
                    }
                }
            }
            else
            {
                if (z!="") {
                    if (accountandpurchase==0) {
                        if(confirm("Are you sure you want to save?"))
                        {
                        var callfunc={onSuccess:NextPage1};
                        form= document.getElementById('updatepurchaseaccount');   
                        PostAjaxScreen("updatepurchaseaccount",form,callfunc);
                        }
                        status=1;
                    }
                }
                else{
                    alert('New Purchase From!!');
                    OpenModalBox('purchaseaccountacceptreject&id=<?php echo $claimID ?>&statusError=1','screen','');
                    status=1;
                }
                
            }
        }
    }

    if (status==0) {
        if (y=="") {
            alert('Account Off Required!');
            OpenModalBox('purchaseaccountacceptreject&id=<?php echo $claimID ?>&statusError=2','screen','');
        }
        else{
            if(confirm("Are you sure you want to save?"))
            {
            var callfunc={onSuccess:NextPage1};
            form= document.getElementById('updatepurchaseaccount');   
            PostAjaxScreen("updatepurchaseaccount",form,callfunc);
            }
        }

        if (x=="") {
            alert('Purchase From Required!');
            OpenModalBox('purchaseaccountacceptreject&id=<?php echo $claimID ?>&statusError=1','screen','');
        }
        else{
            if(confirm("Are you sure you want to save?"))
            {
            var callfunc={onSuccess:NextPage1};
            form= document.getElementById('updatepurchaseaccount');   
            PostAjaxScreen("updatepurchaseaccount",form,callfunc);
            }
        }
    }
}

function NextPage1(){
      LoadAjaxScreen("pendingclaimsunapproved");
}
</script>


<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>