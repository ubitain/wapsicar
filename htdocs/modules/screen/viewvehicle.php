<?php
 require_once(MODULE . "/class/bllayer/vehicles.php");
 $blAddvehicle = new BL_Vehicle();
 $getVehicle = $blAddvehicle->getVehicleMake();
    // print_r_pre($getVehicle);
    $a = count($getVehicle);
    // print_r_pre($getVehicle);
?>
<div id="content">
<section id="" class="">
  <div class="row">
    <!-- NEW WIDGET START -->
   <div class="col-xl-12">
    <div id="panel-1" class="panel">
        <div class="panel-container show">
            <div class="panel-content">
        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
          <thead class="bg-primary-600" style="background: #ff8000">
                <tr>
                  <th nowrap><center>S.No</center></th>
                  <th nowrap><center>Name</center></th>
                  <th nowrap><center>Description</center></th>
                  <th nowrap ><center>Action</center></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                for($i = 0; $i <$a; $i++){
                 
                ?>
                <tr>
                  <td><center><?=$i + 1?></center></td>
                  <td><center><?=$getVehicle[$i]['name']?></center></td>
                  <td><center><?=$getVehicle[$i]['description']?></center></td>

                  <td><center>
                  <button class="btn btn-sm btn-primary" style="background: #ff8000" onClick="LoadAjaxScreen('viewvehiclemodel&id=<?=$getVehicle[$i]['id']?>')"><i class="fal fa-eye" ></i></button>  
                  <button class="btn btn-sm btn-primary" style="background: #ff8000" onClick="LoadAjaxScreen('editvehicle&id=<?=$getVehicle[$i]['id']?>')"><i class="fal fa-edit"></i></button>
                  <button class="btn btn-sm btn-primary"  style="background: #ff8000"title="Delete"  onclick="VehicleDelete('<?=$getVehicle[$i]['id']?>')"><i class="fal fa-times"></i></button>
                    </center>
                  </td>
                </tr>
                
             <?php  }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div>

    </section>
  </div>

<script type="text/javascript">
    
function VehicleDelete(id)
{

  var r = confirm("Are you sure you want to delete?");
  if(r==true)
  {
  o =new Array(id);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=delete&function=VehicleDelete&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: VehicleDeleteCallBack});
  }
  else
  {
    return;
  }
}
function VehicleDeleteCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("viewvehicle");
}
function ReportError(res)
{
}

  </script>
