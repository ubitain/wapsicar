<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
<i class='subheader-icon fal fa-chart-area'></i> Add Claim
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div id="panel-5" class="panel">
<div class="panel-container show">
<div class="panel-content p-0">
<form class="needs-validation" novalidate>
<div class="panel-content">
    <div class="form-row">
        
        <div class="col-md-4 mb-3">
            <label class="form-label" for="validationTooltip01">Claimer Name</label>
            <input type="text" class="form-control" id="validationTooltip01" placeholder=""  disabled="true" value="" >
            <div class="valid-tooltip">
                Looks good!
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label class="form-label" for="validationTooltip02">Shipment Number</label>
            <input type="text" class="form-control" id="validationTooltip02" placeholder="" value="" >
            <div class="valid-tooltip">
                Looks good!
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label class="form-label" for="validationTooltip03">Weight</label>
            <div class="input-group">
                
                <input type="text" class="form-control" id="validationTooltip03" placeholder="" aria-describedby="inputGroupPrepend2" >
            </div>
        </div>
    </div>
    <h6>Product Details</h6>
    <div id="dynamic" style="border:  1px solid lightgrey; padding-top: 15px; ">
        <div class="form-row" id="delete0" style="padding-left: 3px; padding-right: 3px;">
        <div class="col-md-3 mb-3">
            <label class="form-label" >Product Name <span class="text-danger">*</span> </label>
            <select class="select2 form-control w-100" id="select0">
                    <option value="AZ">Nuts</option>
                    <option value="CO">Clutch</option>
                    <option value="ID">Rear Derailleur</option>
                </optgroup>
            </select>
        </div>
        <div class="col-md-2 mb-3">
            <label class="form-label" >Product Quality <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="Quality0" placeholder="" value="" >
        </div>
        <div class="col-md-3 mb-3">
            <label class="form-label" >Description</label>
            <input type="text" class="form-control" id="Description0" placeholder="" value="" >
        </div>
        <div class="col-md-3 mb-3">
            <label class="form-label" >Status <span class="text-danger">*</span></label>
            <div class="input-group">
                
            <select class="select2 form-control w-100" id="status0">
                    <option value="Broken">Broken</option>
                    <option value="PartiallyBroken">Partially Broken</option>
                    <option value="Fine">Fine</option>
                </optgroup>
            </select>
            </div>
        </div>
        <div class="col-md-1 mb-3">
        </div>
    </div>
    </div>
    <div class="form-row">
        <div class="col-md-9 mb-3">
           
        </div>
        <div class="col-md-3 mb-3" style="margin-top: 10px;  padding-left: 80px;" >
        <button type="button" onclick="GetMoreRowsForClaim()"  class="btn btn btn-warning"><span class="fal fa-plus"></span> Add Row</button>
        </div>
    </div>
</div>
</form>
</div>
</div>
</div>
</main>


<script type="text/javascript">
    var count = 0;
    function GetMoreRowsForClaim()
    {   
        count++;
        var data = '<div class="form-row" id="delete'+count+'" style="padding-left: 3px; padding-right: 3px;">';
        data = data +'<div class="col-md-3 mb-3">';
        data = data +'<label class="form-label" >Product Name <span class="text-danger">*</span> </label>';
        data = data +'<select class="select2 form-control w-100" id="select'+count+'"><option value="AZ">Nuts</option><option value="CO">Clutch</option><option value="ID">Rear Derailleur</option></optgroup></select>';
        data = data +'</div>';
        data = data +'<div class="col-md-2 mb-3">';
        data = data +'<label class="form-label" >Product Quality <span class="text-danger">*</span></label>';
        data = data +'<input type="text" class="form-control" id="Quality'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-3 mb-3">';
        data = data +'<label class="form-label" >Description</label>';
        data = data +'<input type="text" class="form-control" id="Description'+count+'" placeholder="" value="" >';
        data = data +'</div>';
        data = data +'<div class="col-md-3 mb-3">';
        data = data +'<label class="form-label" >Status <span class="text-danger">*</span></label>';
        data = data +'<div class="input-group">';
        data = data +' <select class="select2 form-control w-100" id="status'+count+'"><option value="AZ">Broken</option><option value="CO">Partially Broken</option><option value="ID">Fine</option></optgroup></select>';
        data = data +'</div></div>';
        data = data +'<div class="col-md-1 mb-3">';
        data = data +'<label class="form-label" >Action</label>';
        data = data +'<button type="button" class="btn btn-danger"  id="del'+count+'" onclick="DelRowForClaim('+count+')"><i class="fa fa-times"></i> </button>';
        data = data +'</div>';
        data = data +'</div>';


        $("#dynamic").append(data);
    }



    function DelRowForClaim(count)
    {
  
    if(confirm("Are you sure you want to Delete?"))
        {
     var el = count;
     $('#delete'+el).remove();
    }
    }
</script>