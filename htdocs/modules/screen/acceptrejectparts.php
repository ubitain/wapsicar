
<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    $claimId=$_GET['id'];
   // $userId = $mysession->getValue("userid");
    $viewClaims=new BL_ViewClaims();
    $fetchData = $viewClaims->ViewClaimsDetail($claimId);
    $count =  $fetchData->count;

    for ($i=0; $i <1; $i++) { 
          //for
           $claimID = $fetchData->rows[$i]['claim_id'];
            $claimNo = $fetchData->rows[$i]['claim_no'];
            $dealerID = $fetchData->rows[$i]['user_id'];
            
            $fetchData1=$viewClaims->FetchDealerName($dealerID);
            $claimName = $fetchData1->rows[0]['full_name'];
            $claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
            $claimDealerCatergory=strtolower($claimDealerCatergory);
            $claimMobNumber = $fetchData1->rows[0]['mobile_no'];
            $claimDealerAddress = $fetchData1->rows[0]['address'];

            $market = $fetchData1->rows[0]['market'];
            $town = $fetchData1->rows[0]['town'];
            $zone = $fetchData1->rows[0]['zone'];
            $region = $fetchData1->rows[0]['region'];

            $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
            $fetchData1=$viewClaims->FetchDealerName($purchasedFrom);
            $purchasedFrom = $fetchData1->rows[0]['full_name'];
            $purchasedFromDesig = $fetchData1->rows[0]['dealer_category_name'];

            $accountOf = $fetchData->rows[$i]['account_of'];
            $fetchData1=$viewClaims->FetchDealerName($accountOf);
            $accountOf = $fetchData1->rows[0]['full_name'];
            $accountOfDesig = $fetchData1->rows[0]['dealer_category_name'];

            $lcsNo = $fetchData->rows[$i]['lcs_no'];
            $cartonWeight = $fetchData->rows[$i]['carton_weight'];
            $receivingWeight = $fetchData->rows[$i]['receiving_weight'];
            $remarks = $fetchData->rows[$i]['remarks'];
            $claimStatus = $fetchData->rows[$i]['claim_status'];
            $Date = $fetchData->rows[$i]['created_at'];  
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            
            $purchasedFromDesig=strtolower($purchasedFromDesig);
            $purchasedFromDesig=ucfirst($purchasedFromDesig);
            $accountOfDesig=strtolower($accountOfDesig);
            $accountOfDesig=ucfirst($accountOfDesig);

    }

    $fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
    $count2 =  $fetchData2->count;



?>

<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Accept Reject Parts
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

<form class="form-horizontal" name='updatequantityform' id='updatequantityform' method='post'>
<input type='hidden' name='ACTION' value='UPDATEQUANTITY'>
<input type="text" class="form-control" id="count2" name="count2" placeholder="" hidden="true"  value="<?php echo $count2 ?>" >
            <div class="row" style="margin-top: -30px;">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Basic Information</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">ClaimNo</label>
                                                <input type="text" class="form-control" name="" id="" placeholder="" disabled="true"  value="<?php echo $claimNo ?>" >
                                                <input type="text" class="form-control" name="acceptrejectclaimID" id="acceptrejectclaimID" placeholder="" hidden="true"  value="<?php echo $claimID ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimStatus) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Remarks</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($remarks) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Date</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $Date ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Raised By</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Raised By Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimName) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Raised By Type</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimDealerCatergory) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Phone Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimMobNumber ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Address</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimDealerAddress) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Zone</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($zone) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Region</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($region) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Town</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($town) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Market</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($market) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Purchased From</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchased From</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($purchasedFrom)." (".ucfirst($purchasedFromDesig).")"  ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">On Account Of</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($accountOf) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Carton Weight (Kg)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $cartonWeight ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Consigment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $lcsNo ?>" >
                                            </div>

                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Receiving Weight (Kg)</label>
                                                <input type="text" id="receivingweight" name="receivingweight" class="form-control" id="validationTooltip01" placeholder=""   value="<?php echo $receivingWeight ?>" >
                                            </div>

                                </div>
                                <div class="form-row" >
                                    <div class="col-md-12 mb-3">
                                        <h5 style="font-weight: bold;">Claim Item Information</h5>
                                    </div>
                                </div>
                            <div class="form-row" style="">
                            <div class="col-md-12 mb-3">
                                <div style="margin-top:0px;"></div>
                                <table class="table table-bordered table-hover m-0"   >
                                <thead>
                                    <tr style="background-color: #dfe1e6;">
                                        <th style="text-align: center;">Serial No</th>
                                        <th style="text-align: center;">Item Code</th>
                                        <th style="text-align: center;">Item Name</th>
                                        <th style="text-align: center;">Recieved Quantity</th>
                                        <th style="text-align: center;">Approved Quantity</th>
                                        <th style="text-align: center;">Rejected Quantity</th>
                                        <th style="text-align: center;">Other Vendor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?

                                for ($i=0; $i <$count2; $i++) { 
                                      //for
                                        $claimItemId = $fetchData2->rows[$i]['item_id'];
                                        $recievedQty = $fetchData2->rows[$i]['recieved_qty'];
                                        $claimDetailId = $fetchData2->rows[$i]['claim_detail_id'];
                                        $approvedQty = $fetchData2->rows[$i]['approved_qty'];
                                        $rejectedQty = $fetchData2->rows[$i]['rejected_qty'];
                                        $otherQty = $fetchData2->rows[$i]['other_qty'];
                                        $fetchData3=$viewClaims->FetchItemName($claimItemId);
                                        $claimItemName = $fetchData3->rows[0]['item_name'];
                                        $claimItemCode = $fetchData3->rows[0]['item_code'];

                                        $remarks = $fetchData2->rows[$i]['remarks'];

                                ?>
                                <tr>
                                <th scope="row" style="text-align: center;"><?php echo $i+1; ?></th>
                                <td>
                                <input type="text" class="form-control" id="" placeholder="" disabled="true"  value="<?php echo $claimItemCode; ?>" >
                                <input type="text" class="form-control" id="claimDetailId<?php echo $i ?>" name="claimDetailId<?php echo $i ?>" placeholder="" hidden="true"  value="<?php echo $claimDetailId; ?>" >
                                </td>
                                <td>
                                <input type="text" class="form-control"  placeholder="" disabled="true"  value="<?php echo $claimItemName ?>" >
                                </td>
                                <td >
                                <input type="text" style="text-align: center;" class="form-control"  id="recievedQty<?php echo $i ?>" placeholder="" disabled="true"  value="<?php echo $recievedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="Approved<?php echo $i ?>" name="Approved<?php echo $i ?>" placeholder=""  value="<?php echo $approvedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="Rejected<?php echo $i ?>" name="Rejected<?php echo $i ?>" placeholder=""   value="<?php echo $rejectedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="Other<?php echo $i ?>" name="Other<?php echo $i ?>" placeholder=""  value="<?php echo $otherQty ?>" >
                                </td> 
                                <tr>        
                                <?php } ?>
                                </tbody>
                                </table>
                                </div>
                            </div>

                    <div style="margin-top: 20px;"></div>
                    <div class="form-row">
                        <div class="col-md-10  mb-3">
                        </div>
                        <div class="col-md-2  mb-3">
                            <button type="button" onclick="UpdateQuantity(<?php echo $count2 ?>)" class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Save
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('inprocessclaim')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
</form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>

<script type="text/javascript">
$("#receivingweight").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if($("#receivingweight").val().length >7 ) {
            return false;
       }
});
i=0;
var count1 = document.getElementById("count2").value;
if (count1==1) {
    $("#Approved0").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if($("#Approved0").val().length >10 ) {
            return false;
       }
    });

    $("#Rejected0").keypress(function(e){
       var keyCode = e.keyCode || e.which;
        var regex = /^[0-9  ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
           return false
           }

        if($("#Rejected0").val().length >10 ) {
                return false;
           }
    });

    $("#Other0").keypress(function(e){
       var keyCode = e.keyCode || e.which;
        var regex = /^[0-9  ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
           return false
           }

        if($("#Other0").val().length >10 ) {
                return false;
           }
    });    
}
else{
    for (var w = 0; w < count1; w++) {
       $("#Approved"+w).keypress(function(e){
       var keyCode = e.keyCode || e.which;
        var regex = /^[0-9  ]+$/;
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
           return false
           }

        if($("#Approved"+w).val().length >10 ) {
                return false;
           }
    });

        $("#Rejected"+w).keypress(function(e){
           var keyCode = e.keyCode || e.which;
            var regex = /^[0-9  ]+$/;
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
               return false
               }

            if($("#Rejected"+w).val().length >10 ) {
                    return false;
               }
        });

        $("#Other"+w).keypress(function(e){
           var keyCode = e.keyCode || e.which;
            var regex = /^[0-9  ]+$/;
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
               return false
               }

            if($("#Other"+w).val().length >10 ) {
                    return false;
               }
        });    
    }
}

function UpdateQuantity(count2) {
    var count=0;
     for (var i = 0; i < count2; i++) {
        var grandtotal=0;
        var recieved = document.getElementById("recievedQty"+i).value;
        var approved = document.getElementById("Approved"+i).value;
        var rejected = document.getElementById("Rejected"+i).value;
        var others = document.getElementById("Other"+i).value;
        

        // if (approved=="" && rejected=="" && others=="") {
        //     count=2;
        //     break;
        // }

        grandtotal=+approved + +rejected + +others;
        if (grandtotal==0) {
            count=2;
        }
        else if (grandtotal>recieved) {
            
            count=1;
        }
        else if (recieved>grandtotal) {
            
            count=3;
        }
     }

    if (count==0) {
        if(confirm("Are you sure you want to save?"))
        {
            var callfunc={onSuccess:NextPage1};
            form= document.getElementById('updatequantityform');   
            PostAjaxScreen("updatequantityform",form,callfunc);
        } 
    }
    else if (count==1){
        alert("Your Accept, Reject, Other Quantity is greater than Recieved Quantity!");
    }
    else if (count==2){
        alert("You Accept, Reject, Other Quantity must be greater than zero!");
    }
    else if (count==3){
        alert("Your Accept, Reject, Other Quantity is lesser than Recieved Quantity!");
    }
    
}

function NextPage1(){
      LoadAjaxScreen("inprocessclaim");
}


</script>