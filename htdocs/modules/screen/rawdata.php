<?php 
require_once(MODULE."/class/bllayer/user.php");
    $user = new BL_User();
    $dealerCatergory = $user->GetDealerCatergory();
    $count =  $dealerCatergory->count;

    $subDealerCatergory = $user->GetSubDealerCatergory();
    $count1 =  $subDealerCatergory->count;

    $groups = $user->GetGroups();
    $count2 =  $groups->count;

    $city = $user->GetCity();
    $count3 =  $city->count;

    $regions = $user->GetRegions();
    $count4 =  $regions->count;

    $zones = $user->GetZones();
    $count5 =  $zones->count;

?>
<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Add User
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

            <form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
            <input type='hidden' name='ACTION' value='ADDUSER'>

            <div class="row" style="margin-top: -30px;">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5>User Detail</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Registration Number</label>
                                                <input type="text" class="form-control" id="registration" name="registration" placeholder=""    value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Sap Code</label>
                                                <input type="text" class="form-control" id="sapcode" name="sapcode" placeholder=""    value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Dealer Catergory</label>
                                                <select id="dealer" name="dealer" class="form-control" onchange='viewsubcategory(this.value)' >
                                                <option value="">Select Dealer Catergory</option>
                                                <?php 
                                                for ($i=0; $i <$count; $i++) { 
                                                    $dealerCatergoryId = $dealerCatergory->rows[$i]['dealer_category_id'];
                                                    $dealerCatergoryName = $dealerCatergory->rows[$i]['dealer_category_name'];
                                                    $dealerCatergoryName = strtolower($dealerCatergoryName);
                                                    $dealerCatergoryName = ucfirst($dealerCatergoryName);
                                                ?>
                                                    <option value="<?php echo $dealerCatergoryId ?>"><?php echo $dealerCatergoryName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Sub Dealer Catergory</label>
                                                <select id="subdealer" name="subdealer" class="form-control">
                                                <option value="">Select Sub Dealer Catergory</option>
                                                <?php 
                                                for ($i=0; $i <$count1; $i++) { 
                                                    $subDealerCatergoryId = $subDealerCatergory->rows[$i]['sub_category_id'];
                                                    $subDealerCatergoryName = $subDealerCatergory->rows[$i]['sub_category_name'];
                                                ?>
                                                    <option value="<?php echo $subDealerCatergoryId ?>"><?php echo $subDealerCatergoryName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Name</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder=""   value="" >
                                            </div>
                                            <!-- <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Gender</label>
                                                <select id="gender" name="gender" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                </select>
                                            </div> -->
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">CNIC</label>
                                                <input type="text" class="form-control" id="cnic" name="cnic" placeholder="" d   value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Group</label>
                                                <select id="group" name="group" class="form-control">
                                                <option value="">Select Group</option>
                                                <?php 
                                                for ($i=0; $i <$count2; $i++) { 
                                                    $groupId = $groups->rows[$i]['group_id'];
                                                    $groupName = $groups->rows[$i]['group_name'];
                                                ?>
                                                    <option value="<?php echo $groupId ?>"><?php echo $groupName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Joining Date</label>
                                                <input type="date" class="form-control" id="joiningdate" name="joiningdate" placeholder="" d   value="" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Mobile Number</label>
                                                <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="" d   value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Email</label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="" d   value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Address</label>
                                                <input type="text" class="form-control" id="address" name="address" placeholder="" d   value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Country</label>
                                                <select id="country" name="country" class="form-control">
                                                <option value="">Select Country</option>
                                                <option value="Pakistan">Pakistan</option>
                                                </select>
                                            </div>


                                            
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Zone</label>
                                                <select id="zone" name="zone" class="form-control">
                                                    <option value="">Select Zone</option>
                                                 <?php 
                                                for ($i=0; $i <$count5; $i++) { 
                                                    $zonesName = $zones->rows[$i]['zone_name'];
                                                    $zonesName = strtolower($zonesName);
                                                    $zonesName = ucfirst($zonesName);
                                                ?>
                                                    <option value="<?php echo $zonesName ?>"><?php echo $zonesName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Region</label>
                                                <select id="region" name="region" class="form-control">
                                                    <option value="">Select Region</option>
                                                 <?php 
                                                for ($i=0; $i <$count4; $i++) { 
                                                    $regionsName = $regions->rows[$i]['region_name'];
                                                    $regionsName = strtolower($regionsName);
                                                    $regionsName = ucfirst($regionsName);
                                                ?>
                                                    <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div> 
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">City</label>
                                                <select id="city" name="city" class="form-control">
                                                <option value="">Select City</option>
                                                <?php 
                                                for ($i=0; $i <$count3; $i++) { 
                                                    $cityName = $city->rows[$i]['city_name'];
                                                ?>
                                                    <option value="<?php echo $cityName ?>"><?php echo $cityName ?></option>
                                                <?php 
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Area</label>
                                                <select id="area" name="area" class="form-control">
                                                <option value="">Select Area</option>
                                                <option value="North Nazimabad">North Nazimabad</option>
                                                </select>
                                            </div>
                                            
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Town</label>
                                                <select id="town" name="town" class="form-control">
                                                <option value="">Select Town</option>
                                                <option value="North Nazimabad">North Nazimabad</option>
                                                </select>
                                            </div> 
                                           
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Market</label>
                                                <input type="text" class="form-control" id="market" name="market" placeholder="" d   value="" >
                                            </div>
                                           
                                            
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Login ID</label>
                                                <input type="text" class="form-control" id="loginid" name="loginid" placeholder="" d   value="" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Password</label>
                                                <input type="text" class="form-control" id="password" name="password" placeholder="" d   value="" >
                                            </div>
                                </div>
                              
                   
                    <div style="margin-top: 20px;"></div>
                    <div class="form-row">
                        <div class="col-md-9  mb-3">
                        </div>
                        <div class="col-md-1  mb-3">
                        </div>
                        <div class="col-md-2  mb-3" >

                            <button type="button" onclick='AddUser()' class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Add
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('viewuser')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
</main>

<script type="text/javascript">

function viewsubcategory(id)
{
        //alert(id);
    if(id == '')
    {
      alert("No ID found!!");
    }
    else
    {
        o =new Array(id);
        o = JSON.encode(o);
        var pars = 'param='+o;
        var url = "/index.php?object=user&function=GetSubCategory&isajaxcall=1";
        var myAjax = new Ajax.Request( url,
        { method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetSubCategoryCallBack});

    }
}

function GetSubCategoryCallBack(response)
{
    var obj = JSON.decode(response.responseText);
    document.getElementById('subdealer').innerHTML = obj;
}
function  CheckError()
{
    console.log('error');
}

    
function AddUser() {
        if(confirm("Are you sure you want to add this user?"))
        {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('adduserform');   
        PostAjaxScreen("adduserform",form,callfunc);
        }
}

function NextPage(){
      LoadAjaxScreen("viewuser");
}
</script>

<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>



////////////////




<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/viewuser.php");

    $userId=$_GET['id'];
    $user = new BL_User();
    $dealerCatergory = $user->GetDealerCatergory();
    $count =  $dealerCatergory->count;

    $subDealerCatergory = $user->GetSubDealerCatergory();
    $count1 =  $subDealerCatergory->count;

    $groups = $user->GetGroups();
    $count2 =  $groups->count;

    $city = $user->GetCity();
    $count3 =  $city->count;

    $regions = $user->GetRegions();
    $count4 =  $regions->count;

    $zones = $user->GetZones();
    $count5 =  $zones->count;

    $viewUser=new BL_ViewUser();
    $fetchData=$viewUser->ViewUserInformation($userId);
    $countViewUser =  $fetchData->count;
for ($i=0; $i <$countViewUser; $i++) { 
    $userId = $fetchData->rows[$i]['user_id'];
    $userDealerCatergoryId = $fetchData->rows[$i]['dealer_category_id'];
    $userSubCategoryId = $fetchData->rows[$i]['sub_category_id'];
    $userGroupId = $fetchData->rows[$i]['group_id'];
    $password = $fetchData->rows[$i]['password'];
     $fullName = $fetchData->rows[$i]['full_name'];
    $regCode = $fetchData->rows[$i]['reg_code'];
    $cnic = $fetchData->rows[$i]['cnic'];
    $sapCode = $fetchData->rows[$i]['sap_code'];
    $market = $fetchData->rows[$i]['market'];
    $mobNumber = $fetchData->rows[$i]['mobile_no'];
    $loginId = $fetchData->rows[$i]['login_id'];
    $email = $fetchData->rows[$i]['email'];
    $address = $fetchData->rows[$i]['address'];
    $userArea = $fetchData->rows[$i]['area'];
    $town = $fetchData->rows[$i]['town'];
    $Userzone = $fetchData->rows[$i]['zone'];                          
    $userRegion = $fetchData->rows[$i]['region'];                          
    $UserCity = $fetchData->rows[$i]['city'];                          
    $created_at = $fetchData->rows[$i]['created_at'];       
    $Date=substr($created_at,0,11);
    $Date=date_create($Date);                     
    $Date = date_format($Date,"d-M-Y");                    
}
?>



<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Edit User
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>

            <form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
            <input type='hidden' name='ACTION' value='UPDATEUSER'>
            <input type='hidden' class="form-control" id="userID" name="userID" placeholder="" value="<?php echo $userId ?>" >
            <div class="row" style="margin-top: -30px;">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5>User Detail</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Registration Number</label>
                                                <input type="text" class="form-control" id="registration" name="registration" placeholder=""    value="<?php echo $regCode ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Sap Code</label>
                                                <input type="text" class="form-control" id="sapcode" name="sapcode" placeholder=""    value="<?php echo $sapCode ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Dealer Catergory</label>
                                                <select id="dealer" name="dealer" class="form-control">
                                                <option value="">Select Dealer Catergory</option>
                                                <?php 
                                                for ($i=0; $i <$count; $i++) { 
                                                    $dealerCatergoryId = $dealerCatergory->rows[$i]['dealer_category_id'];
                                                    $dealerCatergoryName = $dealerCatergory->rows[$i]['dealer_category_name'];
                                                    $dealerCatergoryName = strtolower($dealerCatergoryName);
                                                    $dealerCatergoryName = ucfirst($dealerCatergoryName);

                                                if ($userDealerCatergoryId==$dealerCatergoryId) {
                                                ?>
                                                    <option value="<?php echo $dealerCatergoryId ?>" selected><?php echo $dealerCatergoryName ?></option>
                                                <?php 
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="<?php echo $dealerCatergoryId ?>"><?php echo $dealerCatergoryName ?></option>
                                                <?php  
                                                }
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Sub Dealer Catergory</label>
                                                <select id="subdealer" name="subdealer" class="form-control">
                                                <option value="">Select Sub Dealer Catergory</option>
                                                <?php 
                                                for ($i=0; $i <$count1; $i++) { 
                                                    $subDealerCatergoryId = $subDealerCatergory->rows[$i]['sub_category_id'];
                                                    $subDealerCatergoryName = $subDealerCatergory->rows[$i]['sub_category_name'];
                                                if ($userSubCategoryId==$subDealerCatergoryId) {
                                                ?>
                                                    <option value="<?php echo $subDealerCatergoryId ?>" selected><?php echo $subDealerCatergoryName ?></option>
                                                <?php 
                                                }
                                                else{
                                                   ?>
                                                    <option value="<?php echo $subDealerCatergoryId ?>"><?php echo $subDealerCatergoryName ?></option>
                                                <?php  
                                                }
                                                }
                                                ?>
                                                </select>
                                            </div>
                                            
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Name</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder=""   value="<?php echo $fullName ?>" >
                                            </div>
                                            <!-- <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Gender</label>
                                                <select id="gender" name="gender" class="form-control">
                                                <option value="">Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                </select>
                                            </div> -->
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">CNIC</label>
                                                <input type="text" class="form-control" id="cnic" name="cnic" placeholder="" d   value="<?php echo $cnic ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Group</label>
                                                <select id="group" name="group" class="form-control">
                                                <option value="">Select Group</option>
                                                <?php 
                                                for ($i=0; $i <$count2; $i++) { 
                                                    $groupId = $groups->rows[$i]['group_id'];
                                                    $groupName = $groups->rows[$i]['group_name'];
                                                if ($userGroupId==$groupId) {
                                                ?>
                                                    <option value="<?php echo $groupId ?>" selected><?php echo $groupName ?></option>
                                                <?php 
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="<?php echo $groupId ?>"><?php echo $groupName ?></option>
                                                <?php 
                                                }
                                                }
                                                ?>
                                                </select>
                                            </div>

                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Joining Date</label>
                                                <input type="text" class="form-control" id="joiningdate" name="joiningdate" placeholder="" d   value="<?php echo $Date ?>" >
                                            </div>
                                </div>
                                <div class="form-row">

                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Mobile Number</label>
                                                <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="" d   value="<?php echo $mobNumber ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Email</label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="" d   value="<?php echo $email ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Address</label>
                                                <input type="text" class="form-control" id="address" name="address" placeholder="" d   value="<?php echo $address ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Country</label>
                                                <select id="country" name="country" class="form-control">
                                                <option value="">Select Country</option>
                                                <option value="Pakistan" selected>Pakistan</option>
                                                </select>
                                            </div>

                                            
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Zone</label>
                                                <select id="zone" name="zone" class="form-control">
                                                    <option value="">Select Zone</option>
                                                 <?php 
                                                for ($i=0; $i <$count5; $i++) { 
                                                    $zonesName = $zones->rows[$i]['zone_name'];
                                                    $zonesName = strtolower($zonesName);
                                                    $zonesName = ucfirst($zonesName);
                                                    if ($Userzone==$zonesName) {
                                                ?>
                                                    <option value="<?php echo $zonesName ?>" selected><?php echo $zonesName ?></option>
                                                <?php 
                                                }
                                                else
                                                {
                                                  ?>
                                                    <option value="<?php echo $zonesName ?>"><?php echo $zonesName ?></option>  
                                                    <?php
                                                }
                                            }
                                                ?>
                                                </select>
                                            </div>
                                     <div class="col-md-3 mb-3">

                                                <label class="form-label" for="">Region</label>
                                                <select id="region" name="region" class="form-control">
                                                    <option value="">Select Region</option>
                                                 <?php 
                                                for ($i=0; $i <$count4; $i++) { 
                                                    $regionsName = $regions->rows[$i]['region_name'];
                                                    $regionsName = strtolower($regionsName);
                                                    $regionsName = ucfirst($regionsName);
                                                    if ($userRegion==$regionsName) {
                                                ?>
                                                    <option value="<?php echo $regionsName ?>" selected><?php echo $regionsName ?></option>
                                                <?php 
                                                }
                                                else{
                                                    ?>
                                                    <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                                                <?php 
                                                }
                                            }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">City</label>
                                                <select id="city" name="city" class="form-control">
                                                <option value="">Select City</option>
                                                <?php 
                                                for ($i=0; $i <$count3; $i++) { 
                                                    $cityName = $city->rows[$i]['city_name'];
                                                if ($UserCity==$cityName) {                                   
                                                ?>
                                                    <option value="<?php echo $cityName ?>" selected><?php echo $cityName ?></option>
                                                <?php 
                                                }
                                                else
                                                {
                                                ?>
                                                    <option value="<?php echo $cityName ?>" ><?php echo $cityName ?></option>
                                                <?php 
                                                }
                                                }

                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Area</label>
                                                <select id="area" name="area" class="form-control">
                                                <option value="">Select Area</option>
                                                <option value="North Nazimabad" selected>North Nazimabad</option>
                                                </select>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Town</label>
                                                <select id="town" name="town" class="form-control">
                                                <option value="">Select Town</option>
                                                <option value="North Nazimabad" selected>North Nazimabad</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Market</label>
                                                <input type="text" class="form-control" id="market" name="market" placeholder="" d   value="<?php echo $market ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Login ID</label>
                                                <input type="text" class="form-control" id="loginid" name="loginid" placeholder="" d   value="<?php echo $loginId ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="">Password</label>
                                                <input type="text" class="form-control" id="password" name="password" placeholder="" d   value="<?php echo $password ?>" >
                                            </div>
                                </div>
                              
                   
                    <div style="margin-top: 20px;"></div>
                    <div class="form-row">
                        <div class="col-md-9  mb-3">
                        </div>
                        <div class="col-md-1  mb-3">
                        </div>
                        <div class="col-md-2  mb-3" >

                            <button type="button" onclick='UpdateUser()' class="btn btn-med btn-warning">
                                <span class="fal fa fa-arrow-right mr-1"></span>
                                Update
                            </button>
                            <button type="button" onClick="LoadAjaxScreen('viewuser')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                        </div>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
</main>

<script type="text/javascript">
    
function UpdateUser() {
        if(confirm("Are you sure you want to update this user?"))
        {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('adduserform');   
        PostAjaxScreen("adduserform",form,callfunc);
        }
}

function NextPage(){
      LoadAjaxScreen("viewuser");
}
</script>

<script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>