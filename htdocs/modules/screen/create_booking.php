<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/viewuser.php");
require_once(MODULE."/class/bllayer/addstuff.php");

$userId=$mysession->getvalue('userid');;
/*
$fcmId="dSTfHIW5QRqp6jy1FnsoBj:APA91bEq6jvjjVY2ulScjoeJ9AB_glHqou2HZW2cudxQqnyq_kQCv739TYW6JwU57sYu1xhIC1R7ewduVqgJ_ceDRwvcsXVMnXLK-x_Pgq8moFN7mLQTd_W7s8myLORcIrY7OHitjghX";
$body="test body";
$title="test title";
$objId="1";
$type="general";
SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId,"customer");
*/
$blUser=new BL_User();
$driverInfo = $blUser->GetUserInfoByUserType(USER_TYPE_DRIVER);

$routes = new BL_AddStuff();
$routesInfo = $routes->GetRoute();

// print_r_pre($driverInfo);
//echo "U:$userStatus";
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title" >
Add  Booking
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
 
<form class="form-horizontal" name='createbookingform' id='createbookingform' method='post'>
<input type='hidden'  name='street_number' id='street_number' value=''>
<input type='hidden' name='route' id='route' value=''>
<input type='hidden' name='locality' id='locality' value=''>
<input type='hidden' name='administrative_area_level_1' id='administrative_area_level_1' value=''>
<input type='hidden' name='country' id='country' value=''>
<input type='hidden' name='postal_code' id='postal_code' value=''>
<input type='hidden' name='ACTION' value='CREATE_BOOKING'>
<input type='hidden' class="form-control" id="userID" name="userID" placeholder="" value="<?php echo $userId ?>" >
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        
		
		
        <div class="form-row">
                    <div class="col-md-4 mb-4" >
						<label class="form-label" for="">Select Driver (Only Active and Online Drivers) </label>
						<select id="driver_id" name="driver_id" class="form-control">
              <option value=""  selected>Select Driver</option>
							<?
							for($x=0;$x<$driverInfo->count;$x++)
							{
								$driverId = $driverInfo->rows[$x]['user_id'];
								$driverName = $driverInfo->rows[$x]['full_name'];
								$status = $driverInfo->rows[$x]['status'];
								$shiftStatus = $driverInfo->rows[$x]['shift_status'];
								
								if($status!=1)
									continue;
								if($shiftStatus!='online')
									continue;
							?>
						   <option value="<?=$driverId?>"><?=$driverName?></option>
						  <?
							}
							?>
						
						</select>
					</div> 
                
                    <div class="col-md-4 mb-4">
                        <label class="form-label" for="">Booking Date</label>
                        <input type="text" class="form-control" id="bookingdate" name="bookingdate" placeholder=""    value="<?=date('Y-m-d') ?>" readonly>
                    </div>
        </div>
        <div class="form-row">
				<div class="col-md-4 mb-4">
						<label class="form-label" for="">Select Route</label>
					<select id="route" name="route" class="form-control">
            <option value="" disabled selected>Select Route</option>
                   <?
					   
					for($x=0;$x<$routesInfo->count;$x++)
					{
						$fromCity = $routesInfo->rows[$x]['from_city_name'];
						$toCity = $routesInfo->rows[$x]['to_city_name'];
						$rates = $routesInfo->rows[$x]['rates'];
						$fromToValue = $fromCity."-".$toCity."-".$rates;
						$fromToText = $fromCity."-".$toCity;
					?>
				   
						<option value="<?=$fromToValue?>"><?=$fromToText?></option>
					<?
					}
					?>
					</select>
				</div>	
                    
                    <div class="col-md-4 mb-4">
                        <label class="form-label" for="" >Pick Up Address (Auto complete)</label>
                        <input type="text" class="form-control" onFocus="geolocate()" id='autocomplete' name="autocomplete" placeholder="Select Location"   value="" >
                    </div>
                       
        </div>
       <!--
         <div class="form-row">

                    <div class="col-md-9 mb-9">
                        <label class="form-label" for="">Instruction</label>
                        <input type="text" class="form-control" id="instruction" name="instruction" placeholder=""   value="<?php echo $mobNumber ?>" >
                    </div>
                   
                       
        </div>-->
       
        
        
      

<div style="margin-top: 20px;"></div>
<div class="form-row">
<div class="col-md-8  mb-3">
</div>
<div class="col-md-2  mb-3">

    
    
</div>
<div class="col-md-2  mb-3">

    <button type="button" onclick='AddBooking()' class="btn btn-med btn-warning" style="background: #ff8000">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Add Booking
    </button>
   
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>
<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button " style="background: #ff8000">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
  
<script type="text/javascript">
function AddBooking() {
	if(document.getElementById('driver_id').value=='')
	{
		alert("Please select driver");
		return false;
	}
	if(document.getElementById('autocomplete').value=='')
	{
		alert("Please provide pickup address");
		return false;
	}
	if(confirm("Are you sure you want to ceate this booking?"))
	{
		var callfunc={onSuccess:ReLoad};
		form= document.getElementById('createbookingform');   
		PostAjaxScreen("createbookingform",form,callfunc);
	}
}
function ReLoad(){
	alert('Your booking created successfully');
	//LoadAjaxScreen("create_booking");
}

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'long_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};
function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById("autocomplete"),
    { types: ["geocode"] }
  );
  
  autocomplete.setComponentRestrictions({
    country: ["pk"],
  });
  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(["address_component"]);
  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener("place_changed", fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  const place = autocomplete.getPlace();

  for (const component in componentForm) {
  //debugger;
    document.getElementById(component).value = "";
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (const component of place.address_components) {
    const addressType = component.types[0];

    if (componentForm[addressType]) {
      const val = component[componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
	initAutocomplete()
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      const geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      const circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy,
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>