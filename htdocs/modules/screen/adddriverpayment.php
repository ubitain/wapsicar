<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/documents.php");


$bldocument = new BL_Documents();
$documentInfo = $bldocument->GetDocumentListing();

//print_r($documentInfo);
?>
<main id="js-page-content" role="main" class="page-content">

<form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
<input type='hidden' name='ACTION' value='ADDDRIVERPAYMENT'>
<input type='hidden' name='userid' value='<?=$userid?>'>

<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
      

          <div class="form-row">
            <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">Transaction #</label>
                        <input type="text" class="form-control" id="transaction_number" name="transaction_number" multiple="true" placeholder=""    value="" >
                    </div>
          </div>
         <div class="form-row">
            <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">Payment</label>
                        <input type="text" class="form-control" id="payment" name="payment" multiple="true" placeholder=""    value="" >
                    </div>
          </div>
         <div class="form-row">
            <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">Description</label>
                        <input type="text" class="form-control" id="description" name="description" multiple="true" placeholder=""    value="" >
                    </div>
          </div>
        
        <div class="form-row">
                    <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">Status</label>
                         <select id="status" name="status" class="form-control"  >
                         <option value="pending">Pending</option>
                         <option value="conflict">Conflict</option>
                         <option value="approved">Approved</option>
                        
                    </select>
                     
                    </div>
                    
                   
        </div>
      

<div style="margin-top: 20px;"></div>
<div class="form-row">

<div class="col-md-12  mb-12" >

    <button type="button" onClick="LoadAjaxScreen('viewuser')" class="btn btn-med btn-dark">
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
    <button type="button" onclick='AddDriverPayment()' class="btn btn-med btn-warning">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Add
    </button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>



<script type="text/javascript">


function AddDriverPayment() {


    if(confirm("Are you sure you want to add this payment?"))
    {
    var callfunc={onSuccess:NextPage};
    form= document.getElementById('adduserform');   
    PostFileAjaxScreen("adduserform",form,callfunc);
    }

}
function ValidateLeadData()
{
    name=document.getElementById('name').value;
    var illegalCharacters = name.match(/[^a-zA-Z- ]/g);
    if (illegalCharacters) {
        $("#name").focus();
        alert('Wrong Name!!');
        return false;
    }
    email=document.getElementById('email').value;
    var emailregex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (emailregex.test(email) == false){
        alert("Wrong Email! (Hint:abc@gmail.com)");
        return false;
    }
}

function NextPage(){
CloseModalBox();
LoadAjaxScreen("showuserdetail&id=<?=$userid?>");
}

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;
var fromdate = today;
document.getElementById("joiningdate").max = fromdate;
//////////////////////////////


$("#name").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#name").val().length >35 ) {
            return false;
       }



});

$("#cnic").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#cnic").val().length >12 ) {
            return false;
       }



});

$("#email").keypress(function(e){

      if( $("#email").val().length >50 ) {
            return false;
        }

  });

$("#phonenumber").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#phonenumber").val().length >10 ) {
            return false;
       }
});

$("#registration").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#registration").val().length >35 ) {
            return false;
       }
});

$("#sapcode").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#sapcode").val().length >35 ) {
            return false;
       }
});

</script>
