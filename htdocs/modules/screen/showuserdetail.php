<?php
    require_once(MODULE."/class/bllayer/viewuser.php");
    require_once(MODULE."/class/bllayer/documents.php");
    require_once(MODULE."/class/bllayer/booking.php");
    require_once(MODULE."/class/bllayer/user.php");

    //echo "here...";
    $bldocuments = new BL_Documents();
    $documentInfo = $bldocuments->GetUserDocuments($id);

    $bluser = new BL_User();
    $userInfo = $bluser->GetUserInfo($id)->rows[0];

    $ridesInfo = $bluser->GetDriverRidesInfo($id);
    $paymentInfo = $bluser->GetPaymentInfo($id);
    $userCityRateCard = $bluser->GetCityRateCard($id);


    global $paymentStatus;
   //print_r_pre($userCityRateCard);     
   // $userId = $mysession->getValue("userid");
    $blBooking = new BL_Booking();
	$ret = $blBooking->GetDriverDashboard($id);
		
	$driverEarnings = ($ret['total_earnings']!="")?$ret['total_earnings']:"0";
	$driverRides = ($ret['total_rides']!="")?$ret['total_rides']:"0";;
	

?>
   <link rel="stylesheet" media="screen, print" href="themes/smartadmin/css/miscellaneous/lightgallery/lightgallery.bundle.css">
<link rel="stylesheet" href="themes/smartadmin/css/fa-brands.css" />
<main id="js-page-content" role="main" class="page-content">
                        <ol class="breadcrumb page-breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Application</a></li>
                            <li class="breadcrumb-item">User</li>
                            <li class="breadcrumb-item active">Profile</li>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date">Monday, September 7, 2020</span></li>
                        </ol>
                       
                        <div class="row">
                            <div class="col-lg-12 col-xl-6 order-lg-1 order-xl-1">
                                <!-- profile summary -->
                                <div class="card mb-g rounded-top">
                                    <div class="row no-gutters row-grid">
                                        <div class="col-12">
                                            <div class="d-flex flex-column align-items-center justify-content-center p-4">
                                                <img src="img/demo/avatars/avatar-admin-lg.png" class="rounded-circle shadow-2 img-thumbnail" alt="">
                                                <h5 class="mb-0 fw-700 text-center mt-3" onclick="LoadAjaxScreen('showuserdetail&id=<?=$id?>')">
                                                    <?=$userInfo['full_name'];?>
                                                    <small class="text-muted mb-0"><?=$userInfo['address'];?></small>
                                                </h5>
                                                
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-center py-3">
                                                <h5 class="mb-0 fw-700">
                                                    <?=$driverRides?>
                                                    <small class="text-muted mb-0">Rides</small>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="text-center py-3">
                                                <h5 class="mb-0 fw-700">
                                                    <?=$driverEarnings?>
                                                    <small class="text-muted mb-0">Earning</small>
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                           <table class="table table-striped m-0">
                                            <tbody>
                                               <tr>
                                                   <td>NIC :</td>
                                                   <td><?=$userInfo['cnic']?></td>
                                               </tr>
                                               <tr>
                                                   <td>NIC Expiry:</td>
                                                   <td><?=$userInfo['cnic_expiry_date']?></td>
                                               </tr>
                                               <tr>
                                                   <td>Email:</td>
                                                   <td><?=$userInfo['email_address']?></td>
                                               </tr>
                                               <tr>
                                                   <td>Phone:</td>
                                                   <td><?=$userInfo['phone_number']?></td>
                                               </tr>
                                               <tr>
                                                   <td>Address:</td>
                                                   <td><?=$userInfo['address']?></td>
                                               </tr>
                                               <tr>
                                                   <td>Account Name:</td>
                                                   <td><?=$userInfo['account_name']?></td>
                                               </tr>
                                               <tr>
                                                   <td>Account Number:</td>
                                                   <td><?=$userInfo['account_no']?></td>
                                               </tr>
                                               </tbody>
                                           </table>
                                        </div>
                                        <div class="col-12">
                                            <div class="p-3 text-center">
                                               
                                                <a href="javascript:void(0);" onclick="LoadAjaxScreen('edituser&id=<?=$id?>')" class="btn-link font-weight-bold">Edit</a>
                                                <span class="text-primary d-inline-block mx-3">●</span>
                                                <a href="#" onclick="OpenModalBox('adduserdocuments&userid=<?=$id?>','screen')" class="btn-link font-weight-bold">Add Document</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- photos -->
                                <div class="card mb-g show">
                                    <div class="row row-grid no-gutters">
                                        <div class="col-12">
                                            <div class="p-3">
                                                <h2 class="mb-0 fs-xl">
                                                    Documents
                                                </h2>

                                            </div>
                                        </div>
 <div id="js-lightgallery">

                                        <?php $document_display = "no"; ?> 
                                        <?php
                                        $a=0;
                                         for($i=0;$i<count($documentInfo);$i++){ 
                                          $a = str_replace("http://".SERVER_NAME, BASE_PATH, $documentInfo[$i]['document_path']);
                                          $a = file_exists($a);
                                            ?>

                                        

                                        <?php if($documentInfo[$i]['document_path'] == "" || $a!=1) continue; 

                                            $document_display = "yes";
                                        ?>


                                        <!-- <div class="col-4" > -->
                                            <a href="<?=$documentInfo[$i]['document_path']?>" class="">
                                                <img class="img-responsive" src="<?=$documentInfo[$i]['document_path']?>" alt="image">
                                            </a>
                                        <!-- </div> -->
                                    <?php 
                                    $a++;
                                  } 


                                    ?>
                                </div>
                                        <?php if($document_display == "no"){ ?>
                                        <div class="col-12">
                                            <div class="p-3 text-center">
                                                <a href="javascript:void(0);" class="btn-link font-weight-bold">No Document(s) </a>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="col-lg-12 col-xl-9 order-lg-6 order-xl-2">
                                <div class="card border mb-g">
                                    <div class="card-body pl-4 pt-4 pr-4 pb-0">
                                         <h5> User City Rated  </h5>
                                           
                                                <table class="table table-striped m-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>From City</th>
                                                            <th>To City</th>
                                                           <th>ATS Rate</th>
														    <th>Driver Rate</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php for($i=0;$i<$userCityRateCard->count;$i++){  ?>
                                                        <tr>
                                                            <th scope="row"><?=$i+1?></th>
                                                            <td><?=$userCityRateCard->rows[$i]['from_city_name']?></td>
                                                            <td><?=$userCityRateCard->rows[$i]['to_city_name']?></td>
                                                            <td><?=$userCityRateCard->rows[$i]['rates']?></td>
                                                            <td><?=$userCityRateCard->rows[$i]['driver_rates']?></td>
                                                        </tr>
                                                    <?php } ?>
                                                       
                                                       
                                                    </tbody>
                                                </table>
                                    </div>
                                </div>
                                 <div class="card border mb-g">
                                    <div class="card-body pl-4 pt-4 pr-4 pb-0">
                                         <h5> User Payment  
                                          <input type="button" onclick="OpenModalBox('adddriverpayment&userid=<?=$id?>','screen')" class="btn btn-med btn-warning pull-right" value="Add Payment" />

                                          </h5>
                                           
                                                <table class="table table-striped m-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Transaction Number</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php for($i=0;$i<$paymentInfo->count;$i++){ ?>
                                                        <tr>
                                                            <th scope="row"><?=$i+1?></th>
                                                            <td><?=$paymentInfo->rows[$i]['transaction_number']?></td>
                                                            <td><?=$paymentInfo->rows[$i]['payment']?></td>
                                                            <td><?=$paymentStatus[$paymentInfo->rows[$i]['status']]?></td>
                                                            
                                                        </tr>
                                                    <?php } ?>
                                                        
                                                        
                                                    </tbody>
                                                </table>
                                    </div>
                                </div>

                                 <div class="card border mb-g">
                                    <div class="card-body pl-4 pt-4 pr-4 pb-0">
                                         <h5> User Rides  </h5>
                                           
                                                <table class="table table-striped m-0">
                                                    <thead>

                                                        <tr>
                                                            <th>#</th>
                                                            <th>Customer Name</th>
                                                            <th>From</th>
                                                            <th>To</th>
                                                            <th>Booking Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php for($i=0;$i<$ridesInfo->count;$i++){ ?>
                                                        <tr>
                                                             <th scope="row"><?=$i+1?></th>
                                                            <td><?=$ridesInfo->rows[$i]['full_name']?></td>
                                                            <td><?=$ridesInfo->rows[$i]['pickup_location']?></td>
                                                            <td><?=$ridesInfo->rows[$i]['dropoff_location']?></td>
                                                            <td><?=$ridesInfo->rows[$i]['dropoff_location']?></td>
                                                            <td><?=date("F j, Y, g:i a",strtotime($ridesInfo->rows[$i]['created_at']))?></td>
                                                        </tr>
                                                    <?php } ?>
                                                       
                                                        
                                                    </tbody>
                                                </table>
                                    </div>
                                </div>
                                
                              
                            </div>


                           
                          
                          <!--  <div class="col-lg-6 col-xl-3 order-lg-2 order-xl-3">
                               
                                <div class="card mb-2">
                                    <div class="card-body">
                                        <a href="javascript:void(0);" class="d-flex flex-row align-items-center">
                                            <div class="icon-stack display-3 flex-shrink-0">
                                                <i class="fal fa-circle icon-stack-3x opacity-100 color-primary-400"></i>
                                                <i class="fas fa-graduation-cap icon-stack-1x opacity-100 color-primary-500"></i>
                                            </div>
                                            <div class="ml-3">
                                                <strong>
                                                    Add Qualifications
                                                </strong>
                                                <br>
                                                Adding qualifications will help gain more clients
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="card mb-g">
                                    <div class="card-body">
                                        <a href="javascript:void(0);" class="d-flex flex-row align-items-center">
                                            <div class="icon-stack display-3 flex-shrink-0">
                                                <i class="fal fa-circle icon-stack-3x opacity-100 color-warning-400"></i>
                                                <i class="fas fa-handshake icon-stack-1x opacity-100 color-warning-500"></i>
                                            </div>
                                            <div class="ml-3">
                                                <strong>
                                                    Add Skills
                                                </strong>
                                                <br>
                                                Gain more potential clients by adding skills
                                            </div>
                                        </a>
                                    </div>
                                </div>
                             
                            </div>

                        -->
                        </div>
                    </main>
                    <p id="js-color-profile" class="d-none">
                        <span class="color-primary-50"></span>
                        <span class="color-primary-100"></span>
                        <span class="color-primary-200"></span>
                        <span class="color-primary-300"></span>
                        <span class="color-primary-400"></span>
                        <span class="color-primary-500"></span>
                        <span class="color-primary-600"></span>
                        <span class="color-primary-700"></span>
                        <span class="color-primary-800"></span>
                        <span class="color-primary-900"></span>
                        <span class="color-info-50"></span>
                        <span class="color-info-100"></span>
                        <span class="color-info-200"></span>
                        <span class="color-info-300"></span>
                        <span class="color-info-400"></span>
                        <span class="color-info-500"></span>
                        <span class="color-info-600"></span>
                        <span class="color-info-700"></span>
                        <span class="color-info-800"></span>
                        <span class="color-info-900"></span>
                        <span class="color-danger-50"></span>
                        <span class="color-danger-100"></span>
                        <span class="color-danger-200"></span>
                        <span class="color-danger-300"></span>
                        <span class="color-danger-400"></span>
                        <span class="color-danger-500"></span>
                        <span class="color-danger-600"></span>
                        <span class="color-danger-700"></span>
                        <span class="color-danger-800"></span>
                        <span class="color-danger-900"></span>
                        <span class="color-warning-50"></span>
                        <span class="color-warning-100"></span>
                        <span class="color-warning-200"></span>
                        <span class="color-warning-300"></span>
                        <span class="color-warning-400"></span>
                        <span class="color-warning-500"></span>
                        <span class="color-warning-600"></span>
                        <span class="color-warning-700"></span>
                        <span class="color-warning-800"></span>
                        <span class="color-warning-900"></span>
                        <span class="color-success-50"></span>
                        <span class="color-success-100"></span>
                        <span class="color-success-200"></span>
                        <span class="color-success-300"></span>
                        <span class="color-success-400"></span>
                        <span class="color-success-500"></span>
                        <span class="color-success-600"></span>
                        <span class="color-success-700"></span>
                        <span class="color-success-800"></span>
                        <span class="color-success-900"></span>
                        <span class="color-fusion-50"></span>
                        <span class="color-fusion-100"></span>
                        <span class="color-fusion-200"></span>
                        <span class="color-fusion-300"></span>
                        <span class="color-fusion-400"></span>
                        <span class="color-fusion-500"></span>
                        <span class="color-fusion-600"></span>
                        <span class="color-fusion-700"></span>
                        <span class="color-fusion-800"></span>
                        <span class="color-fusion-900"></span>
                    </p>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteUser(userID)
{
    var r = confirm("Are you sure you want to delete this user?");
    if(r==true)
    {
    o =new Array(userID);
    o = JSON.encode(o);
    var pars = 'param='+o;
    var url = "/index.php?object=user&function=deleteduser&isajaxcall=1&returnType=string";
    var myAjax = new Ajax.Request( url,
    { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
    }
    else
    {
      return;
    }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("viewuser");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  LoadAjaxScreen("viewuser");
}
</script>

<script src="themes/smartadmin/js/vendors.bundle.js"></script>
<script src="themes/smartadmin/js/app.bundle.js"></script>

<script src="themes/smartadmin/js/datagrid/datatables/datatables.bundle.js"></script>
<script src="themes/smartadmin/js/miscellaneous/lightgallery/lightgallery.bundle.js"></script>
        <script>
       
            
       


$(document).ready(function()
{
$('#dt-basic-example').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});


var $initScope = $('#js-lightgallery');
                if ($initScope.length)
                {
                    $initScope.justifiedGallery(
                    {
                        border: -1,
                        rowHeight: 150,
                        margins: 8,
                        waitThumbnailsLoad: true,
                        randomize: false,
                    }).on('jg.complete', function()
                    {
                        $initScope.lightGallery(
                        {
                            thumbnail: true,
                            animateThumb: true,
                            showThumbByDefault: true,
                        });
                    });
                };
                $initScope.on('onAfterOpen.lg', function(event)
                {
                    $('body').addClass("overflow-hidden");
                });
                $initScope.on('onCloseAfter.lg', function(event)
                {
                    $('body').removeClass("overflow-hidden");
                });

});
</script>

