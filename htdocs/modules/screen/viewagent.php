<?php
 require_once MODULE."/class/bllayer/user.php";
 $blUser = new BL_User();
 $fetchData = $blUser->GetAgent();
    
?>

<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
View Agents
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">
 <!-- datatable start -->
               <table id="dttable" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr align="center">
                         
                            <th align="center">S.No</th>
                            <th align="center">Name</th>
                            <th align="center">Company Name</th>
                            <th align="center">Mobile Number</th>
                            <th align="center">Email</th>
                            <th align="center">Address</th>
                            <th align="center">City</th>
                            <th align="center">Action</th>
                    
                        </tr>
                    </thead>
                    <tbody>

                        <?php

                            for ($i=0; $i <$fetchData->count; $i++) { 
                                $id =$fetchData->rows[$i]['id'];
        


                                //for

                                //print_r_pre($fetchData->rows[$i]);
                                
                                $name = $fetchData->rows[$i]['name'];
                              
                                $mobNumber = $fetchData->rows[$i]['mobile'];
    
                                $email = $fetchData->rows[$i]['email'];
                                $address = $fetchData->rows[$i]['address'];
                                $city =  $fetchData->rows[$i]['city'];                  
                                $companyName =  $fetchData->rows[$i]['company_name'];                  
    
                                //echo "here...";
                        ?>

                                <tr>
                                <td align="center"><?php echo $i+1 ?></td>
                                <td align="center"><?=$name?></td>
                                <td align="center"><?=$companyName?></td>
                                <td align="center"><?=$mobNumber?></td>
                                <td align="center"><?=$email?></td>
                                <td align="center"><?=$address?></td>
                                <td align="center"><?=$city?></td>
                                <td align="center" nowrap><button type="button" class="btn btn-sm btn-warning" onclick="LoadAjaxScreen('agentedit&userid=<?=$id?>')" style="background: #ff8000">
                                  <span class="fal  fa-edit mr-1"></span></button>
                                  <button class=" btn-sm btn-warning" title="Delete"  onclick="Delete('<?=$id?>')" style="background: #ff8000; color: black"><span class="fa fa-trash-o mr-1" style="color: black"></span></button></td>
                                

                               
                                </tr>
                            <?php
                                }
                            ?>
                       
                    </tbody>
                </table>
                
</div>
</div>

</div>
</div>
</div>


</main>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>
                $(document).ready(function()
            {
                // Setup - add a text input to each footer cell
                $('#dttable thead tr').clone(true).appendTo('#dttable thead');
                $('#dttable thead tr:eq(1) th').each(function(i)
                {
                    var title = $(this).text();
                    $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');

                    $('input', this).on('keyup change', function()
                    {
                        if (table.column(i).search() !== this.value)
                        {
                            table
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                });

                var table = $('#dttable').DataTable(
                {
                    responsive: true,
                    orderCellsTop: true,
                    lengthChange: false,
                   // fixedHeader: true,

                    dom:
                        /*  --- Layout Structure 
                            --- Options
                            l   -   length changing input control
                            f   -   filtering input
                            t   -   The table!
                            i   -   Table information summary
                            p   -   pagination control
                            r   -   processing display element
                            B   -   buttons
                            R   -   ColReorder
                            S   -   Select

                            --- Markup
                            < and >             - div element
                            <"class" and >      - div with a class
                            <"#id" and >        - div with an ID
                            <"#id.class" and >  - div with an ID and a class

                            --- Further reading
                            https://datatables.net/reference/option/dom
                            --------------------------------------
                         */
                        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-12 d-flex align-items-center justify-content-end 'lB>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    buttons: [
                        /*{
                            extend:    'colvis',
                            text:      'Column Visibility',
                            titleAttr: 'Col visibility',
                            className: 'mr-sm-3'
                        },*/
                        {
                            extend: 'pdfHtml5',
                            text: 'PDF',
                            titleAttr: 'Generate PDF',
                            className: 'btn-outline-danger btn-sm mr-1'
                        },
                        {
                            extend: 'excelHtml5',
                            text: 'Excel',
                            titleAttr: 'Generate Excel',
                            className: 'btn-outline-success btn-sm mr-1'
                        },
                        {
                            extend: 'csvHtml5',
                            text: 'CSV',
                            titleAttr: 'Generate CSV',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'copyHtml5',
                            text: 'Copy',
                            titleAttr: 'Copy to clipboard',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            titleAttr: 'Print Table',
                            className: 'btn-outline-primary btn-sm'
                        }
                    ]
                });

            });


      
    function Delete(recordId)
{
  
  var r = confirm("Are you sure you want to delete?");
  if(r==true)
  {
  o = new Array(recordId);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=delete&function=DeleteBranch&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteBranchCallBack});
  }
  else
  {
    return;
  }
}
function DeleteBranchCallBack(response)
{
    alert('Delete Sucessfully');
  LoadAjaxScreen("viewagent");
}
function ReportError(res)
{

}
</script>