<?php
require_once MODULE . "/class/bllayer/user.php";
$blUser = new BL_User();


 echo $fromDate=$_REQUEST['fromDate'];
$fromDate=date("Y-m-d",strtotime($fromDate));
 echo $toDate=$_REQUEST['ToDate'];
 $toDate=date("Y-m-d",strtotime($toDate));
$fetchData = $blUser->ReportAgent($fromDate,$toDate);
?>

<link rel="stylesheet" media="screen, print" href="css/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.css">
<div class="form-group row">
    <title></title>


    <main id="js-page-content" role="main" class="page-content">

        <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
                Agent Commission
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
        </ol>
        <div class="row" style="margin-top: -30px;">
            <div class="col-xl-12">
                <div id="panel-1" style="" class="panel">
                    
                    <div class="panel-container show">
                        <div class="panel-content" style="margin-top: 10px;">
                            <!-- <form id="date"> -->
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label">Agent Report  </label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" id="datepicker-1" placeholder="Select date" value="09/01/2021 - 09/15/2021">
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn btn-primary" style="background: #ff8000" onclick="search()">Search</button>
                                    </div>
                                </div>
                            <!-- </form> -->

                            <!-- datatable start -->
                            <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                <thead>
                                    <tr align="center">

                                        <th>Agent Name</th>
                                        <th>Total Booking</th>
                                        <th>Price</th>
                                        <th>Commission</th>
                                        <th>Agent Details</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <?php

                                    for ($i = 0; $i < $fetchData->count; $i++) {
                                        $id = $fetchData->rows[$i]['driver_id'];
                                        $agentId = $fetchData->rows[$i]['agent_id'];
                                        $driverName = $fetchData->rows[$i]['name'];
                                        // $driverId = $fetchData->rows[$i]['agent'];
                                        $totalBooking = $fetchData->rows[$i]['COUNT(record_id)'];
                                        $price = $fetchData->rows[$i]['price'];
                                        $comission = $fetchData->rows[$i]['commission'];
                                    

                                    ?>
                                        <tr>
                                            <td style="text-align: center;"><?= $driverName ?></td>
                                            <!-- <td style="text-align: center;"><?= $driverId ?></td> -->
                                            <td style="text-align: center;"><?= $totalBooking ?></td>
                                            <td style="text-align: center;"><?= $price ?></td>
                                            <td style="text-align: center;"><?= $comission ?></td>
                                            <td style="text-align: center;"><button type="button" onClick="LoadAjaxScreen('agentdriverdetails&agent_id=<?= $agentId ?>')" class="btn btn-primary lg" style="background: #ff8000"><i class="fa fa-eye" aria-hidden="true" style="color: black"></i>
                                                </button></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </main>
    <script src="js/vendors.bundle.js"></script>
    <script src="js/app.bundle.js"></script>
    <script src="js/datagrid/datatables/datatables.bundle.js"></script>
    <script src="js/dependency/moment/moment.js"></script>
    <script src="js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>
    <script>
        $(document).ready(function() {
            $('#dt-basic-example').dataTable({
                responsive: true,
                dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                buttons: [{

                    },
                    {

                    },
                    {

                    },
                    {

                    },
                    {

                    },
                    {

                    },
                    {

                    }

                ],
                select: false
            });
        });
        $(document).ready(function() {
            $('#datepicker-1').daterangepicker({
                opens: 'right'
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });

        function search() {
            date = document.getElementById('datepicker-1').value;
            date1=date.split("-");
            alert(date1);
            
            // console.log();

            fromDate = trim(date1[0]);
            ToDate = trim(date1[1]);
            // console.log(fromDate);
            // console.log(ToDate);
            
            LoadAjaxScreen('agentreport&fromDate='+fromDate+'&ToDate='+ToDate);
         
        }
    </script>