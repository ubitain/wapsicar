<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/viewuser.php");
require_once(MODULE."/class/bllayer/settings.php");

$userId=$_GET['id'];

$blsettings  = new BL_Settings();
$settingInfo = $blsettings->GetSiteSettings();


//print_r_pre($settingInfo);
//die();
/*
$viewUser=new BL_ViewUser();
$fetchData=$viewUser->ViewUserInformation($userId);
$countViewUser =  $fetchData->count;
for ($i=0; $i <$countViewUser; $i++) { 
$userId = $fetchData->rows[$i]['user_id'];
$userDealerCatergoryId = $fetchData->rows[$i]['dealer_category_id'];
$userSubCategoryId = $fetchData->rows[$i]['sub_category_id'];
$userGroupId = $fetchData->rows[$i]['group_id'];
$password = $fetchData->rows[$i]['password'];
$fullName = $fetchData->rows[$i]['full_name'];
$regCode = $fetchData->rows[$i]['reg_code'];
$cnic = $fetchData->rows[$i]['cnic'];
$sapCode = $fetchData->rows[$i]['sap_code'];
$market = $fetchData->rows[$i]['market'];
$mobNumber = $fetchData->rows[$i]['mobile_no'];
$loginId = $fetchData->rows[$i]['login_id'];
$email = $fetchData->rows[$i]['email'];
$address = $fetchData->rows[$i]['address'];
$userArea = $fetchData->rows[$i]['area'];
$town = $fetchData->rows[$i]['town'];
$Userzone = $fetchData->rows[$i]['zone'];                          
$userRegion = $fetchData->rows[$i]['region'];                          
$UserCity = $fetchData->rows[$i]['city'];                          
$accountOf = $fetchData->rows[$i]['account_of'];                         
$created_at = $fetchData->rows[$i]['created_at'];       
$Date=substr($created_at,0,11);
$Date=date_create($Date);                     
$Date = date_format($Date,"d-M-Y");                    
}
*/
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Update Configuration
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
<input type='hidden' name='ACTION' value='UPDATESETTINGS'>
<input type='hidden' class="form-control" id="id" name="id" placeholder="" value="<?php echo $settingInfo['id'] ?>" >
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="validationTooltip01">Application Name</label>
                        <input type="text" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" class="form-control" id="application_name" name="application_name" placeholder=""   value="<?php echo $settingInfo['application_name'] ?>" maxlength="10" >
                    </div>
                    <!-- <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Gender</label>
                        <select id="gender" name="gender" class="form-control">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        </select>
                    </div> -->
                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="validationTooltip01">Phone Number</label>
                        <input onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" id="phoneno" name="phoneno" placeholder=""    value="<?php echo $settingInfo['phoneno'] ?>" maxlength="15" >
                    </div>
                      
                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="">Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder=""    value="<?php echo $settingInfo['address'] ?>" maxlength="40">
                    </div>

                    
        </div>
        <div class="form-row">

                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="">Easypaisa Account Name</label>
                        <input type="text" class="form-control" id="account_name" name="account_name" placeholder=""    value="<?php echo $settingInfo['account_name'] ?>" maxlength="20" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" >
                    </div>
                     <div class="col-md-4 mb-3">
                        <label class="form-label" for="">Easypaisa Account Number</label>
                        <input type="text" class="form-control" id="account_details" name="account_details" placeholder=""    value="<?php echo $settingInfo['account_details'] ?>"onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="20">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="">Twitter URL</label>
                        <input type="text" class="form-control" id="twitter_url" name="twitter_url" placeholder=""   value="<?php echo $settingInfo['twitter_url'] ?>" maxlength="24" >
                    </div>
                    
                   

                    
        </div>
       
        <div class="form-row">
                   
                  
                    <div class="col-md-4 mb-3">
                        <label class="form-label" for="">Facebook URL</label>
                        <input type="text" class="form-control" id="facebook_url" name="facebook_url" placeholder=""   value="<?php echo $settingInfo['facebook_url'] ?>"maxlength="23" >
                    </div>
                    
        </div>
        
      

<div style="margin-top: 20px;"></div>
<div class="form-row">
<div class="col-md-8  mb-3">
</div>
<div class="col-md-2  mb-3" style="margin-left: 880px;" >

    <button type="button" onclick='UpdateUser()' class="btn btn-med btn-warning"style="background: #ff8000">
        <span class="fal fa fa-arrow-right mr-1" ></span>
        Update
    </button>
   
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>
<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
function viewsubcategory(id)
{
//alert(id);

if (id==1 || id==2 || id==9) {
    document.getElementById("accountoffon").disabled = true;
    document.getElementById("group").disabled = true;
    document.getElementById("zone").disabled = true;
    document.getElementById("region").disabled = true;
    document.getElementById("city").disabled = true;
    document.getElementById("area").disabled = true;
    document.getElementById("town").disabled = true;
    document.getElementById("market").disabled = true;
}
else{
    document.getElementById("accountoffon").disabled = false;
    document.getElementById("group").disabled = false;
    document.getElementById("zone").disabled = false;
    document.getElementById("region").disabled = false;
    document.getElementById("city").disabled = false;
    document.getElementById("area").disabled = false;
    document.getElementById("town").disabled = false;
    document.getElementById("market").disabled = false;
}


if(id == '')
{
alert("No ID found!!");


}
else
{
o =new Array(id);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetSubCategory&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetSubCategoryCallBack});

}
}
function GetSubCategoryCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('subdealer').options.length = 0;
document.getElementById('subdealer').innerHTML += "<option value=''>Select Sub Dealer Catergory</option>";
document.getElementById('subdealer').innerHTML += obj;
}
///
function zonetoregion(zone)
{
// alert(zone);
if(zone == '')
{
alert("No ID found!!");
}
else
{
o =new Array(zone);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetRegion&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetRegionCallBack});

}
}
function GetRegionCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('region').options.length = 0;
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('region').innerHTML += obj;
}
///
function regiontocity(region)
{
// alert(region);
if(region == '')
{
alert("No ID found!!");
}
else
{
o =new Array(region);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetCity&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetCityCallBack});

}
}
function GetCityCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('city').options.length = 0;
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('city').innerHTML += obj;
}
///
function citytoarea(city)
{
//  alert(city);
if(city == '')
{
alert("No ID found!!");
}
else
{
o =new Array(city);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetArea&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAreaCallBack});

}
}
function GetAreaCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('area').options.length = 0;
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('area').innerHTML += obj;
}
///
function  CheckError()
{
console.log('error');
}

///

function UpdateUser() {
if(confirm("Are you sure you want to update this settings?"))
{
var callfunc={onSuccess:NextPage};
form= document.getElementById('adduserform');   
PostAjaxScreen("adduserform",form,callfunc);
}
}

function NextPage(){
alert('Update Successfully');    
LoadAjaxScreen("settings");
}
</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>