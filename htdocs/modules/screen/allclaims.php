<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    
   // $userId = $mysession->getValue("userid");
    

    $claimNoSearch=$_GET['claimno'];
    $searchFrom=$_GET['from'];
    $searchTo=$_GET['to'];
    //blViewClaims
    if ($claimNoSearch!="") {
        $blViewClaims=new BL_ViewClaims();
        $fetchDataClaim =$blViewClaims->ViewClaimsNo($claimNoSearch);
        $count =  $fetchDataClaim->count;
    }
    else if($searchFrom!="" && $searchTo!="")
    {
        // $viewClaims=new BL_ViewClaims();
        // $fetchData=$viewClaims->ViewCompletedClaimsDate($searchFrom,$searchTo);
        // $count =  $fetchData->count;
        $searchFrom = date('Y-m-d',strtotime($searchFrom));
        $searchTo = date('Y-m-d',strtotime($searchTo));
        $searchTo = str_replace('-', '/', $searchTo);
        $searchTo = date('Y-m-d',strtotime($searchTo . "+1 days"));
        $blViewClaims=new BL_ViewClaims();
        $fetchDataClaim=$blViewClaims->ViewClaimsDates($searchFrom,$searchTo);
        $count =  $fetchDataClaim->count;
    }
    else
    {
        $blViewClaims=new BL_ViewClaims();
        $fetchDataClaim=$blViewClaims->ViewClaims();
        $count =  $fetchDataClaim->count;
    }
?>



<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
All Claims
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>
<form class="form-horizontal" name='updatefinancee' id='updatefinancee' method='post'>
<input type='hidden' name='ACTION' value='UPDATEFINANCE'>
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">

<div class="panel-container show">
<div class="panel-content" style="margin-top: 10px;">

<div class="form-row">   
        <div class="col-md-4 mb-3" >
            <label class="form-label">Select Dates</label>
            <div class="input-daterange input-group" id="datepicker-5">
                <input type="text" class="form-control" readonly="" name="fromdate" placeholder="From Date" id="fromdate">
                <div class="input-group-append input-group-prepend">
                    <span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
                </div>
                <input type="text" class="form-control" readonly="" name="todate"  placeholder="To Date" id="todate">
            </div>
        </div>
        <!-- <div class="col-md-2 mb-3" >
            <label class="form-label"> From Date</label>
            <div class="input-group" style=" ">
                
                <input type="date" class="form-control" onchange="selectdate()" placeholder="" id="fromdate" >
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-exclamation"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2 mb-3" >
            <label class="form-label"> To Date</label>
            <div class="input-group" style=" ">
                
                <input type="date" class="form-control " placeholder="From Date" id="todate"  >
                <div class="input-group-append">
                    <span class="input-group-text fs-xl">
                        <i class="fal fa-calendar-exclamation"></i>
                    </span>
                </div>
            </div>
        </div> -->
        <div class="col-md-2 mb-3" >
            <label class="form-label"> Claim Number</label>
            <div class="input-group" style=" ">            
                <input type="text" class="form-control " placeholder="" id="claimno">
            </div>
        </div>
        <div class="col-md-3  mb-3" style="margin-top: 23px;">
            <button type="button" onclick="DateOrClaimNoSearch()" class="btn btn-med btn-warning">
                <span class="fal fa fa-search mr-1"></span>
                Search
            </button>
        </div>
    </div>
    <hr>  
 <!-- datatable start -->
                <table id="dt-basic-example" style="text-align: center;" class="table table-bordered table-hover table-striped w-100" >
                    <thead style="background-color: lightgrey;" class="bg-highlight">
                        <tr style="text-align: center;">
                            <th>Serial No</th>
                            <th>Claim Number</th>
                            <th>Raised By</th>
                            <th>Raised By Mobile</th>
                            <th>Raised By Address</th>
                            <th>Raised By Market</th>
                            <th>Purchased From</th>
                            <th>Account Of</th>
                            <th>Claim Type</th>
                            <th>Claim Status</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <?

                        for ($i=0; $i <$count; $i++) { 
                              //for
                                $claimID = $fetchDataClaim->rows[$i]['claim_id'];
                                $claimNo = $fetchDataClaim->rows[$i]['claim_no'];
                                $dealerID = $fetchDataClaim->rows[$i]['user_id'];
                                
                                $fetchData1=$blViewClaims->FetchDealerName($dealerID);
                                $dealerCategoryName = $fetchData1->rows[0]['dealer_category_name'];
                                $claimName = $fetchData1->rows[0]['full_name'];
                                $mobileNo = $fetchData1->rows[0]['mobile_no'];
                                $address = $fetchData1->rows[0]['address'];
                                $market = $fetchData1->rows[0]['market'];

                                $purchasedFrom = $fetchDataClaim->rows[$i]['purchased_from'];
                                $fetchData1=$blViewClaims->FetchDealerName($purchasedFrom);
                                $purchasedFrom = $fetchData1->rows[0]['full_name'];
                                $purchasedFromDesig = $fetchData1->rows[0]['dealer_category_name'];

                                $accountOf = $fetchDataClaim->rows[$i]['account_of'];
                                $fetchData1=$blViewClaims->FetchDealerName($accountOf);
                                $accountOf = $fetchData1->rows[0]['full_name'];
                                $accountOfDesig = $fetchData1->rows[0]['dealer_category_name'];

                                $remarks = $fetchDataClaim->rows[$i]['claim_type'];
                                $claimStatus = $fetchDataClaim->rows[$i]['claim_status'];
                                $Date = $fetchDataClaim->rows[$i]['created_at'];    
                                // $Date=substr($Date,0,11);
                                // $Date=date_create($Date);                     
                                // $Date = date_format($Date,"d-M-Y");
                                $Date = date('d-M-Y',strtotime($Date));
                                $dealerCategoryName=strtolower($dealerCategoryName);
                                $dealerCategoryName=ucfirst($dealerCategoryName);
                                $purchasedFromDesig=strtolower($purchasedFromDesig);
                                $purchasedFromDesig=ucfirst($purchasedFromDesig);
                                $accountOfDesig=strtolower($accountOfDesig);
                                $accountOfDesig=ucfirst($accountOfDesig);

                                if ($claimStatus=="open") {
                                $fetchData1= $blViewClaims->FetchClaimTransactions($claimID);
                                $countTransaction =  $fetchData1->count;
                                $fetchDataPurch = $blViewClaims->FetchClaimTransactionsPurchase($claimID);
                                $countTransactionPurchase =  $fetchDataPurch->count;
                                
                                for ($j=0; $j <$countTransactionPurchase; $j++) { 
                                    $purchasedFrom = $fetchDataPurch->rows[$j]['user_id'];
                                    $fetchDataPurchasedFrom=$blViewClaims->FetchDealerName($purchasedFrom);
                                    $purchasedFrom = $fetchDataPurchasedFrom->rows[0]['full_name'];
                                    $purchasedFromDesig = $fetchDataPurchasedFrom->rows[0]['dealer_category_name'];
                                    $purchasedFromDesig=strtolower($purchasedFromDesig);
                                    $purchasedFromDesig=ucfirst($purchasedFromDesig);
                                }

                                for ($j=0; $j <$countTransaction; $j++) { 
                                    $accountOfId = $fetchData1->rows[$j]['user_id'];
                                    $fetchDataAccountOfId=$blViewClaims->FetchDealerName($accountOfId);
                                    $accountOf = $fetchDataAccountOfId->rows[0]['full_name'];
                                    $accountOfDesig = $fetchDataAccountOfId->rows[0]['dealer_category_name'];
                                    $accountOfDesig=strtolower($accountOfDesig);
                                    $accountOfDesig=ucfirst($accountOfDesig);
                                }
                                }
                        ?>

                        <tr>
                            <td style="text-align: center;"><?=$i+1?></td>
                            <td style="text-align: center;"><?=$claimNo?></td>
                            <td style="text-align: left;"><?=ucfirst($claimName)." - ".$dealerCategoryName.""?></td>
                            <td style="text-align: center;"><?=$mobileNo?></td>
                            <td style="text-align: center;"><?=$address?></td>
                            <td style="text-align: center;"><?=$market?></td>
                            <td style="text-align: left;"><?=ucfirst($purchasedFrom)." - ".$purchasedFromDesig.""?></td>
                            <td style="text-align: left;"><?=ucfirst($accountOf)." - ".$accountOfDesig.""?></td>
                            <td style="text-align: center;"><?=ucfirst($remarks)?></td>
                            <td style="text-align: center;"><?php
                            if ($claimStatus=="approved") {
                              ?>
                              <div style="background-color:lightgrey; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Approved
                              </div>
                              <?php
                            }
                            else if ($claimStatus=="closed") {
                              ?>
                              <div style="background-color:#3cabf0; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Closed
                              </div>
                              <?php
                            }
                            else if ($claimStatus=="inprocess") {
                              ?>
                              <div style="background-color:yellow; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Inprocess
                              </div>
                              <?php
                            }
                            else if ($claimStatus=="completed") {
                              ?>
                              <div style="background-color:orange; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Complete
                              </div>
                              <?php
                            }
                            else if ($claimStatus=="open") {
                              if ($countTransaction<=0 || $countTransactionPurchase<=0) 
                              {
                              ?>
                              <div style="background-color:#f54242; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Incomplete
                              </div>
                              <?php
                               }
                            else
                            {
                                if ($countTransaction>0 && $countTransactionPurchase>0) 
                                {
                                   ?>
                                  <div style="background-color:#f54242; text-align: center;
                                     padding-top: 4px; padding-bottom: 4px;
                                    ">
                                        Unapproved
                                  </div>
                                  <?php
                                }
                            }
                            }
                            ?>
                            </td>
                            <td style="text-align: center;"><?=$Date?></td>
                            <td align="center">
                            <?php 
                            if ($claimStatus=="open") {
                                if ($countTransaction<=0 || $countTransactionPurchase<=0) 
                                {
                            ?>
                            <button type="button" onClick="LoadAjaxScreen('claimdetails&id=<?php echo $claimID ?>&page=1&status=1')" class="btn btn-sm btn-dark">
                            <span class="fal  fa-eye mr-1"></span>
                            </button>
                              <?php
                                }
                                else{
                                    if ($countTransaction>0 && $countTransactionPurchase>0) 
                                    {
                                      ?>
                                <button type="button" onClick="LoadAjaxScreen('claimdetails&id=<?php echo $claimID ?>&page=1&status=2')" class="btn btn-sm btn-dark">
                                <span class="fal  fa-eye mr-1"></span>
                                </button>
                                  <?php  
                                    }
                                }
                            }
                            else{
                                ?>
                                    <button type="button" onClick="LoadAjaxScreen('claimdetails&id=<?php echo $claimID ?>&page=1')" class="btn btn-sm btn-dark">
                                    <span class="fal  fa-eye mr-1"></span>
                                    </button>
                                <?php 
                            }

                            if ($claimStatus=="approved") {
                              ?>

                            <button type="button" onclick="OpenModalBox('consignmentweight&id=<?php echo $claimID ?>','screen','');"  class="btn btn-sm btn-warning" >
                            <span class="fal  fa-edit mr-1"></span>
                            </button>
                              <?php
                            }
                            else if ($claimStatus=="closed"  || $claimStatus=="completed") {
                              ?>
                            <button type="button" disabled="true" class="btn btn-sm btn-warning" >
                            <span class="fal  fa-edit mr-1"></span>
                            </button>
                              <?php
                            }
                            else if ($claimStatus=="inprocess") {
                              ?>
                            <button type="button" onClick="LoadAjaxScreen('acceptrejectparts&id=<?php echo $claimID ?>')" class="btn btn-sm btn-warning" >
                            <span class="fal  fa-edit mr-1"></span>
                            </button>
                              <?php
                            }
                            else if ($claimStatus=="open") {


                            if ($countTransaction<=0 || $countTransactionPurchase<=0) 
                            {
                              ?>
                            <button type="button" onclick="OpenModalBox('purchasefromaccountoff&id=<?php echo $claimID ?>','screen','');"  class="btn btn-sm btn-warning">
                            <span class="fal  fa-edit mr-1"></span>
                            </button>
                              <?php
                            }
                            else{
                                if ($countTransaction>0 && $countTransactionPurchase>0) 
                                {
                                  ?>
                                <button type="button" onclick="OpenModalBox('purchaseaccountacceptreject&id=<?php echo $claimID ?>','screen','');"  class="btn btn-sm btn-warning" >
                                <span class="fal  fa-edit mr-1"></span>
                                </button>
                                  <?php
                                }
                            }
                            }
                            ?>

                          </td>
                        </tr>

                        <?php 
                        }
                        ?>
                       <!--  <tr>
                            <td>voc-13-ad</td>
                            <td>15-Mar-2019</td>
                            <td>Garrett Winters</td>
                            <td></td>
                            <td></td>
                            <td><div style="background-color:red; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    None
                                </div></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-dark">
                            <span class="fal  fa-eye mr-1"></span>
                            </button></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-warning" disabled="true" >
                            <span class="fal  fa-edit mr-1"></span>
                            </button></td>
                        </tr>
                        <tr>
                            <td>voc-05-az</td>
                            <td>10-Jul-2019</td>
                            <td>Brielle Williamson</td>
                            <td>Humza</td>
                            <td>3153463545434</td>
                            
                            <td><div style="background-color:lightgrey; text-align: center;
                                 padding-top: 4px; padding-bottom: 4px;
                                ">
                                    Pending
                                </div></td>
                            <td align="center"><button type="button" class="btn btn-sm btn-dark" onClick="LoadAjaxScreen('claimdetails')">
                            <span class="fal fa-eye mr-1"></span>
                            </button></td>
                            <td align="center"><button type="button" id="modal-btn" class="btn btn-sm btn-warning">
                            <span class="fal  fa-edit mr-1"></span>
                            </button></td>
                        </tr> -->
                       
                    </tbody>
                </table>

</div>
</div>


</div>
</div>
</div>
</form>

<div class="modal fade" id="default-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fal fa-times"></i></span>
            </button>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</div>
</div> 
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">

// $("#fromdate").keypress(function(e){
//    var keyCode = e.keyCode || e.which;
//     var regex = /^[ ]+$/;
//     var isValid = regex.test(String.fromCharCode(keyCode));
//     if (!isValid) {
//        return false
//        }
//     if( $("#fromdate").val().length >35 ) {
//             return false;
//        }
// });

// $("#todate").keypress(function(e){
//    var keyCode = e.keyCode || e.which;
//     var regex = /^[ ]+$/;
//     var isValid = regex.test(String.fromCharCode(keyCode));
//     if (!isValid) {
//        return false
//        }
//     if( $("#todate").val().length >35 ) {
//             return false;
//        }
// });

// function selectdate()
// {
//     var fromdate = document.getElementById("fromdate").value;
//     document.getElementById("todate").min=fromdate;

// }
    
function DateOrClaimNoSearch() {
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    var claimNo = document.getElementById("claimno").value;

    if (claimNo!="") {
        var callfunc={onSuccess:NextPageClaim};
        form= document.getElementById('updatefinancee');   
        PostAjaxScreen("updatefinancee",form,callfunc);
    }
    else if(fromdate!="" && todate!=""){
        if (fromdate!="" && todate!="") {
                var callfunc={onSuccess:NextPageDate};
                form= document.getElementById('updatefinancee');   
                PostAjaxScreen("updatefinancee",form,callfunc);
        }
        else
        {
            alert('Please Select The Dates!');
        }
    }
    else
    {
        alert('Please Select Date or Claim Number!');
    }
}

function NextPageDate(){
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    LoadAjaxScreen("allclaims&from="+fromdate+"&to="+todate);
}

function NextPageClaim(){
    var claimNo = document.getElementById("claimno").value;
    LoadAjaxScreen("allclaims&claimno="+claimNo);
}

</script>


<script src="js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script type="text/javascript">
var controls = {
    leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
    rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
}

$('#datepicker-5').datepicker(
{
    todayHighlight: true,
    format: 'dd-M-yyyy',
    endDate: new Date(),
    startDate: '01-jan-2000',
    templates: controls
});

$("#claimno").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#claimno").val().length >25 ) {
            return false;
       }
});

// var today1 = new Date();
// var mm = String(today1.getMonth() + 1).padStart(2, '0'); //January is 0!
// var yyyy = today1.getFullYear();
// dd = new Date(yyyy, mm , 0).getDate();

// today2 = yyyy + '-' + mm + '-' + dd;
// var fromdate = today2;
// var a = document.getElementById("todate").max = fromdate;
// var a = document.getElementById("fromdate").max = fromdate;

</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script>


     $(document).ready(function()
            {
                // Setup - add a text input to each footer cell
                $('#dt-basic-example thead tr').clone(true).appendTo('#dt-basic-example thead');
                $('#dt-basic-example thead tr:eq(1) th').each(function(i)
                {
                    var title = $(this).text();
                    $(this).html('<input type="text" class="form-control form-control-sm" placeholder="Search ' + title + '" />');

                    $('input', this).on('keyup change', function()
                    {
                        if (table.column(i).search() !== this.value)
                        {
                            table
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                });

                var table = $('#dt-basic-example').DataTable(
                {
                    responsive: true,
                    orderCellsTop: true,
                    lengthChange: false,
                    //fixedHeader: true,

                    dom:
                        /*  --- Layout Structure 
                            --- Options
                            l   -   length changing input control
                            f   -   filtering input
                            t   -   The table!
                            i   -   Table information summary
                            p   -   pagination control
                            r   -   processing display element
                            B   -   buttons
                            R   -   ColReorder
                            S   -   Select

                            --- Markup
                            < and >             - div element
                            <"class" and >      - div with a class
                            <"#id" and >        - div with an ID
                            <"#id.class" and >  - div with an ID and a class

                            --- Further reading
                            https://datatables.net/reference/option/dom
                            --------------------------------------
                         */
                        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-12 d-flex align-items-center justify-content-end'lB>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    buttons: [
                        /*{
                            extend:    'colvis',
                            text:      'Column Visibility',
                            titleAttr: 'Col visibility',
                            className: 'mr-sm-3'
                        },*/
                        {
                            extend: 'pdfHtml5',
                            text: 'PDF',
                            titleAttr: 'Generate PDF',
                            className: 'btn-outline-danger btn-sm mr-1'
                        },
                        {
                            extend: 'excelHtml5',
                            text: 'Excel',
                            titleAttr: 'Generate Excel',
                            className: 'btn-outline-success btn-sm mr-1'
                        },
                        {
                            extend: 'csvHtml5',
                            text: 'CSV',
                            titleAttr: 'Generate CSV',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'copyHtml5',
                            text: 'Copy',
                            titleAttr: 'Copy to clipboard',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            titleAttr: 'Print Table',
                            className: 'btn-outline-primary btn-sm'
                        }
                    ]
                });

            });
        </script>


<!-- <script type="text/javascript">
    
// Get DOM Elements
const modal = document.querySelector('#my-modal');
const modalBtn = document.querySelector('#modal-btn');
const closeBtn = document.querySelector('.close');

// Events
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', outsideClick);

// Open
function openModal() {
  modal.style.display = 'block';
}

// Close
function closeModal() {
  modal.style.display = 'none';
}

// Close If Outside Click
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = 'none';
  }
}

</script> -->