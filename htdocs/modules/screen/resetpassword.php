<?php
    require_once(MODULE."/class/bllayer/user.php");
    $userId= $mysession->getvalue("userid");
?>
<!-- MAIN CONTENT -->
<main id="js-page-content"role="main"class="page-content">
 	<div class="row">
	    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-5" style="margin-top: 30px;">
	      <h1> <span class="page-title txt-color-blueDark" style="margin-left: 10px; margin-top: 20px;">Change Password</span></h1>
	    </div>
  	</div>
	<div class="row">
		<div class="col-xl-12">
		    <div id="panel-1" class="panel">
		        <div class="panel-container show">
		            <div class="panel-content">
						<form class="needs-validation" id='resetpassword' name='resetpassword' novalidate>
							<input type='hidden' name='ACTION' value='RESETPASSWORD1'>
							<input type='hidden' name='userId' id='userId' value='<?=$userId?>'>
							<input type='hidden' name='validated' id='validated' value='0'>
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Old password</label>
									<input onblur="CheckUserName()" class="form-control" value="" placeholder="Old Password" type="password" id='old_password' name='old_password' required="">
									<p id="msgbox"></p>
									<div class="invalid-feedback">
                  					Please Enter Correct Password.
              						</div>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>New Password</label>
									<input onblur="GetLastThreePassword(this.value)" class="form-control" value="" placeholder="New password" type="password" id='new_password' name='new_password' required="">
									<div class="note">
										Your password must contain at least 1 lowercase alphabetical character, uppercase alphabetical character, numeric character, special character and min length of 8
									</div>
									<div class="invalid-feedback">
										Enter the password in the corrcet way as written above.
									</div>
								</div>
              					<div class="col-md-6 mb-3">
									<label>Confirm New Password</label>
									<input class="form-control" value="" placeholder="Confirm New password" type="password" id='confirm_new_password' name='confirm_new_password' required="">
								</div>
								<div class="invalid-feedback">
                  				Please Enter Correct Password.
              					</div>
							</div>

							<div class="form-row">
								<div class="col-md-12 mb-12">
									<button class="btn btn-primary btn-md float-right" type="button" onclick="LoadAjaxScreen('resetpassword')" style= "background: #ff8000;margin-left:13px;color: black"><i class="fal fa-times" ></i> 
											Cancel
										</button>

										<button id="js-save-btn" class="btn btn-primary btn-md float-right" type="button" onclick="Update()"style= "background: #ff8000; color: black; "><i class="fal  fa-edit" ></i>
											Update
										</button>
									
								</div>
              					
								
							</div>
							
								
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
<!-- END MAIN CONTENT -->
<script type="text/javascript">
                $( document ).ready(function() {
                    console.log( "ready!" );
                   // pageSetUp();
                    var divmainheight = $('#main').height();
                    console.log(divmainheight);
                    if (divmainheight < 1200) {
                        divmainheight = divmainheight + 500;
                    }
                    $('#left-panel').css('min-height', divmainheight);
                });
				function GetLastThreePassword(newPassword)
				{
					if (newPassword == "") {
						return true;
					}
					// console.log(newPassword);
					var encodedData = window.btoa(newPassword); // encode a string
					var userId = document.getElementById("userId").value;
					o =new Array(userId,encodedData,'<?=$sid?>')
					o = JSON.encode(o);
					var pars = 'param='+o;
					var url = "index.php?object=user&function=GetLastThreePassword&isajaxcall=1&returnType=string";
					var myAjax = new Ajax.Request( url,
					{ method: 'post', parameters: pars, onFailure: ReportError , onSuccess: GetLastThreePasswordCallBack});
				}
				function GetLastThreePasswordCallBack(res)
				{
					var data = JSON.decode(res.responseText);
					console.log(data);
					if(!data.status)
					{
						alert("You can\'t use last three password");
						document.getElementById("new_password").value='';
						document.getElementById("validated").value='0';
					}else {
						document.getElementById("validated").value='1';
					}
				}
				function CheckUserName()
				{
					var userId = document.getElementById("userId").value;
					let oldPassword = document.getElementById("old_password").value;
                    var encodedData = window.btoa(oldPassword); // encode a string
					o =new Array(userId,encodedData)
					o = JSON.encode(o);
					var pars = 'param='+o;
					var url = "index.php?object=user&function=checkuserName&isajaxcall=1&returnType=string";
					var myAjax = new Ajax.Request( url,
					{ method: 'post', parameters: pars, onFailure: ReportError , onSuccess: ChecksCallBack});
				}
				function ChecksCallBack(res)
				{
					var status = JSON.decode(res.responseText);
					if(status=='0')
					{
						alert("Password not match");
						document.getElementById("old_password").value='';
						LoadAjaxScreen('resetpassword');
					}
					else {
						$('#msgbox').html('Password match');
					}
				}
				function ReportError(res)
				{
					alert("err");
				}

				function Update()
				{
					var validateUser = ValidateUserData();
					if(validateUser==true)
					{
                        let oldPassword=window.btoa(document.getElementById('old_password').value);
                        let newPassword=window.btoa(document.getElementById('new_password').value);
    					let confirmNewPassword=window.btoa(document.getElementById('confirm_new_password').value);
                        $('#old_password').val(oldPassword);
                        $('#new_password').val(newPassword);
                        $('#confirm_new_password').val(confirmNewPassword);
						var obj = {onSuccess:PostAjaxScreenMessage};
						form = document.getElementById('resetpassword');
						PostAjaxScreen("resetpassword",form,obj);
					}
					return false;
				}
				function UpadteCheckinInfoCallBack(res)
				{
					
                    if(res.responseText == 1){
                    	
                        alert('Password reset Successfully');
                        location.href = 'logout.php';
            		}else{
            			
                        alert("Password Not reset");
                        LoadAjaxScreen('resetpassword');
                    }
            		// HideBlockDiv();
				}
                function PostAjaxScreenMessage()
            	{
            		o =new Array('<?=$sid?>')
            		o = JSON.encode(o);
            		var pars = 'param='+o;
            		var url = "index.php?object=user&function=PostAjaxScreenMessage&isajaxcall=1&returnType=string";
            		var myAjax = new Ajax.Request( url,
            		{ method: 'post', parameters: pars, onFailure: ReportError , onSuccess: UpadteCheckinInfoCallBack});
            	}
				function ValidateUserData()
				{
					oldPassword=document.getElementById('old_password').value;
                    newPassword=document.getElementById('new_password').value;
					confirmNewPassword=document.getElementById('confirm_new_password').value;
					validated=document.getElementById('validated').value;

					if(oldPassword=="")
					{
						//document.getElementById('old_password').style.borderColor = "red";
						$("#old_password").focus();
						return false;
					}
					if(newPassword=="")
					{
						//document.getElementById('new_password').style.borderColor = "red";
						$("#new_password").focus();
						return false;
					}
					var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
					if (!strongRegex.test(newPassword)) {
                        //getElementById('new_password').style.borderColor = "red";
						$("#new_password").focus();
                        alert('Password policy not matched');
                        alert('Your password must contain at least 1 lowercase alphabetical character, uppercase alphabetical character, numeric character, special character and min length of 8');
						return false;
			        }
                    if(confirmNewPassword=="" || !strongRegex.test(confirmNewPassword))
					{
						//document.getElementById('confirm_new_password').style.borderColor = "red";
						$("#confirm_new_password").focus();
                        alert('Password policy not matched');
						return false;
					}
                    if (newPassword == confirmNewPassword) {
                        return true;
                    }else {
                        alert('Password not match with new password');
                        return false;
                    }
					if (validated == 0) {
						return false;
					}
					return true;
				}
				$("#js-save-btn").click(function(event)
			    {

			        // Fetch form to apply custom Bootstrap validation
			        var form = $("#resetpassword")

			        if (form[0].checkValidity() === false)
			        {
			            event.preventDefault()
			            event.stopPropagation()
			        }

			        form.addClass('was-validated');
			        // Perform ajax submit here...
			    });
</script>
