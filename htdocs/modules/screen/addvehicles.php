<?php
require_once(MODULE."/class/bllayer/user.php");
$user = new BL_User();


    $heading="Add Vahicle";
    $vehicle = $user->GetVehicle();
    $count3 =  $vehicle->count;
// echo $count3."srsysry";


?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Add Vehicle
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addcityform' id='addcityform' method='post'>
<input type='hidden' name='ACTION' value='ADDVEHICLE'>
<div id="panel-10" class="panel" style="margin-top: -30px;">
        

        <div>

             <div class="form-row " style="margin-top: 30px;">
                    
                          <div class="col-md-1 mb-3">
                        
                    </div>
                                <div class="col-md-5 mb-3">
                                    <label>Add Vehicle</label>
                                    <input type="text" class="form-control" id="vahicle" name="vahicle" placeholder="Vehicle" d   value=""onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" maxlength="12" >
                                </div>
                                <div class="col-md-5 mb-3">
                                    <label>Description</label>
                                    <input type="text" class="form-control" id="description" name="description" placeholder="Description" >
                                </div>
                                <div class="col-md-2 mb-3" style="margin-top: 23px; margin-left: 830px" >
                                        <button type="button" style="width: 100px; background: #ff8000" onclick="AddCity()" class="btn btn-med btn-warning" >
                                            <span class="fal fa fa-plus mr-1"></span>
                                            Add
                                        </button>
                                </div>
                         
                           
                           
                    </div>

            
            <div class="form-row" style="margin-top: 30px;">
                    <div class="col-md-1 mb-3">
                        
                    </div>
                    <div class="col-md-10 mb-3">
                        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr align="center">
                                    <th>S.No</th>
                                    <th>Vehicle Name</th>
                                    <th>Discription</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for ($i=0; $i <$count3; $i++) { 
                                    $vehicleName = $vehicle->rows[$i]['name'];
                                    $vehicleId = $vehicle->rows[$i]['id'];
                                     $description = $vehicle->rows[$i]['description'];
                                ?>
                                <tr>
                                    <td align="center"><?php echo $i+1 ?></td>
                                    <td><?php echo $vehicleName?></td>
                                    <td><?php echo $description?></td>
                                    <td align="center" nowrap>
                                <button type="button" onclick="OpenModalBox('editvehicle&id=<?=$vehicleId?>','screen')" class="btn btn-sm btn-warning" style="background: #ff8000" title="Edit">
                                    <span class="fal  fa-pen mr-1"></span>
                                 </button>
                                <button type="button" onclick="DeleteVahicle('<?php echo $vehicleId ?>')" class="btn btn-sm btn-warning" style="background: #ff8000" title="Delete">
                                <span class="fa fa-trash-o mr-1" style="color: black"></span>
                                </button></td>
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-1 mb-3">
                        
                    </div> -->
                   
            </div>
            

          
        </div>

</div>
<div style="height: 150px;"></div>
  </form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open"/>
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteVahicle(vehicleID)
{
  var r = confirm("Are you sure you want to delete this city?");
  if(r==true)
  {
  o =new Array(vehicleID);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=settings&function=deleteVehicle&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
  }
  else
  {
    return;
  }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("addvehicle");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  // LoadAjaxScreen("addcity");
}
</script>


<script type="text/javascript">
    

function UpdateCity() {
    if(confirm("Are you sure you want to update this City?"))
    {
        var callfunc={onSuccess:NextPage1};
        form= document.getElementById('addcityform');   
        PostAjaxScreen("addcityform",form,callfunc);
    }
}

function NextPage1(){
      LoadAjaxScreen("addvahicle");
}

</script>


<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    var table = $('#dt-basic-example').DataTable();
     
    $('#dt-basic-example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        document.getElementById("CityUpdate").value = data[1];
        document.getElementById("CityID").value = data[0];
        document.getElementById("CityUpdate").disabled = false;
    } );
} );
    
</script>
<script type="text/javascript">
    
  
  function Check(){
   if(document.getElementById('vahicle').value=='')
  {
    alert("Please Add Vehicle");
    return false;
  }
  return true;
}
function AddCity(){
  var x=Check();
  if(x==true){
    if(confirm("Are you sure you want to Add this Vehicle?")){
    var obj = {onSuccess:AddusersInfoCallBack};
          form = document.getElementById('addcityform');
          PostAjaxScreen("addcityform",form,obj)
        }
  }
    function AddusersInfoCallBack(){
     alert('Successfully added');
     LoadAjaxScreen("addvehicle");
    }
  }
</script>
