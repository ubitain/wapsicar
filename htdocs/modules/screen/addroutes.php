<?php
require_once(MODULE."/class/bllayer/user.php");
// require_once(MODULE."/class/bllayer/addstuff.php");
$user = new BL_User();
// $routes = new BL_AddStuff();

    $heading="Add City";
    $city = $user->GetCity();
    $count3 =  $city->count;

    $routesInfo = $user->GetRoute();
?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Add Routes
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='addcityform' id='addcityform' method='post'>
<input type='hidden' name='ACTION' value='ADDROUTE'>
<div id="panel-10" class="panel" style="margin-top: -30px;">
        

        <div>

             <div class="form-row " style="margin-top: 30px;">
                    
                          <div class="col-md-1 mb-3">
                        
                    </div>
                                <div class="col-md-3 mb-3">
                                    <label> From City <span style="color: red"> * </span></label>
                                    <select id="from_city" name="from_city" class="form-control"  >
                                      <option value="0" >Select City</option>
                                    <?php 
                                         for ($i=0; $i <$count3; $i++) { 
                                            $cityName = $city->rows[$i]['city_name'];
                                            $cityID = $city->rows[$i]['city_id'];

                                            echo "<option value='$cityID' >$cityName</option>";
                                                
                                         
                                        }
                                     ?>
                                    </select>
                                  
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label>To City<span style="color: red"> * </span></label>
                                   <select id="to_city" name="to_city" class="form-control"  >
                                    <option value="0">Select City</option>
                                    <?php 
                                         for ($i=0; $i <$count3; $i++) { 
                                            $cityName = $city->rows[$i]['city_name'];
                                            $cityID = $city->rows[$i]['city_id'];

                                            echo "<option value='$cityID' >$cityName</option>";
                                                
                                         
                                        }
                                     ?>
                                    </select>
                                </div> 
                                <div class="input col-md-3 mb-3">
                                    <label>Rate<span style="color: red"> * </span></label>
                                    <input type="text" class="form-control" id="rates" name="rates" placeholder="4,000 " d   value="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'maxlength="15"pattern="^[\d,]+$" >
                                </div>
                                <div class="col-md-2 mb-3" style="margin-top: 23px;">
                                        <button type="button" style="width: 100px; background: #ff8000" onclick="AddRoutes()" class="btn btn-med btn-warning">
                                            <span class="fal fa fa-plus mr-1"></span>
                                            Add
                                        </button>

                                </div>
                         
                           
                           
                    </div>
            
            <div class="form-row" style="margin-top: 30px;">
                    <div class="col-md-1 mb-3">
                        
                    </div>
                    <div class="col-md-10 mb-3">
                        <table id="dt-basic-example"  class="table table-bordered table-hover table-striped w-100">
                            <thead>
                                <tr align="center">
                                    <th>S.No</th>
                                    <th>From City</th>
                                    <th>To City</th>
                                    <th>Rates</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for ($i=0; $i <$routesInfo->count; $i++) { 
                                    $routeId = $routesInfo->rows[$i]['route_id'];
                                    $fromCityName = $routesInfo->rows[$i]['from_city_name'];
                                    $toCityName = $routesInfo->rows[$i]['to_city_name'];
                                    $rates = $routesInfo->rows[$i]['rates'];
                                ?>
                                <tr>
                                    <td align="center"><?php echo $i+1 ?></td>
                                    <td align="center"><?php echo $fromCityName ?></td>
                                    <td align="center"><?php echo $toCityName ?></td>
                                    <td align="center"><?php echo $rates ?></td>
                                    <td align="center" nowrap>
                                <button type="button" onclick="OpenModalBox('editroute&route_id=<?=$routeId?>','screen')" class="btn btn-sm btn-warning" style="background: #ff8000">
                                    <span class="fal  fa-pen mr-1"></span>
                                 </button>
                                <button type="button" onclick="DeleteRoutes('<?php echo $routeId ?>')" class="btn btn-sm btn-warning" style="background: #ff8000">
                                <span  class="fa fa-trash-o mr-1" style="color: black"></span>
                                </button></td>
                                </tr>
                                <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-1 mb-3">
                        
                    </div> -->
                   
            </div>
            

          
        </div>

</div>
<div style="height: 150px;"></div>
  </form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
    
function DeleteRoutes(cityID)
{
  var r = confirm("Are you sure you want to delete this routes");
  if(r)
  {
  o =new Array(cityID);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=settings&function=deleteroute&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: DeleteSuccessCallBack});
  }
  else
  {
    return;
  }
}
function DeleteSuccessCallBack(response)
{
  console.log(response.responseText);
  alert('Delete Sucessfully');
  LoadAjaxScreen("addroutes");
}
function ReportError(response)
{
  console.log(response.responseText);
  alert('NOT DELETED!!');
  LoadAjaxScreen("addroutes");
}

  
    
function AddRoutesCheck(){
    var a=document.getElementById('from_city').value;
    var b=document.getElementById('to_city').value;
    rates=document.getElementById('rates').value;

        

    if(a=='0')
  {
    alert("Please select From city");
    return false;
  }
  if(b=='0')
  {
    alert("Please select To city");
    return false;
  }

  if(a == b)
  {
    alert("Both Are Same ");
    return false;
  }
  if (rates == ""|| rates == 0 ) {
            alert("Rates must be filled");
          return false;
        }
  return true;
   

     
}

function AddRoutes(){
 var check=AddRoutesCheck();
if(check==true){
  if(confirm("Are you sure you want to add this Route?"))
    {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('addcityform');   
        PostAjaxScreen("addcityform",form,callfunc);
    } 
}

}

function NextPage(){
      //alert(response.responseText);
      // arr = JSON.decode(response.responseText);
      // if(arr.code != 0)
      //   {
      //     alert(arr.message);
      //     return false;
      //   }

    alert('Successfullu Added')  //console.log(arr);
      LoadAjaxScreen("addroutes");
}


function UpdateCity() {
    if(confirm("Are you sure you want to update this City?"))
    {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('addcityform');   
        PostAjaxScreen("addcityform",form,callfunc);
    }
}

function NextPage1(response){
      LoadAjaxScreen("addroutes");
}

</script>


<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>

<script src="js/datagrid/datatables/datatables.bundle.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    var table = $('#dt-basic-example').DataTable();
     
    $('#dt-basic-example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
       // document.getElementById("CityUpdate").value = data[1];
       // document.getElementById("CityID").value = data[0];
       // document.getElementById("CityUpdate").disabled = false;
    } );
} );
    
</script>
<script type="text/javascript">
  document.getElementById('rates').addEventListener('input', event =>
  event.target.value = (parseInt(event.target.value.replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US')
);
</script>
