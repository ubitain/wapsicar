<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/addstuff.php");

$city_id = $_REQUEST['city_id'];

$blStuff = new BL_AddStuff();
$cityInfo = $blStuff->GetCityById($city_id)->rows[0];

//print_r($documentInfo);
?>
<main id="js-page-content" role="main" class="page-content">

<form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
<input type='hidden' name='ACTION' value='ADDCITY'>
<input type='hidden' name='userid' value='<?=$userid?>'>
<input type='hidden' name='CityID' value='<?=$cityInfo['city_id']?>'>

<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
      

          <div class="form-row">
            <div class="col-md-12 mb-12">
                        <label class="form-label" for="validationTooltip01">City Name</label>
                        <input type="text" class="form-control" id="city_name" name="city_name" multiple="true" placeholder=""    value="<?=$cityInfo['city_name']?>" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)" maxlength="12">
                    </div>
          </div>
        
        <div class="form-row">
                    <div class="col-md-12 mb-12">
                        <!-- <label class="form-label" for="validationTooltip01" style="margin-top: 6px">Status</label> -->
                         <!-- <select id="status" name="status" class="form-control"  >
                         <option value="1" <?=($cityInfo['status']=="1"?"checked":"")?>>Active</option>
                         <option value="0" <?=($cityInfo['status']=="0"?"checked":"")?>>Disable</option>
                 
                    </select> -->
                     
                    </div>
                    
                   
        </div>
      

<div style="margin-top: 20px;"></div>
<div class="form-row">

<div class="col-md-12  mb-12" >

    <button type="button" onClick="CloseModalBox();" class="btn btn-med btn-dark" style="background: #ff8000; color: black">
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
    <button type="button" onclick='EditCity()' class="btn btn-med btn-warning" style="background: #ff8000">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Edit
    </button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>



<script type="text/javascript">


function EditCity() {


    if(confirm("Are you sure you want to update this city?"))
    {
    var callfunc={onSuccess:NextPage};
    form= document.getElementById('adduserform');   
    PostFileAjaxScreen("adduserform",form,callfunc);
    }

}
function ValidateLeadData()
{
    name=document.getElementById('name').value;
    var illegalCharacters = name.match(/[^a-zA-Z- ]/g);
    if (illegalCharacters) {
        $("#name").focus();
        alert('Wrong Name!!');
        return false;
    }
    email=document.getElementById('email').value;
    var emailregex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (emailregex.test(email) == false){
        alert("Wrong Email! (Hint:abc@gmail.com)");
        return false;
    }
}

function NextPage(){
CloseModalBox();
LoadAjaxScreen("addcity");
}

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;
var fromdate = today;
document.getElementById("joiningdate").max = fromdate;
//////////////////////////////


$("#name").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#name").val().length >35 ) {
            return false;
       }



});

$("#cnic").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#cnic").val().length >12 ) {
            return false;
       }



});

$("#email").keypress(function(e){

      if( $("#email").val().length >50 ) {
            return false;
        }

  });

$("#phonenumber").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#phonenumber").val().length >10 ) {
            return false;
       }
});

$("#registration").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#registration").val().length >35 ) {
            return false;
       }
});

$("#sapcode").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#sapcode").val().length >35 ) {
            return false;
       }
});

</script>

<script type="text/javascript">
  function Chec(){
   if(document.getElementById('city_name').value=='')
  {
    alert("Please insert mobile number");
    return false;
  }
  return true;
}
function EditCity(){
  var x=Chec();
  if(x==true){
    if(confirm("Are you sure you want to Add this user?")){
    var obj = {onSuccess:AddusersInfoCallBack};
          form = document.getElementById('adduserform');
          PostAjaxScreen("adduserform",form,obj)
        }
  }
    function AddusersInfoCallBack(){
     alert('Successfully Update');
     LoadAjaxScreen("addcity");
    }
  }
</script>