<?php 
require_once(MODULE."/class/bllayer/user.php");
$user = new BL_User();
$dealerCatergory = $user->GetDealerCatergory();
$count =  $dealerCatergory->count;

$subDealerCatergory = $user->GetSubDealerCatergory();
$count1 =  $subDealerCatergory->count;

$groups = $user->GetGroups();
$count2 =  $groups->count;

$city = $user->GetCity();
$count3 =  $city->count;

$regions = $user->GetRegions();
$count4 =  $regions->count;

$zones = $user->GetZones();
$count5 =  $zones->count;

$town = $user->GetTown();
$count6 =  $town->count;

?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Add User
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='adduserform' id='adduserform' method='post'>
<input type='hidden' name='ACTION' value='ADDUSER'>

<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <h5>User Details</h5>
                    </div>
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Registration Number</label>
                        <input type="text" class="form-control" id="registration" name="registration" placeholder=""    value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Sap Code</label>
                        <input type="text" class="form-control" id="sapcode" name="sapcode" placeholder=""    value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Dealer Catergory</label>
                        <select id="dealer" name="dealer" class="form-control" onchange='viewsubcategory(this.value)' >
                        <option value="">Select Dealer Catergory</option>
                        <?php 
                        for ($i=0; $i <$count; $i++) { 
                            $dealerCatergoryId = $dealerCatergory->rows[$i]['dealer_category_id'];
                            $dealerCatergoryName = $dealerCatergory->rows[$i]['dealer_category_name'];
                            $dealerCatergoryName = strtolower($dealerCatergoryName);
                            $dealerCatergoryName = ucfirst($dealerCatergoryName);
                            if ($dealerCatergoryName=="Superuser") {
                               
                            }
                            else
                            {
                        ?>
                            <option value="<?php echo $dealerCatergoryId ?>"><?php echo $dealerCatergoryName ?></option>
                        <?php 
                            }
                        }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Sub Dealer Catergory</label>
                        <select id="subdealer" name="subdealer" class="form-control">
                        <option value="">Select Sub Dealer Catergory</option> 
                        <!-- <?php 
                        for ($i=0; $i <$count1; $i++) { 
                            $subDealerCatergoryId = $subDealerCatergory->rows[$i]['sub_category_id'];
                            $subDealerCatergoryName = $subDealerCatergory->rows[$i]['sub_category_name'];
                        ?>
                            <option value="<?php echo $subDealerCatergoryId ?>"><?php echo $subDealerCatergoryName ?></option>
                        <?php 
                        }
                        ?> -->
                        </select>
                    </div>
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder=""   value="" >
                    </div>
                    <!-- <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Gender</label>
                        <select id="gender" name="gender" class="form-control">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        </select>
                    </div> -->
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">CNIC</label>
                        <input type="text" class="form-control" id="cnic" name="cnic" placeholder="" d   value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">Group</label>
                        <select id="group" name="group" class="form-control">
                        <option value="">Select Group</option>
                        <?php 
                        for ($i=0; $i <$count2; $i++) { 
                            $groupId = $groups->rows[$i]['group_id'];
                            $groupName = $groups->rows[$i]['group_name'];
                        ?>
                            <option value="<?php echo $groupId ?>"><?php echo $groupName ?></option>
                        <?php 
                        }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Joining Date</label>
                        <input type="date" class="form-control" id="joiningdate"  name="joiningdate" placeholder="" value="" >
                    </div>
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Mobile Number</label>
                        <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="" d   value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="" d   value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Address</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="" d   value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Country</label>
                        <select id="country" name="country" class="form-control">
                        <option value="">Select Country</option>
                        <option value="Pakistan">Pakistan</option>
                        </select>
                    </div>


                    
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Zone</label>
                        <select id="zone" name="zone" class="form-control" onchange='zonetoregion(this.value)' >
                            <option value="">Select Zone</option>
                         <?php 
                        for ($i=0; $i <$count5; $i++) { 
                            $zonesName = $zones->rows[$i]['zone_name'];
                            $zoneId = $zones->rows[$i]['zone_id'];
                            $zonesName = strtolower($zonesName);
                            $zoneId = strtolower($zoneId);
                            $zonesName = ucfirst($zonesName);
                            $zoneId = ucfirst($zoneId);
                        ?>
                            <option value="<?php echo $zonesName.",".$zoneId ?>"><?php echo $zonesName ?></option>
                        <?php 
                        }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Region</label>
                        <select id="region" name="region" class="form-control" onchange='regiontocity(this.value)'>
                             <option value="">Select Region</option>
                      <!--   <?php 
                        for ($i=0; $i <$count4; $i++) { 
                            $regionsName = $regions->rows[$i]['region_name'];
                            $regionsName = strtolower($regionsName);
                            $regionsName = ucfirst($regionsName);
                        ?>
                            <option value="<?php echo $regionsName ?>"><?php echo $regionsName ?></option>
                        <?php 
                        }
                        ?> -->
                        </select>
                    </div> 
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="validationTooltip01">City</label>
                        <select id="city" name="city" class="form-control" onchange='citytoarea(this.value)'>
                        <option value="">Select City</option>
                        <!-- <?php 
                        for ($i=0; $i <$count3; $i++) { 
                            $cityName = $city->rows[$i]['city_name'];
                        ?>
                            <option value="<?php echo $cityName ?>"><?php echo $cityName ?></option>
                        <?php 
                        }
                        ?> -->
                        </select>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Area</label>
                        <select id="area" name="area" class="form-control">
                        <option value="">Select Area</option>

                        </select>
                    </div>
                    
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Town</label>
                        <select id="town" name="town" class="form-control">
                        <option value="">Select Town</option>
                        <?php 
                        for ($i=0; $i <$count6; $i++) { 
                            $townName = $town->rows[$i]['town'];
                            $townId = $town->rows[$i]['town_id'];
                            $townName = strtolower($townName);
                            $townId = strtolower($townId);
                            $townName = ucfirst($townName);
                            $townId = ucfirst($townId);
                        ?>
                            <option value="<?php echo $townName ?>"><?php echo $townName ?></option>
                        <?php 
                        }
                        ?>
                        </select>
                    </div> 
                   
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Market</label>
                        <input type="text" class="form-control" id="market" name="market" placeholder="" d   value="" >
                    </div>
                   
                    
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Login ID</label>
                        <input type="text" class="form-control" id="loginid" name="loginid" placeholder="" d   value="" >
                    </div>
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Example: $ab13c4$@#!" d   value="" >
                    </div>
        </div>
        <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Account Off?</label>
                        <select id="accountoffon" name="accountoffon" class="form-control">
                        <option value="">Select One</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                        </select>
                    </div> 
        </div>
      

<div style="margin-top: 20px;"></div>
<div class="form-row">
<div class="col-md-9  mb-3">
</div>
<div class="col-md-1  mb-3">
</div>
<div class="col-md-2  mb-3" >

    <button type="button" onclick='AddUser()' class="btn btn-med btn-warning">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Add
    </button>
    <button type="button" onClick="LoadAjaxScreen('viewuser')" class="btn btn-med btn-dark">
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>


<script type="text/javascript">

function viewsubcategory(id)
{
//alert(id);
if (id==1 || id==2 || id==9) {
    document.getElementById("accountoffon").disabled = true;
    document.getElementById("group").disabled = true;
    document.getElementById("zone").disabled = true;
    document.getElementById("region").disabled = true;
    document.getElementById("city").disabled = true;
    document.getElementById("area").disabled = true;
    document.getElementById("town").disabled = true;
    document.getElementById("market").disabled = true;
}
else{
    document.getElementById("accountoffon").disabled = false;
    document.getElementById("group").disabled = false;
    document.getElementById("zone").disabled = false;
    document.getElementById("region").disabled = false;
    document.getElementById("city").disabled = false;
    document.getElementById("area").disabled = false;
    document.getElementById("town").disabled = false;
    document.getElementById("market").disabled = false;
}
if(id == '')
{
    alert("No ID found!!");
}
else
{
o =new Array(id);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetSubCategory&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetSubCategoryCallBack});

}
}
function GetSubCategoryCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('subdealer').options.length = 0;
document.getElementById('subdealer').innerHTML += "<option value=''>Select Sub Dealer Catergory</option>";
document.getElementById('subdealer').innerHTML += obj;
}
///
function zonetoregion(zone)
{
// alert(zone);
if(zone == '')
{
alert("No ID found!!");
}
else
{
o =new Array(zone);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetRegion&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetRegionCallBack});

}
}
function GetRegionCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('region').options.length = 0;
document.getElementById('region').innerHTML += "<option value=''>Select Region</option>";
document.getElementById('region').innerHTML += obj;
}
///
function regiontocity(region)
{
// alert(region);
if(region == '')
{
alert("No ID found!!");
}
else
{
o =new Array(region);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetCity&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetCityCallBack});

}
}
function GetCityCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('city').options.length = 0;
document.getElementById('city').innerHTML += "<option value=''>Select City</option>";
document.getElementById('city').innerHTML += obj;
}
///
function citytoarea(city)
{
//  alert(city);
if(city == '')
{
alert("No ID found!!");
}
else
{
o =new Array(city);
o = JSON.encode(o);
var pars = 'param='+o;
var url = "/index.php?object=user&function=GetArea&isajaxcall=1";
var myAjax = new Ajax.Request( url,
{ method: 'post', parameters: pars, onFailure: CheckError , onSuccess: GetAreaCallBack});

}
}
function GetAreaCallBack(response)
{
var obj = JSON.decode(response.responseText);
document.getElementById('area').options.length = 0;
document.getElementById('area').innerHTML += "<option value=''>Select Area</option>";
document.getElementById('area').innerHTML += obj;
}
///
function  CheckError()
{
console.log('error');
}


function AddUser() {

var id = document.getElementById("dealer").value;
if (id=="") {
    alert("Dealer Not Found!!");
    return false;
}
if (id==1 || id==2 || id==9) {
    // document.getElementById("accountoffon").disabled = true;
    // document.getElementById("group").disabled = true;
    // document.getElementById("zone").disabled = true;
    // document.getElementById("region").disabled = true;
    // document.getElementById("city").disabled = true;
    // document.getElementById("area").disabled = true;
    // document.getElementById("town").disabled = true;
    // document.getElementById("market").disabled = true;

    var registration = document.getElementById("registration").value;
    var sapcode = document.getElementById("sapcode").value;
    var dealer = document.getElementById("dealer").value;
    var subdealer = document.getElementById("subdealer").value;
    var name = document.getElementById("name").value;
    var cnic = document.getElementById("cnic").value;
    var joiningdate = document.getElementById("joiningdate").value;
    var phonenumber = document.getElementById("phonenumber").value;
    var email = document.getElementById("email").value;
    var address = document.getElementById("address").value;
    var country = document.getElementById("country").value;
    var loginid = document.getElementById("loginid").value;
    var password = document.getElementById("password").value;

    if (registration=="") {
        alert("Registration Number Not Found!!");
        return false;
    }
    if (sapcode=="") {
        alert("Sapcode Not Found!!");
        return false;
    }
    if (dealer=="") {
        alert("Dealer Not Found!!");
        return false;
    }
    if (subdealer=="") {
        alert("SubDealer Not Found!!");
        return false;
    }
    if (name=="") {
        alert("Name Not Found!!");
        return false;
    }
    if (cnic=="") {
        alert("Cnic Not Found!!");
        return false;
    }
    if (joiningdate=="") {
        alert("Joining Date Not Found!!");
        return false;
    }
    if (phonenumber=="") {
        alert("Phone Number Not Found!!");
        return false;
    }
    if (email=="") {
        alert("Email Not Found!!");
        return false;
    }
    if (address=="") {
        alert("Address Not Found!!");
        return false;
    }
    if (country=="") {
        alert("Country Not Found!!");
        return false;
    }
    if (loginid=="") {
        alert("Login Id Not Found!!");
        return false;
    }
    if (password=="") {
        alert("Password Not Found!!");
        return false;
    }

    if(confirm("Are you sure you want to add this user?"))
    {
    var callfunc={onSuccess:NextPage};
    form= document.getElementById('adduserform');   
    PostAjaxScreen("adduserform",form,callfunc);
    }
}
else{

    var registration = document.getElementById("registration").value;
    var sapcode = document.getElementById("sapcode").value;
    var dealer = document.getElementById("dealer").value;
    var subdealer = document.getElementById("subdealer").value;
    var name = document.getElementById("name").value;
    var cnic = document.getElementById("cnic").value;
    var group = document.getElementById("group").value;
    var joiningdate = document.getElementById("joiningdate").value;
    var phonenumber = document.getElementById("phonenumber").value;
    var email = document.getElementById("email").value;
    var address = document.getElementById("address").value;
    var country = document.getElementById("country").value;
    var zone = document.getElementById("zone").value;
    var region = document.getElementById("region").value;
    var city = document.getElementById("city").value;
    var area = document.getElementById("area").value;
    var town = document.getElementById("town").value;
    var market = document.getElementById("market").value;
    var loginid = document.getElementById("loginid").value;
    var password = document.getElementById("password").value;
    var accountoffon = document.getElementById("accountoffon").value;

    if (registration=="") {
        alert("Registration Number Not Found!!");
        return false;
    }
    if (sapcode=="") {
        alert("Sapcode Not Found!!");
        return false;
    }
    if (dealer=="") {
        alert("Dealer Not Found!!");
        return false;
    }
    if (subdealer=="") {
        alert("SubDealer Not Found!!");
        return false;
    }
    if (name=="") {
        alert("Name Not Found!!");
        return false;
    }
    if (cnic=="") {
        alert("Cnic Not Found!!");
        return false;
    }
    if (group=="") {
        alert("Group Not Found!!");
        return false;
    }
    if (joiningdate=="") {
        alert("Joining Date Not Found!!");
        return false;
    }
    if (phonenumber=="") {
        alert("Phone Number Not Found!!");
        return false;
    }
    if (email=="") {
        alert("Email Not Found!!");
        return false;
    }
    if (address=="") {
        alert("Address Not Found!!");
        return false;
    }
    if (country=="") {
        alert("Country Not Found!!");
        return false;
    }
    if (zone=="") {
        alert("Zone Not Found!!");
        return false;
    }
    if (region=="") {
        alert("Region Not Found!!");
        return false;
    }
    if (city=="") {
        alert("City Not Found!!");
        return false;
    }
    if (area=="") {
        alert("Area Not Found!!");
        return false;
    }
    if (town=="") {
        alert("Town Not Found!!");
        return false;
    }
    if (market=="") {
        alert("Market Not Found!!");
        return false;
    }
    if (loginid=="") {
        alert("Login Id Not Found!!");
        return false;
    }
    if (password=="") {
        alert("Password Not Found!!");
        return false;
    }
    if (accountoffon=="") {
        alert("Account Of Not Found!!");
        return false;
    }

    

    var validateLeadDataVar = ValidateLeadData();
    if (validateLeadDataVar==true) {
        if(confirm("Are you sure you want to add this user?"))
        {
        var callfunc={onSuccess:NextPage};
        form= document.getElementById('adduserform');   
        PostAjaxScreen("adduserform",form,callfunc);
        }
    }
}
}
function ValidateLeadData()
{
    name=document.getElementById('name').value;
    var illegalCharacters = name.match(/[^a-zA-Z- ]/g);
    if (illegalCharacters) {
        $("#name").focus();
        alert('Wrong Name!!');
        return false;
    }
    email=document.getElementById('email').value;
    var emailregex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (emailregex.test(email) == false){
        alert("Wrong Email! (Hint:abc@gmail.com)");
        return false;
    }
}

function NextPage(){
LoadAjaxScreen("viewuser");
}

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;
var fromdate = today;
document.getElementById("joiningdate").max = fromdate;
//////////////////////////////


$("#name").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#name").val().length >35 ) {
            return false;
       }



});

$("#cnic").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#cnic").val().length >12 ) {
            return false;
       }



});

$("#email").keypress(function(e){

      if( $("#email").val().length >50 ) {
            return false;
        }

  });

$("#phonenumber").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[0-9  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }

    if( $("#phonenumber").val().length >10 ) {
            return false;
       }
});

$("#registration").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#registration").val().length >35 ) {
            return false;
       }
});

$("#sapcode").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#sapcode").val().length >35 ) {
            return false;
       }
});

</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>