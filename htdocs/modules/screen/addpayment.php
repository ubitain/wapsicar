<?php 
require_once(MODULE."/class/bllayer/user.php");
require_once(MODULE."/class/bllayer/viewuser.php");

?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
Payments
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='adduserform' id='adduserform'>
<input type='hidden' name='ACTION' value='ADDPAYMENT'>
<input type='hidden' class="form-control" id="id" name="id" placeholder="" value="<?php echo $settingInfo['id'] ?>" >
<div class="row" style="margin-top: -30px;">
<div class="col-xl-12">
<div id="panel-1" style="" class="panel">


<div class="panel-container show">
    <div class="panel-content" >
        <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <h5 onclick="LoadAjaxScreen('addpayment')">Add Payment</h5>
                    </div>
        </div>
       
        <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="validationTooltip01">Transaction Number</label>
                        <input type="text" class="form-control" id="transaction_number" name="transaction_number" placeholder=""   value="" onblur="CheckTransaction(this.value)">
                    </div>
                    <!-- <div class="col-md-3 mb-3">
                        <label class="form-label" for="">Gender</label>
                        <select id="gender" name="gender" class="form-control">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        </select>
                    </div> -->
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="validationTooltip01">Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount" placeholder=""    value="" >
                    </div>
                      
                   

                    
        </div>
       
      
      

<div style="margin-top: 20px;"></div>
<div class="form-row">
<div class="col-md-8  mb-3">
</div>
<div class="col-md-2  mb-3">

     <button type="button" onClick="LoadAjaxScreen('viewpayments')" class="btn btn-med btn-dark">
        <span class="fal fa fa-arrow-left mr-1"></span>
        Back
    </button>
    
</div>
<div class="col-md-2  mb-3" >

    <button type="button" onclick='AddPayment()' class="btn btn-med btn-warning">
        <span class="fal fa fa-arrow-right mr-1"></span>
        Update
    </button>
   
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</main>
<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script type="text/javascript">
function CheckTransaction(transactionId){

  // alert(empId);
  o =new Array(transactionId);
  o = JSON.encode(o);
  var pars = 'param='+o;
  var url = "/index.php?object=payment&function=CheckTranscation&isajaxcall=1&returnType=string";
  var myAjax = new Ajax.Request( url,
  { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: CheckTransactionCallBack});

}
function CheckTransactionCallBack(res){
var ret = JSON.decode(res.responseText);
console.log(ret);
if(ret == 1)
{
    alert("Already Approved");
    $('#transaction_number').val('');
}
// else
// {
//      alert("Not Exit Or  NOT Approved");
// }
}
function AddPayment() {

    if($("#transaction_number").val() == "")
    {
        alert("transaction number cannot be empty");
        return false;
    }


    if($("#amount").val() == "")
    {
        alert("amount number cannot be empty");
        return false;
    }
if(confirm("Are you sure you want to Add this Update?"))
{
var callfunc={onSuccess:NextPage};
form= document.getElementById('adduserform');   
PostAjaxScreen("adduserform",form,callfunc);
}
}

function NextPage(){
LoadAjaxScreen("addpayment");
}


// function GetName()
// {
//   var empId = document.getElementById("emp_id").value;
//   // alert(empId);
//   o =new Array(empId);
//   o = JSON.encode(o);
//   var pars = 'param='+o;
//   var url = "/index.php?object=attendance&function=GetName&isajaxcall=1&returnType=string";
//   var myAjax = new Ajax.Request( url,
//   { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: AttendanceCallBack});
// }

// function AttendanceCallBack(res)
// {
//     ret = JSON.decode(res.responseText);
//   //   alert(ret.length);
//   // console.log(ret);
//   if(ret.length == 0 )
//   {
//     alert("Worng Employee Id");
//     return false;
//   }
//   else{
//        document.getElementById('emp_name').value= ret[0]['first_name'];
//    }

 // var data = JSON.decode(res.responseText);
// var myString = JSON.stringify(data);
  //alert(myString);
  //alert('User is in the list');

  //LoadAjaxScreen("addattendance");
// }
function ReportError()
{
  alert("Error!");
}

</script>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>