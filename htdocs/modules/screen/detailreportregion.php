<?php
require_once(MODULE."/class/bllayer/detailreports.php");
    
    $searchFrom=$_GET['from'];
    $searchTo=$_GET['to'];
    $status="";
    if($searchFrom!="" && $searchTo!="")
    {
        // $viewClaims=new BL_ViewClaims();
        // $fetchData=$viewClaims->ViewCompletedClaimsDate($searchFrom,$searchTo);
        // $count =  $fetchData->count;
        $searchFrom = date('Y-m-d',strtotime($searchFrom));
        $searchTo = date('Y-m-d',strtotime($searchTo));
        $searchTo = str_replace('-', '/', $searchTo);
        $searchTo = date('Y-m-d',strtotime($searchTo . "+1 days"));
        // $viewClaims=new BL_ViewClaims();
        // $fetchData=$viewClaims->ViewInProcessClaimsDate($searchFrom,$searchTo);
        // $count =  $fetchData->count;
        $status="Date";
        $reports = new BL_Reports();   
        $regionDetail = $reports->GetRegionDetail();
        $countRegionDetail =  $regionDetail->count;
        $regionName = $reports->GetRegionName();
        $countregionName =  $regionName->count;
    }
    else
    {
        $reports = new BL_Reports();   
        $regionDetail = $reports->GetRegionDetail();
        $countRegionDetail =  $regionDetail->count;
        $regionName = $reports->GetRegionName();
        $countregionName =  $regionName->count;
    }

    $finalRegionCount=0;    
    $finalAccepted=0;    
    $finalRejected=0;    
    $finalOthers=0;    

?>
<main id="js-page-content" role="main" class="page-content">

<ol class="breadcrumb page-breadcrumb">
<h1 class="subheader-title">
<i class='subheader-icon fal fa-chart-area'></i> Region Report
</h1>
<li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
</ol>

<form class="form-horizontal" name='updatefinancee' id='updatefinancee' method='post'>

 <div id="panel-10" class="panel" style="margin-top: -30px;">
       
       
        <div class="panel-container show" style="margin-top: -50px;">
            <div class="panel-content">
                <div class="tab-content py-3">
                    <div style="">
                        <div style="margin-top: 50px;"></div>
                        <div class="form-row">   
                            <div class="col-md-4 mb-3" >
                                <label class="form-label">Select Dates</label>
                                <div class="input-daterange input-group" id="datepicker-5">
                                    <input type="text" class="form-control" readonly="" name="fromdate" placeholder="From Date" id="fromdate">
                                    <div class="input-group-append input-group-prepend">
                                        <span class="input-group-text fs-xl"><i class="fal fa-ellipsis-h"></i></span>
                                    </div>
                                    <input type="text" class="form-control" readonly="" name="todate"  placeholder="To Date" id="todate">
                                </div>
                            </div>
                            <!-- <div class="col-md-2 mb-3" >
                                <label class="form-label"> From Date</label>
                                <div class="input-group" style=" ">
                                    
                                    <input type="date" class="form-control " onchange="selectdate()" placeholder="" id="fromdate">
                                    <div class="input-group-append">
                                        <span class="input-group-text fs-xl">
                                            <i class="fal fa-calendar-exclamation"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-3" >
                                <label class="form-label"> To Date</label>
                                <div class="input-group" style=" ">
                                    
                                    <input type="date" class="form-control " placeholder="" id="todate">
                                    <div class="input-group-append">
                                        <span class="input-group-text fs-xl">
                                            <i class="fal fa-calendar-exclamation"></i>
                                        </span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-md-2 mb-3" >
                                <label class="form-label"> Claim Number</label>
                                <div class="input-group" style=" ">            
                                    <input type="text" class="form-control " placeholder="" id="claimno">
                                </div>
                            </div> -->
                            <div class="col-md-3  mb-3" style="margin-top: 23px;">
                                <button type="button" onclick="DateOrClaimNoSearch()" class="btn btn-med btn-warning">
                                    <span class="fal fa fa-search mr-1"></span>
                                    Search
                                </button>
                            </div>
                    </div>
                    <hr>
                         <table class="table table-bordered table-hover m-0" id="partwise" >
                            <thead align="center">
                                <tr>
                                    <th>Serial No</th>
                                    <th>Region Name</th>
                                    <th>No of Complains</th>
                                    <th>Accepted Quantity</th>
                                    <th>Rejected Quantity</th>
                                    <th>Other Vendors Quantity</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                
                                <?php 

                                    for ($j=0; $j <$countregionName; $j++) { 
                                            $regionNameTable = $regionName->rows[$j]['region_name'];

                                                for ($i=0; $i <$countRegionDetail; $i++) {
                                                    $regionName2 = $regionDetail->rows[$i]['region'];
                                                    if ($regionNameTable==$regionName2) {
                                                        
                                                        $claimID = $regionDetail->rows[$i]['claim_id'];
                                                        if ($status=="Date") {
                                                        $quantities = $reports->GetRegionAccRejOthDate($claimID,$searchFrom,$searchTo);
                                                        $countquantities =  $quantities->count;
                                                        if ($countquantities>0) {
                                                            $finalRegionCount++;
                                                        }
                                                        $finalAccepted = $finalAccepted+$quantities->rows[0]['approved_qty'];
                                                        $finalRejected = $finalRejected+$quantities->rows[0]['rejected_qty'];
                                                        $finalOthers = $finalOthers+$quantities->rows[0]['other_qty'];
                                                        }
                                                        else{
                                                        $finalRegionCount++;
                                                        $quantities = $reports->GetRegionAccRejOth($claimID);
                                                        $finalAccepted = $finalAccepted+$quantities->rows[0]['approved_qty'];
                                                        $finalRejected = $finalRejected+$quantities->rows[0]['rejected_qty'];
                                                        $finalOthers = $finalOthers+$quantities->rows[0]['other_qty'];
                                                        }

                                                    }
                                                }

                                                ?>
                                    <tr>
                                        <th scope="row"><?php echo $j+1 ?></th>
                                        <td><?php echo $regionNameTable ?></td>
                                        <td><?php echo $finalRegionCount ?></td>
                                        <td><?php echo $finalAccepted ?></td>
                                        <td><?php echo $finalRejected ?></td>
                                        <td><?php echo $finalOthers ?></td>
                                        

                                    </tr>
                                 <?php 
                                 $finalRegionCount=0;
                                 $finalAccepted=0;
                                 $finalRejected=0;
                                 $finalOthers=0;
                             }
                                    
                                    // $itemID = $productDetail->rows[$i]['item_id'];
                                    // $regionName = $regionDetail->rows[$i]['regionname'];
                                    // $itemName = $productDetail->rows[$i]['item_name'];
                                    // $fetchData = $reports->FetchClaimItemCount($itemID);
                                    // $countProductDetail1 = $fetchData->count;
                                    // $fetchData = $reports->FetchClaimItemAccepted($itemID);
                                    // $claimItemAccepted = $fetchData->rows[0]['accepted'];
                                    // $fetchData = $reports->FetchClaimItemRejected($itemID);
                                    // $claimItemRejected = $fetchData->rows[0]['rejected'];
                                    // $fetchData = $reports->FetchClaimItemOthers($itemID);
                                    // $claimItemOthers = $fetchData->rows[0]['others'];
                                                ?>                                                       
                            </tbody>
                        </table>
                    </div>
                    
                    </div>
                </div>
            </div>

        </div>
    </form>
                   <div style="height: 150px;"></div>
        
    </div>




</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.bundle.js"></script>
<script src="js/datagrid/datatables/datatables.export.js"></script>
<script src="js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">

function DateOrClaimNoSearch() {
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    //var claimNo = document.getElementById("claimno").value;

    if(fromdate!="" && todate!=""){
        if (fromdate!="" && todate!="") {
                var callfunc={onSuccess:NextPageDate};
                form= document.getElementById('updatefinancee');   
                PostAjaxScreen("updatefinancee",form,callfunc);
        }
        else
        {
            alert('Please Select The Dates!');
        }
    }
    else
    {
        alert('Please Select The Date!');
    }
}
function NextPageDate(){
    var fromdate = document.getElementById("fromdate").value;
    var todate = document.getElementById("todate").value;
    LoadAjaxScreen("detailreportregion&from="+fromdate+"&to="+todate);
}


var controls = {
    leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
    rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
}

$('#datepicker-5').datepicker(
{
    todayHighlight: true,
    format: 'dd-M-yyyy',
    endDate: new Date(),
    startDate: '01-jan-2000',
    templates: controls
});

$("#claimno").keypress(function(e){
   var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9-  ]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
       return false
       }
    if( $("#claimno").val().length >25 ) {
            return false;
       }
});
    

</script>
<script>
            $(document).ready(function()
            {

                // initialize datatable
                $('#partwise').dataTable(
                {
                    responsive: true,
                    lengthChange: false,
                    dom:
                        /*  --- Layout Structure 
                            --- Options
                            l   -   length changing input control
                            f   -   filtering input
                            t   -   The table!
                            i   -   Table information summary
                            p   -   pagination control
                            r   -   processing display element
                            B   -   buttons
                            R   -   ColReorder
                            S   -   Select

                            --- Markup
                            < and >             - div element
                            <"class" and >      - div with a class
                            <"#id" and >        - div with an ID
                            <"#id.class" and >  - div with an ID and a class

                            --- Further reading
                            https://datatables.net/reference/option/dom
                            --------------------------------------
                         */
                        "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-12 d-flex align-items-center justify-content-end'lB>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    buttons: [
                        /*{
                            extend:    'colvis',
                            text:      'Column Visibility',
                            titleAttr: 'Col visibility',
                            className: 'mr-sm-3'
                        },*/
                        {
                            extend: 'pdfHtml5',
                            text: 'PDF',
                            titleAttr: 'Generate PDF',
                            className: 'btn-outline-danger btn-sm mr-1'
                        },
                        {
                            extend: 'excelHtml5',
                            text: 'Excel',
                            titleAttr: 'Generate Excel',
                            className: 'btn-outline-success btn-sm mr-1'
                        },
                        {
                            extend: 'csvHtml5',
                            text: 'CSV',
                            titleAttr: 'Generate CSV',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'copyHtml5',
                            text: 'Copy',
                            titleAttr: 'Copy to clipboard',
                            className: 'btn-outline-primary btn-sm mr-1'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            titleAttr: 'Print Table',
                            className: 'btn-outline-primary btn-sm'
                        }
                    ]
                });

            });

        </script>