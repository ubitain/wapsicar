<?php
    require_once(MODULE."/class/bllayer/viewclaims.php");
    
    $viewClaims=new BL_ViewClaims();
    $blCountData=$viewClaims->ViewClaimsTotal();
    $count =  $blCountData->count;

    $claimsToday=$viewClaims->ClaimsToday();
    $countToday =  $claimsToday->count;

    $claimsMonth=$viewClaims->ClaimsMonth();
    $countMonth =  $claimsMonth->count;


    $claimsYear=$viewClaims->ClaimsYear();
    $countYear =  $claimsYear->count;

    $blCountData=$viewClaims->ViewPendingClaims();
    $countPending =  $blCountData->count;

    $blCountData=$viewClaims->ViewInProcessClaims();
    $countInProcess =  $blCountData->count;

    $blCountData=$viewClaims->ViewCompletedClaims();
    $countCompleted =  $blCountData->count;

    $blCountData=$viewClaims->ViewInCompletedClaims();
    $countInComplete =  $blCountData->count;

    $blCountData=$viewClaims->ViewInProcessResolveClaims();
    $countCompletedResolve =  $blCountData->count;

    $blCountData=$viewClaims->ViewTopProductsInClaim();
    $viewTopProductsInClaim =  $blCountData->count;

   
    
?>


<main id="js-page-content" role="main" class="page-content">

                        <ol class="breadcrumb page-breadcrumb">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-chart-area'></i><span class='fw-300'>Dashboard</span>
                            </h1>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
                        </ol>
                        <!--   <div class="subheader">
                     </div> -->

                     <div class="row" style="margin-top: -25px;">
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $count; ?>
                                            <small class="m-0 l-h-n">Total Claims</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countToday; ?>
                                            <small class="m-0 l-h-n">Reported Today</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countMonth; ?>
                                            <small class="m-0 l-h-n">Reported This Month</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            <?php echo $countYear; ?>
                                            <small class="m-0 l-h-n">Reported This Year</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                    </div>

<div class="row"  >
    <div class="col-lg-6">
        <div id="panel-4" class="panel">
            <div class="panel-container show" style="">
                    <div class="form-row">
                        <div class="col-md-12 mb-3" style="margin-left: 15px; margin-top: 20px;">
                            <h3>Claims (Status Wise)</h3>
                        </div>
                    </div>
                    <div class="form-row" style="margin-left: 5px; margin-right: 5px;">
                        <div class="col-md-12 mb-3">
                            <div style="margin-top:0px;"></div>
                            <table class="table table-bordered table-hover m-0" style="text-align: center;">
                            <thead>
                                <tr>
                                    
                                    <th>Serial No</th>
                                    <th>Name</th>
                                    <th>No. of Claims</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Approved</td>
                                    <td><?php echo $countPending; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Inprocess</td>
                                    <td><?php echo $countInProcess; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>Completed</td>
                                    <td><?php echo $countCompletedResolve; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>Closed</td>
                                    <td><?php echo $countCompleted; ?></td>
                                </tr>
                            </tbody>
                        </table>


                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6" >
        <div id="panel-4" class="panel">
            <div class="panel-container show" style="">
                    <div class="form-row">
                        <div class="col-md-12 mb-3" style="margin-left: 15px; margin-top: 20px;">
                            <h3>Top 3 Claims (Part Wise)</h3>
                        </div>
                    </div>
                    <div class="form-row" style="margin-left: 5px; margin-right: 5px;">
                        <div class="col-md-12 mb-3">
                            
                            <div style="margin-top:0px;"></div>
                            <table class="table table-bordered table-hover m-0" style="text-align: center;">
                            <thead>
                                <tr>
                                    <th>Serial No</th>
                                    <th>Part Code</th>
                                    <th>Part Name</th>
                                    <th>No. of Claims</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?

                                for ($i=0; $i <$viewTopProductsInClaim; $i++) { 
                              //for
                                    $itemID = $blCountData->rows[$i]['ITEM_ID'];
                                    $totalQuantity = $blCountData->rows[$i]['TotalQuantity'];
                                    $blCountData1=$viewClaims->FetchProductName($itemID);
                                    $claimProductName = $blCountData1->rows[0]['ITEM_NAME'];
                                    $itemCode = $blCountData1->rows[0]['item_code'];
                            
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $i+1 ?></th>
                                    <td><?php echo $itemCode; ?></td>
                                    <td><?php echo $claimProductName; ?></td>
                                    <td><?php echo $totalQuantity ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                        </div>
                    </div>
            </div>
            <div style="height: 42px;"></div>
        </div>

    </div>


</div>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>
<?php

    $viewClaims=new BL_ViewClaims();
    $blCountData=$viewClaims->ViewCompletedClaims();
    $count =  $blCountData->count;

?>

    <div style="height: 140px;"></div>
</main>

<script>
$(document).ready(function()
{
$('#tableabc').dataTable(
{
responsive: true,
dom:
"<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
buttons: [
{

},
{

},
{

},
{

},
{

},
{

},
{

}

],
select: false
});
});
</script>