<?php
 require_once(MODULE . "/class/bllayer/vehicles.php");
 $blAddvehicle = new BL_Vehicle();
 $getVehicle = $blAddvehicle->GetDetail($id);
//  echo($id);
//  print_r_pre($getVehicle);
    
?>
<div id="content">
<section id="" class="">
  <div class="row">
    <!-- NEW WIDGET START -->
   <div class="col-xl-12">
    <div id="panel-1" class="panel">
        <div class="panel-container show">
            <div class="panel-content">
        <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
          <thead class="bg-primary-600" style="background: #ff8000">
                <tr>
                  <th nowrap><center>S.No</center></th>
                  <th nowrap><center>Name</center></th>
                  <th nowrap><center>Description</center></th>
                  <th nowrap ><center>Model Name</center></th>
                  <th nowrap ><center>Car Type</center></th>
                  <th nowrap ><center>Action</center></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                for($i = 0; $i < $getVehicle->count; $i++){
                ?>
                <tr>
                  <td><?=$i+1?></td>
                  <td><?=$getVehicle->rows[$i]['name'];?></td>
                  <td><?=$getVehicle->rows[$i]['description']?></td>
                  <td><?=$getVehicle->rows[$i]['model_name']?></td>
                  <td><?=$getVehicle->rows[$i]['car_type']?></td>
                  <td><center>
                  <button class="btn btn-sm btn-primary" style="background: #ff8000" onclick="LoadAjaxScreen('editvehicalmodel&childId=<?=$getVehicle->rows[$i]['id']?>&parentId=<?=$getVehicle->rows[$i]['make_id']?>')"><i class="fal fa-edit"></i></button>
                  <button class="btn btn-sm btn-primary"  style="background: #ff8000"title="Delete"  onclick="VehicleModelDelete('<?=$getVehicle->rows[$i]['id']?>')"><i class="fal fa-times"></i></button>
                    </center>
                  </td>
                </tr>
                
             <?php  }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    </div>

    </section>
  </div>

  <script type="text/javascript">
    
    function VehicleModelDelete(id)
    {
    
      var r = confirm("Are you sure you want to delete?");
      if(r==true)
      {
      o =new Array(id);
      o = JSON.encode(o);
      var pars = 'param='+o;
      var url = "/index.php?object=delete&function=VehicleModelDelete&isajaxcall=1&returnType=string";
      var myAjax = new Ajax.Request( url,
      { method: 'post', parameters: pars, onFailure: ReportError , onSuccess: VehicleModelDeleteCallBack});
      }
      else
      {
        return;
      }
    }
    function VehicleModelDeleteCallBack(response)
    {
      console.log(response.responseText);
      alert('Delete Sucessfully');
      LoadAjaxScreen("viewvehiclemodel");
    }
    function ReportError(res)
    {
    }
    
      </script>
    