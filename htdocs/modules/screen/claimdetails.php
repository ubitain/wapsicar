
<style>
* {
  box-sizing: border-box;
}

.image1 {

  transition: transform .2s;
  width: 100px;
  height: 100px;
  margin: 0 auto;
}

.image1:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}

</style>

<?php
    require_once(MODULE . "/class/bllayer/claims.php"); 
    require_once(MODULE."/class/bllayer/viewclaims.php");
    $claimId=$_GET['id'];
    $page=$_GET['page'];
   // $userId = $mysession->getValue("userid");
    $viewClaims=new BL_ViewClaims();
    $viewClaimsMaxTrans=new BL_AddPurchaseFromAccountOff();
    $fetchData=$viewClaims->ViewClaimsDetail($claimId);
    $count =  $fetchData->count;

    for ($i=0; $i <1; $i++) { 
          //for
            $claimID = $fetchData->rows[$i]['claim_id'];
            $claimNo = $fetchData->rows[$i]['claim_no'];
            $dealerID = $fetchData->rows[$i]['user_id'];
            
            $fetchData1=$viewClaims->FetchDealerName($dealerID);
            $claimName = $fetchData1->rows[0]['full_name'];
            $claimDealerCatergory = $fetchData1->rows[0]['dealer_category_name'];
            $claimDealerCatergory=strtolower($claimDealerCatergory);
            $claimMobNumber = $fetchData1->rows[0]['mobile_no'];
            $claimDealerAddress = $fetchData1->rows[0]['address'];

            $market = $fetchData1->rows[0]['market'];
            $town = $fetchData1->rows[0]['town'];
            $zone = $fetchData1->rows[0]['zone'];
            $region = $fetchData1->rows[0]['region'];

            $purchasedFrom = $fetchData->rows[$i]['purchased_from'];
            if ($purchasedFrom=="" || $purchasedFrom=="0") {
                $getMaxTrans = $viewClaimsMaxTrans->GetMaxTrans($claimID);
                $claimTransctionsId = $getMaxTrans->rows[0]['claimTransctionsId'];
                if ($claimTransctionsId!="") {
                    $getMaxTransUser = $viewClaimsMaxTrans->GetMaxTransUser($claimTransctionsId);
                    $claimTransctionsUserId = $getMaxTransUser->rows[0]['user_id'];
                    $claimTransctionsUserStatus = $getMaxTransUser->rows[0]['status'];
                    $fetchData1=$viewClaims->FetchDealerName($claimTransctionsUserId);
                }
                else{
                    $fetchData1=$viewClaims->FetchDealerName(0);
                }
                
            }
            else
            {
                $fetchData1=$viewClaims->FetchDealerName($purchasedFrom);
                $claimTransctionsUserStatus="Accept";
            }
            $purchasedFrom = $fetchData1->rows[0]['full_name'];
            $purchasedFromDesig = $fetchData1->rows[0]['dealer_category_name'];

            $accountOf = $fetchData->rows[$i]['account_of'];
            if ($accountOf=="" || $accountOf=="0") {
                $getMaxTransAccount = $viewClaimsMaxTrans->GetMaxTransAccount($claimID);
                $claimTransctionsAccountId = $getMaxTransAccount->rows[0]['claimTransctionsAccountId'];
                if ($claimTransctionsAccountId!="") {
                    $getMaxTransUser = $viewClaimsMaxTrans->GetMaxTransUser($claimTransctionsAccountId);
                    $claimTransctionsUserIdAccount = $getMaxTransUser->rows[0]['user_id'];
                    $claimTransctionsUserStatusAccount = $getMaxTransUser->rows[0]['status'];
                    $fetchData1=$viewClaims->FetchDealerName($claimTransctionsUserIdAccount);
                }
                else
                {
                    $fetchData1=$viewClaims->FetchDealerName(0);
                }
                
            }
            else
            {
                $fetchData1=$viewClaims->FetchDealerName($accountOf);
                $claimTransctionsUserStatusAccount="Accept";
            }
            $accountOf = $fetchData1->rows[0]['full_name'];
            $accountOfDesig = $fetchData1->rows[0]['dealer_category_name'];

            $lcsNo = $fetchData->rows[$i]['lcs_no'];
            $cartonWeight = $fetchData->rows[$i]['carton_weight'];
            $receivingWeight = $fetchData->rows[$i]['receiving_weight'];
            $remarks = $fetchData->rows[$i]['remarks'];
            $claimStatus = $fetchData->rows[$i]['claim_status'];
            $Date = $fetchData->rows[$i]['created_at'];  
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");

            $dealerCategoryName=strtolower($dealerCategoryName);
            $dealerCategoryName=ucfirst($dealerCategoryName);
            $purchasedFromDesig=strtolower($purchasedFromDesig);
            $purchasedFromDesig=ucfirst($purchasedFromDesig);
            $accountOfDesig=strtolower($accountOfDesig);
            $accountOfDesig=ucfirst($accountOfDesig);
    }

    $fetchData2=$viewClaims->ViewClaimsProductDetail($claimId);
    $count2 =  $fetchData2->count;

    $fetchDataImages=$viewClaims->ViewClaimsProductImages($claimId);
    $countImages =  $fetchDataImages->count;

    $fetchDataPurchaseHierarchy = $viewClaims->ViewClaimsPurchaseHierarchyDetail($claimId);
    $countPurchaseHierarchy =  $fetchDataPurchaseHierarchy->count;

    $fetchData6=$viewClaims->ViewClaimsGrandTotal($claimId);
    $count6 =  $fetchData6->count;
    $grandTotal = $fetchData6->rows[0]['grand_total'];
    $discount = $fetchData6->rows[0]['discount'];

?>

<main id="js-page-content" role="main" class="page-content">

            <ol class="breadcrumb page-breadcrumb">
            <h1 class="subheader-title">
            Claim Details
            </h1>
            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
            </ol>


            <div class="row" style="margin-top: -20px;">
                <div class="col-xl-12">
                    <div id="panel-1" style="" class="panel">
                        
                        <div class="panel-container show">
                            <div class="panel-content" >
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Basic Information</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">ClaimNo</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" hidden="true"  value="<?php echo $claimID ?>" >
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimNo ?>" >
                                            </div>
                                            <?php 

                                            $tempStatus=$_GET['status'];
                                            if ($tempStatus==1) {
                                                $claimStatus = "Incomplete";
                                            }
                                            else if ($tempStatus==2) {
                                                $claimStatus = "Unapproved";
                                            }


                                            ?>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Status</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimStatus) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Remarks</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($remarks) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Date</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $Date ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Raised By</h5>
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Raised By Name</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimName) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Raised By Type</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimDealerCatergory) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Phone Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimMobNumber ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Address</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimDealerAddress) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Zone</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($zone) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Region</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($region) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Town</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($town) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Market</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($market) ?>" >
                                            </div>
                                </div>
                                <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <h5 style="font-weight: bold;">Purchased From</h5>
                                            </div>
                                </div>
                                <?php 
                                if($claimStatus=='inprocess' || $claimStatus=='approved' )
                                {
                                ?>
                                <div class="form-row">
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchased From</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true" title="<?php echo ucfirst($purchasedFrom)." - ".ucfirst($purchasedFromDesig)." - ".ucfirst($claimTransctionsUserStatus) ?>"  value="<?php echo ucfirst($purchasedFrom)." - ".ucfirst($purchasedFromDesig)." - ".ucfirst($claimTransctionsUserStatus) ?>" >
                                            </div>
                                            <div class="col-md-2 mb-3">
                                                <label class="form-label" for="validationTooltip01">On Account Of</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true" title="<?php echo ucfirst($accountOf)." - ".ucfirst($accountOfDesig)." - ".ucfirst($claimTransctionsUserStatusAccount) ?>" value="<?php echo ucfirst($accountOf)." - ".ucfirst($accountOfDesig)." - ".ucfirst($claimTransctionsUserStatusAccount) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Carton Weight (Kg)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $cartonWeight ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Consignment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $lcsNo ?>" >
                                            </div>
                                            <?php
                                            if ($claimStatus=='inprocess') {
                                            ?>
                                            <div class="col-md-2 mb-3" style="margin-top: 30px;">
                                                <button type="button" onclick="OpenModalBox('consignmentweight&id=<?php echo $claimID ?>&page=3','screen','');"  class="btn btn-sm btn-warning" >
                                                <span class="fal  fa-edit mr-1"></span>
                                                </button>
                                            </div>
                                    <?php   }
                                            else if ($claimStatus=='approved') {
                                            ?>
                                            <div class="col-md-2 mb-3" style="margin-top: 30px;">
                                                <button type="button" onclick="OpenModalBox('consignmentweight&id=<?php echo $claimID ?>&page=5','screen','');"  class="btn btn-sm btn-warning" >
                                                <span class="fal  fa-edit mr-1"></span>
                                                </button>
                                            </div>
                                        <?php }  ?>

                                </div>
                            <?php }
                            else
                                {
                                ?>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Purchased From</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($purchasedFrom)." - ".ucfirst($purchasedFromDesig)." - ".ucfirst($claimTransctionsUserStatus) ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">On Account Of</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($accountOf)." - ".ucfirst($accountOfDesig)." - ".ucfirst($claimTransctionsUserStatusAccount)  ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Carton Weight (Kg)</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $cartonWeight ?>" >
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Leopard Consignment Number</label>
                                                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $lcsNo ?>" >
                                            </div>

                                </div>
                            <?php } ?>
                                 
                            <?php 

                            if ($claimStatus=='closed' || $claimStatus=='completed' ||  $claimStatus=='inprocess') {
                                ?>
                                <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label" for="validationTooltip01">Receiving Weight (Kg)</label>
                                                <input type="text" id="receivingweight" name="receivingweight" class="form-control" id="validationTooltip01" placeholder=""   value="<?php echo $receivingWeight ?>" disabled="true">
                                            </div>

                                </div>
                                <?php
                            }

                            ?>

                                <?php 
                                if($claimStatus=='closed' || $claimStatus=='completed' || $claimStatus=='approved' || $claimStatus=='inprocess' || $claimStatus=='open')
                                {
                                ?>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <h5 style="font-weight: bold;">Purchased From Hierarchy</h5>
                                    </div>
                                </div>

                                <div class="form-row">
                                     <table class="table table-bordered table-hover m-0" style="text-align: center;">
                                        <thead>
                                            <tr style="background-color: #dfe1e6;">
                                                
                                                <th>Serial No</th>
                                                <th>Assigned By</th>
                                                <th>Assigned To</th>
                                                <th>Type (Assigned To)</th>
                                                <th>Phone Number (Assigned To)</th>
                                                <th>Address (Assigned To)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $finalPurchaseFrom="";
                                                $claimBy=$claimName;
                                                    for ($i=0; $i <$countPurchaseHierarchy; $i++) { 
                                
                                                    $userId = $fetchDataPurchaseHierarchy->rows[$i]['user_id'];
                                                    $status = $fetchDataPurchaseHierarchy->rows[$i]['status'];
                                                    $fetchDataPurchaseName = $viewClaims->FetchPurchaseName($userId);
                                                    $claimPurchaseType = $fetchDataPurchaseName->rows[0]['dealer_category_name'];
                                                    $claimPurchaseName = $fetchDataPurchaseName->rows[0]['full_name'];
                                                    $claimPurchaseMobileNo = $fetchDataPurchaseName->rows[0]['mobile_no'];
                                                    $claimPurchaseAddress = $fetchDataPurchaseName->rows[0]['address'];
                                                    $claimPurchaseArea = $fetchDataPurchaseName->rows[0]['area'];
                                                    $claimPurchaseCity = $fetchDataPurchaseName->rows[0]['city'];
                                                    $claimPurchaseRegion = $fetchDataPurchaseName->rows[0]['region'];
                                                    $claimPurchaseZone = $fetchDataPurchaseName->rows[0]['zone'];
                                                    $finalAdress = $claimPurchaseAddress.",".$claimPurchaseArea.",".$claimPurchaseCity.",".$claimPurchaseRegion.",".$claimPurchaseZone;
                                                        ?>
                                                        <tr>
                                                            <th scope="row"><?php echo $i+1 ?></th>
                                                            <td><?php echo $claimBy ?></td>
                                                            <td><?php echo $claimPurchaseName ?></td>
                                                            <td><?php echo ucfirst( strtolower($claimPurchaseType)) ?></td>
                                                            <td><?php echo $claimPurchaseMobileNo ?></td>
                                                            <td><?php echo $finalAdress ?></td>

                                                        </tr>
                                                        <?php
                                                        $claimBy=$claimPurchaseName;
                                                    }
                                            ?> 
                                            
                                            
                                        </tbody>
                                    </table>
                                    
                                <label style="padding-left: 5px;"><?php echo $finalPurchaseFrom ?></label>
                                <div style="height: 30px;"></div>
                                </div>
                                <?php 
                                }
                                ?>
                                

                                <?php 
                                if ($count2>=1)
                                {
                                ?>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <h5 style="font-weight: bold;">Claim Part Information</h5>
                                    </div>
                                </div>
                                <div class="form-row" style="">
                                <div class="col-md-12 mb-3">
                                    <div style="margin-top:0px;"></div>
                                    <table class="table table-bordered table-hover m-0"   style="text-align: center;">
                                    <thead>
                                        <tr style="background-color: #dfe1e6;">
                                            <th>Serial No</th>
                                            <th>Part Code</th>
                                            <th>Part Name</th>
                                            <th>Recieved Quantity</th>
                                            <?php
                                            if ($claimStatus=='inprocess' || $claimStatus=='completed' || $claimStatus=='closed') {
                                            ?>
                                            <th>Approved Quantity</th>
                                            <th>Rejected Quantity</th>
                                            <th>Other Vendor</th>
                                            <?php
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                <?

                                for ($i=0; $i <$count2; $i++) { 
                                      //for
                                        $claimItemId = $fetchData2->rows[$i]['item_id'];
                                        
                                        $recievedQty = $fetchData2->rows[$i]['recieved_qty'];
                                        $approvedQty = $fetchData2->rows[$i]['approved_qty'];
                                        $rejectedQty = $fetchData2->rows[$i]['rejected_qty'];
                                        $otherQty = $fetchData2->rows[$i]['other_qty'];
                                        $fetchData3=$viewClaims->FetchItemName($claimItemId);
                                        $claimItemCode = $fetchData3->rows[0]['item_code'];
                                        $claimItemName = $fetchData3->rows[0]['item_name'];

                                        $remarks = $fetchData2->rows[$i]['remarks'];

                                ?>
                                
                                        <tr>
                                            <th scope="row"><?php echo $i+1; ?></th>
                                            <td>
                                            <input type="text" class="form-control" id="validationTooltip01" placeholder="" hidden="true" value="<?php echo $claimItemId; ?>" >
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimItemCode) ?>" >
                                            </td>
                                            <td>
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimItemName ?>" >
                                            </td>
                                            <td>
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $recievedQty ?>" >
                                            </td>
                                            <?php
                                            if ($claimStatus=='inprocess' || $claimStatus=='completed' || $claimStatus=='closed') {
                                            ?>
                                            <th>
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $approvedQty ?>" >
                                            </th>
                                            <th>
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $rejectedQty ?>" >
                                            </th>
                                            <th>
                                            <input type="text" style="border: 5px; background-color: white; text-align: center;" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $otherQty ?>" >
                                            </th>
                                            <?php } ?>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                    </div>
                                </div>
                                <?php 
                                } 
                                ?>
                    <div class="form-row" style="margin-top: 30px;">
                        <div class="col-md-6 mb-3">
                            <h5 style="font-weight: bold;">Claim Parts Pictures</h5>
                        </div>
                    </div>
                    <div class="form-row" style="">
                        <?php
                         for ($i=0; $i <$countImages; $i++) { 
                          //for
                            $claimImagesId = $fetchDataImages->rows[$i]['claim_images_id'];
                            $claimImageUrl = $fetchDataImages->rows[$i]['claim_image_url'];
                        ?>
                        <div class="col-md-2 mb-3">
                            <img src='<?php echo $claimImageUrl ?>' height="150px" width="150px" onclick="OpenModalBox('claimimages&claimImageUrl=<?php echo $claimImageUrl ?>','screen','','700px');">
                        </div>
                        <?php 
                        }
                        ?>
                    </div>



                    <?php 
                    if($claimStatus=='closed')
                    {
                    ?>
                    <div class="form-row" >
                        <div class="col-md-12 mb-3">
                            <h5 style="font-weight: bold;">Claim Settlement Information</h5>
                        </div>
                    </div>
                            <div class="form-row" style="">
                            <div class="col-md-12 mb-3">
                                <div style="margin-top:0px;"></div>
                                <table class="table table-bordered table-hover m-0"   >
                                <thead>
                                    <tr style="background-color: #dfe1e6;">
                                        <th style="text-align: center;">Serial No</th>
                                        <th style="text-align: center;">Item Code</th>
                                        <th style="text-align: center;">Item Name</th>
                                        <th style="text-align: center;">Recieved Quantity</th>
                                        <th style="text-align: center;">Approved Quantity</th>
                                        <th style="text-align: center;">Price Per Item</th>
                                        <th style="text-align: center;">Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?

                                for ($i=0; $i <$count2; $i++) { 
                                      //for
                                        $claimItemId = $fetchData2->rows[$i]['item_id'];
                                        $recievedQty = $fetchData2->rows[$i]['recieved_qty'];
                                        $claimDetailId = $fetchData2->rows[$i]['claim_detail_id'];
                                        $approvedQty = $fetchData2->rows[$i]['approved_qty'];
                                        $totalPrice = $fetchData2->rows[$i]['totalprice'];
                                        $pricePerItem = $fetchData2->rows[$i]['priceperitem'];
                                        $fetchData3=$viewClaims->FetchItemName($claimItemId);
                                        $claimItemName = $fetchData3->rows[0]['item_name'];
                                        $claimItemCode = $fetchData3->rows[0]['item_code'];

                                        $remarks = $fetchData2->rows[$i]['remarks'];

                                ?>
                                <tr>
                                <th scope="row" style="text-align: center;"><?php echo $i+1; ?></th>
                                <td>
                                <input type="text" class="form-control" id="" placeholder="" disabled="true"  value="<?php echo $claimItemCode; ?>" >
                                <input type="text" class="form-control" id="claimDetailId<?php echo $i ?>" name="claimDetailId<?php echo $i ?>" placeholder="" hidden="true"  value="<?php echo $claimDetailId; ?>" >
                                </td>
                                <td>
                                <input type="text" class="form-control"  placeholder="" disabled="true"  value="<?php echo $claimItemName ?>" >
                                </td>
                                <td >
                                <input type="text" style="text-align: center;" class="form-control"  id="recievedQty<?php echo $i ?>" placeholder="" disabled="true"  value="<?php echo $recievedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="Approved<?php echo $i ?>" disabled="true" name="Approved<?php echo $i ?>" placeholder=""  value="<?php echo $approvedQty ?>" >
                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="pricePerItem<?php echo $i ?>" onchange="countValues(<?php echo $i ?>)" name="pricePerItem<?php echo $i ?>" placeholder=""  disabled="true" value="<?php echo $pricePerItem ?>">

                                </td>
                                <td>
                                <input type="text" style="text-align: center;" class="form-control" id="totalPrice<?php echo $i ?>" readonly name="totalPrice<?php echo $i ?>" placeholder=""  value="<?php echo $totalPrice ?>" >
                                <!-- <input type="text" style="text-align: center;" class="form-control" id="totalPrice<?php echo $i ?>" name="totalPrice<?php echo $i ?>" hidden placeholder=""  value="<?php echo $totalPrice ?>" > -->
                                </td> 
                                <tr>        
                                <?php } ?>
                                <tr>
                                <th scope="row" style="text-align: center;"></th>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td >
                                </td>
                                <td>
                                </td>
                                <td align="center">
                            <h6 style="font-weight: bold; margin-top: 15px;">Discount</h6>
                                </td>
                                <td>
                                    <input type="text" style="text-align: center; " class="form-control" id="discount" name="discount" disabled="true"  placeholder="" onchange="calculate()" value="<?php echo $discount ?>">
                                </td> 
                                <tr>
                                   <tr>
                                <th scope="row" style="text-align: center;"></th>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td >
                                </td>
                                <td>
                                </td>
                                <td align="center">
                            <h5 style="font-weight: bold; margin-top: 15px;">Total Amount</h5>
                                </td>
                                <td>

                                    <input type="text" style="text-align: center; " class="form-control" id="grandSum" name="grandSum"   placeholder="" readonly="" value="<?php echo $grandTotal ?>">
                                </td> 
                                <tr>    
                                </tbody>
                                </table>

                                </div>
                            </div>

                        <?php } ?>



                    <?php 
                    if($claimStatus=='closed')
                    {
                        $fetchData3=$viewClaims->FetchVoucherDetail($claimID);
                        $count2 =  $fetchData3->count;
                    ?>            
                    <?php 
                    if ($count2>=1)
                    {
                    ?>      
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <h5 style="font-weight: bold;">Claim Voucher Information</h5>
                        </div>
                    </div>
                    <div class="form-row" style="">
                        <div class="col-md-6 mb-3">
                            <div style="margin-top:0px;"></div>
                            <table class="table table-bordered table-hover m-0"   style="text-align: center;">
                            <thead>
                                <tr style="background-color: #dfe1e6;">
                                    <th>Serial No</th>
                                    <th>Voucher Number</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php
                                    
                        for ($i=0; $i <$count2; $i++) { 
                                
                                $voucherId = $fetchData3->rows[$i]['voucher_id'];
                                $descriptionVoucher = $fetchData3->rows[$i]['description'];

                        ?>
                        <tr>
                        <th scope="row"><?php echo $i+1; ?></th>
                        <td>
                        <input type="text" class="form-control" style="border: 5px; background-color: white; text-align: center;" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $voucherId; ?>" >
                        </td>
                        <td>
                        <input type="text" class="form-control" style="border: 5px; background-color: white; text-align: center;" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $descriptionVoucher ?>" >
                        </td>    
                        <?php 
                        }
                        ?>
                            </tbody>
                            </table>
                        </div>
                    </div>

                    <?php           
                    } 
                    } 
                    ?>   
                                   
                   <!--  <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <h5>Images</h5>
                        </div>
                    </div> -->

                   <!--  <div class="form-row" align="center"> 
                        <div class="col-md-3  mb-3" >
                            <img src="\img\parts\nuts.jpg" class="image1">
                        </div>
                        <div class="col-md-3  mb-3">
                            <img src="\img\parts\nuts2.jpg"  class="image1">
                        </div>
                        <div class="col-md-3  mb-3" >
                            <img src="\img\parts\clutch.jpg" class="image1">
                        </div>
                        <div class="col-md-3  mb-3">
                            <img src="\img\parts\clutch2.jpg"  class="image1">
                        </div>
                    </div>     -->
                    <div class="form-row">
                        <div class="col-md-11  mb-3">
                        </div>
                        <div class="col-md-1  mb-3">
                            <?php 
                            if ($page==1) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('allclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                                
                            }
                            elseif ($page==2) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('incompleteclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }
                            elseif ($page==3) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('inprocessclaim')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }
                            elseif ($page==4) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('completedclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }
                            elseif ($page==5) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('pendingclaims')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }
                            elseif ($page==6) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('pendingclaimsunapproved')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }
                            elseif ($page==7) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('completedclaimsresolve')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }  
                            elseif ($page==9) {
                                ?>
                            <button type="button" onClick="LoadAjaxScreen('resolveclaim')" class="btn btn-med btn-dark">
                                <span class="fal fa fa-arrow-left mr-1"></span>
                                Back
                            </button>
                                <?php
                            }     
                            ?>
                            
                        </div>
                    </div>
                    <div style="margin-top: 20px;"></div>
                    
                    </div>
                        </div>
                    </div>
                </div>
            </div>

</main>

<nav class="shortcut-menu d-none d-sm-block">
    <input type="checkbox" class="menu-open" name="menu-open" id="menu_open" />
    <label for="menu_open" class="menu-open-button ">
        <span class="app-shortcut-icon d-block"></span>
    </label>
    <a href="#" class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Scroll Top">
        <i class="fal fa-arrow-up" style="margin-top: 15px;"></i>
    </a>
    <a href="logout.php"  class="menu-item btn" data-toggle="tooltip" data-placement="left" title="Logout">
        <i class="fal fa-sign-out" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-fullscreen" data-toggle="tooltip" data-placement="left" title="Full Screen">
        <i class="fal fa-expand" style="margin-top: 15px;"></i>
    </a>
    <a href="#" class="menu-item btn" data-action="app-print" data-toggle="tooltip" data-placement="left" title="Print page">
        <i class="fal fa-print" style="margin-top: 15px;"></i>
    </a>
</nav>

<script src="js/vendors.bundle.js"></script>
<script src="js/app.bundle.js"></script>





<!-- <div class="form-row" style="padding-left: 3px; padding-right: 3px;">
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Item Code</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" hidden="true"  value="<?php echo $claimItemId; ?>" >
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo ucfirst($claimItemCode) ?>" >
            </div>
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Item Name</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $claimItemName ?>" >
            </div>
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Recieved</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $recievedQty ?>" >
            </div>
            <?php
            if ($claimStatus=='inprocess' || $claimStatus=='completed') {
            ?>
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Approved Quantity</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $approvedQty ?>" >
            </div>
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Rejected Quantity</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $rejectedQty ?>" >
            </div>
            <div class="col-md-2 mb-3">
                <label class="form-label" for="validationTooltip01">Other Vendor</label>
                <input type="text" class="form-control" id="validationTooltip01" placeholder="" disabled="true"  value="<?php echo $otherQty ?>" >
            </div>
        <?php } ?>
</div> -->