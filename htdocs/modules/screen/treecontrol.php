
<?php
	require_once MODULE."/class/bllayer/user.php";
    $usertype = $mysession->getvalue('usertype');
    $userId = $mysession->getvalue('userid');
    $userName = $mysession->getvalue('username');

    //die(print_r($mysession));
?>
<style type="text/css">
    .nav-link-text {
        color: white;
    }
    .menu-item-parent{
        color: white;
    }
</style>

<aside class="page-sidebar" style="width: 300;background: #ff8000">
                    <div class="page-logo" style="background: #ff8000">
                        <a href="#" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
                            <h1 style="text-align: center; color: white; margin-top: 40px; margin-left: 20px;" >Admin</h1>
                            <span class="page-logo-text mr-1"><?php echo $userName ?></span>
                            <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
                           
                        </a>
                    </div>
                    <!-- BEGIN PRIMARY NAVIGATION -->
                    <nav id="js-primary-nav" class="primary-nav" role="navigation">
                        <div class="nav-filter">
                            <div class="position-relative">
                                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                                <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                                    <i class="fal fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div>
                           
                            
                        </div>
		<?php

        //die("her.......");
	
			if($usertype & USER_TYPE_ADMIN)
			{
                // echo USER_TYPE_ADMIN;

              
			?>
       
			<ul id="js-nav-menu" class="nav-menu" class="active gray">      <li class="open" >
                                <a  title="Dashboard " data-filter-tags="Dashboard">
                                    <i class="fa fa-dashboard" style="color: white"></i>
                                    <span class="nav-link-text" data-i18n="nav.application_intel">Dashboard</span>
                                </a>
                                 
                             </li>
						<li class="open" >
                                <a  title=" Booking" data-filter-tags="Dashboard">
                                    <i class="fa fa-lg fa-fw fa-group" style="color: white"></i>
                                    <span class="nav-link-text" data-i18n="nav.application_intel">Booking</span>
                                </a>
                                 <ul style="display: none;">
                                    <li>
                                        <a onclick="LoadAjaxScreen('create_booking')" title="Add Booking" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Add Booking</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="LoadAjaxScreen('view_booking')" title="View Booking" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">View Booking</span>
                                        </a>
                                    </li>
                                     
                                </ul>  
                             </li>
                            <li class="open">
                                <a  title="Users Management" data-filter-tags="Dashboard">
                                    <i class="fa fa-user" aria-hidden="true" style="color: white"></i>
                                    <span class="nav-link-text" data-i18n="nav.application_intel">Users Management</span>
                                </a>
                                 <ul style="display: none;">
                                    <li>
                                        <a onclick="LoadAjaxScreen('viewallusers')" title="All Users" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">All Users</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="#" title="Driver" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Driver</span>
                                        </a>
                                        <ul> 
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewusers&type=all')" title="All Drivers" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">All Drivers</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewusers&type=pending')" title="Pending" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Pending</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewusers&type=incomplete')" title="Incomplete" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Incomplete</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewusers&type=active')" title="Active" ><span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Active</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewusers&type=block')" title="Block" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Block</span>
                                                </a>
                                            </li>
                                            <li>
                                                 <a onclick="LoadAjaxScreen('deleteddriver')" title="Deleted" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Deleted</span>
                                                </a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                     <li>
                                        
                                        <li>
                                        <a onclick="#" title="Customer" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Customer</span>
                                        </a>
                                        <ul> 
                                            <li>
                                                <a onclick="LoadAjaxScreen('pendingclaimsunapproved&type=all')" title="All Customer" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">All Customer</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('pendingclaimsunapproved&type=pending')" title="Pending" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Pending</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('pendingclaimsunapproved&type=incomplete')" title="Incomplete" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Incomplete</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('pendingclaimsunapproved&type=active')" title="Active" ><span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Active</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('pendingclaimsunapproved&type=block')" title="Block" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Block</span>
                                                </a>
                                            </li>
                                            <li>
                                                 <a onclick="LoadAjaxScreen('deletedcustomer')" title="Deleted" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Deleted</span>
                                                </a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                             </li>
                             <li>
                                        
                                        <li>
                                        <a onclick="#" title="Agent" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Agent</span>
                                        </a>
                                        <ul> 
                                            <li>
                                                <a onclick="LoadAjaxScreen('addagent')" title="Add Agent" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Add Agent</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('viewagent')" title="View Agent" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">View Agent</span>
                                                </a>
                                            </li>
                                            
                                    
                                            
                                            
                                        </ul>
                                    </li>
                             </li>
                             <li>
                                        
                                        <!-- <li>
                                        <a onclick="#" title="Vehicle" >
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Vehicle</span>
                                        </a>
                                        <ul> 
                                            <li>
                                                <a onclick="LoadAjaxScreen('addvehicle')" title="Add Agent" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Add Vehicle</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a onclick="LoadAjaxScreen('addvehcilemodel')" title="Add Vehicle Model" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">Add Vehicle Model </span>
                                                </a>
                                            </li>
                                            
                                    
                                            
                                            
                                        </ul>
                                    </li>
                             </li> -->
                         </ul>
                             
                             <li class="open">
                                <a href="#" title="Domain Location" > <i class="fa fa-lg fa-fw fa-map-marker" style="color: white"></i><span class="nav-link-text" data-i18n="nav.application_intel">Domain Location</span></a>
                                <ul style="display: none;">
                                    
                                    <li>
                                        <a href="#" title="City" onclick="LoadAjaxScreen('addcity')"> <span class="menu-item-parent">City</span> </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Routes" onclick="LoadAjaxScreen('addroutes')"> <span class="menu-item-parent">Routes</span> </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Approve City Request" onclick="LoadAjaxScreen('approve')"> <span class="menu-item-parent">Approve City Request</span>  </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="open">
                                    <a href="#" title="Payments" ><i class="fa fa-lg fa-fw fa-money" style="color: white"></i><span class="nav-link-text" data-i18n="nav.application_intel">Payments</span></a>
                                    <ul style="display: none;">
                                        <li>
                                            <a href="#" title="Add Payment" onclick="LoadAjaxScreen('addpayment')"> <span class="menu-item-parent">Add Payment</span></a>

                                        </li>
                                        <li>
                                            <a href="#" title="Approve" onclick="LoadAjaxScreen('paymentapprove')"> <span class="menu-item-parent">Approve</span></a>

                                        </li>
                                        <li>
                                            <a href="#" title="Pending" onclick="LoadAjaxScreen('paymentpending')"> <span class="menu-item-parent">Pending</span></a>

                                        </li>
                                        <li>
                                            <a href="#" title="Unlink" onclick="LoadAjaxScreen('paymentunlink')"> <span class="menu-item-parent">Unlink</span></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Conflict" onclick="LoadAjaxScreen('paymentconflict')"> <span class="menu-item-parent">Conflict</span></a>
                                        </li>
                                    </ul>
                                </li>
                            <li class="open">
                                <a href="#" title="Reports" data-filter-tags="Reports">
                                    <i class="fal fa-chart-pie" style="color: white"></i>
                                    <span class="nav-link-text" data-i18n="nav.package_info">Reports</span>
                                </a>
                                <ul style="display: none;">
                                    <li>
                                    <a href="#" title="Payment" ><span class="nav-link-text" data-i18n="nav.package_info">Payment</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" title="Driver Wise" onclick="LoadAjaxScreen('viewdriverreport')"> <span class="menu-item-parent" data-i18n="nav.reportpayment"></span>Driver Wise </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Customer Wise" onclick="LoadAjaxScreen('viewcustomerreport')"> <span class="menu-item-parent" data-i18n="nav.reportpayment"></span>Customer Wise </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" title="Ride" ><span class="nav-link-text" data-i18n="nav.package_info">Ride</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" title="Driver Wise" onclick="LoadAjaxScreen('showUserPipeLine')"> <span class="menu-item-parent"></span>Driver Wise </a>
                                        </li>
                                        <li>
                                            <a href="#" title="Customer Wise" onclick="LoadAjaxScreen('showUserPipeLine')"> <span class="menu-item-parent"></span>Customer Wise </a>
                                        </li>
                                    </ul>
                                 </li>
                                 <li>
                                                <a onclick="LoadAjaxScreen('agentreport')" title="Report Agent" >
                                                    <span class="nav-link-text" data-i18n="nav.theme_settings_layout_options">  Report Agent</span>
                                                </a>
                                            </li>
                            </ul>
                        </li>
                            
                        <li>
                            <a href="#" title="Setting"><i class="fa fa-lg fa-fw fa fa-wrench" style="color: white"></i> <span class="nav-link-text" data-i18n="nav.package_info">Setting</span></a>
                            <ul style="display: none;">
                                <li>
                                    <a href="#" title="Configration"  onclick="LoadAjaxScreen('settings')"><span class="menu-item-parent">Configuration</span></a>
                                </li>
                                
                                <li>
                                    <a href="#" title="ChangePassword" onclick="LoadAjaxScreen('resetpassword')"> <span class="menu-item-parent">Change Password</span></a>

                                </li>
                            </ul>
                        </li>
                        <li>
                   <li>         
					 <a  title="" data-filter-tags="Addvehicle">
						<i class="fa fa-lg fa-fw fa-map-marker"></i>
						<span class="nav-link-text" data-i18n="nav.wapsicar">Vehicle</span>
					 </a>
					 <ul>
						<li>
							<a href="#" title="" onclick="LoadAjaxScreen('viewvehicle')" data-filter-tags="wapsicar">
								<span class="nav-link-text" data-i18n="nav.wapsicar">View Vehicle details</span>
							</a>
						</li>

						<li>
							<a href="#" title="" onclick="LoadAjaxScreen('addvehicle')" data-filter-tags="wapsicar">
								<span class="nav-link-text" data-i18n="nav.wapsicar">Add Make</span>
							</a>
						</li>
                        <li>
							<a href="#" title="" onclick="LoadAjaxScreen('addvehiclemodel')" data-filter-tags="wapsicar">
								<span class="nav-link-text" data-i18n="nav.wapsicar">Add Model</span>
							</a>
						</li>
                     </ul>
                   
						
				</li>
            </ul>
                 
			<?php
           
            }
           
            ?>
            </nav>
</aside>
<style>
        .nav-link {
            color: green;
        }
  
        .nav-item>a:hover {
            color: green;
        }
  
        /*code to change background color*/
        .navbar-nav>.active>a {
            background-color: #C0C0C0;
            color: green;
        }
    </style>
    <script type="text/javascript">
    $("a").click(function(){
        $("a").css("backgroundColor", "");
        $(this).css("backgroundColor", "black");
        $(this).css("color", "#fff");
    });


</script>

