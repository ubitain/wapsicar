<?php
	define("ENABLE_TRACE", "1"); // 1-enable,0-disable
	define("MAX_TRACE_FILE_SIZE", "100"); // 100MB fater tat it will refresh
	define("ENABLE_QUERY_BACKUP", "1"); // if 1 all insert/update and delete query will be logged
	define("SUPPORT_EMAIL", "info@alpha-trend.com");
	define("EMAIL_POOL", "");
	define("EMAIL_POOL_PASSWORD", "");

	define('GENERATE_CLOAKING_CONSTANTS', TRUE);
	define("ACTION_TIME_LIMIT", 3600);
	define("DEFAULT_TIME_LIMIT", 30);
	define("SESSION_TYPE_WEB_GENERAL", 1);
	define("SESSION_TYPE_WEB_REGISTARTION", 2);
	define("SESSION_TYPE_WEB_INVITATION", 3);
	define("SESSION_TYPE_WEB_VERIFICATION", 4);
	define("SESSION_TYPE_MOBILE", 5);
	define("SESSION_TYPE_WEB_EMAIL_LINK", 40);
	define("PAGE_LIMIT", 10);
	define("CLASSIFIED_MSG_MAX_LEN", 400);
	define("CLASSIFIED_SUB_MAX_LEN", 100);
	define("MAX_COLUMN_IN_ROW", 1);
	define("UPLOAD_CV_SIZE", 5*1024*1024);
	define("UPLOAD_IMG_SIZE", 5*1024*1024);

	define("SESSION_TIMEOUT",1*60000); // 5*36000 used in session.php
	define("LINK_EXPIRE_TIME",2592000); // 30 days  used in session.php

	define("USER_TYPE_ADMIN", 1);
	define("USER_TYPE_DRIVER", 2);
	define("USER_TYPE_CUSTOMER", 4);
	

	define("APP_VERSION","1.0.0");

	define("DISTANCE_DIFFERENCE","50");
	define("LEAD_DISTANCE_DIFFERENCE","100");


	//setting todays visits and calls
	define("TODAYS_CALLS_TARGETS","10");
	define("TODAYS_VISITS_TARGETS","3");
	define("WORKING_DAYS_MONTH","25");


	//Notification constant
	define("NOTIFICATION_CRM_LEAD","1");
	define("NOTIFICATION_GENERAL","2");
	define("NOTIFICATION_REDEMPTION","3");

	// LOGIN NUM_OF_LOGIN_ATTEMPTS
	define('NUM_OF_LOGIN_ATTEMPTS', '3');
	define('NUM_OF_LOGIN_ATTEMPTS_ALLOWED', '15');
	// BLOCKED_TIME
	define('BLOCKED_TIME', '15');

	// USER STATUS
	define('INACTIVE_USER', '-1');
	define('ACTIVE_USER', '1');
	define('PENDING_ADD', '2');
	define('PENDING_INACTIVE', '3');
	
	// PASSWORD
	define('MAX_ACCEPT_TIME',10); //in minutes
	define('PASSWORD_EXPIRE_DAYS',300);

	global $paymentStatus;
	$paymentStatus = array();
    $paymentStatus["2"] = "Pending";
    $paymentStatus["3"] = "Conflict";
    $paymentStatus["1"] = "Approved";
	
?>
