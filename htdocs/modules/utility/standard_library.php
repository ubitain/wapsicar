<?php
require_once(MODULE . "/class/lib/phpmailer/class.phpmailer.php");
require_once(MODULE . "/utility/emails.php");

	global $errorMessages ;
	$errorMessages =array();
	global $successMessages;
	$successMessages=array();
	global $logMessages;
	$logMessages = array();

	function spongLog()
	{
		return true;
	}
	function username_style($fName,$lName,$empCode)
	{
    	$empCode = ($empCode=="")?"( NA )":"( ".$empCode." )";
		return ucwords($fName." ".$lName)." ".$empCode;
	}
	function user_type_decoder($value='')
	{
		switch ($value) {
			case USER_TYPE_ADMIN:
				echo "Sales Admin";
				break;
			case USER_TYPE_NSM:
				echo "National Sales Manager";
				break;
			case USER_TYPE_RSM:
				echo "Regional Sales Manager";
				break;
			case USER_TYPE_ASM:
				echo "Area Sales Manager";
				break;
			case USER_TYPE_TEAM_LEAD:
				echo "Team Leader";
				break;
			case USER_TYPE_BDO:
				echo "Business Development Officer";
				break;
			default:
				// do nothing...
				break;
		}
	}
	function traceMessage($str="")
	{
		//echo LOGFILE_DIRECTORY_PATH;
		$filename = LOGFILE_DIRECTORY_PATH."logs.txt";
		//echo $filename;
		$today = date("D, M j/y G:i:s") . " " . microtime();
		error_log($today .":".$str."\n",3,$filename);
		_rotate($filename);

	}
	function LogQuery($sql)
	{
		$filename = LOGFILE_DIRECTORY_PATH."/query.txt";
		$today = date("D, M j/y G:i:s") . " " . microtime();
		error_log($today .":".$sql."\n",3,$filename);
		_rotate($filename);
	}
	function _rotate($filename)
	{
		$length = filesize ( $filename );
		$maxSize = MAX_TRACE_FILE_SIZE*1024*1024;//2mb
		if ( file_exists ( $filename )&&  $length >= $maxSize )
		{
			unlink($filename);
		}

		if ( !touch ( $filename ) )
		{
			die("Unable to create file: ".$filename);
		}

	}
	function sLog($msg,$type="debug")
	{
	}
	function LogMsg($desc,$type="debug",$code="0")
	{
	}
	function ReportError($errorObject)
	{
		global $errorMessages;
		global $logMessages;
		// Log error object
		$desc = $errorObject->desc;
		$type = strtolower($errorObject->type);
		$code = $errorObject->code;
		$filename = $errorObject->filename;
		$func = $errorObject->func;
		$classname = $errorObject->classname;
		$line = $errorObject->line;
		if ($classname)
			$func = strtoupper($classname)."::".$func;

		if (strtoupper(trim($desc)) == "START" || strtoupper(trim($desc)) == "END")
		{
			$desc = strtoupper($desc);
			$info = "[".$filename.":".$line."] \r\n****** $desc of $func ******";
		}
		else
			$info = "[".$filename.":".$line."] \r\n<$func> - $desc";

		if ($type=="error")
		{
		}
		if ($type=="success")
		{
			traceMessage("case success");
		}
		else if ($type=="critical" || $type=="silent")
		{

		}

		sLog($info,$type);

		// Push object in $errorMessages array
		$errorObjectArr = object_to_array($errorObject);

		array_push($errorMessages,$errorObject);
	}

	// Funcion de Objeto a Array
function object_to_array($object)
{
  if(is_array($object) || is_object($object))
  {
    $array = array();
    foreach($object as $key => $value)
    {
      $array[$key] = object_to_array($value);
    }
    return $array;
  }
  return $object;
}

// Funcion de Array a Objeto
function array_to_object($array = array())
{
	return (object) $array;
}

	function print_r_pre($var,$heading="")
	{
		if ($heading!="")
			echo "<br><b>$heading:</b>";

		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
	function print_r_log($var)
	{
		ob_start();
		print_r($var);
		$ret_str = ob_get_contents();
		ob_end_clean();

		return $ret_str;
	}
	function Authenticate($user,$pass,$enc="no")
	{
		require_once(MODULE . "/class/bllayer/user.php");

		global $mysession;
		$mysession = new session();

		$mynewsession = new session();
		$mynewsession->startsession($user,"","");

		$blUser =  new BL_User();
		$authInfo = $blUser->AuhenticateUser($user,$pass,$enc);

		if(!$authInfo)
		{
			$ret["code"]="-1";
			$ret["desc"]=NOT_AUTHENTICATED;
			$ret["sid"]=-1;
		}
		else
		{
			$userInfo = $blUser->GetUserInfo($authInfo["user_id"]);
			
			$ret["code"]="0";
			$ret["desc"]="login successfully";
			$ret["sid"]=$mynewsession->sessionid;
			$ret["user_id"]=$userInfo->rows[0]['user_id'];

			
		}
		$mysession=$mynewsession;

		//die(print_r($authInfo["user_id"]));
		MakeSession($mynewsession,$userInfo);
		//print_r_pre($userInfo);
		//die('aa');	
		return $ret;
	}
	function MakeSession($mynewsession,$userInfo)
	{
		$info = $userInfo->rows[0];
		$mynewsession->setvalue("regionid",$info["region"]);
		$mynewsession->setvalue("usertype",$info["user_type"]);
		$mynewsession->setvalue("userid",$info["user_id"]);
		$mynewsession->setvalue("loginid",$info["login_id"]);
		$mynewsession->setvalue("department_id",$info["department_id"]);
		$mynewsession->setvalue("username",$info["first_name"]." ".$info["middle_name"]." ".$info["last_name"]);
		$mynewsession->setvalue("shopowner",$info["own_shop"]);

	}
	function CheckCondition($condition, $errorObject)
	{
		if ($condition)
		{
			ReportError($errorObject);
			return true;
		}
		else
		{
			return false;
		}
	}
	function trimstring($str,$num,$type="forward")
	{
		if (strlen($str)<=$num) return $str;
		if($type=="forward")
		{
			$num = $num-3;
			$str = substr($str,0,$num);
			$str = str_pad($str, $num+3, ".", STR_PAD_RIGHT);
		}
		else if($type=="reverse")
		{
			$num = $num-3;
			$str=substr($str,-$num);
			$str = str_pad($str,$num+3, ".", STR_PAD_LEFT);
		}
		return $str;
	}
	function get_rand_letters($length)
	{
		$allchars = 'abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ';
		$string = '';
		mt_srand ((double) microtime() * 1000000);

		for ($i = 0; $i < $length; $i++)
			$string .= $allchars{mt_rand(0,strlen($allchars)-1)};


		return $string;
	}
	function get_rand_numbers($length)
	{
		$allchars = '0123456789';
		$string = '';
		mt_srand ((double) microtime() * 1000000);

		for ($i = 0; $i < $length; $i++)
			$string .= $allchars{mt_rand(0,strlen($allchars)-1)};


		return $string;
	}
	function CheckUserType($usertype,$iotype)
	{
		$iotype = (int)$iotype;
		$usertype = (int)$usertype;
		return ($iotype & $usertype);
	}
	function SendMailFromSupport($body,$mailTo,$subject="",$attachments="",$attName="")
	{
		//return;
		$mail = new PHPMailer();

		$mail->From = SUPPORT_EMAIL;
		$mail->FromName = SITE_HEADING;
		$mail->Subject = $subject;

		$mail->MsgHTML($body);
		$mail->AddAddress($mailTo, "");
		$mail->AddBCC(EMAIL_POOL);

		if($attachments!='')
			$mail->AddAttachment($attachments,$attName); // attachment

		$mail->IsHTML(true);

		traceMessage(print_r_log($mail));
		/*
		if(!$mail->Send())
		{
		  echo "Mailer Error: " . $mail->ErrorInfo;
		}
		else
		{
		  echo "Message sent!";
		}*/
		//print_r_pre($mail);
		return true;
	}
	function SendMail($fromName,$from,$body,$mailTo,$subject)
	{
		traceMessage("$fromName,$from,$mailTo,$subject");
		//return;
		traceMessage(print_r_log($body)."asdasdqweqweqwefdsfdsf");

		$mail = new PHPMailer();
		//traceMessage(print_r_log("asdasdasdasdqweqweqwe"));
		$mail->From = $from;
		$mail->FromName = $fromName;
		$mail->Subject = $subject;

		$mail->MsgHTML($body);
		$mail->AddAddress($mailTo, "");
		$mail->AddBCC(EMAIL_POOL);

		if($attachments!='')
			$mail->AddAttachment($attachments,$attName); // attachment

		$mail->IsHTML(true);
		traceMessage(print_r_log($mail)."DAD");
		// traceMessage(print_r_log("wwww")."DAD".print_r_log($mail->ErrorInfo));
		if(!$mail->Send())
		{
			traceMessage(print_r_log("wwww")."DAD".$mail->ErrorInfo);
		  	echo "Mailer Error: " . $mail->ErrorInfo;
		}
		else
		{
			traceMessage(print_r_log("mmmm")."DAD");
		  	echo "Message sent!";
		}
		traceMessage("DONE!!");
		print_r_pre($mail);
		return true;
	}

	function InsertFSLog($fileName,$filePath,$type)
	{
		require_once(MODULE . "/class/bllayer/user.php");
		$bluser = new BL_User();
		traceMessage("InsertFSLog::::::$fileName,$filePath,$type");
		$bluser->InsertFSLog($fileName,$filePath,$type);
	}
	function getElapsedTime($eventTime)
	{
	    $totaldelay = time() - strtotime($eventTime);
	    if($totaldelay <= 0)
	    {
	        return '';
	    }
	    else
	    {
	        if($days=floor($totaldelay/86400))
	        {
	            $totaldelay = $totaldelay % 86400;
	            return $days.' day(s) ago.';
	        }
	        if($hours=floor($totaldelay/3600))
	        {
	            $totaldelay = $totaldelay % 3600;
	            return $hours.' hour(s) ago.';
	        }
	        if($minutes=floor($totaldelay/60))
	        {
	            $totaldelay = $totaldelay % 60;
	            return $minutes.' minute(s) ago.';
	        }
	        if($seconds=floor($totaldelay/1))
	        {
	            $totaldelay = $totaldelay % 1;
	            return $seconds.' second(s) ago.';
	        }
	    }
	}
	function getLatLong($address)
	{
		if (!is_string($address))
	    {
	    	traceMessage("All Addresses must be passed as a string");
	    	return false;
	    }
	    $_url = sprintf('http://maps.google.com/maps?output=js&q=%s',rawurlencode($address));

	    $_result = false;
	    if($_result = file_get_contents($_url)) {
	        if(strpos($_result,'errortips') > 1 || strpos($_result,'Did you mean:') !== false) return false;
	        preg_match('!center:\s*{lat:\s*(-?\d+\.\d+),lng:\s*(-?\d+\.\d+)}!U', $_result, $_match);
	        $_coords['lat'] = $_match[1];
	        $_coords['long'] = $_match[2];
	    }
	    return $_coords;
	}
	function getLatitudeLongitude($address)
	{
		if (!is_string($address))
	    {
	    	traceMessage("All Addresses must be passed as a string");
	    	return false;
	    }
		$address=urlencode($address);
	    $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyCcbWlKpbS5BOqrktc7iW0CCgKrQkvG8zc&address='.$address.'&sensor=false');
		// traceMessage('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
		$output= json_decode($geocode);
		// traceMessage("Output Info ".print_r_log($output));
		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;
		$locationArray['lat']="$lat";
		$locationArray['long']="$long";
		return $locationArray;
	}
	function distance($lat1, $lon1, $lat2, $lon2, $unit)
	{
	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
		return ($miles * 1.609344);
	  } else if ($unit == "N") {
		  return ($miles * 0.8684);
		} else {
			return $miles;
		  }
	}
	function add3dots($string, $repl, $limit)
	{
		if(strlen($string) > $limit)
		{
			return substr($string, 0, $limit);
		}
		else
		{
			return $string;
		}
	}
	function ShowMessages()
	{
		global $errorMessages;
		global $successMessages;

		if (count($successMessages)>0)
		{

			?>
			<script>
			var msg = "";
			msg  = "______________________________________________________\n\n";
			<?php for ($c=0;$c<count($successMessages);$c++) { ?>
			msg += "- <?php echo $successMessages[$c]?>\n" ;
			<?php } ?>
			msg += "______________________________________________________\n\n";
			<!--alert(msg);-->
			<?php
			echo "</script>";
		}
		else if (count($errorMessages)>0)
		{
			$arr = GetErrorMessages();
			//traceMessage(print_r_log($arr));
			$criticalMessageList = $arr["critical"];
			$errorMessageList = $arr["error"];
			$warningMessageList = $arr["warning"];
			$noticeMessageList = $arr["notice"];
			if (count($criticalMessageList)>0)
			{
				echo "<script>";
				echo 'alert("This service / resource is temporarily unavailable, Please try again after reloading (Refresh) your screen. \n\nIf problem persist, Please contact '.SUPPORT_EMAIL.'");';
				echo "</script>";
			}
			else if (count($noticeMessageList)>0)
			{
				// JsErrorMessage($noticeMessageList);
			}
			else if (count($errorMessageList)>0)
			{
				//JsErrorMessage($errorMessageList);
				echo "<script>";
				echo 'alert("This service / resource is temporarily unavailable, Please try again after reloading (Refresh) your screen. \n\nIf problem persist, Please contact '.SUPPORT_EMAIL.'");';
				echo "</script>";
			}
			traceMessage($criticalMessageList);
			traceMessage($errorMessageList);
			traceMessage($warningMessageList);
			traceMessage($noticeMessageList);
			/*
			else if (count($warningMessageList)>0)
				JsErrorMessage($warningMessageList);
			*/
		}

	}
	function GetErrorMessages($preserve=0)
	{
		global $errorMessages;

		$criticalMessageList = array();
		$errorMessageList = array();
		$warningMessageList = array();
		$noticeMessageList = array();
		$debugMessageList = array();
		$successMessageList =array();
		$silentMessageList =array();

		$x = 0;
		while(isset($errorMessages[$x]))
		{
			if ($preserve)
				$e = $errorMessages[$x++];
			else
				$e = array_pop($errorMessages);

			if ($e->type == "notice")
			{
				$noticeMessageList[] = $e;
			}
			else if ($e->type == "warning")
			{
				$warningMessageList[] = $e;
			}
			else if ($e->type == "debug")
			{
				$debugMessageList[] = $e;
			}
			else if ($e->type == "error")
			{
				$errorMessageList[] = $e;
			}
			else if ($e->type == "critical")
			{
				$criticalMessageList[] = $e;
			}
			else if ($e->type == "success")
			{
				$successMessageList[] = $e;
			}
			else if ($e->type == "silent")
			{
				$silentMessageList[] = $e;
			}
		}

		return array("notice"=>$noticeMessageList,
						"warning"=>$warningMessageList,
						"debug"=>$debugMessageList,
						"error"=>$errorMessageList,
						"success"=>$successMessageList,
						"silent"=>$silentMessageList,
						"critical"=>$criticalMessageList);
	}
	function DistanceDifference($lat1, $lon1, $lat2, $lon2, $unit)
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K")
		{
			return round(($miles * 1.609344),4);
		}
		else if ($unit == "N")
		{
			return round(($miles * 0.8684),4);
		}
		else if ($unit == "M")
		{
			$distanceMeter=$miles * 1.609344;
			return round(($distanceMeter * 1000),4);
		}
		else
		{
			return $miles;
		}
	}
	//////////////////////////////////////////////////////////////////////
	//PARA: Date Should In YYYY-MM-DD Format
	//RESULT FORMAT:
	// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'      =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
	// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
	// '%m Month %d Day'                                            =>  3 Month 14 Day
	// '%d Day %h Hours'                                            =>  14 Day 11 Hours
	// '%d Day'                                                     =>  14 Days
	// '%h Hours %i Minute %s Seconds'                              =>  11 Hours 49 Minute 36 Seconds
	// '%i Minute %s Seconds'                                       =>  49 Minute 36 Seconds
	// '%h Hours                                                    =>  11 Hours
	// '%a Days                                                     =>  468 Days
	//////////////////////////////////////////////////////////////////////
	function datetimeDifference($date_1 , $date_2 , $differenceFormat = '%h' )
	{
	    $datetime1 = date_create($date_1);
	    $datetime2 = date_create($date_2);

	    $interval = date_diff($datetime1, $datetime2);

	    return $interval->format($differenceFormat);

	}

	//multidimensional array unique by array key
	function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
	    foreach($array as $val) {
	        if (!in_array($val[$key], $key_array)) {
	            $key_array[$i] = $val[$key];
	            $temp_array[$i] = $val;
	        }
	        $i++;
	    }
	    return $temp_array;
	}
	// fcm notification
	function fcm_send_notification_del($data)
    {
		// return true;
        require_once(MODULE . "/class/bllayer/user.php");
        $bluser = new BL_User();
        traceMessage("Insert Notification: ".print_r_log($data['db']));
        $notificationId = $bluser->InsertNotification($data);

        #API access key from Google API's Console
        define('API_ACCESS_KEY', 'AAAAY6pfdU8:APA91bEj4obH4bLkCOqWToKZNzndffQfiq9yTxB-Dx91JGk66Y8ALTdZTAePxZI5cctnZGulZoniM-__xed0CEJ9x2iO2gJdMkqVjXQ8A1rWr7Ps0vq6eG98xnVgvLK6svEnG_Zx4zOR');
        $registrationIds = $data['id'];
        #prep the bundle
        $msg = $data['msg'];
        $fields = array(
            'to'		=> $registrationIds,
            'data'	=> $msg
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        traceMessage("Firebase Server");
        // $rustart = getrusage();
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        /*
		$ru = getrusage();
        traceMessage("This process used " . rutime($ru, $rustart, "utime") ." ms for its computations\n");
        traceMessage("It spent " . rutime($ru, $rustart, "stime") ." ms in system calls\n");
        */
        traceMessage("***************************\n");
        $result_arr = json_decode($result, true);
        traceMessage("result".print_r_log($result_arr));
        if ($result_arr['success'] === 1) {
            $sentStatus = "success";
        } elseif ($result_arr['failure'] === 1) {
            $sentStatus = "failure";
        }
        $param = array(
            'id' => $notificationId,
            'sent_status' => $sentStatus
        );
        traceMessage("Update Notification: ".print_r_log($param));
        //$bluser->UpdateNotification($param);
        traceMessage("Notification return");
        #Echo Result Of FireBase Server
        return $param;
    }

    if(!function_exists(mysql_escape_string)){


    	function mysql_escape_string($str)
    	{
    		return $str;
    	}
    }
	
	
	
	function SendPushNotificationsIOS($fcmId,$body,$title ,$objId,$type)
	{
		traceMessage("SendAndroidPushNotification--$fcmId,$body,$title");
		define( 'API_ACCESS_KEY', 'AAAAZKMm0OM:APA91bHZ7YxB2qf13F1Wu8uEptM2dI3L7hNlc3M2DeuSMLbU1mlKL9m4phcJzeROBeGmbQCYN34jFrpwEy7GnBjX-gVlD_EcXtmDnfHeSdf4VnfTSpZ1nnSvv0zg-JNQk9dOdLRw5i8w');
		
		
		
		
		
		
		$registrationIds = "$fcmId";
		
		 
		 $data=array(
			'objid' =>$objId,
			'notification_type' =>$type,
			'icon' =>'myicon',
			'sound' =>'mySound',
			'body' 	=> $body,
			'title'	=> $title
			
		 );
		 
		 $not=array(
			'icon' =>'myicon',
			'sound' =>'default',
			'body' 	=> $body,
			'title'	=> $title
			
		 );
		 
		// print_r_pre($data,"aa");
		
		$and=array(
			'priority'=>'high'
		 );
		
		
			$fields = array
			(
				'to'		=> $registrationIds,
				//'notification'		=> $notification,
				'data'		=> $data,
				'android'		=> $and,
				'notification'		=> $not,
				
			);
			//print_r_pre($fields);
		$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
//traceMessage("fileds");
traceMessage(print_r_log($fields));
//traceMessage("json");
//traceMessage(print_r_log(json_encode( $fields )));
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		traceMessage("Res:".print_r_log($result));
		
		$resArr = json_decode($result);
	//	print_r_pre($resArr->success,"res Arr");
	//echo "<pre>";
	//print_r($fields);
	//	print_r($result);
		/*
		if($resArr->success==1)
			return "yes";
		else	
			return "no";*/
		
		return $resArr;
	}
function SendPushNotificationsAndroid($fcmId,$body,$title ,$objId,$type,$userId,$source)
	{
		
		traceMessage("SendAndroidPushNotification--$fcmId,$body,$title");
		/*old
		if($source=="customer") //notification will be sent to driver app
			define( 'API_ACCESS_KEY1', 'AAAACiq3JjI:APA91bH0n7zQ4YbgdmSsc1wurDmTA2EuMLb0tDZSYmwykBq-FO4mdFsHbyV5HuEn0rApT0t7lNGXnIylpzsXCvy8ZILB9YsDikcgejc34a9EOYcxhnwim-yU3xcHwl6cYNtGKuIykEVq');
		else
			define( 'API_ACCESS_KEY1', 'AAAAeXO9aCA:APA91bF2_XPIlpupOWFxSISzG4YslAMwFajxnViaCTsDGnVm6wBVPE7jgkl1c8vJbgnef-SJ0vH_NLZZwaaI8kp3EpoNJZZhKPIsLK0R3JLvex8-djUj6-2zLNA2xCwHydSeZHks9gWg');
		
		*/
	
	/*
		if($source!="customer") //notification will be sent to driver app
			$API_ACCESS_KEY1 =  'AAAAeXO9aCA:APA91bF2_XPIlpupOWFxSISzG4YslAMwFajxnViaCTsDGnVm6wBVPE7jgkl1c8vJbgnef-SJ0vH_NLZZwaaI8kp3EpoNJZZhKPIsLK0R3JLvex8-djUj6-2zLNA2xCwHydSeZHks9gWg';
		else
			$API_ACCESS_KEY1 = 'AAAACiq3JjI:APA91bH0n7zQ4YbgdmSsc1wurDmTA2EuMLb0tDZSYmwykBq-FO4mdFsHbyV5HuEn0rApT0t7lNGXnIylpzsXCvy8ZILB9YsDikcgejc34a9EOYcxhnwim-yU3xcHwl6cYNtGKuIykEVq';
		*/
		if($source!="customer") //notification will be sent to driver app
				$API_ACCESS_KEY1 = 'AAAAH61QAic:APA91bEtpLYsqZ418Z0-ebcbGrKRGCFK_mIzj3SmdnzRnw_RfrY5jsPtnDUiZbnOa4mS9149TKNrjwcIfpLrWkzUKctw-RY6NRQr1T5Pf9s-aPeDmin1dv1n6n5BhBHDkoRp91AYqwSP';
		else
				$API_ACCESS_KEY1 =  'AAAAl_dsXNs:APA91bEpWhfJng08eRq2AO1KmFR6KLbGX7hfqQ27zgvxEN8GPS7vDlzXPUMw5CBso47hunnVkB6wbK2fCSdEF_LjmzY_mpEIv2mB2zNTA0k3Am2-CVavfFo9FPk7Bug_mPN5-lDIesd_';
	
		
		/*if($source!="customer") //notification will be sent to driver app
			$API_ACCESS_KEY1 =  'AAAAhqyIjV4:APA91bGPmdP16nYg9LmnepHHJqLtrFBD_rOAiIIM4iWKgumjyHM2B5x1KaFuFekXmmOf2akALRXh51_zV5XnrVwYSCcZyPfZfCZrGmS5S-cExgmPs8elT_OXAf_PHFTTBq2Q50lrR21N';
		else
			$API_ACCESS_KEY1 = 'AAAA_eggvJg:APA91bHknvQKJvnn6JMu4mS1yCuOuJhNlWEep4zwOlt9rEvFMgA54crXbBwAO9jjdWa_sXQn4fF46xD_y6C3EF2tCxRQkT2tSWOb7UBQRtyCJJTNP6y8ComJsp_IWxzQWGRQKjU_oQnu';
		
		
		if($source!="customer") //notification will be sent to driver app
				$API_ACCESS_KEY1 = 'AAAA_eggvJg:APA91bHknvQKJvnn6JMu4mS1yCuOuJhNlWEep4zwOlt9rEvFMgA54crXbBwAO9jjdWa_sXQn4fF46xD_y6C3EF2tCxRQkT2tSWOb7UBQRtyCJJTNP6y8ComJsp_IWxzQWGRQKjU_oQnu';
		else
			$API_ACCESS_KEY1 =  'AAAAhqyIjV4:APA91bGPmdP16nYg9LmnepHHJqLtrFBD_rOAiIIM4iWKgumjyHM2B5x1KaFuFekXmmOf2akALRXh51_zV5XnrVwYSCcZyPfZfCZrGmS5S-cExgmPs8elT_OXAf_PHFTTBq2Q50lrR21N';
		*/
		
		
		if($source!="customer") //notification will be sent to driver app
				$API_ACCESS_KEY1 = 'AAAAH61QAic:APA91bEtpLYsqZ418Z0-ebcbGrKRGCFK_mIzj3SmdnzRnw_RfrY5jsPtnDUiZbnOa4mS9149TKNrjwcIfpLrWkzUKctw-RY6NRQr1T5Pf9s-aPeDmin1dv1n6n5BhBHDkoRp91AYqwSP';
		else
				$API_ACCESS_KEY1 =  'AAAAl_dsXNs:APA91bEpWhfJng08eRq2AO1KmFR6KLbGX7hfqQ27zgvxEN8GPS7vDlzXPUMw5CBso47hunnVkB6wbK2fCSdEF_LjmzY_mpEIv2mB2zNTA0k3Am2-CVavfFo9FPk7Bug_mPN5-lDIesd_';
	
		$registrationIds = "$fcmId";
		
		  $not=array(
			'icon' =>'myicon',
			'sound' =>'default',
			'body' 	=> $body,
			'title'	=> $title
			
		 );
		 
		 $data=array(
			'objid' =>$objId,
			'not_type' =>$type,
			'icon' =>'myicon',
			'sound' =>'mySound',
			'body' 	=> $body,
			'title'	=> $title,
			
			);
		 
		
		
		$and=array(
			'priority'=>'high'
		 );
		
		
			$fields = array
			(
				'to'		=> $registrationIds,
				'data'		=> $data,
				'android'		=> $and
				
			);
		$headers = array
			(
				'Authorization: key=' . $API_ACCESS_KEY1,
				'Content-Type: application/json'
			);
traceMessage(print_r_log($API_ACCESS_KEY1));
traceMessage(print_r_log($fields));
traceMessage(print_r_log($headers));
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		traceMessage("Res:".print_r_log($result));
	//echo "<pre>";
	//print_r($fields);
	//	print_r($result);
		$resArr = json_decode($result);
	
		/*
		if($resArr->success==1)
			return "yes";
		else	
			return "no";*/
		
		$blUser = new BL_User();
		$notArr = array();
		
		$notArr['medium']="android";
		$notArr['notification_type']=$type;
		$notArr['booking_id']=$objId;
		$notArr['user_id']=$userId;
		$notArr['insertion_datetime']=date('Y-m-d H:i:s');
		$notArr['title']=$title;
		$notArr['message']=$body;
		if($resArr->success==1)
			$notArr['sent_status']="yes";
		else
			$notArr['sent_status']="no";
		$notArr['read_status']="no";
		
		$blUser->AddNotification($notArr);
		return $resArr;
	}
	function SendSMS($agentMobile,$randomnum)
	{
		traceMessage("IN SendSMS($agentMobile,$randomnum)");
		$url = "https://pk.eocean.us/APIManagement/API/RequestAPI?user=wapsi&pwd=AD5XHEeVLEmTuyU7qvbdINJxfzi8dbeD%2fAeguH92tYKdSQS6rk9tzASC811rjPjFPw%3d%3d&sender=WAPSI&reciever=$agentMobile&msg-data=$randomnum&response=string";
		
		traceMessage("URL::::".$url);
		$gblCallHeaders[] = "Content-Type: application/json;charset=utf-8";

		 $gblCall = curl_init();
		curl_setopt($gblCall, CURLOPT_URL, $url);
		curl_setopt($gblCall, CURLOPT_GET, TRUE);
		curl_setopt($gblCall, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($gblCall, CURLOPT_HTTPHEADER, $gblCallHeaders);

		// Get the response
		$response = curl_exec($gblCall);
	}
?>
