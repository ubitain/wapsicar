<?php
function SendCustomerWelcomeEmail($emailAddress,$password)
{

	$header = "Congratulations your account at ".DOMAIN_NAME." is created!!";
	$body = "Welcome to <a href='http://".SERVER_NAME."'>".SITE_HEADING."</a> .

	Your account at <a href='http://".SERVER_NAME."'>".SITE_HEADING."</a> has been activated. Following are your login details.
	
	LoginId: $emailAddress
	Password: $password
					
	you can now login to your restaurant and manage your account, in case of any issue feel free to contact our support team at ".SUPPORT_ADDRESS."
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}
function SendShopWelcomeEmail($emailAddress,$storename,$userName,$password,$sid)
{

	$header = "Congratulations your restaurant ".ucwords($storename)." at ".DOMAIN_NAME." is created!!";
	$body = "Welcome to <a href='http://".SERVER_NAME."'>".SITE_HEADING."</a> .

	Your account at <a href='http://".SERVER_NAME."'>".SITE_HEADING."</a> has been activated. Following are your login details.
	
	Store Name:".ucwords($storename)."		
	LoginId: $emailAddress
	Password: $password
					
	you can now login to your restaurant and manage your account, in case of any issue feel free to contact our support team at ".SUPPORT_ADDRESS."
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}
function SendAuthenticationConfirm($emailAddress,$storename,$userName,$password,$sid)
{
	global $mysessionemail;
	$mysessionemail = new Session();
	$mysessionemail->startsession("","");
	$sid = $mysessionemail->sessionid; 
	$mysessionemail->setvalue("emailaddress",$emailAddress);
	$mysessionemail->setvalue("storename",$storename);
	$mysessionemail->setvalue("username",$userName);
	$mysessionemail->setvalue("password",$password);
	$header = DOMAIN_NAME." restaurant Validation is required";
	$body = "You have created a new restaurant $storename at ".DOMAIN_NAME."
	Please click on the link below to complete the authentication process.
	
	<a href='http://".SERVER_NAME."/validateshop.php?mysid=$sid'>Validate your restaurant</a> 
	";
	
	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}

function SendOrderEmailToAdmin($emailAddress,$shopId,$orderNo)
{
	require_once(MODULE . "/class/bllayer/shop.php");
	require_once(MODULE . "/class/bllayer/orders.php");
	
	
	$blShops = new BL_Shop();
	$blOrder = new BL_Order();
	$shopList = $blShops->GetShopInfo($shopId);
	$category = $shopList->rows[0]["category"];
	$shopCurrency = $shopList->rows[0]["currency"];
	$currencySymbol = $shopList->rows[0]["currency_symbol"];
	$shopName = ucwords($shopList->rows[0]["shop_full_name"]);

	$shopOrders = $blOrder->GetOrderDetailsByOrderNo($shopId,$orderNo);
	//print_r_pre($shopOrders,"aa");
	$orderRecId = $shopOrders->rows[0]["orderRecId"];
	$orderNo = $shopOrders->rows[0]["order_no"];
	$orderDate = date("M j, Y, g:i a",strtotime($shopOrders->rows[0]["order_date"]));
	$paymentMode = $shopOrders->rows[0]["payment_mode"];
	//$price = $currencySymbol.$shopOrders->rows[0]["total_price"];
	$price = $currencySymbol.round($shopOrders->rows[0]["total_price"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$status = $shopOrders->rows[0]["status"];
	
	$couponCode = $shopOrders->rows[0]["coupon_code"];
	if($shopOrders->rows[0]["discounted_pirce"]==0)
		$disPrice = $price;
	else	
		$disPrice = $currencySymbol.round($shopOrders->rows[0]["discounted_pirce"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$orderGridStatus = $shopOrders->rows[0]["order_status"];
	
	global $statusArr;
	$status = $statusArr[$orderGridStatus];
	
	
	$body = GetCss('New Order Placed At '.$shopName);
	$body .= "<p>There is a new order placed on your restaurant '$shopName', below is the details of the order.</p>";
	
	$body .= "<table height=100% width=100% border=0 class=''  cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top>
		
			<table class='table' width=100%>
				
				<thead>
					
					<tr>
						<th colspan=6 align=left>
							<table width=50% border=0>
								<tr>
									<th width=1% nowrap align=left>Restaurant Name:</th>
									<th align=left>$shopName</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Order No:</th>
									<th align=left>$orderNo</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Date:</th>
									<th align=left>$orderDate</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Payment Mode:</th>
									<th align=left>$paymentMode</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Price:</th>
									<th align=left>$price</th>
								</tr>
								
								";
								
								if($couponCode!="")
								{
								$body.="<tr>
									<th width=1% nowrap align=left>Coupon Code:</th>
									<th align=left>$couponCode</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Discounted Price:</th>
									<th>$disPrice</th>
								</tr>";
								}
							$body.="
								
							</table>
						</th>
					</tr>
					<tr>
						<td><br><br></td>
					</tr>
					<tr>
					
						<th align=left>
							Product Name
						</th>
						<th align=left>
							Extra Info
						</th>
						<th align=left>
							Qty Ordered
						</th>
						<th align=left>
							Unit Price
						</th>
						<th align=left>
							Total Price
						</th>
						
					</tr>
					</thead>
					<tbody>
					";
					
					$sum = 0;
					for($x=0;$x<$shopOrders->count;$x++)
					{
						$recId = $shopOrders->rows[$x]["order_product_record_id"];
						$parentRecId = $shopOrders->rows[$x]["parent_record_id"];
						if($parentRecId!="-1")
							continue;
								
						$sum = $sum+($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]); 
						$variantId = $shopOrders->rows[$x]["variant_id"];
						$productId = $shopOrders->rows[$x]["product_id"];
						$prComments = nl2br($shopOrders->rows[$x]["productComments"]);
						$productPrice = $currencySymbol.$shopOrders->rows[$x]["price"];
						$totalPrPrice = $currencySymbol.($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]);
						$childProducts = $blOrder->GetOrderDetailsByParentRecordId($shopId,$orderRecId,$recId);
						
						
						if($variantId!="" && $variantId!="0")
						{
							$variantInfo = $blShops->GetProductVariantInfo($variantId,$productId,$shopId);
							$pName = $variantInfo[0]["product_variant_name"];
							
						} 
						else
							$pName = $shopOrders->rows[$x]["product"];
							//$pName = $shopOrders->rows[0]["product"];
						
						//print_r_pre($variantInfo);
						$quantityOrdered = $shopOrders->rows[$x]["quantity"];
						if($variantId==0)
						{
							$quantityLeft = $shopOrders->rows[$x]["quantity_left"];
							$unlimited = $shopOrders->rows[$x]["unlimited_quantity"];
						}
						else
						{
							$quantityLeft = $variantInfo[0]["quantity"];
							$unlimited = 0;
						}
						
						
						
							$class = ""	;
							$msg = "";
						if($unlimited!=0)
							$quantityLeft = "&infin;";
						if($orderGridStatus=='1')
							$class="";
						
					$body.="<tr>
						<td align=left>
							$pName($productPrice)";
					
				
							for($x1=0;$x1<$childProducts->count;$x1++)
							{
								$sum = $sum+($childProducts->rows[$x1]["quantity"]*$childProducts->rows[$x1]["price"]);
								$cVariantId = $childProducts->rows[$x1]["variant_id"];
								$cProductId = $childProducts->rows[$x1]["product_id"];
								
								if($cVariantId!="" && $cVariantId!="0")
								{
									$cvariantInfo = $blShops->GetProductVariantInfo($cVariantId,$cProductId,$shopId);
									$cpName = $cvariantInfo[0]["product_variant_name"];
									
								} 
								else
									$cpName = $childProducts->rows[$x1]["product"];
								
								//print_r_pre($variantInfo);
								$cquantityOrdered = $childProducts->rows[$x1]["quantity"];
								if($cVariantId==0)
								{
									$cquantityLeft = $childProducts->rows[$x1]["quantity_left"];
									$cunlimited = $childProducts->rows[$x1]["unlimited_quantity"];
								}
								else
								{
									$cquantityLeft = $cvariantInfo[0]["quantity"];
									$cunlimited = 0;
								}
								$cUunitPrice = $childProducts->rows[$x1]["price"];
								$unitPrice = $unitPrice+$cUunitPrice;
								if($cUunitPrice=="" || $cUunitPrice<0)
									$cUunitPrice = "0.00";
								$body.="<table width=100% height=100% style='padding-left:10px'>
										<tr>
											<td style='border:0px solid grey;padding-left:50px'>$cpName($currencySymbol$cUunitPrice)</td>
										</tr>
										
									</table>";
								
							}

						$body.="</td>
						<td width='100px' align=left>
							$prComments
						</td>
						<td align=left>
							$quantityOrdered
						</td>
						<td align=left>
							$productPrice
						</td>
						<td align=left>
							$totalPrPrice
							<span style='font-size:12px'>$msg</span>
						</td>
						
					</tr>";
					}
					$sum = $sum+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"];
					$body.="<tr>
						
						<th colspan=6 align=right style='text-align:right'>".GST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["gst_price"],2)."</th>
					</tr>
					<tr>
						
						<th colspan=6 align=right style='text-align:right'>".PST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["pst_price"],2)."</th>
					</tr>
					
					<tr>
						
						<th colspan=6 align=right style='text-align:right'><br><br>Grand Total:".$currencySymbol.round($sum,2)."</th>
					</tr>				
				</tbody>
			</table>
		</td>
	</tr>
</table>";
	
	$body .= GetFooter(); 
	
	//echo $body;
	$header = "New Order has been placed on your restaurant ".$shopName;
	SendMailFromSupport($body,$emailAddress,$header);
}

function SendOrderEmail($emailAddress,$shopId,$orderNo)
{
	// need to deploy ,sb.quantity as quantity_left on function GetOrderDetailsByOrderNo on live AT
	require_once(MODULE . "/class/bllayer/shop.php");
	require_once(MODULE . "/class/bllayer/orders.php");
	
	$blShops = new BL_Shop();
	$blOrder = new BL_Order();
	$shopList = $blShops->GetShopInfo($shopId);
	$category = $shopList->rows[0]["category"];
	$shopCurrency = $shopList->rows[0]["currency"];
	$currencySymbol = $shopList->rows[0]["currency_symbol"];
	$shopName = ucwords($shopList->rows[0]["shop_full_name"]);

	$shopOrders = $blOrder->GetOrderDetailsByOrderNo($shopId,$orderNo);
	//print_r_pre($shopOrders,"aa");
	$orderRecId = $shopOrders->rows[0]["orderRecId"];
	$orderNo = $shopOrders->rows[0]["order_no"];
	$orderDate = date("M j, Y, g:i a",strtotime($shopOrders->rows[0]["order_date"]));
	$paymentMode = $shopOrders->rows[0]["payment_mode"];
	//$price = $currencySymbol.$shopOrders->rows[0]["total_price"];
	$price = $currencySymbol.round($shopOrders->rows[0]["total_price"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$status = $shopOrders->rows[0]["status"];
	
	$couponCode = $shopOrders->rows[0]["coupon_code"];
	if($shopOrders->rows[0]["discounted_pirce"]==0)
		$disPrice = $price;
	else	
		$disPrice = $currencySymbol.round($shopOrders->rows[0]["discounted_pirce"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
		
	$orderGridStatus = $shopOrders->rows[0]["order_status"];
	
	global $statusArr;
	$status = $statusArr[$orderGridStatus];
	
	$body = GetCss('Your Order has been placed'); 
	
	$body .= "<p>Dear Customer<br><br>Thank you for placing the order on ".DOMAIN_NAME.", below is the details for your order.</p>";
	$body .= "<p>in case of any issue feel free to contact our support team at ".SUPPORT_ADDRESS."</p>";
	$body .= "<table height=100% width=100% border=0 class=''  cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top>
		
			<table class='table' width=100%>
				
				<thead>
					
					<tr>
						<th colspan=6 align=right>
							<table class='table' width=100% border=0>
								
								<tr>
									<th width=1% nowrap align=left>Restaurant Name:</th>
									<th align=left>$shopName</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Order No:</th>
									<th align=left>$orderNo</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Date:</th>
									<th align=left>$orderDate</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Payment Mode:</th>
									<th align=left>$paymentMode</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Price:</th>
									<th align=left>$price</th>
								</tr>
								";
								
								if($couponCode!="")
								{
								$body.="<tr>
									<th width=1% nowrap align=left>Coupon Code:</th>
									<th align=left>$couponCode</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Discounted Price:</th>
									<th>$disPrice</th>
								</tr>";
								}
							$body.="</table>
						</th>
					</tr>
					<tr>
						<td><br><br><br></td>
					</tr>
					<tr>
					
						<th align=left>
							Product Name
						</th>
						<th align=left>
							Extra Info
						</th>
						<th align=left>
							Qty Ordered
						</th>
						<th align=left>
							Unit Price
						</th>
						<th align=left>
							Total Price
						</th>
						
					</tr>
					</thead>
					<tbody>
					";
					
					$sum = 0;
					for($x=0;$x<$shopOrders->count;$x++)
					{
						$recId = $shopOrders->rows[$x]["order_product_record_id"];
						$parentRecId = $shopOrders->rows[$x]["parent_record_id"];
						if($parentRecId!="-1")
							continue;
								
						$sum = $sum+($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]); 
						$variantId = $shopOrders->rows[$x]["variant_id"];
						$productId = $shopOrders->rows[$x]["product_id"];
						$prComments = nl2br($shopOrders->rows[$x]["productComments"]);
						$productPrice = $currencySymbol.$shopOrders->rows[$x]["price"];
						$totalPrPrice = $currencySymbol.($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]);
						$childProducts = $blOrder->GetOrderDetailsByParentRecordId($shopId,$orderRecId,$recId);
						
						
						if($variantId!="" && $variantId!="0")
						{
							$variantInfo = $blShops->GetProductVariantInfo($variantId,$productId,$shopId);
							$pName = $variantInfo[0]["product_variant_name"];
							
						} 
						else
							$pName = $shopOrders->rows[$x]["product"];
							//$pName = $shopOrders->rows[0]["product"];
						
						//print_r_pre($variantInfo);
						$quantityOrdered = $shopOrders->rows[$x]["quantity"];
						if($variantId==0)
						{
							$quantityLeft = $shopOrders->rows[$x]["quantity_left"];
							$unlimited = $shopOrders->rows[$x]["unlimited_quantity"];
						}
						else
						{
							$quantityLeft = $variantInfo[0]["quantity"];
							$unlimited = 0;
						}
						
						
						
							$class = ""	;
							$msg = "";
						if($unlimited!=0)
							$quantityLeft = "&infin;";
						if($orderGridStatus=='1')
							$class="";
						
					$body.="<tr>
						<td align=left>
							$pName($productPrice)";
					
				
							for($x1=0;$x1<$childProducts->count;$x1++)
							{
								$sum = $sum+($childProducts->rows[$x1]["quantity"]*$childProducts->rows[$x1]["price"]);
								$cVariantId = $childProducts->rows[$x1]["variant_id"];
								$cProductId = $childProducts->rows[$x1]["product_id"];
								
								if($cVariantId!="" && $cVariantId!="0")
								{
									$cvariantInfo = $blShops->GetProductVariantInfo($cVariantId,$cProductId,$shopId);
									$cpName = $cvariantInfo[0]["product_variant_name"];
									
								} 
								else
									$cpName = $childProducts->rows[$x1]["product"];
								
								//print_r_pre($variantInfo);
								$cquantityOrdered = $childProducts->rows[$x1]["quantity"];
								if($cVariantId==0)
								{
									$cquantityLeft = $childProducts->rows[$x1]["quantity_left"];
									$cunlimited = $childProducts->rows[$x1]["unlimited_quantity"];
								}
								else
								{
									$cquantityLeft = $cvariantInfo[0]["quantity"];
									$cunlimited = 0;
								}
								$cUunitPrice = $childProducts->rows[$x1]["price"];
								$unitPrice = $unitPrice+$cUunitPrice;
								
								if($cUunitPrice=="" || $cUunitPrice<0)
									$cUunitPrice = "0.00";
								
								$body.="<table width=100% height=100% style='padding-left:10px'>
										<tr>
											<td style='border:0px solid grey;padding-left:50px'>$cpName($currencySymbol$cUunitPrice)</td>
										</tr>
										
									</table>";
								
							}

						$body.="</td>
						<td width='100px' align=left>
							$prComments
						</td>
						<td align=left>
							$quantityOrdered
						</td>
						<td align=left>
							$productPrice
						</td>
						<td align=left>
							$totalPrPrice
							<span style='font-size:12px'>$msg</span>
						</td>
						
					</tr>";
					}
					$sum = $sum+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"];
					$body.="<tr>
						
						<th colspan=6 align=right style='text-align:right'>".GST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["gst_price"],2)."</th>
					</tr>
					<tr>
						
						<th colspan=6 align=right style='text-align:right'>".PST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["pst_price"],2)."</th>
					</tr>
					
					<tr>
						
						<th colspan=6 align=right style='text-align:right'><br><br>Grand Total:".$currencySymbol.round($sum,2)."</th>
					</tr>				
				</tbody>
			</table>
		</td>
	</tr>
</table>";	
	$body .= GetFooter(); 

	
	//echo $body;
	$header = "your order has been placed";
	SendMailFromSupport($body,$emailAddress,$header);
}

function SendPaymentEmailToAdmin($emailAddress,$shopId,$orderNo)
{
	// need to deploy ,sb.quantity as quantity_left on function GetOrderDetailsByOrderNo on live AT
	require_once(MODULE . "/class/bllayer/shop.php");
	require_once(MODULE . "/class/bllayer/orders.php");
	
	$blShops = new BL_Shop();
	$blOrder = new BL_Order();
	$shopList = $blShops->GetShopInfo($shopId);
	$category = $shopList->rows[0]["category"];
	$shopCurrency = $shopList->rows[0]["currency"];
	$currencySymbol = $shopList->rows[0]["currency_symbol"];
	$shopName = ucwords($shopList->rows[0]["shop_full_name"]);

	$shopOrders = $blOrder->GetOrderDetailsByOrderNo($shopId,$orderNo);
	//print_r_pre($shopOrders,"aa");
	$orderRecId = $shopOrders->rows[0]["orderRecId"];
	$orderNo = $shopOrders->rows[0]["order_no"];
	$orderDate = date("M j, Y, g:i a",strtotime($shopOrders->rows[0]["order_date"]));
	$paymentMode = $shopOrders->rows[0]["payment_mode"];
	//$price = $currencySymbol.$shopOrders->rows[0]["total_price"];
	$price = $currencySymbol.round($shopOrders->rows[0]["total_price"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$status = $shopOrders->rows[0]["status"];
	
	$couponCode = $shopOrders->rows[0]["coupon_code"];
	if($shopOrders->rows[0]["discounted_pirce"]==0)
		$disPrice = $price;
	else	
		$disPrice = $currencySymbol.round($shopOrders->rows[0]["discounted_pirce"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
		
	$orderGridStatus = $shopOrders->rows[0]["order_status"];
	
	global $statusArr;
	$status = $statusArr[$orderGridStatus];
	
	
	
	
	$body = GetCss("Order Status Updated at ".$shopName); 
	
	$body .= "<p>The status of an order on your restaurant '$shopName' has been update, please check the details below,</p>";
	
	$body .= "<p>There is a new order placed on your restaurant '$shopName', below is the details of the order.</p>";
	
	$body .= "<table height=100% width=100% border=0 class=''  cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top>
		
			<table class='table' width=100%>
				
				<thead>
					
					<tr>
						<th colspan=6 align=right>
							<table class='table' width=100% border=0>
								
								<tr>
									<th width=1% nowrap align=left>Restaurant Name:</th>
									<th align=left>$shopName</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Order No:</th>
									<th align=left>$orderNo</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Date:</th>
									<th align=left>$orderDate</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Payment Mode:</th>
									<th align=left>$paymentMode</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Price:</th>
									<th align=left>$price</th>
								</tr>
								";
								
								if($couponCode!="")
								{
								$body.="<tr>
									<th width=1% nowrap align=left>Coupon Code:</th>
									<th align=left>$couponCode</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Discounted Price:</th>
									<th>$disPrice</th>
								</tr>";
								}
							$body.="</table>
						</th>
					</tr>
					<tr>
						<td><br><br><br></td>
					</tr>
					<tr>
					
						<th align=left>
							Product Name
						</th>
						<th align=left>
							Extra Info
						</th>
						<th align=left>
							Qty Ordered
						</th>
						<th align=left>
							Unit Price
						</th>
						<th align=left>
							Total Price
						</th>
						
					</tr>
					</thead>
					<tbody>
					";
					
					$sum = 0;
					for($x=0;$x<$shopOrders->count;$x++)
					{
						$recId = $shopOrders->rows[$x]["order_product_record_id"];
						$parentRecId = $shopOrders->rows[$x]["parent_record_id"];
						if($parentRecId!="-1")
							continue;
								
						$sum = $sum+($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]); 
						$variantId = $shopOrders->rows[$x]["variant_id"];
						$productId = $shopOrders->rows[$x]["product_id"];
						$prComments = nl2br($shopOrders->rows[$x]["productComments"]);
						$productPrice = $currencySymbol.$shopOrders->rows[$x]["price"];
						$totalPrPrice = $currencySymbol.($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]);
						$childProducts = $blOrder->GetOrderDetailsByParentRecordId($shopId,$orderRecId,$recId);
						
						
						if($variantId!="" && $variantId!="0")
						{
							$variantInfo = $blShops->GetProductVariantInfo($variantId,$productId,$shopId);
							$pName = $variantInfo[0]["product_variant_name"];
							
						} 
						else
							$pName = $shopOrders->rows[$x]["product"];
							//$pName = $shopOrders->rows[0]["product"];
						
						//print_r_pre($variantInfo);
						$quantityOrdered = $shopOrders->rows[$x]["quantity"];
						if($variantId==0)
						{
							$quantityLeft = $shopOrders->rows[$x]["quantity_left"];
							$unlimited = $shopOrders->rows[$x]["unlimited_quantity"];
						}
						else
						{
							$quantityLeft = $variantInfo[0]["quantity"];
							$unlimited = 0;
						}
						
						
						
							$class = ""	;
							$msg = "";
						if($unlimited!=0)
							$quantityLeft = "&infin;";
						if($orderGridStatus=='1')
							$class="";
						
					$body.="<tr>
						<td align=left>
							$pName($productPrice)";
					
				
							for($x1=0;$x1<$childProducts->count;$x1++)
							{
								$sum = $sum+($childProducts->rows[$x1]["quantity"]*$childProducts->rows[$x1]["price"]);
								$cVariantId = $childProducts->rows[$x1]["variant_id"];
								$cProductId = $childProducts->rows[$x1]["product_id"];
								
								if($cVariantId!="" && $cVariantId!="0")
								{
									$cvariantInfo = $blShops->GetProductVariantInfo($cVariantId,$cProductId,$shopId);
									$cpName = $cvariantInfo[0]["product_variant_name"];
									
								} 
								else
									$cpName = $childProducts->rows[$x1]["product"];
								
								//print_r_pre($variantInfo);
								$cquantityOrdered = $childProducts->rows[$x1]["quantity"];
								if($cVariantId==0)
								{
									$cquantityLeft = $childProducts->rows[$x1]["quantity_left"];
									$cunlimited = $childProducts->rows[$x1]["unlimited_quantity"];
								}
								else
								{
									$cquantityLeft = $cvariantInfo[0]["quantity"];
									$cunlimited = 0;
								}
								$cUunitPrice = $childProducts->rows[$x1]["price"];
								$unitPrice = $unitPrice+$cUunitPrice;
								
								if($cUunitPrice=="" || $cUunitPrice<0)
									$cUunitPrice = "0.00";
								
								$body.="<table width=100% height=100% style='padding-left:10px'>
										<tr>
											<td style='border:0px solid grey;padding-left:50px'>$cpName($currencySymbol$cUunitPrice)</td>
										</tr>
										
									</table>";
								
							}

						$body.="</td>
						<td width='100px' align=left>
							$prComments
						</td>
						<td align=left>
							$quantityOrdered
						</td>
						<td align=left>
							$productPrice
						</td>
						<td align=left>
							$totalPrPrice
							<span style='font-size:12px'>$msg</span>
						</td>
						
					</tr>";
					}
					$sum = $sum+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"];
					$body.="<tr>
						
						<th colspan=6 align=right style='text-align:right'>".GST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["gst_price"],2)."</th>
					</tr>
					<tr>
						
						<th colspan=6 align=right style='text-align:right'>".PST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["pst_price"],2)."</th>
					</tr>
					
					<tr>
						
						<th colspan=6 align=right style='text-align:right'><br><br>Grand Total:".$currencySymbol.round($sum,2)."</th>
					</tr>				
				</tbody>
			</table>
		</td>
	</tr>
</table>";
	$body .= GetFooter(); 
	$header = "Order Status has been updated on your restaurant $shopName";
	SendMailFromSupport($body,$emailAddress,$header);
	//echo $body;
}


function SendPaymentEmail($emailAddress,$shopId,$orderNo)
{
	// need to deploy ,sb.quantity as quantity_left on function GetOrderDetailsByOrderNo on live AT
	require_once(MODULE . "/class/bllayer/shop.php");
	require_once(MODULE . "/class/bllayer/orders.php");
	
	$blShops = new BL_Shop();
	$blOrder = new BL_Order();
	$shopList = $blShops->GetShopInfo($shopId);
	$category = $shopList->rows[0]["category"];
	$shopCurrency = $shopList->rows[0]["currency"];
	$currencySymbol = $shopList->rows[0]["currency_symbol"];
	$shopName = ucwords($shopList->rows[0]["shop_full_name"]);

	$shopOrders = $blOrder->GetOrderDetailsByOrderNo($shopId,$orderNo);
	//print_r_pre($shopOrders,"aa");
	$orderRecId = $shopOrders->rows[0]["orderRecId"];
	$orderNo = $shopOrders->rows[0]["order_no"];
	$orderDate = date("M j, Y, g:i a",strtotime($shopOrders->rows[0]["order_date"]));
	$paymentMode = $shopOrders->rows[0]["payment_mode"];
	//$price = $currencySymbol.$shopOrders->rows[0]["total_price"];
	$price = $currencySymbol.round($shopOrders->rows[0]["total_price"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$status = $shopOrders->rows[0]["status"];
	
	$couponCode = $shopOrders->rows[0]["coupon_code"];
	if($shopOrders->rows[0]["discounted_pirce"]==0)
		$disPrice = $price;
	else	
		$disPrice = $currencySymbol.round($shopOrders->rows[0]["discounted_pirce"]+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"],2);
	
	$orderGridStatus = $shopOrders->rows[0]["order_status"];
	
	global $statusArr;
	$status = $statusArr[$orderGridStatus];
	
	
	
	$body = GetCss("Order Status Updates"); 
	
	$body .= "<p>Dear Customer</p>";
	$body .= "<p>The status of your order has been update, please check the details below,in case of any issue feel free to contact our support team at ".SUPPORT_ADDRESS."</p>";
	$body .= "<table height=100% width=100% border=0 class=''  cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top>
		
			<table class='table' width=100%>
				
				<thead>
					
					<tr>
						<th colspan=6 align=right>
							<table class='table' width=100% border=0>
								
								<tr>
									<th width=1% nowrap align=left>Restaurant Name:</th>
									<th align=left>$shopName</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Order No:</th>
									<th align=left>$orderNo</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Date:</th>
									<th align=left>$orderDate</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Payment Mode:</th>
									<th align=left>$paymentMode</th>
								</tr>
								
								<tr>
									<th width=1% nowrap align=left>Order Price:</th>
									<th align=left>$price</th>
								</tr>
								";
								
								if($couponCode!="")
								{
								$body.="<tr>
									<th width=1% nowrap align=left>Coupon Code:</th>
									<th align=left>$couponCode</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Discounted Price:</th>
									<th>$disPrice</th>
								</tr>";
								}
							$body.="</table>
						</th>
					</tr>
					<tr>
						<td><br><br><br></td>
					</tr>
					<tr>
					
						<th align=left>
							Product Name
						</th>
						<th align=left>
							Extra Info
						</th>
						<th align=left>
							Qty Ordered
						</th>
						<th align=left>
							Unit Price
						</th>
						<th align=left>
							Total Price
						</th>
						
					</tr>
					</thead>
					<tbody>
					";
					
					$sum = 0;
					for($x=0;$x<$shopOrders->count;$x++)
					{
						$recId = $shopOrders->rows[$x]["order_product_record_id"];
						$parentRecId = $shopOrders->rows[$x]["parent_record_id"];
						if($parentRecId!="-1")
							continue;
								
						$sum = $sum+($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]); 
						$variantId = $shopOrders->rows[$x]["variant_id"];
						$productId = $shopOrders->rows[$x]["product_id"];
						$prComments = nl2br($shopOrders->rows[$x]["productComments"]);
						$productPrice = $currencySymbol.$shopOrders->rows[$x]["price"];
						$totalPrPrice = $currencySymbol.($shopOrders->rows[$x]["quantity"]*$shopOrders->rows[$x]["price"]);
						$childProducts = $blOrder->GetOrderDetailsByParentRecordId($shopId,$orderRecId,$recId);
						
						
						if($variantId!="" && $variantId!="0")
						{
							$variantInfo = $blShops->GetProductVariantInfo($variantId,$productId,$shopId);
							$pName = $variantInfo[0]["product_variant_name"];
							
						} 
						else
							$pName = $shopOrders->rows[$x]["product"];
							//$pName = $shopOrders->rows[0]["product"];
						
						//print_r_pre($variantInfo);
						$quantityOrdered = $shopOrders->rows[$x]["quantity"];
						if($variantId==0)
						{
							$quantityLeft = $shopOrders->rows[$x]["quantity_left"];
							$unlimited = $shopOrders->rows[$x]["unlimited_quantity"];
						}
						else
						{
							$quantityLeft = $variantInfo[0]["quantity"];
							$unlimited = 0;
						}
						
						
						
							$class = ""	;
							$msg = "";
						if($unlimited!=0)
							$quantityLeft = "&infin;";
						if($orderGridStatus=='1')
							$class="";
						
					$body.="<tr>
						<td align=left>
							$pName($productPrice)";
					
				
							for($x1=0;$x1<$childProducts->count;$x1++)
							{
								$sum = $sum+($childProducts->rows[$x1]["quantity"]*$childProducts->rows[$x1]["price"]);
								$cVariantId = $childProducts->rows[$x1]["variant_id"];
								$cProductId = $childProducts->rows[$x1]["product_id"];
								
								if($cVariantId!="" && $cVariantId!="0")
								{
									$cvariantInfo = $blShops->GetProductVariantInfo($cVariantId,$cProductId,$shopId);
									$cpName = $cvariantInfo[0]["product_variant_name"];
									
								} 
								else
									$cpName = $childProducts->rows[$x1]["product"];
								
								//print_r_pre($variantInfo);
								$cquantityOrdered = $childProducts->rows[$x1]["quantity"];
								if($cVariantId==0)
								{
									$cquantityLeft = $childProducts->rows[$x1]["quantity_left"];
									$cunlimited = $childProducts->rows[$x1]["unlimited_quantity"];
								}
								else
								{
									$cquantityLeft = $cvariantInfo[0]["quantity"];
									$cunlimited = 0;
								}
								$cUunitPrice = $childProducts->rows[$x1]["price"];
								$unitPrice = $unitPrice+$cUunitPrice;
								
								if($cUunitPrice=="" || $cUunitPrice<0)
									$cUunitPrice = "0.00";
								
								$body.="<table width=100% height=100% style='padding-left:10px'>
										<tr>
											<td style='border:0px solid grey;padding-left:50px'>$cpName($currencySymbol$cUunitPrice)</td>
										</tr>
										
									</table>";
								
							}

						$body.="</td>
						<td width='100px' align=left>
							$prComments
						</td>
						<td align=left>
							$quantityOrdered
						</td>
						<td align=left>
							$productPrice
						</td>
						<td align=left>
							$totalPrPrice
							<span style='font-size:12px'>$msg</span>
						</td>
						
					</tr>";
					}
					$sum = $sum+$shopOrders->rows[0]["gst_price"]+$shopOrders->rows[0]["pst_price"];
					$body.="<tr>
						
						<th colspan=6 align=right style='text-align:right'>".GST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["gst_price"],2)."</th>
					</tr>
					<tr>
						
						<th colspan=6 align=right style='text-align:right'>".PST_CHARGES.":".$currencySymbol.round($shopOrders->rows[0]["pst_price"],2)."</th>
					</tr>
					
					<tr>
						
						<th colspan=6 align=right style='text-align:right'><br><br>Grand Total:".$currencySymbol.round($sum,2)."</th>
					</tr>				
				</tbody>
			</table>
		</td>
	</tr>
</table>";
$body .= GetFooter();	
//echo $body;
	$header = "Your Order Status has been updated";
	SendMailFromSupport($body,$emailAddress,$header);
}

function SendPayPalGateWayEmail($emailAddress,$body)
{

	$header = "Dear User";
	$body = "$body
					
	if you have not done this activity please let us know at ".SUPPORT_ADDRESS." so that we can resolve this matter immediately
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}
function SendAuthNetGateWayEmail($emailAddress,$body)
{
	$header = "Dear User";
	$body = "$body
					
	if you have not done this activity please let us know at ".SUPPORT_ADDRESS." so that we can resolve this matter immediately
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}

function GetCss($heading)
{
	$css = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html><head><title></title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta name='viewport' content='width=320, target-densitydpi=device-dpi'><style type='text/css'>
/* Mobile-specific Styles */
@media only screen and (max-width: 660px) { 
table[class=w0], td[class=w0] { width: 0 !important; }
table[class=w10], td[class=w10], img[class=w10] { width:10px !important; }
table[class=w15], td[class=w15], img[class=w15] { width:5px !important; }
table[class=w30], td[class=w30], img[class=w30] { width:10px !important; }
table[class=w60], td[class=w60], img[class=w60] { width:10px !important; }
table[class=w125], td[class=w125], img[class=w125] { width:80px !important; }
table[class=w130], td[class=w130], img[class=w130] { width:55px !important; }
table[class=w140], td[class=w140], img[class=w140] { width:90px !important; }
table[class=w160], td[class=w160], img[class=w160] { width:180px !important; }
table[class=w170], td[class=w170], img[class=w170] { width:100px !important; }
table[class=w180], td[class=w180], img[class=w180] { width:80px !important; }
table[class=w195], td[class=w195], img[class=w195] { width:80px !important; }
table[class=w220], td[class=w220], img[class=w220] { width:80px !important; }
table[class=w240], td[class=w240], img[class=w240] { width:180px !important; }
table[class=w255], td[class=w255], img[class=w255] { width:185px !important; }
table[class=w275], td[class=w275], img[class=w275] { width:135px !important; }
table[class=w280], td[class=w280], img[class=w280] { width:135px !important; }
table[class=w300], td[class=w300], img[class=w300] { width:140px !important; }
table[class=w325], td[class=w325], img[class=w325] { width:95px !important; }
table[class=w360], td[class=w360], img[class=w360] { width:140px !important; }
table[class=w410], td[class=w410], img[class=w410] { width:180px !important; }
table[class=w470], td[class=w470], img[class=w470] { width:200px !important; }
table[class=w580], td[class=w580], img[class=w580] { width:280px !important; }
table[class=w840], td[class=w840], img[class=w840] { width:300px !important; }
table[class*=hide], td[class*=hide], img[class*=hide], p[class*=hide], span[class*=hide] { display:none !important; }
table[class=h0], td[class=h0] { height: 0 !important; }
p[class=footer-content-left] { text-align: center !important; }
#headline p { font-size: 30px !important; }
.article-content, #left-sidebar{ -webkit-text-size-adjust: 90% !important; -ms-text-size-adjust: 90% !important; }
.header-content, .footer-content-left {-webkit-text-size-adjust: 80% !important; -ms-text-size-adjust: 80% !important;}
img { height: auto; line-height: 100%;}
 } 
/* Client-specific Styles */
#outlook a { padding: 0; }	/* Force Outlook to provide a 'view in browser' button. */
body { width: 100% !important; }
.ReadMsgBody { width: 100%; }
.ExternalClass { width: 100%; display:block !important; } /* Force Hotmail to display emails at full width */
/* Reset Styles */
/* Add 100px so mobile switch bar doesn't cover street address. */
body { background-color: #ececec; margin: 0; padding: 0; }
img { outline: none; text-decoration: none; display: block;}
br, strong br, b br, em br, i br { line-height:100%; }
h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { color: blue !important; }
h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {	color: red !important; }
/* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }
/* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */  
table td, table tr { border-collapse: collapse; }
.yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {
color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;
}	/* Body text color for the New Yahoo.  This example sets the font of Yahoo's Shortcuts to black. */
/* This most probably won't work in all email clients. Don't include code blocks in email. */
code {
  white-space: normal;
  word-break: break-all;
}
#background-table { background-color: #ececec; }
/* Webkit Elements */
#top-bar { border-radius:6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius:6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #556c90; color: #d9fffd; }
#top-bar a { font-weight: bold; color: #d9fffd; text-decoration: none;}
#footer { border-radius:0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius:0px 0px 6px 6px; -webkit-font-smoothing: antialiased; }
/* Fonts and Content */
body, td { font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; }
.header-content, .footer-content-left, .footer-content-right { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; }
/* Prevent Webkit and Windows Mobile platforms from changing default font sizes on header and footer. */
.header-content { font-size: 12px; color: #d9fffd; }
.header-content a { font-weight: bold; color: #d9fffd; text-decoration: none; }
#headline p { color: #fcfcfc; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 36px; text-align: center; margin-top:0px; margin-bottom:30px; }
#headline p a { color: #fcfcfc; text-decoration: none; }
.article-title { font-size: 18px; line-height:24px; color: #525252; font-weight:bold; margin-top:0px; margin-bottom:18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; }
.article-title a { color: #525252; text-decoration: none; }
.article-title.with-meta {margin-bottom: 0;}
.article-meta { font-size: 13px; line-height: 20px; color: #ccc; font-weight: bold; margin-top: 0;}
.article-content { font-size: 13px; line-height: 18px; color: #444444; margin-top: 0px; margin-bottom: 18px; font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; }
.article-content a { color: #3f6569; font-weight:bold; text-decoration:none; }
.article-content img { max-width: 100% }
.article-content ol, .article-content ul { margin-top:0px; margin-bottom:18px; margin-left:19px; padding:0; }
.article-content li { font-size: 13px; line-height: 18px; color: #444444; }
.article-content li a { color: #3f6569; text-decoration:underline; }
.article-content p {margin-bottom: 15px;}
.footer-content-left { font-size: 12px; line-height: 15px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; }
.footer-content-left a { color: #d9fffd; font-weight: bold; text-decoration: none; }
.footer-content-right { font-size: 11px; line-height: 16px; color: #ffffff; margin-top: 0px; margin-bottom: 15px; }
.footer-content-right a { color: #d9fffd; font-weight: bold; text-decoration: none; }
#footer { background-color: #425470; color: #ffffff; }
#footer a { color: #d9fffd; text-decoration: none; font-weight: bold; }
#permission-reminder { white-space: normal; }
#street-address { color: #d9fffd; white-space: normal; }
</style><!--[if gte mso 9]><style _tmplitem='281' >.article-content ol, .article-content ul {   margin: 0 0 0 24px;   padding: 0;   list-style-position: inside;}</style><![endif]-->
<table width='100%' cellpadding='0' cellspacing='0' border='0' id='background-table'>
<tr>
<td align='center' bgcolor='#ececec'>
<table class='w840' style='margin:0 10px;' width='840' cellpadding='0' cellspacing='0' border='0'>
<tr><td class='w840' width='840' height='20'></td></tr>

<tr>
<td class='w840' width='840'>
<table id='top-bar' class='w840' width='840' cellpadding='0' cellspacing='0' border='0' bgcolor='#425470'>
<tr>
<td class='w30' width='100%' align=center height='30px' style='color:white'>&nbsp;</td>

<td class='w15' width='15'></td>
</tr>
</table>

</td>
</tr>
<tr>
<td id='header' class='w840' width='840' align='center' bgcolor='#425470'>

<table class='w840' width='840' cellpadding='0' cellspacing='0' border='0'>
<tr><td class='w30' width='30'></td><td class='w580' width='580' height='30'></td><td class='w30' width='30'></td></tr>
<tr>
<td class='w30' width='30'></td>
<td class='w580' width='580'>
<div align='center' id='headline' style='font-size:24px;color:white'>
<p style='font-size:24px;color:white'>
<strong><singleline label='Title' style='font-size:24px;color:white'>$heading</singleline></strong>
</p>
</div>
</td>
<td class='w30' width='30'></td>
</tr>
</table>


</td>
</tr>

<tr><td class='w840' width='840' height='30' bgcolor='#ffffff'></td></tr>
<tr id='simple-content-row'><td class='w840' width='840' bgcolor='#ffffff'>
<table class='w840' width='840' cellpadding='0' cellspacing='0' border='0'>
<tr>
<td class='w30' width='30'></td>
<td class='w580' width='580'>";
	
	return $css;
}
function GetFooter()
{
	$str =" </td>
            <td class='w30' width='30'></td>
        </tr>
    </tbody></table>
</td></tr>
                <tr><td class='w840' width='840' height='15' bgcolor='#ffffff'></td></tr>
                
                <tr>
                <td class='w840' width='840'>
    <table id='footer' class='w840' width='840' cellpadding='0' cellspacing='0' border='0' bgcolor='#425470'>
        <tbody><tr><td class='w30' width='30'></td><td class='w580 h0' width='360' height='30'></td><td class='w0' width='60'></td><td class='w0' width='160'></td><td class='w30' width='30'></td></tr>
        <tr>
            <td class='w30' width='30'></td>
            <td class='w580' width='360' valign='top'>
            <span class='hide'><p id='permission-reminder' align='left' class='footer-content-left'></p></span>
            <p align='left' class='footer-content-left'></p>
            </td>
            <td class='hide w0' width='60'></td>
            <td class='hide w0' width='160' valign='top'>
            <p id='street-address' align='right' class='footer-content-right'></p>
            </td>
            <td class='w30' width='30'></td>
        </tr>
        <tr><td class='w30' width='30'></td><td class='w580 h0' width='360' height='15'></td><td class='w0' width='60'></td><td class='w0' width='160'></td><td class='w30' width='30'></td></tr>
    </tbody></table>
</td>
                </tr>
                <tr><td class='w840' width='840' height='60'></td></tr>
            </tbody></table>
        </td>
	</tr>
</tbody></table></body></html>";
	return $str;
	
}
function SendVoucherEmail($orderNo,$shopId)
{
		
	require_once(MODULE . "/class/bllayer/shop.php");
		
	$blShops = new BL_Shop();
	$voucherInfo = $blShops->GetGiftVoucherByOrderNo($orderNo,$shopId,"-1");
	
	$vRecId = $voucherInfo->rows[0]["record_id"];
	$price = $voucherInfo->rows[0]["price"];
	$fromUserId = $voucherInfo->rows[0]["from_user_id"];
	$fromName = $voucherInfo->rows[0]["from_name"];
	$toName = $voucherInfo->rows[0]["to_name"];
	$toEmail = $voucherInfo->rows[0]["to_email_address"];
	$msg = $voucherInfo->rows[0]["msg"];
	$shopId = $voucherInfo->rows[0]["shop_id"];
	$couponCode = $voucherInfo->rows[0]["coupon_code"];
	$voucherBG = $voucherInfo->rows[0]["voucher_bg"];
	
	$shopList = $blShops->GetShopInfo($shopId);
	$shopName = $shopList->rows[0]["shop_full_name"];
	$shopUrl = SERVER_URL."/".GetShopURL($shopId);
	
	$body = file_get_contents(APPLICATION_ROOT."/html/vouchers/emails/voucheremail_$voucherBG.php");
	$body = str_replace("<!--TO_USER_NAME-->",$toName,$body);
	$body = str_replace("<!--MESSAGE-->",nl2br($msg),$body);
	$body = str_replace("<!--FROM_USER_NAME-->",$fromName,$body);
	$body = str_replace("<!--SHOP_NAME-->",$shopName,$body);
	$body = str_replace("<!--AMOUNT-->",$price,$body);
	$body = str_replace("<!--SHOP_URL-->",$shopUrl,$body);
	$body = str_replace("<!--VOUCHER_CODE-->",$couponCode,$body);
	$header = "Your Gift Vocuher at Cite-local.com";
	SendMailFromSupport(($body),$toEmail,$header);
		
}	
function SendForgotPasswordEmail($emailAddress)
{
	// need to deploy ,sb.quantity as quantity_left on function GetOrderDetailsByOrderNo on live AT
	require_once(MODULE . "/class/bllayer/user.php");
	
	
	$blUser = new BL_User();
	
	$uInfo = $blUser->GetUserByEmail($emailAddress);
	
	if($uInfo->count<=0)
	{
		return "error";
	}
	else
	{
		$password = $uInfo->rows[0]["password"];
	}
	
	
	
	$body = GetCss("Login Credentials"); 
	
	$body .= "<p>Dear Customer</p>";
	$body .= "<p>On your request we are resending your login credentials,in case of any issue feel free to contact our support team at ".SUPPORT_ADDRESS."</p>";
	$body .= "<table height=100% width=100% border=0 class=''  cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top>
		
			<table class='table' width=100%>
				
				<thead>
					
					<tr>
						<th colspan=6 align=right>
							<table class='table' width=100% border=0>
								
								<tr>
									<th width=1% nowrap align=left>Login Id:</th>
									<th align=left>$emailAddress</th>
								</tr>
								<tr>
									<th width=1% nowrap align=left>Password:</th>
									<th align=left>$password</th>
								</tr>
							</table>
						</th>
					</tr>
					</thead>
			</table>
		</td>
	</tr>
</table>";	
$body .= GetFooter();	
//echo $body;
	$header = "Your Login Credentials at ".DOMAIN_NAME;
	SendMailFromSupport($body,$emailAddress,$header);
	
	return "success";
}

function SendPSIGateWayEmail($emailAddress,$body)
{
	$header = "Dear User";
	$body = "$body
					
	if you have not done this activity please let us know at ".SUPPORT_ADDRESS." so that we can resolve this matter immediately
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}
function SendMonerisGateWayEmail($emailAddress,$body)
{
	$header = "Dear User";
	$body = "$body
					
	if you have not done this activity please let us know at ".SUPPORT_ADDRESS." so that we can resolve this matter immediately
	
	Regards,
	
	".DOMAIN_NAME." Team";

	SendMailFromSupport(nl2br($body),$emailAddress,$header);
}
	
?>