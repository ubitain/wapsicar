
<?php
traceMessage("add Customer Info in Action".print_r_log($_REQUEST));
require_once(MODULE . "/class/bllayer/customer.php");
 $blcustomer=new BL_Customer();
 $customerInfo = array();
 $customerId = $_REQUEST['customer_id'];
 $customerInfo['customer_name']= $_REQUEST['customer_name'];
 $customerInfo['company_name']= $_POST['company_name'];
 $customerInfo['contact_number']= $_REQUEST['contact_number'];
 $customerInfo['email_address']= $_REQUEST['email_address'];
 $customerInfo['customer_type']= $_REQUEST['customer_type'];
 $customerInfo['address']= $_REQUEST['autocomplete'];
 //get lat long
 $addressInfo=getLatitudeLongitude($_REQUEST['autocomplete']);
 // traceMessage("Address Info ".print_r_log($addressInfo));
 $customerInfo['latitude']= $addressInfo['lat'];
 $customerInfo['longitude']= $addressInfo['long'];

 if (empty($customerId)) {
     $blcustomer->AddCustomer($customerInfo);
 }
 else {
     $blcustomer->UpdateCustomer($customerInfo,$customerId);
 }
?>
