<?
	require_once(MODULE . "/class/bllayer/claims.php");	
    require_once(MODULE."/class/bllayer/viewclaims.php");

	traceMessage(print_r_log($_REQUEST));
	$info = $_REQUEST;
	$blAdd = new BL_AddPurchaseFromAccountOff();
	$viewClaims=new BL_ViewClaims();
	$addstuff = array();



	$claimID = $info['claimID'];
	$claimNo = $info['claimNo'];
	$purchaseFromStatusByForm = $info['PurchaseFromStatusByForm'];
	$PurchaseFromStatusByFormAccount = $info['PurchaseFromStatusByFormAccount'];
	$claimerIdHidden = $info['ClaimerIdHidden'];

	
	$getUserDetail = $viewClaims->FetchDealerName($claimerIdHidden);
    $claimerName = $getUserDetail->rows[0]['full_name'];
    $claimerEmail = $getUserDetail->rows[0]['email'];
    $androidPushId = $getUserDetail->rows[0]['android_push_id'];
    $applePushId = $getUserDetail->rows[0]['apple_push_id'];


	$purchasedFromHidden = $info['purchasedFromHidden'];
	$getPurchaseFromDetail = $viewClaims->FetchDealerName($purchasedFromHidden);
	$purchaseFromName = $getPurchaseFromDetail->rows[0]['full_name'];


if ($purchaseFromStatusByForm=="accept") {
		$data1 = array();
		$data1['status'] = "accept";
		 

		$getMaxTransLog = $blAdd->GetMaxTransLog($claimID);
		$claimTransctionsLogId = $getMaxTransLog->rows[0]['claimTransctionsLogId'];

		$getMaxTrans = $blAdd->GetMaxTrans($claimID);
		$claimTransctionsId = $getMaxTrans->rows[0]['claimTransctionsId'];
		
// traceMessage("usama is i".$claimID."usama is the champ!".$claimTransctionsLogId."!".$claimTransctionsId);

		$blAdd->UpdatePurchaseFrom($data1,$claimTransctionsId);
		$blAdd->UpdatePurchaseFromLog($data1,$claimTransctionsLogId);

		$getMaxTransUser = $blAdd->GetMaxTransUser($claimTransctionsId);
		$claimTransctionsUserId = $getMaxTransUser->rows[0]['user_id'];
		$updatePurchaseFromInClaim = array();
		$updatePurchaseFromInClaim['purchased_from'] = $claimTransctionsUserId;

		$blAdd->UpdatePurchaseFromClaim($updatePurchaseFromInClaim,$claimID);

		$getMaxTransAccountClaim = $blAdd->GetMaxTransAccountClaim($claimID);
		$claimTransctionsAccountOff = $getMaxTransAccountClaim->rows[0]['account_of'];

		if ($claimTransctionsAccountOff!="" ) {
			if ( $claimTransctionsAccountOff!="0") {
			$blAdd->UpdateStatusFromClaim($claimID);
			}
		}

		
		// $message = "Dear $claimerName, $purchaseFromName Your claim ($claimNo) have been accepted!";
		
		//  notifications($claimerIdHidden,$claimerEmail,$androidPushId,$applePushId,$message,$claimID);
		// $getMaxTransAccount = $blAdd->GetMaxTransAccount($claimID);
		// $countGetMaxTransAccount =  $getMaxTransAccount->count;
		// if ($countGetMaxTransAccount>0) {
		// 	$claimTransctionsAccountId = $getMaxTransAccount->rows[0]['claimTransctionsAccountId'];
		// 	$getMaxTransAccountStatus = $blAdd->GetMaxTransAccountStatus($claimID);
		// 	$getMaxAccountStatus = $getMaxTransAccountStatus->rows[0]['claimTransctionsAccountStatus'];
		// 	if ($getMaxAccountStatus=="accept") {
				
		// 	}
		// }
}
else if ($purchaseFromStatusByForm=="change_purchased_from")
{

		// $message = "Dear $claimerName, $purchaseFromName Purchase From against your claim is changed by !";
		// notifications($claimerIdHidden,$claimerEmail,$androidPushId,$applePushId,$message,$claimID);

		

		$dataChangeStatus = array();
		$dataChangeStatus['status'] = "change_purchased_from";
		$getMaxTrans = $blAdd->GetMaxTrans($claimID);
		$claimTransctionsId = $getMaxTrans->rows[0]['claimTransctionsId'];
		$getMaxTransLog = $blAdd->GetMaxTransLog($claimID);
		$claimTransctionsLogId = $getMaxTransLog->rows[0]['claimTransctionsLogId'];
		$blAdd->UpdatePurchaseFrom($dataChangeStatus,$claimTransctionsId);
		$blAdd->UpdatePurchaseFromLog($dataChangeStatus,$claimTransctionsLogId);

		$str=$info['PurchaseSelect'];
		$arr=explode(',',$str);
		$purchase = $arr[0];
		$purchaseUserId = $arr[1];

		$getNewPurchaseUserDetail = $viewClaims->FetchDealerName($purchaseUserId);
	    $purchaserName = $getNewPurchaseUserDetail->rows[0]['full_name'];
	    $purchaserEmail = $getNewPurchaseUserDetail->rows[0]['email'];
	    $androidPushIdPurchaser = $getNewPurchaseUserDetail->rows[0]['android_push_id'];
	    $applePushIdPurchaser = $getNewPurchaseUserDetail->rows[0]['apple_push_id'];

	 //    $messages = "Dear $purchaserName, $purchaserName  !";
		// notifications($purchaseUserId,$purchaserEmail,$androidPushIdPurchaser,$applePushIdPurchaser,$messages,$claimID);
		
		$dataPurchase = array();
		$dataPurchase['claim_id'] = $claimID;
		$dataPurchase['user_id'] = $purchaseUserId;
		$dataPurchase['assigned_to'] = "purchase";
		$dataPurchase['status'] = "pending";
		$dataPurchase['created_date'] = date("Y-m-d H:i:s");
		$blAdd->AddPurchaseFromhis($dataPurchase);
		$blAdd->AddPurchaseFromhislog($dataPurchase);
		
}
else if ($purchaseFromStatusByForm=="rejected")
{
		// $message = "Dear $claimerName, $purchaseFromName Your claim have been rejected!";
		// notifications($claimerIdHidden,$claimerEmail,$androidPushId,$applePushId,$message,$claimID);

		$dataChangeStatus = array();
		$dataChangeStatus['status'] = "rejected";
		$getMaxTransLog = $blAdd->GetMaxTransLog($claimID);
		$claimTransctionsLogId = $getMaxTransLog->rows[0]['claimTransctionsLogId'];
		$blAdd->DeletePurchaseFrom($claimID);
		$blAdd->UpdatePurchaseFromLog($dataChangeStatus,$claimTransctionsLogId);
}



if ($PurchaseFromStatusByFormAccount=="accept") {
		$accountOffHidden = $info['accountOffHidden'];
		$data1 = array();
		$data1['status'] = "accept";

		$getMaxTransLogAccount = $blAdd->GetMaxTransAccountLog($claimID);
		$claimTransctionsAccountIdLog = $getMaxTransLogAccount->rows[0]['claimTransctionsAccountIdLog'];

		$getMaxTransAccount = $blAdd->GetMaxTransAccount($claimID);
		$claimTransctionsAccountId = $getMaxTransAccount->rows[0]['claimTransctionsAccountId'];
		
// traceMessage("usama is i".$claimID."usama is the champ!".$claimTransctionsLogId."!".$claimTransctionsId);

		$blAdd->UpdatePurchaseFrom($data1,$claimTransctionsAccountId);
		$blAdd->UpdatePurchaseFromLog($data1,$claimTransctionsAccountIdLog);

		$getMaxTransUser = $blAdd->GetMaxTransUser($claimTransctionsAccountId);
		$claimTransctionsUserId = $getMaxTransUser->rows[0]['user_id'];
		$updatePurchaseFromInClaim = array();
		$updatePurchaseFromInClaim['account_of'] = $claimTransctionsUserId;

		$blAdd->UpdatePurchaseFromClaim($updatePurchaseFromInClaim,$claimID);

		$getMaxTransPurchaseClaim = $blAdd->GetMaxTransPurchaseClaim($claimID);
		$claimTransctionsPurchase = $getMaxTransPurchaseClaim->rows[0]['purchased_from'];

		if ($claimTransctionsPurchase!="" ) {
			if ( $claimTransctionsPurchase!="0") {
			$blAdd->UpdateStatusFromClaim($claimID);
			}
		}
		// $getMaxTransAccount = $blAdd->GetMaxTransAccount($claimID);
		// $countGetMaxTransAccount =  $getMaxTransAccount->count;
		// if ($countGetMaxTransAccount>0) {
		// 	$claimTransctionsAccountId = $getMaxTransAccount->rows[0]['claimTransctionsAccountId'];
		// 	$getMaxTransAccountStatus = $blAdd->GetMaxTransAccountStatus($claimID);
		// 	$getMaxAccountStatus = $getMaxTransAccountStatus->rows[0]['claimTransctionsAccountStatus'];
		// 	if ($getMaxAccountStatus=="accept") {
				
		// 	}
		// }
}
else if ($PurchaseFromStatusByFormAccount=="rejected")
{
		$dataChangeStatus = array();
		$dataChangeStatus['status'] = "rejected";
		$getMaxTransLogAccount = $blAdd->GetMaxTransAccountLog($claimID);
		$claimTransctionsAccountIdLog = $getMaxTransLogAccount->rows[0]['claimTransctionsAccountIdLog'];

		$blAdd->DeleteAccountOff($claimID);
		$blAdd->UpdateAccountOffFromLog($dataChangeStatus,$claimTransctionsAccountIdLog);
}


function notifications($userId,$userEmail,$androidPushId,$applePushId,$message,$claimID)
{
    traceMessage("FCM NOTIFICATION FIRST STEP!!!");

    $inserttime = date('Y-m-d H:i:s');
    $notificationtype = NOTIFICATION_GENERAL;
    $data_notify = array();
    $db = array('user_id' => $userId,
              'reference_id' => $claimID,
              'title' => 'Claim Alert!',
              'type' => $notificationtype,
              'message' => $message,
              'creation_time' => $inserttime,
              'email_address' => $userEmail,
              'android_push_id' => $androidPushId,
              'apple_push_id' => $applePushId,
              'status' => 1,
    );
    $msg = array (
          'body' 	=> $message,
          'title'	=> 'Claim Alert!',
          'type'	=> $notificationtype,
          'icon'	=> 'myicon',
          'sound' => 'mySound'
    );
    $data_notify['id'] = $androidPushId;
    $data_notify['msg'] = $msg;
    $data_notify['db'] = $db;
    traceMessage("FCM NOTIFICATION".print_r_log($data_notify));
    $result = fcm_send_notification($data_notify);
    traceMessage('FCM RETURN;'.print_r_log($result));
    $temp     = array(
          "header" => array(
              "success" => "0",
              "message" => "Notification Sent!",
              "token" => $token
          ),
          "body" => $obj
      );
      $response = json_encode($temp);
      echo $response;
}
?>

