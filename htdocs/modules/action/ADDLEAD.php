<?php
traceMessage("add lead form Info".print_r_log($_REQUEST));
// CRM BL Layer
require_once(MODULE . "/class/bllayer/crm.php");
// Customer BL Layer
require_once(MODULE . "/class/bllayer/customer.php");
// User BL Layer
require_once(MODULE . "/class/bllayer/user.php");

$blcustomer = new BL_Customer();
$blcrm = new BL_Crm();
$bluser = new BL_User();


$data = array();

$data['customer_name']= $_REQUEST['customer_name'];
$data['company_name']= $_REQUEST['company_name'];
$data['contact_number']= $_REQUEST['contact_number'];
$data['email_address']= $_REQUEST['email_address'];
$data['customer_type']= 'CRM';

$data['address']= $_REQUEST['address'];
//Get latitude and longitude from autocomplete google given location
$addressInfo=getLatitudeLongitude($_REQUEST['autocomplete']);
$data['latitude']= $addressInfo['lat'];
$data['longitude']= $addressInfo['long'];



$existingCustomer = $blcrm->GetCustomerIdByPhoneNumber($data['contact_number']);
traceMessage("Existing customer ".print_r_log($existingCustomer->rows));
foreach ($existingCustomer->rows as $key => $value) {
    if ($key == 'customer_id' && !empty($value['customer_id'])) {
        $data['customer_id']= $value['customer_id'];
        break;
    }
}
$leadId = "";
if (!empty($data['customer_id'])) {
    traceMessage("Customer Already Exist with Id:".$data['customer_id']);
    unset($data['customer_type']);
    $data['nearby_location']= $_REQUEST['autocomplete'];
    $data['create_at']= date('Y-m-d H:i:s',strtotime("now"));
    $data['update_at']= date('Y-m-d H:i:s',strtotime("now"));
    $data['nic']= $_REQUEST['nic'];
    $data['lead_type']= $_REQUEST['lead_type'];
    $data['city']= $_REQUEST['city'];
    $data['officer_name']= $_REQUEST['officer_name'];
    $data['call_time']= $_REQUEST['call_time'];
    $data['status']= "PENDING";
    $data['monthly_income']= empty($_REQUEST['monthly_income']) ? NULL:$_REQUEST['monthly_income'];
    $data['nearest_branch']= empty($_REQUEST['nearest_branch']) ? NULL:$_REQUEST['nearest_branch'];
    $data['car_name']= empty($_REQUEST['car_name']) ? NULL:$_REQUEST['car_name'];
    $data['motorcycle_name']= empty($_REQUEST['motorcycle_name']) ? NULL:$_REQUEST['motorcycle_name'];
    $data['fund_type']= empty($_REQUEST['fund_type']) ? NULL:$_REQUEST['fund_type'];
    $data['branch']= empty($_REQUEST['branch']) ? NULL:$_REQUEST['branch'];
    $data['ac_certificate']= empty($_REQUEST['ac_certificate']) ? NULL:$_REQUEST['ac_certificate'];
    $data['remarks']= empty($_REQUEST['remarks']) ? NULL:$_REQUEST['remarks'];
    $data['package_days']= empty($_REQUEST['package_days']) ? NULL:$_REQUEST['package_days'];
    $data['product']= empty($_REQUEST['product']) ? NULL:$_REQUEST['product'];
    $data['tractor_model']= empty($_REQUEST['tractor_model']) ? NULL:$_REQUEST['tractor_model'];

    if (!empty($data['nic'])) {
        $param = array();
        $param['nic'] =$data['nic'];
        $leadInfo = $blcrm->GetLeadIdByNic($param);
        $leadId = $leadInfo->rows[0]['lead_id'];
    }
    if (empty($leadId)) {
        $leadId = $blcrm->AddLead($data);
    }
    $param = array();
    $param['customer_id'] =$data['customer_id'];
    traceMessage("param here ".print_r_log($param));
    $usersIdInfo = $blcrm->GetUserIdByCustomerId($param);
    traceMessage("Existing user mapping ".print_r_log($usersIdInfo->rows));
    foreach ($usersIdInfo->rows as $key => $value) {
        if (!empty($leadId) && !empty($value['user_id'])) {
            $params = array();
            $params['user_id'] = $value['user_id'];
            $params['lead_id'] = $leadId;
            $params['status'] = "PENDING";
            $params['create_at']= date('Y-m-d H:i:s',strtotime("now"));
            $params['update_at']= date('Y-m-d H:i:s',strtotime("now"));
            $assignUsersLeadRecordId = $blcrm->AssignUsersToLeadId($params);
            // Notification Sent
            $userdata = $bluser->GetUserInfo($value['user_id']);
            $inserttime = date('Y-m-d H:i:s');
            $notificationtype = 1;
            $data = array();
            $db = array('user_id' => $userdata->rows[0]['user_id'],
                        'reference_id' => $leadId,
                        'title' => 'New Lead',
                        'type' => $notificationtype,
                        'message' => "Please accept the lead",
                        'creation_time' => $inserttime,
                        'email_address' => $userdata->rows[0]['email_address'],
                        'android_push_id' => $userdata->rows[0]['android_push_id'],
                        'apple_push_id' => $userdata->rows[0]['apple_push_id'],
                        'status' => 1,
            );
            $msg = array (
                    'body' 	=> 'Please accept the lead"',
                    'title'	=> 'New Lead',
                    'type'	=> $notificationtype,
                    'reference_id' => $leadId,
                    'icon'	=> 'myicon',
                    'sound' => 'mySound'
            );
            $data['id'] = $userdata->rows[0]['android_push_id'];
            $data['msg'] = $msg;
            $data['db'] = $db;
            traceMessage("FCM NOTIFICATION".print_r_log($data));
            $result = fcm_send_notification($data);
            traceMessage('FCM RETURN;'.print_r_log($result));
        }
        break;
    }
}else {
    //For New Lead not exist in the system
    $customerId = $blcustomer->AddCustomer($data);
    unset($data['customer_type']);
    $data['nearby_location']= $_REQUEST['autocomplete'];
    $data['create_at']= date('Y-m-d H:i:s',strtotime("now"));
    $data['update_at']= date('Y-m-d H:i:s',strtotime("now"));
    $data['customer_id'] = $customerId;
    $data['nic']= $_REQUEST['nic'];
    $data['lead_type']= $_REQUEST['lead_type'];
    $data['city']= $_REQUEST['city'];
    $data['officer_name']= $_REQUEST['officer_name'];
    $data['call_time']= $_REQUEST['call_time'];
    $data['status']= "PENDING";
    $data['monthly_income']= empty($_REQUEST['monthly_income']) ? NULL:$_REQUEST['monthly_income'];
    $data['nearest_branch']= empty($_REQUEST['nearest_branch']) ? NULL:$_REQUEST['nearest_branch'];
    $data['car_name']= empty($_REQUEST['car_name']) ? NULL:$_REQUEST['car_name'];
    $data['motorcycle_name']= empty($_REQUEST['motorcycle_name']) ? NULL:$_REQUEST['motorcycle_name'];
    $data['fund_type']= empty($_REQUEST['fund_type']) ? NULL:$_REQUEST['fund_type'];
    $data['branch']= empty($_REQUEST['branch']) ? NULL:$_REQUEST['branch'];
    $data['ac_certificate']= empty($_REQUEST['ac_certificate']) ? NULL:$_REQUEST['ac_certificate'];
    $data['remarks']= empty($_REQUEST['remarks']) ? NULL:$_REQUEST['remarks'];
    $data['package_days']= empty($_REQUEST['package_days']) ? NULL:$_REQUEST['package_days'];
    $data['product']= empty($_REQUEST['product']) ? NULL:$_REQUEST['product'];
    $data['tractor_model']= empty($_REQUEST['tractor_model']) ? NULL:$_REQUEST['tractor_model'];

    if (!empty($data['nic'])) {
        $param = array();
        $param['nic'] =$data['nic'];
        $leadInfo = $blcrm->GetLeadIdByNic($param);
        $leadId = $leadInfo->rows[0]['lead_id'];
    }
    if (empty($leadId)) {
        $leadId = $blcrm->AddLead($data);
    }
    $nearbyUsersInfo = $blcrm->NearCustomerUserId();
    traceMessage("hello here".print_r_log($nearbyUsersInfo->rows));
    $distanceRadius = LEAD_DISTANCE_DIFFERENCE;
    foreach ($nearbyUsersInfo->rows as $key => $value) {
        $userLat = $value['latitude'];
        $userLong = $value['longitude'];
        $distanceDifference=DistanceDifference($userLat, $userLong, $data['latitude'], $data['longitude'], "K");
        if($distanceDifference <= $distanceRadius)
        {
            // Insert all the user_id in lead_users table
            $params = array();
            $params['user_id'] = $value['user_id'];
            $params['lead_id'] = $leadId;
            $params['status'] = "PENDING";
            $params['create_at']= date('Y-m-d H:i:s',strtotime("now"));
            $params['update_at']= date('Y-m-d H:i:s',strtotime("now"));
            if (!empty($leadId) && !empty($value['user_id'])) {
                $assignUsersLeadRecordId = $blcrm->AssignUsersToLeadId($params);
                // Notification Sent
                $userdata = $bluser->GetUserInfo($value['user_id']);
        		$inserttime = date('Y-m-d H:i:s');
        		$notificationtype = 1;
        		$data = array();
        		$db = array('user_id' => $userdata->rows[0]['user_id'],
                            'reference_id' => $leadId,
        					'title' => 'New Lead',
        					'type' => $notificationtype,
        					'message' => "Please accept the lead",
        					'creation_time' => $inserttime,
        					'email_address' => $userdata->rows[0]['email_address'],
        					'android_push_id' => $userdata->rows[0]['android_push_id'],
        					'apple_push_id' => $userdata->rows[0]['apple_push_id'],
        					'status' => 1,
        		);
        		$msg = array (
        		   		'body' 	=> 'Please accept the lead"',
        				'title'	=> 'New Lead',
        		   		'type'	=> $notificationtype,
                        'reference_id' => $leadId,
        		        'icon'	=> 'myicon',
        		        'sound' => 'mySound'
        		);
        		$data['id'] = $userdata->rows[0]['android_push_id'];
        		$data['msg'] = $msg;
        		$data['db'] = $db;
        		traceMessage("FCM NOTIFICATION".print_r_log($data));
        		$result = fcm_send_notification($data);
                traceMessage('FCM RETURN;'.print_r_log($result));
            }

        }
    }
}


?>
