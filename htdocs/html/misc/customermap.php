<?php
	set_time_limit(0);
	require_once('../../modules/utility/initialization.php');
	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if(!$con)
	{
		die("could not connect ".mysqli_error());
	}
	$selQuery = "select c.customer_id,c.customer_name,c.latitude,c.longitude,c.address from customer c where c.latitude!='' and c.longitude!='' limit 2000";
	$rsSel=mysqli_query($con,$selQuery);
	$countSel=mysqli_num_rows($rsSel);
	$ch = $jsonStr = "";
	for($i=0;$i<$countSel;$i++)
	{
		$data=mysqli_fetch_row($rsSel);
		$jsonStr.=$ch."[\"$data[1]\",$data[2],$data[3],'']";
		$ch=",";
	}
	$jsonStr="[".$jsonStr."]";
?>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-sooax1EiXpeAASfGxhn7e19kgog55To">
</script>
<script>
	var markersArray =[];
	var markers =<?=$jsonStr?>;
	var myCenter=new google.maps.LatLng(29.520627, 69.809232);
	function initializeMaps() {
		var myOptions = {
			zoom: 6,
			center: myCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false
		};

		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var infowindow = new google.maps.InfoWindow(), marker, i;
		console.log(markers.length);
		for (i = 0; i < markers.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markers[i][1], markers[i][2]),
				// icon: image,
				map: map

			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markers[i][0]+'  '+markers[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}


		function clearOverlays() {
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];
		}

	}
	google.maps.event.addDomListener(window, 'load', initializeMaps);
</script>

<div id="map_canvas"  style="height: 97%; width: 84%;position: absolute;"></div>
