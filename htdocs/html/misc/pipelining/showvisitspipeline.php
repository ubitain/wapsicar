<?php
require_once("../../globalpath.php");
require_once(MODULE . "/utility/initialization.php");
require_once(MODULE . "/utility/standard_library.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */


traceMessage("request data ".print_r_log($_REQUEST));

$pipeLineData = json_decode($_REQUEST['pipeLineData'],true);



// DB table to use
$table = 'visits';

// Table's primary key
$primaryKey = 'visit_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

$visit_status = "";
$visit_status = $pipeLineData['visit_status'];
$childId = $pipeLineData['child_id'];

$sqlExtra = "";
if ($pipeLineData['from_date'] && $pipeLineData['to_date'] ){
    $fromDate = $pipeLineData['from_date'];
    $toDate = $pipeLineData['to_date'];
    $sqlExtra= " AND date(`visited_time`) between '$fromDate' AND '$toDate'";
}

// print_r($childId);exit();

// $sql = "SELECT * FROM visits v LEFT JOIN visit_survey vs ON v.`visit_id`=vs.`visit_id`,customer c,users u WHERE v.customer_id = c.customer_id AND v.user_id = u.user_id
// AND v.user_id IN ($childId) $sqlExtra group by vs.visit_id";



$columns = array(
    array( 'db' => '`c`.`customer_name`','dt' => 0,  'field'   => 'customer_name','formatter'=>function($d,$row){return ucfirst($d);}),
    array( 'db' => '`u`.`first_name`',   'dt' => 1,  'field'   => 'first_name','formatter'=>function($d,$row){return ucfirst($d);}),
    array( 'db' => '`u`.`user_type`',    'dt' => 2, 'field'   => 'user_type', 'formatter' => function( $d, $row ) {return user_type_decoder($d);}),
    array( 'db' => '`v`.`visit_type`',   'dt' => 3, 'field'   => 'visit_type','formatter' => function($d,$row){return ucwords($d);}),
    array( 'db' => '`v`.`visited_time`',
           'dt' => 4,
           'field'  => 'visited_time',
           'formatter' => function($d,$row){                                            
                                            return date('h:i A', strtotime($d));
                                            }
        ),  
   
);

// SQL server connection information
$sql_details = array(
    'user' => DESKTOPDBUSERNAME,
    'pass' => DESKTOPDBPASSWORD,
    'db'   => DESKTOPDBDATABASE,
    'host' => DESKTOPDBHOSTNAME
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
 $joinQuery = "	FROM visits v LEFT JOIN visit_survey vs ON v.`visit_id`=vs.`visit_id`,customer c,users u ";
 $extraWhere = "v.customer_id = c.customer_id AND v.user_id = u.user_id AND v.user_id IN ($childId) $sqlExtra ";
 $groupBy = " vs.visit_id ";
 echo json_encode(
 	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere,$groupBy)
 );
exit;
