<?php
require_once("../../globalpath.php");
require_once(MODULE . "/utility/initialization.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'users';

// Table's primary key
$primaryKey = 'user_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

    // SELECT u.*, v.first_name as pfname, v.last_name as plname, v.emp_code as pempcode
    // from users u Left JOIN user_mapping um on um.child_id=u.user_id
    // LEFT JOIN users v on um.parent_id=v.user_id
    // where u.user_type <> '1' and u.status=1
    // order by u.user_type DESC

$columns = array(
    array( 'db' => '`u`.`login_id`',       'dt' => 0,  'field'   => 'login_id'),
    array( 'db' => '`u`.`password`',       'dt' => 1,  'field'   => 'password'),
    array( 'db' => '`u`.`first_name`',       'dt' => 2, 'field'   => 'first_name'),
    array( 'db' => '`u`.`user_type`',
            'dt' => 3,
            'field'   => 'user_type',
            'formatter' => function( $d, $row ) {                            
                $user_type = '';
                if ($d == "1") {
                    $user_type = "Admin";
                }
                elseif ($d == "2") {
                    $user_type = "National Sales Manager";
                }
                elseif ($d == "4") {
                    $user_type = "Regional Sales Manager";
                }
                elseif ($d == "8") {
                    $user_type = "Area Sales Manager";
                }
                elseif ($d == "16") {
                    $user_type = "Team Leader";
                }
                elseif ($d == "32") {
                    $user_type = "Business Development Officer";
                }
                else {
                    $user_type = $d;
                }
                return $user_type;
            }
    ),
    array( 'db' => '`u`.`email_address`',        'dt' => 4,  'field'   => 'email_address'),
    array( 'db' => '`v`.`first_name`',        'dt' => 5,  'as' => 'report_to' ,'field'   => 'first_name'),
   
    array(
        'db'        => '`u`.`user_id`',
        'dt'        => 6,
        'field'     => 'user_id',
        'formatter' => function( $d, $row ) {
            return '<div class="btn-group btn-group-sm">
                <a href="#" onClick="LoadAjaxScreen(\'userdetail&user_id='.$d.'\')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="View"><i class="fa fa-eye"></i></a>
                <a href="#" onClick="LoadAjaxScreen(\'edituser&user_id='.$d.'\')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="View"><i class="fa fa-pencil-square-o"></i></a>
                <a href="#" onClick="DeleteUser(\''.$d.'\')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="Delete"><i class="fa fa fa-trash-o"></i></a>
            </div>';
        }
    )
);

// SQL server connection information
$sql_details = array(
    'user' => DESKTOPDBUSERNAME,
    'pass' => DESKTOPDBPASSWORD,
    'db'   => DESKTOPDBDATABASE,
    'host' => DESKTOPDBHOSTNAME
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
 $joinQuery = "FROM users u 
                Left JOIN user_mapping um on um.child_id = u.user_id
                LEFT JOIN users v on um.parent_id = v.user_id";
 $extraWhere = " u.user_type <> '1' and u.status=1 ";
 echo json_encode(
 	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
 );
exit;
