<?php
require_once("../../globalpath.php");
require_once(MODULE . "/utility/initialization.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'customer';

// Table's primary key
$primaryKey = 'customer_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => '`c`.`customer_name`',       'dt' => 0,  'field'   => 'customer_name',),
    // array( 'db' => '`c`.`email_address`',       'dt' => 1,  'field'   => 'email_address',),
    array( 'db' => '`c`.`customer_type`',
            'dt' => 1,
            'field'   => 'customer_type',
            'formatter' => function( $d, $row ) {
                $customer_type = '';
                if ($d == "CUSTOMER") {
                    $customer_type = "ETB";
                }
                elseif ($d == "LEAD") {
                    $customer_type = "NTB";
                }
                else {
                    $customer_type = $d;
                }
                return $customer_type;
            }
    ),
    // array( 'db' => '`c`.`address`',             'dt' => 2,  'field'   => 'address',),
    array( 'db' => '`cd`.`branch_name`',        'dt' => 2,  'field'   => 'branch_name',),
    array( 'db' => '`cd`.`account_number`',     'dt' => 3,  'field'   => 'account_number',),
    array(
        'db'        => '`cd`.`account_balance`',
        'dt'        => 4,
        'field'     => 'account_balance',
        'formatter' => function( $d, $row ) {
            return number_format($d,2);
        }
    ),
    array(
        'db'        => '`c`.`customer_id`',
        'dt'        => 5,
        'field'     => 'customer_id',
        'formatter' => function( $d, $row ) {
            return '<div class="btn-group btn-group-sm">
                <a href="#" onClick="LoadAjaxScreen(\'customerdetail\'+\'&customer_id=\'+'.$d.')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="View"><i class="fa fa-eye"></i></a>
                <a href="#" onClick="LoadAjaxScreen(\'editcustomer\'+\'&customer_id=\'+'.$d.')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="View"><i class="fa fa-pencil-square-o"></i></a>
                <a href="#" onClick="DeleteCustomer(\''.$d.'\')" data-toggle="tooltip" title="" class="btn btn-xs btn-primary" data-original-title="Delete"><i class="fa fa fa-trash-o"></i></a>
            </div>';
        }
    )
);

// SQL server connection information
$sql_details = array(
    'user' => DESKTOPDBUSERNAME,
    'pass' => DESKTOPDBPASSWORD,
    'db'   => DESKTOPDBDATABASE,
    'host' => DESKTOPDBHOSTNAME
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
 $joinQuery = "FROM `customer` as `c` left join `customer_details` as `cd` on `c`.`customer_id`=`cd`.`customer_id`";
 $extraWhere = "`c`.`status` >= 1";
 echo json_encode(
 	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
 );
exit;
