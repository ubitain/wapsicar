<?php
require_once("../../globalpath.php");
require_once(MODULE . "/utility/initialization.php");
require_once(MODULE . "/utility/standard_library.php");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'customer';

// Table's primary key
$primaryKey = 'customer_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes

//select c.*,cd.branch_name,cd.account_number,cd.account_balance from customer c left join customer_details cd on c.customer_id=cd.customer_id where 
// c.status='$status' order by c.customer_id desc limit 5000

// account_balance
// account_number
// branch_name

$columns = array(
    array( 'db' => '`c`.`customer_name`',       'dt' => 0,  'field'   => 'customer_name','formatter'=>function($d,$row){return ucfirst($d);}),
    array( 'db' => '`c`.`customer_type`',
            'dt' => 1,
            'field'   => 'customer_type',
            'formatter' => function( $d, $row ) {
                $customer_type = '';
                if ($d == "CUSTOMER") {
                    $customer_type = "ETB";
                }
                elseif ($d == "LEAD") {
                    $customer_type = "NTB";
                }
                else {
                    $customer_type = $d;
                }
                return $customer_type;
            }
    ),
    array( 'db' => '`c`.`contact_number`',       'dt' => 2, 'field'   => 'contact_number'),
    array( 'db' => '`c`.`email_address`',       'dt' => 3, 'field'   => 'email_address' ),
    array( 'db' => '`c`.`address`',       'dt' => 4, 'field'   => 'address' ,'formatter' => function($d,$row){return add3dots($d);}	),       
    array(
        'db'        => '`c`.`balance`',
        'dt'        => 5,
        'field'     => 'balance',
        'formatter' => function( $d, $row ) {
            return number_format($d,2);
        }
    ),
    array( 'db' => '`cd`.`branch_name`',       'dt' => 6, 'field'   => 'branch_name'),           
    array( 'db' => '`c`.`account_num`',       'dt' => 7, 'field'   => 'account_num'),           
   
);

// SQL server connection information
$sql_details = array(
    'user' => DESKTOPDBUSERNAME,
    'pass' => DESKTOPDBPASSWORD,
    'db'   => DESKTOPDBDATABASE,
    'host' => DESKTOPDBHOSTNAME
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
 $joinQuery = "	FROM `customer` as `c` left join customer_details cd on c.customer_id=cd.customer_id ";
  $extraWhere = " c.status='1' ";
 echo json_encode(
 	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
 );
exit;
