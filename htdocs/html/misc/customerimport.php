<?php
	set_time_limit(0);
	define("MODULE","/var/www/meezandev.uhfsolutions.com/htdocs/modules");
	require_once(MODULE . "/utility/configuration.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/utility/initialization.php");
	require_once(MODULE . "/class/datalayer/dataaccess.php");
	require_once("simplexlsx.class.php");

	$xlsx = new SimpleXLSX('Customer_Data.xlsx');
	foreach( $xlsx->rows() as $k=>$r ) {
        if ($k == 0)
            continue;

		if($r[5] == "TitleofAccount")
			continue;

		if($r[3] == "TEAM LEADER")
			continue;

		$db = new DataAccessBase();
		$db->Connect('mysql');

		$branchCode=mysql_escape_string($r[1]);
		$branchCode2=mysql_escape_string($r[2]);
		$branchName=mysql_escape_string($r[3]);
		$accountNumber=mysql_escape_string($r[4]);
		$clientId=mysql_escape_string($r[5]);
		$accountTitle=mysql_escape_string($r[6]);
		$currency=mysql_escape_string($r[7]);
		$relManager=mysql_escape_string($r[8]);
		$bdoCode=mysql_escape_string($r[9]);
		$accountBalance=mysql_escape_string($r[10]);
		$avgBalance=mysql_escape_string($r[11]);
		$accountOpeningDate=mysql_escape_string($r[12]);
		$customerOpeningDate=mysql_escape_string($r[13]);
		$accountCategory=mysql_escape_string($r[14]);
		$categoryDescription=mysql_escape_string($r[15]);
		$productCategory=mysql_escape_string($r[16]);
		$idNumber=mysql_escape_string($r[17]);
		$sector=mysql_escape_string($r[18]);
		$sectorDesc=mysql_escape_string($r[19]);
		$businessSegment=mysql_escape_string($r[20]);
		$street=mysql_escape_string($r[21]);
		$townCountry=mysql_escape_string($r[22]);
		$postCode=mysql_escape_string($r[23]);
		$country=mysql_escape_string($r[24]);
		$customerContactNumber=mysql_escape_string($r[26]);
		$customerContactNumber2=mysql_escape_string($r[27]);
		$dob=mysql_escape_string($r[28]);
		$updateDate=mysql_escape_string($r[29]);
		$emailOne=mysql_escape_string($r[30]);
		$dormat=mysql_escape_string($r[31]);
		$amim=mysql_escape_string($r[33]);
		$ci=mysql_escape_string($r[34]);
		$hf=mysql_escape_string($r[35]);
		$kafalah=mysql_escape_string($r[36]);
		$lpi=mysql_escape_string($r[37]);
		$debitCard=mysql_escape_string($r[38]);
		$internetBanking=mysql_escape_string($r[39]);
		$smsAlerts=mysql_escape_string($r[40]);
        $bdoCode = "0".$bdoCode;
		$selSql="SELECT * FROM users u WHERE u.`login_id`='$bdoCode'";
		$userInfo = $db->ExecuteQuery($selSql);


		$customerType="CUSTOMER";
		$insSql="INSERT IGNORE INTO `meezandev`.`customer`(`customer_id`,`customer_name`,`company_name`,
            `email_address`,`customer_type`,`address`,`contact_number`,`image_url`,
            `latitude`,`longitude`,`account_num`,`balance`,`status`)

			VALUES (NULL,'$accountTitle','$accountTitle','$emailOne','$customerType','$townCountry',
			'$customerContactNumber','','','','$accountNumber','$accountBalance','1')";

		$insRes = $db->ExecuteNonQuery($insSql);

		//$customerId="";
		$customerId=$db->GetInsertId();


		$userId=$userInfo[0]['user_id'];
		if($userId > 0 && $customerId > 0)
		{
			$insMapping="INSERT INTO `meezandev`.`user_customer`
            (`record_id`,`user_id`,`customer_id`,`status`)
			VALUES (NULL,'$userId','$customerId','1')";

			$insMapRs = $db->ExecuteNonQuery($insMapping);

		}
		if($customerId > 0)
		{
			$insDetails="INSERT IGNORE INTO `meezandev`.`customer_details`
			(`customer_detail_id`,`customer_id`,`branch_code`,`branch_code2`,`branch_name`,`account_number`,`client_id`,`account_title`,`currency`,`rel_manager`,`account_balance`,`average_balance`,`account_opening_date`,`customer_open_date`,`account_category`,`category_description`,`product_category`,`id_number`,`sector`,`sector_description`,`business_segment`,`street`,`town_country`,`post_code`,`country`,`contact_number`,`contact_number_second`,`dob`,`update_date`,`email_one`,`dormat`,`anim`,`ci`,`hf`,`kafalah`,`lpt`,`debit_card`,`internet_banking`,`sms_alerts`)
			VALUES (NULL,'$customerId','$branchCode','$branchCode2','$branchName','$accountNumber',
			'$clientId','$accountTitle','$currency','$relManager',
			'$accountBalance','$avgBalance','$accountOpeningDate','$customerOpeningDate',
			'$accountCategory','$categoryDescription','$productCategory','$idNumber','$sector',
			'$sectorDesc','$businessSegment','$street','$townCountry','$postCode','$country',
			'$customerContactNumber','$customerContactNumber2','$dob','$updateDate','$emailOne',
			'$dormat','$anim','$ci','$hf','$kafalah','$lpt','$debitCard','$internetBanking','$smsAlerts')";

			$insDetailsRs = $db->ExecuteNonQuery($insDetails);

		}
		echo $insSql."<br/>";
		echo $insMapping."<br/>";
		//exit();

	}
?>
