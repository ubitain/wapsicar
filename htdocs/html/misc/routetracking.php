<?php
	error_reporting(E_ERROR);
	require_once('../../modules/utility/initialization.php');
	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if(!$con)
	{
		die("could not connect ".mysqli_error());
	}

	$userId=$_REQUEST['user_id'];
	if($userId=="" || !isset($userId))
		die("Invalid User Please Login Again");


	$toDate=date("Y-m-d",strtotime($_REQUEST['toDate']));
	$fromDate=date("Y-m-d",strtotime($_REQUEST['fromDate']));

	$query="select first_name,latitude,longitude,location_time,distance from user_tracking ut,users u
	where ut.user_id=u.user_id and ut.user_id=$userId AND location_time between '$fromDate 00:00:00' and '$toDate 23:59:59' AND latitude!=0 group by latitude order by location_time";
	$rs=mysqli_query($con,$query);
	$count=mysqli_num_rows($rs);
	if($count > 0)
	{
		for($i=0;$i<$count;$i++)
		{
			$row=mysqli_fetch_assoc($rs);
			// if($row['distance'] < 20 || $row['distance'] > 100)
				// continue;
			$markersArray[$i]['title']=$row['first_name'];
			$markersArray[$i]['lat']=$row['latitude'];
			$markersArray[$i]['lng']=$row['longitude'];
			$markersArray[$i]['icon']='../images/iconsm.png';
			$markersArray[$i]['description']=$row['first_name']." on <b>".date("j M Y H:i:s",strtotime($row['location_time']));
		}
	}
	else
	{
		die('no locations found on your desired day');
	}
	if(is_array($markersArray2) && count($markersArray2) > 0 && is_array($markersArray) && count($markersArray) > 0 && is_array($markersArray3) && count($markersArray3) > 0)
		$finalMarkerArray=array_merge($markersArray,$markersArray2,$markersArray3);
	else if(is_array($markersArray2) && count($markersArray2) > 0 && is_array($markersArray) && count($markersArray) > 0)
		$finalMarkerArray=array_merge($markersArray,$markersArray2);
	else if(count($markersArray) > 0)
		$finalMarkerArray=$markersArray;
	else
		die('no locations found on your desired day');

	$finalMarkerArray=array_values($finalMarkerArray);
	$jsonStr=json_encode($finalMarkerArray);

	$jsonStr2=json_encode($markersArray);

?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = <?=$jsonStr?>;
    var startEndPoint = <?=$jsonStr2?>;
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
			zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

	   var infoWindow = new google.maps.InfoWindow();

	  var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();

	for (i = 0; i < markers.length; i++)
		{
			var data = markers[i]
			var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				icon : data.icon ,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);

	//strat End Flag points
	var lat_lng_stratEnd = new Array();
	for (i = 0; i < startEndPoint.length; i++)
		{
				var data1 = startEndPoint[i]
				var myLatlng2 = new google.maps.LatLng(data1.lat, data1.lng);
				lat_lng_stratEnd.push(myLatlng2);


        }

		for (var i = 0; i < lat_lng_stratEnd.length; i++)
		{

			var source = lat_lng_stratEnd[0];
			var destination = lat_lng_stratEnd[lat_lng_stratEnd.length - 1];
		}

		//alert("source"+source)
		//alert("destination"+destination)

	   //******End Strat ENd ******//

	   //***********ROUTING****************//

        //Initialize the Path Array
        var path = new google.maps.MVCArray();

        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();

        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeColor: 'blue' });

        //Loop and Draw Path Route between the Points on MAP
        for (var i = 0; i < lat_lng.length; i++)
		{
            if ((i + 1) < lat_lng.length)
			{
                // if($i%2==0)
				// {
					// continue;
				// }
				var src = lat_lng[i];
				var des = lat_lng[i + 1];


                path.push(src);
                // poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }

				var source1 = lat_lng[0];
				var destination1 = lat_lng[lat_lng.length - 1];

        }

		// alert("doc source "+source1)
		// alert("des destination1"+destination1)

		var image1 = '../images/flag b.png';
		var marker2 = new google.maps.Marker({
                position: source,
                map: map,
				icon : image1 ,

            });

			var image2 = '../images/flag a.png';
		var marker3 = new google.maps.Marker({
                position: destination,
                map: map,
				icon : image2 ,

            });


		 // var image1 = '../images/pin_5.png';
		// var beachMarker1 = new google.maps.Marker({
		// position: {lat: 24.9108962, lng: 670503535000},
		// map: map,
		// icon: image1
	  // });


    }
	// clearMarkers();
</script>
<div id="dvMap" style="height: 97%; width: 84%;position: absolute;">
</div>
