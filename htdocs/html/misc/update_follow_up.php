<?php
	error_reporting(E_ERROR);
	date_default_timezone_set('Asia/Karachi');
	ini_set('max_execution_time', 150); 	
    require_once('../../modules/utility/initialization.php');
    require_once('../../modules/utility/configuration.php');
	require_once('../../modules/utility/standard_library.php');
	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	define("EMERGENCY_BRAKE","/var/wwwlog/advance.uhfsolutions.com/emergencybrake");
    define("IN_PROCESS","/var/wwwlog/advance.uhfsolutions.com/inprocess");
	if(!$con)
	{
		die("could not connect ".mysqli_error());
	}
    if(file_exists(EMERGENCY_BRAKE))
	{
		unlink(IN_PROCESS);
		die("Emergency Brakes Applied");
	}
	if (file_exists(IN_PROCESS))
	{
		$time1= filemtime(IN_PROCESS);
		$time2= time();
		$difference=$time2-$time1;
		if($difference > 3600)
		{
			unlink(IN_PROCESS);
		}
		else
		{
			die("Script Already Running");
		}
	}
	touch(IN_PROCESS);
    $planDate = date('Y-m-d');
    $planDay = date('l');
    $query="SELECT visit_recovery.recovery_id,visits.user_id,visits.customer_id FROM visit_recovery 
    		INNER JOIN visits ON visits.visit_id = visit_recovery.visit_id
    		WHERE visit_recovery.status_id = 2 AND visit_recovery.final_payment_status = 'incomplete'
    		GROUP BY visits.customer_id";
	$rs=mysqli_query($con,$query);
    $count=mysqli_num_rows($rs);
    $visitRecoveryId = array();
	if($count > 0)
	{

		for($i=0;$i<$count;$i++)
		{
            if(file_exists(EMERGENCY_BRAKE))
			{
				unlink(IN_PROCESS);
				die("Emergency Brakes Applied");
			}
			$row=mysqli_fetch_assoc($rs);
            $userId = $row['user_id'];
            $customerId = $row['customer_id'];
            $visitRecoveryId = $row['recovery_id'];
            // echo "<pre>";
            // print_r($row);
            if ( !empty($userId) && !empty($customerId) && !empty($visitRecoveryId) ) {
                $insSql = "INSERT ignore into advance.`visit_plan` (visit_plan_id, user_id, customer_id, plan_date, status) VALUES
                (NULL, '$userId', '$customerId', '$planDate', '1')";
                mysqli_query($con,$insSql);

                // echo $insSql."<br>";

                $updateSql = "update visit_recovery set status_id = '3' where recovery_id =" . $visitRecoveryId;
                mysqli_query($con, $updateSql);
                // echo $updateSql;
            }
		}
        traceMessage("Successfully updated follow up plan");
	}
	else
	{
        unlink(IN_PROCESS);
		die('follow up plan not found');
	}
    unlink(IN_PROCESS);
    
