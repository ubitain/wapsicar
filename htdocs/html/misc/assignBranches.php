<?php
	ini_set('max_execution_time', 600);
	require_once("simplexlsx.class.php");
	define("MODULE", dirname($_SERVER['DOCUMENT_ROOT'])."/modules");
	require_once(MODULE . "/utility/initialization.php");
	require_once(MODULE . "/utility/configuration.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/common/session.php");
	require_once(MODULE . "/class/datalayer/dataaccess.php");
	$con=mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if (mysqli_connect_errno())
	{
		traceMessage("Failed to connect to MySQL: " . mysqli_connect_error());
		exit;
	}

	// traceMessage("Request Data ".print_r_log($_REQUEST));
	// traceMessage("File Array ".print_r_log($_FILES));
	// $time = time();
	// $targetFolderUser = UPLOAD_DIRECTORY_PATH."SalesTeam/";
	// if(!is_dir($targetFolderUser))
	// {
	// 	traceMessage("Create Directory $targetFolderUser");
	// 	mkdir($targetFolderUser);
	// }
	// $targetFolder = $targetFolderUser.$time."/";
	// if(!is_dir($targetFolder))
	// {
	// 	traceMessage("Create Directory $targetFolder");
	// 	mkdir($targetFolder);
	// }
	// $fileName=basename($_FILES['usersheet']['name']);
	// $targetFolder = $targetFolder . $fileName;
	// if(file_exists($targetFolder))
	// {
	// 	traceMessage("file already exists".$targetFolder);
	// 	if(unlink($targetFolder))
	// 	{
	// 		traceMessage("Already exists , unlink and reupload");
	// 		echo "<script>alert('File Already exists');</script>";
	// 	}
	// }
	// else if(move_uploaded_file($_FILES['usersheet']['tmp_name'], $targetFolder))
	// {


		$targetFolder = "Lat Long of Branches -.xlsx";
		// die("string".$targetFolder);
		traceMessage("File Uploaded Successfully");
		$xlsx = new SimpleXLSX($targetFolder);
		echo "<pre>";

		// print_r($xlsx->rows());exit();

		$sheetData = array();
		foreach($xlsx->rows() as $k=>$r){

			$sheetData['branch_name']    	 = $r[1];
			$sheetData['branch_address']	 = $r[2];
			$sheetData['branch_code']		 = $r[3];
			$sheetData['longitude']			 = $r[4];
			$sheetData['latitude']			 = $r[5];
			$sheetData['branch_map']		 = $r[6];
			$sheetData['region']			 = $r[7];
			$sheetData['area']				 = $r[8];
			$sheetData['rsm_name']		 	 = $r[9];
			$sheetData['rsm_code']		 	 = $r[10];
			$sheetData['asm_name']			 = $r[11];
			$sheetData['asm_code']			 = $r[12];

			break;

		}

		$sheetData['branch_name'] = preg_replace('/\s+/', '',		$sheetData['branch_name']);
		$sheetData['branch_address'] = preg_replace('/\s+/', '', 	$sheetData['branch_address']);
		$sheetData['branch_code'] = preg_replace('/\s+/', '', 		$sheetData['branch_code']);
		$sheetData['longitude'] = preg_replace('/\s+/', '', 		$sheetData['longitude']);
		$sheetData['latitude'] = preg_replace('/\s+/', '', 			$sheetData['latitude']);
		$sheetData['branch_map'] = preg_replace('/\s+/', '', 		$sheetData['branch_map']);
		$sheetData['region'] = preg_replace('/\s+/', '', 			$sheetData['region']);
		$sheetData['area'] = preg_replace('/\s+/', '', 				$sheetData['area']);
		$sheetData['rsm_name'] = preg_replace('/\s+/', '', 			$sheetData['rsm_name']);
		$sheetData['rsm_code'] = preg_replace('/\s+/', '', 			$sheetData['rsm_code']);
		$sheetData['asm_name'] = preg_replace('/\s+/', '', 			$sheetData['asm_name']);
		$sheetData['asm_code'] = preg_replace('/\s+/', '', 			$sheetData['asm_code']);




		if(
			$sheetData['branch_name'] 	!= "BranchName" ||
			$sheetData['branch_address'] != "BranchAddress" ||
			$sheetData['branch_code'] 			!= "BranchCode"  ||
			$sheetData['longitude'] 			!= "Longitude" ||
			$sheetData['latitude'] 			!= "Latitude"  ||
			$sheetData['branch_map'] 			!= "BranchMap" ||
			$sheetData['region'] 			!= "Region"  ||
			$sheetData['area']			!= "Area" ||
			$sheetData['rsm_name']			!= "RSMName" ||
			$sheetData['rsm_code']			!= "RSMCode" ||
			$sheetData['asm_name']			!= "ASMName" ||
			$sheetData['asm_code']			!= "ASMCode"
			){

			// echo "Sorry Columns Not Matched";
			echo "<script>alert('Sorry Columns Not Matched');</script>";
			// return $ret;

			exit();

		}


		$rsm = 4;
		$asm = 8;
		$currentDate = date("Y-m-d H:i:s");
		$branch_codes = array();
		$emp_codes = array();

		foreach($xlsx->rows() as $k=>$r){

				if($k==0)
			{
				continue;
			}

			if(!empty($r[10]) && trim($r[10]) != "#N/A" && trim($r[10]) != "-"){

				$emp_code = mysqli_real_escape_string($con, $r[10]);

				$selectUserQuery = "SELECT user_id FROM users WHERE emp_code = '$emp_code' ";
				echo $selectUserQuery."<br>";//exit();
				$selectUserQueryrs = mysqli_query($con,$selectUserQuery);
				$row = mysqli_fetch_assoc($selectUserQueryrs);
				$user_id = $row['user_id'];

				$branch_code = mysqli_real_escape_string($con, $r[3]);

				$numOfRows1 =mysqli_num_rows($selectUserQueryrs);

				if($numOfRows1 < 1){

					$login_id 		= mysqli_real_escape_string($con,  $r[10]);
			        $first_name 	= mysqli_real_escape_string($con,  $r[9]);
			        $user_type 		= $rsm;
			        $emp_code 		= mysqli_real_escape_string($con,  $r[10]);
			        $branch 		= mysqli_real_escape_string($con,  $r[1]);
			        $branch_code 	= mysqli_real_escape_string($con,  $r[3]);
			        $area 			= mysqli_real_escape_string($con,  $r[8]);
			        $region 		= mysqli_real_escape_string($con,  $r[7]);
			        $password = 'aa';
			        $designation = 'RSM';

					$insertUserQuery = "INSERT INTO users (login_id,first_name,user_type,emp_code,branch,branch_code,area,region,password,designation) VALUES ('$login_id','$first_name','$user_type','$emp_code','$branch','$branch_code','$area','$region','$password','$designation')";

					echo $insertUserQuery."<br>";
					$insRs = mysqli_query($con,$insertUserQuery);

					if(!$insRs){

						echo("<br> $insertUserQuery <br> insertion failed ".mysqli_error($con));
						continue;

					}else{
						$user_id = mysqli_insert_id($con);
					}

				}else{

					$login_id 		= mysqli_real_escape_string($con,  $r[10]); 
			        $first_name 	= mysqli_real_escape_string($con,  $r[9]); 
			        $user_type 		= $rsm; 
			        $emp_code 		= mysqli_real_escape_string($con,  $r[10]); 
			        $branch 		= mysqli_real_escape_string($con,  $r[1]); 
			        $branch_code 	= mysqli_real_escape_string($con,  $r[3]); 
			        $area 			= mysqli_real_escape_string($con,  $r[8]); 
			        $region 		= mysqli_real_escape_string($con,  $r[7]); 
			        $password 		= 'aa';
			        $designation 	= 'RSM';					

					$updateUserQuery = "UPDATE users SET login_id = '$login_id',first_name = '$first_name',user_type = '$user_type',branch = '$branch',branch_code = '$branch_code',area = '$area',	region = '$region',designation = 'designation' WHERE emp_code = '$emp_code' ";

					echo $updateUserQuery."<br>";
					mysqli_query($con,$updateUserQuery);

				}




				if(!empty($r[3])){

					$selectbranchQuery = "SELECT branch_id FROM branch WHERE branch_code = '$branch_code' ";
					echo $selectbranchQuery."<br>";//exit();
					$selectbranchQueryrs = mysqli_query($con,$selectbranchQuery);
					$crow = mysqli_fetch_assoc($selectbranchQueryrs);
					$branch_id = $crow['branch_id'];
					$numOfRows2 =mysqli_num_rows($selectbranchQueryrs);
					if($numOfRows2 < 1){

						  	$branch_name 	= mysqli_real_escape_string($con, $r[1]);	                        
	                        $longitude 		= mysqli_real_escape_string($con,  $r[4]);
	                        $latitude		= mysqli_real_escape_string($con,  $r[5]);
	                        $branch_address = mysqli_real_escape_string($con,  $r[2]);
							$branch_code 	= mysqli_real_escape_string($con,  $r[3]);
	                        $status 		= '1';

						$insertbranchQuery = "INSERT INTO branch (branch_name,branch_address,branch_code,longitude,latitude,status)
						VALUES ('$branch_name','$branch_address','$branch_code','$longitude','$latitude','$status')";
						echo $insertbranchQuery."<br>";
						$insCRs = mysqli_query($con,$insertbranchQuery);

						if(!$insCRs){

							echo("<br> $insertbranchQuery <br> insertion failed ".mysqli_error($con));
							continue;

						}else{
							$branch_id = mysqli_insert_id($con);
						}

					}else{
						
						$branch_name 	= mysqli_real_escape_string($con, $r[1]);	                    
	                    $longitude 		= mysqli_real_escape_string($con, $r[4]);
	                    $latitude 		= mysqli_real_escape_string($con, $r[5]);
	                    $branch_address = mysqli_real_escape_string($con, $r[2]);
						$branch_code	= mysqli_real_escape_string($con, $r[3]);
	                    $status 		= '1';

						$updatebranchQuery = "UPDATE branch SET branch_name = '$branch_name',branch_address = '$branch_address',longitude = '$longitude',latitude = '$latitude',status = '$status' WHERE branch_code = '$branch_code' ";

						echo $updatebranchQuery."<br>";
						mysqli_query($con,$updatebranchQuery);
					}
				}else{
					continue;
				}
				// echo "user_id: ".$user_id."<br> select user query ".$selectUserQuery."<br> insert user query ".$insertUserQuery."<br>branch id ".
				// 		$branch_id."<br>select branch query ".$selectbranchQuery."<br> insert branch query ".$insertbranchQuery;
				// exit();


	// echo "good<br>";echo $user_id."<br>";echo $selectbranchQuery;
	// print_r($r);exit();

				$selectAssingQuery = "SELECT branch_id,user_id FROM user_branch WHERE user_id = '$user_id' AND branch_id = '$branch_id' ";
				echo $selectAssingQuery."<br>";
				$selectAssingQueryrs = mysqli_query($con,$selectAssingQuery);
				$arow = mysqli_fetch_assoc($selectAssingQueryrs);
				$abranch_id = $arow['branch_id'];
				$auser_id = $arow['user_id'];

				$numOfRows3=mysqli_num_rows($selectAssingQueryrs);

				if($numOfRows3 < 1){

					$insertassignQuery = "INSERT INTO user_branch (user_id,branch_id,status)
										VALUES ('$user_id','$branch_id','1')";
					echo $insertassignQuery."<br>";
					$insertassignQueryrs = mysqli_query($con,$insertassignQuery);

					if(!$insertassignQueryrs){

						echo("<br> $insertassignQuery <br> insertion failed ".mysqli_error($con));
						continue;

					}else{
						$branch_id = mysqli_insert_id($con);
					}
				}



			}else{
				continue;
			}


			if(!empty($r[12]) && trim($r[12]) != "#N/A" && trim($r[12]) != "-"){

				$emp_code = mysqli_real_escape_string($con, $r[12]);

				$selectUserQuery = "SELECT user_id FROM users WHERE emp_code = '$emp_code' ";
				echo $selectUserQuery."<br>";

				$selectUserQueryrs = mysqli_query($con,$selectUserQuery);
				$row = mysqli_fetch_assoc($selectUserQueryrs);
				$user_id = $row['user_id'];

				$branch_code = mysqli_real_escape_string($con, $r[3]);

				$numOfRows4 =mysqli_num_rows($selectUserQueryrs);

				if($numOfRows4 < 1){

					$login_id 		= mysqli_real_escape_string($con, $r[12]);
			        $first_name 	= mysqli_real_escape_string($con, $r[11]);
			        $user_type 		= $asm;
			        $emp_code 		= mysqli_real_escape_string($con, $r[12]);
			        $branch 		= mysqli_real_escape_string($con, $r[1]);
			        $branch_code 	= mysqli_real_escape_string($con, $r[3]);
			        $area 			= mysqli_real_escape_string($con, $r[8]);
			        $region 		= mysqli_real_escape_string($con, $r[7]);
			        $password 	= 'aa';
			        $designation = 'ASM';

					$insertUserQuery = "INSERT INTO users (login_id,first_name,user_type,emp_code,branch,branch_code,area,region,password,designation) 
					VALUES ('$login_id','$first_name','$user_type','$emp_code','$branch','$branch_code','$area','$region','$password','$designation')";
					echo $insertUserQuery."<br>"; 
					$insRs = mysqli_query($con,$insertUserQuery);

					if(!$insRs){

						echo("<br> $insertUserQuery <br> insertion failed ".mysqli_error($con));
						continue;

					}else{
						$user_id = mysqli_insert_id($con);
					}

				}else{

					$login_id 		= mysqli_real_escape_string($con, $r[10]); 
			        $first_name 	= mysqli_real_escape_string($con, $r[9]); 
			        $user_type 		= $rsm; 
			        $emp_code 		= mysqli_real_escape_string($con, $r[10]); 
			        $branch 		= mysqli_real_escape_string($con, $r[1]); 
			        $branch_code 	= mysqli_real_escape_string($con, $r[3]); 
			        $area 			= mysqli_real_escape_string($con, $r[8]); 
			        $region 		= mysqli_real_escape_string($con, $r[7]); 
			        $password 		= 'aa';
			        $designation 	= 'RSM';					

					$updateUserQuery = "UPDATE users SET login_id = '$login_id',first_name = '$first_name',user_type = '$user_type',branch = '$branch',branch_code = '$branch_code',area = '$area',	region = '$region',designation = 'designation' WHERE emp_code = '$emp_code' ";

					echo $updateUserQuery."<br>";
					mysqli_query($con,$updateUserQuery);
				}

				if(!empty($r[3])){


					$selectbranchQuery = "SELECT branch_id FROM branch WHERE branch_code = '$branch_code' ";
					echo $selectbranchQuery."<br>";
					$selectbranchQueryrs = mysqli_query($con,$selectbranchQuery);
					$crow = mysqli_fetch_assoc($selectbranchQueryrs);
					$branch_id = $crow['branch_id'];
					$numOfRows5 =mysqli_num_rows($selectbranchQueryrs);
					if($numOfRows5 < 1){

						  	$branch_name 	= mysqli_real_escape_string($con,$r[1]);
	                        $longitude 		= mysqli_real_escape_string($con,$r[4]); 
	                        $latitude 		= mysqli_real_escape_string($con,$r[5]); 
	                        $branch_address = mysqli_real_escape_string($con,$r[2]); 
							$branch_code 	= mysqli_real_escape_string($con,$r[3]); 
	                        $status = '1';

						$insertbranchQuery = "INSERT INTO branch (branch_name,branch_address,branch_code,longitude,latitude,status)
						VALUES ('$branch_name','$branch_address','$branch_code','$longitude','$latitude','$status')";
						echo $insertbranchQuery."<br>";
						$insCRs = mysqli_query($con,$insertbranchQuery);

						if(!$insCRs){

							echo("<br> $insertbranchQuery <br> insertion failed ".mysqli_error($con));
							continue;

						}else{
							$branch_id = mysqli_insert_id($con);
						}						

					}else{
						$branch_name 	= mysqli_real_escape_string($con, $r[1]);
	                    $branch_type 	= 'branch';
	                    $longitude 		= mysqli_real_escape_string($con, $r[4]); 
	                    $latitude 		= mysqli_real_escape_string($con, $r[5]); 
	                    $branch_address = mysqli_real_escape_string($con, $r[2]); 
						$branch_code 	= mysqli_real_escape_string($con, $r[3]); 
	                    $status 		= '1';

						$updatebranchQuery = "UPDATE branch SET branch_name = '$branch_name',branch_address = '$branch_address',longitude = '$longitude',latitude = '$latitude',status = '$status' WHERE branch_code = '$branch_code' ";

						echo $updatebranchQuery."<br>";
						mysqli_query($con,$updatebranchQuery);
					}
				}else{
					continue;
				}


				$selectAssingQuery = "SELECT branch_id,user_id FROM user_branch WHERE user_id = '$user_id' AND branch_id = '$branch_id' ";
				echo $selectAssingQuery."<br>";
				$selectAssingQueryrs = mysqli_query($con,$selectAssingQuery);
				$arow = mysqli_fetch_assoc($selectAssingQueryrs);
				$abranch_id = $arow['branch_id'];
				$auser_id = $arow['user_id'];
				$numOfRows6 =mysqli_num_rows($selectAssingQueryrs);
				if($numOfRows6 < 1){

					$insertassignQuery = "INSERT INTO user_branch (user_id,branch_id,status)
										VALUES ('$user_id','$branch_id','1')";
					echo $insertassignQuery."<br>";
					$insertassignQueryrs = mysqli_query($con,$insertassignQuery);

					if(!$insertassignQueryrs){

						echo("<br> $insertassignQuery <br> insertion failed ".mysqli_error($con));
						continue;

					}else{
						$branch_id = mysqli_insert_id($con);
					}
				}



			}
			// echo $selectbranchQuery."<br>".$insertbranchQuery."<br>".$selectAssingQuery."<br>".$insertassignQuery;
			// break;
		}


		echo "<script>alert('File Uploaded Successfully');</script>";





		//
		// foreach($xlsx->rows() as $k=>$r)
		// {
		// 	if($k==0)
		// 	{
		// 		continue;
		// 	}
		//
		// 	$clusterName=trim($r[1]);
		// 	$rsmName=($r[2]);
		// 	$dsrOrRecoveryPersonName=($r[3]);
		// 	$designation=($r[4]);
		// 	/*
		// 	$loginRsmId = lowercase_clean($rsmName);
		// 	$loginDSRId = lowercase_clean($dsrOrRecoveryPersonName);
		// 	$emailRsm = $loginRsmId."@advancetelecom.com.pk";
		// 	$emailDSR = $loginDSRId."@advancetelecom.com.pk";
		// 	*/
		// 	$regionSql="select region_id,region_name from region where region_name='$clusterName'";
		// 	$regionRs = mysqli_query($con,$regionSql);
		// 	$row = mysqli_fetch_assoc($regionRs);
		// 	$regionId = $row['region_id'];
		//
		// 	$rsmSql="select * from users where first_name='$rsmName'";
		// 	$rsmRs = mysqli_query($con,$rsmSql);
		// 	$row = mysqli_fetch_assoc($rsmRs);
		// 	$rsmId = $row['user_id'];
		// 	if ($rsmId == "") {
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				  VALUES (NULL,'$loginRsmId','$rsmName','aa','$emailRsm','4','RSM','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 		$rsmId = mysqli_insert_id($con);
		// 	}
		//
		// 	if($designation == "DSR"){
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				VALUES (NULL,'$loginDSRId','$dsrOrRecoveryPersonName','aa','$emailDSR','16','$designation','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 		$dsrId = mysqli_insert_id($con);
		// 		$insSql="INSERT INTO `advance`.`user_mapping`(`mapping_id`,`parent_id`,`child_id`)
		// 				VALUES (NULL,'$rsmId','$dsrId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 	}
		// 	if($designation == "Recovery Officer"){
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				VALUES (NULL,'$loginDSRId','$dsrOrRecoveryPersonName','aa','$emailDSR','32','$designation','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 	}
		//

		//}
		// mysqli_close($con);
		// echo "<script>alert('File Uploaded Successfully');</script>";
	// }
	// else
	// {
	// 	traceMessage("Error in Uploading");
	// }

?>
