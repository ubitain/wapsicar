<?php
	require_once('../../modules/utility/initialization.php');
	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if(!$con)
	{
		die("could not connect ".mysqli_error());
	}
	$childId=$_REQUEST['child_id'];
	if($childId=="" || !isset($childId))
		die("Invalid User Please Login Again");

	$selQuery="SELECT ut.`record_id`,first_name,latitude,longitude,ut.`location_time` FROM user_tracking ut,users u WHERE ut.`user_id`=u.`user_id` AND ut.`user_id` in($childId) ";
	$selQuery = "select record_id,first_name,latitude,longitude,location_time from user_tracking ut, users u where ut.user_id = u.user_id and ut.record_id in
	(select max(ut1.record_id) from user_tracking ut1 where ut1.user_id in ($childId) group by ut1.user_id);";
	$rsSel=mysqli_query($con,$selQuery);
	$countSel=mysqli_num_rows($rsSel);
	$ch = $jsonStr = "";
	for($i=0;$i<$countSel;$i++)
	{
		$data=mysqli_fetch_row($rsSel);
		$jsonStr.=$ch."['$data[1]',$data[2],$data[3],'$data[4]']";
		$ch=",";
	}
	$jsonStr="[".$jsonStr."]";
	//echo $selQuery;
	//echo $jsonStr;

	//die('aa');
?>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-sooax1EiXpeAASfGxhn7e19kgog55To">
</script>
<script>
	//var marker = [];
	var markersArray =[];
	var markers =<?=$jsonStr?>;
	var myCenter=new google.maps.LatLng(<?=$data[2]?>, <?=$data[3]?>);
	function initializeMaps() {
		var myOptions = {
			zoom: 12,
			center: myCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false
		};

		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var infowindow = new google.maps.InfoWindow(), marker, i;
		console.log(markers.length);
		for (i = 0; i < markers.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markers[i][1], markers[i][2]),
				// icon: image,
				map: map

			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markers[i][0]+'  '+markers[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}


		function clearOverlays() {
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];
		}

	/*	setInterval(function() {
			clearOverlays();
			GetNewLocation();
		}, 10000);
*/

		function GetNewLocation()
		{
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
				} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
					var markersStr = xmlhttp.responseText;
					// console.log(markersStr);
					markers=JSON.parse(markersStr);
					// console.log(markers);
					var infowindow = new google.maps.InfoWindow(), marker, i;
					// console.log(markers.length);
					for (i = 0; i < markers.length; i++) {
						url = markers[i][4];
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(markers[i][2], markers[i][3]),
							icon: url,
							map: map
						});
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(markers[i][1]);
								infowindow.open(map, marker);
							}
						})(marker, i));
						markersArray.push(marker);
					}
				}
			};
			xmlhttp.open("GET","getuserdata.php?child_id=<?=$childId?>",true);
			xmlhttp.send();

		}

	}
	google.maps.event.addDomListener(window, 'load', initializeMaps);
</script>

<div id="map_canvas"  style="height: 97%; width: 84%;position: absolute;"></div>
