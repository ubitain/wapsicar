<title>Attendance Map</title>
<?php
set_time_limit(0);
error_reporting(E_ERROR |  E_PARSE );
// require_once('config.php');
define("MODULE", dirname($_SERVER['DOCUMENT_ROOT'])."/modules");
require_once(MODULE . "/utility/initialization.php");
require_once(MODULE . "/utility/configuration.php");
require_once(MODULE . "/utility/standard_library.php");
require_once(MODULE . "/class/common/session.php");
require_once(MODULE . "/class/datalayer/dataaccess.php");
require_once(MODULE . "/class/bllayer/teammapping.php");

$teamMap = new BL_TeamMapping();

global $mysession;
global $con;
$mysession= new Session();
traceMessage("post request:".print_r_log($_POST));
$sid = $_REQUEST['sid'];
$type = $_REQUEST['type'];
$showUserType = $_REQUEST['showUserType'];
$showUserId = $_REQUEST['showUserId'];

$mysession->updatesession($sid);
$userType = $mysession->getValue("usertype");
$userId = $mysession->getValue("userid");

	if(!isset($showUserType)){
		$showUserType = "all";
	}

	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if(mysqli_connect_errno()){
		die("Failed to connect to MySQL: " . mysqli_connect_error());
	}
	$childId=$_REQUEST['child_id'];

	$c=0;
	$childIds = "$userId";
	$childArr = array();
	$childIds = GetChildIds1($userId,$childIds,$c,$childArr);

	function GetChildIds1($userId,&$childIds,$c,$childArr){
		global $con;
		$sql ="select distinct child_id from user_mapping um,users u where um.child_id=u.user_id and parent_id=$userId and u.status=1";
		$rsSel=mysqli_query($con,$sql);
		$countSel=mysqli_num_rows($rsSel);
		for($x=0;$x<$countSel;$x++){
			$c++;
			if($c==1000){
				die("ddd::$childIds");
			}
			$data=mysqli_fetch_row($rsSel);
			$childId = $data[0];
			if(in_array($childId,$childArr)){
				continue;
			}
			$childArr[] = $childId;
			$childIds = $childIds.','.$childId;
			$ch=",";
			GetChildIds1($childId,$childIds,$c,$childArr);
		}
		return $childIds;
	}
	if(isset($showUserId)){
		$childIds = $showUserId;
	}
	$childIds = explode(",",$childIds);
	$childIds = array_unique($childIds);
	$childIds = implode(",",$childIds);

	$childernArr = array();
	$trackingRecordArr = array();
	$childernArr = explode(",",$childIds);
	foreach ($childernArr as $key => $value) {
		$selQuery = "select max(ut1.`record_id`) from `user_tracking` ut1 where ut1.`user_id`='$value' group by ut1.`user_id`";
		$rsSel=mysqli_query($con,$selQuery);
		$countSel=mysqli_num_rows($rsSel);
		if ($countSel > 0) {
			$data=mysqli_fetch_row($rsSel);
			$trackingRecordArr[] = $data[0];
		}
	}
	$trackingRecord = implode(",",$trackingRecordArr);

	$toDate = date('Y-m-d')." 23:59:59";
	$fromDate = date('Y-m-d')." 00:00:00";
	$onlineTodaySql = "select distinct a.user_id,first_name from attendance a, users u where a.user_id=u.user_id and u.user_id in ($childIds) and time between '$fromDate' and '$toDate' ORDER BY u.first_name ASC";
	$rsSelAtt=mysqli_query($con,$onlineTodaySql);
	$countSelAtt=mysqli_num_rows($rsSelAtt);
	$presenntUserIds=array();
	$presenntUserNames=array();
	$absentUserIds=array();
	$absentUserNames=array();
	for($i=0;$i<$countSelAtt;$i++){
		$dataAtt=mysqli_fetch_row($rsSelAtt);
		$presenntUserIds[]=$dataAtt[0];
		$presenntUserNames[]=$dataAtt[1];
	}
	// sort($presenntUserNames);
	$selQuery = "select record_id,first_name,latitude,longitude,location_time,user_type,ut.user_id from user_tracking ut, users u where ut.user_id = u.user_id and ut.record_id in ($trackingRecord) and u.status='1'  ORDER BY u.first_name ASC";
	$rsSel=mysqli_query($con,$selQuery);
	$countSel=mysqli_num_rows($rsSel);
	// Initialization of variable
	$ch1= $ch2= $ch3= $ch4= $ch5= $absch1= "";
	$jsonStrBDO= $jsonStrLead= $jsonStrASM= $jsonStrRSM= $jsonStrNSM= $jsonStrABS= "";
	for($i=0;$i<$countSel;$i++){
		$data=mysqli_fetch_row($rsSel);
		$dateformat = "<br/>".date("F j, Y H:i A", strtotime($data[4]));
		$userType=$data[5];
		$userId=$data[6];
		if(!in_array($userId,$presenntUserIds)){
			$jsonStrABS.=$absch1."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
			$absch1=",";
			$absentUserIds[] = $data[6];
			$absentUserNames[] = $data[1];
		}
		else{
			if($userType==USER_TYPE_NSM){
				$jsonStrNSM.=$ch1."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
				$ch1=",";
			}
			else if($userType==USER_TYPE_RSM){
				$jsonStrRSM.=$ch2."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
				$ch2=",";
			}
			else if($userType==USER_TYPE_ASM){
				$jsonStrASM.=$ch3."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
				$ch3=",";
			}
			else if($userType==USER_TYPE_TEAM_LEAD){
				$jsonStrLead.=$ch4."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
				$ch4=",";
			}
			else if($userType==USER_TYPE_BDO){
				$jsonStrBDO.=$ch5."['$data[1]',$data[2],$data[3],'$dateformat','$data[5]']";
				$ch5=",";
			}
		}
	}
	$jsonStrBDO="[".$jsonStrBDO."]";
	$jsonStrLead="[".$jsonStrLead."]";
	$jsonStrASM="[".$jsonStrASM."]";
	$jsonStrRSM="[".$jsonStrRSM."]";
	$jsonStrNSM="[".$jsonStrNSM."]";
	$jsonStrABS="[".$jsonStrABS."]";
	if($type=="present"){
		$jsonStrABS="[]";
	}
	else if($type=="absent"){
		$jsonStrBDO=$jsonStrLead=$jsonStrASM=$jsonStrRSM=$jsonStrNSM="[]";
	}
	if($type!="absent"){
		if($showUserType == "bdo"){
			$jsonStrLead=$jsonStrASM=$jsonStrRSM=$jsonStrNSM=$jsonStrABS="[]";
		}
		else if($showUserType == "leader"){
			$jsonStrBDO=$jsonStrASM=$jsonStrRSM=$jsonStrNSM=$jsonStrABS="[]";
		}
		else if($showUserType == "asm"){
			$jsonStrBDO=$jsonStrLead=$jsonStrRSM=$jsonStrNSM=$jsonStrABS="[]";
		}
		else if($showUserType == "rsm"){
			$jsonStrBDO=$jsonStrLead=$jsonStrASM=$jsonStrNSM=$jsonStrABS="[]";
		}
		else if($showUserType == "nsm"){
			$jsonStrBDO=$jsonStrLead=$jsonStrASM=$jsonStrRSM=$jsonStrABS="[]";
		}
	}
	// sort($absentUserNames);
?>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-sooax1EiXpeAASfGxhn7e19kgog55To">
</script>
<style media="screen">
/* html,body {
  overflow-y: hidden;
} */
/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1;
}

/* Handle */
::-webkit-scrollbar-thumb {
    background: #888;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.anyClass {
height:600px;
overflow-y: scroll;
}
</style>
<script>
	//var marker = [];
	var markersArray =[];
	var markersBDO =<?=$jsonStrBDO?>;
	console.log(markersBDO);
	var markersLead =<?=$jsonStrLead?>;
	var markersASM =<?=$jsonStrASM?>;
	var markersRSM =<?=$jsonStrRSM?>;
	var markersNSM =<?=$jsonStrNSM?>;
	var markersABS =<?=$jsonStrABS?>;
	var myCenter=new google.maps.LatLng(29.520627, 69.809232);
	function initializeMaps() {
		var myOptions = {
			zoom: 6,
			center: myCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false
		};

		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var infowindow = new google.maps.InfoWindow(), marker, i;
		for (i = 0; i < markersBDO.length; i++) {
			var image = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersBDO[i][1], markersBDO[i][2]),
				// icon: image,
				map: map

			});
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersBDO[i][0]+'  '+markersBDO[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}

		for (i = 0; i < markersLead.length; i++) {
			var image = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersLead[i][1], markersLead[i][2]),
				// icon: image,
				map: map

			});
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/purple-dot.png');
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersLead[i][0]+'  '+markersLead[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}

		for (i = 0; i < markersASM.length; i++) {
			var image = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersASM[i][1], markersASM[i][2]),
				// icon: image,
				map: map

			});
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png')
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersASM[i][0]+'  '+markersASM[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}

		for (i = 0; i < markersRSM.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersRSM[i][1], markersRSM[i][2]),
				// icon: image,
				map: map

			});
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png')
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersRSM[i][0]+'  '+markersRSM[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}

		for (i = 0; i < markersNSM.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersNSM[i][1], markersNSM[i][2]),
				// icon: image,
				map: map

			});
			marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersNSM[i][0]+'  '+markersNSM[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}

		for (i = 0; i < markersABS.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markersABS[i][1], markersABS[i][2]),
				// icon: image,
				map: map

			});


			//marker.setIcon('https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png')
			marker.setIcon('/misc/offline.png')
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markersABS[i][0]+'  '+markersABS[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}


		function clearOverlays() {
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];
		}

	/*	setInterval(function() {
			clearOverlays();
			GetNewLocation();
		}, 10000);
*/

		function GetNewLocation(){
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
				} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
					var markersStr = xmlhttp.responseText;
					// console.log(markersStr);
					markers=JSON.parse(markersStr);
					// console.log(markers);
					var infowindow = new google.maps.InfoWindow(), marker, i;
					// console.log(markers.length);
					for (i = 0; i < markers.length; i++) {
						url = markers[i][4];
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(markers[i][2], markers[i][3]),
							icon: url,
							map: map
						});
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(markers[i][1]);
								infowindow.open(map, marker);
							}
						})(marker, i));
						markersArray.push(marker);
					}
				}
			};
			xmlhttp.open("GET","getuserdata.php?child_id=<?=$childId?>",true);
			xmlhttp.send();

		}

	}
	google.maps.event.addDomListener(window, 'load', initializeMaps);

	function ShowUsers(type){
		console.log(type);
		location.href='/misc/newallusersmaptracking.php?sid=<?=$sid?>&showUserType=<?=$showUserType?>&type='+type;
	}
	function ShowUserType(type){
		location.href='/misc/newallusersmaptracking.php?sid=<?=$sid?>&type=<?=$type?>&showUserType='+type;
	}
	function ShowUserId(type,id){
		location.href='/misc/newallusersmaptracking.php?sid=<?=$sid?>&type=<?=$type?>&showUserType='+type+'&showUserId='+id;
	}
</script>

<?php
if ($type == "all") {
	$allTd="";
	$absTd="none";
	$presentTd="none";
} else if($type == "present"){
	$presentTd="";
	$allTd="none";
	$absTd="none";
} else if($type == "absent"){
	$absTd="";
	$allTd="none";
	$presentTd="none";
}
?>
<div class="container-fluid">
		<!-- Content here -->
		<div class="row mt-3">
			<div class="col-4 position-sticky border-right">
				<div class="col-12 mb-1">
				<ul class="nav nav-tabs">
				  <li class="nav-item">
				    <a class="nav-link <?=($type=='all')?"active":""?>" href="#" onClick="ShowUsers('all')" >All Users</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link <?=($type=='present')?"active":""?>" href="#" onClick="ShowUsers('present')" >Present Today</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link <?=($type=='absent')?"active":""?>" href="#" onClick="ShowUsers('absent')" >Absent Today</a>
				  </li>
				</ul>
				</div>
				<div class="col-12 mb-1">
				<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
					<a class="btn btn-light"><img style='cursor:pointer' src='http://maps.google.com/mapfiles/ms/icons/blue-dot.png' height="20" width="20" onclick= "ShowUserType('nsm')"> NSM</a>
				  <a class="btn btn-light"><img style='cursor:pointer' style='cursor:pointer' src='http://maps.google.com/mapfiles/ms/icons/yellow-dot.png' height="20" width="20" onclick= "ShowUserType('rsm')"> RSM</a>
				  <a class="btn btn-light"><img style='cursor:pointer' src='http://maps.google.com/mapfiles/ms/icons/red-dot.png' height="20" width="20" onclick= "ShowUserType('asm')"> ASM</a>
				  <a class="btn btn-light"><img style='cursor:pointer' src='http://maps.google.com/mapfiles/ms/icons/purple-dot.png' height="20" width="20" onclick= "ShowUserType('leader')"> Leader</a>
				  <a class="btn btn-light"><img style='cursor:pointer' src='http://maps.google.com/mapfiles/ms/icons/green-dot.png' height="20" width="20" onclick= "ShowUserType('bdo')"> BDO</a>
				</div>
				</div>
				<div class="col-12">
					<form class="form-inline">
  						<input class="form-control col-8 mr-sm-2" id="myInput" type="search" placeholder="Filter" aria-label="Search">
  						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Filter</button>
  				  </form>
				</div>
				<div class="table-responsive-sm anyClass">
					<table class="table" id='allstaff' style='display:<?=$allTd?>'>
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">All Staff</th>
					    </tr>
					  </thead>
					  <tbody>
						<?for($x=0;$x<count($presenntUserNames);$x++){?>
							<tr>
						      <td><a href="#" onclick="ShowUserId('<?=$type?>','<?=$presenntUserIds[$x]?>')"><?=ucwords(strtolower($presenntUserNames[$x]))?></a></td>
						    </tr>
  						<?}?>
						<?for($x=0;$x<count($absentUserNames);$x++){?>
							<tr>
								<td><a href="#" onclick="ShowUserId('<?=$type?>','<?=$absentUserIds[$x]?>')"><?=ucwords(strtolower($absentUserNames[$x]))?></a></td>
						    </tr>
  						<?}?>
					  </tbody>
					</table>
					<table class="table" id='presentstaff' style='display:<?=$presentTd?>'>
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Present Staff</th>
					    </tr>
					  </thead>
					  <tbody>
						<?for($x=0;$x<count($presenntUserNames);$x++){?>
							<tr>
						      <td><a href="#" onclick="ShowUserId('<?=$type?>','<?=$presenntUserIds[$x]?>')"><?=ucwords(strtolower($presenntUserNames[$x]))?></a></td>
						    </tr>
  						<?}?>
					  </tbody>
					</table>
					<table class="table" id='absentstaff'  style='display:<?=$absTd?>'>
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Absent Staff</th>
					    </tr>
					  </thead>
					  <tbody>
						<?for($x=0;$x<count($absentUserNames);$x++){?>
							<tr>
								<td><a href="#" onclick="ShowUserId('<?=$type?>','<?=$absentUserIds[$x]?>')"><?=ucwords(strtolower($absentUserNames[$x]))?></a></td>
						    </tr>
  						<?}?>
					  </tbody>
					</table>
				</div>
			</div>
			<div class="col-8">
				<div class="row mb-2">
					<!-- Summary -->
					<div class="col-3">
						<h2>Summary</h2>
					</div>
					<div class="col-6">
						<div class="row mt-3">
							<h6 class="mr-3">Total <span class="badge badge-pill badge-primary"><?=intval($countSel)?> </span></h6>
							<h6 class="mr-3">Present Today <span class="badge badge-pill badge-primary"><?=intval($countSelAtt)?> </span></h6>
							<h6 class="mr-3">Absent Today <span class="badge badge-pill badge-primary"><?=intval($countSel-$countSelAtt)?> </span></h6>
						</div>
					</div>
					<div class="col-3">
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary btn-sm btn-block mt-2" data-toggle="modal" data-target="#exampleModal">
						  Search
						</button>
					</div>
				</div>
				<div class="row mb-2">
					<!-- Map -->
					<div id="map_canvas"  style="height: 90%; width: 100%;position: ;"></div>
				</div>
			</div>
		</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Advance Search</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
		  <div class="container-fluid">
		      <div class="row">
		        <div class="col-sm-12">
					<form>
							<div class="form-group row">
  					        	<label for="userType" class="col-sm-3 col-form-label">User Type</label>
								<div class="col-sm-9">
		  					        <select id="userType" class="form-control">
		  					          <option selected>Choose...</option>
									  <option <?=($userType=="bdo") ? 'selected':''?> value='<?="bdo"?>'><?user_type_decoder(USER_TYPE_BDO)?></option>
									  <option <?=($userType=="leader") ? 'selected':''?> value='<?="leader"?>'><?user_type_decoder(USER_TYPE_TEAM_LEAD)?></option>
									  <option <?=($userType=="asm") ? 'selected':''?> value='<?="asm"?>'><?user_type_decoder(USER_TYPE_ASM)?></option>
									  <option <?=($userType=="rsm") ? 'selected':''?> value='<?="rsm"?>'><?user_type_decoder(USER_TYPE_RSM)?></option>
									  <option <?=($userType=="nsm") ? 'selected':''?> value='<?="nsm"?>'><?user_type_decoder(USER_TYPE_NSM)?></option>
		  					        </select>
								</div>
  					      </div>
					      <div class="form-group row">
					        <label for="userList" class="col-sm-3 col-form-label">Users List</label>
							<div class="col-sm-9">
						        <select id="userList" class="form-control">
						          <option selected>Choose...</option>
						          <option>...</option>
						        </select>
							</div>
					      </div>

				  </form>
		        </div>
		      </div>
		    </div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary">Save changes</button>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#myInput").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#absentstaff tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
	$("#myInput").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#presentstaff tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
	$("#myInput").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#allstaff tr").filter(function() {
		  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
	$('#exampleModal').on('shown.bs.modal', function () {
	  // $('#myInput').trigger('focus')
  });
});
</script>
