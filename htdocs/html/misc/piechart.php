<?php
	set_time_limit(0);


require_once('config.php');
require_once(MODULE . "/utility/initialization.php");
require_once(MODULE . "/utility/configuration.php");
require_once(MODULE . "/utility/standard_library.php");
require_once(MODULE . "/class/common/session.php");
require_once(MODULE . "/class/datalayer/dataaccess.php");
require_once(MODULE . "/class/bllayer/dashboard.php");

$userId=$_REQUEST['userid'];

global $mysession;
$blDashboard = new BL_Dashboard();
$fromDate = date('Y-m-d')." 00:00:00";
$toDate =  date('Y-m-d')." 23:59:59";
$totalVisits = $blDashboard->GetCalls('visit',$userId,$fromDate,$toDate);
$totalVisits += $blDashboard->GetCalls('jointvisit',$userId,$fromDate,$toDate);
$totalVisits = $blDashboard->GetCalls('',$userId,$fromDate,$toDate);
$totalCalls = $blDashboard->GetCalls('call',$userId,$fromDate,$toDate);
	$con=mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);

	$selSql="select * From performance_details where user_id='$userId' ORDER BY performance_id DESC LIMIT 1";
	$selRs=mysqli_query($con,$selSql);
	$performanceInfo=mysqli_fetch_assoc($selRs);

	$ca=$performanceInfo['ca']!="" ? $performanceInfo['ca']:"N/A";
	$sa=$performanceInfo['sa']!="" ? $performanceInfo['sa']:"N/A";
	$td	=$performanceInfo['td']!="" ? $performanceInfo['td']:"N/A";
	$total_portfolio=$performanceInfo['total_portfolio']!="" ? $performanceInfo['total_portfolio']:"N/A";
	$target=$performanceInfo['target']!="" ? $performanceInfo['target']:"N/A";
	$variance=$performanceInfo['variance']!="" ? $performanceInfo['variance']:"N/A";



?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Meezan Performance Sheet</title>
		<meta charset="utf-8">
		<!--<meta name="viewport" content="width=device-width, initial-scale=1"> -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/your_style.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/smartadmin-skins.css">
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/smartadmin/css/demo.css">
		<!-- Loader CSS -->
		<link rel="stylesheet" type="text/css" media="screen" href="../themes/css/loader.css">
		<!-- Select2 CSS -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

	</head>
	<body style='overflow:auto'>

		<div class="container">
			<div class="row" style="margin-top:20px;">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th style='font-size:24px' colspan=2 style='text-align:center'>Performance Sheet</th>


						</tr>
					</thead>
					<tr>
						<th style='font-size:24px;background-color:white'>CA</th>
						<td style='font-size:24px;background-color:white'><?=$ca?></td>

					</tr>

					<tbody>
						<tr>
							<th style='font-size:24px;background-color:white'>SA</th>
							<td style='font-size:24px;background-color:white'><?=$sa?></td>
						</tr>
						<tr>
							<th style='font-size:24px;background-color:white'>TD</th>
							<td style='font-size:24px;background-color:white'><?=$td?></td>
						</tr>
						<tr>
							<th style='font-size:24px;background-color:white'>Total Portfolio</th>
							<td style='font-size:24px;background-color:white'><?=$total_portfolio?></td>
						</tr>
						<tr>
							<th style='font-size:24px;background-color:white'>Target</th>
							<td style='font-size:24px;background-color:white'><?=$target?></td>
						</tr>
						<tr>
							<th style='font-size:24px;background-color:white'>Variance</th>
							<td style='font-size:24px;background-color:white'><?=$variance?></td>
						</tr>
					</tbody>
				</table>






			</div>

			<div class="row" style="margin-top:20px;">

									<div class="col-xs-6">

										<div class="well well-xs bg-color-darken txt-color-white text-center">
											<h5 style='font-size:24px;font-weight:bold'>Today's Calls

												<br><?=$totalCalls?></h5>
										</div>

									</div>

									<div class="col-xs-6">

										<div class="well well-xs bg-color-teal txt-color-white text-center">
											<h5 style='font-size:24px;;font-weight:bold'>Today Visits
												<br><?=$totalVisits?></h5>
										</div>

									</div>



								</div>
			<div class="row">
				<div class="jarviswidget" id="wid-id-7" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
					<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"

					-->
					<header>

						<h2>&nbsp;</h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
							<input class="form-control" type="text">
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body">

							<!-- this is what the user will see -->
							<canvas id="pieChart" height="250"></canvas>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>

			</div>
			<div class="row">

				<div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
					<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"

					-->
					<header>

						<h2>Bar Chart </h2>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
							<input class="form-control" type="text">
						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body">

							<!-- this is what the user will see -->
							<canvas id="barChart" height="250"></canvas>

						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
			</div>
		</div>
		<script src="../themes/smartadmin/js/plugin/moment/moment.min.js"></script>
		<script src="../themes/smartadmin/js/plugin/chartjs/chart.min.js"></script>
		<script>

			var randomScalingFactor = function() {
				return Math.round(Math.random() * 100);
				//return 0;
			};

			var barChartData = {
				labels: ["Target V/s Acheivement"],
				datasets: [{
					label: 'Total Portfolio',
					backgroundColor: "#FFA538",
					data: ['<?=$total_portfolio?>']
		            }, {
					label: 'Target',
					backgroundColor: "#563D7C",
					data: ['<?=$target?>']
				}]

			};



			var PieConfig = {
				type: 'pie',
				data: {
					datasets: [{
						data: [
						'<?=$ca?>','<?=$sa?>','<?=$td?>'
						],
						backgroundColor: [
						"#F7464A",
						"#563D7C",
						"#FDB45C"

						],
					}],
					labels: [
					"CA",
					"SA",
					"TD"

					]
				},
				options: {
					responsive: true
				}
			};

			window.onload = function() {

				window.myPie = new Chart(document.getElementById("pieChart"), PieConfig);
				window.myBar = new Chart(document.getElementById("barChart"), {
					type: 'bar',
					data: barChartData,
					options: {
						responsive: true,
					}
				});
			};
		</script>
	</body>
</html>
