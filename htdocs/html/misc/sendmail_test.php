<?
	exit;
error_reporting(0);
$dboBody=file_get_contents("http://pharmatec.hyenoon.com/dbo.php");
$bumBody=file_get_contents("http://pharmatec.hyenoon.com/dashboard_bum.php");
$smBody=file_get_contents("http://pharmatec.hyenoon.com/sm.php");
$asmBody=file_get_contents("http://pharmatec.hyenoon.com/asm.php");
$to="ufar786@gmail.com";
$dboSubject="DBO Daily Report";
$bumSubject="BUM Daily Report";
$smSubject="SM Daily Report";
$asmSubject="ASM Daily Report";
SendMailFromSupport($dboBody,$to,$dboSubject);
SendMailFromSupport($bumBody,$to,$bumSubject);
SendMailFromSupport($smBody,$to,$smSubject);
SendMailFromSupport($asmBody,$to,$asmSubject);
 
function print_r_pre($var,$heading="")
{
	if ($heading!="")
		echo "<br><b>$heading:</b>";
		
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

function SendMailFromSupport($body,$mailTo,$subject)
{
	require_once("../../modules/class/lib/phpmailer/class.phpmailer.php");
	$mail = new PHPMailer(); 
	
	$mail->From = "info@hyenoon.com";
	$mail->FromName = "Hye Noon Support";
	$mail->Subject = $subject;
	
	$mail->MsgHTML($body);
	$mail->AddAddress($mailTo, "");
	
	$mail->AddCC("tahamie@gmail.com", "");
	
	
	$mail->IsHTML(true);
	
	if(!$mail->Send()) 
	{
		traceMessage(print_r_log($mail->ErrorInfo));
	} 
	else 
	{
		traceMessage("Email Message sent!");
	}
	return true;
}

function traceMessage($str="")
{
	$filename = "/home/uhfsolutions/public_html/pharmatec/htdocs/html/logs/email.txt";
	$today = date("D, M j/y G:i:s") . " " . microtime(); 	
	error_log($today .":".$str."\n",3,$filename);
	_rotate($filename);
	
}

function _rotate($filename)
{
	$length = filesize ( $filename );
	$maxSize = 100*1024*1024;//2mb 
	if ( file_exists ( $filename )&&  $length >= $maxSize )
	{	
		unlink($filename);
	}
	
	if ( !touch ( $filename ) )
	{
		die("Unable to create file: ".$filename);
	}	
	
}

function print_r_log($var)
{
	ob_start();
	print_r($var);
	$ret_str = ob_get_contents();
	ob_end_clean();

	return $ret_str;
}

?>