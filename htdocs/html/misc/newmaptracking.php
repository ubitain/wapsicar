<title>New Team Location</title>
<?php
	// Script start
	$rustart = getrusage();
	set_time_limit(0);
	error_reporting(E_ERROR |  E_PARSE );
	require_once('../../modules/utility/initialization.php');
	global $con;
	$con= mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if(!$con){
		die("could not connect ".mysqli_error());
	}
	$userId=$_REQUEST['child_id'];
	if($userId=="" || !isset($userId)){
		die("Invalid User Please Login Again");
	}

	$c=0;
	$childArr = array();
	$childIds = GetChildIds1($userId,$childIds,$c,$childArr);

	function GetChildIds1($userId,&$childIds,$c,$childArr) {
		global $con;
		$sql = "SELECT distinct `child_id` from `user_mapping` um,`users` u where um.`child_id`=u.`user_id` and um.`parent_id`='$userId' and u.`status`='1'";
		$rsSel = mysqli_query($con,$sql);
		$countSel = mysqli_num_rows($rsSel);
		for ($x=0; $x < $countSel; $x++) {
			$c++;
			if ($c == 1000) {
				die("ddd::$childIds");
			}
			$data = mysqli_fetch_row($rsSel);
			$childId = $data[0];
			if (in_array($childId,$childArr)) {
				continue;
			}
			$childArr[] = $childId;
			$childIds = $childIds.','.$childId;
			$ch=",";
			GetChildIds1($childId,$childIds,$c,$childArr);
		}
		return $childIds;
	}
	$childIds = ltrim($childIds, ',');
	$childIds = explode(",",$childIds);
	$childIds = array_unique($childIds);
	$trackingRecordArr = array();
	foreach ($childIds as $key => $value) {
		$selQuery = "select max(ut1.`record_id`) from `user_tracking` ut1 where ut1.`user_id`='$value' group by ut1.`user_id`";
		$rsSel=mysqli_query($con,$selQuery);
		$countSel=mysqli_num_rows($rsSel);
		if ($countSel > 0) {
			$data=mysqli_fetch_row($rsSel);
			$trackingRecordArr[] = $data[0];
		}
	}
	$trackingRecord = implode(",",$trackingRecordArr);

	$selQuery = "select record_id,first_name,latitude,longitude,location_time from user_tracking ut, users u where ut.user_id = u.user_id and ut.record_id in ($trackingRecord);";
	$rsSel=mysqli_query($con,$selQuery);

	$countSel=mysqli_num_rows($rsSel);
	$ch = $jsonStr = "";
	for($i=0;$i<$countSel;$i++)
	{
		$data=mysqli_fetch_row($rsSel);
		$dateformat = "<br/>".date("F j, Y H:i A", strtotime($data[4]));
		$jsonStr.=$ch."['$data[1]',$data[2],$data[3],'$dateformat']";
		$ch=",";
	}
	$jsonStr="[".$jsonStr."]";
?>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
	<link rel="stylesheet" type="text/css" media="screen" href="../../themes/smartadmin/css/smartadmin-production-plugins.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../../themes/smartadmin/css/smartadmin-production.css">
	<link rel="stylesheet" type="text/css" media="screen" href="../../themes/smartadmin/css/smartadmin-skins.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-sooax1EiXpeAASfGxhn7e19kgog55To">
</script>
<style media="screen">
/* html,body {
  overflow-y: hidden;
} */
/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1;
}

/* Handle */
::-webkit-scrollbar-thumb {
    background: #888;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555;
}
.anyClass {
height:600px;
overflow-y: scroll;
}
</style>
<script>
	//var marker = [];
	var markersArray =[];
	var markers =<?=$jsonStr?>;
	var myCenter=new google.maps.LatLng(29.520627, 69.809232);
	function initializeMaps() {
		var myOptions = {
			zoom: 6,
			center: myCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false
		};

		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var infowindow = new google.maps.InfoWindow(), marker, i;
		console.log(markers.length);
		for (i = 0; i < markers.length; i++) {
			var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markers[i][1], markers[i][2]),
				// icon: image,
				map: map

			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(markers[i][0]+'  '+markers[i][3]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			markersArray.push(marker);
		}


		function clearOverlays() {
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];
		}

	/*	setInterval(function() {
			clearOverlays();
			GetNewLocation();
		}, 10000);
*/

		function GetNewLocation()
		{
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
				} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
					var markersStr = xmlhttp.responseText;
					// console.log(markersStr);
					markers=JSON.parse(markersStr);
					// console.log(markers);
					var infowindow = new google.maps.InfoWindow(), marker, i;
					// console.log(markers.length);
					for (i = 0; i < markers.length; i++) {
						url = markers[i][4];
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(markers[i][2], markers[i][3]),
							icon: url,
							map: map
						});
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(markers[i][1]);
								infowindow.open(map, marker);
							}
						})(marker, i));
						markersArray.push(marker);
					}
				}
			};
			xmlhttp.open("GET","getuserdata.php?child_id=<?=$childId?>",true);
			xmlhttp.send();

		}

	}
	google.maps.event.addDomListener(window, 'load', initializeMaps);
</script>
<div class="container-fluid">
		<!-- Content here -->
		<div class="row mt-3">
			<div class="col-4 position-sticky border-right">
				<!-- <nav class="navbar navbar-light bg-light">
				  <form class="form-inline">
						<input class="form-control mr-sm-2" id="myInput" type="search" placeholder="Filter" aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Filter</button>
				  </form>
				</nav> -->
				<div class="col-12">
					<form class="form-inline">
  						<input class="form-control col-10 mr-sm-2" id="myInput" type="search" placeholder="Filter" aria-label="Search">
  						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Filter</button>
  				  </form>
				</div>
				<div class="table-responsive-sm anyClass">
					<!-- NEW WIDGET START -->
					<article class="col-sm-12 col-md-12 col-lg-6">

						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
							<!-- widget options:
							usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

							data-widget-colorbutton="false"
							data-widget-editbutton="false"
							data-widget-togglebutton="false"
							data-widget-deletebutton="false"
							data-widget-fullscreenbutton="false"
							data-widget-custombutton="false"
							data-widget-collapsed="true"
							data-widget-sortable="false"

							-->
							<header>
								<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
								<h2>Simple View </h2>

							</header>

							<!-- widget div-->
							<div>

								<!-- widget edit box -->
								<div class="jarviswidget-editbox">
									<!-- This area used as dropdown edit box -->

								</div>
								<!-- end widget edit box -->

								<!-- widget content -->
								<div class="widget-body">

									<div class="tree smart-form">
										<ul>
											<li>
												<span><i class="fa fa-lg fa-folder-open"></i> Parent</span>
												<ul>
													<li>
														<span><i class="fa fa-lg fa-plus-circle"></i> Administrators</span>
														<ul>
															<li style="display:none">
																<span> <label class="checkbox inline-block">
																		<input type="checkbox" name="checkbox-inline">
																		<i></i>Michael.Jackson</label> </span>
															</li>
															<li style="display:none">
																<span> <label class="checkbox inline-block">
																		<input type="checkbox" checked="checked" name="checkbox-inline">
																		<i></i>Sunny.Ahmed</label> </span>
															</li>
															<li style="display:none">
																<span> <label class="checkbox inline-block">
																		<input type="checkbox" checked="checked" name="checkbox-inline">
																		<i></i>Jackie.Chan</label> </span>
															</li>
														</ul>
													</li>
													<li>
														<span><i class="fa fa-lg fa-minus-circle"></i> Child</span>
														<ul>
															<li>
																<span><i class="icon-leaf"></i> Grand Child</span>
															</li>
															<li>
																<span><i class="icon-leaf"></i> Grand Child</span>
															</li>
															<li>
																<span><i class="fa fa-lg fa-plus-circle"></i> Grand Child</span>
																<ul>
																	<li style="display:none">
																		<span><i class="fa fa-lg fa-plus-circle"></i> Great Grand Child</span>
																		<ul>
																			<li style="display:none">
																				<span><i class="icon-leaf"></i> Great great Grand Child</span>
																			</li>
																			<li style="display:none">
																				<span><i class="icon-leaf"></i> Great great Grand Child</span>
																			</li>
																		</ul>
																	</li>
																	<li style="display:none">
																		<span><i class="icon-leaf"></i> Great Grand Child</span>
																	</li>
																	<li style="display:none">
																		<span><i class="icon-leaf"></i> Great Grand Child</span>
																	</li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li>
												<span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
												<ul>
													<li>
														<span><i class="icon-leaf"></i> Child</span>
													</li>
												</ul>
											</li>
										</ul>
									</div>

								</div>
								<!-- end widget content -->

							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->
				</div>
			</div>
			<div class="col-8">
				<div class="row mb-2">
					<!-- Summary -->
					<div class="col-4">
						<h2>Team Location</h2>
					</div>
				</div>
				<div class="row mb-2">
					<!-- Map -->
					<div id="map_canvas"  style="height: 90%; width: 100%;position: ;"></div>
				</div>
			</div>
		</div>
</div>
<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

$(document).ready(function() {

	// pageSetUp();

	// PAGE RELATED SCRIPTS

	$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
	$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
		var children = $(this).parent('li.parent_li').find(' > ul > li');
		if (children.is(':visible')) {
			children.hide('fast');
			$(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
		} else {
			children.show('fast');
			$(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
		}
		e.stopPropagation();
	});

})

</script>
<?php
// Script end
function rutime($ru, $rus, $index) {
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
     -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

$ru = getrusage();
echo "This process used " . rutime($ru, $rustart, "utime") .
    " ms for its computations\n";
echo "It spent " . rutime($ru, $rustart, "stime") .
    " ms in system calls\n";
die();
?>
