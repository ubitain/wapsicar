<?php
	require_once("simplexlsx.class.php");
	require_once("config.php");
	require_once(MODULE . "/utility/initialization.php");
	require_once(MODULE . "/utility/configuration.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/common/session.php");
	require_once(MODULE . "/class/datalayer/dataaccess.php");
	$con=mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if (mysqli_connect_errno())
	{
		traceMessage("Failed to connect to MySQL: " . mysqli_connect_error());
		exit;
	}
	traceMessage("Request Data ".print_r_log($_REQUEST));
	traceMessage("File Array ".print_r_log($_FILES));
	$time = time();
	$targetFolderUser = UPLOAD_DIRECTORY_PATH."SalesTeam/";
	if(!is_dir($targetFolderUser))
	{
		traceMessage("Create Directory $targetFolderUser");
		mkdir($targetFolderUser);
	}
	$targetFolder = $targetFolderUser.$time."/";
	if(!is_dir($targetFolder))
	{
		traceMessage("Create Directory $targetFolder");
		mkdir($targetFolder);
	}
	$fileName=basename($_FILES['usersheet']['name']);
	$targetFolder = $targetFolder . $fileName;
	if(file_exists($targetFolder))
	{
		traceMessage("file already exists".$targetFolder);
		if(unlink($targetFolder))
		{
			traceMessage("Already exists , unlink and reupload");
			echo "<script>alert('File Already exists');</script>";
		}
	}
	else if(move_uploaded_file($_FILES['usersheet']['tmp_name'], $targetFolder))
	{
		traceMessage("File Uploaded Successfully");
		// $xlsx = new SimpleXLSX($targetFolder);
		// echo "<pre>";
		//
		// foreach($xlsx->rows() as $k=>$r)
		// {
		// 	if($k==0)
		// 	{
		// 		continue;
		// 	}
		//
		// 	$clusterName=trim($r[1]);
		// 	$rsmName=($r[2]);
		// 	$dsrOrRecoveryPersonName=($r[3]);
		// 	$designation=($r[4]);
		// 	/*
		// 	$loginRsmId = lowercase_clean($rsmName);
		// 	$loginDSRId = lowercase_clean($dsrOrRecoveryPersonName);
		// 	$emailRsm = $loginRsmId."@advancetelecom.com.pk";
		// 	$emailDSR = $loginDSRId."@advancetelecom.com.pk";
		// 	*/
		// 	$regionSql="select region_id,region_name from region where region_name='$clusterName'";
		// 	$regionRs = mysqli_query($con,$regionSql);
		// 	$row = mysqli_fetch_assoc($regionRs);
		// 	$regionId = $row['region_id'];
		//
		// 	$rsmSql="select * from users where first_name='$rsmName'";
		// 	$rsmRs = mysqli_query($con,$rsmSql);
		// 	$row = mysqli_fetch_assoc($rsmRs);
		// 	$rsmId = $row['user_id'];
		// 	if ($rsmId == "") {
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				  VALUES (NULL,'$loginRsmId','$rsmName','aa','$emailRsm','4','RSM','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 		$rsmId = mysqli_insert_id($con);
		// 	}
		//
		// 	if($designation == "DSR"){
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				VALUES (NULL,'$loginDSRId','$dsrOrRecoveryPersonName','aa','$emailDSR','16','$designation','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 		$dsrId = mysqli_insert_id($con);
		// 		$insSql="INSERT INTO `advance`.`user_mapping`(`mapping_id`,`parent_id`,`child_id`)
		// 				VALUES (NULL,'$rsmId','$dsrId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 	}
		// 	if($designation == "Recovery Officer"){
		// 		$insSql="INSERT INTO `advance`.`users`(`user_id`,`login_id`,`first_name`,`password`,`email_address`,`user_type`,`designation`,`region`)
		// 				VALUES (NULL,'$loginDSRId','$dsrOrRecoveryPersonName','aa','$emailDSR','32','$designation','$regionId')";
		//
		// 		$insRs = mysqli_query($con,$insSql);
		// 		if(!$insRs)
		// 		{
		// 			echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
		// 		}
		// 	}
		//

		//}
		mysqli_close($con);
		echo "<script>alert('File Uploaded Successfully');</script>";
	}
	else
	{
		traceMessage("Error in Uploading");
	}

?>
