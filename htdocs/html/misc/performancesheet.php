<?php
	require_once("config.php");
	require_once(MODULE . "/utility/initialization.php");
	require_once(MODULE . "/utility/configuration.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/class/common/session.php");
	require_once(MODULE . "/class/datalayer/dataaccess.php");
	require_once("simplexlsx.class.php");
	$date = $_REQUEST["date"];
	$userId=$_REQUEST["userId"];
	$con=mysqli_connect(DESKTOPDBHOSTNAME,DESKTOPDBUSERNAME,DESKTOPDBPASSWORD,DESKTOPDBDATABASE);
	if (mysqli_connect_errno())
	{
		traceMessage("Failed to connect to MySQL: " . mysqli_connect_error());
		exit;
	}
	traceMessage("Request Data ".print_r_log($_REQUEST));
	traceMessage("File Array ".print_r_log($_FILES));
	$targetFolderUser = UPLOAD_DIRECTORY_PATH;
	if(!is_dir($targetFolderUser))
	{
		traceMessage("Create Directory $targetFolderUser");
		mkdir($targetFolderUser);
	}
	$targetFolderDate = $targetFolderUser.$date."/";
	if(!is_dir($targetFolderDate))
	{
		traceMessage("Create Directory $targetFolderDate");
		mkdir($targetFolderDate);
	}
	$fileName=basename($_FILES['performancesheet']['name']);
	$browsePath=BROWSE_PATH.$date."/".$fileName;
	$targetFolder = $targetFolderDate . $fileName;
	if(file_exists($targetFolder))
	{
		traceMessage("file already exists".$targetFolder);
		if(unlink($targetFolder))
		{
			traceMessage("Already exists , unlink and reupload");
			move_uploaded_file($_FILES['performancesheet']['tmp_name'], $targetFolder);
			$insSql="INSERT INTO `performance_sheet`(`sheet_id`,`date`,`file_name`,`file_path`,`target_folder`,`status`)
			VALUES (NULL,'$date','$fileName','$browsePath','$targetFolder','1')";
			traceMessage("$insSql");
			mysqli_query($con,$insSql);
			// traceMessage("aaaaaaaaaaaaaaa");
			$performanceSheetId=mysqli_insert_id($con);
			// traceMessage("bbbbbbbbbb");
			$xlsx = new SimpleXLSX($targetFolder);
			$insertionDate=date("Y-m-d H:i:s");
			// traceMessage("performanceSheetId - $performanceSheetId :::: targetFolder - $targetFolder");
			// exit;
			foreach($xlsx->rows() as $k=>$r)
			{
				if($k==0)
				{
					continue;
				}
				$bdoCode=$r[2];
				$ca=round($r[5]);
				$sa=round($r[6]);
				$td=round($r[7]);
				$totalPortfolio=round($r[8]);
				$target=round($r[9]);
				$variance=round($r[10]);

				$selSql="SELECT * FROM users u WHERE u.`emp_code`='$bdoCode'";
				traceMessage("sel user sql : $selSql");
				$userRs = mysqli_query($con,$selSql);
				$userInfo=mysqli_fetch_assoc($userRs);
				$userId=$userInfo['user_id'];

				if($userId > 0)
				{
					$insSql="INSERT INTO `meezanbank`.`performance_details`(`performance_id`,`performance_sheet_id`,
					 `user_id`,`ca`,`sa`,`td`,`total_portfolio`,`target`,`variance`,`performance_date`,`insertion_date`,`status`)
					VALUES (NULL,'$performanceSheetId','$userId','$ca','$sa','$td','$totalPortfolio','$target','$variance','$date','$insertionDate','1')";

					$insRs = mysqli_query($con,$insSql);
					if(!$insRs)
					{
						echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
					}
				}
			}
			echo "<script>alert('File Uploaded Successfully');</script>";
		}
	}
	else if(move_uploaded_file($_FILES['performancesheet']['tmp_name'], $targetFolder))
	{
		traceMessage("File Uploaded Successfully");

		$insSql="INSERT INTO `performance_sheet`(`sheet_id`,`date`,`file_name`,`file_path`,`target_folder`,`status`)
		VALUES (NULL,'$date','$fileName','$browsePath','$targetFolder','1')";
		traceMessage("$insSql");
		mysqli_query($con,$insSql);
		$performanceSheetId=mysqli_insert_id($con);
		$xlsx = new SimpleXLSX($targetFolder);
		$insertionDate=date("Y-m-d H:i:s");
		foreach($xlsx->rows() as $k=>$r)
		{
			if($k==0)
			{
				continue;
			}
			$bdoCode=$r[2];
			$ca=round($r[5]);
			$sa=round($r[6]);
			$td=round($r[7]);
			$totalPortfolio=round($r[8]);
			$target=round($r[9]);
			$variance=round($r[10]);

			$selSql="SELECT * FROM users u WHERE u.`emp_code`='$bdoCode'";
			$userRs = mysqli_query($con,$selSql);
			$userInfo=mysqli_fetch_assoc($userRs);
			$userId=$userInfo['user_id'];

			if($userId > 0)
			{
				$insSql="INSERT INTO `meezanbank`.`performance_details`(`performance_id`,`performance_sheet_id`,
				 `user_id`,`ca`,`sa`,`td`,`total_portfolio`,`target`,`variance`,`performance_date`,`insertion_date`,`status`)
				VALUES (NULL,'$performanceSheetId','$userId','$ca','$sa','$td','$totalPortfolio','$target','$variance','$date','$insertionDate','1')";

				$insRs = mysqli_query($con,$insSql);
				if(!$insRs)
				{
					echo("<br> $insSql <br> insertion failed ".mysqli_error($con));
				}
			}
		}
		echo "<script>alert('File Uploaded Successfully');</script>";
	}
	else
	{
		traceMessage("Error in Uploading");
	}
?>
