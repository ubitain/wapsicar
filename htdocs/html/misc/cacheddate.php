<?php
/**
 * This file contains the class GoogleCache.
 * For more information on this file and how to use the class please visit
 * http://www.hashbangcode.com/blog/google-last-cached-date-finder-in-php-1973.html
 *
 * Changes in this version:
 * 1. Changed the IP address used to webcache.googleusercontent.com.
 * 
 * @category GoogleCache
 * @package  GoogleCache
 * @author   Philip Norton
 * @link     http://www.hashbangcode.com/
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License 
 *
 */

/**
 * The GoogleCache Class can be used to find out the last cached date of a URL
 * from Google. This is the date that Google last accessed the URL.
 *
 * @category GoogleCache
 * @package  GoogleCache
 * @author   Philip Norton
 * @version  1.1 22/08/2011
 * @link     http://www.hashbangcode.com/examples/googlecache/GoogleCache.phps
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 *
 */

class GoogleCache
{

  /**
   *
   * @var array
   */
  protected $googleAddresses = array("webcache.googleusercontent.com");

  /**
   *
   * @var null
   */
  protected $urlUsed;
  protected $rawResult;
  protected $result;
  protected $userAgent;

  /**
   * Class constructor.
   *
   * @param string $userAgent The user agent to be used for the request.
   */
  public function GoogleCache($userAgent = null)
  {
    if ($userAgent == null) {
      // Assign a default user agent.
      $this->userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.12) Gecko/20080201 Firefox/2.0.0.12";
    } else {
      $this->userAgent = $userAgent;
    }
  }

  /**
   *
   * @param string $userAgent The user agent to be used with the next request.
   */
  public function setUserAgent($userAgent)
  {
    $this->userAgent = $userAgent;
  }

  /**
   * Return the current user agent string.
   *
   * @return string
   */
  public function getUserAgent()
  {
    return $this->userAgent;
  }

  /**
   * The raw string returned from the last query. If the last query failed then
   * this value will be a blank string.
   *
   * @return string
   */
  public function getRawResult()
  {
    return $this->rawResult;
  }

  /**
   * The date returned from the last query. If the last query failed then this
   * value will be a blank string.
   *
   * @return mixed
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * This property is set in the getLastCached function and is the URL that
   * was used to find out the last cached date from Google.
   *
   * @return string
   */
  public function getUrlUsed()
  {
    return $this->urlUsed;
  }

  /**
   * Run the query to fetch the last cached date from Google using the given
   * string as the URL to check.
   *
   * @param string $url The URL to find out the cached date for.
   * @return mixed A string containing the date if everything worked, otherwise
   *               false is returned.
   */
  public function getLastCached($url)
  {
    // Reset the result
    $this->result    = '';
    $this->rawResult = '';

    // Make sure that the URL is valid.
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
      return false;
    };

    // URL can't be longer than 2048
    if (strlen($url) > 2048) {
      return false;
    }

    // Fetch random Google address.
    $submit_url = 'http://' . $this->googleAddresses[mt_rand(0, count($this->googleAddresses)-1)];

    // Construct Google Query
    $submit_vars["hl"]    = "en";
    $submit_vars["q"]     = 'cache:'.urlencode($url);  // max length is 2048
    $submit_vars["strip"] = 1;

    $query = '';
    foreach ($submit_vars as $var => $val) {
      $query .= '&' . $var . '=' . $val;
    };

    $query         = substr($query, 1);
    $url           = $submit_url . '/search?' . $query;
    $this->urlUsed = $url;

    // Send CURL request to fetch data
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL,            $this->urlUsed);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT,      $this->userAgent);
    curl_setopt($curl, CURLOPT_HEADER,         0);
    $this->rawResult = curl_exec($curl);
	echo "<pre>";
	print_r($this->rawResult);
    curl_close($curl);

    // If no result is returned then return false.
    if (!is_string($this->rawResult ) || !strlen($this->rawResult)) {
      return false;
    }

    // Extract date from data.
    $pattern = '/(\d{1,2}\s[a-zA-Z]{3}\s\d{4}\s\d{2}:\d{2}:\d{2}.*?)\./';
    preg_match($pattern, $this->rawResult, $matches);

    // Return output
    if (isset($matches[1])) {
      $this->result = $matches[1];
      return $this->result;
    }
    return false;
  }
}


$googleCache = new GoogleCache();
$date = $googleCache->getLastCached("http://www.bbc.co.uk/");
echo $date;
?>