<?php
	error_reporting(E_ERROR);
	include('config.php');
	date_default_timezone_set('Asia/Karachi');
	if(file_exists(EMERGENCY_BRAKE))
	{
		unlink(IN_PROCESS);
		die("Emergency Brakes Applied");
	}
	if (file_exists(IN_PROCESS))
	{
		$time1= filemtime(IN_PROCESS);
		$time2= time();
		$difference=$time2-$time1;
		if($difference > 3600)
		{
			unlink(IN_PROCESS);
		}
		else
		{
			die("Script Already Running");
		}
	}
	touch(IN_PROCESS);
	$sql="select * from tracking_cache where status=1";
	$rsTracking=mysqli_query($con,$sql);
	$trackingCacheCount=mysqli_num_rows($rsTracking);
	for($j=0;$j<$trackingCacheCount;$j++)
	{
		$cacheInfo=mysqli_fetch_assoc($rsTracking);
		if(!is_array($cacheInfo))
		{
			unlink(IN_PROCESS);
			die("No Records Found");
		}

		$users_data = $cacheInfo["tracking_data"];
		if($users_data=="")
		{
			echo "no user data";
			continue;
		}
		$infoArray = json_decode($users_data,true);

		$hash=rand().time();
		$insStr="";
		$ch="";
		for($i=0;$i<count($infoArray);$i++)
		{
			$userId = $infoArray[$i]["userId"];
			$latitude = $infoArray[$i]["lat"];
			$longitude = $infoArray[$i]["long"];

			$infoArray[$i]["location_time"]=($infoArray[$i]["location_time"]/1000);
			$locationTime = date("Y-m-d H:i:s",$infoArray[$i]["location_time"]);
			if(strstr($locationTime,"1970-01-18"))
			{
				$infoArray[$i]["location_time"]=($infoArray[$i]["location_time"]*1000);
				$locationTime = date("Y-m-d H:i:s",$infoArray[$i]["location_time"]);
			}
			if($userId=="" || $latitude == "" || $longitude == "" || $latitude == "0" || $longitude == "0")
			{
				echo "key missing";
				continue;
			}
			$insStr.=$ch."(NULL, '$userId','$latitude', '$longitude','$locationTime', '$hash')";
			$ch=",";
		}
		$query="INSERT INTO `user_tracking_offline` (`record_id`,`user_id`,`latitude`,`longitude`,`location_time`,`hash`)
		VALUES $insStr";
		$rs = mysqli_query($con,$query);
		$selQuery="SELECT `user_id` AS `userId`,`latitude` AS `lat`,`longitude` AS `long`,`location_time` AS `location_time`
		FROM user_tracking_offline WHERE `hash`='$hash' ORDER BY location_time ASC";
		$countSel="";
		$selRs=mysqli_query($con,$selQuery);
		$countSel=mysqli_num_rows($selRs);
		for($x=0;$x<$countSel;$x++)
		{
			if(file_exists(EMERGENCY_BRAKE))
			{
				unlink(IN_PROCESS);
				die("Emergency Brakes Applied");
			}
			$infoData=array();
			$infoData=mysqli_fetch_assoc($selRs);
			$distanceDifference=0;
			$differenceInSeconds=0;
			$userId = $infoData["userId"];
			$latitude = $infoData["lat"];
			$longitude = $infoData["long"];
			$locationTime = $infoData["location_time"];

			if($userId=="" || $latitude == "" || $longitude == "" || $latitude == "0" || $longitude == "0")
			{
				continue;
			}

			$distanceQuery = "SELECT * FROM user_tracking WHERE user_id ='$userId' ORDER BY record_id DESC LIMIT 1";
			$distanceRs=mysqli_query($con,$distanceQuery);
			$distRow=mysqli_fetch_assoc($distanceRs);
			$lastLatitude=$distRow['latitude'];
			$lastLongitude=$distRow['longitude'];

			if(mysqli_num_rows($distanceRs)==0)
			{
				$distanceDifference=0;
				$differenceInSeconds=0;
			}
			else
			{
				$distanceDifference=distance($lastLatitude, $lastLongitude, $latitude, $longitude, 'M');
				$timeFirst  =strtotime(date("Y-m-d H:i:s",strtotime($distRow['location_time'])));
				$timeSecond = strtotime(date("Y-m-d H:i:s",strtotime($locationTime)));
				$differenceInSeconds = $timeSecond - $timeFirst;
			}

			if(($distanceDifference < 0) || ($distanceDifference > 1000))
				$distanceDifference=0;
			if(($differenceInSeconds < 0) || ($differenceInSeconds > 300))
				$differenceInSeconds=0;

			$distanceDifference=round($differenceInSeconds,2);
			$currentTime=date("Y-m-d H:i:s",strtotime($locationTime));
			if($userId=="" || $latitude == "" || $longitude == "")
			{
				continue;
			}
			$insStr="(NULL, '$userId', '$currentTime', '$latitude', '$longitude','$distanceDifference', '$differenceInSeconds' , 1)";
			$mainQuery="INSERT INTO `user_tracking` (`record_id`, `user_id`, `location_time`, `latitude`, `longitude`,`distance`, `time_difference` , `status`)
			VALUES $insStr";
			$insRs = mysqli_query($con,$mainQuery);
		}
		if($insRs)
		{
			$delQuery="delete from `user_tracking_offline` where `hash`='$hash'";
			$delRs=mysqli_query($con,$delQuery);
			if($delRs)
			{
				$delCacheQuery="update `tracking_cache` set status='-1' where `record_id`=".$cacheInfo['record_id'];
				$delCacheRs=mysqli_query($con,$delCacheQuery);
			}
		}
	}
	unlink(IN_PROCESS);
?>
