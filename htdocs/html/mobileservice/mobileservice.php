<?php

error_reporting(E_ALL);

include('config.php');
date_default_timezone_set('Asia/Karachi');
$action = $_REQUEST["action"];
traceMessage("REQUEST DATA" . print_r_log($_REQUEST));
traceMessage("REQUEST FILES" . print_r_log($_FILES)); 
function check_null($var, $default="N/A") {
    return ($var == "") ? $default : $var;
}
// echo $action;
$obj        = new stdClass(); //other than list jo bhi empty return hai vahan object return hoga
$emptyArray = array(); // jahan jahan list return ho rahi hai vahan empty k case mai ye array return hoga

// SESSION
global $mysession;
$mysession = new session();
if (isset($_REQUEST["userid"])) {
    $sid = $_REQUEST["userid"];
}
if (isset($_REQUEST["user_id"])) {
    $sid = $_REQUEST["user_id"];
}
$mysession->updatesession($sid);
$userId = $mysession->getValue("userid");
// echo $userId; exit;
// REQUEST Logging
$entity = array(
    'name' => $mysession->getValue("loginid"),
    'userid' => $userId
);
$postFields = $_REQUEST;
$url = SERVER_NAME.$_SERVER['REQUEST_URI'];
$name = $action;
// $ipAddress = getUserIP();
// InsertApiLog($ipAddress,$entity,$name,$url,$postFields);
// SESSION Validating
$actionExempt =  array("version_check","login","zones","region_names","area_names","city_names","get_parts","get_category_sub_category","search_purchasefrom_person","final_purchase","final_account","loop_purchase_account","search_accountoff_person","dashboard_counter","purchase_from_hierarchy","view_notification_detail","search_parts");
$token = '0';
if ( $userId > 0 || in_array($action,$actionExempt) ) {
    $_REQUEST["userid"] = $userId;
    $_REQUEST["user_id"] = $userId;
}
else {
    $status = array(
        "header" => array(
            "success" => "0",
            "message" => "user invalid",
            "token" => $token,
        ),
        "body" => $obj
    );
    traceMessage(print_r_log($status));
    echo json_encode($status);
    exit;
}

// die("aa");

if ($action == "login") { //only sales executive can login
    $username = $_REQUEST["username"];
    $password = $_REQUEST["password"];
    $androidPushId = $_REQUEST["android_token"];

    if ($username == "" || $password == "") {
        $status = array(
            "header" => array(
                "success" => "0",
                "message" => "login details not provided"
            ),
            "body" => $obj
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
        exit;
    }
    $login = mysqli_query($con, "select * from users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.dealer_category_id IN (SELECT dealer_category_id FROM dealer_category WHERE dealer_category_name IN ('Mechanics','SubDealers','Dealers','Distributor')) and login_id='$username' and password='$password'");
        if (@mysqli_num_rows($login) > 0) {
		
		$mynewsession = new session();
        $mynewsession->startsession($username, "", "","");
		
        $row    = @mysqli_fetch_assoc($login);
        if(isset($androidPushId) && $androidPushId!='')
		{
		$updateSql = "update users set android_push_id = '$androidPushId' where user_id =".$row['user_id'];
		mysqli_query($con,$updateSql);
		}
		
		$mysession=$mynewsession;
        $mynewsession->setvalue("regionid", $row["region"]);
        $mynewsession->setvalue("usertype", $row["user_type"]);
        $mynewsession->setvalue("userid", $row["user_id"]);
        $mynewsession->setvalue("loginid", $row["login_id"]);
        $mynewsession->setvalue("username", $row["first_name"]." ".$row["middle_name"]." ".$row["last_name"]);
        $userId = $row["user_id"];
		$row['user_idd'] =  $row["user_id"];
		$row['user_id'] = $mysession->sessionid;
        $row["android_push_id"] = $androidPushId;
        // Null will be replaced by N/A
        foreach ($row as $key => $value) {
            $row[$key] = check_null($value);
        }
        // Unset the password key
        unset($row['password']);
        $status = array(
            "header" => array(
                "success" => "1",
                "message" => "login successful"
            ),
            "body" => array(
                "users" => $row
            )
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
    } else {
        $status = array(
            "header" => array(
                "success" => "0",
                "message" => "Username and password is not correct" // yaha change kra ha!
            ),
            "body" => $obj
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
    }
}
elseif ($action == "products_detail") {

 	$queryProduct = "select * from items";
    $rsQuery   = mysqli_query($con, $queryProduct);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $products = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($products, $data);
        }
    }
     echo json_encode($products);
}
elseif ($action == "notification_api") {
    $userIdCheck = $_REQUEST["user_id"];
    if ($userIdCheck=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Found!"),"body"=>$userIdCheck);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    $queryNotification = "select * from notifications where user_id=".$userIdCheck." ORDER BY record_id DESC";
    $rsQuery   = mysqli_query($con, $queryNotification);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $notification = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
                $fullDetail = array();
                $claimNo = $data['reference_id'];
                // array_push($notification, $data);
                $fullDetail['record_id'] = $data['record_id'];
                $fullDetail['user_id'] = $data['user_id'];
                $fullDetail['reference_id'] = $data['reference_id'];
                $fullDetail['title'] = $data['title'];
                $fullDetail['type'] = $data['type'];
                $fullDetail['page'] = $data['page'];
                $fullDetail['message'] = $data['message'];
                $fullDetail['creation_time'] = $data['creation_time'];
                $fullDetail['email_address'] = $data['email_address'];
                $fullDetail['android_push_id'] = $data['android_push_id'];
                $fullDetail['apple_push_id'] = $data['apple_push_id'];
                $fullDetail['status'] = $data['status'];
                $fullDetail['sent_status'] = $data['sent_status'];
                $fullDetail['is_read'] = $data['is_read'];


                $queryUserId = "select u.full_name as full_name, c.claim_id as claim_id from users u, claims c where u.user_id=c.user_id and c.claim_no='".$claimNo."'";
                $rsQuery1   = mysqli_query($con, $queryUserId);
                $numOfRows = mysqli_num_rows($rsQuery1);
                //traceMessage(print_r_log($queryUserId."mmmmmmmmmm"));
                if ($numOfRows > 0) {
                //traceMessage(print_r_log("mmmmmmmmmm1"));

                    while ($data1 = @mysqli_fetch_assoc($rsQuery1)) {
                //traceMessage(print_r_log("mmmmmmmmmm2"));

                        $name = $data1['full_name'];
                        $claimId = $data1['claim_id'];
                        
                        $fullDetail['from_notification'] = $name;
                        $fullDetail['claim_id'] = $claimId;

                        // array_push($notification, $fullDetail);
                    }
                }
                array_push($notification, $fullDetail);
        }

        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$notification);
        $response = json_encode($temp);
    }
    else{
        $notification = array();
        $temp = array("header"=>array("success"=>"1","message"=>"Not Showed!"),"body"=>$notification);
        $response = json_encode($temp);
    }
        echo $response;

    
     //echo json_encode($products);
}
elseif ($action == "get_lov") {

    $queryRegions = "select * from regions";
    $rsQuery   = mysqli_query($con, $queryRegions);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $regions = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($regions, $data);
        }
    }

    $queryZone = "select * from zone";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $zone = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($zone, $data);
        }
    }
    
    $queryBranch = "select * from branch";
    $rsQuery   = mysqli_query($con, $queryBranch);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $branch = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($branch, $data);
        }
    }
    
    $queryCity = "select * from city";
    $rsQuery   = mysqli_query($con, $queryCity);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $city = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($city, $data);
        }
    }
    
    $queryDealer = "select * from users where dealer_category_id!=1";
    $rsQuery   = mysqli_query($con, $queryDealer);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $dealer = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($dealer, $data);
        }
    }
    
    $queryItemModel = "select * from item_model";
    $rsQuery   = mysqli_query($con, $queryItemModel);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $itemModel = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($itemModel, $data);
        }
    }
    
    $queryItem = "select * from items";
    $rsQuery   = mysqli_query($con, $queryItem);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $items = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($items, $data);
        }
    }
    
    $queryItemGroup = "select * from item_group";
    $rsQuery   = mysqli_query($con, $queryItemGroup);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $itemGroup = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($itemGroup, $data);
        }
    }

    $queryItemType = "select * from item_type";
    $rsQuery   = mysqli_query($con, $queryItemType);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $itemType = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($itemType, $data);
        }
    }

    $queryDealerCategory = "select * from dealer_category";
    $rsQuery   = mysqli_query($con, $queryDealerCategory);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $dealerCategory = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($dealerCategory, $data);
        }
    }

    $queryDealerSubCategory = "select * from dealer_sub_category";
    $rsQuery   = mysqli_query($con, $queryDealerSubCategory);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $dealerSubCategory = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($dealerSubCategory, $data);
        }
    }
    
    $lovArray['regions'] = $regions;
    $lovArray['zone'] = $zone;
    $lovArray['branch'] = $branch;
    $lovArray['city'] = $city;
    $lovArray['dealer'] = $dealer;
    $lovArray['itemModel'] = $itemModel;
    $lovArray['items'] = $items;
    $lovArray['itemGroup'] = $itemGroup;
    $lovArray['itemType'] = $itemType;
    $lovArray['dealersubcategory'] = $dealerSubCategory;
    $lovArray['dealercategory'] = $dealerCategory;

    //json_encode($lovArray);
    $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$lovArray);
    $response = json_encode($temp);
    echo $response;
}
elseif ($action == "zones") {
   
    $queryZone = "select * from zone";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
		$arr = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $zone = array();
			$zone['zone_name'] = $data['zone_name'];
			$zone['zone_id'] = $data['zone_id'];
			
			$queryReg = "SELECT * FROM regions WHERE zone_id =".$data['zone_id'];
			$rsQueryReg   = mysqli_query($con, $queryReg);
			 while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
				 $region =array();
				 $region['region_name'] = $dataReg['region_name'];
			     $region['region_id'] = $dataReg['region_id'];
				
				  
				 $queryCity = "SELECT * FROM city WHERE region_id =".$dataReg['region_id'];
				 $rsQueryCity   = mysqli_query($con, $queryCity);
				 while ($dataCity = @mysqli_fetch_assoc($rsQueryCity)) {
					 $city =array();
					 $city['city_name'] = $dataCity['city_name'];
					 $city['city_id'] = $dataCity['city_id'];
					 
					 $queryArea = "SELECT * FROM `area` WHERE city_id =".$dataCity['city_id'];
					 $rsQueryArea   = mysqli_query($con, $queryArea);
					 while ($dataArea = @mysqli_fetch_assoc($rsQueryArea)) {
						 $area =array();
						 $area['area_name'] = $dataArea['area_name'];
						 $area['area_id'] = $dataArea['area_id'];
						 $city['area'][] = $area;
			 
						}  
					 $region['city'][] = $city;
		 
			        } 
				 $zone['region'][] = $region;	
			  }
			array_push($arr,$zone);			
        }
    }
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    // }
    // else
    // {
        // $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$zone);
        // $response = json_encode($temp);
        // echo $response;
    // }
    
}
elseif ($action == "get_parts") {
   
    // $queryZone = "SELECT * from item_model";
    // $rsQuery   = mysqli_query($con, $queryZone);
    // $numOfRows = mysqli_num_rows($rsQuery);
    // if ($numOfRows > 0) {
    //     $arr = array();
    //     while ($data = @mysqli_fetch_assoc($rsQuery)) {
    //         $model = array();
    //         $model['model_id'] = $data['item_model_id'];
    //         $model['model_name'] = $data['item_model_name'];
            
    //         $queryReg = "SELECT * FROM item_type WHERE item_model_id =".$data['item_model_id'];
    //         $rsQueryReg   = mysqli_query($con, $queryReg);
    //          while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
    //              $type =array();
				 
    //              $type['parts_name'] = $dataReg['item_type_name'];
    //              $type['parts_id'] = $dataReg['item_type_id'];
                
                  
    //              $queryCity = "SELECT * FROM item_group WHERE item_type_id =".$dataReg['item_type_id'];
    //              $rsQueryCity   = mysqli_query($con, $queryCity);
    //              while ($dataCity = @mysqli_fetch_assoc($rsQueryCity)) {
    //                  $group =array();
    //                  $group['category_name'] = $dataCity['item_group_name'];
    //                  $group['category_id'] = $dataCity['item_group_id'];
                      
    //                  $type['category'][] = $group;
         
    //                 } 
    //              $model['part'][] = $type;   
    //           }
    //         array_push($arr,$model);         
    //     }
    // }

    $queryCity = "SELECT * FROM item_group";
    $rsQueryCity   = mysqli_query($con, $queryCity);
    $arr = array();
    while ($dataCity = @mysqli_fetch_assoc($rsQueryCity)) {
        $group =array();
        $group['category_name'] = $dataCity['item_group_name'];
        $group['category_id'] = $dataCity['item_group_id'];

        $queryZone = "SELECT * from item_model where item_group_id=".$dataCity['item_group_id'];
        $rsQuery   = mysqli_query($con, $queryZone);
        //echo $numOfRows = mysqli_num_rows($rsQuery);
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
           $model = array();
           $model['model_id'] = $data['item_model_id'];
           $model['model_name'] = $data['item_model_name'];

           $queryReg = "SELECT * FROM item_type WHERE item_model_id =".$data['item_model_id'];
           $rsQueryReg   = mysqli_query($con, $queryReg);
           while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                $type =array();
                $type['parts_name'] = $dataReg['item_type_name'];
                $type['parts_id'] = $dataReg['item_type_id'];
                $model['part'][] = $type;
           }
           $group['model'][]=$model; 
        }
        array_push($arr,$group); 
    } 
    
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    // }
    // else
    // {
        // $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$zone);
        // $response = json_encode($temp);
        // echo $response;
    // }
    
}
elseif ($action == "get_category_sub_category") {
    $type = $_REQUEST["type"];

    if ($type=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Type Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    
    if ($type=="purchaseFrom") {
        $queryZone = "SELECT * from dealer_category where  dealer_category_name IN ('Mechanics','Distributor','SubDealers','Dealers')";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $arr = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $category = array();
            $category['category_id'] = $data['dealer_category_id'];
            $category['category_name'] = $data['dealer_category_name'];
            
            $queryReg = "SELECT * FROM dealer_sub_category WHERE dealer_category_id =".$data['dealer_category_id'];
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 $subcategories =array();
                 $subcategories['sub_category_id'] = $dataReg['sub_category_id'];
                 $subcategories['sub_category_name'] = $dataReg['sub_category_name'];
                 
                 $category['subcategories'][] = $subcategories;   
              }
            array_push($arr,$category);         
        }
    }
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    }
    else if ($type=="AccountOff") {
    $queryZone = "SELECT * from dealer_category where dealer_category_name IN ('Mechanics','Distributor','SubDealers','Dealers')";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $arr = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $category = array();
            $category['category_id'] = $data['dealer_category_id'];
            $category['category_name'] = $data['dealer_category_name'];
            
            $queryReg = "SELECT * FROM dealer_sub_category WHERE dealer_category_id =".$data['dealer_category_id'];
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 $subcategories =array();
                 $subcategories['sub_category_id'] = $dataReg['sub_category_id'];
                 $subcategories['sub_category_name'] = $dataReg['sub_category_name'];
                 
                 $category['subcategories'][] = $subcategories;   
              }
            array_push($arr,$category);         
        }
    }
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    }
    
    // }
    // else
    // {
        // $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$zone);
        // $response = json_encode($temp);
        // echo $response;
    // }
    
}
elseif ($action == "region_names") {
    $zoneId = $_REQUEST["zone_id"];
    if ($zoneId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No ZoneID Found!"),"body"=>$zoneId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $queryRegions = "select * from regions where zone_id=".$zoneId;
    $rsQuery   = mysqli_query($con, $queryRegions);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $regions = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($regions, $data);
        }
    }
    $countno =  count($regions);
    if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$regions);
        $response = json_encode($temp);
        echo $response;
    }
    else
    {
        $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$regions);
        $response = json_encode($temp);
        echo $response;
    }
}
elseif ($action == "dashboard_counter") {
    $userId = $_REQUEST["user_id"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Id Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    $queryRegions = "SELECT COUNT(claim_id) AS count,claim_status FROM claims WHERE user_id='".$userId."' GROUP BY claim_status";
    $rsQuery   = mysqli_query($con, $queryRegions);
    $numOfRows = mysqli_num_rows($rsQuery);
    $dashboardCounter = array();
    if ($numOfRows > 0) {
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $dashboardCounter[$data['claim_status']] = $data['count'];
        }
    }

    if (empty($dashboardCounter['open'])) {
        $dashboardCounter['open']=0;
    }
    if (empty($dashboardCounter['closed'])) {
        $dashboardCounter['closed']=0;
    }
    if (empty($dashboardCounter['completed'])) {
        $dashboardCounter['completed']=0;
    }
    if (empty($dashboardCounter['inprocess'])) {
        $dashboardCounter['inprocess']=0;
    }
    if (empty($dashboardCounter['approved'])) {
        $dashboardCounter['approved']=0;
    }

    $queryTransactions = "SELECT COUNT(claim_id) AS count FROM claim_transactions where user_id=".$userId." and status='pending' and assigned_to='purchase'";
    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
    $numOfRows = mysqli_num_rows($rsQueryTransactions);
    if ($numOfRows > 0) {
        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
        $dashboardCounter['approvedTaggingPurchase']=$dataClaimTrans['count'];
    }
    }

    $queryTransactions = "SELECT COUNT(claim_id) AS count FROM claim_transactions where user_id=".$userId." and status='pending' and assigned_to='account'";
    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
    $numOfRows = mysqli_num_rows($rsQueryTransactions);
    if ($numOfRows > 0) {
        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
        $dashboardCounter['approvedTaggingAccountOff']=$dataClaimTrans['count'];
    }
    }

    if (empty($dashboardCounter['approvedTaggingPurchase'])) {
        $dashboardCounter['approvedTaggingPurchase']=0;
    }
    if (empty($dashboardCounter['approvedTaggingAccountOff'])) {
        $dashboardCounter['approvedTaggingAccountOff']=0;
    }
    
        $temp = array("header"=>array("success"=>"1","message"=>""),"body"=>$dashboardCounter);
        $response = json_encode($temp);
        echo $response;
}

elseif ($action == "city_names") {
    $regionId = $_REQUEST["region_id"];
    if ($regionId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No RegionID Found!"),"body"=>$regionId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $queryCity = "select * from city where region_id=".$regionId;
    $rsQuery   = mysqli_query($con, $queryCity);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $city = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($city, $data);
        }
    }
    $countno =  count($city);
    if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$city);
        $response = json_encode($temp);
        echo $response;
    }
    else
    {
        $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$city);
        $response = json_encode($temp);
        echo $response;
    }

}
elseif ($action == "area_names") {
    $cityId = $_REQUEST["city_id"];
    if ($cityId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No CityID Found!"),"body"=>$cityId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $queryArea = "select * from area where city_id=".$cityId;
    $rsQuery   = mysqli_query($con, $queryArea);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $area = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            array_push($area, $data);
        }
    }
    $countno =  count($area);
    if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$area);
        $response = json_encode($temp);
        echo $response;
    }
    else
    {
        $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>$area);
        $response = json_encode($temp);
        echo $response;
    }

}
elseif ($action == "purchasefrom_update") {

    $assignedTo = "purchase";
    $claimDate = date("Y-m-d H:i:s");
    $claimId = $_REQUEST["claim_id"];
    $newPurchaseFromId = $_REQUEST["new_purchase_from_id"];
    $userId = $_REQUEST["user_id"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Claim Id Found!"),"body"=>$claimId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($newPurchaseFromId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Purchase From Found!"),"body"=>$newPurchaseFromId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimId."";
    $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
    $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
    if ($numOfRows > 0) {
        while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
        }
    }
    //
    $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
    $numOfRows = mysqli_num_rows($rsQueryTransactions);
    if ($numOfRows > 0) {
        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
        }
    }
    //
    $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and transaction_id='".$claimTransctionsId."' and assigned_to='purchase'";
    $result1=mysqli_query($con,$queryTransactionsUpdate);
    //
    $queryTransactionsUpdateLog="DELETE FROM claim_transactions_log where claim_id='".$claimId."' and claim_transactions_log_id='".$claimTransctionsLogId."' and assigned_to='purchase'";
    $result1=mysqli_query($con,$queryTransactionsUpdateLog);
    //

    $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newPurchaseFromId','purchase','pending','$claimDate')";
    $result1=mysqli_query($con,$addClaimTransactionsQuery);
        //
    $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newPurchaseFromId','purchase','pending','$claimDate')";
    $result1=mysqli_query($con,$addClaimTransactionsLogQuery);




    // $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
    // $rsQueryUserId   = mysqli_query($con, $queryUserId);
    // $numOfRows = mysqli_num_rows($rsQueryUserId);
    // if ($numOfRows > 0) {
    //     while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
    //             $claimerId = $dataClaimUser['user_id'];
    //             $claimNo = $dataClaimUser['claim_no'];
    //     }
    // }
    // $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
    //     $rsQueryReg   = mysqli_query($con, $queryPurc);
    //     $totalData1= @mysqli_fetch_assoc($rsQueryReg);
    //     $purchaseFromName = $totalData1['full_name'];

    // $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
    //     $rsQueryReg   = mysqli_query($con, $queryPurc);
    //     $totalData= @mysqli_fetch_assoc($rsQueryReg);
    //     $name = $totalData['full_name'];
    //     $email = $totalData['email'];
    //     $androidPushId = $totalData['android_push_id'];
    //     $applePushId = $totalData['apple_push_id'];

    // $message = "Dear $name, $purchaseFromName has rejected as a Purchase From Person on your claim ($claimNo).";
    // notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo);


    $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
    $rsQueryUserId   = mysqli_query($con, $queryUserId);
    $numOfRows = mysqli_num_rows($rsQueryUserId);
    if ($numOfRows > 0) {
        while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                $claimerId = $dataClaimUser['user_id'];
                $claimNo = $dataClaimUser['claim_no'];
        }
    }

    $queryPurc1 = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $newPurchaseFromId;
        $rsQueryReg1   = mysqli_query($con, $queryPurc1);
        $totalData1= @mysqli_fetch_assoc($rsQueryReg1);
        $purchaseFromNameNew = $totalData1['full_name'];
        //$name = $totalData['full_name'];
        $emailPurchaseFromNameNew = $totalData1['email'];
        $androidPushIdPurchaseFromNameNew = $totalData1['android_push_id'];
        $applePushIdPurchaseFromNameNew = $totalData1['apple_push_id'];

    $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
        $rsQueryReg   = mysqli_query($con, $queryPurc);
        $totalData= @mysqli_fetch_assoc($rsQueryReg);
        $name = $totalData['full_name'];
        $email = $totalData['email'];
        $androidPushId = $totalData['android_push_id'];
        $applePushId = $totalData['apple_push_id'];
        $page="open_claims";

    $message = "Dear $name, Purchase From of your claim ($claimNo) is now changed to $purchaseFromNameNew.";
    notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);
    $page="purchase_from_tagging";
    $message1 = "Dear $purchaseFromNameNew, You are nominated as a Purchase From on claim ($claimNo) from $name";
    notifications($newPurchaseFromId,$emailPurchaseFromNameNew,$androidPushIdPurchaseFromNameNew,$applePushIdPurchaseFromNameNew,$message1,$claimNo,$page);

    $temp = array("header"=>array("success"=>"1","message"=>"Successfully Updated!"),"body"=>$status);
    $response = json_encode($temp);
    echo $response;

}
elseif ($action == "accountoff_update") {

    $assignedTo = "account";
    $claimDate = date("Y-m-d H:i:s");
    $claimId = $_REQUEST["claim_id"];
    $userId = $_REQUEST["user_id"];
    $newAccountOfId = $_REQUEST["newaccountofid"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($newAccountOfId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Account Of Found!"),"body"=>$newAccountOfId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Claim Id Found!"),"body"=>$claimId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='account' AND claim_id=".$claimId."";
    $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
    $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
    if ($numOfRows > 0) {
        while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
        }
    }
    //
    $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
    $numOfRows = mysqli_num_rows($rsQueryTransactions);
    if ($numOfRows > 0) {
        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
        }
    }
    //
    if ($claimTransctionsId!="") {
    $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and transaction_id='".$claimTransctionsId."' and assigned_to='account'";
    $result1=mysqli_query($con,$queryTransactionsUpdate);
    //
    $queryTransactionsUpdateLog="DELETE FROM claim_transactions_log where claim_id='".$claimId."' and claim_transactions_log_id='".$claimTransctionsLogId."' and assigned_to='account'";
    $result1=mysqli_query($con,$queryTransactionsUpdateLog);
    }

    $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newAccountOfId','account','pending','$claimDate')";
    $result1=mysqli_query($con,$addClaimTransactionsQuery);
        //
    $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newAccountOfId','account','pending','$claimDate')";
    $result1=mysqli_query($con,$addClaimTransactionsLogQuery);
    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
    $rsQueryUserId   = mysqli_query($con, $queryUserId);
    $numOfRows = mysqli_num_rows($rsQueryUserId);
    if ($numOfRows > 0) {
        while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                $claimerId = $dataClaimUser['user_id'];
                $claimNo = $dataClaimUser['claim_no'];
        }
    }

    $queryPurc1 = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $newAccountOfId;
        $rsQueryReg1   = mysqli_query($con, $queryPurc1);
        $totalData1= @mysqli_fetch_assoc($rsQueryReg1);
        $purchaseFromNameNew = $totalData1['full_name'];
        //$name = $totalData['full_name'];
        $emailPurchaseFromNameNew = $totalData1['email'];
        $androidPushIdPurchaseFromNameNew = $totalData1['android_push_id'];
        $applePushIdPurchaseFromNameNew = $totalData1['apple_push_id'];

    $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
        $rsQueryReg   = mysqli_query($con, $queryPurc);
        $totalData= @mysqli_fetch_assoc($rsQueryReg);
        $name = $totalData['full_name'];
        $email = $totalData['email'];
        $androidPushId = $totalData['android_push_id'];
        $applePushId = $totalData['apple_push_id'];
        $page="open_claims";
    $message = "Dear $name, AccountOf of your claim ($claimNo) is now changed to $purchaseFromNameNew.";
    notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);
        $page="Accoun_of_tagging";
    $message1 = "Dear $purchaseFromNameNew, You are nominated as a Account Off on claim ($claimNo) from $name";
    notifications($newAccountOfId,$emailPurchaseFromNameNew,$androidPushIdPurchaseFromNameNew,$applePushIdPurchaseFromNameNew,$message1,$claimNo);


    $temp = array("header"=>array("success"=>"1","message"=>"Successfully Updated!"),"body"=>$status);
    $response = json_encode($temp);
    echo $response;
}
elseif ($action == "edit_claim") {
    $claimType = $_REQUEST["claim_type"];
    $claimType = strtolower($claimType);
    $purchaseFrom = $_REQUEST["purchase_from"];
    $accountOff = $_REQUEST["account_of"];
    $userId = $_REQUEST["user_id"];
    $remarks = $_REQUEST["remarks"];
    $claimId = $_REQUEST["claim_id"];
    $claimDate = date("Y-m-d H:i:s");

       traceMessage("111111111111111111111");
    $productDetail = $_REQUEST["product_details"];
    $infoArray = json_decode($productDetail);
    $count=count($infoArray);
    if ($count<1) {
        $temp = array("header"=>array("success"=>"1","message"=>"Product Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Claim Id Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimDate=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Date Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimType=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Claim Type Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
       traceMessage("2222222222222222222222222222");

    $queryPurc = "SELECT * from claims  where claim_id=". $claimId;
        $rsQueryReg   = mysqli_query($con, $queryPurc);
        $totalData1= @mysqli_fetch_assoc($rsQueryReg);
        $claimNo = $totalData1['claim_no'];
        $number = substr($claimNo, 2);

    if ($claimType=="claim") {
        $claimNo = "CL".$number;
    }
    else if ($claimType=="return")
    {
        $claimNo = "SR".$number;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Claim Type!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($purchaseFrom=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Purchased From Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimNo=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Claim No Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $claimTypeArr = array('claim','return');
    if (!in_array($claimType, $claimTypeArr)) {
        $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Claim Type!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
       traceMessage("33333333333333333333".$claimType);

    $claimStatus="open";
    
    //echo $claimNo.$claimType.$remarks.$claimDate;
    $updateClaimQuery="UPDATE claims SET claim_no='$claimNo' , claim_type='$claimType' , remarks='$remarks' , updated_at='$claimDate' where claim_id=".$claimId;
    traceMessage("sql:$updateClaimQuery");
    $result=mysqli_query($con,$updateClaimQuery);



    $queryClaimDetailsDelete="DELETE FROM claim_details where claim_id='".$claimId."'";
    $result1=mysqli_query($con,$queryClaimDetailsDelete);
    
       traceMessage("4444444444444444444444".$claimType);

    $productDetail = $_REQUEST["product_details"];

    $infoArray = json_decode($productDetail);
    for($i=0;$i<count($infoArray);$i++)
    {    
        $itemId=$infoArray[$i]->item_id;
        $recievedQty = $infoArray[$i]->recieved_qty;
        $remarks = $infoArray[$i]->remarks;
        $createdAt = $infoArray[$i]->created_at;

        $addClaimDetailQuery="INSERT INTO `claim_details` (`claim_detail_id`,`claim_id`,`item_id`,`created_at`,`recieved_qty`,`remarks`) VALUES (`claim_detail_id`,'$claimId','$itemId','$createdAt','$recievedQty','$remarks')";
        traceMessage("sql:$addClaimDetailQuery");
        $result1=mysqli_query($con,$addClaimDetailQuery);
    }
       traceMessage("555555555555555555555555555".$claimType);

    if ($purchaseFrom!="") {
        if ($purchaseFrom!="0") {
            $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimId."";
            $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
            $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
            if ($numOfRows > 0) {
                while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                        $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
                }
            }
            //
            $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
            $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
            $numOfRows = mysqli_num_rows($rsQueryTransactions);
            if ($numOfRows > 0) {
                while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                        $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                }
            }
            //
            $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and transaction_id='".$claimTransctionsId."' and assigned_to='purchase'";
            $result1=mysqli_query($con,$queryTransactionsUpdate);
            //
            $queryTransactionsUpdateLog="DELETE FROM claim_transactions_log where claim_id='".$claimId."' and claim_transactions_log_id='".$claimTransctionsLogId."' and assigned_to='purchase'";
            $result1=mysqli_query($con,$queryTransactionsUpdateLog);
            //

            $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$purchaseFrom','purchase','pending','$claimDate')";
            $result1=mysqli_query($con,$addClaimTransactionsQuery);
                //
            $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$purchaseFrom','purchase','pending','$claimDate')";
            $result1=mysqli_query($con,$addClaimTransactionsLogQuery);

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFrom;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";

            $message = "Dear $name, You are nominated as a Purchase From Person on claim ($claimNo).";
            notifications($purchaseFrom,$email,$androidPushId,$applePushId,$message,$claimNo,$page);


        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Purchase From!"),"body"=>$obj);
            $response = json_encode($temp);
            echo $response;
            exit();
        }
    }

    if ($accountOff!="") {
        if ($accountOff!="0") {
            $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='account' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
        $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
        $numOfRows = mysqli_num_rows($rsQueryTransactions);
        if ($numOfRows > 0) {
            while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                    $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
            }
        }
        //
        if ($claimTransctionsId!="") {
        $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and transaction_id='".$claimTransctionsId."' and assigned_to='account'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="DELETE FROM claim_transactions_log where claim_id='".$claimId."' and claim_transactions_log_id='".$claimTransctionsLogId."' and assigned_to='account'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        }

        $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$accountOff','account','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsQuery);
            //
        $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$accountOff','account','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsLogQuery);



        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $accountOff;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";

        $message = "Dear $name, You are nominated as an Account Of Person on claim ($claimNo).";
        notifications($accountOff,$email,$androidPushId,$applePushId,$message,$claimNo,$page);

        }
        else{
            $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Account Off!"),"body"=>$obj);
            $response = json_encode($temp);
            echo $response;
            exit();
        }
    }
       traceMessage("66666666666666666666666666666".$claimType);

    $arr = array();
    $arr['claim_id'] = $claimId;

    $queryClaimDetailsDelete="DELETE FROM claim_images where claim_id='".$claimId."'";
    $result1=mysqli_query($con,$queryClaimDetailsDelete);

    // yaha se start ha claim images wala kaam.
    traceMessage(print_r_log($_FILES["claim_image"]."ANAS CHECK KRO YAHA LOG ARHI!"));

    
        if (isset($_FILES["claim_image"])) {
            // $inboxImages = $_FILES['claim_image'];
            $scount      = count($_FILES['claim_image']['name']);
            $folderId    = time() . $claimID;

            for ($s = 0; $s < $scount; $s++) {
                traceMessage("timess--------------$s");
                $files = count($_FILES['claim_image']['name']);
                traceMessage("files--------------$files");
                $targetFolder = UPLOAD_DIRECTORY_PATH . "claim_image/";// . $folderId . "/";
                if (!is_dir($targetFolder)) {
                    traceMessage("creating directory--By Date------:$targetFolder");
                    mkdir($targetFolder);
                }
                $targetFolder = UPLOAD_DIRECTORY_PATH . "claim_image/" . $folderId . "/";
                $browsePath   = BROWSE_PATH . "claim_image/" . $folderId . "/";

                if (!is_dir($targetFolder)) {
                    traceMessage("creating directory--By Date------:$targetFolder");
                    mkdir($targetFolder);
                }
                $targetFolder = $targetFolder . basename($_FILES['claim_image']['name'][$s]);
                $browsePath   = $browsePath . basename($_FILES['claim_image']['name'][$s]);
                $fileName     = $_FILES['claim_image']['tmp_name'][$s];
                $name         = $_FILES['claim_image']['name'][$s];
                $size         = $_FILES['claim_image']['size'][$s];
                $type         = $_FILES['claim_image']['type'][$s];
                if ($fileName != "") {
                    traceMessage("coming");
                    if (move_uploaded_file($fileName, $targetFolder)) {
                        //save the filename
                        traceMessage("success");
                        //when file uploaded successully
                        $addClaimImageQuery="INSERT INTO `claim_images` (`claim_id`,`claim_image_url`,`status`) VALUES ('$claimId','$browsePath',1)";
                        $result2=mysqli_query($con,$addClaimImageQuery);
                    } else {
                        traceMessage("no success");
                        // $browsePath = "";
                        //when file not uploaded successully
                    }
                }
            }
        }

       traceMessage("7777777777777777777777".$claimType);

    
    $temp = array("header"=>array("success"=>"1","message"=>"Successfully Updated!"),"body"=>$arr);
    $response = json_encode($temp);
    echo $response;
}
elseif ($action == "accept_reject_change_purchasefrom") {
    $status = $_REQUEST["status"];
    $status = strtolower($status);
    $assignedTo = "purchase";
    $claimDate = date("Y-m-d H:i:s");
    $claimId = $_REQUEST["claim_id"];
    $newPurchaseFromId = $_REQUEST["new_purchase_from_id"];
    $userId = $_REQUEST["user_id"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($status=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Status Found!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Status Found!"),"body"=>$claimId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($status=="accept") {
        
        $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
        $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
        $numOfRows = mysqli_num_rows($rsQueryTransactions);
        if ($numOfRows > 0) {
            while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                    $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
            }
        }
        //
        $queryTransactionsUpdate="UPDATE claim_transactions SET status='".$status."' where transaction_id='".$claimTransctionsId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="UPDATE claim_transactions_log SET status='".$status."' where claim_transactions_log_id='".$claimTransctionsLogId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        //
        $queryTransactionsUserId = "SELECT user_id FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
        $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
        if ($numOfRows > 0) {
            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                    $PurchaseFromId = $dataClaimTransUser['user_id'];
            }
        }
        //
        $queryPurchaseFromUpdate="UPDATE claims SET purchased_from='".$PurchaseFromId."' where claim_id='".$claimId."'";
        $result1=mysqli_query($con,$queryPurchaseFromUpdate);
        //
        $queryTransactionsAccountId = "SELECT account_of FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryTransactionsAccountId   = mysqli_query($con, $queryTransactionsAccountId);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsAccountId);
        if ($numOfRows > 0) {
            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsAccountId)) {
                    $accountOfId = $dataClaimTransUser['account_of'];
            }
        }

        if ($accountOfId!="" ) {
            if ( $accountOfId!="0") {
                $queryStatusUpdate="UPDATE claims SET claim_status='approved' where claim_id='".$claimId."'";
                $result1=mysqli_query($con,$queryStatusUpdate);
            }
        }


        $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryUserId   = mysqli_query($con, $queryUserId);
        $numOfRows = mysqli_num_rows($rsQueryUserId);
        if ($numOfRows > 0) {
            while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                    $claimerId = $dataClaimUser['user_id'];
                    $claimNo = $dataClaimUser['claim_no'];
            }
        }


        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg);
            $purchaseFromName = $totalData1['full_name'];

        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";
        $message = "Dear $name, $purchaseFromName has accepted as a Purchase From Person on your claim ($claimNo).";
        notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);

        $temp = array("header"=>array("success"=>"1","message"=>"Successfully ".$status."!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;

    }
    else if ($status=="change_purchased_from") {

        if ($newPurchaseFromId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Select New Purchase From!"),"body"=>$newPurchaseFromId);
        $response = json_encode($temp);
        echo $response;
        exit();
        }
        
        $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
        $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
        $numOfRows = mysqli_num_rows($rsQueryTransactions);
        if ($numOfRows > 0) {
            while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                    $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
            }
        }
        //
        $queryTransactionsUpdate="UPDATE claim_transactions SET status='".$status."' where transaction_id='".$claimTransctionsId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="UPDATE claim_transactions_log SET status='".$status."' where claim_transactions_log_id='".$claimTransctionsLogId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        //
        $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newPurchaseFromId','purchase','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsQuery);
        //
        $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$newPurchaseFromId','purchase','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsLogQuery);
        //


        $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryUserId   = mysqli_query($con, $queryUserId);
        $numOfRows = mysqli_num_rows($rsQueryUserId);
        if ($numOfRows > 0) {
            while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                    $claimerId = $dataClaimUser['user_id'];
                    $claimNo = $dataClaimUser['claim_no'];
            }
        }
        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg);
            $purchaseFromName = $totalData1['full_name'];

        $queryPurc1 = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $newPurchaseFromId;
            $rsQueryReg1   = mysqli_query($con, $queryPurc1);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg1);
            $purchaseFromNameNew = $totalData1['full_name'];
            //$name = $totalData['full_name'];
            $emailPurchaseFromNameNew = $totalData1['email'];
            $androidPushIdPurchaseFromNameNew = $totalData1['android_push_id'];
            $applePushIdPurchaseFromNameNew = $totalData1['apple_push_id'];

        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";
        $message = "Dear $name, Purchase From of your claim ($claimNo) is now changed from $purchaseFromName to $purchaseFromNameNew.";
        notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);
        $page="purchase_from_tagging";
        $message1 = "Dear $purchaseFromNameNew, You are nominated as a Purchase From on claim ($claimNo) from $purchaseFromName";
        notifications($newPurchaseFromId,$emailPurchaseFromNameNew,$androidPushIdPurchaseFromNameNew,$applePushIdPurchaseFromNameNew,$message1,$claimNo,$page);
        
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Change Purchased From!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;

    }
    else if ($status=="rejected") {

        $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='purchase' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and assigned_to='purchase'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="UPDATE claim_transactions_log SET status='".$status."' where claim_transactions_log_id='".$claimTransctionsLogId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        //

        $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryUserId   = mysqli_query($con, $queryUserId);
        $numOfRows = mysqli_num_rows($rsQueryUserId);
        if ($numOfRows > 0) {
            while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                    $claimerId = $dataClaimUser['user_id'];
                    $claimNo = $dataClaimUser['claim_no'];
            }
        }
        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg);
            $purchaseFromName = $totalData1['full_name'];

        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";
        $message = "Dear $name, $purchaseFromName has rejected as a Purchase From Person on your claim ($claimNo).";
        notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);


        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Rejected!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;

    }
    else 
    {
        $temp = array("header"=>array("success"=>"1","message"=>"Wrong Status!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;
    }


}
elseif ($action == "accept_reject_change_accountoff") {
    $status = $_REQUEST["status"];
    $status = strtolower($status);
    $assignedTo = "account";
    $claimDate = date("Y-m-d H:i:s");
    $claimId = $_REQUEST["claim_id"];
    $userId = $_REQUEST["user_id"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($status=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Status Found!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Status Found!"),"body"=>$claimId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($status=="accept") {
        
        $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='account' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
        $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
        $numOfRows = mysqli_num_rows($rsQueryTransactions);
        if ($numOfRows > 0) {
            while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                    $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
            }
        }
        //
        $queryTransactionsUpdate="UPDATE claim_transactions SET status='".$status."' where transaction_id='".$claimTransctionsId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="UPDATE claim_transactions_log SET status='".$status."' where claim_transactions_log_id='".$claimTransctionsLogId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        //
        $queryTransactionsUserId = "SELECT user_id FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
        $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
        if ($numOfRows > 0) {
            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                    $accountoffId = $dataClaimTransUser['user_id'];
            }
        }
        //
        $queryPurchaseFromUpdate="UPDATE claims SET account_of='".$accountoffId."' where claim_id='".$claimId."'";
        $result1=mysqli_query($con,$queryPurchaseFromUpdate);
        //
        $queryTransactionsAccountId = "SELECT purchased_from FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryTransactionsAccountId   = mysqli_query($con, $queryTransactionsAccountId);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsAccountId);
        if ($numOfRows > 0) {
            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsAccountId)) {
                    $pruchaseFromIdCheck = $dataClaimTransUser['purchased_from'];
            }
        }

        if ($pruchaseFromIdCheck!="" ) {
            if ( $pruchaseFromIdCheck!="0") {
                $queryStatusUpdate="UPDATE claims SET claim_status='approved' where claim_id='".$claimId."'";
                $result1=mysqli_query($con,$queryStatusUpdate);
            }
        }


        $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryUserId   = mysqli_query($con, $queryUserId);
        $numOfRows = mysqli_num_rows($rsQueryUserId);
        if ($numOfRows > 0) {
            while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                    $claimerId = $dataClaimUser['user_id'];
                    $claimNo = $dataClaimUser['claim_no'];
            }
        }


        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg);
            $accountOffName = $totalData1['full_name'];

        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";
        $message = "Dear $name, $accountOffName has accepted as a Account Of Person on your claim ($claimNo).";
        notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);


        $temp = array("header"=>array("success"=>"1","message"=>"Successfully ".$status."!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;

    }
    else if ($status=="rejected") {

        
        $queryTransactionsLog = "SELECT MAX(claim_transactions_log_id) as claimTransctionsLogId  FROM claim_transactions_log WHERE assigned_to='account' AND claim_id=".$claimId."";
        $rsQueryTransactionsLog   = mysqli_query($con, $queryTransactionsLog);
        $numOfRows = mysqli_num_rows($rsQueryTransactionsLog);
        if ($numOfRows > 0) {
            while ($dataClaimTransLog = @mysqli_fetch_assoc($rsQueryTransactionsLog)) {
                    $claimTransctionsLogId = $dataClaimTransLog['claimTransctionsLogId'];
            }
        }
        //
        $queryTransactionsUpdate="DELETE FROM claim_transactions where claim_id='".$claimId."' and assigned_to='account'";
        $result1=mysqli_query($con,$queryTransactionsUpdate);
        //
        $queryTransactionsUpdateLog="UPDATE claim_transactions_log SET status='".$status."' where claim_transactions_log_id='".$claimTransctionsLogId."'";
        $result1=mysqli_query($con,$queryTransactionsUpdateLog);
        //


        $queryUserId = "SELECT user_id,claim_no FROM claims WHERE  claim_id=".$claimId."";
        $rsQueryUserId   = mysqli_query($con, $queryUserId);
        $numOfRows = mysqli_num_rows($rsQueryUserId);
        if ($numOfRows > 0) {
            while ($dataClaimUser = @mysqli_fetch_assoc($rsQueryUserId)) {
                    $claimerId = $dataClaimUser['user_id'];
                    $claimNo = $dataClaimUser['claim_no'];
            }
        }
        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $userId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData1= @mysqli_fetch_assoc($rsQueryReg);
            $accountOfName = $totalData1['full_name'];

        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimerId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="open_claims";

        $message = "Dear $name, $accountOfName has rejected as a Account Of Person on your claim ($claimNo).";
        notifications($claimerId,$email,$androidPushId,$applePushId,$message,$claimNo,$page);

        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Rejected!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;

    }
    else 
    {
        $temp = array("header"=>array("success"=>"1","message"=>"Wrong Status!"),"body"=>$status);
        $response = json_encode($temp);
        echo $response;
    }
}
elseif ($action == "raise_claim") {
    $claimType = $_REQUEST["claim_type"];
    $claimType = strtolower($claimType);
    $claimDate = date("Y-m-d H:i:s");
    $purchaseFrom = $_REQUEST["purchase_from"];
    $accountOff = $_REQUEST["account_of"];
    $userId = $_REQUEST["user_id"];
    $remarks = $_REQUEST["remarks"];

    $queryClaimNo = "SELECT max(claim_id) as maxclaim FROM claims";
    $rsQueryReg   = mysqli_query($con, $queryClaimNo);
    $dataReg = @mysqli_fetch_assoc($rsQueryReg);
    $claimNo = $dataReg['maxclaim'];
    $claimNo++;


    //$claimStatus = $_REQUEST["claim_status"];
    $productDetail = $_REQUEST["product_details"];
    $infoArray = json_decode($productDetail);
    $count=count($infoArray);
    if ($count<1) {
        $temp = array("header"=>array("success"=>"1","message"=>"Product Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimType=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Claim Type Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimType=="claim") {
        $claimNo = "CL".$claimNo;
    }
    else if ($claimType=="return")
    {
        $claimNo = "SR".$claimNo;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Claim Type!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($claimDate=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Date Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($purchaseFrom=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Purchased From Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($claimNo=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Claim No Required!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $claimTypeArr = array('claim','return');
    if (!in_array($claimType, $claimTypeArr)) {
        $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Claim Type!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    
        $claimStatus="open";
    
    
    
    $addClaimQuery="INSERT INTO `claims` (`claim_no`,`user_id`,`claim_type`,`created_at`,`remarks`,`claim_status`) VALUES ('$claimNo','$userId','$claimType','$claimDate','$remarks','$claimStatus')";
    traceMessage("sql:$addClaimQuery");
    $result=mysqli_query($con,$addClaimQuery);
    $claimID=mysqli_insert_id($con);
    $productDetail = $_REQUEST["product_details"];

    $infoArray = json_decode($productDetail);
    for($i=0;$i<count($infoArray);$i++)
    {    
        $itemId=$infoArray[$i]->item_id;
        $recievedQty = $infoArray[$i]->recieved_qty;
        $remarks = $infoArray[$i]->remarks;
        $createdAt = $infoArray[$i]->created_at;

        $addClaimDetailQuery="INSERT INTO `claim_details` (`claim_detail_id`,`claim_id`,`item_id`,`created_at`,`recieved_qty`,`remarks`) VALUES (`claim_detail_id`,'$claimID','$itemId','$claimDate','$recievedQty','$remarks')";
        traceMessage("sql:$addClaimDetailQuery");
        $result1=mysqli_query($con,$addClaimDetailQuery);
    }

    traceMessage("purchase start");

    if ($purchaseFrom!="") {
        if ($purchaseFrom!="0") {
            traceMessage("purchase in");
        $addClaimTransactionsQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimID','$purchaseFrom','purchase','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsQuery);

        $addClaimTransactionsLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimID','$purchaseFrom','purchase','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsLogQuery);


       
        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFrom;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="purchase_from_tagging";

        $message = "Dear $name, You are nominated as a Purchase From Person on claim ($claimNo).";
        notifications($purchaseFrom,$email,$androidPushId,$applePushId,$message,$claimNo,$page);


        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Purchase From!"),"body"=>$obj);
            $response = json_encode($temp);
            echo $response;
            exit();
        }
    }
traceMessage("account start");
    if ($accountOff!="") {
        if ($accountOff!="0") {
traceMessage("account in");

        $addClaimTransactionsAccountQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimID','$accountOff','account','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsAccountQuery);

        $addClaimTransactionsLogAccountQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimID','$accountOff','account','pending','$claimDate')";
        $result1=mysqli_query($con,$addClaimTransactionsLogAccountQuery);


        $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $accountOff;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $name = $totalData['full_name'];
            $email = $totalData['email'];
            $androidPushId = $totalData['android_push_id'];
            $applePushId = $totalData['apple_push_id'];
            $page="Account_of_tagging";

        $message = "Dear $name, You are nominated as an Account Of Person on claim ($claimNo).";
        notifications($accountOff,$email,$androidPushId,$applePushId,$message,$claimNo,$page);

        }
        else{
            $temp = array("header"=>array("success"=>"1","message"=>"Incorrect Account Off!"),"body"=>$obj);
            $response = json_encode($temp);
            echo $response;
            exit();
        }
    }
    $arr = array();
    $arr['claim_id'] = $claimID;



    // yaha se start ha claim images wala kaam.
    traceMessage(print_r_log($_FILES["claim_image"]."ANAS CHECK KRO YAHA LOG ARHI!"));

    
        if (isset($_FILES["claim_image"])) {
            // $inboxImages = $_FILES['claim_image'];
            $scount      = count($_FILES['claim_image']['name']);
            $folderId    = time() . $claimID;

            for ($s = 0; $s < $scount; $s++) {
                traceMessage("timess--------------$s");
                $files = count($_FILES['claim_image']['name']);
                traceMessage("files--------------$files");
                $targetFolder = UPLOAD_DIRECTORY_PATH . "claim_image/";// . $folderId . "/";
                if (!is_dir($targetFolder)) {
                    traceMessage("creating directory--By Date------:$targetFolder");
                    mkdir($targetFolder);
                }
                $targetFolder = UPLOAD_DIRECTORY_PATH . "claim_image/" . $folderId . "/";
                $browsePath   = BROWSE_PATH . "claim_image/" . $folderId . "/";

                if (!is_dir($targetFolder)) {
                    traceMessage("creating directory--By Date------:$targetFolder");
                    mkdir($targetFolder);
                }
                $targetFolder = $targetFolder . basename($_FILES['claim_image']['name'][$s]);
                $browsePath   = $browsePath . basename($_FILES['claim_image']['name'][$s]);
                $fileName     = $_FILES['claim_image']['tmp_name'][$s];
                $name         = $_FILES['claim_image']['name'][$s];
                $size         = $_FILES['claim_image']['size'][$s];
                $type         = $_FILES['claim_image']['type'][$s];
                if ($fileName != "") {
                    traceMessage("coming");
                    if (move_uploaded_file($fileName, $targetFolder)) {
                        //save the filename
                        traceMessage("success");
                        //when file uploaded successully
                        $addClaimImageQuery="INSERT INTO `claim_images` (`claim_id`,`claim_image_url`,`status`) VALUES ('$claimID','$browsePath',1)";
                        $result2=mysqli_query($con,$addClaimImageQuery);
                    } else {
                        traceMessage("no success");
                        // $browsePath = "";
                        //when file not uploaded successully
                    }
                }
            }
        }



    $temp1 = array("header"=>array("success"=>"1","message"=>"Recorded Successfully Inserted"),"body"=>$arr);
    $response = json_encode($temp1);
    echo $response;
    exit();
    //  $temp1 = array("header"=>array("success"=>"1","message"=>"Recorded Successfully Inserted"),"body"=>$obj);

    // $response1 = json_encode($temp1);
    // return $response1;
}
// elseif ($action == "view_claim") {
//     $userId = $_REQUEST["user_id"];

//     $queryViewClaim = "select c.created_at as abc,c.*,d.*,i.* from claims c, claim_details d,items i where c.claim_id=d.claim_id and i.item_id=d.item_id and c.user_id='".$userId."'";
//     $rsQuery   = mysqli_query($con, $queryViewClaim);
//     $numOfRows = mysqli_num_rows($rsQuery);
//     if ($numOfRows > 0) {
//         $arr = array();
//          $claimDetails = array();
//         while ($data = @mysqli_fetch_assoc($rsQuery)) {
//             $claimId = $data['claim_id'];
//             $purchasedFromId = $data['purchased_from'];
//             $accountOfId = $data['account_of'];
//             $Date = $data['abc'];
//             $Date=substr($Date,0,11);
//             $Date=date_create($Date);                     
//             $Date = date_format($Date,"d-m-Y");
//             $data['date'] = $Date;

//             $queryPurc = "SELECT full_name from users where user_id=".$purchasedFromId;
//             $rsQueryReg   = mysqli_query($con, $queryPurc);
//             $totalData= @mysqli_fetch_assoc($rsQueryReg);
//             $data['Purchase_form_name'] = $totalData['full_name'];
//             array_push($claimDetails, $data);

//             $queryAcc = "SELECT full_name from users where user_id=".$accountOfId;
//             $rsQueryReg   = mysqli_query($con, $queryAcc);
//             $totalData= @mysqli_fetch_assoc($rsQueryReg);
//             $data['accountOfName'] = $totalData['full_name'];
//             array_push($claimDetails, $data);

//             $queryReg = "SELECT SUM(d.`recieved_qty`) as total_quantity,c.`claim_id` FROM claims c, claim_details d,items i WHERE c.claim_id=d.claim_id AND i.item_id=d.item_id AND c.user_id='".$userId."' AND c.claim_id='".$claimId."' GROUP BY d.`claim_id`";
//             $rsQueryReg   = mysqli_query($con, $queryReg);
//             $totalData= @mysqli_fetch_assoc($rsQueryReg);
//             $data['total_quantity'] = $totalData['total_quantity'];
//             array_push($claimDetails, $data);
           
            
//     }
//     $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimDetails);
//     $response = json_encode($temp);
//     echo $response;
// }
// }

elseif ($action == "view_approve_tagging_accountoff") {
    $userId = $_REQUEST["user_id"];
    $selectedDate = $_REQUEST["selecteddate"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"User Not Found!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
 //IN ('MECHANICS','SUBDEALERS','DEALERS')
    if ($selectedDate!="") {
        $selectedDate = date('Y-m-d',strtotime($selectedDate));
//and status='pending'
        $arr = array();
$queryTransactions = "SELECT * FROM claim_transactions where user_id=".$userId."  and assigned_to='account' ORDER BY claim_id DESC";
$rsQueryTransactions   = mysqli_query($con, $queryTransactions);
$numOfRows = mysqli_num_rows($rsQueryTransactions);
if ($numOfRows > 0) {
    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
            $claimNumberTrans = $dataClaimTrans['claim_id'];
            $claimsarray = array();
            $claimsarray['assigned_to_account_purchase'] = $dataClaimTrans['assigned_to'];

    $queryZone = "SELECT * FROM claims where claim_id=".$claimNumberTrans." and date(created_at)='".$selectedDate."' ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {

        while ($data = @mysqli_fetch_assoc($rsQuery)) {
           // $claimsarray = array();
           // array_push($claimsarray, $data);
            $claimsarray['claimNo'] = $data['claim_id'];
            //$claimsarray['purchasedFromId'] = $data['purchased_from'];

            $claimId = $data['claim_id'];
            $checkPurchaseFrom = $data['purchased_from'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans2 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans2['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['purchasedFromId']  = $dataClaimTransUser['user_id'];
                                $purchaseFromFinal =  $dataClaimTransUser['user_id'];             
                                $claimsarray['purchasedFromStatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                //$claimsarray['purchasedFromId'] = $data['purchased_from'];
                $purchaseFromFinal = $data['purchased_from'];
                $claimsarray['purchasedFromStatus'] = "Accept";
            }

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFromFinal;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);


            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['claimerId'] = $data['user_id'];

            //$claimsarray['accountOfId'] = $data['account_of'];
            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions3 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions12   = mysqli_query($con, $queryTransactions3);
                $numOfRows = mysqli_num_rows($rsQueryTransactions12);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans3 = @mysqli_fetch_assoc($rsQueryTransactions12)) {
                            $claimTransctionsId = $dataClaimTrans3['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId1 = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId1   = mysqli_query($con, $queryTransactionsUserId1);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId1);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser1 = @mysqli_fetch_assoc($rsQueryTransactionsUserId1)) {
                                //$claimsarray['accountOfId']  = $dataClaimTransUser['user_id'];
                                $accountOffFinal = $dataClaimTransUser1['user_id'];
                                $claimsarray['accountofidstatus'] = ucfirst($dataClaimTransUser1['status']);
                        }
                    }
                }
                else{
                    $data['accountOfId'] = 0;
                }
                
            }
            else{
               // $claimsarray['accountOfId'] = $data['account_of'];
                $accountOffFinal = $data['account_of'];
                $claimsarray['accountofidstatus'] = "Accept";
            }

            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$accountOffFinal;
            $rsQueryReg1   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg1);


            $claimsarray['account_of'] = $accountOfInfo;


            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $Date = $data['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            $claimsarray['date'] = $Date;


            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimsarray['claimerId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['claimerName'] = $totalData['full_name'];
            $claimsarray['claimer_category_name'] = $totalData['category_name'];
            $claimsarray['claimer_sub_category_name'] = $totalData['sub_category_name'];

            $queryPurc = "SELECT full_name from users where user_id=". $claimsarray['purchasedFromId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryAcc1 = "SELECT full_name from users where user_id=".$claimsarray['accountOfId'];
            $rsQueryReg   = mysqli_query($con, $queryAcc1);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['accountOfName'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryTotal = "SELECT grand_total from claim_amount where claim_id=".$claimId;
            $rsQueryReg   = mysqli_query($con, $queryTotal);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['grand_total'] = $totalData['grand_total'];


            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_details'][] = $dataReg;   
              }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }
              //print_r_pre($claimsarray);
            array_push($arr,$claimsarray);         
        }
    }
}
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
}
else{
    $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$arr);
    $response = json_encode($temp);
    echo $response;
}


    }
    else{
//and status='pending'
$arr = array();
$queryTransactions = "SELECT * FROM claim_transactions where user_id=".$userId."   and assigned_to='account' ORDER BY claim_id DESC";
$rsQueryTransactions   = mysqli_query($con, $queryTransactions);
$numOfRows = mysqli_num_rows($rsQueryTransactions);
if ($numOfRows > 0) {
    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
            $claimNumberTrans = $dataClaimTrans['claim_id'];
            $claimsarray = array();
            $claimsarray['assigned_to_account_purchase'] = $dataClaimTrans['assigned_to'];

    $queryZone = "SELECT * FROM claims where claim_id=".$claimNumberTrans." ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {

        while ($data = @mysqli_fetch_assoc($rsQuery)) {
           // $claimsarray = array();
           // array_push($claimsarray, $data);
            $claimsarray['claimNo'] = $data['claim_id'];
            //$claimsarray['purchasedFromId'] = $data['purchased_from'];

            $claimId = $data['claim_id'];
            $checkPurchaseFrom = $data['purchased_from'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans2 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans2['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['purchasedFromId']  = $dataClaimTransUser['user_id'];
                                $purchaseFromFinal =  $dataClaimTransUser['user_id'];             
                                $claimsarray['purchasedFromStatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                //$claimsarray['purchasedFromId'] = $data['purchased_from'];
                $purchaseFromFinal = $data['purchased_from'];
                $claimsarray['purchasedFromStatus'] = "Accept";
            }

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFromFinal;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);


            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['claimerId'] = $data['user_id'];

            //$claimsarray['accountOfId'] = $data['account_of'];
            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions3 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions12   = mysqli_query($con, $queryTransactions3);
                $numOfRows = mysqli_num_rows($rsQueryTransactions12);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans3 = @mysqli_fetch_assoc($rsQueryTransactions12)) {
                            $claimTransctionsId = $dataClaimTrans3['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId1 = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId1   = mysqli_query($con, $queryTransactionsUserId1);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId1);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser1 = @mysqli_fetch_assoc($rsQueryTransactionsUserId1)) {
                                //$claimsarray['accountOfId']  = $dataClaimTransUser['user_id'];
                                $accountOffFinal = $dataClaimTransUser1['user_id'];
                                $claimsarray['accountofidstatus'] = ucfirst($dataClaimTransUser1['status']);
                        }
                    }
                }
                else{
                    $data['accountOfId'] = 0;
                }
                
            }
            else{
               // $claimsarray['accountOfId'] = $data['account_of'];
                $accountOffFinal = $data['account_of'];
                $claimsarray['accountofidstatus'] = "Accept";
            }

            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$accountOffFinal;
            $rsQueryReg1   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg1);


            $claimsarray['account_of'] = $accountOfInfo;


            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $Date = $data['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            $claimsarray['date'] = $Date;


            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimsarray['claimerId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['claimerName'] = $totalData['full_name'];
            $claimsarray['claimer_category_name'] = $totalData['category_name'];
            $claimsarray['claimer_sub_category_name'] = $totalData['sub_category_name'];

            $queryPurc = "SELECT full_name from users where user_id=". $claimsarray['purchasedFromId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryAcc1 = "SELECT full_name from users where user_id=".$claimsarray['accountOfId'];
            $rsQueryReg   = mysqli_query($con, $queryAcc1);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['accountOfName'] = $totalData['full_name'];
            //array_push($claimsarray, $data);
            $queryTotal = "SELECT grand_total from claim_amount where claim_id=".$claimId;
            $rsQueryReg   = mysqli_query($con, $queryTotal);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['grand_total'] = $totalData['grand_total'];

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_details'][] = $dataReg;   
              }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }
              //print_r_pre($claimsarray);
            array_push($arr,$claimsarray);         
        }
    }
}
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
}
else{
    $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$arr);
    $response = json_encode($temp);
    echo $response;
}
}
}



////////////////////////////////////////////////////////////



elseif ($action == "view_approve_tagging") {
    $userId = $_REQUEST["user_id"];
    $selectedDate = $_REQUEST["selecteddate"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"User Not Found!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    if ($selectedDate!="") {
        $selectedDate = date('Y-m-d',strtotime($selectedDate));
//and status='pending'
        $arr = array();
$queryTransactions = "SELECT * FROM claim_transactions where user_id=".$userId."   and assigned_to='purchase' ORDER BY claim_id DESC";
$rsQueryTransactions   = mysqli_query($con, $queryTransactions);
$numOfRows = mysqli_num_rows($rsQueryTransactions);
if ($numOfRows > 0) {
    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
            $claimNumberTrans = $dataClaimTrans['claim_id'];
            $claimsarray = array();
            $claimsarray['assigned_to_account_purchase'] = $dataClaimTrans['assigned_to'];


    $queryZone = "SELECT * FROM claims where claim_id=".$claimNumberTrans." and date(created_at)='".$selectedDate."' ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
           // $claimsarray = array();
           // array_push($claimsarray, $data);
            $claimsarray['claimNo'] = $data['claim_id'];
            //$claimsarray['purchasedFromId'] = $data['purchased_from'];
            $claimId = $data['claim_id'];
            $checkPurchaseFrom = $data['purchased_from'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans1 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans1['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['purchasedFromId']  = $dataClaimTransUser['user_id'];
                                $purchaseFromFinal = $dataClaimTransUser['user_id'];
                                $claimsarray['purchasedFromStatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                //$claimsarray['purchasedFromId'] = $data['purchased_from'];
                $purchaseFromFinal = $data['purchased_from'];
                $claimsarray['purchasedFromStatus'] = "Accept";
            }

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFromFinal;
            $rsQueryReg1   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg1);


            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['claimerId'] = $data['user_id'];
            //$claimsarray['accountOfId'] = $data['account_of'];

            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans2 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans2['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['accountOfId']  = $dataClaimTransUser['user_id'];
                                $accountOffFinal = $dataClaimTransUser['user_id'];
                                $claimsarray['accountofidstatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['accountOfId'] = 0;
                }
                
            }
            else{
               // $claimsarray['accountOfId'] = $data['account_of'];
                $accountOffFinal = $data['account_of'];
                $claimsarray['accountofidstatus'] = "Accept";
            }

            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$accountOffFinal;
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg);


            $claimsarray['account_of'] = $accountOfInfo;
            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $Date = $data['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            $claimsarray['date'] = $Date;

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimsarray['claimerId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['claimerName'] = $totalData['full_name'];
            $claimsarray['claimer_category_name'] = $totalData['category_name'];
            $claimsarray['claimer_sub_category_name'] = $totalData['sub_category_name'];

            $queryPurc = "SELECT full_name from users where user_id=". $claimsarray['purchasedFromId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryAcc = "SELECT full_name from users where user_id=".$claimsarray['accountOfId'];
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['accountOfName'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
            
                 $claimsarray['product_details'][] = $dataReg;   
            }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }

              //print_r_pre($claimsarray);

        }
        array_push($arr,$claimsarray);   
        $purchaseFromStatus="";     
        $accountOfStatus="";     

    }
}
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
}
else{
    $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$arr);
    $response = json_encode($temp);
    echo $response;
}


    }
    else{
//and status='pending'
        // and status IN ('pending','accept')
$arr = array();
$queryTransactions = "SELECT * FROM claim_transactions where user_id=".$userId." and assigned_to='purchase' ORDER BY claim_id DESC";
$rsQueryTransactions   = mysqli_query($con, $queryTransactions);
$numOfRows = mysqli_num_rows($rsQueryTransactions);
if ($numOfRows > 0) {
    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
            $claimNumberTrans = $dataClaimTrans['claim_id'];
            $claimsarray = array();
            $claimsarray['assigned_to_account_purchase'] = $dataClaimTrans['assigned_to'];


    $queryZone = "SELECT * FROM claims where claim_id=".$claimNumberTrans." ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
           // $claimsarray = array();
           // array_push($claimsarray, $data);
            $claimsarray['claimNo'] = $data['claim_id'];
            //$claimsarray['purchasedFromId'] = $data['purchased_from'];
            $claimId = $data['claim_id'];
            $checkPurchaseFrom = $data['purchased_from'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans1 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans1['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['purchasedFromId']  = $dataClaimTransUser['user_id'];
                                $purchaseFromFinal = $dataClaimTransUser['user_id'];
                                $claimsarray['purchasedFromStatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                //$claimsarray['purchasedFromId'] = $data['purchased_from'];
                $purchaseFromFinal = $data['purchased_from'];
                $claimsarray['purchasedFromStatus'] = "Accept";
            }

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFromFinal;
            $rsQueryReg1   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg1);


            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['claimerId'] = $data['user_id'];
            //$claimsarray['accountOfId'] = $data['account_of'];

            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions1 = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions1   = mysqli_query($con, $queryTransactions1);
                $numOfRows = mysqli_num_rows($rsQueryTransactions1);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans2 = @mysqli_fetch_assoc($rsQueryTransactions1)) {
                            $claimTransctionsId = $dataClaimTrans2['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                //$claimsarray['accountOfId']  = $dataClaimTransUser['user_id'];
                                $accountOffFinal = $dataClaimTransUser['user_id'];
                                $claimsarray['accountofidstatus'] = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['accountOfId'] = 0;
                }
                
            }
            else{
               // $claimsarray['accountOfId'] = $data['account_of'];
                $accountOffFinal = $data['account_of'];
                $claimsarray['accountofidstatus'] = "Accept";
            }

            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$accountOffFinal;
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg);


            $claimsarray['account_of'] = $accountOfInfo;
            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $Date = $data['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            $claimsarray['date'] = $Date;

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $claimsarray['claimerId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['claimerName'] = $totalData['full_name'];
            $claimsarray['claimer_category_name'] = $totalData['category_name'];
            $claimsarray['claimer_sub_category_name'] = $totalData['sub_category_name'];

            $queryPurc = "SELECT full_name from users where user_id=". $claimsarray['purchasedFromId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryAcc = "SELECT full_name from users where user_id=".$claimsarray['accountOfId'];
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['accountOfName'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
            
                 $claimsarray['product_details'][] = $dataReg;   
            }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }

              //print_r_pre($claimsarray);

        }
        array_push($arr,$claimsarray);   
        $purchaseFromStatus="";     
        $accountOfStatus="";     

    }
}
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
}
else{
    $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$arr);
    $response = json_encode($temp);
    echo $response;
}

}

}

elseif ($action == "view_claim") {
    $userId = $_REQUEST["user_id"];


    $queryZone = "SELECT * FROM claims where user_id=".$userId." ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $arr = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $claimsarray = array();
           // array_push($claimsarray, $data);
            $claimsarray['claimNo'] = $data['claim_id'];

            $claimsarray['purchasedFromId'] = $data['purchased_from'];
            $claimsarray['accountOfId'] = $data['account_of'];
            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $Date = $data['created_at'];
            $Date=substr($Date,0,11);
            $Date=date_create($Date);                     
            $Date = date_format($Date,"d-M-Y");
            $claimsarray['date'] = $Date;

            $queryPurc = "SELECT full_name from users where user_id=". $claimsarray['purchasedFromId'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryAcc = "SELECT full_name from users where user_id=".$claimsarray['accountOfId'];
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $totalData= @mysqli_fetch_assoc($rsQueryReg);
            $claimsarray['accountOfName'] = $totalData['full_name'];
            //array_push($claimsarray, $data);

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
             while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_details'][] = $dataReg;   
              }





              //print_r_pre($claimsarray);
            array_push($arr,$claimsarray);         
        }
    }
    // $countno =  count($zone);
    // if ($countno>0) {
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
}
elseif ($action == "view_notification_detail") {
    
    $claimNo = $_REQUEST["claim_no"];

    if ($claimNo=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Provide Claim No!"),"body"=>$claimNo);
        $response = json_encode($temp);
        echo $response;
        exit();
    }

    
    $queryZone = "SELECT * FROM claims where claim_no='".$claimNo."'";
    $rsQuery   = mysqli_query($con, $queryZone);
    traceMessage(print_r_log($queryZone));
    $numOfRows = mysqli_num_rows($rsQuery);

    if ($numOfRows > 0) {

        
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $claimsarray = array();
           // array_push($claimsarray, $data);
            $checkPurchaseFrom = $data['purchased_from'];
            $claimId = $data['claim_id'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                $numOfRows = mysqli_num_rows($rsQueryTransactions);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                            $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                $data['purchased_from'] = $dataClaimTransUser['user_id'];
                                $purchaseFromStatus = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                $purchaseFromStatus = "Accept";
            }


            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $data['purchased_from'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);
            // $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);


            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                $numOfRows = mysqli_num_rows($rsQueryTransactions);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                            $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                $data['account_of'] = $dataClaimTransUser['user_id'];
                                $accountOfStatus = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['account_of'] = 0;
                }
                
            }
            else{
                $accountOfStatus = "Accept";
            }


            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$data['account_of'];
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg);
            // $claimsarray['accountOfName'] = $totalData['full_name'];
            
            if(empty($accountOfInfo))
                $accountOfInfo = $obj;
            
            if(empty($purchaseFromInfo))
                $purchaseFromInfo = $obj;
           
            $claimsarray['claimNo'] = $data['claim_id'];
            $claimsarray['purchase_from_status'] = $purchaseFromStatus;
            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['account_of_status'] = $accountOfStatus;
            $claimsarray['account_of'] = $accountOfInfo;
            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $claimsarray['date'] = date('d-M-Y', strtotime($data['created_at']));
     
           
            //array_push($claimsarray, $data);

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
            while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_details'][] = $dataReg;   
            }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }

            if ($claimsarray['claim_status']=="closed") {
                $queryReg = "SELECT * FROM claim_finalsettlement WHERE claim_id='".$claimsarray['claimNo']."'";
                $rsQueryReg   = mysqli_query($con, $queryReg);
                while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                     //$claimsarraydetails =array();
                     //array_push($claimsarraydetails, $dataReg);
                     
                     $claimsarray['voucher_details'][] = $dataReg; 
                    }
            }

             
            $purchaseFromStatus="";     
            $accountOfStatus="";     
        }
          $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimsarray);
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>array());
        $response = json_encode($temp);
        echo $response;
    }
    
      
}
elseif ($action == "view_claim_status") {
    $userId = $_REQUEST["user_id"];
    $userStatus = $_REQUEST["status"];
    $userStatusQuery="";

    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Provide User Id!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    if ($userStatus=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"Provide One Status!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    else
    {
        if ($userStatus=="inprocess" || $userStatus=="open" || $userStatus=="approved" || $userStatus=="closed" || $userStatus=="completed") {
            if ($userStatus=="approved") {
                $userStatusQuery = " and claim_status='approved' or claim_status='inprocess' ";
            }
            else{
                $userStatusQuery = " and claim_status='$userStatus'";
            }
        }
        else{
            $temp = array("header"=>array("success"=>"1","message"=>"Wrong Status!"),"body"=>$arr);
            $response = json_encode($temp);
            echo $response;
            exit();
        }
    }
    //
    
    $selectedDate = $_REQUEST["selecteddate"];

    if ($selectedDate!="") {
        $selectedDate = date('Y-m-d',strtotime($selectedDate));

        $queryZone = "SELECT * FROM claims where  user_id=".$userId." and Date(created_at)='".$selectedDate."'  $userStatusQuery ORDER BY claim_id DESC";
        $rsQuery   = mysqli_query($con, $queryZone);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            $arr = array();
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                $claimsarray = array();
               // array_push($claimsarray, $data);
                $checkPurchaseFrom = $data['purchased_from'];
                $claimId = $data['claim_id'];
                if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                    //
                    $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                    $numOfRows = mysqli_num_rows($rsQueryTransactions);
                    if ($numOfRows > 0) {
                        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                                $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                        }
                    }
                    if ($claimTransctionsId!="") {
                        $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                        $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                        $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                        if ($numOfRows > 0) {
                            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                    $data['purchased_from'] = $dataClaimTransUser['user_id'];
                                    $purchaseFromStatus = ucfirst($dataClaimTransUser['status']);
                            }
                        }
                    }
                    else{
                        $data['purchased_from'] = 0;
                    }
                    
                }
                else{
                    $purchaseFromStatus = "Accept";
                }


                $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $data['purchased_from'];
                $rsQueryReg   = mysqli_query($con, $queryPurc);
                $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);
                // $claimsarray['Purchase_form_name'] = $totalData['full_name'];
                //array_push($claimsarray, $data);


                $checkAccountOf = $data['account_of'];
                if ($checkAccountOf=="" || $checkAccountOf=="0") {
                    //
                    $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                    $numOfRows = mysqli_num_rows($rsQueryTransactions);
                    if ($numOfRows > 0) {
                        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                                $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                        }
                    }
                    if ($claimTransctionsId!="") {
                        $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                        $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                        $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                        if ($numOfRows > 0) {
                            while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                    $data['account_of'] = $dataClaimTransUser['user_id'];
                                    $accountOfStatus = ucfirst($dataClaimTransUser['status']);
                            }
                        }
                    }
                    else{
                        $data['account_of'] = 0;
                    }
                    
                }
                else{
                    $accountOfStatus = "Accept";
                }


                $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$data['account_of'];
                $rsQueryReg   = mysqli_query($con, $queryAcc);
                $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg);
                // $claimsarray['accountOfName'] = $totalData['full_name'];
                
                if(empty($accountOfInfo))
                    $accountOfInfo = $obj;
                
                if(empty($purchaseFromInfo))
                    $purchaseFromInfo = $obj;
               
                $claimsarray['claimNo'] = $data['claim_id'];
                $claimsarray['purchase_from_status'] = $purchaseFromStatus;
                $claimsarray['purchase_from'] = $purchaseFromInfo;
                $claimsarray['account_of_status'] = $accountOfStatus;
                $claimsarray['account_of'] = $accountOfInfo;
                $claimsarray['claim_no'] = $data['claim_no'];
                $claimsarray['claim_type'] = $data['claim_type'];
                $claimsarray['carton_weight'] = $data['carton_weight'];
                $claimsarray['receiving_weight'] = $data['receiving_weight'];
                $claimsarray['lcs_no'] = $data['lcs_no'];
                $claimsarray['bilty_no'] = $data['bilty_no'];
                $claimsarray['book_no'] = $data['book_no'];
                $claimsarray['pickup_no'] = $data['pickup_no'];
                $claimsarray['remarks'] = $data['remarks'];
                $claimsarray['claim_status'] = $data['claim_status'];
                $claimsarray['date'] = date('d-M-Y', strtotime($data['created_at']));
         
               
                //array_push($claimsarray, $data);

                $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
                $rsQueryReg   = mysqli_query($con, $queryReg);
                while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                     //$claimsarraydetails =array();
                     //array_push($claimsarraydetails, $dataReg);
                     
                     $claimsarray['product_details'][] = $dataReg;   
                }

                $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
                $rsQueryImage   = mysqli_query($con, $queryImage);
                while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                     //$claimsarraydetails =array();
                     //array_push($claimsarraydetails, $dataReg);
                     
                     $claimsarray['product_images'][] = $dataImage;   
                }

                if ($userStatus=="closed") {
                    $queryReg = "SELECT * FROM claim_finalsettlement WHERE claim_id='".$claimsarray['claimNo']."'";
                    $rsQueryReg   = mysqli_query($con, $queryReg);
                    while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                         //$claimsarraydetails =array();
                         //array_push($claimsarraydetails, $dataReg);
                         
                         $claimsarray['voucher_details'][] = $dataReg;   
                }
                }

                array_push($arr,$claimsarray);    
                $purchaseFromStatus="";     
                $accountOfStatus="";     
            }
              $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
            $response = json_encode($temp);
            echo $response;
        }
        else{
            $temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>array());
            $response = json_encode($temp);
            echo $response;
        }
    }
    else{

    $queryZone = "SELECT * FROM claims where  user_id=".$userId."  $userStatusQuery ORDER BY claim_id DESC";
    $rsQuery   = mysqli_query($con, $queryZone);
    $numOfRows = mysqli_num_rows($rsQuery);
    if ($numOfRows > 0) {
        $arr = array();
        while ($data = @mysqli_fetch_assoc($rsQuery)) {
            $claimsarray = array();
           // array_push($claimsarray, $data);
            $checkPurchaseFrom = $data['purchased_from'];
			$claimId = $data['claim_id'];
            if ($checkPurchaseFrom=="" || $checkPurchaseFrom=="0") {
                //
                $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='purchase' AND claim_id=".$claimId."";
                $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                $numOfRows = mysqli_num_rows($rsQueryTransactions);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                            $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                $data['purchased_from'] = $dataClaimTransUser['user_id'];
                                $purchaseFromStatus = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['purchased_from'] = 0;
                }
                
            }
            else{
                $purchaseFromStatus = "Accept";
            }


		    $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $data['purchased_from'];
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);
            // $claimsarray['Purchase_form_name'] = $totalData['full_name'];
            //array_push($claimsarray, $data);


            $checkAccountOf = $data['account_of'];
            if ($checkAccountOf=="" || $checkAccountOf=="0") {
                //
                $queryTransactions = "SELECT MAX(transaction_id) as claimTransctionsId  FROM claim_transactions WHERE assigned_to='account' AND claim_id=".$claimId."";
                $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
                $numOfRows = mysqli_num_rows($rsQueryTransactions);
                if ($numOfRows > 0) {
                    while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
                            $claimTransctionsId = $dataClaimTrans['claimTransctionsId'];
                    }
                }
                if ($claimTransctionsId!="") {
                    $queryTransactionsUserId = "SELECT user_id,status FROM claim_transactions WHERE  transaction_id=".$claimTransctionsId."";
                    $rsQueryTransactionsUserId   = mysqli_query($con, $queryTransactionsUserId);
                    $numOfRows = mysqli_num_rows($rsQueryTransactionsUserId);
                    if ($numOfRows > 0) {
                        while ($dataClaimTransUser = @mysqli_fetch_assoc($rsQueryTransactionsUserId)) {
                                $data['account_of'] = $dataClaimTransUser['user_id'];
                                $accountOfStatus = ucfirst($dataClaimTransUser['status']);
                        }
                    }
                }
                else{
                    $data['account_of'] = 0;
                }
                
            }
            else{
                $accountOfStatus = "Accept";
            }


            $queryAcc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=".$data['account_of'];
            $rsQueryReg   = mysqli_query($con, $queryAcc);
            $accountOfInfo= @mysqli_fetch_assoc($rsQueryReg);
            // $claimsarray['accountOfName'] = $totalData['full_name'];
			
			if(empty($accountOfInfo))
				$accountOfInfo = $obj;
			
			if(empty($purchaseFromInfo))
				$purchaseFromInfo = $obj;
		   
            $claimsarray['claimNo'] = $data['claim_id'];
            $claimsarray['purchase_from_status'] = $purchaseFromStatus;
            $claimsarray['purchase_from'] = $purchaseFromInfo;
            $claimsarray['account_of_status'] = $accountOfStatus;
            $claimsarray['account_of'] = $accountOfInfo;
            $claimsarray['claim_no'] = $data['claim_no'];
            $claimsarray['claim_type'] = $data['claim_type'];
            $claimsarray['carton_weight'] = $data['carton_weight'];
            $claimsarray['receiving_weight'] = $data['receiving_weight'];
            $claimsarray['lcs_no'] = $data['lcs_no'];
            $claimsarray['bilty_no'] = $data['bilty_no'];
            $claimsarray['book_no'] = $data['book_no'];
            $claimsarray['pickup_no'] = $data['pickup_no'];
            $claimsarray['remarks'] = $data['remarks'];
            $claimsarray['claim_status'] = $data['claim_status'];
            $claimsarray['date'] = date('d-M-Y', strtotime($data['created_at']));
     
           
            //array_push($claimsarray, $data);

            $queryReg = "SELECT * FROM claim_details c,items i WHERE i.item_id=c.item_id and  claim_id='".$claimsarray['claimNo']."'";
            $rsQueryReg   = mysqli_query($con, $queryReg);
            while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_details'][] = $dataReg;   
            }

            $queryImage = "SELECT * FROM claim_images WHERE claim_id='".$claimsarray['claimNo']."'";
            $rsQueryImage   = mysqli_query($con, $queryImage);
            while ($dataImage = @mysqli_fetch_assoc($rsQueryImage)) {
                 //$claimsarraydetails =array();
                 //array_push($claimsarraydetails, $dataReg);
                 
                 $claimsarray['product_images'][] = $dataImage;   
            }

            if ($userStatus=="closed") {
                $queryReg = "SELECT * FROM claim_finalsettlement WHERE claim_id='".$claimsarray['claimNo']."'";
                $rsQueryReg   = mysqli_query($con, $queryReg);
                while ($dataReg = @mysqli_fetch_assoc($rsQueryReg)) {
                     //$claimsarraydetails =array();
                     //array_push($claimsarraydetails, $dataReg);
                     
                     $claimsarray['voucher_details'][] = $dataReg;   
            }
            }

            array_push($arr,$claimsarray);    
            $purchaseFromStatus="";     
            $accountOfStatus="";     
        }
		  $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    }
	else{
		$temp = array("header"=>array("success"=>"1","message"=>"No Data Found!"),"body"=>array());
        $response = json_encode($temp);
        echo $response;
	}
    
    }
    
      
}
elseif ($action == "final_purchase") {

    $purchaseFrom = $_REQUEST["purchased_from"];
    $claimId = $_REQUEST["claim_id"];
    if ($purchaseFrom!='' && $claimId!='') {
        $updateSql = "update claims set purchased_from = '$purchaseFrom' where claim_id =".$claimId;
        mysqli_query($con,$updateSql);
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Null value!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
}
elseif ($action == "update_android_push_id") {

    $userId = $_REQUEST["user_id"];
    if ($userId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No User Id Found!"),"body"=>$userId);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $updateAndroidPushId = $_REQUEST["android_push_id"];
    if ($userId!='' && $updateAndroidPushId!='') {
        $updateSql = "update users set android_push_id = '$updateAndroidPushId' where user_id =".$userId;
        mysqli_query($con,$updateSql);
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Not Enough Value!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
    // echo "asdasdasdsdas";
}
elseif ($action == "final_account") {

    $accountOff = $_REQUEST["account_off"];
    $claimId = $_REQUEST["claim_id"];
    if ($accountOff!='' && $claimId!='') {
        $updateSql = "UPDATE claims set account_of ='$accountOff' , claim_status='pending' where claim_id =".$claimId;
        mysqli_query($con,$updateSql);
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Added!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Null value!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
}
elseif ($action == "purchase_from_hierarchy") {
    $claimId = $_REQUEST["claim_id"];
    if ($claimId=="") {
        $temp = array("header"=>array("success"=>"1","message"=>"No Claim Id Found!"),"body"=>$obj);
        $response = json_encode($temp);
        echo $response;
        exit();
    }
    $arr = array();
    $queryTransactions = "SELECT * from claim_transactions where claim_id='".$claimId."' and assigned_to='purchase'";
    $rsQueryTransactions   = mysqli_query($con, $queryTransactions);
    $numOfRows = mysqli_num_rows($rsQueryTransactions);
    if ($numOfRows > 0) {
        //$claimsarray = array();
        while ($dataClaimTrans = @mysqli_fetch_assoc($rsQueryTransactions)) {
            
            $purchaseFromUserId = $dataClaimTrans['user_id'];
            $purchaseFromUserStatus = $dataClaimTrans['status'];

            $queryPurc = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.user_id=". $purchaseFromUserId;
            $rsQueryReg   = mysqli_query($con, $queryPurc);
            $purchaseFromInfo= @mysqli_fetch_assoc($rsQueryReg);
        
        if(empty($purchaseFromInfo))
                $purchaseFromInfo = $obj;

        // $claimsarray['claimId'] = $claimId;
        // $claimsarray['purchase_from_id'] = $purchaseFromUserId;
        // $claimsarray['purchase_from_status'] = $purchaseFromUserStatus;
        $arr['purchase_from'][] = $purchaseFromInfo;
       //array_push($arr,$claimsarray);    
        //$purchaseFromStatus="";   
    }
    $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$arr);
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $nothing="";
        $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$nothing);
        $response = json_encode($temp);
        echo $response;
    }
        
}
elseif ($action == "loop_purchase_account") {

    $user_id = $_REQUEST["user_idd"];
    $claimId = $_REQUEST["claim_id"];
    $assignedTo = $_REQUEST["type_purchase_account"];
    $status = $_REQUEST["status"];
    $date =  date("Y-m-d H:i:s");

    echo $user_id;
    echo $claimId;
    echo $assignedTo;
    echo $status;
    echo $date;

    if ($user_id!='' && $claimId!='' && $assignedTo!='' && $status!='') {

        $addClaimUserLoopQuery="INSERT INTO `claim_transactions` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$user_id','$assignedTo','$status','$date')";
        $result1=mysqli_query($con,$addClaimUserLoopQuery);

        $addClaimUserLoopLogQuery="INSERT INTO `claim_transactions_log` (`claim_id`,`user_id`,`assigned_to`,`status`,`created_date`) VALUES ('$claimId','$user_id','$assignedTo','$status','$date')";
        $result1=mysqli_query($con,$addClaimUserLoopLogQuery);

        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Added!"),"body"=>"");
        $response = json_encode($temp);
        echo $response;
    }
    else{
        $temp = array("header"=>array("success"=>"1","message"=>"Null value!"),"body"=> "");
        $response = json_encode($temp);
        echo $response;
    }
}
elseif ($action == "search_accountoff_person") {

        $mobileNo = $_REQUEST["mobile_no"];
        if ($mobileNo!="") {
            $querySearchProduct = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.account_of=1  and mobile_no=".$mobileNo;
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            $claimSearchPurchaseFrom = array();
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchPurchaseFrom, $data);
            }
        }
        if ($numOfRows > 0) {

            $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        exit();
    }
    else{

        $zone = $_REQUEST["zone"];
        $region = $_REQUEST["region"];
        $city = $_REQUEST["city"];
        $area = $_REQUEST["area"];
        $categoryId = $_REQUEST["category_id"];
        $subCategoryId = $_REQUEST["sub_category_id"];
        $queryZone = "";
        $queryRegion = "";
        $queryCity = "";
        $queryArea = "";
        $querycategoryId = "";
        $querySubCategoryId = "";


        if ($zone=="") {
            $queryZone.="u.zone!=''";
        }
        else
        {
            $queryZone.="u.zone_id='$zone'";
        }
        //
        if ($region=="") {
            $queryRegion.="u.region!=''";
        }
        else
        {
            $queryRegion.="u.region_id='$region'";
        }
        //
        if ($city=="") {
            $queryCity.="u.city!=''";
        }
        else
        {
            $queryCity.="u.city_id='$city'";
        }
        //
        if ($area=="") {
            $queryArea.="u.area!=''";
        }
        else
        {
            $queryArea.="u.area_id='$area'";
        }
        //
        if ($categoryId=="") {
            $querycategoryId.="u.dealer_category_id!=''";
        }
        else
        {
            $querycategoryId.="u.dealer_category_id='$categoryId'";
        }
        //
        if ($subCategoryId=="") {
            $querySubCategoryId.="u.sub_category_id!=''";
        }
        else
        {
            $querySubCategoryId.="u.sub_category_id='$subCategoryId'";
        }

        $querySearchProduct = "SELECT u.* , d.* , d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and $queryZone and $queryRegion and $queryCity and $queryArea and $querycategoryId and $querySubCategoryId and u.account_of=1 ";
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            $claimSearchPurchaseFrom = array();
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchPurchaseFrom, $data);
            }
        }
        if ($numOfRows > 0) {

            $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
    }
}
elseif ($action == "search_purchasefrom_person") {


        $mobileNo = $_REQUEST["mobile_no"];
        if ($mobileNo!="") {
            $querySearchProduct = "SELECT u.*,d.*,d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and u.dealer_category_id IN (SELECT dealer_category_id FROM dealer_category WHERE dealer_category_name IN ('MECHANICS','SUBDEALERS','DEALERS')) and mobile_no=".$mobileNo;
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            $claimSearchPurchaseFrom = array();
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchPurchaseFrom, $data);
            }
        }
        if ($numOfRows > 0) {

            $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        exit();
    }
    else{

        $zone = $_REQUEST["zone"];
        $region = $_REQUEST["region"];
        $city = $_REQUEST["city"];
        $area = $_REQUEST["area"];
        $categoryId = $_REQUEST["category_id"];
        $subCategoryId = $_REQUEST["sub_category_id"];
        $queryZone = "";
        $queryRegion = "";
        $queryCity = "";
        $queryArea = "";
        $querycategoryId = "";
        $querySubCategoryId = "";


        if ($zone=="") {
            $queryZone.="u.zone!=''";
        }
        else
        {
            $queryZone.="u.zone_id='$zone'";
        }
        //
        if ($region=="") {
            $queryRegion.="u.region!=''";
        }
        else
        {
            $queryRegion.="u.region_id='$region'";
        }
        //
        if ($city=="") {
            $queryCity.="u.city!=''";
        }
        else
        {
            $queryCity.="u.city_id='$city'";
        }
        //
        if ($area=="") {
            $queryArea.="u.area!=''";
        }
        else
        {
            $queryArea.="u.area_id='$area'";
        }
        //
        if ($categoryId=="") {
            $querycategoryId.="u.dealer_category_id!=''";
        }
        else
        {
            $querycategoryId.="u.dealer_category_id='$categoryId'";
        }
        //
        if ($subCategoryId=="") {
            $querySubCategoryId.="u.sub_category_id!=''";
        }
        else
        {
            $querySubCategoryId.="u.sub_category_id='$subCategoryId'";
        }

        $querySearchProduct = "SELECT u.* , d.* , d.dealer_category_name AS category_name,s.sub_category_name AS sub_category_name FROM users u, dealer_category d,dealer_sub_category s  WHERE d.dealer_category_id=u.dealer_category_id AND s.sub_category_id=u.sub_category_id and $queryZone and $queryRegion and $queryCity and $queryArea and $querycategoryId and $querySubCategoryId and u.dealer_category_id IN (SELECT dealer_category_id FROM dealer_category WHERE dealer_category_name IN ('MECHANICS','SUBDEALERS','DEALERS'))";
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            $claimSearchPurchaseFrom = array();
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchPurchaseFrom, $data);
            }
        }
        if ($numOfRows > 0) {

            $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
        else
        {
            $temp = array("header"=>array("success"=>"1","message"=>"No Record Found!"),"body"=>$claimSearchPurchaseFrom);
            $response = json_encode($temp);
            echo $response;
        }
    }
}
elseif ($action == "search_parts") {

    $itemModel = $_REQUEST["item_model"];
    $itemType = $_REQUEST["item_type"];
    $itemGroup = $_REQUEST["item_group"];
    $queryItemModel = "";
    $queryItemType = "";
    $queryItemGroup = "";
    $nameCode = $_REQUEST["name_code"];

    if ($nameCode!="") {
        $claimSearchProductDetail = array();
        $querySearchProduct = "select * from items where item_code='".$nameCode."' or item_name='".$nameCode."'";
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchProductDetail, $data);
            }
        }
    }
    else
    {
        if ($itemModel=="" || $itemType=="" || $itemGroup=="") {
            $temp = array("header"=>array("success"=>"1","message"=>"Select all 3 options!"),"body"=>$itemModel);
            $response = json_encode($temp);
            echo $response;
            exit();
        }

        if ($itemModel!="") {
            $queryItemModel.="item_model_id='$itemModel'";
        }
        //
        if ($itemType!="") {
            $queryItemType.="and item_type_id='$itemType'";
        }
        //
        if ($itemGroup!="") {
            $queryItemGroup.="and item_group_id='$itemGroup'";
        }

        $claimSearchProductDetail = array();
        $querySearchProduct = "select * from items where $queryItemModel $queryItemType $queryItemGroup";
        $rsQuery   = mysqli_query($con, $querySearchProduct);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchProductDetail, $data);
            }
        }
    }
        $temp = array("header"=>array("success"=>"1","message"=>"Successfully Showed!"),"body"=>$claimSearchProductDetail);
        $response = json_encode($temp);
        echo $response;

} 
elseif ($action == "force_logout") {
    if (is_numeric($_REQUEST['user_id'])) {
        $userId     = $_REQUEST['user_id'];
    } else {
        $temp = array(
            "header" => array(
                "success" => "0",
                "message" => "data not provided"
            ),
            "body" => $obj
        );
        echo json_encode($temp);
        exit;
    }

    if ($userId == "") {
        $status = array(
            "header" => array(
                "success" => "0",
                "message" => "user_id is Empty"
            ),
            "body" => $obj
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
        exit;
    }
    $check_status = mysqli_query($con, "SELECT status FROM users WHERE user_id='$userId'");
    if (@mysqli_num_rows($check_status) == 1) {
        $row    = @mysqli_fetch_assoc($check_status);
        $status = array(
            "header" => array(
                "success" => "1",
                "message" => "user status"
            ),
            "body" => array(
                "status" => $row['status']
            )
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
    }elseif ($action == 'get_availaible_agent') {
      $query = "select * from agent where status = active";
        $rsQuery   = mysqli_query($con, $query);
        $numOfRows = mysqli_num_rows($rsQuery);
        if ($numOfRows > 0) {
            while ($data = @mysqli_fetch_assoc($rsQuery)) {
                array_push($claimSearchProductDetail, $data);
    }
}
}


    else {
        $status = array(
            "header" => array(
                "success" => "0",
                "message" => "user not found"
            ),
            "body" => array(
                "status" => 0
            )
        );
        traceMessage(print_r_log($status));
        echo json_encode($status);
    }
}


function notifications($userId,$userEmail,$androidPushId,$applePushId,$message,$claimID,$page)
{
    traceMessage("FCM NOTIFICATION FIRST STEP!!!");

    $inserttime = date('Y-m-d H:i:s');
    $notificationtype = NOTIFICATION_GENERAL;
    $data_notify = array();
    $db = array('user_id' => $userId,
              'reference_id' => $claimID,
              'title' => 'Claim Alert!',
              'type' => $notificationtype,
              'message' => $message,
              'creation_time' => $inserttime,
              'email_address' => $userEmail,
              'android_push_id' => $androidPushId,
              'apple_push_id' => $applePushId,
              'page' => $page,
              'status' => 1, 

    );
    $msg = array (
          'body'    => $message,
          'title'   => 'Claim Alert!',
          'reference_id'    => $claimID,
          'type'    => $notificationtype,
          'icon'    => 'myicon',
          'sound' => 'mySound'
    );
    $data_notify['id'] = $androidPushId;
    $data_notify['msg'] = $msg;
    $data_notify['db'] = $db;
    traceMessage("FCM NOTIFICATION".print_r_log($data_notify));
    $result = fcm_send_notification($data_notify);
    traceMessage('FCM RETURN;'.print_r_log($result));
    /*
    $temp     = array(
          "header" => array(
              "success" => "0",
              "message" => "Notification Sent!",
              "token" => $token
          ),
          "body" => $obj
      );*/
      // $response = json_encode($data_notify);
      // echo $response;
    
}