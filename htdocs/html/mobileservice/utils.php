<?php 
	function cleanLog()
	{
		$fileName = "tracelogs.txt";
		$handle = @fopen($fileName, "r+"); 
		if ($handle !== false)
		{
			ftruncate($handle, 0);
			fclose($handle);
		}
		else
		{
			return true;
		}
	}
	function print_r_pre($var,$heading="")
	{
		if ($heading!="")
			echo "<br><b>$heading:</b>";
			
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
	
	function print_r_log($var)
	{
		ob_start();
		print_r($var);
		$ret_str = ob_get_contents();
		ob_end_clean();

		return $ret_str;
	}
	function traceMessage($str="",$logFilePath="")
	{
		// if(ENABLE_TRACE!="1")
		// return; 
		if($logFilePath=="")
		{
			$fileName = "/var/wwwlog/meezan.uhfsolutions.com/logs.txt";
		}
		else
		{
			$fileName = $logFilePath;
		}
		$today = date("D, M j/y G:i:s") . " " . microtime(); 	
		error_log("\n".$today .":".$str."\n",3,$fileName);
		//_rotate($fileName);	
	}
	function _rotate($filename)
	{
		$length = filesize ( $filename );
		//$maxSize = MAX_TRACE_FILE_SIZE*1024*1024;//2mb 
		if ( file_exists ( $filename ))
		{	
			unlink($filename);
		}
		
		if ( !touch ( $filename ) )
		{
			die("Unable to create file: ".$filename);
		}	
		
	}
	function distance($lat1, $lon1, $lat2, $lon2, $unit) 
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		
		if ($unit == "K") 
		{
			return round(($miles * 1.609344),4);
		}
		else if ($unit == "N") 
		{
			return round(($miles * 0.8684),4);
		}
		else if ($unit == "M") 
		{
			$distanceMeter=$miles * 1.609344;
			return round(($distanceMeter * 1000),4);
		}
		else 
		{
			return $miles;
		}
	}
?>