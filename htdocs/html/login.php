<?php

//echo phpinfo();
    ini_set('session.cookie_httponly', 1);
    header( "Set-Cookie: name=value; httpOnly" );
    header('X-Frame-Options: SAMEORIGIN');


    session_start();
    //This should contain something like www.zend.com
    error_reporting(E_ERROR |  E_PARSE );
    require_once("globalpath.php");
    //This gets the path part of the URI.  It should get something
    //like /~leon/fe/htdocs/, but it could be just /
    //define("EXTERNAL_PATH", eregi_replace(SCRIPT_NAME, "", $SCRIPT_NAME));
    /*
    ** set constants
    */
    
    require_once(MODULE . "/utility/configuration.php");
    require_once(MODULE . "/utility/standard_library.php");
    require_once(MODULE . "/utility/initialization.php");
    require_once(MODULE . "/utility/ScreenInfo.php");   // need to enter every sceen in it
    require_once(MODULE . "/class/datalayer/dataaccess.php");
    require_once(MODULE . "/class/common/session.php");
    global $mysession;
    $mysession = new Session();
    require_once(MODULE."/class/ajax/pserver.php");
    
    require_once(MODULE . "/class/ajax/user.php");
    
    
    require_once(MODULE . "/class/bllayer/user.php");
    

    $verified_captcha = "yes";
    if(isset($hloginnow) && $hloginnow=="yes" && ($captcha != $_SESSION['vercode'])){
        $verified_captcha = "no";
        $msg = "Please verify you are human";
        echo "<script>alert('".$msg."')</script>";
    }
    if(isset($hloginnow) && $hloginnow=="yes" && $verified_captcha == "yes")
    {

        traceMessage("in auth");
        $password = base64_decode($password);
        $ret = Authenticate($emailaddress,$password);
        $_SESSION['sid'] = $ret['sid'];
        $loginSid = $ret['sid'];
       
        if($loginSid!="-1")
        {
            echo "<script>location.href='/index.php?SCREEN=maincontroller';</script>";
            exit();
        }
        else
        {
            $msg = "Invalid credential";
            echo "<script>alert('".$msg."')</script>";
        }
    }

    if($_SERVER['HTTP_HOST'] == "wapsicar.local.pk")
    {
        $logo = "";
        $bgimg = "";
    }
    else{
        $logo = "images/ats_logo.png";
        $bgimg = "hero-bg-1";
    }
   // print_r_pre($_SERVER);
    $randVal = rand();
    $_SESSION['ramdomval'] = $randVal;
    //echo $_SESSION['ramdomval'];
    //die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/x-icon" href="images/ats_logo.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title -->
    <title><?=TITLE?></title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/themes/login/assets/css/bootstrap.min.css">
    <!-- Font awesome CSS -->
    <link rel="stylesheet" href="/themes/login/assets/css/font-awesome.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="/themes/login/assets/css/style.css">
    <!-- jQuery -->
    <script src="/themes/login/assets/js/jquery-1.11.3.min.js"></script>
</head>
<body>

    <!-- Preloader starts -->
    <div class="mega-site-preloader-wrap">
        <div class="cont">
            <div class="line square"></div>
            <div class="line square2"></div>
            <div class="line square3"></div>
            <div class="line square4"></div>
        </div>
    </div>
    <!-- Preloader ends -->

    <div class="fullwidth-cotnent-block <?=$bgimg?>">
         <div><img src="<?=$logo?>"></div>
        <div class="fullwidth-content-tablecell">
            <div class="container" style="margin-top:50px">
                <div class="row">
                    
                    <div class="col-md-8  col-sm-8 col-sm-offset-2 text-center" style="margin-left: 600px" style="background: red;
                          width: 300px;
                          height: 300px;
                          border-radius: 150px;
                          outline: 5px solid green;">
                          <div class="info-card" style="background: #FF7300; width: 40%;  margin-left: 125px;margin-top: 90px;border-radius: 25px;">
                            <h4 style='color:white;padding-top: 10px;'><b>Login to your Account</b></h4>
                            
                           <form id="loginForm" class="smart-form client-form" name='loginForm' method='post'>
                            <input type='hidden' id='hloginnow' name='hloginnow' value="">
                                    <div class="form-group">
                                        <input type="text" style='width:250px;color:black;border-radius: 25px;text-align: center' placeholder="Username" name="emailaddress" id="emailaddress" maxlength="10">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" style='width:250px;color:black ;border-radius: 25px;text-align: center' name="password" id="password" placeholder="Password" maxlength="">
                                    </div>
                                    <div class="form-group">
                                                
                                    <p class="img-container">
                                    <img src="captcha.php?r_id=<?=$randVal?>" style='width:250px;color:black;border-radius: 25px;text-align: center;height: 45px;'/>
                                    </p>
                                    <p>
                                    <input type="text" style='width:250px;color:black;border-radius: 25px;text-align: center; margin-left: 25px;height: 45px;' class="form-control form-control-lg" placeholder="Verify Robot" name="captcha" id="captcha" onkeyup="CheckLoginEnter(event)" required>
                                    </p>

                                </div>
                                    <div class="form-group" style="padding-bottom: 10px;">
                                        <button style='border-color: #000000;width:150px;border-radius: 25px;text-align: center;background-color: white; color:black' type="button" onclick="SubmitLoginForm()">Login 
                                        </button>
                                    </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>









    <!-- Active JS -->
    <script src="/themes/login/assets/js/active.js"></script>
</body>
</html>

<script>
        function SubmitLoginForm()
        {
            var email = document.getElementById('emailaddress').value;
            var pass = document.getElementById('password').value;
            if(email=="")
            {
                alert("Please provide your username or emailaddress");
                return false;
            }

            if(pass=="")
            {
                alert("Please provide your password");
                return false;
            }

            
            var encodedData = window.btoa(pass); // encode a string
            document.getElementById('password').value=encodedData;
            document.getElementById('hloginnow').value='yes';
            document.getElementById('loginForm').submit();
        }
        function CheckLoginEnter(evt)
        {
            if(evt.keyCode==13)
            SubmitLoginForm();
        }
    </script>
