function CreateTabPanel(id,display,backcolor)
{
	if (!display)
		display = "none";
	else if (display == 1)
		display = "block";
	else if (display == 0)
		display = "none";
		
	if (!backcolor)
		backcolor = 'white';
		
	this.panelId = id;
	
	var str = '';
	str += '<table style="display:'+display+';height:30px;" id="'+id+'" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">';
		str += '<tr id="trTabs" width="100%">';
			str += '<td class="sidebar" id="'+id+'_emptyTr" >&nbsp;</td>';
		str += '</tr>';
		str += '<tr id="trRow">';
			str += '<td colspan="99" align="center" class="sidebar" height="1"><img src="/images/blank.gif" height="2"></td>';
		str += '</tr>';
	str += '</table>';

	//if(_browsername_!='Mozilla')
	//		doc = parent.document.getElementById('IFControl').contentWindow.document;
	//else
	//		doc = parent.document.getElementById('IFControl').contentDocument;
	
	//doc.write(str);
	// Fix fox ajax driven screens, parent TD id should be <id+'Container'>
	if (!$(id))
	{
		//alert($(id) +'|'+ parent.frames['IFControl'].document.getElementById(id));
		$(id+'Container').innerHTML += str;
	}	

	this.SelectTab = SelectTab;
	this.UpdateTab = UpdateTab;	
	this.AddTab = AddTab;
	this.ClearTabs = ClearTabs;
	this.HighlightTab = HighlightTab;
	this.BlinkTab = BlinkTab;
	this.DeleteTab = DeleteTab
}

function SelectTab(o,action)
{
	if (typeof o == "string")
	{
		this.HighlightTab(o);
	}
	else
	{
		//alert(this.panelId);
		var table = document.getElementById(this.panelId);
		var gid = o.getAttribute("gid");
	
		var tds = table.getElementsByTagName("td");
		for (var i=0;i<tds.length;i++)
		{
			if (tds[i].getAttribute("gid") == gid)
				tds[i].className = 'tabSelected';
			else if (tds[i].getAttribute("gid") && tds[i].className != 'tabBlink')
				tds[i].className = 'tabNormal';
		}
	}
	action();
}

function UpdateTab(tid,action,title)
{
	var table = document.getElementById(this.panelId);
	var o = this;	
	
	var tds = table.getElementsByTagName("td");
	for (var i=0;i<tds.length;i++)
	{
		if (tds[i].getAttribute("gid") == tid)
		{
			if (title && tds[i].id == "td_"+tid+"_text")
				tds[i].innerHTML = title;
				
			tds[i].onclick = function(){o.SelectTab(this,action)};
		}
	}
}


function AddTab(tid,title,action,tooltip,del,delaction)
{
	var o = this;
	tooltip = (typeof(tooltip) == 'undefined') ? title : tooltip;
	
	var mainTable = document.getElementById(this.panelId);
	var emptyTab = document.getElementById(this.panelId+"_emptyTr");
	var trTabs = mainTable.rows[0];
	var availWidth;
	if(window.screenLeft <50)
		availWidth = '13%';
	else
		availWidth = '25%';

	var mtd = document.createElement('td');
	mtd.id = tid;
	mtd.align='left';
	mtd.style.width = availWidth;
	
	
	var ctable = document.createElement('table');
	ctable.border=0;
	ctable.cellSpacing=0;
	ctable.cellPadding=0;
	

	var ctbody = document.createElement('tbody');	
	var ctr = document.createElement('tr');		
	ctr.setAttribute('gid',tid);

	var ctd1 = document.createElement('td');
	ctd1.className = "tabNormal";
	ctd1.setAttribute('gid',tid);
	ctd1.width="1%";
	ctd1.align="left";
	
	var i1 = document.createElement('img');
	i1.src="/images/misc/tab_leftcorner.gif";
	i1.height="30";
	i1width="1";
	i1.border ="0";
	
	ctd1.onclick = function(){o.SelectTab(ctd1,action);};
	ctd1.style.cursor = 'hand';
	ctd1.appendChild(i1);
		
	var ctd2 = document.createElement('td');
	//ctd2.nowrap="true";
	ctd2.noWrap=true;
	ctd2.id= 'td1_'+tid+'_text';
	ctd2.className = "tabNormal";
	ctd2.setAttribute('gid',tid);
	ctd2.width="80%";
	ctd2.align="center";
	ctd2.title = tooltip;
	ctd2.tooltip = tooltip;
	ctd2.onclick = function(){o.SelectTab(ctd1,action);};
	
	//ctd2.innerHTML = trimString(title,15);

	if(del=='yes')
	{
		ctd2.innerHTML = trimString(title,10);
	}
	else
		ctd2.innerHTML = "<font size=3 face='comicsan'><b>"+trimString(title,15)+"</b></font>";

	ctd2.style.cursor = 'hand';
	
	var ctd3 = document.createElement('td');
	ctd3.className="tabNormal";
	ctd3.setAttribute('gid',tid);
	ctd3.width="1%";
	ctd3.align ="right";
	
	var i3 = document.createElement('img');
	i3.src="/images/dialog/button-close-focus.gif";
	i3.border ="0";
	i3.onclick = function(){o.DeleteTab(tid,delaction);};

	ctd3.style.cursor = 'hand';
	ctd3.appendChild(i3);
	
	var ctd4 = document.createElement('td');
	ctd4.className="tabNormal";
	ctd4.setAttribute('gid',tid);
	ctd4.width="1%";
	ctd4.align ="left";
	
	var i4 = document.createElement('img');
	i4.src="/images/misc/tab_rightcorner.gif";
	i4.border ="0";
	i4.width = "10";
	i4.height="30";
	
	ctd4.appendChild(i4);
	ctd4.onclick = function(){o.SelectTab(ctd1,action);};
	ctd4.style.cursor = 'hand';

	var ctd5 = document.createElement('td');
	ctd5.className="sidebar";
	ctd5.width="1%";
	ctd5.align ="left";
	
	var i5 = document.createElement('img');
	i5.src="images/blank.gif";
	i5.border ="0";
	i5.width = "2";
	i5.height="19";
	ctd5.appendChild(i5);
	//	var ctdemtpty = document.createElement('<td width="99%">');;
	
	ctr.appendChild(ctd1);
	ctr.appendChild(ctd2);
	if(del=='yes')
	{
		ctr.appendChild(ctd3);
	}
	ctr.appendChild(ctd4);
	ctr.appendChild(ctd5);
	//ctr.appendChild(ctdemtpty);
	//ctbody.appendChild(ctr);
	ctbody.appendChild(ctr);
	ctable.appendChild(ctbody);	
	mtd.appendChild(ctable);
	trTabs.insertBefore(mtd,emptyTab);
//	trTabs.appendChild(mtd);
}

function ClearTabs()
{
	var mainTable = document.getElementById(this.panelId);
	var emptyTab = document.getElementById(this.panelId+"_emptyTr");
	var str = emptyTab.outerHTML;
	var trTabs = mainTable.rows[0];
	var c = trTabs.cells.length;
	for (var x=0;x<c-1;x++)
	{
		trTabs.deleteCell(0);
	}
}

function HighlightTab(tid)
{
	var table = document.getElementById(this.panelId);
	if(table==null)
		return;
	var o = this;	
	var gid;
	var tds = table.getElementsByTagName("td");
	for (var i=0;i<tds.length;i++)
	{
		if (gid = tds[i].getAttribute("gid"))
		{
			if (gid == tid)
				tds[i].className = 'tabSelected';
			else if(tds[i].className != 'tabBlink')
			{	
				tds[i].className = 'tabNormal';
			}	
		}
		
	}
}

function BlinkTab(tid)
{
	var table = document.getElementById(this.panelId);
	var o = this;	
	var gid;
	var tds = table.getElementsByTagName("td");
	for (var i=0;i<tds.length;i++)
	{
		if (gid = tds[i].getAttribute("gid"))
		{
			if (gid == tid)
				if(tds[i].className != 'tabSelected')
				tds[i].className = 'tabBlink';
		}
		
	}
}
function DeleteTab(tid,action)
{
	var mainTable = document.getElementById(this.panelId);
	var trTabs = mainTable.rows[0];
	trTabs.removeChild(document.getElementById(tid)); 
	action();
}