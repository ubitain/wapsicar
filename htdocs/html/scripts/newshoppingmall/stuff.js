
var to = null;

function fadeOutMenu(m) {
	$("#"+m).fadeOut('100');
	$("#"+m).prev("a.act").removeClass('act');
	clearTimeout(to);
}

$(document).ready(function(){

	/*  Change Your Location  */
	$('#yLocChange').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('act')) {
			$(this).removeClass('act');
			$("#yLocFrm").fadeOut('100');
			clearTimeout(to);
		} else {
			$(this).addClass('act');
			$("#yLocFrm").fadeIn('150');
		}
		
	});
	
		$("#yLocFrm a").click(function(e) {
			e.preventDefault();
			$('#yLocChange').removeClass('act');
			$("#yLocFrm").fadeOut('100');
			clearTimeout(to);
		});
		
		$("#yLocFrm, #yLocChange.act").hover(
			function() { clearTimeout(to); },
			function() { to = setTimeout(function() { fadeOutMenu('yLocFrm') }, 1000); }
		);
		
		$("#yLocChange").hover(
			function() {},
			function() { if ($(this).hasClass('act')) to = setTimeout(function() { fadeOutMenu('yLocFrm') }, 1000); }
		);

	/*  The Best Places tabs  */
	$('#bpTabs a').click(function(e) {
		e.preventDefault();
		$('#bpTabs a').removeClass('act');
		$("div[id^='bp_tab']").hide();
		
		$(this).addClass('act');

		tmp = $(this).attr('id')+"_frm";
		$("#"+tmp).fadeIn('200');
	});
	
	/*  Select Your Region  */
	$('#yRegSelect').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('act')) {
			$(this).removeClass('act');
			$("#yRegSelect_dd").fadeOut('100');
			clearTimeout(to);
		} else {
			$(this).addClass('act');
			$("#yRegSelect_dd").fadeIn('150');
		}
	});
	
		$("#yRegSelect_dd a").click(function(e) {
			e.preventDefault();
			$('#yRegSelect').removeClass('act');
			$("#yRegSelect_dd").fadeOut('100');
			clearTimeout(to);
		});
		
		$("#yRegSelect_dd, #yRegSelect.act").hover(
			function() { clearTimeout(to); },
			function() { to = setTimeout(function() { fadeOutMenu('yRegSelect_dd') }, 1000); }
		);
		
		$("#yRegSelect").hover(
			function() {},
			function() { if ($(this).hasClass('act')) to = setTimeout(function() { fadeOutMenu('yRegSelect_dd') }, 1000); }
		);
	
	
	/*  Facebook box  */
	$("#fbBox").hover(
		function() { $(this).addClass('hover'); },	
		function() { $(this).removeClass('hover'); }	
	);

	
	/*  Yadig On Your Mobile  */
	$("#mobTabs li a").click(function(e) {
		e.preventDefault();		
		$('#mobTabs li a').removeClass('act');
		$("div[id^='mob_tab']").hide();
		
		$(this).addClass('act');

		tmp = $(this).attr('id')+"_txt";
		$("#"+tmp).fadeIn('200');
	});
	
	
	/*  List with thumbnails  */
	$(".thmbList li").hover(
		function() { $(this).addClass('hover'); },	
		function() { $(this).removeClass('hover'); }	
	);
	
});