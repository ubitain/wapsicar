/*
List of functions in this file

function isDomainName(value)
function isAlphabet(value)
function isUsername(value)
function isRequired(value)
function isWavFile(value)
function isPassword(value)
function isAlphaSpace(value)
function isAlphaNumericSpace(value)
function isAlphaNumeric(value)
function isDigit(value)
function isAmount(value)
function isSpecialPhone(value)
function isEmail(value)
function isValidEmail (value) /// allows . and - in the email address too
function isMultiEmail(value)
function isPhone(value)
function isAscii(value)
*/

function isDomainName(value)
{
//var x = new RegExp("^(http://)?(www\.)?([A-Za-z0-9_]{2,})(\.[A-Za-z]{2,5})+$");
//var x = new RegExp("^[A-Za-z0-9_\-]{3,}([\.]{1,1}[A-Za-z]{2,5})+$");
var x = new RegExp("([A-Za-z0-9_\-]{2,}[\.]{1,1})+([A-Za-z]{2,5})$");
return x.test(value);
}

function isAlphabet(value)
{
var x = new RegExp("^[A-Za-z]+$");
return x.test(value);
}

function isQuickName(value)
{
var x = new RegExp("^[_A-Za-z0-9-]+$");
return x.test(value);
}

function isUsername(value)
{
var x = new RegExp("^[A-Za-z][A-Za-z0-9_]{0,}$");
return x.test(value);
}

function isRequired(value)
{
if (value!="")
	return true;
else
	return false;
/*
var x = new RegExp("^.{1,}$");
return x.test(value);
*/
}

function isWavFile(value)
{
value = value.toLowerCase();
var x = new RegExp("^.{1,}[\.]wav$");
return x.test(value);
}

function isPassword(value)
{
var x = new RegExp("^.{2,}$");
return x.test(value);
}

function isAccountName(value)
{
	var x = new RegExp("^.{2,}@.{2,}$");
	return x.test(value);
}

function isAlphaSpace(value)
{
	if (typeof(value)!="object")
	{	
		var x = new RegExp("^[A-Za-z][A-Za-z ]*$");
		return x.test(value);
	}
	else 
	{
		
	}	
}

function isAlphaNumericSpace(value)
{
	if (typeof(value)!="object")
	{
		var x = new RegExp("^[A-Za-z0-9@][@A-Za-z0-9 \.]*$");
		return x.test(value);
	}
	else
	{
		var x = new RegExp("^[A-Za-z0-9@][@A-Za-z0-9 \.]*$");
		return x.test(value);
	}
}

function isUserName(value)
{
	if (typeof(value)!="object")
	{
		var x = new RegExp("^[A-Za-z0-9][A-Za-z0-9_]*$");
		return x.test(value);
	}
	else
	{
		var x = new RegExp("^[A-Za-z0-9@][@A-Za-z0-9]*$");
		return x.test(value);
	}
}

function isAlphaNumeric(value)
{
var x = new RegExp("^[A-Za-z0-9]{2,}$");
return x.test(value);
}

function isDigit(value)
{
var x = new RegExp("^[0-9]+$");
return x.test(value);
}

function isDecimal(value)
{
var x = new RegExp("^[0-9]+(\.[0-9]+)?$");
return x.test(value);
}

function isAmount(value)
{
var x = new RegExp("^[0-9]+(\.[0-9]{1,5})?$");
return x.test(value);
}

function isSpecialPhone(value)
{
var x = new RegExp("^[0-9]{7,20}$");
return x.test(value);
}

function isEmail(value)
{
//var x = new RegExp("^[A-Za-z][A-Za-z0-9_\.]{1,}@[A-Za-z0-9_\-]{2,5}(\.[A-Za-z]{2,5})+$");
var x = new RegExp("^[A-Za-z][A-Za-z0-9_.\-]{0,}@([A-Za-z0-9_\-]{1,}[\.]{1,1})+([A-Za-z]{2,5})$");
return x.test(value);
}

function isValidEmail ( value )
{
var x = new RegExp("^[A-Za-z0-9][A-Za-z0-9_\.\-]{0,}@([A-Za-z0-9_\-]{1,}[\.]{1,1})+([A-Za-z]{2,5})$");
return x.test(value); // allows . and - in the email address
}

function isValidServer ( value )
{
var x = new RegExp("^[A-Za-z0-9.]{1,}$");
return x.test(value); // allows . and - in the email address
}

function isMultiEmail(value)
{
var x = new RegExp("^([A-Za-z][A-Za-z0-9_\.\-]{0,}@([A-Za-z0-9_\-]{1,}[\.]{1,1})+([A-Za-z]{2,5})([\,]{0,}))+$"); // Multiple Emails
return x.test(value);
}

function isPhone(value)
{
var x = new RegExp("^[+][0-9]{1,3}[(][0-9]{1,3}[)][0-9]{3}-[0-9]{4}$");
return x.test(value);
}

function isAscii(value)
{
var x = new RegExp("^[\x20-\x7E\x0A\x0D]*$");
return x.test(value);
}

// ======================== //
// VAILIDATION FUNCTIONS v2 //
// ======================== //

function isValidNonEmpty(o)
{
	if (!isRequired(o.value))
	{
		//alert("Field '"+o.name+"' must have numbers");
		alert("Value must not be empty");
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}

function isValidNumber(o)
{
	if (!isDigit(o.value))
	{
		//alert("Field '"+o.name+"' must have numbers");
//		alert("Value must have numbers");
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}

function isValidAmount(o)
{
	if (!isAmount(o.value))
	{
		//alert("Field '"+o.name+"' must have numbers");
		//alert("Value must be decimal");
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}

function isValidNonZeroNumber(o)
{
	if (!(o.value>0))
	{
		//alert("Field '"+o.name+"' must be greater than zero");
//		alert("Value must be greater than zero");
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}

function isValidAlphaNumericSpace(o)
{
	if (!isAlphaNumericSpace(o.value))
	{
		alert("Value must contain Alphabets, Numeric or Spaces");
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}

function isValidDate(o)
{
	if (!isDate(o.value))
	{
		o.focus();
		o.className='errorHighlight';
		return false;
	}
	else
	{
		o.className='';
		return true;	
	}
}
function isWavFile(value)
{
	var x = new RegExp("^.{1,}[\.]wav$");
	return x.test(value);
}
function opencalc()//its a jugar used in htm pages .. this script is included in all the pages so we can use it
{
        window.open("http://www.infinioffice.com/registration/getprice.php","","width=800,height=550,status=yes, menubar=no, scrollbars=yes,left=75,top=100,resizable=yes");
}

