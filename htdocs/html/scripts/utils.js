	var product1 = new Array();
	var ids = new Array();

function ChangeDivContent( contents )
{
	document.getElementById('ajaxDivContent').innerHTML=contents;
}
function RemoveWhiteSpace(str)
{
     var str1 = (str).replace(/^\s*|\s*$/g,'');
     return str1;
}
function cut(arr, i)
{
   var pre = arr.slice(0,i);
   var post = arr.slice(i+1, arr.length);
   return pre.concat(post);
}
function trimString(str,len)
{
	if (str.length <= len)
		return str;
	else
		return str.substr(0,len-3)+'...';
}
function getExpDate(days, hours, minutes) {
    var expDate = new Date( );
    if (typeof days == "number" && typeof hours == "number" &&
        typeof hours == "number") {
        expDate.setDate(expDate.getDate( ) + parseInt(days));
        expDate.setHours(expDate.getHours( ) + parseInt(hours));
        expDate.setMinutes(expDate.getMinutes( ) + parseInt(minutes));
        return expDate.toGMTString( );
    }
}

// utility function called by getCookie( )
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf (";", offset);
    if (endstr == -1) {
        endstr = document.cookie.length;
    }
    return unescape(document.cookie.substring(offset, endstr));
}

// primary function to retrieve cookie by name
function getCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg) {
            return getCookieVal(j);
        }
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return "";
}
 function ReplaceString(string,char)
 {
 	var intIndexOfMatch = string.indexOf( char );

	     // Loop over the string value replacing out each matching substring.
	     while (intIndexOfMatch != -1){
	    	 // Relace out the current instance.
		     string = string.replace( "+", " " )

		     // Get the index of any next matching substring.
	     	intIndexOfMatch = string.indexOf( "+" );
	     }
	    return string;
 }
// store cookie value with optional details as needed
function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape (value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

// remove the cookie by setting ancient expiration date
function deleteCookie(name,path,domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}
function ParseValues(str)
{
	if (str!="")
	{
		var sSearch = str;
		if (sSearch.length > 0)
		{
	    	var recValues = sSearch.split('|');

			var arr1 = new Array(recValues.length-1);
			for (var j = 0; j < recValues.length; j++)
			{
				var asKeyValues = recValues[j].split('&');
				var asKeyValue = '';

				arr1[j] = new Array(asKeyValues.length-1);

				for (var i = 0; i < asKeyValues.length; i++)
				{
					asKeyValue = asKeyValues[i].split('=');
	            	arr1[j][asKeyValue[0]] = asKeyValue[1];

				}
			}
		}
		return arr1;
	}
	else
	{
		return new Array();
	}
}
function ShowWindow(url,scroll,width,height,name)
{
	if(width == undefined && height == undefined)
	{
		width=.80;
		height=.70;
	}

	if (scroll == undefined)
		scroll="yes";

	if (name == undefined)
		name = "win";

	var w = parent.document.body.clientWidth*width;
	var h = screen.height*height;

	var x = (parent.document.body.clientWidth - w)/2;
	var y = (screen.height - h)/4;

	return OpenWindow(name,w,h,y,x,"yes","yes",scroll,"no","no",url,"");
}
function QueryString(url)
{
	var oQuery = new Object();
	var sSearch;

	if (url)
		sSearch = url;
	else
		sSearch = document.location.search.substring(1);

	if (sSearch.length > 0)
	{
		var asKeyValues = sSearch.split('&');
		var asKeyValue = '';
		for (var i = 0; i < asKeyValues.length; i++)
		{
			asKeyValue = asKeyValues[i].split('=');
			oQuery[asKeyValue[0]] = asKeyValue[1];
		}
	}
	return oQuery;
}


// Trim leading,traailing and middle spaces in a string
function trim(value) {
   var temp = value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
   var obj = / +/g;
   temp = temp.replace(obj, " ");
   if (temp == " ") { temp = ""; }
   return temp;
}

// Trim leading,traailing and middle spaces in all fields of a form
function trimAll()
{//alert();
	var frms = document.forms;
	//alert(document);
	//alert(document.forms);
	var arr;

	for(i=0;i<frms.length;i++)
	{
		arr = frms[i].elements;
		for(j=0;j<arr.length;j++)
		{
			if(arr[j].type == "textarea" || arr[j].type == "text")
				arr[j].value = trim(arr[j].value);
		}
	}
}
function ShowError(errorList)
{
	var msg = "";

	msg  = "______________________________________________________\n\n";
	msg += "The form could not continue because of the following error(s).\n";
	msg += "Please correct these error(s) and re-continue.\n";
	msg += "______________________________________________________\n\n";

	msg += errorList;
	alert(msg);
}
function encode64(input)
{

   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) +
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);

   return output;
}

function decode64(input)
{
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length );

   return output;
}
	function PostAjaxScreen(url,form,obj,isUrl)
	{
		//debugger;
		ShowBlockDiv("");
		var i = Math.round(10000000*Math.random());

	 	if(!isUrl)
	 		url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
	 	url = url + "&rand="+i;
		if(!form)
		{
			alert('form is null');
			return false;
		}

		var args = obj;

		if(typeof(form)=="object" && typeof(form.serialize)!='undefined')
		{
			data = form.serialize();
		}
		else
		{
			if(!$)
			 $=jQuery;
			data = $(form).serialize();
		}

		if(args)
		{

			if(!args.method)
				args.method =  'post';

			if(!args.parameters)
				args.parameters = data;
			else
				args.parameters += '&' + data;
				//args.parameters.push(data);

			if(!args.onSuccess)
				args.onSuccess = AjaxCallback;

			if(!args.onFailure)
				args.onFailure = AjaxReportError ;

			if(!args.onException)
				args.onException = AjaxException;
		}
		else
		{
			args = Object();
			args.method =  'post';
			args.parameters = data;
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;
			args.onException = AjaxException;
		}

		if(typeof(srcW) != 'undefined' && typeof(srcW.ShowWaitDialog) != 'undefined')
			srcW.ShowWaitDialog();
			//alert(srcW.ShowWaitDialog);
		//alert(parent.parent.Ajax.Request);
		//if(typeof(parent.parent.document.getElementById('IFControl'))!="undefined")
		//	var myAjax = new parent.parent.document.getElementById('IFControl').contentWindow.Ajax.Request( url, args );
		//else
			var myAjax = new Ajax.Request( url, args );
	}

	function PostFileAjaxScreen(url,form,obj,isUrl)
	{
		ShowBlockDiv("");
		var i = Math.round(10000000*Math.random());
	 	if(!isUrl)
	 		url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
	 	url = url + "&rand="+i;
		if(!form)
		{
			alert('form is null');
			return false;
		}

		var args = obj;

		if(typeof(form)=="object" && typeof(form.serialize)!='undefined')
		{
			data = form.serialize();
		}
		else
		{
			if(!$)
			 $=jQuery;
			//data = $(form).serializeArray();//$(form).serialize();
			var data = new FormData($(form)[0]);
		}

		if(args)
		{

			if(!args.method)
				args.method =  'post';

			if(!args.parameters)
				args.parameters = data;
			else
				args.parameters += '&' + data;
				//args.parameters.push(data);

			if(!args.onSuccess)
				args.onSuccess = AjaxCallback;

			if(!args.onFailure)
				args.onFailure = AjaxReportError ;

			if(!args.onException)
				args.onException = AjaxException;
		}
		else
		{
			args = Object();
			args.method =  'post';
			args.parameters = data;
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;
			args.onException = AjaxException;
		}

		if(typeof(srcW) != 'undefined' && typeof(srcW.ShowWaitDialog) != 'undefined')
			srcW.ShowWaitDialog();
		jQuery.ajax({
			url: url,
			type: 'POST',
			data: args.parameters,
			cache: false,
			contentType: false,
	        processData: false,
			//dataType: "json",
	        mimeType: "multipart/form-data",
			async: false,
			success: function(res)
			{
				response = new Object();
				response.responseText = res;
				eval(args.onSuccess(response));
			}

		});
		// alert(srcW.ShowWaitDialog);
		// alert(parent.parent.Ajax.Request);
		//if(typeof(parent.parent.document.getElementById('IFControl'))!="undefined")
		//	var myAjax = new parent.parent.document.getElementById('IFControl').contentWindow.Ajax.Request( url, args );
		//else
			// var myAjax = new Ajax.Request( url, args );
	}
	function ShowLastAjaxScreen()
	 {
		ShowBlockDiv("");
		var cookie = getCookie('last_ajax_action');
		var lastsid = getCookie('last_sid');

		if(cookie && _sessionid_==lastsid )
		{
				var op = JSON.decode(cookie);
				alert(op.url+'|'+op.args);
		}
	}
	function LoadLastAjaxScreen()
	{
		ShowBlockDiv("");
		var cookie = getCookie('last_ajax_action');
		var lastsid = getCookie('last_sid');
		if(cookie && _sessionid_==lastsid )
		{
				var op = JSON.decode(cookie);
				LoadAjaxScreen(op.url,op.args,true);
		}
		else
			LoadAjaxScreen('showemailaccounts');


	}
	/*function check_hash()
	{
    if ( window.location.hash != current_hash ) {
        current_hash = window.location.hash;
        page_change( current_hash.substr( 1,  current_hash.length) );
    }
	}*/

	function html_entity_decode(str)
	{
		var  tarea=document.createElement('textarea');
		tarea.innerHTML = str;
		return tarea.value;
		tarea.parentNode.removeChild(tarea);
	}
	function check_hash()
	{

		if(loading==1)
		{
			//alert('file is loaing....');
			return;
		}
		current_hash1 = document.getElementById('historyFrame').contentWindow.document.body.innerHTML;
		latestHash = '#'+html_entity_decode(current_hash1);
		latestHashString = latestHash.split('&');
		urlHash = latestHash;

		windownHash = window.location.hash.split('&');


		winURL= window.location.hash.split('&');

		//alert(winURL[0]+'--'+latestHashString[0]);
		//return;
		//alert(urlHash);
	   if(urlHash!='#test' & urlHash!='#stop')
	   {
		   	if ( winURL[0] != latestHashString[0] )
		    {
		    	//alert('reloadto::::'+urlHash);
		    	var urls = urlHash.split('#');
		    	var url = urls[1];

		    	/*succurl = latestHashString[1];
		    	onsucstring = succurl.split('objsuc=');
		    	onsuc = onsucstring[1];
		    	var obj = {onSuccess:onsuc};*/
		    	//alert(url+'--'+obj)

		    	LoadAjaxScreen(url);
		       // current_hash = window.location.hash;
		       // page_change( current_hash.substr( 1,  current_hash.length) );
		    }

		    //else
		    //	alert('rehnedo');
	   }
	   else if(urlHash=='#stop')
	   {
	   }

	   else
	   {
	    if(location.href.indexOf('weddingplanner')!=-1 || location.href.indexOf('shadi')!=-1)
	    	LoadAjaxScreen('weddingplanner_homepage');
	    else if(location.href.indexOf('ehub.com.pk')!=-1)
	    	LoadAjaxScreen('maincontroller');
	    else
	       	LoadAjaxScreen('mainpage');
	   }

	  // alert('done');
	}
	var c=0;
	var loading=0;
	function LoadAjaxScreen(url,obj,isUrl,popup)
	{
		

		loading =1;
		if(popup!="yes")
		{
			location.hash=url;
			var current_hash = window.location.hash;
		
			//alert(document.getElementById('historyFrame').contentWindow.document.body.innerHTML);
		}
		ShowBlockDiv("");
		//debugger;
		var i = Math.round(10000000*Math.random());
		if(!isUrl)
			url = "index.php?SCREEN=" + url +'&sid='+_sessionid_+'&isajaxcall=1' ;

		url = url + "&rand="+i;
		var args = obj;
		if(args)
		{
			if(!args.onSuccess)
			{
				args.onSuccess = function(){HideBlockDiv();AjaxCallback};
				//ShowBlockDiv("");
			}

			/*if(!args.onFailure)
				args.onFailure = AjaxReportError ;

			if(!args.onException)
				args.onException = AjaxException;
			*/
		}
		else
		{
			//ShowBlockDiv("");
			args = Object();
			args.method =  'post';
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;
			args.onException = AjaxException;
		}
		if(!args.deleteList)
		{
				if(typeof(list)=="object" && list.GetItemCount() >0)
				list.DeleteAllItems();
		}

		//alert("load"+mylink.length);
		var link = new Array();
		if(typeof(mylink)!="undefined")
			link = mylink;
		else
			link = parent.mylink;


		//setCookie("last_sid", _sessionid_);

		if(typeof(link)!="undefined")
		{
			if(link.length !=0 && link[link.length-1]['url']==url&& link[link.length-1]['args']==args)
	    	{
	    		return false;
	    	}

			if (!args.skipHistory)
			{
				// History work
		    	var op = {"url":url,"args":args};
		    	op = JSON.encode(op);
		    	setCookie("last_ajax_action", op);
				link[link.length] = {"url":url,"args":args};
				parent.tmpindex=link.length-1;
			}
		}

		jQuery.ajax({
					url: url,
					type: "POST",
					data: encodeURIComponent(args),
					cache: false,
					success: args.onSuccess
				});


	}




  	function AjaxCallback(response)
 	{

		HideBlockDiv();
		try
		{
			// For backward compatibility option, will remove later when fully on ajax
			/*if (frames['IFControl'] && frames['IFControl'].frames['operationsarea'] && frames['IFControl'].frames['operationsarea'].showDiv)
				frames['IFControl'].frames['operationsarea'].showDiv('ajaxDivContent');
			else if(getIframeID(this) == 'IFControl' && frames['operationsarea'].showDiv)
				frames['operationsarea'].showDiv('ajaxDivContent');
			*/

			if (document.getElementById('projectNav'))
				document.getElementById('projectNav').style.display='none';

			if (document.getElementById('documentDivContent'))
				AjaxCallbackWorker(response,'documentDivContent',true);
			else
				AjaxCallbackWorker(response,'ajaxDivContent');
		}
		catch(e)
		{
			alert('Exp in callbackworker:'+e.description);
		}
  	}
	function AjaxCallbackWorker(response,divName,popup)
	{
		HideBlockDiv();


		//alert();
		if(typeof(srcW) != 'undefined')
 			srcW.HideWaitDialog();
		try
		{
			/*Tariq:used in ibz.sharetaskrights need to open screen in popup wizard and callback will be always document*/
			if(popup==true)
			{
				doc = document;
				var divElement = doc.getElementById(divName);
				parent.srcW.HideWaitDialog();
			}
			else if(typeof(divName) != 'object')
			{
				if(parent.document.getElementById('IFControl'))
				{
					if(_browsername_!='Mozilla')
						doc = parent.document.getElementById('IFControl').contentWindow.document;
					else
						doc = parent.document.getElementById('IFControl').contentDocument;
				}
				else if(document.getElementById('IFControl'))
				{
					if(document.getElementById('IFControl').contentWindow.document)
						doc = document.getElementById('IFControl').contentWindow.document;
					else
						doc = document.getElementById('IFControl').contentDocument;
				}
				else
				{
					if(document)
						doc = document;
					else
						doc= contentDocument;
				}

				var divElement = doc.getElementById(divName);
				//alert(divName);
				if(!divElement)
				{
					return;
				}
			}
			else
			{
				var divElement = divName;
			}

			doc = divElement.ownerDocument;
			//alert(doc.title);
			// Fix: Calling ajax screen from no ajax screen, opens navigate first and load ajax screen content from cookie of history

			if(divElement == null)
			{
				doc.location.href="/index.php?SCREEN=navigate&sid=" + _sessionid_ ;
			}
			else
				divElement.innerHTML = "";

			if(divElement == null)
				return;

			if(typeof(divName) == 'object')
			jQuery(divName).html(response);
			else
			jQuery('#'+divName).html(response);


		}
		catch(e)
		{

			alert('Exception:'+e.description);
		}
	}

	function AjaxException(objRequest, ex)
	{
		alert(ex);
	}

	function AjaxReportError(response)
	{
		HideBlockDiv();
		alert("Error:There is some error in completing this request. Please try again later.");
		response = null;
	}
	function ShowPopUpDiv()
	{
		document.getElementById('popupdiv').inerHTML='';
		document.getElementById('popupdiv').style.display='';
	}
	function HidePopUpDiv()
	{
		document.getElementById('popupdiv').style.display='none';
	}


	function trim(str, chars) {
		return ltrim(rtrim(str, chars), chars);
	}

	function ltrim(str, chars) {
		chars = chars || "\\s";
		return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
	}

	function rtrim(str, chars) {
		chars = chars || "\\s";
		return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
	}

	function getFileextension(inputId)
	{
		var fileinput = document.getElementById("foo");
		if(!fileinput )
			return "";
		var filename = fileinput.value;
		if( filename.length == 0 )
			return "";
		var dot = filename.lastIndexOf(".");
		if( dot == -1 )
			return "";
		var extension = filename.substr(dot,filename.length);
		return extension;
	}
	function Back()
	{
		ShowBlockDiv("");
		//alert("Start"+mylink.length);
		alert('back index'+tmpindex);

		var postBack;
		var urls;
		if(parent.mylink.length==undefined||parent.tmpindex==0||parent.mylink.length==0)
		{
			//document.getElementById('prevButton').disabled = true;
			//alert('No Previous Record found');
			return false;
		}

		parent.tmpindex--;
			//alert("back data"+mylink[tmpindex]['url']);

		postBack=parent.mylink[parent.tmpindex]['args'];
		urls=parent.mylink[parent.tmpindex]['url'];
		//alert(urls);
		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls, postBack );
		var myAjax = new Ajax.Request( urls, postBack );
//		alert(parent.tmpindex);
	}

	function Fwd()
	{
		ShowBlockDiv("");
		//alert("fwd s"+mylink.length);
		alert('fedindex'+tmpindex);
		var postFwd;
		var urls2;
		if(parent.mylink.length==0||parent.tmpindex<0||((parent.mylink.length-1)==parent.tmpindex))
		{
			//alert('No Fwd Record found');
			//document.getElementById('nextButton').disabled = true;
			return false;
		}

		parent.tmpindex++;
		postFwd=parent.mylink[parent.tmpindex]['args'];
		urls2=parent.mylink[parent.tmpindex]['url'];

		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls2, postFwd );
		var myAjax = new Ajax.Request( urls2, postFwd );
	}
	function ReLoad()
	{
		//alert("Start"+mylink.length);
		//alert('back index'+tmpindex);
		//debugger;
		var postBack;
		var urls;

		if(parent.mylink.length==undefined||parent.tmpindex==0||parent.mylink.length==0)
		{
			LoadAjaxScreen('showemailaccounts');
			//alert('No Previous Record found');
			return false;
		}

		parent.tmpindex;
		//alert("back data"+mylink[tmpindex]['url']);

		postBack=parent.mylink[parent.tmpindex]['args'];
		urls=parent.mylink[parent.tmpindex]['url'];

		srcW.ShowWaitDialog();
		LoadLastAjaxScreen();
		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls, postBack );
//		alert(parent.tmpindex);
	}

	function ShowBlockDiv(message)
	{
		//debugger;
		// w=document.getElementById('blockDiv');
		// if(message=="")
			// message="loading";
		// if(w)
		// {
			// w.style.display="";
			// if(message!='undefined')
				// w.innerHTML="<b><span class='disablespantext'>"+message+"</span></b>";
		// }
		////setTimeout("HideBlockDiv()", 5000);
		// $.blockUI();
			//document.getElementById("loader").style.display = "block";
	}
	function HideBlockDiv()
	{
		// w1=document.getElementById('blockDiv');
		// if(w1)
			// w1.style.display="none";
		// $.unblockUI();
			//document.getElementById("loader").style.display = "none";
	}

	function CheckError(response)
	{
		console.log(response);
		return false;
	}
