/*
var width = document.body.clientWidth * .50;
var height = document.body.clientHeight * .50;

var posx = (document.body.clientWidth - width)/2;
var posy = (document.body.clientHeight - height)/4;
*/

function RemoveWhiteSpace(str)
{
     var str1 = (str).replace(/^\s*|\s*$/g,'');
     return str1;
}

function GetFirstParentOfType(obj,tag)
{
	while(obj.tagName.toLowerCase()!=tag.toLowerCase() && obj.tagName.toLowerCase()!="body")
	{
		obj = obj.parentNode;
		
	}
	return obj;
}

// OLpageDefaults(RELX,posx,RELY,posy,FILTER,STATUS,'InfiniOffice Help Manager',WIDTH,width,HEIGHT,height,FADEIN,38,FADEOUT,38,STICKY,BGCOLOR,'#1E64B4',FGCOLOR,'aliceblue');

function cut(arr, i)
{
   var pre = arr.slice(0,i);
   var post = arr.slice(i+1, arr.length);
   return pre.concat(post);
}

function ParseValues(str)
{
	if (str!="")
	{
		var sSearch = str;
		if (sSearch.length > 0)
		{
	    	var recValues = sSearch.split('|');

			var arr1 = new Array(recValues.length-1);
			for (var j = 0; j < recValues.length; j++)
			{
				var asKeyValues = recValues[j].split('&');
				var asKeyValue = '';

				arr1[j] = new Array(asKeyValues.length-1);

				for (var i = 0; i < asKeyValues.length; i++)
				{
					asKeyValue = asKeyValues[i].split('=');
	            	arr1[j][asKeyValue[0]] = asKeyValue[1];

				}
			}
		}
		return arr1;
	}
	else
	{
		return new Array();
	}
}

function showWiz(args)
{
	var w = screen.availWidth-12;
	var h = screen.availHeight-50;
	var wd;
	
    if (args==undefined)
		wd = window.open('https://'+location.hostname+'/registration/signup.php','','left=0,top=0,toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h) ;
	else
		wd = window.open('https://'+location.hostname+'/registration/signup.php?'+args,'','left=0,top=0,toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width='+w+', height='+h) ;
}


function ShowWindow(url,scroll,width,height,name)
{
	if(width == undefined && height == undefined)
	{
		width=.80;
		height=.70;
	}
	
	if (scroll == undefined)
		scroll="yes";
	
	if (name == undefined)
		name = "win";
		
	var w = parent.document.body.clientWidth*width;
	var h = screen.height*height;
	
	var x = (parent.document.body.clientWidth - w)/2;
	var y = (screen.height - h)/4;

	return OpenWindow(name,w,h,y,x,"yes","yes",scroll,"no","no",url,"");
}

function pop(scr) //used in vooffice reports
{

	var r = Math.random();
	var w = parent.document.body.clientWidth*.80;
	var h = screen.height*.70;
	var x = (parent.document.body.clientWidth+w);
	var y = (screen.height+h);

//	url='<?=$PHP_SELF ?>?&SCREEN='+scr+'&sid=<?= $mysession->sessionid ?>'
	window.open(scr,target='_blank','left=40,top=70,toolbar=no,location=no,status=no,resizable=yes,menubar=no,scrollbars=yes,width='+(screen.availWidth-75)+',height='+(screen.availHeight-100))	
	
}

function OpenWindow(strName,iW,iH,TOP,LEFT,R,S,SC,T,TB,URL,TYPE)
{
	if (TYPE=="modal" || TYPE=="modalIframe")
	{
		var sF=""
		sF+=T?'titlebar='+T+',':'';
		sF+=TB?'help:'+TB+';':'';
		sF+=S?'status:'+S+';':'';
		sF+=SC?'scroll:'+SC+';':'';
		sF+=R?'resizable:'+R+';':'';
		sF+=iW?'dialogWidth:'+iW+'px;':'';
		sF+=iH?'dialogHeight:'+iH+'px;':'';
		sF+=TOP?'dialogTop:'+TOP+'px;':'';
		sF+=LEFT?'dialogLeft:'+LEFT+'px;':'';
		if (TYPE=="modal")
		window.showModalDialog(URL+"&r="+Math.round(Math.random()*1000000),"",sF);
	}
	else
	{
		var sF=""
		sF += iW?'width='+iW+',':'';
		sF+=iH?'height='+iH+',':'';
		sF+=R?'resizable='+R+',':'';
		sF+=S?'status='+S+',':'';
		sF+=SC?'scrollbars='+SC+',':'';
	//	sF+=T?'titlebar='+T+',':'';
		sF+=TB?'toolbar='+TB+',':'';
		sF+=TB?'menubar='+TB+',':'';
		sF+=TOP?'top='+TOP+',':'';
		sF+=LEFT?'left='+LEFT+',':'';

		//var HMW=window.open(URL?URL:'about:blank',strName?strName:'',sF);
		var HMW=window.open(URL?URL:'about:blank',strName?strName:'',sF);		
		if ( (document.window != null) && (!HMW.opener) )
		HMW.opener=document.window;
		HMW.focus();
	}

}

//========================================
//ACCESSING QUERYSTRING THROUGH JAVASCRIPT
//========================================
function QueryString(url)
{
	var oQuery = new Object();
	var sSearch;

	if (url)
		sSearch = url;
	else
		sSearch = document.location.search.substring(1);

	if (sSearch.length > 0)
	{
		var asKeyValues = sSearch.split('&');
		var asKeyValue = '';
		for (var i = 0; i < asKeyValues.length; i++)
		{
			asKeyValue = asKeyValues[i].split('=');
			oQuery[asKeyValue[0]] = asKeyValue[1];
		}
	}
	return oQuery;
}


// Trim leading,traailing and middle spaces in a string
function trim(value) {
   var temp = value;
   var obj = /^(\s*)([\W\w]*)(\b\s*$)/;
   if (obj.test(temp)) { temp = temp.replace(obj, '$2'); }
   var obj = / +/g;
   temp = temp.replace(obj, " ");
   if (temp == " ") { temp = ""; }
   return temp;
}

// Trim leading,traailing and middle spaces in all fields of a form
function trimAll()
{//alert();
	var frms = document.forms;
	//alert(document);
	//alert(document.forms);
	var arr;

	for(i=0;i<frms.length;i++)
	{
		arr = frms[i].elements;
		for(j=0;j<arr.length;j++)
		{
			if(arr[j].type == "textarea" || arr[j].type == "text")
				arr[j].value = trim(arr[j].value);
		}
	}
}

// Javascript Error Message
function ShowError(errorList)
{
	var msg = "";

	msg  = "______________________________________________________\n\n";
	msg += "The form could not continue because of the following error(s).\n";
	msg += "Please correct these error(s) and re-continue.\n";
	msg += "______________________________________________________\n\n";

	msg += errorList;
	alert(msg);
}

// To check Windows SP2
function CheckBrowserSP2()
{ 
	var issp2 = false; 
	issp2 = (window.navigator.userAgent.indexOf("SV1")!= -1); 

	if (issp2)
	{ 
		alert('This browser is internet Explorer in sp2') 
		return true;
	} 
	else 
	{ 
		alert('This browser is not internet Explorer in sp2'); 
		return false;
	}
}

// Date functions starts here

var dtCh= "-";
var minYear=1900;
var maxYear=2100;

function isInteger(s)
{
	var i;
	for (i = 0; i < s.length; i++){
		// Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9"))) return false;
	}
	// All characters are numbers.
	return true;
}

function stripCharsInBag(s, bag)
{
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++){
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1) returnString += c;
	}
	return returnString;
}

function daysInFebruary (year)
{
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) 
{
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   }
   return this
}

function isDate(dtStr)
{
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : dd-mm-yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid year")
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
	return true
}

// date functions ends here


// Cookie functions starts here

// utility function to retrieve an expiration date in proper
// format; pass three integer parameters for the number of days, hours,
// and minutes from now you want the cookie to expire (or negative
// values for a past date); all three parameters are required,
// so use zeros where appropriate
function getExpDate(days, hours, minutes) {
    var expDate = new Date( );
    if (typeof days == "number" && typeof hours == "number" && 
        typeof hours == "number") {
        expDate.setDate(expDate.getDate( ) + parseInt(days));
        expDate.setHours(expDate.getHours( ) + parseInt(hours));
        expDate.setMinutes(expDate.getMinutes( ) + parseInt(minutes));
        return expDate.toGMTString( );
    }
}
   
// utility function called by getCookie( )
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf (";", offset);
    if (endstr == -1) {
        endstr = document.cookie.length;
    }
    return unescape(document.cookie.substring(offset, endstr));
}
   
// primary function to retrieve cookie by name
function getCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg) {
            return getCookieVal(j);
        }
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break; 
    }
    return "";
}
   
// store cookie value with optional details as needed
function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape (value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
   
// remove the cookie by setting ancient expiration date
function deleteCookie(name,path,domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}

// Cookie functions ends here


// Hide or Show Group (eg. <tr group="anygroup"> ... </tr> )
function ShowGroup(tableid,gname,show)
{
	//alert(tableid+','+gname+','+show);
	if (show == undefined)
		show = "";
	else if (!show)
		show = "none";
	//alert(document.getElementById(tableid));
	var trs = document.getElementById(tableid).rows;
	//alert(trs.length);
	for (var i=0;i<trs.length;i++)
	{
		//alert(trs[i].value+'|'+trs[i].getAttribute("group"));
		if (trs[i].getAttribute("group")==gname)
		{
			if (trs[i].style.display.toLowerCase() != show)
				trs[i].style.display = show;
		}	
	}
}

function ShowTr()
{
	var args = arguments;
	for (var i=0; i<args.length; i++)
	{
		if (document.getElementById(args[i]))
			document.getElementById(args[i]).style.display = "";
	}
}

function HideTr()
{
	var args = arguments;
	for (var i=0; i<args.length; i++)
	{
		if (document.getElementById(args[i]))
			document.getElementById(args[i]).style.display = "none";
	}
}

function getIframeID(el)
{
	var myTop;
	if (window.frameElement) 
	{
		myTop = window.frameElement;
	} 
	else if (window.top) 
	{
		myTop = window.top;
		var myURL = el.location.href;
		var iFs = myTop.document.getElementsByTagName('iframe');
		var x, i = iFs.length;
		while ( i-- )
		{
			x = iFs[i];
			if (x.src && x.src == myURL)
			{
				myTop = x;
				break;
			}
		}
	}
	if (myTop){
		return myTop.id;
	} 
	else 
	{
		return null;
	}
}

function trimString(str,len)
{
	if (str.length <= len)
		return str;
	else	
		return str.substr(0,len-3)+'...';
}

function GetProjectLabel(projectname,path)
{
//	alert("->GetProjectLabel("+projectname+","+path+")");
	path = projectname+"/"+path;
	path = path.substring(0,path.length-1);
	var re = new RegExp('/' , 'g');
	path = path.replace(re,' &rsaquo; ');
	return path;
}


/************* String functions **************/

String.prototype.trim = function() 
{
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() 
{
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() 
{
	return this.replace(/\s+$/,"");
}

/************************************************/

/************** Base 64 Encoding Decoding ***************/
function encode64(input) 
{
   
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
   
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) +
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);
  
   return output;
}

function decode64(input) 
{
   var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
   
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length );

   return output;
}

//********************************************************/
//********* Ajax Functions ****//////
/*	
	function PostAjaxScreen(url,form,args,isUrl) 
	{
		var i = Math.round(10000000*Math.random());
	 		 		
		if(!isUrl)
			url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
		url = url + "&isajaxcall=1&rand="+i;

		if(!form)
		{
			alert('form is null');
			return false;
		}
	
		if(!args)
			args = Object();
			
		if(!args.onSucess)
			args.onSuccess = AjaxCallback;
			
		if(!args.onFailure)
			args.onFailure = AjaxReportError ;
			
		if(!args.onException)
			args.onException = AjaxException;
				
		srcW.ShowWaitDialog();
		
		alert(Ajax);
		alert(form.parent.parent.frames['IFControl'].request);
//		form.parent.parent.frames['IFControl'].request(args);
		var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( url, args );
					
	}*/
	
	function PostAjaxScreen(url,form,obj,isUrl) 
	{
		//alert();
	 	var i = Math.round(10000000*Math.random());
	 		 		
	 	if(!isUrl)
	 		url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
	 	url = url + "&rand="+i;
		
		if(!form)
		{
			alert('form is null');
			return false;
		}
	
		var args = obj;
		//alert("form:"+form);
		data = Form.serialize(form);
		
		//alert("url:"+url);
//		if(obj.params)
//			args.parameters = obj.params;
			
		if(args)
		{
			
			if(!args.method)
				args.method =  'post';
				
			if(!args.parameters)
				args.parameters = data;	
			else
				args.parameters += '&' + data;
				//args.parameters.push(data);
				
			if(!args.onSuccess)
				args.onSuccess = AjaxCallback;
				
			if(!args.onFailure)
				args.onFailure = AjaxReportError ;
				
			if(!args.onException)
				args.onException = AjaxException;
		}
		else
		{
			args = Object();
			args.method =  'post';
			args.parameters = data;	
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;	
			args.onException = AjaxException;
		}
				
		if(typeof(srcW) != 'undefined' && typeof(srcW.ShowWaitDialog) != 'undefined')	
			srcW.ShowWaitDialog();
			//alert(srcW.ShowWaitDialog);
		//alert(parent.parent.Ajax.Request);
		//if(typeof(parent.parent.document.getElementById('IFControl'))!="undefined")
		//	var myAjax = new parent.parent.document.getElementById('IFControl').contentWindow.Ajax.Request( url, args );
		//else
			var myAjax = new Ajax.Request( url, args );
	}
	function ShowLastAjaxScreen()
	{
		var cookie = getCookie('last_ajax_action');
		var lastsid = getCookie('last_sid');

		if(cookie && _sessionid_==lastsid )			
		{
				var op = JSON.decode(cookie);
				alert(op.url+'|'+op.args);	
		}	
	}
	function LoadLastAjaxScreen()
	{
		var cookie = getCookie('last_ajax_action');
		var lastsid = getCookie('last_sid');
		if(cookie && _sessionid_==lastsid )			
		{
				var op = JSON.decode(cookie);
				LoadAjaxScreen(op.url,op.args,true);	
		}		
		else
			LoadAjaxScreen('showemailaccounts');

		
	}
	function ControlAct(controller,action,isUrl)
	{
		var i = Math.round(10000000*Math.random());
	 		 	var url;	
		if(!isUrl)
			url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
		
		url = url + "&rand="+i;
		url = "main.php?controller=" + controller + "&action=" + action +"&sid=" + _sessionid_ ;
//		var args = obj;
		var args = {};
		if(args)
		{
			if(!args.onSuccess)
				args.onSuccess = AjaxCallback;
				
			if(!args.onFailure)
				args.onFailure = AjaxReportError ;
				
			if(!args.onException)
				args.onException = AjaxException;
			
		}
		else
		{
			args = Object();
			args.method =  'post';
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;	
			args.onException = AjaxException;
		}
		
		if(!args.deleteList)	
		{
				if(typeof(list)=="object" && list.GetItemCount() >0)
				list.DeleteAllItems();	
		}		
		//alert("load"+mylink.length);		
		var link;
		if(typeof(mylink)!="undefined")
			link = mylink;
		else
			link = parent.mylink;			
		
		
		setCookie("last_sid", _sessionid_); 
		if(typeof(link)!="undefined")
		{
			if(link.length !=0 && link[link.length-1]['url']==url&& link[link.length-1]['args']==args)
	    	{
	    		return false;
	    	}		
	
			if (!args.skipHistory)
			{
				// History work
		    	var op = {"url":url,"args":args};
		    	op = JSON.encode(op);
		    	setCookie("last_ajax_action", op); 
		    	//alert('co:'+getCookie("last_ajax_action"));
				link[link.length] = {"url":url,"args":args};
				parent.tmpindex=link.length-1;
			}
		}
		// Loading div
		if(typeof(srcW) != 'undefined')	
			srcW.ShowWaitDialog();
		//alert(url);
		var w;
		var myAjax ;
		
//		if(Ajax)
			myAjax = new Ajax.Request( url, args );
//		else if(parent.frames['IFControl'].Ajax)
//			myAjax= parent.frames['IFControl'].Ajax.Request( url, args );
//		else
//			myAjax= parent.parent.frames['IFControl'].Ajax.Request( url, args );

//		alert("End"+mylink.length+"tmpindex"+tmpindex);
			
	}
	function LoadAjaxScreen(url,obj,isUrl) 
	{

		
		if( _siteprefix_=="")
			var href = '';
		else
			var href = 'http://www.infinime.com';
		if( _siteprefix_=="123")
		{
			//alert(parent._start_)
			if(parent._start_==0)
			{
				//alert("setting cookie");
				var op = {"val":_sessionid_};
		    	op = JSON.encode(op);
		    	setCookie(_sessionid_, op); 
		    	test = getCookie(_sessionid_);
		    	if(test=='')
		    	{
		    		alert('Cookies are currently not enabled. Please enable your browser cookies.');
		    		return;
		    	}
		    	parent._start_=1;
			}
			else if(parent._start_==1)
			{
				//alert("getting cookie--"+_sessionid_+"--");
				op1 = getCookie(_sessionid_);
				//alert(op1);
				if(op1=="")
				{
					alert("You are unable to continue, Your IP Address is not authenticated");
					//url = "noauth";
					//location.href=href;
					return;
				}
				else
				{
					op1 = JSON.decode(op1);
				
					if(op1["val"]!=parent._checkwith_)
					{
						//alert(op1["val"]+'====='+parent._checkwith_);
						alert("You are unable to continue, Your IP Address is not authenticated");
						//url = "noauth";					
						//location.href=href;
						return;
					}
				}
			}
		}
		//debugger;
		var i = Math.round(10000000*Math.random());
		if(!isUrl)
			url = "index.php?SCREEN=" + url +'&sid='+_sessionid_ ;
		
		url = url + "&rand="+i;
		//url = "main.php?controller=Test&action=showtest&sid=" + _sessionid_ ;
		var args = obj;
		if(args)
		{
			if(!args.onSuccess)
			{
				args.onSuccess = AjaxCallback;
				ShowBlockDiv("");
			}
				
			if(!args.onFailure)
				args.onFailure = AjaxReportError ;
				
			if(!args.onException)
				args.onException = AjaxException;
			
		}
		else
		{
			ShowBlockDiv("");
			args = Object();
			args.method =  'post';
			args.onFailure = AjaxReportError;
			args.onSuccess = AjaxCallback;	
			args.onException = AjaxException;
		}
		
		if(!args.deleteList)	
		{
				if(typeof(list)=="object" && list.GetItemCount() >0)
				list.DeleteAllItems();	
		}		
		//alert("load"+mylink.length);		
		var link;
		if(typeof(mylink)!="undefined")
			link = mylink;
		else
			link = parent.mylink;			
		
		
		setCookie("last_sid", _sessionid_); 
		if(typeof(link)!="undefined")
		{
			if(link.length !=0 && link[link.length-1]['url']==url&& link[link.length-1]['args']==args)
	    	{
	    		return false;
	    	}		
	
			if (!args.skipHistory)
			{
				// History work
		    	var op = {"url":url,"args":args};
		    	op = JSON.encode(op);
		    	setCookie("last_ajax_action", op); 
		    	//alert('co:'+getCookie("last_ajax_action"));
				link[link.length] = {"url":url,"args":args};
				parent.tmpindex=link.length-1;
			}
		}
		// Loading div
		if(typeof(srcW) != 'undefined')	
			srcW.ShowWaitDialog();
		//alert(url);
		var w;
		var myAjax ;
//		if(Ajax)
			myAjax = new Ajax.Request( url, args );
//		else if(parent.frames['IFControl'].Ajax)
//			myAjax= parent.frames['IFControl'].Ajax.Request( url, args );
//		else
//			myAjax= parent.parent.frames['IFControl'].Ajax.Request( url, args );

//		alert("End"+mylink.length+"tmpindex"+tmpindex);
	}


	
 
  	function AjaxCallback(response)
 	{
		HideBlockDiv();
		try
		{
			// For backward compatibility option, will remove later when fully on ajax
			/*if (frames['IFControl'] && frames['IFControl'].frames['operationsarea'] && frames['IFControl'].frames['operationsarea'].showDiv)
				frames['IFControl'].frames['operationsarea'].showDiv('ajaxDivContent');
			else if(getIframeID(this) == 'IFControl' && frames['operationsarea'].showDiv)
				frames['operationsarea'].showDiv('ajaxDivContent');
			*/

			if (document.getElementById('projectNav'))
				document.getElementById('projectNav').style.display='none';
			
			if (document.getElementById('documentDivContent'))
				AjaxCallbackWorker(response,'documentDivContent',true);							
			else
				AjaxCallbackWorker(response,'ajaxDivContent');
		}
		catch(e)
		{
			alert('Exp in callbackworker:'+e.description);
		}	
  	}
	function AjaxCallbackWorker(response,divName,popup)
	{
		//alert('worker');
		//debugger;
		if($('projectNav'))
		{
	 		if($('projectNav').style.display == 'block')
			{
				$('ajaxDivContent').style.height = '95%';
			}
			else
				$('ajaxDivContent').style.height = '100%';
		}	
		
 		if(typeof(srcW) != 'undefined')
 			srcW.HideWaitDialog();
		try
		{
			/*Tariq:used in ibz.sharetaskrights need to open screen in popup wizard and callback will be always document*/
			if(popup==true)
			{
				doc = document;
				var divElement = doc.getElementById(divName);
				parent.srcW.HideWaitDialog();
			}
			else if(typeof(divName) != 'object')
			{
				if(parent.document.getElementById('IFControl'))
				{
					if(_browsername_!='Mozilla')
						doc = parent.document.getElementById('IFControl').contentWindow.document;
					else
						doc = parent.document.getElementById('IFControl').contentDocument;
				}
				else if(document.getElementById('IFControl'))
				{
					if(document.getElementById('IFControl').contentWindow.document)
						doc = document.getElementById('IFControl').contentWindow.document;
					else
						doc = document.getElementById('IFControl').contentDocument;
				}
				else
				{	
					if(document)
						doc = document;
					else
						doc= contentDocument;	
				}
	
				var divElement = doc.getElementById(divName);
			}
			else
			{
				var divElement = divName;
			}	
			doc = divElement.ownerDocument;
			//alert(doc.title);
			// Fix: Calling ajax screen from no ajax screen, opens navigate first and load ajax screen content from cookie of history

			if(divElement == null)
			{
				doc.location.href="/index.php?SCREEN=navigate&sid=" + _sessionid_ ;
			}
			else
				divElement.innerHTML = "";
			

			if(divElement == null)
				return;
			var stags = response.responseText.extractScripts();
			// Fix for external JS files in ajax request
			var matchAll = new RegExp('(?:<script.*src=(.)*?>)((\n|\r|.)*?)(?:<\/script>)', 'img');
			var scripts = response.responseText.match(matchAll);
			if (scripts == null) scripts = [];
			// To avoid duplicate loading
			var scriptFiles = [];
			var sc = doc.getElementsByTagName('script');
			for (var index=0; index<sc.length; index++)
			{
				if (sc[index].src != '')
				{
					
					/*if(sc[index].attributes[0].nodeValue)
					
					scriptFiles[sc[index].src] = 1;
					
					*/
					if(_browsername_=='Mozilla')
						scriptFiles[sc[index].attributes[0].nodeValue] = 1;
					else 				
						scriptFiles[sc[index].src] = 1;
					
				}
			}

			var exScripts = [];
			for (var x=0;x<scripts.length;x++)
			{
				var matchOne = /(?:<script.*src=[' "]?(([^'"])*)['"]? ?.*>)((\n|\r|.)*?)(?:<\/script>)/im;
				var js = scripts[x].match(matchOne)[1];

				// skip if already included
				if (typeof(scriptFiles[js]) != 'undefined')
					continue;

				if(js.substring(js.length-3,js.length) == "gif")
				{
					continue;	
				}	
				var s1 = doc.createElement('SCRIPT');
				//s1.type = 'text/javascript'; 
				s1.setAttribute('type', 'text/javascript');
				s1.src = js
				
//				(s1.src);
//				alert(js);
				exScripts[exScripts.length] = s1;
			//	divElement.getElementsByTagName('head')[0].appendChild(s1);
			//	divElement.appendChild(s1);
			}

			for (var i1=0;i1<exScripts.length;i1++)
			{
				//alert(exScripts[i1]);
				doc.getElementsByTagName('head')[0].appendChild(exScripts[i1]);
				//doc.appendChild(exScripts[i1]);
			}
			
			
			divElement.innerHTML = response.responseText;
//			alert('opening window');
//			var winObj = window.open(response.responseText);
//			winObj.document.write(response.responseText);
			
			for (var i=0;i<stags.length;i++)
			{
				if(stags[i] == "")
					continue;
				
				var s = doc.createElement("SCRIPT");
				s.text = stags[i];
				divElement.appendChild(s);				
			}
			
				
		}
		catch(e)
		{
			
			alert('Exception:'+e.description);
		}
		response = null;
		stags = null;
		scripts = null;	

	}

	function AjaxException(objRequest, ex)
	{
		alert(ex);
	}

	function AjaxReportError(response)
	{
		HideBlockDiv();
		alert("Error:There is some error in completing this request. Please try again later.");
		response = null;
	}

///*********End Ajax Functions *********
//********************************************************

	// Navigation Functions
 	function Back()
	{
		//alert("Start"+mylink.length);		
		//alert('back index'+tmpindex);
		
		var postBack;		
		var urls;
	
		if(parent.mylink.length==undefined||parent.tmpindex==0||parent.mylink.length==0)
		{
			//document.getElementById('prevButton').disabled = true;
			//alert('No Previous Record found');
			return false;
		}
	
		parent.tmpindex--;
		//alert("back data"+mylink[tmpindex]['url']);
		
		postBack=parent.mylink[parent.tmpindex]['args'];
		urls=parent.mylink[parent.tmpindex]['url'];
		
		srcW.ShowWaitDialog();
		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls, postBack );			
		var myAjax = new Ajax.Request( urls, postBack );			
//		alert(parent.tmpindex);
	}
	
	function Fwd()
	{
		//alert("fwd s"+mylink.length);
		//alert('fedindex'+tmpindex);
		var postFwd;
		var urls2;
		if(parent.mylink.length==0||parent.tmpindex<0||((parent.mylink.length-1)==parent.tmpindex))
		{	
			//alert('No Fwd Record found');
			//document.getElementById('nextButton').disabled = true;
			return false;
		}
		
		parent.tmpindex++;	
		postFwd=parent.mylink[parent.tmpindex]['args'];
		urls2=parent.mylink[parent.tmpindex]['url'];
		
		srcW.ShowWaitDialog();
		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls2, postFwd );	
		var myAjax = new Ajax.Request( urls2, postFwd );	
	}	
	function ReLoad()
	{
		//alert("Start"+mylink.length);		
		//alert('back index'+tmpindex);
		//debugger;
		var postBack;		
		var urls;
	
		if(parent.mylink.length==undefined||parent.tmpindex==0||parent.mylink.length==0)
		{
			LoadAjaxScreen('showemailaccounts');
			//alert('No Previous Record found');
			return false;
		}
	
		parent.tmpindex;
		//alert("back data"+mylink[tmpindex]['url']);
		
		postBack=parent.mylink[parent.tmpindex]['args'];
		urls=parent.mylink[parent.tmpindex]['url'];
		
		srcW.ShowWaitDialog();
		LoadLastAjaxScreen();
		//var myAjax = new parent.parent.frames['IFControl'].Ajax.Request( urls, postBack );			
//		alert(parent.tmpindex);
	}
	
	// Calendar control date format	
	function validateDate(dateString) 
	{
	    var RegExPattern = /^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;
	    if ( (dateString!='') && (dateString.match(RegExPattern)))
	        return true; 
	    
	    return false; 
	}
	
	//Event Notifier
	function EventNotifier(evt,args)
	{
		OnNotification(evt,args);
	}
	function playPhoneRecord(sid,type,id)
	{
		var uid=list.items[0]['uid'];
		
		ShowWindow("/misc/play.php?type="+type+"&uniqueid="+uid+"&sid="+sid+"&recordid="+id,'no',.2,.2);
	}
	function ShowBlockDiv(message)
	{	
		//debugger;
		if (document.getElementById('blockDiv'))
			w=document.getElementById('blockDiv');
		else if(parent.document.getElementById('blockDiv'))
			w=parent.document.getElementById('blockDiv');
		else if(parent.parent.document.getElementById('blockDiv'))
			w=parent.parent.document.getElementById('blockDiv');
		
		
		if(w)
		{
			w.style.display="";
			if(message!='undefined')
				w.innerHTML="<b><span class='disablespantext'>"+message+"</span></b>";
		}
		
	}
	function HideBlockDiv()
	{

		if (frames['IFControl'])
			w=frames['IFControl'];
		else if(parent.frames['IFControl'])
			w=parent.frames['IFControl'];
		else if(parent.parent.frames['IFControl'])
			w=parent.parent.frames['IFControl'];

		if (document.getElementById('blockDiv'))
			w1=document.getElementById('blockDiv');
		else if(parent.document.getElementById('blockDiv'))
			w1=parent.document.getElementById('blockDiv');
		else if(parent.parent.document.getElementById('blockDiv'))
			w1=parent.parent.document.getElementById('blockDiv');
		
		if(w1)
			w1.style.display="none";
	}
	//custom alert
	function alert1(msg,type)
	{
		/*
		 * type undefined or 0 ---- Error
		 * type=1 msg
		 * */
		ShowMsgDiv(msg,'');
		
		if (document.getElementById('btnDivOk'))
			d=document;
		else if(parent.document.getElementById('btnDivOk'))
			d=parent.document;		
		else if(parent.parent.document.getElementById('btnDivOk'))
			parent.parent.document;					

		if(typeof msgType=='undefined' || msgType==0)
			d.getElementById('headermsg').innerHTML="<b>Oops!</b>";
			
		d.getElementById('btnDivCancel').style.display='none';
		d.getElementById('btnDivContinue').style.display='none';
		d.getElementById('btnDivOk').style.display='';
	}
	/*
	 * type=1 --- cancel and continue
	 * type=2---- ok
	 * */
	function ShowBtn(type)
	{
		if (document.getElementById('btnDivOk'))
			d=document;
		else if(parent.document.getElementById('btnDivOk'))
			d=parent.document;		
		else if(parent.parent.document.getElementById('btnDivOk'))
			parent.parent.document;					

		if(typeof msgType=='undefined' || msgType==0)
			d.getElementById('headermsg').innerHTML="<b>Preview!</b>";
			
		if(type==1)
		{
			d.getElementById('btnDivCancel').style.display='';
			d.getElementById('btnDivContinue').style.display='';
			d.getElementById('btnDivOk').style.display='none';
		}
		else if(type==2)
		{
			d.getElementById('btnDivCancel').style.display='none';
			d.getElementById('btnDivContinue').style.display='none';
			d.getElementById('btnDivOk').style.display='';
		}
	}
	function ShowMsgDiv(message,continueClick,isImg)
	{	
		//debugger;
		if (document.getElementById('popupMsgDiv'))
			d=document;
		else if(parent.document.getElementById('popupMsgDiv'))
			d=parent.document;
		else if(parent.parent.document.getElementById('popupMsgDiv'))
			d=parent.parent.document;
		
		
		if(d)
		{
			w=d.getElementById('popupMsgDiv');
			w1=d.getElementById('popupMsgDivMessage');
			b1=d.getElementById('btnDivContinue');
			t1=d.getElementById('popupMsgDivTable');
			t2=d.getElementById('popupMsgDivTable2');
				
			d.getElementById('btnDivCancel').style.display='';
			d.getElementById('btnDivContinue').style.display='';
			d.getElementById('btnDivOk').style.display='none';
	
			if(isImg=='yes')
			{
				ShowBtn(2)
				w.style.display="";
				w.style.height='500px';
				w1.style.height='250px';
				t2.style.paddingTop='310';
			}
			else
			{
				w.style.display="";
				w.style.height='500px';
				w1.style.height='200px';
				t2.style.paddingTop='310';
			}
			
			if(message!='undefined')
			{
				w1.innerHTML=message;
				b1.onclick=function(){eval(continueClick)};
			}
		}
		
	}
	function HideMsgDiv()
	{

		if (frames['IFControl'])
			w=frames['IFControl'];
		else if(parent.frames['IFControl'])
			w=parent.frames['IFControl'];
		else if(parent.parent.frames['IFControl'])
			w=parent.parent.frames['IFControl'];

		//w.document.getElementById('blockDiv').style.display="none";
		
		if (document.getElementById('popupMsgDiv'))
		{
			w1=document.getElementById('popupMsgDiv');
			w2=document.getElementById('popupMsgDivMessage');
		}
		else if(parent.document.getElementById('popupMsgDiv'))
		{
			w1=parent.document.getElementById('popupMsgDiv');
			w2=parent.document.getElementById('popupMsgDivMessage');		
		}
		else if(parent.parent.document.getElementById('popupMsgDiv'))
		{
			w1=parent.parent.document.getElementById('popupMsgDiv');
			w2=parent.parent.document.getElementById('popupMsgDivMessage');			
		}
		
		
		if(w1)
			w1.style.display="none";
		if(w2)
			w2.innerHTML="";


	}
// This code was written by Tyler Akins and has been placed in the
// public domain.  It would be nice if you left this header intact.
// Base64 code from Tyler Akins -- http://rumkin.com

var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

function encode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + 
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);
   
   return output;
}

function decode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length);

   return output;
}

function SetScreenOutline(params,height)	// height => factor,to adjust the height of the contentDiv  
{
	var factor = document.getElementById('treeTd').offsetWidth; 
	if(typeof(params)== 'undefined')
		params = '';
		
	if(params != 'InfiniV')// ultimate jugar to adjust the height of newly uploaded page from Infini V
	{
		document.getElementById('contentdiv1').style.height = document.body.clientHeight - 85 + 'px'; 
		// document.getElementById('ajaxDivContent').style.height = document.body.clientHeight - 85 + 'px'; 
	}
	if(typeof(height) != 'undefined')
		document.getElementById('contentdiv1').style.height = document.body.clientHeight - height + 'px';
		
	document.getElementById('ajaxDivContent').style.width = document.body.clientWidth - factor + 'px'; 
	document.getElementById('contentdiv1').style.width = document.body.clientWidth - factor-25 + 'px'; 
}

function customWizCallback(response) // productWizCallBack
{
	if(document.getElementById('wizardDiv'))
		var wizardDiv = document.getElementById('wizardDiv');
	else
	{
		var wizardDiv = document.createElement('div');
		document.getElementById('projectSummaryDivContent').innerHTML = "";
		document.getElementById('projectSummaryDivContent').appendChild(wizardDiv);
		wizardDiv.style.display = 'none';
	}
	if(typeof(wizardDiv) != 'undefined')
	{
		try
		{
			AjaxCallbackWorker(response,wizardDiv);
			wizardDiv.style.display="block";
		}
		catch(e)
		{
			alert("Exception::Function__ customWizCallback1()::"+e);
		}
	}
}
function OpenControl(url,params,isScroll,width,height,wname,closeFlag)
{
	//OpenWizard('/misc/uploadfiles.php?label=<?=$label?>&projectid=<?=$projectid?>&projectpath=<?=$projectpath?>&type=<?=$caller?>','no','0.5','0.45','url','true');
	var obj = OpenWizard(url+'?'+params,isScroll,width,height,wname,closeFlag);
	obj.setCloseCallback(onCloseControlCB);
}