<?php

	date_default_timezone_set('Asia/Karachi');
	//echo date("m-d-y h:i:s");
	//session_start();

	//This should contain something like www.zend.com
	error_reporting(E_ERROR |  E_PARSE );
	require_once("globalpath.php");
	//This gets the path part of the URI.  It should get something
	//like /~leon/fe/htdocs/, but it could be just /
	//define("EXTERNAL_PATH", eregi_replace(SCRIPT_NAME, "", $SCRIPT_NAME));

	/*
	** set constants
	*/
	require_once(MODULE . "/utility/configuration.php");
	require_once(MODULE . "/utility/standard_library.php");
	require_once(MODULE . "/utility/initialization.php");
	require_once(MODULE . "/utility/ScreenInfo.php");	// need to enter every sceen in it
	require_once(MODULE . "/class/datalayer/dataaccess.php");
	require_once(MODULE . "/class/common/session.php");
	//include(APPLICATION_ROOT."/html/fckeditor/fckeditor.php");

	// traceMessage("test");
	/*
	** Execute action module, if requested.
	** Check for actions that include paths or other suspicious characters.
	*/
	global $mysession;
	$mysession = new session();
	/*user for ordering from menu card*/
	if(!isset($_SESSION['sid']) || $_SESSION['sid']=="")
	{
		$mysession_temp = new session();
		$mysession_temp->startsession("","");
		$sid = $mysession_temp->sessionid;
		$_SESSION['sid'] = $sid;
		
		echo "<script>location.href='/login.php'</script>";
		echo "<script>alert('Your sesion has been expired');</script>";
		
	}
	else
	{
		$sid = $_SESSION['sid'];
		$succ=$mysession->updatesession($sid);
		if(!$succ) {
			echo "<script>alert('Your sesion has been expired');</script>";
			echo "<script>location.href='/login.php'</script>";
		}
	}

	 

	// print_r($_SESSION."USAMA");
	// print_r_pre($_SESSION);
	// traceMessage("USAMA".print_r_log($_SESSION));
	// die();

	traceMessage("ACTIOn::$ACTION");
	global $sid;
	require_once(MODULE."/class/ajax/pserver.php");

	if(!isset($SCREEN) || $SCREEN=="")
	{
		//echo "<script>location.href='http://".SERVER_NAME."/default.php';</script>";
		echo "<script>location.href='/login.php';</script>";
	}
	if (!isset($sid)|| $sid=='')
	{
		if((isset($SCREEN) && $SCREEN == "login")|| strtolower($inputsubmit1)=="sign in" ) 
		{
			if(isset($user) && $user!='')
			{

				// if(isset($sid_menu) && $sid_menu!="")
				// {
				// 	$mysession_temp->updatesession($sid_menu);
				// 	$data = $mysession_temp->getvalue("product-$sid_menu");
				// 	$isOsCommerce = $mysession_temp->getvalue("is_oscommerce");
				// }
				// if(isset($jobsid) && $jobsid!="")
				// {
				// 	$mysession_temp->updatesession($jobsid);
				// 	$jobRecordId = $mysession_temp->getvalue("jobrecordid");
				// 	$mysession_temp->closesession($jobsid);

				// }
				$ret=Authenticate($user,$pass);
				if($ret["code"]!="0")
				{
					$e1 = new ErrorData(__FUNCTION__ . $ret["desc"],__FILE__,__LINE__,'notice');
					reportError($e1);

						$SCREEN="login";
				}
				else
				{
					$SCREEN="logged";
					$sid = $ret["sid"];
					if(trim($data)!="" && strstr($data,"partnerid"))
					{
						$mysession->setvalue("product-$sid",$data);
						$mysession->setvalue("gotoshop","yes");
						$mysession->setvalue("is_oscommerce",$isOsCommerce);
					}
					if(trim($jobRecordId)!="" && $jobRecordId>0)
					{
						$mysession->setvalue("jobRecordId",$jobRecordId);
					}

				}
			}
		}
		else
		{
			$SCREEN="welcome";
		}
	}
	else
	{
		$succ = $mysession->updatesession($sid);
		if (!$succ) {
			$mysession->closesession($_SESSION['sid']);
			$_SESSION['sid'] = "";
			echo "<script> location.href = 'login.php';</script>";
		}
	}

	if(isset($ACTION) AND ($ACTION != ""))
	{
		if(preg_match('/^[A-Z_0-9]*$/i', $ACTION) AND
			file_exists(MODULE . "/action/$ACTION.php"))
		{
			//try, as much as possible, to disallow actions from being interrupted
			set_time_limit(ACTION_TIME_LIMIT);
			$userAbortOrig = ignore_user_abort(TRUE);

			if(!include(MODULE . "/action/$ACTION.php"))
			{
				array_push($ActionResults, "\"$ACTION\" failed!");
			}

			//restore default settings
			set_time_limit(DEFAULT_TIME_LIMIT);
			ignore_user_abort($userAbortOrig);
		}
		else
		{
			array_push($ActionResults, "\"$ACTION\" is not a valid action.");
		}
	}
	/*
	** If we aren't given a screen, go home
	*/

?>

<?php
	/*
	** Start page
	*/

	print('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" ');
	print('"http://www.w3.org/TR/html40/loose.dtd">');
	print("\n");

	print("<HTML>\n");
	print("<HEAD>\n");
	echo "<style type='text/css'>body,table { margin: 0; padding: 0; }</style>";
	//print("<TITLE>" . $ScreenInfo[$SCREEN][SI_TITLE] . "</TITLE>\n");

	print("<TITLE>".TITLE."</TITLE>\n");?>
		<link rel="shortcut icon" type="image/x-icon" href="images/ats_logo.png" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="<?=$description?>" />
		<meta name="author" content="<?=$author?>" />
		<title>WAPSI Car</title>


	
		<link rel="stylesheet" media="screen, print" href="css/datagrid/datatables/datatables.bundle.css">
		<?php
	       print("<link rel='stylesheet' type='text/css' href='/css/style.css' />");

      	?>

       <script src='/scripts/utils.js'></script>
       <script src='/scripts/prototype_custom.js'></script>
       <!--<script src='/scripts/jquery-1.7.1.min.js'></script>-->
       <script src='/scripts/json.js'></script>
       <script src='/scripts/regexp.js'></script>



		<script type="text/javascript" src="/scripts/jquery-1.7.1.js"></script>
		<script type="text/javascript" src="/scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="/scripts/scripts.js"></script>




	   <script type='text/javascript' src='/scripts/bookmarks.js'></script>
	   
       <?php

    print("</HEAD>\n");





	/*
	** go to layout module, that will in turn use navigation and screen modules
	*/



	if(file_exists(MODULE . "/layout/".ALIAS.".".$ScreenInfo[$SCREEN][SI_LAYOUT].".php"))
	{
		if(!include(MODULE . "/layout/".ALIAS.".".$ScreenInfo[$SCREEN][SI_LAYOUT].".php"))
		{
			print("An error occured while laying out the page1<br>\n");
		}
	}
	else
	{

		if(!include(MODULE . "/layout/".$ScreenInfo[$SCREEN][SI_LAYOUT].".php"))
		{
			print("An error occured while laying out the page<br>\n");
		}
	}

	/*
	** close page
	*/
	ShowMessages();

	print("</HTML>\n");
?>
<script>
		var _sessionid_='<?php echo $sid?>';
		var _browsername_='IE';

		function loadWindow()
		{
			<?php
			$url = "index.php?SCREEN=maincontroller&sid=".$mysession->sessionid;
			?>
			parent.location.href='<?=$url?>';

		}
	window.onload = function(){
	    bookmarks.initialize();
	}
</script>
